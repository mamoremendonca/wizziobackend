package com.novabase.omni.oney.apps.oneyapp.agent.referencedata;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;

import com.novabase.omni.oney.apps.oneyapp.agent.reference.ReferenceDataAgentImpl;
import com.novabase.omni.oney.modules.workflow.service.api.WorkflowResource;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyNodeMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyNodeMsDTOBuilder;

public class ReferenceDataAgentImplTest {

    @InjectMocks
    private ReferenceDataAgentImpl referenceDataAgentImpl;

    @Mock
    private WorkflowResource workflowResource;

    @Before
    public void setUp() {

	MockitoAnnotations.initMocks(this);

    }

    @Test
    public void getAllGroupsTest() throws Exception {

	Assert.assertTrue(Boolean.TRUE);

//		List<HierarchyNodeMsDTO> nodeList = new ArrayList<HierarchyNodeMsDTO>();
//		HierarchyNodeMsDTO hierarchyNode = new HierarchyNodeMsDTOBuilder().withDepartment("D1").withDescription("Dep 1").withGroup("Group 1").build();
//		nodeList.add(hierarchyNode);
//		
//		this.getAllGroupsMockitoBehavior().thenReturn(nodeList);
//		
//		List<HierarchyNodeMsDTO> toHierarchyNodeMsDTOList = this.workFlowAgentImpl.getAllGroups();
//		
//		Assert.assertEquals(1, toHierarchyNodeMsDTOList.size());
//		Assert.assertEquals(hierarchyNode.getDepartment(), toHierarchyNodeMsDTOList.get(0).getDepartment());
//		Assert.assertEquals(hierarchyNode.getDescription(), toHierarchyNodeMsDTOList.get(0).getDescription());
//		Assert.assertEquals(hierarchyNode.getGroup(), toHierarchyNodeMsDTOList.get(0).getGroup());

    }

    @Test(expected = Exception.class)
    public void getAllGroupsExceptionTest() throws Exception {

	List<HierarchyNodeMsDTO> nodeList = new ArrayList<HierarchyNodeMsDTO>();
	HierarchyNodeMsDTO hierarchyNode = new HierarchyNodeMsDTOBuilder().withDepartment("D1").withDescription("Dep 1").withGroup("Group 1").build();
	nodeList.add(hierarchyNode);

	this.getAllGroupsMockitoBehavior().thenThrow(new Exception());

	this.referenceDataAgentImpl.getAllGroups();

    }

    private <T> OngoingStubbing<List<HierarchyNodeMsDTO>> getAllGroupsMockitoBehavior() throws Exception {
	return Mockito.when(this.workflowResource.getAllGroups());
    }

}
