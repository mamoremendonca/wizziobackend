package com.novabase.omni.oney.apps.oneyapp.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;

import com.novabase.omni.oney.apps.oneyapp.agent.reference.ReferenceDataAgentImpl;
import com.novabase.omni.oney.apps.oneyapp.agent.workflow.WorkflowAgentImpl;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.GroupDTO;
import com.novabase.omni.oney.apps.oneyapp.enums.ReferenceDataType;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyNodeMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyNodeMsDTOBuilder;

public class ReferenceDataManagerTest {

    @InjectMocks
    private ReferenceDataManager referenceDataManager;

    @Mock
    private ReferenceDataAgentImpl referenceAgentImpl;

    @Before
    public void setUp() {
	MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getReferenceDataTest() throws Exception {

	List<HierarchyNodeMsDTO> nodeList = new ArrayList<HierarchyNodeMsDTO>();
	HierarchyNodeMsDTO hierarchyNode = new HierarchyNodeMsDTOBuilder().withDepartment("D1").withDescription("Dep 1").withGroup("Group 1").build();
	nodeList.add(hierarchyNode);

	this.getAllGroupsMockitoBehavior().thenReturn(nodeList);

	final List<String> filters = new ArrayList<String>();
	filters.add(ReferenceDataType.GROUP.name());
	final Map<String, List<GroupDTO>> resultMap = this.referenceDataManager.getReferenceData(filters);
	Assert.assertEquals(1, resultMap.size());

	final List<GroupDTO> resultList = resultMap.get(ReferenceDataType.GROUP.name());

	Assert.assertNotNull(resultList);
	Assert.assertEquals(1, resultList.size());
	Assert.assertEquals(hierarchyNode.getDepartment(), resultList.get(0).getDepartmentCode());
	Assert.assertTrue(resultList.get(0).active);
	Assert.assertEquals(hierarchyNode.getGroup(), resultList.get(0).code);
	Assert.assertEquals(String.format("%s - %s", hierarchyNode.getGroup(), hierarchyNode.getDescription()), resultList.get(0).description);

    }

    @Test(expected = Exception.class)
    public void getReferenceDataExceptionTest() throws Exception {

	List<HierarchyNodeMsDTO> nodeList = new ArrayList<HierarchyNodeMsDTO>();
	HierarchyNodeMsDTO hierarchyNode = new HierarchyNodeMsDTOBuilder().withDepartment("D1").withDescription("Dep 1").withGroup("Group 1").build();
	nodeList.add(hierarchyNode);

	this.getAllGroupsMockitoBehavior().thenThrow(new Exception());

//	this.referenceDataManager();

    }

    private <T> OngoingStubbing<List<HierarchyNodeMsDTO>> getAllGroupsMockitoBehavior() throws Exception {
	return Mockito.when(this.referenceAgentImpl.getAllGroups());
    }

}
