package com.novabase.omni.oney.apps.oneyapp.mapper;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.novabase.omni.oney.apps.oneyapp.dto.reference.GroupDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyNodeMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyNodeMsDTOBuilder;

public class ReferenceDataMapperTest {

    @Before
    public void setUp() {
	
    }
    
    @Test
    public void toGroupDTOTest() {
	
	HierarchyNodeMsDTO hierarchyNode = new HierarchyNodeMsDTOBuilder().withDepartment("D1").withDescription("Dep 1").withGroup("Group 1").build();
	GroupDTO group = ReferenceDataMapper.INSTANCE.toGroupDTO(hierarchyNode);
	
	Assert.assertEquals(hierarchyNode.getDepartment(), group.getDepartmentCode());
	Assert.assertTrue(group.active);
	Assert.assertEquals(hierarchyNode.getGroup(), group.code);
	Assert.assertEquals(String.format("%s - %s", hierarchyNode.getGroup(), hierarchyNode.getDescription()), group.description);
	
    }
    
    @Test
    public void toGroupDTOListTest() {
	
	List<HierarchyNodeMsDTO> nodeList = new ArrayList<HierarchyNodeMsDTO>();
	HierarchyNodeMsDTO hierarchyNode = new HierarchyNodeMsDTOBuilder().withDepartment("D1").withDescription("Dep 1").withGroup("Group 1").build();
	nodeList.add(hierarchyNode);
	
	List<GroupDTO> groupList = ReferenceDataMapper.INSTANCE.toGroupDTOList(nodeList);
	
	Assert.assertEquals(hierarchyNode.getDepartment(), groupList.get(0).getDepartmentCode());
	Assert.assertTrue(groupList.get(0).active);
	Assert.assertEquals(hierarchyNode.getGroup(), groupList.get(0).code);
	Assert.assertEquals(String.format("%s - %s", hierarchyNode.getGroup(), hierarchyNode.getDescription()), groupList.get(0).description);
	
    }
    
}
