/*-
 * #%L
 * Apps :: Purchase Order App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.oneyapp.agent.reference;

import java.util.ArrayList;
import java.util.List;

import javax.cache.annotation.CacheResult;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.apps.oneyapp.AppContext;
import com.novabase.omni.oney.apps.oneyapp.AppProperties;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.ReferenceDataDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.ReferenceDataResource;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.AssetMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.CostCenterMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.DepartmentMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.InternalOrderMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ItemMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ItemTypeMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ProjectMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ReferenceDataMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ReferenceDataMsDTOBuilder;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.RetentionCodeMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SearchFilterMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierGeneralDataMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.TaxMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.WorkflowResource;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyNodeMsDTO;

import io.digitaljourney.platform.modules.mvc.api.agent.AbstractCoreAgent;

/**
 * Implementation of an Agent to communicate with the Reference micro-service.
 *
 */
// @formatter:off
@Component(
	service = { Object.class, ReferenceDataAgentImpl.class },
	configurationPid = ReferenceAgentConfig.CPID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	reference = {
		@Reference(
			name = AppProperties.REF_CONTEXT,
			service = AppContext.class,
			cardinality = ReferenceCardinality.MANDATORY
		)
	}
)
@Designate(ocd = ReferenceAgentConfig.class)
// @formatter:on
public class ReferenceDataAgentImpl extends AbstractCoreAgent<AppContext, ReferenceAgentConfig> {

    @Reference
    private ReferenceDataResource referenceData;
    
    @Reference
    public WorkflowResource workflowResource;

    /**
     * Method called whenever the component is activated.
     *
     * @param ctx    Component context
     * @param config Component configuration
     */
    @Activate
    public void activate(ComponentContext ctx, ReferenceAgentConfig config) {
	prepare(ctx, config);
    }

    /**
     * Method called whenever the component configuration changes.
     *
     * @param config Component configuration
     */
    @Modified
    public void modified(ReferenceAgentConfig config) {
	prepare(config);
    }

    @CacheResult(cacheName = AppProperties.CACHE_REFERENCE_SUPPLIERS)
    public List<SupplierMsDTO> getSuppliers(SearchFilterMsDTO searchFilter) {

	try {
	    return this.referenceData.getSuppliers(searchFilter);
	} catch (Exception e) {
	    if (e.getMessage() != null && (e.getMessage().contains("ZAPI_WIZZIO/001") || e.getMessage().contains("There is no data"))) {
		return new ArrayList<>();
	    }
	    throw e;
	}
    }

    @CacheResult(cacheName = AppProperties.CACHE_REFERENCE_ITEMS)
    public List<ItemMsDTO> getItems() {
	try {
	    return this.referenceData.getItems();
	} catch (Exception e) {
	    if (e.getMessage() != null && (e.getMessage().contains("ZAPI_WIZZIO/001") || e.getMessage().contains("There is no data"))) {
		return new ArrayList<>();
	    }
	    throw e;
	}
    }

    @CacheResult(cacheName = AppProperties.CACHE_REFERENCE_ITEM_TYPES)
    public List<ItemTypeMsDTO> getItemTypes() {
	try {
	    return this.referenceData.getItemTypes();
	} catch (Exception e) {
	    if (e.getMessage() != null && (e.getMessage().contains("ZAPI_WIZZIO/001") || e.getMessage().contains("There is no data"))) {
		return new ArrayList<>();
	    }
	    throw e;
	}
    }

    @CacheResult(cacheName = AppProperties.CACHE_REFERENCE_TAXES)
    public List<TaxMsDTO> getTaxes() {
	try {
	    return this.referenceData.getTaxes();
	} catch (Exception e) {
	    if (e.getMessage() != null && (e.getMessage().contains("ZAPI_WIZZIO/001") || e.getMessage().contains("There is no data"))) {
		return new ArrayList<>();
	    }
	    throw e;
	}
    }

    @CacheResult(cacheName = AppProperties.CACHE_REFERENCE_COST_CENTERS)
    public List<CostCenterMsDTO> getCostCenters() {
	try {
	    return this.referenceData.getCostCenters();
	} catch (Exception e) {
	    if (e.getMessage() != null && (e.getMessage().contains("ZAPI_WIZZIO/001") || e.getMessage().contains("There is no data"))) {
		return new ArrayList<>();
	    }
	    throw e;
	}
    }

    @CacheResult(cacheName = AppProperties.CACHE_REFERENCE_PROJECTS)
    public List<ProjectMsDTO> getProjects() {
	try {
	    return this.referenceData.getProjects();
	} catch (Exception e) {
	    if (e.getMessage() != null && (e.getMessage().contains("ZAPI_WIZZIO/001") || e.getMessage().contains("There is no data"))) {
		return new ArrayList<>();
	    }
	    throw e;
	}
    }

    @CacheResult(cacheName = AppProperties.CACHE_REFERENCE_INTERNAL_ORDERS)
    public List<InternalOrderMsDTO> getInternalOrdens() {
	try {
	    return this.referenceData.getInternalOrdens();
	} catch (Exception e) {
	    if (e.getMessage() != null && (e.getMessage().contains("ZAPI_WIZZIO/001") || e.getMessage().contains("There is no data"))) {
		return new ArrayList<>();
	    }
	    throw e;
	}
    }

    @CacheResult(cacheName = AppProperties.CACHE_REFERENCE_RETENTION_CODES)
    public List<RetentionCodeMsDTO> getRetentionCode() {
	try {
	    return this.referenceData.getRetentionCode();
	} catch (Exception e) {
	    if (e.getMessage() != null && (e.getMessage().contains("ZAPI_WIZZIO/001") || e.getMessage().contains("There is no data"))) {
		return new ArrayList<>();
	    }
	    throw e;
	}
    }

    @CacheResult(cacheName = AppProperties.CACHE_REFERENCE_ASSETS)
    public List<AssetMsDTO> getAssets() {
	try {
	    return this.referenceData.getAssets();
	} catch (Exception e) {
	    if (e.getMessage() != null && (e.getMessage().contains("ZAPI_WIZZIO/001") || e.getMessage().contains("There is no data"))) {
		return new ArrayList<>();
	    }
	    throw e;
	}
    }

    @CacheResult(cacheName = AppProperties.CACHE_REFERENCE_SUPPLIERS_GANERALDATA)
    public SupplierGeneralDataMsDTO getSupplierMarketInfo(final String supplierCode) {

	final SupplierGeneralDataMsDTO msResult = this.referenceData.getSupplierMarketInfo(supplierCode);
	return msResult != null ? msResult : new SupplierGeneralDataMsDTO();

    }
    
    @CacheResult(cacheName = AppProperties.CACHE_REFERENCE_DATA_DEPARTMENTS)
    public List<DepartmentMsDTO> getDepartments() {

	try {
	    return this.referenceData.getDepartments();
	} catch (Exception e) {
	    
	    // CHECK FOR NO DATA ERROR AND RETURNS EMPTY LIST
	    if (e.getMessage() != null && (e.getMessage().contains("ZAPI_WIZZIO/001") || e.getMessage().contains("There is no data"))) {
		return new ArrayList<>();
	    }
	    
	    throw e;
	    
	}
	
    }
    
    /**
     * returns the list of departments a
     * 
     * @param userGroup
     * @return List of Departments in the userGroup subtree (Organizational
     *         Structure)
     */
    @CacheResult(cacheName = AppProperties.CACHE_GROUPS)
    public List<HierarchyNodeMsDTO> getAllGroups() {
	return this.workflowResource.getAllGroups();
    }

}
