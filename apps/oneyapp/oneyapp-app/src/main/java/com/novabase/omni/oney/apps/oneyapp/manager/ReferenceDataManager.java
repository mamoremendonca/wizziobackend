package com.novabase.omni.oney.apps.oneyapp.manager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.shiro.util.CollectionUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.oneyapp.agent.reference.ReferenceDataAgentImpl;
import com.novabase.omni.oney.apps.oneyapp.dto.SearchFilterDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.SupplierDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.SupplierGeneralDataDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.DepartmentDTO;
import com.novabase.omni.oney.apps.oneyapp.enums.ReferenceDataType;
import com.novabase.omni.oney.apps.oneyapp.mapper.ReferenceDataMapper;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierGeneralDataMsDTO;

@Component(service = { ReferenceDataManager.class })
public class ReferenceDataManager {

    @Reference
    private ReferenceDataAgentImpl referenceAgent;

    public <T> Map<String, List<T>> getReferenceData(final List<String> referenceList) {
	
	final Map<String, List<T>> resultMap = new HashMap<>();
	if (!CollectionUtils.isEmpty(referenceList)) {
	    referenceList.stream().forEach(referenceName -> resultMap.put(referenceName, ReferenceDataType.valueOf(referenceName).getData(this.referenceAgent)));
	}
	
	return resultMap;
	
    }

    public List<SupplierDTO> searchSuppliers(SearchFilterDTO filter) {
	
	return this.referenceAgent.getSuppliers(ReferenceDataMapper.INSTANCE.toSearchFilter(filter)).stream().
		map(s -> ReferenceDataMapper.INSTANCE.toSupplierDTO(s)).
		collect(Collectors.toList());
	
    }
    
    public SupplierGeneralDataDTO getSupplierMarketInfo(final String supplierCode) {
	
	final SupplierGeneralDataMsDTO msResult = this.referenceAgent.getSupplierMarketInfo(supplierCode);
	return msResult.getMarketType() != null ? ReferenceDataMapper.INSTANCE.toSupplierGeneralDataDTO(this.referenceAgent.getSupplierMarketInfo(supplierCode)) 
						: null;
	
    }
    
}
