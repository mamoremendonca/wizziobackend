package com.novabase.omni.oney.apps.oneyapp.manager;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.oneyapp.agent.workflow.WorkflowAgentImpl;

@Component(service = { WorkflowManager.class })
public class WorkflowManager {

    @Reference
    private WorkflowAgentImpl workflowAgent;
    
    public List<String> getGroups() {
	return this.workflowAgent.getGroups();
    }
    
}
