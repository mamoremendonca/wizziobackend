package com.novabase.omni.oney.apps.oneyapp.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class GenericProcessDTO implements Serializable {

    private static final long serialVersionUID = 1733641780747946146L;
    
    @ApiModelProperty
    public Long instanceId;

    @ApiModelProperty
    public String number;
    
    @ApiModelProperty
    public String friendlyNumber;
    
    @ApiModelProperty
    public String journeyType;
    
    @ApiModelProperty
    public String entityCode;
    
    @ApiModelProperty
    public String departmentCode;
    
    @ApiModelProperty
    public String status;
    
    @ApiModelProperty
    public String creationDate;
    
    @ApiModelProperty
    public String expirationDate;
    
    @ApiModelProperty
    public Double value;

}
