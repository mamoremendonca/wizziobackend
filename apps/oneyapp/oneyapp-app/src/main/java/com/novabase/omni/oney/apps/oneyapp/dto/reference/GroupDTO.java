package com.novabase.omni.oney.apps.oneyapp.dto.reference;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("GroupDTO")
public class GroupDTO extends ReferenceDataDTO {

    @ApiModelProperty
    private String departmentCode;

    public GroupDTO() {
	super();
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }
    
}
