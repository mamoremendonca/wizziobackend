package com.novabase.omni.oney.apps.oneyapp.enums;

import java.util.List;

import com.novabase.omni.oney.apps.oneyapp.agent.reference.ReferenceDataAgentImpl;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.AssetDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.CostCenterDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.DepartmentDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.GroupDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.InternalOrderDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.ItemDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.ItemTypeDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.ProjectDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.ReferenceDataDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.RetentionCodeDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.TaxDTO;
import com.novabase.omni.oney.apps.oneyapp.providers.ReferenceDataProvider;

public enum ReferenceDataType {
    
    ASSET() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AssetDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getAssets();
	}
	
    }, 
    
    COST_CENTER() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CostCenterDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getCostCenters();
	}
	
    }, 
    
    DOCUMENT_TYPE() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getDocumentTypes();
	}
	
    }, 
    
    INTERNAL_ORDER() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<InternalOrderDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getInternalOrdens();
	}
	
    }, 
    
    ITEM() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ItemDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getItems();
	}
	
    }, 
    
    ITEM_TYPE() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ItemTypeDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getItemTypes();
	}
	
    }, 
    
    PROJECT() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ProjectDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getProjects();
	}
	
    }, 
    
    RETENTION_CODE() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RetentionCodeDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getRetentionCode();
	}
	
    }, 
    
    TAX() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TaxDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getTaxes();
	}
	
    }, 
    
    WORK_LIST_FILTER_COLUMNS() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getWorkListFilterColumns();
	}
	
    }, 
    
    PURCHASE_ORDER_WORK_LIST_STATUS() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getPurchaseOrderWorkListStatus();
	}
	
    },
    
    JOURNEY_TYPE() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getJourneyType();
	}
	
    }, 
    
    ORDER_TYPE() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getOrderType();
	}
	
    },

    DEPARTMENT() {

	@SuppressWarnings("unchecked")
	@Override
	public List<DepartmentDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getDepartments();
	}

    },
    
    GROUP() {

	@SuppressWarnings("unchecked")
	@Override
	public List<GroupDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getAllGroups();
	}

    };

    public abstract <T> List<T> getData(final ReferenceDataAgentImpl agent);
    
    
}
