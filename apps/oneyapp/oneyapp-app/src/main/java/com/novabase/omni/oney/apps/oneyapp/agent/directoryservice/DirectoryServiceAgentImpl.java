/*-
 * #%L
 * Apps :: Purchase Order App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.oneyapp.agent.directoryservice;

import java.util.Arrays;
import java.util.List;

//import javax.cache.annotation.CacheResult;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.apps.oneyapp.AppContext;
import com.novabase.omni.oney.apps.oneyapp.AppProperties;
import com.novabase.omni.oney.apps.oneyapp.dto.GroupDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.PrincipalDTO;
import com.novabase.omni.oney.apps.oneyapp.mapper.DirectoryServiceMapper;
import com.novabase.omni.oney.modules.directoryservice.service.api.DirectoryServiceResource;

import io.digitaljourney.platform.modules.mvc.api.agent.AbstractCoreAgent;
import io.digitaljourney.platform.plugins.modules.uam.service.api.UAMResource;
import io.digitaljourney.platform.plugins.modules.uam.service.api.dto.UserDTO;
import io.digitaljourney.platform.plugins.modules.uam.service.api.dto.UserSearchDTO;

/**
 * Implementation of an Agent to communicate with the DirectoryService
 * micro-service.
 *
 */
// @formatter:off
@Component(service = { Object.class,
		DirectoryServiceAgentImpl.class }, configurationPid = DirectoryServiceAgentConfig.CPID, configurationPolicy = ConfigurationPolicy.REQUIRE, reference = {
				@Reference(name = AppProperties.REF_CONTEXT, service = AppContext.class, cardinality = ReferenceCardinality.MANDATORY) })
@Designate(ocd = DirectoryServiceAgentConfig.class)
// @formatter:on
public class DirectoryServiceAgentImpl extends AbstractCoreAgent<AppContext, DirectoryServiceAgentConfig> {

    @Reference
    private DirectoryServiceResource directoryService;

    @Reference
    private UAMResource uamResource;

    /**
     * Method called whenever the component is activated.
     *
     * @param ctx    Component context
     * @param config Component configuration
     */
    @Activate
    public void activate(ComponentContext ctx, DirectoryServiceAgentConfig config) {
	prepare(ctx, config);
    }

    /**
     * Method called whenever the component configuration changes.
     *
     * @param config Component configuration
     */
    @Modified
    public void modified(DirectoryServiceAgentConfig config) {
	prepare(config);
    }

    public List<GroupDTO> getGroups(String userName) {

	try {
	    UserSearchDTO searchUserFilter = new UserSearchDTO();

	    if (userName.contains("\\")) {
		userName = userName.substring(userName.indexOf("\\") + 1);
	    }

	    if (getConfig().userAdminOney().equals(userName) || "admin".contentEquals(userName)) {
		return Arrays.asList(new GroupDTO("ADMIN", "ADMIN"));
	    }

	    searchUserFilter.setName(userName);

	    List<UserDTO> users = uamResource.findUsers(searchUserFilter);

	    // TODO: rever workarround para continuar a conseguir testar com users fora da
	    // ad, tambem para conseguir logar com admin, em produção devemos remover isso,
	    // em QUA tambem !!!!!
	    //@formatter:off
			if (users != null && users.isEmpty() == false) {
				if ("ldapAuthenticator".equals(users.get(0).getAuthenticatorName()) == false) {
					return Arrays.asList(
							new GroupDTO("SG-1400", "Digitalização (SG-1400)"),
							new GroupDTO("WIZZIOCAPUsers", "Contas a Pagar (WIZZIOCAPUsers)"),
							new GroupDTO("SG-6410", "Colaborador (SG-6410)"),
							new GroupDTO("SG-6410n-2", "Team Leader (SG-6410n-2)"),
							new GroupDTO("SG-6400n-1", "Manager (SG-6400n-1)"),
							new GroupDTO("SG-6000n", "Director DR (SG-6000n)"),
							new GroupDTO("SG-1000n", "Diretor DAF (SG-1000n)"),
							new GroupDTO("SG-0000n", "Diretor DG (SG-0000n)"));
				}
			}
			//@formatter:on

	} catch (Exception e) {
	    info(e.getMessage());
	}

//	if (this.getConfig().offlineMode()) {
//	    // @formatter:off
//			return Arrays.asList(
//        				new GroupDTO("SG-1400", "Digitalização (SG-1400)"),
//        				new GroupDTO("WIZZIOCAPUsers", "Contas a Pagar (WIZZIOCAPUsers)"),
//        				new GroupDTO("SG-6410", "Colaborador (SG-6400)"),
//					new GroupDTO("SG-6410n-2", "Team Leader (SG-6410n-2)"),
//					new GroupDTO("SG-6400n-1", "Manager (SG-6400n-1)"),
//					new GroupDTO("SG-6000n", "Director DR (SG-6000n)"),
//					new GroupDTO("SG-1000n", "Diretor DAF (SG-1000n)"),
//					new GroupDTO("SG-0000n", "Diretor DG (SG-0000n)"));
//			// @formatter:on
//	} else {
	    return DirectoryServiceMapper.INSTANCE.toListGroupDTO(directoryService.getGroups(getConfig().provider(), userName));
//	}

    }

    public List<PrincipalDTO> searchUsers() {

	return DirectoryServiceMapper.INSTANCE.toListPrincipalDTO(directoryService.searchUsers(getConfig().provider(), ""));
    }

}
