package com.novabase.omni.oney.apps.oneyapp.dto.reference;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("CostCenterDTO")
public class CostCenterDTO extends ReferenceDataDTO {

    @ApiModelProperty(value = "Department")
    public String departmentCode;

}
