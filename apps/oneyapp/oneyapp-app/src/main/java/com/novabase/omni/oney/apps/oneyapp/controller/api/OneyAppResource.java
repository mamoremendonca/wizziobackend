/*-
 * #%L
 * Apps :: Oney Boot App App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.oneyapp.controller.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.novabase.omni.oney.apps.oneyapp.AppProperties;
import com.novabase.omni.oney.apps.oneyapp.dto.GenericProcessDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.GroupDTO;
import com.novabase.omni.oney.apps.oneyapp.exception.OneyAppException;
import com.novabase.omni.oney.apps.oneyapp.exception.OneyAppNotFoundException;
import com.novabase.omni.oney.modules.oneycommon.service.api.dto.worklist.SearchCriteriaWorklistDTO;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.FilterAttribute;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.OrderType;

import io.digitaljourney.platform.modules.commons.type.HttpStatusCode;
import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.ApiKeyAuthDefinition.ApiKeyLocation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.BasicAuthDefinition;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;

//@formatter:off
/**
 * Base Oney Boot App Resource, this is only used for documentation purposes.
 * *
 */
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path(AppProperties.ADDRESS)
@SwaggerDefinition(
	securityDefinition = @SecurityDefinition(
		basicAuthDefinitions = {
			@BasicAuthDefinition(key = RSProperties.SWAGGER_BASIC_AUTH),
		},
		apiKeyAuthDefinitions = {
			@ApiKeyAuthDefinition(
				key = RSProperties.SWAGGER_BEARER_AUTH,
				name = RSProperties.HTTP_HEADER_API_KEY,
				in = ApiKeyLocation.HEADER)
			}
	),
	schemes = {
		SwaggerDefinition.Scheme.HTTP,
		SwaggerDefinition.Scheme.HTTPS,
		SwaggerDefinition.Scheme.DEFAULT
	},
	info = @Info(
		title = "Oney Boot App API",
		description = "The Oney Boot App API.",
		version = AppProperties.CURRENT_VERSION,
		license = @License(name = "Digital Journey License", url = "http://www.digitaljourney.io/license")
	),
	basePath = "bin/mvc.do/"+AppProperties.ADDRESS
)
@Api(
	value = "Oney Boot App API",
	authorizations = {
		@Authorization(value = RSProperties.SWAGGER_BASIC_AUTH),
		@Authorization(value = RSProperties.SWAGGER_BEARER_AUTH)
	}
)
@ApiResponses(
	value = {
		@ApiResponse(code = HttpStatusCode.UNAUTHORIZED_CODE, message = RSProperties.SWAGGER_UNAUTHORIZED_MESSAGE),
		@ApiResponse(code = HttpStatusCode.FORBIDDEN_CODE, message = RSProperties.SWAGGER_FORBIDDEN_MESSAGE)
	}
)
//@formatter:on
public interface OneyAppResource {

    @GET
    @Path(AppProperties.ADDRESS_INIT)
    @ApiOperation(value = "Initializes basic configurations", notes = "Initializes basic configurations")
    public void init();
    
    @POST
    @Path(AppProperties.ADDRESS_WORKLIST)
    @ApiOperation(value = "Endpoint description", notes = "Retrives the worklist",  response = GenericProcessDTO.class, responseContainer = "List")
    public List<GenericProcessDTO> getWorklist(
	    SearchCriteriaWorklistDTO searchCriteriaDTO, 
	    @QueryParam("orderField") FilterAttribute orderField, 
	    @QueryParam("orderType") OrderType orderType);

    @GET
    @Path(AppProperties.ADDRESS_MEMBERSHIPS)
    @ApiOperation(value = "Retrieves User groups (memberships)", notes = "Returns the groups the users belongs to", response = GroupDTO.class, responseContainer = "List")
    @ApiResponses(value = { @ApiResponse(code = 200, response = GroupDTO.class, responseContainer = "List", message = "OK"),
	    @ApiResponse(code = 404, response = OneyAppNotFoundException.class, message = "User not found"),
	    @ApiResponse(code = 500, response = OneyAppException.class, message = "Internal Server Error") })
    public List<GroupDTO> getMemberships();

    @POST
    @Path(AppProperties.ADDRESS_GROUP)
    @ApiOperation(value = "Sets the selected User group", notes = "Saves the selected user group")
    @ApiResponses(value = { @ApiResponse(code = 200, response = GroupDTO.class, responseContainer = "List", message = "OK"),
	    @ApiResponse(code = 404, response = OneyAppNotFoundException.class, message = "User not found"),
	    @ApiResponse(code = 500, response = OneyAppException.class, message = "Internal Server Error") })
    public void setUserGroup(
	    @ApiParam(value = "User group selected", required = true) GroupDTO group);

    @GET
    @Path(AppProperties.ADDRESS_GROUP)
    @ApiOperation(value = "Gets the selected User group", notes = "Retrieve selected user group")
    @ApiResponses(value = { @ApiResponse(code = 200, response = GroupDTO.class, message = "OK"), @ApiResponse(code = 404, response = OneyAppNotFoundException.class, message = "User not found"),
	    @ApiResponse(code = 500, response = OneyAppException.class, message = "Internal Server Error") })
    public GroupDTO getUserGroup();
}
