/*-
 * #%L
 * Apps :: Purchase Order App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.oneyapp.agent.workflow;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Purchase Order Core Agent configuration.
 */
@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/agent/workflow", icon = @Icon(resource = "OSGI-INF/icon/agent.png", size = 32))
public @interface WorkflowAgentConfig {
    
	/** Component Persistent Identifier */
	public static final String CPID = "fs.oney.apps.oneyapp.agent.workflow";

	/**
	 * Gets the name of the user used to access core resources.
	 * 
	 * @return System user name
	 */
	@AttributeDefinition(name = "%systemUserName.name", description = "%systemUserName.description")
	public String systemUserName();

	/**
	 * Gets the password of the user to access core resources.
	 * 
	 * @return System user password
	 */
	@AttributeDefinition(name = "%systemPassword.name", type = AttributeType.PASSWORD, description = "%systemPassword.description")
	public String systemPassword();
	
	/**
	 * Gets the password of the user to access core resources.
	 * 
	 * @return System user password
	 */
	@AttributeDefinition(name = "%offlineMode.name", type = AttributeType.BOOLEAN, description = "%offlineMode.description")
	public boolean offlineMode() default false;
	
	/**
	 * Gets the master department name.
	 * 
	 * @return Master Department Name
	 */
	@AttributeDefinition(name = "%masterDepartment.name", type = AttributeType.STRING, description = "%masterDepartment.description")
	public String masterDepartment() default "SG-0000n";
	
}
