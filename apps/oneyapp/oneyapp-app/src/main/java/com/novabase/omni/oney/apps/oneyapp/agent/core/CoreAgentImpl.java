/*-
 * #%L
 * Apps :: Oney Boot App App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.oneyapp.agent.core;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.apps.oneyapp.AppContext;
import com.novabase.omni.oney.apps.oneyapp.AppProperties;
import com.novabase.omni.oney.apps.oneyapp.dto.GenericProcessDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.PrincipalDTO;
import com.novabase.omni.oney.apps.oneyapp.mapper.WorklistMapper;

import io.digitaljourney.platform.modules.mvc.api.agent.AbstractCoreAgent;
import io.digitaljourney.platform.plugins.modules.processcontinuity.service.api.ProcesscontinuityResource;
import io.digitaljourney.platform.plugins.modules.rulesengine.service.api.RulesEngineResource;
import io.digitaljourney.platform.plugins.modules.uam.service.api.UAMResource;
import io.digitaljourney.platform.plugins.modules.uam.service.api.dto.PermissionDTO;
import io.digitaljourney.platform.plugins.modules.uam.service.api.dto.PermissionSearchDTOBuilder;
import io.digitaljourney.platform.plugins.modules.uam.service.api.dto.RoleDTO;
import io.digitaljourney.platform.plugins.modules.uam.service.api.dto.RoleSearchDTO;
import io.digitaljourney.platform.plugins.modules.uam.service.api.dto.RoleSearchDTOBuilder;
import io.digitaljourney.platform.plugins.modules.uam.service.api.dto.SaveRoleDTOBuilder;
import io.digitaljourney.platform.plugins.modules.uam.service.api.dto.SaveUserDTOBuilder;
import io.digitaljourney.platform.plugins.modules.uam.service.api.dto.UserDTO;
import io.digitaljourney.platform.plugins.modules.uam.service.api.dto.UserSearchDTOBuilder;
import io.digitaljourney.platform.plugins.modules.uam.service.api.type.UserStatusType;

/**
 * Implementation of a Core Agent to communicate with micro-service.
 *
 */
// @formatter:off
@Component(service = { Object.class,
		CoreAgentImpl.class }, configurationPid = CoreAgentConfig.CPID, configurationPolicy = ConfigurationPolicy.REQUIRE, reference = {
				@Reference(name = AppProperties.REF_CONTEXT, service = AppContext.class, cardinality = ReferenceCardinality.MANDATORY) })
@Designate(ocd = CoreAgentConfig.class)
// @formatter:on
public class CoreAgentImpl extends AbstractCoreAgent<AppContext, CoreAgentConfig> {

	private static final String LDAP_AUTHENTICATOR = "ldapAuthenticator";

	@Reference
	private UAMResource uam;

	@Reference
	private RulesEngineResource rulesEngine;

	@Reference
	private ProcesscontinuityResource processContinuity;

	/**
	 * Method called whenever the component is activated.
	 *
	 * @param ctx    Component context
	 * @param config Component configuration
	 */
	@Activate
	public void activate(ComponentContext ctx, CoreAgentConfig config) {
		prepare(ctx, config);
	}

	/**
	 * Method called whenever the component configuration changes.
	 *
	 * @param config Component configuration
	 */
	@Modified
	public void modified(CoreAgentConfig config) {
		prepare(config);
	}

	// UAM related methods
	public boolean userExists(PrincipalDTO principal) {
		UserDTO user = uam.findUsers(new UserSearchDTOBuilder().withName(principal.accountName).build()).stream()
				.findFirst().orElse(null);

		return (user != null);
	}

	public UserDTO findUser(String accountName) {
		UserDTO user = uam.findUsers(new UserSearchDTOBuilder().withName(accountName).build()).stream().findFirst()
				.orElse(null);

		return user;
	}

    public UserDTO createUser(PrincipalDTO principal) {

	String firstName = "";
	String lastName = "";

	if (StringUtils.isBlank(principal.name)) {
	    firstName = principal.accountName;
	} else {
	    int spaceIdx = principal.name.indexOf(" ");
	    if (spaceIdx != -1) {
		firstName = principal.name.substring(0, spaceIdx).trim();
		lastName =  principal.name.substring(spaceIdx).trim();
	    } else {
		firstName = principal.name;
	    }
	}

	//@formatter:off
	return uam.createUser(
			new SaveUserDTOBuilder()
				.withName(principal.accountName)
				.withFirstName(firstName)
				.withLastName(lastName)
				.withStatus(UserStatusType.ENABLED)
				.withEmail(principal.mail)
				.withAuthenticatorName(LDAP_AUTHENTICATOR)
				.build());
	//@formatter:on
    }

	public void createUser(String username, String password, String name, String surname, String email,
			String... roleNames) {
		UserDTO user = uam.findUsers(new UserSearchDTOBuilder().withName(username).build()).stream().findFirst()
				.orElse(null);

		if (user == null) {
			user = uam.createUser(new SaveUserDTOBuilder().withName(username).withPassword(password).withFirstName(name)
					.withLastName(surname).withEmail(email).withStatus(UserStatusType.ENABLED).build());
		}

		List<RoleDTO> roles = uam.findUserRoles(user.id, null, null);
		if (roleNames != null) {
			for (String roleName : roleNames) {
				RoleDTO role = roleExists(roleName);
				if (role != null && roles.stream().noneMatch(r -> r.id == role.id)) {
					uam.addRoleToUser(user.id, role.id);
				}
			}
		}
	}

	public void addRolesToUser(UserDTO uamUser, List<RoleDTO> roles) {
		if (roles != null) {
			// Find existing roles associated with user
			List<RoleDTO> existingRoles = uam.findUserRoles(uamUser.id, null, null);
			// For each new roles
			roles.forEach(role -> {
				// Check if already set
				if (existingRoles.stream().noneMatch(r -> r.id == role.id)) {
					uam.addRoleToUser(uamUser.id, role.id);
				}
			});
		}
	}

	public List<RoleDTO> findDefaultRoles() {
		RoleSearchDTO search = new RoleSearchDTOBuilder().withName(getConfig().defaultRole()).build();

		return uam.findRoles(search);
	}

	public RoleDTO roleExists(String roleName) {
		return uam.findRoles(new RoleSearchDTOBuilder().withName(roleName).build()).stream().findFirst().orElse(null);
	}

	public void createRole(String roleName, String roleDescription, String... permissionNames) {

		RoleDTO role = roleExists(roleName);

		if (role == null) {
			role = uam.createRole(new SaveRoleDTOBuilder().withName(roleName).withDescription(roleDescription).build());
		}

		updateRolePermissions(role, permissionNames);
	}

	private void updateRolePermissions(RoleDTO role, String... permissionNames) {

		// TODO: consider removing old permissions not in the current permissionName
		// list.

		if (permissionNames != null) {
			List<PermissionDTO> currentPermissions = uam.findRolePermissions(role.id, null, null);
			Arrays.asList(permissionNames).forEach(name -> {
				// Permission doesn't exist yet.
				if (currentPermissions.stream().filter(p -> p.name.equals(name)).findFirst().orElse(null) == null) {
					PermissionDTO permission = permissionExists(name);
					if (permission != null) {
						uam.addPermissionToRole(role.id, permission.id);
					}
				}
			});
		}
	}

	private PermissionDTO permissionExists(String permissionName) {
		return uam.findPermissions(new PermissionSearchDTOBuilder().withName(permissionName).build()).stream()
				.findFirst().orElse(null);
	}

	// RuleEngine related methods

	//TODO: receber o user logado e passar no lugar do admin
	//TODO: meter o "system" (channel) no ficheiro de config para se poder alterar em runtime
	public List<GenericProcessDTO> getWorkQueue(String expression) {
		return WorklistMapper.INSTANCE
				.toGenericProcessDTOList(processContinuity.searchProcesses(expression, "system", "admin"));
	}
}
