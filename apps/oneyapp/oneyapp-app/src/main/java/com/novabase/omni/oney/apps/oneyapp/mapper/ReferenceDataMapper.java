package com.novabase.omni.oney.apps.oneyapp.mapper;

import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.apps.oneyapp.dto.SearchFilterDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.SupplierDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.SupplierGeneralDataDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.AssetDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.CostCenterDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.DepartmentDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.GroupDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.InternalOrderDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.ItemDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.ItemTypeDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.ProjectDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.ReferenceDataDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.RetentionCodeDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.TaxDTO;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.ReferenceDataEnum;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.AssetMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.CostCenterMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.DepartmentMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.InternalOrderMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ItemMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ItemTypeMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ProjectMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.RetentionCodeMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SearchFilterMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierGeneralDataMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.TaxMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyNodeMsDTO;

@Mapper
public interface ReferenceDataMapper {

    public static final ReferenceDataMapper INSTANCE = Mappers.getMapper(ReferenceDataMapper.class);

    public AssetDTO toAssetDTO(final AssetMsDTO assetMs);

    @IterableMapping(elementTargetType = AssetDTO.class)
    public List<AssetDTO> toAssetDTOList(final List<AssetMsDTO> assetMsList);

    public CostCenterDTO toCostCenterDTO(final CostCenterMsDTO costCenterMs);

    @IterableMapping(elementTargetType = CostCenterDTO.class)
    public List<CostCenterDTO> toCostCenterDTOList(final List<CostCenterMsDTO> costCenterMsList);

    public InternalOrderDTO toInternalOrderDTO(final InternalOrderMsDTO internalOrderMs);

    @IterableMapping(elementTargetType = InternalOrderDTO.class)
    public List<InternalOrderDTO> toInternalOrderDTOList(final List<InternalOrderMsDTO> internalOrderMsList);

    public ItemDTO toItemDTO(final ItemMsDTO itemMs);

    @IterableMapping(elementTargetType = ItemDTO.class)
    public List<ItemDTO> toItemDTOList(final List<ItemMsDTO> itemMsList);

    public ItemTypeDTO toItemTypeDTO(final ItemTypeMsDTO itemTypeMs);

    @IterableMapping(elementTargetType = ItemTypeDTO.class)
    public List<ItemTypeDTO> toItemTypeDTOList(final List<ItemTypeMsDTO> itemTypeMsList);

    public ProjectDTO toProjectDTO(final ProjectMsDTO projectMs);

    @IterableMapping(elementTargetType = ProjectDTO.class)
    public List<ProjectDTO> toProjectDTOList(final List<ProjectMsDTO> projectMsList);

    public RetentionCodeDTO toRetentionCodeDTO(final RetentionCodeMsDTO retentionCodeMs);

    @IterableMapping(elementTargetType = RetentionCodeDTO.class)
    public List<RetentionCodeDTO> toRetentionCodeDTOList(final List<RetentionCodeMsDTO> retentionCodeMsList);

    public TaxDTO toTaxDTO(final TaxMsDTO taxMs);

    @IterableMapping(elementTargetType = TaxDTO.class)
    public List<TaxDTO> toTaxDTOList(final List<TaxMsDTO> taxMsList);

    @Mapping(expression = "java(stringParam)", target = "code")
    @Mapping(expression = "java(stringParam)", target = "description")
    @Mapping(target = "active", expression = "java(true)")
    public ReferenceDataDTO toReferenceDTO(final String stringParam);

    @IterableMapping(elementTargetType = ReferenceDataDTO.class)
    public List<ReferenceDataDTO> toReferenceDTOList(final List<String> stringParamList);

    @Mapping(source = "description", target = "name")
    public SupplierDTO toSupplierDTO(final SupplierMsDTO obj);

    @Mapping(source = "code", target = "id")
    public SearchFilterMsDTO toSearchFilter(final SearchFilterDTO filter);

    @Mapping(source = "description", target = "name")
    public SupplierGeneralDataDTO toSupplierGeneralDataDTO(final SupplierGeneralDataMsDTO obj);

    @Mapping(target = "active", expression = "java(true)")
    ReferenceDataDTO toReferenceDTOFromEnum(ReferenceDataEnum referenceDataEnum);
    
    public List<ReferenceDataDTO> toReferenceDTOListFromEnum(final List<ReferenceDataEnum> referenceDataEnum);

    public DepartmentDTO toDepartmnetDTO(final DepartmentMsDTO department);
 
    @IterableMapping(elementTargetType = DepartmentDTO.class)
    public List<DepartmentDTO> toDepartmentDTOList(final List<DepartmentMsDTO> departments);
    
    @Mapping(expression = "java(node.getGroup())", target = "code")
    @Mapping(expression = "java(node.getDepartment())", target = "departmentCode")
    @Mapping(expression = "java(true)", target = "active")
    @Mapping(expression = "java(String.format(\"%s - %s\", node.getGroup(), node.getDescription()))", target = "description")
    public GroupDTO toGroupDTO(final HierarchyNodeMsDTO node);

    @IterableMapping(elementTargetType = GroupDTO.class)
    public List<GroupDTO> toGroupDTOList(final List<HierarchyNodeMsDTO> nodes);
    
}
