package com.novabase.omni.oney.apps.oneyapp.manager.worklist;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.novabase.omni.oney.modules.oneycommon.service.api.dto.worklist.SearchCriteriaWorklistDTO;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.FilterAttribute;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.JourneyType;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.OrderType;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.TargetType;

public class WorklistQueryBuilder {

    private static final String IN_CREATION_STATUS = "'In Creation'";
    private static final String LIMIT_100 = " LIMIT 100";
    private static final String DRAFT_WITHOUT_COTE = "Draft";
    private static final String DRAFT_STATUS = "'Draft'";
    private static final String REJECTED_STATUS = "'Rejected'";
    private static final String IN_APPROVAL_STATUS = "'In Approval'";
    private static final String FORMAT_ORIENTDB_DATE = ".format('yyyy-MM-dd')";
    private static final String DATE_FORMAT_MASK = "yyyy-MM-dd";
    private static final String SPACE = " ";
    private static final String OPEN_PARENTHESES = " (";
    private static final String CLOSE_PARENTHESES = ") ";
    private static final String SINGLE_QUOTE = "'";
    private static final String SELECT = "SELECT * FROM PROCESS ";
    private static final String WHERE = " WHERE ";
    private static final String AND = " AND ";
    private static final String OR = " OR ";
    private static final String COMMA = " , ";
    private static final String EQUALS = " = ";
    private static final String LESS_THAN_OR_EQUAL_TO = " <= ";
    private static final String GREATER_THAN_OR_EQUAL_TO = " >= ";
    private static final String ORDER_BY = " ORDER BY ";

    //@formatter:off
    public static String buildQueryFromSearchCriteriaWorklistDTO(
	    SearchCriteriaWorklistDTO criteriaWorklistDTO, 
	    FilterAttribute orderField, 
	    OrderType orderType, 
	    String userGroup, 
	    Long userId) {

	return new StringBuilder()
			.append(SELECT)
			.append(buildWhereClauses(criteriaWorklistDTO, userGroup, userId))
			.append(sortResults(orderField, orderType))
			.append(LIMIT_100)
			.toString();
    }
    //@formatter:on

    private static Object sortResults(FilterAttribute orderField, OrderType orderType) {
	String orderClause = ORDER_BY;
	if (orderField != null) {
	    orderClause += SPACE + orderField.getQueryPath();
	} else {
	    return orderClause + FilterAttribute.EXPIRATION_DATE.getQueryPath() + SPACE + OrderType.ASC.name();
	}

	if (orderType != null) {
	    orderClause += (SPACE + orderType.name());
	} else {
	    orderClause += (SPACE + OrderType.ASC.name());
	}

	return orderClause;
    }

    private static String buildWhereClauses(SearchCriteriaWorklistDTO criteriaWorklistDTO, String userGroup, Long userId) {
	List<String> whereClauses = new ArrayList<>();
	whereClauses.add(filterByInApprovalOrCurrentUserOwner(userId));
	whereClauses.add(filterByJourneyType(criteriaWorklistDTO.journeyType));
	whereClauses.add(filterByNumber(criteriaWorklistDTO.number, criteriaWorklistDTO.journeyType));
	whereClauses.add(filterByEntityCode(criteriaWorklistDTO.entityCode));
	whereClauses.add(filterByMinTotalValue(criteriaWorklistDTO.minValue));
	whereClauses.add(filterByMaxTotalValue(criteriaWorklistDTO.maxValue));
	whereClauses.add(filterByStatus(criteriaWorklistDTO.status));
	whereClauses.add(filterByCreationDate(criteriaWorklistDTO.creationDate));
	whereClauses.add(filterByExpirationDate(criteriaWorklistDTO.expirationDate));
	whereClauses.add(filterByDepartmentCode(criteriaWorklistDTO.departmentCode));
	whereClauses.add(filterByCurrentUserGroup(userGroup));

	//@formatter:off
	String whereClause = 
		whereClauses.stream()
			.filter(clause -> StringUtils.isNotBlank(clause))
			.collect(Collectors.joining(AND));
	//@formatter:on

	return WHERE + whereClause;
    }

    //@formatter:off
    private static String filterByInApprovalOrCurrentUserOwner(Long userId) {
	return 
		new StringBuilder()
			.append(OPEN_PARENTHESES)
				.append("status = ")
				.append(IN_APPROVAL_STATUS)
				.append(OR)
				.append("status = ")
				.append(IN_CREATION_STATUS)
				.append(OR)
				.append(OPEN_PARENTHESES)
					.append("status in [")
					.append(DRAFT_STATUS)
					.append(COMMA)
					.append(REJECTED_STATUS)
					.append("]")
					.append(AND)
					.append("targets.CREATION_USER_ID.value = ")
					.append(SINGLE_QUOTE)
					.append(userId)
					.append(SINGLE_QUOTE)
				.append(CLOSE_PARENTHESES)
			.append(CLOSE_PARENTHESES)
			.toString();
    }
    //@formatter:on

    private static String filterByCurrentUserGroup(String userGroup) {
	if (StringUtils.isNotBlank(userGroup)) {
	    //@formatter:off
	    return 
		    new StringBuilder()
		    	.append("targets.CURRENT_GROUP.value")
		    	.append(EQUALS)
		    	.append(SINGLE_QUOTE)
		    	.append(userGroup)
		    	.append(SINGLE_QUOTE)
		    	.toString();
		//@formatter:on
	}
	return "";
    }

    private static String filterByDepartmentCode(String departmentCode) {
	if (StringUtils.isNotBlank(departmentCode)) {
	    //@formatter:off
	    return 
		    new StringBuilder()
		    	.append(FilterAttribute.DEPARTMENT_CODE.getQueryPath())
		    	.append(EQUALS)
		    	.append(SINGLE_QUOTE)
		    	.append(departmentCode)
		    	.append(SINGLE_QUOTE)
		    	.toString();
		//@formatter:on
	}
	return "";
    }

    private static String filterByExpirationDate(Date expirationDate) {
	if (expirationDate != null) {
	    //@formatter:off
	    return 
		    new StringBuilder()
		    	.append(FilterAttribute.EXPIRATION_DATE.getQueryPath())
		    	.append(EQUALS)
		    	.append(SINGLE_QUOTE)
		    	.append(new SimpleDateFormat(DATE_FORMAT_MASK).format(expirationDate))
		    	.append(SINGLE_QUOTE)
		    	.toString();
		//@formatter:on
	}
	return "";
    }

    private static String filterByCreationDate(Date creationDate) {
	if (creationDate != null) {
	    //@formatter:off
	    return 
		    new StringBuilder()
		    	.append(FilterAttribute.CREATION_DATE.getQueryPath())
		    	.append(FORMAT_ORIENTDB_DATE)
		    	.append(EQUALS)
		    	.append(SINGLE_QUOTE)
		    	.append(new SimpleDateFormat(DATE_FORMAT_MASK).format(creationDate))
		    	.append(SINGLE_QUOTE)
		    	.toString();
		//@formatter:on
	}
	return "";
    }

    private static String filterByStatus(String status) {
	if (StringUtils.isNotBlank(status)) {

	    if (DRAFT_WITHOUT_COTE.equalsIgnoreCase(status)) { // in order to retrieve all process in Draft or In Creation, we must filter by
							       // this two status when filter by Draft
		//@formatter:off
		    return 
			    new StringBuilder()
			     	.append(OPEN_PARENTHESES)
			    	.append(FilterAttribute.STATUS.getQueryPath())
			    	.append(EQUALS)
			    	.append(SINGLE_QUOTE)
			    	.append(status)
			    	.append(SINGLE_QUOTE)
			    	.append(OR)
			    	.append(FilterAttribute.STATUS.getQueryPath())
			    	.append(EQUALS)
			    	.append(IN_CREATION_STATUS)
			    	.append(CLOSE_PARENTHESES)
			    	.toString();
			//@formatter:on
	    }

	    //@formatter:off
	    return 
		    new StringBuilder()
		    	.append(FilterAttribute.STATUS.getQueryPath())
		    	.append(EQUALS)
		    	.append(SINGLE_QUOTE)
		    	.append(status)
		    	.append(SINGLE_QUOTE)
		    	.toString();
		//@formatter:on
	}
	return "";
    }

    private static String filterByMinTotalValue(Double minValue) {
	if (minValue != null) {
	    //@formatter:off
	    return 
		    new StringBuilder()
		    	.append(FilterAttribute.VALUE.getQueryPath())
		    	.append(GREATER_THAN_OR_EQUAL_TO)
		 	.append(TargetType.formatValue(minValue))
		    	.toString();
		//@formatter:on
	}
	return "";
    }

    private static String filterByMaxTotalValue(Double maxValue) {
	if (maxValue != null) {
	    //@formatter:off
	    return 
		    new StringBuilder()
		    	.append(FilterAttribute.VALUE.getQueryPath())
		    	.append(LESS_THAN_OR_EQUAL_TO)
		    	.append(TargetType.formatValue(maxValue))
		    	.toString();
		//@formatter:on
	}
	return "";
    }

    private static String filterByEntityCode(String entityCode) {
	if (StringUtils.isNotBlank(entityCode)) {
	    //@formatter:off
	    return 
		    new StringBuilder()
		    	.append(FilterAttribute.ENTITY_CODE.getQueryPath())
		    	.append(EQUALS)
		    	.append(SINGLE_QUOTE)
		    	.append(entityCode)
		    	.append(SINGLE_QUOTE)
		    	.toString();
		//@formatter:on
	}
	return "";
    }

    private static String filterByJourneyType(JourneyType journeyType) {
	if (journeyType != null) {
	    //@formatter:off
	    return 
		    new StringBuilder()
		    	.append(FilterAttribute.JOURNEY_TYPE.getQueryPath())
		    	.append(EQUALS)
		    	.append(SINGLE_QUOTE)
		    	.append(journeyType.name())
		    	.append(SINGLE_QUOTE)
		    	.toString();
	    //@formatter:on
	}
	return "";
    }

    private static String filterByNumber(String number, JourneyType journeyType) {
	
	if (number != null) {
	  //@formatter:off
	    return 
		    new StringBuilder()
		    	.append(FilterAttribute.NUMBER.getQueryPath())
		    	.append(EQUALS)
		    	.append(SINGLE_QUOTE)
		    	.append( (JourneyType.PURCHASE_ORDER.equals(journeyType) ?  TargetType.formatNumber(number) : number))
		    	.append(SINGLE_QUOTE)
		    	.toString();
	    //@formatter:on
	}
	return "";
    }

}
