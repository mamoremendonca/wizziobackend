package com.novabase.omni.oney.apps.oneyapp.dto.reference;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("RetentionCodeDTO")
public class RetentionCodeDTO extends ReferenceDataDTO {

    @ApiModelProperty(value = "TaxPercentage")
    public String taxPercentage;
}
