package com.novabase.omni.oney.apps.oneyapp.dto.reference;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("TaxDTO")
public class TaxDTO extends ReferenceDataDTO {

    @ApiModelProperty(value = "TaxPercentage")
    public String taxPercentage;

    @ApiModelProperty(value = "Market")
    public String marketCode;

}
