package com.novabase.omni.oney.apps.oneyapp.dto.reference;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("ItemDTO")
public class ItemDTO extends ReferenceDataDTO {

	@ApiModelProperty(value = "Department")
	public String departmentCode;

	@ApiModelProperty(value = "CostCenter")
	public String defaultCostCenterCode;

	@ApiModelProperty(value = "Tax")
	public String defaultTaxCode;

	@ApiModelProperty(value = "Type")
	public String itemTypeCode;
}
