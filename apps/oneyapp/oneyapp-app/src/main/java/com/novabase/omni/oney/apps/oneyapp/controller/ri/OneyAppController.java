/*-
 * #%L
 * Apps :: Oney Boot App App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.oneyapp.controller.ri;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.eclipse.gemini.blueprint.extensions.annotation.ServiceReference;
import org.osgi.service.component.annotations.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.novabase.omni.oney.apps.oneyapp.AppProperties;
import com.novabase.omni.oney.apps.oneyapp.controller.api.OneyAppResource;
import com.novabase.omni.oney.apps.oneyapp.dto.GenericProcessDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.GroupDTO;
import com.novabase.omni.oney.apps.oneyapp.manager.CoreManager;
import com.novabase.omni.oney.apps.oneyapp.manager.DirectoryServiceManager;
import com.novabase.omni.oney.apps.oneyapp.manager.worklist.WorklistManager;
import com.novabase.omni.oney.modules.oneycommon.service.api.dto.worklist.SearchCriteriaWorklistDTO;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.FilterAttribute;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.OrderType;

import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;

//@formatter:off
/**
 * Base Oney Boot App Resource, this is only used for documentation purposes. *
 */
@Component
@RSProvider(value = AppProperties.ADDRESS)
@RestController
@RequestMapping(AppProperties.ADDRESS)
//@formatter:on
public class OneyAppController extends AbstractAppController implements OneyAppResource {

	@ServiceReference
	private DirectoryServiceManager directoryServiceManager;

	@ServiceReference
	private CoreManager coreManager;

	@ServiceReference
	private WorklistManager worklistManager;

	@Override
	@RequestMapping(path = AppProperties.ADDRESS_INIT, method = RequestMethod.GET)
	@RequiresPermissions(AppProperties.PERMISSION_EXECUTE)
	public void init() {

		coreManager.createRoles();

		coreManager.createRules();

		// Update admin with Role JD 
		coreManager.updateAdminRole();

		// Retrieves users from AD and inserts into UAM with basic role level
		// (configured)
		coreManager.importUsers();
	}

	@Override
	@RequestMapping(path = AppProperties.ADDRESS_MEMBERSHIPS, method = RequestMethod.GET)
	@RequiresPermissions(AppProperties.PERMISSION_READ)
	public List<GroupDTO> getMemberships() {
		return this.directoryServiceManager.getUserGroups(this.currentUser());
	}

	@Override
	@RequestMapping(path = AppProperties.ADDRESS_GROUP, method = RequestMethod.POST)
	@RequiresPermissions(AppProperties.PERMISSION_UPDATE)
	public void setUserGroup(@RequestBody GroupDTO group) {

		directoryServiceManager.setUserGroup(currentUser(), group);
	}

	@Override
	@RequestMapping(path = AppProperties.ADDRESS_GROUP, method = RequestMethod.GET)
	@RequiresPermissions(AppProperties.PERMISSION_READ)
	public GroupDTO getUserGroup() {
		return new GroupDTO(directoryServiceManager.getUserGroup(currentUser()), "");
	}

	// @formatter:off
	@Override
	@RequestMapping(path = AppProperties.ADDRESS_WORKLIST, method = RequestMethod.POST)
	@RequiresPermissions(AppProperties.PERMISSION_READ)
	public List<GenericProcessDTO> getWorklist(@RequestBody SearchCriteriaWorklistDTO searchCriteriaDTO,
			@RequestParam(required = false) FilterAttribute orderField,
			@RequestParam(required = false) OrderType orderType) {

		String userGroup = directoryServiceManager.getUserGroup(currentUser());

		return worklistManager.getWorklist(searchCriteriaDTO, orderField, orderType, userGroup, currentLoggedUserId());
	}
	// @formatter:on
}
