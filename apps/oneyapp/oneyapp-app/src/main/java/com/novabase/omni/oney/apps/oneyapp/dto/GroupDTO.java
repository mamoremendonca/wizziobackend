package com.novabase.omni.oney.apps.oneyapp.dto;

import java.io.Serializable;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class GroupDTO extends DTO implements Serializable {

    private static final long serialVersionUID = -1668414748064146951L;

    @ApiModelProperty
    public String name; //TODO: mudar para codigo : avisar o frontend
    
    @ApiModelProperty
    public String description;
    
    public GroupDTO() {
	
	this.name = "";
	this.description = "";
	
    };
    
    public GroupDTO(final String name, final String description) {
	
	this.name = name;
	this.description = description;
	
    }
    
}
