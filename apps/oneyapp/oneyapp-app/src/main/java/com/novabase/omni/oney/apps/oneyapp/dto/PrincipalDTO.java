package com.novabase.omni.oney.apps.oneyapp.dto;

import java.io.Serializable;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class PrincipalDTO extends DTO implements Serializable {

    private static final long serialVersionUID = -1668414748064146951L;

    @ApiModelProperty(value = "Display Name")
    public String name;

    @ApiModelProperty(value = "Username")
    public String accountName;
    
    @ApiModelProperty(value = "Mail")
    public String mail;
    
    public PrincipalDTO() {
	
	this.name = "";
	this.accountName = "";
	this.mail = "";
	
    };
    
    public PrincipalDTO(final String name, final String accountName, final String mail) {
	
	this.name = name;
	this.accountName = accountName;
	this.mail = mail;
	
    }
    
}
