package com.novabase.omni.oney.apps.oneyapp.dto.reference;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("ItemTypeDTO")
public class ItemTypeDTO extends ReferenceDataDTO {

	@ApiModelProperty(value = "Department")
	public String departmentCode;
}
