package com.novabase.omni.oney.apps.oneyapp.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.apps.oneyapp.dto.GroupDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.PrincipalDTO;

@Mapper
public interface DirectoryServiceMapper {

    public static final DirectoryServiceMapper INSTANCE = Mappers.getMapper(DirectoryServiceMapper.class);

    public List<GroupDTO> toListGroupDTO(List<com.novabase.omni.oney.modules.directoryservice.service.api.dto.GroupDTO> list);
    public GroupDTO toGroupDTO(com.novabase.omni.oney.modules.directoryservice.service.api.dto.GroupDTO obj);

    public List<PrincipalDTO> toListPrincipalDTO(List<com.novabase.omni.oney.modules.directoryservice.service.api.dto.PrincipalDTO> list);
    public PrincipalDTO toPrincipalDTO(com.novabase.omni.oney.modules.directoryservice.service.api.dto.PrincipalDTO obj);
}
