package com.novabase.omni.oney.apps.oneyapp.dto.reference;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("ItemDTO")
public class AssetDTO extends ReferenceDataDTO {

//	@ApiModelProperty(value = "Department")
//	public String departmentCode;

	@ApiModelProperty(value = "Tax")
	public String defaultTaxCode;
}
