package com.novabase.omni.oney.apps.oneyapp.dto.reference;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("DocumentTypeDTO")
public class DocumentTypeDTO extends ReferenceDataDTO {

	@ApiModelProperty(value = "Type")
	public String type;
}
