package com.novabase.omni.oney.apps.oneyapp.mapper;

import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.apps.oneyapp.dto.GenericProcessDTO;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.TargetType;

import io.digitaljourney.platform.plugins.modules.processcontinuity.service.api.dto.ProcessDTO;
import io.digitaljourney.platform.plugins.modules.processcontinuity.service.api.dto.header.TargetEntryDTO;

@Mapper
public abstract class WorklistMapper {

    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final WorklistMapper INSTANCE = Mappers.getMapper(WorklistMapper.class);

    public abstract List<GenericProcessDTO> toGenericProcessDTOList(List<ProcessDTO> processDTOList);

    public GenericProcessDTO toGenericProcessDTO(ProcessDTO process) {
	GenericProcessDTO genericProcess = new GenericProcessDTO();
	
	genericProcess.creationDate = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT).format(process.header.createdOn);
	genericProcess.expirationDate = toLocalDateTimeString(process.header.targets.get(TargetType.EXPIRATION_DATE.name()));
	genericProcess.entityCode = toString(process.header.targets.get(TargetType.ENTITY_CODE.name()));
	genericProcess.departmentCode = toString(process.header.targets.get(TargetType.DEPARTMENT_CODE.name()));
	genericProcess.value = toDouble(process.header.targets.get(TargetType.VALUE.name()));
	genericProcess.journeyType = process.header.processType;
	genericProcess.status = process.header.status;
	genericProcess.instanceId = process.header.instanceId;
	genericProcess.friendlyNumber = toString(process.header.targets.get(TargetType.FRIENDLY_NUMBER.name()));
	genericProcess.number = toString(process.header.targets.get(TargetType.NUMBER.name()));
	
	return genericProcess;
    }
    
    /**
     * Concat "T12:00:00.000Z" at the end of the String
     * 
     * @param targetEntryDTO
     * @return String 
     */
    private String toLocalDateTimeString(TargetEntryDTO targetEntryDTO) {
	if(targetEntryDTO != null && StringUtils.isNotBlank(targetEntryDTO.value)) {
	    return targetEntryDTO.value + "T12:00:00.000Z";
	}
	return null;
    }
    
    /**
     * Null safe to String value from TargetEntryDTO
     * 
     * @param targetEntryDTO
     * @return LocalDateTime 
     */
    private String toString(TargetEntryDTO targetEntryDTO) {
	if(targetEntryDTO != null) {
	    return targetEntryDTO.value;
	}
	return null;
    }
    
    /**
     * Null safe to Double value from TargetEntryDTO
     * 
     * @param targetEntryDTO
     * @return LocalDateTime 
     */
    private Double toDouble(TargetEntryDTO targetEntryDTO) {
	if(targetEntryDTO != null) {
	    try {
		return Double.valueOf(targetEntryDTO.value);
	    } catch (Exception e) {
	    }
	}
	return null;
    }
    
    /**
     * Null safe to Long value from TargetEntryDTO
     * 
     * @param targetEntryDTO
     * @return LocalDateTime 
     */
    private Long toLong(TargetEntryDTO targetEntryDTO) {
	if(targetEntryDTO != null) {
	    try {
		return Long.valueOf(targetEntryDTO.value);
	    } catch (Exception e) {
	    }
	}
	return null;
    }

}
