package com.novabase.omni.oney.apps.oneyapp.providers;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.novabase.omni.oney.apps.oneyapp.agent.reference.ReferenceDataAgentImpl;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.AssetDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.CostCenterDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.DepartmentDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.GroupDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.InternalOrderDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.ItemDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.ItemTypeDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.ProjectDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.ReferenceDataDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.RetentionCodeDTO;
import com.novabase.omni.oney.apps.oneyapp.dto.reference.TaxDTO;
import com.novabase.omni.oney.apps.oneyapp.enums.DocumentClassType;
import com.novabase.omni.oney.apps.oneyapp.mapper.ReferenceDataMapper;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.purchaseorder.PurchaseOrderWorkListStatus;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.FilterAttribute;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.JourneyType;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.OrderType;

public class ReferenceDataProvider {

    private ReferenceDataAgentImpl referenceAgent;
    
    public ReferenceDataProvider(final ReferenceDataAgentImpl referenceAgent) {
	this.referenceAgent = referenceAgent;
    }
    
    public List<ItemDTO> getItems() {
	return ReferenceDataMapper.INSTANCE.toItemDTOList(this.referenceAgent.getItems());
    }

    public List<ItemTypeDTO> getItemTypes() {
	return ReferenceDataMapper.INSTANCE.toItemTypeDTOList(this.referenceAgent.getItemTypes());
    }

    public List<TaxDTO> getTaxes() {
	return ReferenceDataMapper.INSTANCE.toTaxDTOList(this.referenceAgent.getTaxes());
    }

    public List<CostCenterDTO> getCostCenters() {
	return ReferenceDataMapper.INSTANCE.toCostCenterDTOList(this.referenceAgent.getCostCenters());
    }

    public List<ProjectDTO> getProjects() {
	return ReferenceDataMapper.INSTANCE.toProjectDTOList(this.referenceAgent.getProjects());
    }

    public List<InternalOrderDTO> getInternalOrdens() {
	return ReferenceDataMapper.INSTANCE.toInternalOrderDTOList(this.referenceAgent.getInternalOrdens());
    }

    public List<RetentionCodeDTO> getRetentionCode() {
	return ReferenceDataMapper.INSTANCE.toRetentionCodeDTOList(this.referenceAgent.getRetentionCode());
    }

    public List<AssetDTO> getAssets() {
	return ReferenceDataMapper.INSTANCE.toAssetDTOList(this.referenceAgent.getAssets());
    }

    public List<ReferenceDataDTO> getDocumentTypes() {
	
	return ReferenceDataMapper.INSTANCE.toReferenceDTOList(Arrays.asList(DocumentClassType.values()).parallelStream().
                                                                                                		map(enumParam -> enumParam.name()).
                                                                                                		collect(Collectors.toList()));
	
    }
    
    public List<ReferenceDataDTO> getWorkListFilterColumns() {
	
	return ReferenceDataMapper.INSTANCE.toReferenceDTOList(Arrays.asList(FilterAttribute.values()).parallelStream().
                                                                                                		map(enumParam -> enumParam.name()).
                                                                                                		collect(Collectors.toList()));
	
    }
    
    public List<ReferenceDataDTO> getPurchaseOrderWorkListStatus() {
	
	return ReferenceDataMapper.INSTANCE.toReferenceDTOListFromEnum(Arrays.asList(PurchaseOrderWorkListStatus.values()).parallelStream().
                                                                                                        		map(enumParam -> enumParam).
                                                                                                        		collect(Collectors.toList()));
	
    }
    
    public List<ReferenceDataDTO> getJourneyType() {
	
	return ReferenceDataMapper.INSTANCE.toReferenceDTOListFromEnum(Arrays.asList(JourneyType.values()).parallelStream().
                                                                                        		map(enumParam -> enumParam).
                                                                                        		collect(Collectors.toList()));
	
    }
    
    public List<ReferenceDataDTO> getOrderType() {
	
	return ReferenceDataMapper.INSTANCE.toReferenceDTOList(Arrays.asList(OrderType.values()).parallelStream().
                                                                                        		map(enumParam -> enumParam.name()).
                                                                                        		collect(Collectors.toList()));
	
    }
    
    public List<DepartmentDTO> getDepartments() {
	return ReferenceDataMapper.INSTANCE.toDepartmentDTOList(this.referenceAgent.getDepartments());
    }
    
    public List<GroupDTO> getAllGroups() {
	return ReferenceDataMapper.INSTANCE.toGroupDTOList(this.referenceAgent.getAllGroups());
    }

}
