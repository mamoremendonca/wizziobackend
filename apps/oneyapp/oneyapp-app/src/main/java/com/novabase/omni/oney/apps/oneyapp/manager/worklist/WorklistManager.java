package com.novabase.omni.oney.apps.oneyapp.manager.worklist;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.oneyapp.AppContext;
import com.novabase.omni.oney.apps.oneyapp.agent.core.CoreAgentImpl;
import com.novabase.omni.oney.apps.oneyapp.dto.GenericProcessDTO;
import com.novabase.omni.oney.modules.oneycommon.service.api.dto.worklist.SearchCriteriaWorklistDTO;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.FilterAttribute;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.OrderType;

@Component(service = { WorklistManager.class })
public class WorklistManager {

    @Reference
    private AppContext ctx;

    @Reference
    private CoreAgentImpl coreAgent;

    public List<GenericProcessDTO> getWorklist(SearchCriteriaWorklistDTO criteriaWorklistDTO, FilterAttribute orderField, OrderType orderType, String userGroup, Long userId) {

	//@formatter:off
	String query = WorklistQueryBuilder
				.buildQueryFromSearchCriteriaWorklistDTO(
					criteriaWorklistDTO, 
					orderField, 
					orderType, 
					userGroup, 
					userId);
	//@formatter:on

	return coreAgent.getWorkQueue(query);
    }

}
