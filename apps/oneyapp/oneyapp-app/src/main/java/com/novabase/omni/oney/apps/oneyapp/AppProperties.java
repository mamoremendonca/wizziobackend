/*-
 * #%L
 * Apps :: Oney Boot App App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.oneyapp;

import io.digitaljourney.platform.modules.mvc.api.MVCProperties;

/**
 * Oney Boot App App properties
 */
public final class AppProperties extends MVCProperties {
    private AppProperties() {
    }

    public static final String APP_NAME = "oneyapp";
    public static final String CURRENT_VERSION = "1";
    public static final String APP_VERSION = "/v" + CURRENT_VERSION;
    public static final String ADDRESS = "/" + APP_NAME + APP_VERSION;

    // For Controllers (publish in Core)    
    public static final String ADDRESS_REFERENCE = ADDRESS + "/reference";
    public static final String ADDRESS_INIT = "/init";
    public static final String ADDRESS_WORKLIST = "/worklist";
    public static final String ADDRESS_USER = "/user";
    public static final String ADDRESS_MEMBERSHIPS = ADDRESS_USER + "/memberships";
    public static final String ADDRESS_GROUP = ADDRESS_USER + "/group";

    public static final String SUBSERVICE_NAME = SUBSERVICE_PREFIX + APP_NAME;

    public static final String DOCS_ADDRESS = CMS_RESOURCE_PATTERN + ADDRESS + DOCS_PATH;

    public static final String PERMISSION_ALL = APP_NAME + ACTION_ALL;
    public static final String PERMISSION_CREATE = APP_NAME + ACTION_CREATE;
    public static final String PERMISSION_READ = APP_NAME + ACTION_READ;
    public static final String PERMISSION_UPDATE = APP_NAME + ACTION_UPDATE;
    public static final String PERMISSION_DELETE = APP_NAME + ACTION_DELETE;
    public static final String PERMISSION_EXECUTE = APP_NAME + ACTION_EXECUTE;

    /** General Exception error code */
    public static final String CACHE_REFERENCE_ASSETS = "Cache_Assets";
    public static final String CACHE_REFERENCE_COST_CENTERS = "Cache_CostCenters";
    public static final String CACHE_REFERENCE_DOCUMENT_TYPES = "Cache_DocumentTypes";
    public static final String CACHE_REFERENCE_INTERNAL_ORDERS = "Cache_InternalOrders";
    public static final String CACHE_REFERENCE_ITEMS = "Cache_Items";
    public static final String CACHE_REFERENCE_ITEM_TYPES = "Cache_ItemTypes";
    public static final String CACHE_REFERENCE_PROJECTS = "Cache_Projects";
    public static final String CACHE_REFERENCE_RETENTION_CODES = "Cache_RetentionCodes";
    public static final String CACHE_REFERENCE_SUPPLIERS = "Cache_Suppliers";
    public static final String CACHE_REFERENCE_TAXES = "Cache_Taxes";
    public static final String CACHE_REFERENCE_SUPPLIERS_GANERALDATA = "Cache_Suppliers_GeneralData";
    public static final String CACHE_REFERENCE_DATA_DEPARTMENTS = "Cache_Departments";
    public static final String CACHE_GROUPS = "Cache_Groups";
    
    public static final String ONEYAPP000 = "ONEYAPP000";

    // Cache Buckets
    public static final String CACHE_USER_PROPERTIES = "user_properties";
    // Cache keys
    public static final Object CACHE_KEY_GROUP = "group";
}
