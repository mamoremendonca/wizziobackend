package com.novabase.omni.oney.apps.oneyapp.manager;

import java.util.Arrays;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.oneyapp.AppContext;
import com.novabase.omni.oney.apps.oneyapp.agent.core.CoreAgentImpl;
import com.novabase.omni.oney.apps.oneyapp.agent.directoryservice.DirectoryServiceAgentImpl;
import com.novabase.omni.oney.apps.oneyapp.dto.PrincipalDTO;

import io.digitaljourney.platform.plugins.modules.uam.service.api.dto.RoleDTO;
import io.digitaljourney.platform.plugins.modules.uam.service.api.dto.UserDTO;

@Component(service = { CoreManager.class })
public class CoreManager {

	@Reference
	private AppContext ctx;

	@Reference
	private CoreAgentImpl coreAgent;

	@Reference
	private DirectoryServiceAgentImpl directoryServiceAgent;

	public void createRoles() {

//	coreAgent.createRole("UFE_User", "Generic UFE User", "uam:read", "foundation:*");
//	coreAgent.createRole("Journey_User", "Journey Usage Requirements", "processcontinuity:*", "blob:*", "message:*", "rulesengine:read", "rulesengine:execute", "ocr:*", "oneyapp:read");
		// @formatter:off
		coreAgent.createRole("Colaborador", "Colaborador Oney", "oneyapp:*", "purchaseorder:*", "processcontinuity:*",
				"blob:*", "message:*", "rulesengine:*", "ocr:*", "directoryservice:*", "ecmservice:*", "oneycommon:*",
				"purchaseorderrepository:*", "financialdocumentrepository:*",  "referencedata:*", "sapservice:*", "workflow:*", "uam:read", "uam:create","financialdocument:*", "journeyblueprint:read");
		
		coreAgent.createRole("Digitalização", "Digitalização", "oneyapp:*", "purchaseorder:*", "processcontinuity:*",
			"blob:*", "message:*", "rulesengine:*", "ocr:*", "directoryservice:*", "ecmservice:*", "oneycommon:*",
			"purchaseorderrepository:*", "financialdocumentrepository:*", "referencedata:*", "sapservice:*", "workflow:*", "uam:read", "uam:create","financialdocument:*", "journeyblueprint:read");
		
		coreAgent.createRole("Integration", "User Integração", "uam:read", "uam:create","financialdocumentrepository:*", "financialdocumentintegration:integration");
		// @formatter:on
		// coreAgent.createRole("Oney_Manager", "Oney manager", "message:execute",
		// "oneyapp:*", "purchaseorder:*");
	}

	public void createRules() {
		// TODO: ....
	}

	public void importUsers() {

		// Loads initial users from BaseDN (AD) and creates in UAM (if not already
		// exists)
		List<PrincipalDTO> users = directoryServiceAgent.searchUsers();

		// Find default roles
		List<RoleDTO> roles = coreAgent.findDefaultRoles();

		users.forEach(user -> {

			// Check if already exists in UAM
			if (!coreAgent.userExists(user)) {

				// If not, create it
				UserDTO uamUser = coreAgent.createUser(user);

				// and add default role
				coreAgent.addRolesToUser(uamUser, roles);
			}
		});
	}

	public void updateAdminRole() {

		UserDTO user = coreAgent.findUser("admin");
		RoleDTO jdRole = coreAgent.roleExists("Journey_Designer");

		if (user != null && jdRole != null) {
			coreAgent.addRolesToUser(user, Arrays.asList(jdRole));
		}
	}

//	public void createUserColaborador() {
//		coreAgent.createUser("colaborador", "colaborador", "Colaborador", "Oney", "colaborador@wizzio-dev.oney.pt",
//				"Colaborador");
//	}
}
