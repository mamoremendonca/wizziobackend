package com.novabase.omni.oney.apps.oneyapp.manager;

import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.oneyapp.AppContext;
import com.novabase.omni.oney.apps.oneyapp.AppProperties;
import com.novabase.omni.oney.apps.oneyapp.agent.directoryservice.DirectoryServiceAgentImpl;
import com.novabase.omni.oney.apps.oneyapp.dto.GroupDTO;

import io.digitaljourney.platform.modules.cache.api.Cache;

@Component(service = { DirectoryServiceManager.class })
public class DirectoryServiceManager {

    private static final String REFRESH_TOKEN_CACHE_REF_NAME = "refreshTokenCache"; //TODO; validar se o nome tem que ser refreshtoken
    
    @Reference(name = REFRESH_TOKEN_CACHE_REF_NAME, target="(cacheName=" + AppProperties.CACHE_USER_PROPERTIES + ")")
    private volatile Cache cache;

    @Reference
    private AppContext ctx;

    @Reference
    private DirectoryServiceAgentImpl directoryServiceAgent;
    
    @Reference
    private WorkflowManager workflowManager;
    
    // Search for AD User membership (groups)
    public List<GroupDTO> getUserGroups(final String username) {
	
	final List<String> workflowGroupsList = this.workflowManager.getGroups();
	
	workflowGroupsList.add("ADMIN");
	
	return this.directoryServiceAgent.getGroups(username).stream().
								filter(group -> workflowGroupsList.contains(group.name)).
								collect(Collectors.toList());
	
    }

    public void setUserGroup(String username, GroupDTO group) {

	// ---------------------------------------------------------------------------------------------------
	// Store in Cache (HazelCast - CMS) => not accessible in Modules (specific Hazelcast instance in Core)
	// ---------------------------------------------------------------------------------------------------

	if (username == null)
	    throw ctx.invalidInputFieldException("username");
	if (group == null)
	    throw ctx.invalidInputFieldException("group");	
	
	// Retrieve property list (with username as cache(key))
	Properties props = getProperties(username);
	
	// If property (group) already set, remove it
	if (props.containsKey(AppProperties.CACHE_KEY_GROUP)) {
	    props.remove(AppProperties.CACHE_KEY_GROUP);
	}

	// Update selected group
	props.put(AppProperties.CACHE_KEY_GROUP, group.name);
	
	// Updates Cache with new props
	setProperties(username, props);
    }

    //public GroupDTO getUserGroup(String username) {
    public String getUserGroup(String username) {

	// Retrieve (if exists) User Properties
	Properties props = getProperties(username);
	
	// Returns Group if found
	return (props != null && !props.isEmpty() && props.containsKey(AppProperties.CACHE_KEY_GROUP)?
		(String)props.get(AppProperties.CACHE_KEY_GROUP) : "");
		//(GroupDTO)props.get(AppProperties.CACHE_KEY_GROUP) : null);
    }
    
    private Properties getProperties(String username) {
	Properties props = cache.get(username, Properties.class);
	return (props != null ? props : new Properties());
    }
    
    private void setProperties(String username, Properties props) {
	cache.put(username, props);
    }
}
