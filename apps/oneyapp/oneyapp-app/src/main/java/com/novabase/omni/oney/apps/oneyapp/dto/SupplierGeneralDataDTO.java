package com.novabase.omni.oney.apps.oneyapp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class SupplierGeneralDataDTO extends SupplierDTO {
    
    @ApiModelProperty
    private String fiscalDesignator;
    
    @ApiModelProperty
    private String socialDesignator;

    @ApiModelProperty
    public String marketType;

}
