package com.novabase.omni.oney.apps.oneyapp.dto.reference;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("ReferenceDataDTO")
public class ReferenceDataDTO extends DTO {

	@ApiModelProperty(value = "Code")
	public String code;

	@ApiModelProperty(value = "Description")
	public String description;

	@ApiModelProperty(value = "Active")
	public boolean active;
	
}
