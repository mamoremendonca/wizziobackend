package com.novabase.omni.oney.apps.oneyapp.dto;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class SupplierDTO extends DTO {
    
    @ApiModelProperty
    public String code;

    @ApiModelProperty
    public String nif;

    @ApiModelProperty
    public String name;

}
