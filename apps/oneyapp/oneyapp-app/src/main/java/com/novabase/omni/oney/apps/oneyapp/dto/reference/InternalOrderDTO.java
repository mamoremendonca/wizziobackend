package com.novabase.omni.oney.apps.oneyapp.dto.reference;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("InternalOrderDTO")
public class InternalOrderDTO extends ReferenceDataDTO {

	@ApiModelProperty(value = "CostCenter")
	public String costCenterCode;

	@ApiModelProperty(value = "Department")
	public String departmentCode;
}
