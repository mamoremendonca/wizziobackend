package com.novabase.omni.oney.apps.oneyapp.enums;

public enum DocumentClassType {

    PURCHASE_ORDER, 
    FINANCIAL_DOCUMENT
    
}
