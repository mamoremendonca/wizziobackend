import React from "react";
import Typography from "@ui-lib/core/Typography";
import Icon from "@ui-lib/core/Icon";
import Button from "@ui-lib/core/Button";
import moment from "moment";

const ExpansionPanelSummaryContent = ({ classes, reports }) => {
  return (
    <div>
      <div className={classes.panelSummaryTitle}>
        <Typography variant="h5">{reports.title}</Typography>
        {reports.status !== undefined && (
          <div className={classes.statusContainer}>
            <span>{reports.status}</span>
          </div>
        )}
      </div>
      <div className={classes.annexActionContainer}>
        {reports.annexes && reports.annexes.length > 0 && (
          <div>
            <Icon className={classes.annexExpandIcon}>attachment_outline</Icon>
            <div className={classes.annexExpandIconBall}>
              <p>{reports.annexes.length}</p>
            </div>
          </div>
        )}
        {reports.action && (
          <div className={classes.actionContainer}>
            <Button
              variant="contained"
              className={classes.actionButton}
              onClick={reports.action.callback}
            >
              <Icon className={classes.actionButtonIcon}>
                add_circle_outline
              </Icon>
              <Typography variant="h5">{reports.action.title}</Typography>
            </Button>
          </div>
        )}
      </div>
      {reports.user && reports.date && (
        <div className={classes.userContainer}>
          <span className={classes.userDateTitle}>
            <Typography>{moment(reports.date).format("DD/MM/YYYY")}</Typography>
          </span>
          <div className={classes.dotContainer} />
          <span className={classes.userDateTitle}>
            <Typography>{reports.user}</Typography>
          </span>
        </div>
      )}
      {reports.date && !reports.user && (
        <div className={classes.singleUserDateTitle}>
          <Typography>{moment(reports.date).format("DD/MM/YYYY")}</Typography>
        </div>
      )}
      {reports.user && !reports.date && (
        <div className={classes.singleUserDateTitle}>
          <Typography>{reports.user}</Typography>
        </div>
      )}
      {reports.items &&
        reports.items.length > 0 &&
        reports.items.map((item, index) => {
          return (
            <div className={classes.itemsContainer}>
              {React.isValidElement(item.header) ||
              React.isValidElement(item.content) ? (
                <div>
                  {item.header}
                  {item.content}
                </div>
              ) : (
                <Typography>
                  {item.header && item.header
                    ? `${item.header}: ${item.content}`
                    : item.header
                    ? item.header
                    : item.content}
                </Typography>
              )}
            </div>
          );
        })}
      {reports.footer && (
        <div className={classes.dateFooterContainer}>
          {reports.footer.card && (
            <span className={classes.footerRow}>
              <Icon className={classes.footerIcon}>folder_open</Icon>
              <Typography>{reports.footer.card}</Typography>
            </span>
          )}
          {reports.footer.card && reports.footer.value && (
            <div className={classes.dotContainer} />
          )}
          {reports.footer.value && (
            <span className={classes.footerRow}>
              <Icon className={classes.footerIcon}>euro_symbol</Icon>
              <Typography>{reports.footer.value}</Typography>
            </span>
          )}
        </div>
      )}
    </div>
  );
};

export default ExpansionPanelSummaryContent;
