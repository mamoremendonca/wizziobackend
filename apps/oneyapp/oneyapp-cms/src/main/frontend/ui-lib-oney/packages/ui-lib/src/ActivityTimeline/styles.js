export const styles = (theme) => ({
    root: {
        '& [class*=MuiExpansionPanel-root]': {
            backgroundColor: '#fff',
            '&:before':{
                content: "none"
            }
        },
        '& [class*=MuiExpansionPanel-disabled]': {
            backgroundColor: '#fff',
        },
        '& [class*=MuiExpansionPanelSummary-disabled]': {
            opacity: 1
        },
        '& [class*=MuiExpansionPanel-expanded]': {
        },
        '& [class*=MuiExpansionPanelSummary-expanded]': {
            // margin: 0
        },
        '& [class*=MuiExpansionPanelSummary-expandIcon]': {
            top: 22
        },
        '& [class*=MuiExpansionPanelDetails-root]': {
            top: 22
        },
        '& [class*=MuiPaper-elevation1]': {
            boxShadow: "none"
        }
    },
    expansions: {
        marginBottom: 20,
        position: 'relative',

    },
    dataContainer: {
    },
    eventItem: {
        margin: '-42px 0 0 50px'
    },
    firstEventItem: {
        margin: '-42px 0 0 50px'
    },
    vl: {
        width: 2,
        height: 'calc(100% + 20px)',
        border: `1px solid #333`,
        display: 'inline-block',
        zIndex: 1,
        position: 'absolute'
    },
    vlInitial: {
        borderLeft: `1px solid ${theme.palette.main}`,
        position: 'absolute',
        height: '100%',
        marginLeft: -29,
        top: 0,
        width: 1,
    },
    contentContainer: {
        width: '100%',
        zIndex: 10,

    },
    dataContainer: {
        margin: '0px -5px 0px 0px',
        display: 'inline-block',
        position: 'relative',
        verticalAlign: 'top'
    },
    ballIcon: {
        backgroundColor:"#fff",
        border: "1px solid #333",
        borderRadius:"50%",
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 40,
        marginRight: -16,
        zIndex: 2,
        display: 'flex',
        position: 'relative',
        width: '40px',
        height: '40px'
    },
    ballIcon: {
        backgroundColor:"#fff",
        border: "1px solid #333",
        borderRadius:"50%",
        justifyContent: 'center',
        alignItems: 'center',
        fontSize: 40,
        marginRight: -16,
        zIndex: 2,
        display: 'flex',
        position: 'relative',
        width: '40px',
        height: '40px'
    },
    selectedBallIcon:{
        color: '#fff',
        backgroundColor: theme.palette.primary.main,
        borderColor: theme.palette.primary.main
    },
    loader: {
        '& svg': {
            width: 18,
            marginBottom: -3
        }
    },
    arrowLeft: {
        width: 0,
        height: 0,
        borderTop: "8px solid transparent",
        borderBottom: "8px solid transparent",
        borderRight: "8px solid white",
        position: 'absolute',
        left: -8,
        top: 10,
        zIndex: 500,

    },
    annexIconContainer:{
        position: "absolute",
        right: 50,
        top: 10,
        fontWeight: '100'
    },
    annexExpandIcon:{
        transform: "translateY(0%) rotate(135deg)",
        position: "relative",
        color: "#8d8d8d"
    },
    annexExpandIconBall:{
        height: 16,
        width: 16,
        backgroundColor: theme.palette.primary.main,
        borderRadius: '50%',
        border: "1px solid white",
        margin: "-25px 0 0 15px",
        position: "relative",
        '& p':{
            color: 'white',
            fontSize: "11px",
            textAlign: "center",
            fontWeight: 800,
            margin: 0
        }
    },
    anexLabel:{
        marginRight: 10,
        cursor: "pointer",
        '&:hover':{
            textDecoration: 'underline',
            color: theme.palette.primary.main
        }
    },
    anexIcon:{
        transform: "translateY(0%) rotate(135deg)",
        position: "relative",
        color: "#8d8d8d",
        marginRight: 10
    },
    anexCloseIcon:{
        cursor: 'pointer',
        '&:hover':{
            color: theme.palette.primary.main
        }
    }
});