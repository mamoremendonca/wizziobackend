const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  mode: "production",
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'build/dist'),
    filename: 'ui-lib-oney-umd.js',
    library: "Oney",
    libraryTarget: "umd"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', '@babel/react'],
          plugins: [
            'babel-plugin-optimize-clsx',
            ['@babel/plugin-proposal-class-properties', { loose: true }],
            ['@babel/plugin-proposal-object-rest-spread', { loose: true }],
            '@babel/plugin-transform-runtime',
            // for IE 11 support
            '@babel/plugin-transform-object-assign',
          ]
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  externals: [
    function (context, request, callback) {
      if (/@ui-lib\/core\/.*/.test(request)) {
        if (request.replace("@ui-lib/core", "") === "") {
          return callback(null, "var " + 'UiLibCore');
        } else {
          return callback(null, "var " + 'UiLibCore.' + request.replace("@ui-lib/core/", "").replace('/', '.'));
        }
      }
      if (/@ui-lib\/custom-components\/.*/.test(request)) {
        if (request.replace("@ui-lib/custom-components", "") === "") {
          return callback(null, "var " + 'UiLibCustomComponents');
        } else {
          return callback(null, "var " + 'UiLibCustomComponents.' + request.replace("@ui-lib/custom-components/", "").replace('/', '.'));
        }
      }
      callback();
    }
  ],
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()],
  }
};