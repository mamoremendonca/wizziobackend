import React from "react";
import Typography from "@ui-lib/core/Typography";
import Icon from "@ui-lib/core/Icon";

const ExpansionPanelDetailsContent = ({
  classes,
  reports,
  downloadClick,
  closeClick,
}) => {
  return reports.annexes && reports.annexes.length > 0
    ? reports.annexes.map((annex) => {
        return (
          <div className={classes.expansionPanelDetailsContentInnerContainer}>
            {annex.editMode && annex.editMode === true ? (
              <React.Fragment>
                <div className={classes.editableAnnexContainer}>
                  <Icon className={classes.anexIcon}>attachment_outline</Icon>
                  <Typography
                    className={classes.anexLabel}
                    onClick={() => downloadClick(annex)}
                  >
                    {annex.fileName}
                  </Typography>
                  {
                    <Icon
                      className={classes.anexCloseIcon}
                      onClick={() => closeClick(annex)}
                    >
                      close
                    </Icon>
                  }
                </div>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <div className={classes.nonEditableAnnexContainer}>
                  <Icon className={classes.nonEditableAnnexContainerIcon}>
                    attachment_outline
                  </Icon>
                  <Typography
                    className={classes.anexLabel}
                    onClick={() => downloadClick(annex)}
                  >
                    {annex.fileName}
                  </Typography>
                </div>
              </React.Fragment>
            )}
          </div>
        );
      })
    : null;
};

export default ExpansionPanelDetailsContent;
