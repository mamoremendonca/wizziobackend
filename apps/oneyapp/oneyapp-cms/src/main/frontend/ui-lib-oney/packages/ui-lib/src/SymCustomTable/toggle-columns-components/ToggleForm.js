import React from 'react';


import {withStyles} from "@ui-lib/core/styles";
import Grid from "@ui-lib/core/Grid";
import Icon from "@ui-lib/core/Icon";
import TextField from "@ui-lib/core/TextField";
import classNames from "@ui-lib/core/classNames";


const styles = theme => ({
  root: {
    padding: "10px 8px",
    zIndex: "10000",
    backgroundColor: "white",
    "& [class*=MuiFormControl-marginNormal]":
    {
      width: "calc(100% - 28px)",
      marginLeft: "10px",
      marginTop: "0px",
      marginBottom: "0px",
      "& [class*=MuiInput-root]":
      {
        marginTop: "0px"
      }
    }
  },
  searchIcon:
  {
    fontSize: "18px",
    color: "#848484"
  },
  textField:
  {
    "& [class*=MuiInput-input]":
    {
      fontSize: "14px",
      padding: "0px"
    },
    "& [class*=MuiInput-root]":
    {
      "&::after":
      {
        display: "none"
      },
      "&::before":
      {
        display: "none"
      }
    }
  }
});

const ToggleForm = ({ classes, ...props }) => {
  return (
    <Grid item xs={12} style={{}} key={props.recordIndex} className={classes.root}>
      <Icon className={classNames(classes.searchIcon, 'icon-search')}/>
      <TextField placeholder={props.getLabelFn ? props.getLabelFn('table_component_toggle_columns_search', 'Search') : 'Search'} id="input1" label="" margin="normal" InputLabelProps={{ shrink: true }} className={classes.textField} onChange={(event) => props.handleSearch(event)} />
    </Grid>
  )
}

export default withStyles(styles)(ToggleForm);
