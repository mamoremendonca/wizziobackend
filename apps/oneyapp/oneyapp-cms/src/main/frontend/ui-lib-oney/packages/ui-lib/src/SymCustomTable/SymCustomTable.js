import React from "react";
import classNames from "@ui-lib/core/classNames";
import DynamicFilters from './DynamicFilters';
import DatePickerInput  from '../DatePickerInput';
import moment from 'moment/moment.js';
import Table from "@ui-lib/core/Table";
import TableHead from "@ui-lib/core/TableHead";
import TableRow from "@ui-lib/core/TableRow";
import TableCell from "@ui-lib/core/TableCell";
import Checkbox from "@ui-lib/core/Checkbox";
import Icon from "@ui-lib/core/Icon";
import Grid from "@ui-lib/core/Grid";
import Tooltip from "@ui-lib/core/Tooltip";
import Typography from "@ui-lib/core/Typography";
import CircularProgress from "@ui-lib/core/CircularProgress";
import TextField from "@ui-lib/core/TextField";
import TableBodyStruct from './TableBodyStruct';
import AutocompleteInput from '../AutocompleteInput';
import { tableStyles } from './styles';
import _ from "lodash";
import ActionsBar from "./ActionsBar";
import ToggleColumnsForm from "./ToggleColumnsForm";
import ToggleItem from './toggle-columns-components/ToggleItem';
import {withStyles} from "@ui-lib/core/styles";
export const customTableStyles = tableStyles;

class SymCustomTableClass extends React.Component {

    //Construir configuracao de colunas por user
    buildTableConfig = (tableHeaders) => {
        let tableConfig = [];

        for (let arrayIndex = 0; arrayIndex < tableHeaders.length; arrayIndex++) {
            tableConfig[tableHeaders[arrayIndex].name] = tableHeaders[arrayIndex].visible;
        }

        return tableConfig;
    };

    setColumnState = (columnKey, stateVal) => {
        let currTableConfig = this.state.columnsConfiguration;

        currTableConfig[columnKey] = stateVal;

        let nextMap = [];

        let nextMapKeys = Object.keys(currTableConfig);

        for (let currIndex = 0; currIndex < nextMapKeys.length; currIndex++) {
            let currKey = nextMapKeys[currIndex];

            nextMap[currKey] = currTableConfig[currKey];
        }
        this.setState({
            columnsConfiguration: nextMap
        });
    };

    changeColumnState = (columnKey) => {
        let currState = this.getColumnStateVal(columnKey);

        this.setColumnState(columnKey, !currState);
    };

    getColumnStateVal = (columnKey) => {
        let columnVal = this.state.columnsConfiguration[columnKey];

        return columnVal == true;
    };

    getBeforeClassHeader = (type) => {
        let className;
        switch (type) {
            case 'critical':
                className = 'critical';
                break;
            case 'non-critical':
                className = 'non-critical';
                break;
            case 'other':
                className = 'other';
                break;
            default:
                className = '';
        }
        return className;
    }

    //getTotalOfVisibleC

    //Manter em cookie a configuracao das colunas do user (de acordo com o id da tabela)

    //Verificar se existe cookie de configuracao de colunas, em caso positivo carrega a existente.

    constructor(props) {
        super(props);

        let sortedFields = [];
        let defaultSortedField = {};

        if (props.defaultSortColumn) {
            defaultSortedField.name = props.defaultSortColumn;
            defaultSortedField.order = "ASC";
            sortedFields.push(defaultSortedField);
        }

        if (Object.keys(defaultSortedField).length > 0 && props.defaultSortOrder) {
            defaultSortedField.order = props.defaultSortOrder.toUpperCase() === 'ASC' ? "ASC" : "DESC";
        }

        this.state = {
            page: 1,
            selectedIds: [],
            selectedRows: [],
            allSelected: false,
            sortedFields: sortedFields,
            loadBtnDisabled: false,
            filtersMap: [],
            showFilter: false,
            showColumnsSel: false,
            columnsConfiguration: [],//this.buildTableConfig(props.headers)
            toggleTerms: undefined,
            colToggleList: [],
            searchedCols: false,
            conditionalPrefixes: null,
            dataPagesMap: [],
            allDataRecords: []
        };
    }

    static defaultProps = {
        component: ("div"),
        rowsSelectable: false,
        headers: [],
        allDataRecords: [],
        counterString: undefined,
        onRowClick: undefined,
        rowsPerPage: 10,
        onLoadMore: null,
        width: "100%",
        showCheckAll: true,
        loadMoreLabel: "Load more",
        loadAllLabel: "Load all results",
        emptyText: "No data",
        dataPagesMap: []
    };

    getApi = () => {
        return {
            getSelectedRows: () => {
                return this.state.selectedRows;
            },
            getCurrentSelectedRows: () => {
                let rowToReturn = [];
                this.props.data.forEach((e) => {
                    if (this.state.selectedRows.find((ele) => e.id === ele.id) !== undefined) {
                        rowToReturn.push(e);
                    }
                });
                return rowToReturn;
            },
            getCurrentPage: () => {
                return this.state.page;
            },
            setCurrentPage: (page) => {
                this.setState({ page: page });
            },
            setCurrentSortedFields: (sortedFields) => {
                this.setState({ sortedFields: sortedFields });
            },
            getTotalRows: () => {
                /*if (this.props.data.length > this.state.page * this.props.rowsPerPage) {
                    return this.state.page * this.props.rowsPerPage;
                } else {
                    return this.props.data.length;
                }*/
                return this.getTotalOfRows()
            },
            getSortedFields: () => {
                return this.state.sortedFields;
            },
            resetSortedFields: () => {
                this.setState({ sortedFields: [] });
            },
            selectRow: (row) => {
                this.onToggleSelect(row);
            },
            setSelectedFields: (rows) => {
                this.setState({ selectedRows: rows });
            }
        };
    };

    /**
     * Get number of visible rows,
     */
    getTotalOfRows = () => {
        let total = 0;
        if (this.state.dataPagesMap) {
            for (let currKey in this.state.dataPagesMap) {
                if (currKey <= this.state.page) {
                    total += this.state.dataPagesMap[currKey].length;
                }
            }
        }

        if (total == 0 && this.props.data) {
            total = this.props.data.length;
        }

        return total;
    }

    getNormalizedFilter = () => {
        let normalizedMap = [];

        let filterableCols = this.getVisibleCols(this.props.headers, true, false);
        if (filterableCols) {
            for (let headerIndex = 0; headerIndex < filterableCols.length; headerIndex++) {
                let currHeader = filterableCols[headerIndex];
                if (this.state.filtersMap[currHeader.name]) {
                    let trimmedValue = this.state.filtersMap[currHeader.name].value.trim(); //trim blankspaces on both sides of the string
                    let filterObject = this.state.filtersMap[currHeader.name]; //set filter value to trimmed value string
                    filterObject.value = trimmedValue;
                    normalizedMap[currHeader.dbColumnName ? currHeader.dbColumnName : currHeader.name] = filterObject;
                }
            }
        }
        return normalizedMap;
    }

    getNormalizedSortInfo = (sortInfoArray) => {
        let normalizedSortInfoArray = sortInfoArray;

        if (sortInfoArray &&
            sortInfoArray.length > 0 &&
            sortInfoArray[0].dbFieldName) {

            let sortInfoItem = sortInfoArray[0];
            let normalizedSortInfo = JSON.parse(JSON.stringify(sortInfoItem));

            normalizedSortInfo.name = sortInfoItem.dbFieldName;
            normalizedSortInfoArray = [];
            normalizedSortInfoArray.push(normalizedSortInfo);
        }
        return normalizedSortInfoArray;
    }

    onLoadMore = (newPage, requestAgain) => {

        this.setState({ allSelected: false, loadBtnDisabled: true });

        if (this.getTotalOfRows() > this.props.rowsPerPage * newPage) {
            if (requestAgain) {
                this.props.onLoadMore && this.props.onLoadMore(newPage, this.props.rowsPerPage, this.getNormalizedSortInfo(this.state.sortedFields), this.getNormalizedFilter()).then(() => {
                    this.setState({ page: newPage });
                }).catch(() => {
                    this.setState({ loadBtnDisabled: false });
                });
            } else {
                this.setState({ page: newPage, loadBtnDisabled: false });
            }
        } else {
            this.props.onLoadMore && this.props.onLoadMore(newPage, this.props.rowsPerPage, this.getNormalizedSortInfo(this.state.sortedFields), this.getNormalizedFilter()).then(() => {
                this.setState({ page: newPage });
            }).catch(() => {
                this.setState({ loadBtnDisabled: false });
            });
        }
    };

    onLoadAll = (newPage, requestAgain) => {

        this.setState({ allSelected: false, loadBtnDisabled: true });
        if (this.getTotalOfRows() > this.props.rowsPerPageAll * newPage) {
            if (requestAgain) {
                this.props.onLoadMore && this.props.onLoadMore(newPage, this.props.rowsPerPageAll, this.getNormalizedSortInfo(this.state.sortedFields), this.getNormalizedFilter()).then(() => {
                    this.setState({ page: newPage });
                }).catch(() => {
                    this.setState({ loadBtnDisabled: false });
                });
            } else {
                this.setState({ page: newPage, loadBtnDisabled: false });
            }
        } else {
            this.props.onLoadMore && this.props.onLoadMore(newPage, this.props.rowsPerPageAll, this.getNormalizedSortInfo(this.state.sortedFields), this.getNormalizedFilter()).then(() => {
                this.setState({ page: newPage });
            }).catch(() => {
                this.setState({ loadBtnDisabled: false });
            });
        }
    };

    onSortClicked = (fieldName, dbFieldName) => {

        let currPage = this.state.page;
        if (this.props.resetPageOnSort && this.props.resetPageOnSort === true) {
            currPage = 1;
        }

        if (this.state.loadBtnDisabled) {
            return;
        }

        let field = this.state.sortedFields.find((e) => e.name === fieldName);

        if (field !== undefined) {
            let newSortedFields = _.cloneDeep(this.state.sortedFields);
            if (field.order === "asc") {
                newSortedFields = newSortedFields.map((e) => {
                    if (e.name === field.name) {
                        return { "name": e.name, "dbFieldName": dbFieldName, "order": "desc", };
                    }
                    else {
                        return e;
                    }
                });
            }
            else {
                newSortedFields = newSortedFields.filter((e) => e.name !== field.name);
            }
            this.setState({ sortedFields: newSortedFields }, () => this.onLoadMore(currPage, true));
        } else {
            let newSortedFields = [];//_.cloneDeep(this.state.sortedFields);
            newSortedFields.push({ "name": fieldName, "dbFieldName": dbFieldName, "order": "asc" });
            this.setState({ sortedFields: newSortedFields }, () => this.onLoadMore(currPage, true));
        }
    };

    onCheckAll = () => {
        if (this.state.allSelected) {
            this.setState({ selectedRows: [], allSelected: false });

            this.props.handleCheckAll ? 
            this.props.handleCheckAll([]) :
            console.log("pass down a handleCheckAll function to table component")
        } else {
            let newSelectedItems = [];
            this.props.data.forEach((e) => {
                newSelectedItems.push(e);
            });
            this.setState({ selectedRows: newSelectedItems, allSelected: true });
            //send checked items info to parent component
            this.props.handleCheckAll ? 
            this.props.handleCheckAll(newSelectedItems) :
            console.log("pass down a handleCheckAll function to table component")
        }
    };

    onToggleSelect = (event, row) => {
        let element = this.state.selectedRows.find((e) => e.id === row.id);

        if (element !== undefined) {
            let newSelectedRows = _.cloneDeep(this.state.selectedRows);
            _.remove(newSelectedRows, (e) => e.id === row.id);
            this.setState({ selectedRows: newSelectedRows, allSelected: false })
            
            if(this.props.handleSelectRow){
            this.props.handleSelectRow(newSelectedRows);
            }
            else
            {
             console.log("pass down a handleSelectRow to table component")
            }
        } else {
            let allSelected = this.state.selectedRows.length + 1 === this.getTotalOfRows();
            this.setState({ selectedRows: this.state.selectedRows.concat([row]), allSelected: allSelected });
       
            if(this.props.handleSelectRow){
                    this.props.handleSelectRow(this.state.selectedRows.concat([row]));
                }
                else
                {
                    console.log("pass down a handleSelectRow to table component")
                }
        }
    };

    renderHeader = (header, sortedField, isSubHeader, getLabel) => {
        let headerConfig = this.props.customHeaderConfig;
        let sortHeaderClassName = "";
        let styles = { cursor: "pointer", position: "relative", userSelect: "none" };
        if (this.state.sortedFields) {
            sortHeaderClassName = sortedField ? this.props.classes.headerSorted : this.props.classes.headerNonSorted;
        }
        if (this.state.loadBtnDisabled) {
            styles = Object.assign({}, styles, { cursor: "wait" });
        }

        let tempLabel = getLabel ? getLabel(header.i18nKey, header.label) : header.label;

        if (header.customHeaderFunc) {
            tempLabel = header.customHeaderFunc(header.label);
        }

        if (header.sortable) {
            return (
                <span className={sortHeaderClassName}
                    style={styles}
                    onClick={() => this.onSortClicked(header.name, header.dbColumnName)}
                >
                    <Icon style={{ display: header.toggable ? "" : "none" }} className={classNames(this.props.classes.hideColumnIcon,this.props.classes.placeholderIcon,'icon-minus')} onClick={() => this.changeColumnState(header.name)}></Icon>
                    {header.tooltip ?
                        <Tooltip placement={"top"} title={header.tooltip}>
                            <span style={isSubHeader ? { color: "#AEAEAE", fontSize: "14px" } : {}}>
                                {(headerConfig && headerConfig()[header.name]) ?  headerConfig()[header.name]() : tempLabel}
                                {sortedField && <Icon className={classNames(this.props.classes.sortIcon,(sortedField.order === "asc" ? "icon-arrow-up" : "icon-arrow-down"))}/>
                                }
                            </span>
                        </Tooltip>
                        :
                        <span className={header.beforeClassHeader ? this.props.classes[this.getBeforeClassHeader(header.beforeClassHeader)] : ''} style={isSubHeader ? { color: "#AEAEAE", fontSize: "14px" } : {}}>
                            {(headerConfig && headerConfig()[header.name]) ?  headerConfig()[header.name]() : tempLabel}
                            {sortedField && <Icon className={classNames(this.props.classes.sortIcon,(sortedField.order === "asc" ? "icon-arrow-up" : "icon-arrow-down"))}>
                            </Icon>}
                            <Icon  className={classNames(this.props.classes.sortIcon,this.props.classes.placeholderIcon,"icon-arrow-up")}/>
                        </span>
                    }
                </span>
            );
        } else {
            return (
                <span className={sortHeaderClassName} style={styles}>
                    <Icon style={{ display: header.toggable ? "" : "none" }} className={classNames(this.props.classes.hideColumnIcon,this.props.classes.placeholderIcon,'icon-minus')} onClick={() => this.changeColumnState(header.name)}/>
                    {header.tooltip ?
                        <Tooltip placement={"top"} title={header.tooltip}>
                            <span>{tempLabel}</span>
                        </Tooltip>
                        :
                        <span style={isSubHeader ? { color: "#AEAEAE", fontSize: "14px" } : {}}>{tempLabel}</span>
                    }
                </span>
            );
        }
    };

    componentDidMount() {
        if (this.props.api) {
            this.props.api(this.getApi());
        }
    };

    componentWillReceiveProps(nextProps) {
        if (this.state.searchedCols === false && this.props.headers !== nextProps.headers) {
            this.setState({ colToggleList: this.getVisibleCols(nextProps.headers, false, true) });
            this.setState({ searchedCols: true })
        }

        let tempDataPagesMap = this.state.dataPagesMap;

        tempDataPagesMap[this.state.page] = nextProps.data;

        let newDataRecords = [];

        for (let arrayIndex = 1; arrayIndex <= this.state.page; arrayIndex++) {

            let currPage = tempDataPagesMap[arrayIndex];
            if (currPage) {
                for (let pageRecIndex = 0; pageRecIndex < currPage.length; pageRecIndex++) {
                    newDataRecords.push(currPage[pageRecIndex]);
                }
            }
        }

        this.setState({
            allDataRecords: newDataRecords,
            dataPagesMap: tempDataPagesMap,
            loadBtnDisabled: false
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.columnsConfiguration &&
            Object.keys(this.state.columnsConfiguration).length == 0 &&
            this.props.headers.length > 0) {
            let columnsConfigurationInst = this.buildTableConfig(this.getVisibleCols(this.props.headers, false, true));
            if (columnsConfigurationInst && Object.keys(columnsConfigurationInst).length > 0) {
                this.setState({
                    columnsConfiguration: columnsConfigurationInst
                });
            }
        }

        if (!this.state.conditionalPrefixes &&
            this.props.headers &&
            this.props.headers.length > 0) {
            let tempCondPrefixes = [];
            for (let arrayIndex = 0; arrayIndex < this.props.headers.length; arrayIndex++) {
                let currHeader = this.props.headers[arrayIndex];

                if (currHeader && currHeader.conditionalPrefix) {
                    for (let condPrefixIndex = 0; condPrefixIndex < currHeader.conditionalPrefix.length; condPrefixIndex++) {
                        let currCondPrefix = currHeader.conditionalPrefix[condPrefixIndex];
                        if (currHeader.name && currCondPrefix.code && (currCondPrefix.cssClass != undefined || currCondPrefix.cssClass != null)) {
                            tempCondPrefixes[currHeader.name.toUpperCase() + "_" + currCondPrefix.code.toUpperCase()] = currCondPrefix.cssClass.toUpperCase();
                        }
                    }
                }
            }
            this.setState({
                conditionalPrefixes: tempCondPrefixes
            });
        }
    };

    /**
     * Reset filter map.
     */
    resetFilterMap = () => {

        /**
         * Function to execute before apply reset.
         */
        if (this.props.beforeReset) {
            this.props.beforeReset();
        }

        this.setState({
            filtersMap: []
        }, this.handleFilter);

        if (this.props.setFilterMapFn) {
            this.props.setFilterMapFn({});
        }
    };

    /**
     * Set filter map.
     */
    setFilterMap = (columnKey, value) => {

        let tempFiltersMap = this.state.filtersMap;

        let cols = this.getVisibleCols(this.props.headers, true, false);

        for (let arrayIndex = 0; arrayIndex < cols.length; arrayIndex++) {
            let currColumn = cols[arrayIndex];
            if (currColumn.name == columnKey) {

                let formatedValue = value;
                if (currColumn.dataType == 'DATE') {
                    let defaultDateFormat = "DD/MM/YYYY";
                    if (currColumn.applyDateFormat) {
                        defaultDateFormat = currColumn.applyDateFormat;
                    }
                    formatedValue = (formatedValue === null || formatedValue === '') ? '' :
                        moment(value, defaultDateFormat).format("YYYY-MM-DD HH:mm:ss")
                }

                tempFiltersMap[columnKey] = {
                    "dataType": currColumn.dataType,
                    "value": formatedValue
                }

                if (currColumn.exactValue) {
                    tempFiltersMap[columnKey].exactValue = currColumn.exactValue;
                }

            }
        }
        this.setState({
            filtersMap: tempFiltersMap
        });

        if (this.props.setFilterMapFn) {
            this.props.setFilterMapFn(tempFiltersMap);
        }
    };

    /**
     * Update filter map from an event object.
     */
    setFilterMapFromEvent = (columnKey, event) => {
        this.setFilterMap(columnKey, event.target.value);
    };

    /**
     * Toggle show filter state.
     */ toggleFilterState = () => {
        this.setState({
            showFilter: !this.state.showFilter
        });
    };

    hideTableFilter = () => {
        this.setState({
            showFilter: false
        });
    };

    toggleColumnsSelState = () => {
        this.setState({
            showColumnsSel: !this.state.showColumnsSel
        });
    };

    hideColumnsSel = () => {
        this.setState({
            showColumnsSel: false
        });
    };

    /**
     * Handle filter.
     */
    handleFilter = () => {
        this.onLoadMore(1, true);
    };

    capitalizeFirstLetter = (string) => {
        return string
            .charAt(0)
            .toUpperCase() + string.slice(1);
    }

    /**
     * Generate default filter input.
     */
    generateDefaultFilterInput = (recordIndex, column) => {
        let filterKey = column.name;
        // if (column.dbColumnName) {
        //     filterKey = column.dbColumnName;
        // }


        return (
            <Grid item xs={3} style={{ padding: "20px", zIndex: 200 - recordIndex }} key={recordIndex}>
                <TextField
                    InputLabelProps={{ shrink: true }}
                    fullWidth
                    id={this.props.prefix + "-" + column.name}
                    key={column.key}
                    label={this.props.getLabelFn && column.i18nKey ? this.capitalizeFirstLetter(this.props.getLabelFn(column.i18nKey, column.label)) : this.capitalizeFirstLetter(column.label)}
                    margin="normal"
                    onChange={(event) => this.setFilterMapFromEvent(filterKey, event)}
                    placeholder={this.props.getLabelFn ? this.props.getLabelFn('table_filters_insert_sring', 'Insert ') : 'Insert ' + column.label.toLowerCase() + '...'}
                    value={(this.state.filtersMap && this.state.filtersMap[filterKey] && this.state.filtersMap[filterKey].value) ? this.state.filtersMap[filterKey].value || "" : ""}
                    onKeyPress={event => {
                        if (event.key === 'Enter') {
                            this.handleFilter();
                        }
                    }}
                />
            </Grid>
        );
    };

    generateDropDownFilterInput = (recordIndex, column) => {
        let filterKey = column.name;
        return (
            <Grid item xs={3} style={{ padding: "20px", zIndex: 200 - recordIndex }} key={recordIndex}>
                <AutocompleteInput
                    dropdown={column.dropdownType}
                    options={column.dropdownOptions}
                    maxVisible={column.maxVisibleOptions}
                    servicesApi={this.props.servicesApi}
                    prefix={this.props.prefix + "-" + column.name}
                    label={this.props.getLabelFn && column.i18nKey ? this.capitalizeFirstLetter(this.props.getLabelFn(column.i18nKey, column.label)) : this.capitalizeFirstLetter(column.label)}
                    setValueFn={(value) => this.setFilterMap(filterKey, value)}
                    optionsURL={column.dropdownOptionsUrl}
                    defaultValue={(this.state.filtersMap && this.state.filtersMap[filterKey] && this.state.filtersMap[filterKey].value) ? this.state.filtersMap[filterKey].value || "" : ""}
                    getLabelFn={this.props.getLabelFn ? (key, defaultLabel) => this.props.getLabelFn(key, defaultLabel) : null}
                />
            </Grid>
        );
    };

    //
    generateDatePickerFilterInput = (recordIndex, column) => {
        let filterKey = column.name;

        return (
            <Grid item xs={3} style={{ padding: "20px", zIndex: 200 - recordIndex }} key={recordIndex}>
                <DatePickerInput
                     onChangeToggleValue={this.onChangeToggleValue}
                    isToggle={false}
                    label={this.props.getLabelFn && column.i18nKey ? this.capitalizeFirstLetter(this.props.getLabelFn(column.i18nKey, column.label)) : this.capitalizeFirstLetter(column.label)}
                    currentFromFieldValue={(this.state.filtersMap && this.state.filtersMap[filterKey] && this.state.filtersMap[filterKey].value) ? this.state.filtersMap[filterKey].value || "" : ""}
                    applySpecificFormat={column.applyDateFormat ? column.applyDateFormat : "DD/MM/YYYY"}
                    applyFormat={true}
                    onlyCalendar={true}
                    onlySingleDay={true}
                    forTableFilter={true}
                    getLabelFn={this.props.getLabelFn ? (key, defaultLabel) => this.props.getLabelFn(key, defaultLabel) : null}
                    handleDayClick={(value) => this.setFilterMap(filterKey, value)}
                    prefix={`${this.props.prefix}${filterKey}-datepicker`}
                    >
                </DatePickerInput>
            </Grid>
        );
    };

    //generate table filters according to current columns
    createFilters = (filterableCols) => {
        if (filterableCols.length == 0) {
            return <div></div>
        }
        else {
            return filterableCols.map((column, index) => {
                if (this.props.noFilter) {
                    let hidden = this.props.noFilter.indexOf(column.key.toLowerCase()) !== -1 ? true : false;
                    if (hidden) {
                        return false;
                    }
                }
                if (column.dataType === 'DATE' && column.filterable !== false) {
                    return this.generateDatePickerFilterInput(index, column);
                }
                //default filter input
                else {
                    if (column.dropdown) {
                        return this.generateDropDownFilterInput(index, column);
                    }
                    else {
                        return this.generateDefaultFilterInput(index, column);
                    }
                }
            });
        }
    };

    /**
     * Get all toggable columns.
     */
    getAllToggableCols = () => {
        let visibleCols = this.getVisibleCols(this.props.headers, false, true);
        let allVisToggableCols = [];
        for (let arrayIndex = 0; arrayIndex < visibleCols.length; arrayIndex++) {
            let currColl = visibleCols[arrayIndex];
            if (currColl.toggable) {
                allVisToggableCols.push(currColl);
            }
        }
        return allVisToggableCols;
    }



    /**
     * Handle action after click on select all checkbox.
     */
    clickSelectAllAction = (checkVal) => {

        const checkCollIsVisible = (name) => {
            let isVisible = false;
            for (let arrayIndex = 0; arrayIndex < this.state.colToggleList.length; arrayIndex++) {
                if (this.state.colToggleList[arrayIndex].name == name) {
                    isVisible = true;
                    break;
                }
            }
            return isVisible;
        };

        let allToggableCols = this.getAllToggableCols();
        for (let arrayIndex = 0; arrayIndex < allToggableCols.length; arrayIndex++) {
            let currColl = allToggableCols[arrayIndex];
            let collIsVisible = checkCollIsVisible(currColl.name);

            if (currColl.toggable && collIsVisible) {
                this.setColumnState(currColl.name, checkVal);
            }
        }
    };

    /**
     * Build select all checkbox.
     */
    buildSelectAll = (disabled) => {
        let column = { "label": this.props.getLabelFn ? this.props.getLabelFn('table_component_toggle_columns_all', 'Select All') : 'Select All' };
        let totalOfToggableChecked = 0;
        let allToggableCols = this.state.colToggleList;//this.getAllToggableCols();
        let totalOfToggable = allToggableCols.length;

        for (let arrayIndex = 0; arrayIndex < allToggableCols.length; arrayIndex++) {
            let currCol = allToggableCols[arrayIndex];

            if (this.state.columnsConfiguration &&
                this.state.columnsConfiguration[currCol.name]) {
                totalOfToggableChecked++;
            }
        }

        let checkedVal = (totalOfToggable > 0 && (totalOfToggable == totalOfToggableChecked));
        return (<ToggleItem recordIndex={1} checkedVal={checkedVal} handleChange={() => this.clickSelectAllAction(!checkedVal)} column={column} disabled={disabled}>
        </ToggleItem>
        );
    };

    handleColsSearch = (event) => {
        let result = this.getVisibleCols(this.props.headers, false, true).filter(item => item.label.toLowerCase().includes(event.target.value.toLowerCase()));

        this.setState({ colToggleList: result })
    }

    getVisibleCols = (headers, includeChilds, showHidden, onlyFilterable) => {
        let onlyVisibleColsArray = [];

        for (let arrayIndex = 0; arrayIndex < headers.length; arrayIndex++) {
            let currHeader = headers[arrayIndex];

            let visibleSubHeaderCols = [];
            if (currHeader.subcolumns) {
                visibleSubHeaderCols = currHeader.subcolumns;
            }

            if (!this.props.onlyVisibleCols ||
                showHidden ||
                (this.props.onlyVisibleCols &&
                    (currHeader.subcolumns == undefined || visibleSubHeaderCols.length > 0) &&
                    this.state.columnsConfiguration &&
                    this.state.columnsConfiguration[currHeader.name])) {

                if (includeChilds && visibleSubHeaderCols.length > 0) {
                    onlyVisibleColsArray = onlyVisibleColsArray.concat(visibleSubHeaderCols);
                }
                else {
                    onlyVisibleColsArray.push(currHeader);
                }

            }
        }

        //Exclude not filter allowed cols.
        if (onlyFilterable && onlyVisibleColsArray) {
            let onlyFilterableCols = [];//visibleCols;
            for (let collIndex = 0; collIndex < onlyVisibleColsArray.length; collIndex++) {

                let currColl = onlyVisibleColsArray[collIndex];

                if (currColl && currColl.filterable != false) {
                    onlyFilterableCols.push(currColl);
                }
            }

            onlyVisibleColsArray = onlyFilterableCols;
        }
        return onlyVisibleColsArray;
    };


    /**
     * Apply a format from header to a cell value.
     */
    formatCell = (header, value) => {
        let formatedValue = value;

        if (header.applyDateFormat && value) {
            try {
                formatedValue = moment(value).format(header.applyDateFormat);
            }
            catch (e) {

            }
        }

        return formatedValue;
    };


    renderSubHeader = (header, headerIndex, classes, getLabel) => {
        if (header.subcolumns && header.subcolumns.length > 0) {
            return header.subcolumns.map((subheader, index) => {
                let sortedField = this.state.sortedFields.find((e) => e.name === subheader.name);
                return (
                    <TableCell className={classes.headerCell} key={`row-${index}-${subheader.name}-subheader`}>
                        {this.renderHeader(subheader, sortedField, true, getLabel)}
                    </TableCell>
                );
            });
        }
        else {
            return <TableCell className={classes.headerCell} key={`row-${headerIndex}-${header.name}-subheader`}>{""}</TableCell>
        }
    };

    computePageNumber = (rowsPerPage) => {
        let tempTotalOfRows = this.getTotalOfRows();

        return Math.floor(tempTotalOfRows / rowsPerPage) + 1;
    };

    render() {

        const {
            classes,
            component: ComponentProp,
            rowsSelectable,
            showCheckAll,
            counterString,
            allowRefresh,
            width,
            loadMoreLabel,
            loadAllLabel,
            emptyText,
            onRowClick,
            totalOfRows,
            allowFilterRecs,
            allowShowHideCols,
            allowDownloadCSV,
            allowAddRecord,
            addRecordTitle,
            addRecordFn,
            actions,
            rowAction,
            getLabelFn,
            displayRowWarningCondition,
            refreshData,
            downloadTitle,
            tableTitle
        } = this.props;


        let headers = this.getVisibleCols(this.props.headers);

        let hasSubHeaders = false;

        headers.map((header, index) => {
            if (header.subcolumns && header.subcolumns.length > 0) {
                hasSubHeaders = true;
            }
        })

        let allCols = this.getVisibleCols(this.props.headers, true);
        let onlyFilterableCols = this.getVisibleCols(this.props.headers, true, false, true);
        let newDataRecords = this.state.allDataRecords;

        let showMore = (newDataRecords ? newDataRecords.length : 0) < totalOfRows;

        let totalizerMsg = "";
        if (counterString) {
            totalizerMsg = counterString().replace("<TOTAL_OF_ROWS>", newDataRecords.length);
        }

        return (
            <ComponentProp>
                <div id={this.props.prefix + "-table"}>

                    {/*TABLE ACTIONS TOOLBAR*/}
                    <ActionsBar
                        prefix={this.props.prefix + "actions-bar"} handleToggleFilterState={() => this.toggleFilterState()}headers={this.props.headers}
                        addRecordTitle={addRecordTitle}
                        allowAddRecord={allowAddRecord}
                        addRecordFn={addRecordFn}
                        getLabelFn={getLabelFn}
                        allowRefresh={allowRefresh}
                        refreshData={refreshData}
                        allowDownloadCSV={allowDownloadCSV}
                        allowShowHideCols={allowShowHideCols}
                        onlyFilterableCols={onlyFilterableCols}
                        allowFilterRecs={allowFilterRecs}
                        showColumnsSel={this.state.showColumnsSel}
                        downloadTitle={downloadTitle}
                        allDataRecords={this.state.allDataRecords}
                        getVisibleCols={this.getVisibleCols}
                        showFilter={this.state.showFilter}
                        toggleColumnsSelState={this.toggleColumnsSelState}
                    ></ActionsBar>

                    {/*SHOW AND HIDE COLUMNS FORM*/}
                    <ToggleColumnsForm
                        colToggleList={this.state.colToggleList}
                        showColumnsSel={this.state.showColumnsSel}
                        headers={this.props.headers}
                        hideColumnsSel={() => this.hideColumnsSel()}
                        handleColsSearch={this.handleColsSearch}
                        getLabelFn={getLabelFn}
                        buildSelectAll={this.buildSelectAll}
                        allowShowHideCols={allowShowHideCols}
                        prefix={this.props.prefix + "toggle-columns-form"}
                        buildSelectAll={this.buildSelectAll}
                        getColumnStateVal={this.getColumnStateVal}
                    >
                    </ToggleColumnsForm>

                    {tableTitle && 
                    <Grid  item xs={12} style={{marginTop: -20}}>
                        <Typography style={{fontSize: 18}} variant={'h6'}>
                        {tableTitle}
                        </Typography>
                   </Grid>}

                    <Grid item xs={12} className={classes.filters}>
                        <DynamicFilters
                            clearFilters={() => this.resetFilterMap()}
                            filters={this.createFilters(onlyFilterableCols)}
                            handleFilter={() => this.handleFilter()}
                            searchTerms={true}
                            showFilter={this.state.showFilter}
                            hideCompFn={() => this.hideTableFilter()}
                            relatedIcon={this.props.prefix + "-filter-button"}
                            getLabelFn={getLabelFn ? (key, defaultLabel) => getLabelFn(key, defaultLabel) : null}
                        />
                    </Grid>

                    <Grid item xs={12}>
                        <div className={classes.counter}>
                            <Typography variant="body2">{totalizerMsg}</Typography>
                        </div>
                    </Grid>

                    

                    <Grid item xs={12} style={{ position: "relative" }}>
                        {this.props.status === "loading" &&
                            <div className={hasSubHeaders ? `${classes.tableLoader} ${classes.loaderHasSubheaders}` : classes.tableLoader}>
                                <CircularProgress className={classes.loader} />
                            </div>
                        }
                        <Table style={{ width: width }} className={classes.root}>
                            <TableHead>
                                <TableRow>
                                    {
                                        showCheckAll && newDataRecords.length > 0 ?
                                            rowsSelectable ?
                                                <TableCell className={classes.checkboxCell}>
                                                    <Checkbox checked={this.state.allSelected}
                                                        onChange={(event) => this.onCheckAll(event)}
                                                        icon={false}
                                                        checkedIcon={<Icon style={{ fontSize: "16px" }}>done</Icon>} />
                                                </TableCell>
                                                :
                                                undefined
                                            :
                                            rowsSelectable && newDataRecords.length ?
                                                <TableCell className={classes.checkboxCell}>{""}</TableCell>
                                                :
                                                undefined
                                    }
                                    {
                                        headers.map((header, index) => {
                                            let sortedField = this.state.sortedFields.find((e) => e.name === header.name);

                                            let totalOfSubHeaders = 0;
                                            if (header.subcolumns && header.subcolumns.length > 0) {
                                                totalOfSubHeaders = header.subcolumns.length;
                                            }

                                            let customStyle = {};
                                            if (totalOfSubHeaders > 0) {
                                                customStyle.borderBottom = "1px solid #EEEEEE";
                                            }
                                            if (header.align) {
                                                customStyle.textAlign = header.align;
                                            }

                                            return (
                                                <TableCell className={classes.headerCell} key={`row-${index}-${header.name}`} colSpan={totalOfSubHeaders == 0 ? 1 : totalOfSubHeaders} style={customStyle}>
                                                    {this.renderHeader(header, sortedField, false, getLabelFn ? (key, defaultLabel) => getLabelFn(key, defaultLabel) : null)}
                                                </TableCell>
                                            );
                                        })
                                    }
                                    {actions && newDataRecords.length > 0 && actions.length > 0 && <TableCell className={classes.actions} />}
                                </TableRow>
                                {hasSubHeaders &&
                                    <TableRow>
                                        {
                                            showCheckAll ?
                                                rowsSelectable ? <TableCell className={classes.checkboxCell}>{""}</TableCell> : undefined
                                                :
                                                rowsSelectable ? <TableCell className={classes.checkboxCell}>{""}</TableCell> : undefined
                                        }
                                        {

                                            headers.map((header, index) => {
                                                return (
                                                    this.renderSubHeader(header, index, classes, getLabelFn ? (key, defaultLabel) => getLabelFn(key, defaultLabel) : null)
                                                );
                                            })
                                        }
                                    </TableRow>}
                            </TableHead>

                            <TableBodyStruct
                                displayRowWarningCondition={displayRowWarningCondition ? displayRowWarningCondition : null}
                                rowsSelectable={rowsSelectable}
                                emptyText={emptyText}
                                onRowClick={onRowClick}
                                customCellsConfig={this.props.customCellsConfig}
                                actions={actions}
                                rowAction={(row) => rowAction(row)}
                                headers={headers}
                                newDataRecords={newDataRecords}
                                classNames={classNames}
                                allCols={allCols}
                                selectedRows={this.state.selectedRows}
                                conditionalPrefixes={this.state.conditionalPrefixes}
                                status={this.props.status}
                                getLabelFn={getLabelFn ? (key, defaultLabel) => getLabelFn(key, defaultLabel) : null}
                                disableMenuCondition={this.props.disableMenuCondition ? this.props.disableMenuCondition : null}
                                onToggleSelect={(event, row) => this.onToggleSelect(event, row)}
                            />
                        </Table>
                        {
                            showMore &&
                                this.state.loadBtnDisabled && this.props.status !== "loading" ?
                                (
                                    <div className={classes.loadMoreCell}>
                                        <CircularProgress />
                                    </div>)
                                :
                                showMore && this.props.status !== "loading" ?
                                    (
                                        <div>
                                            <Grid container spacing={40}>
                                                <Grid item xs={6}>
                                                    <div id={this.props.prefix + "-readmore"} className={classes.loadMoreCell}
                                                        onClick={() => {
                                                            this.onLoadMore(this.computePageNumber(this.props.rowsPerPage), false);
                                                        }}>
                                                        <Typography variant={"button"}
                                                            className={classes.loadMoreLabel}>
                                                            {getLabelFn ? getLabelFn('load_more_label', loadMoreLabel) : loadMoreLabel}
                                                        </Typography>
                                                        <Icon className={classNames(classes.loadMoreIcon,"icon-chevron-down")}></Icon>
                                                    </div>
                                                </Grid>
                                                <Grid item xs={6}>
                                                    <div id={this.props.prefix + "-readmore"} className={classes.loadMoreCell}
                                                        onClick={() => {
                                                            this.onLoadAll(this.computePageNumber(this.props.rowsPerPageAll), false);
                                                        }}>
                                                        <Typography variant={"button"}
                                                            className={classes.loadMoreLabel}>
                                                            {getLabelFn ? getLabelFn('load_all_label', loadAllLabel) : loadAllLabel}
                                                        </Typography>
                                                        <Icon className={classNames(classes.loadMoreIcon,"icon-chevron-down")}></Icon>
                                                    </div>
                                                </Grid>
                                            </Grid>
                                        </div>)
                                    :
                                    undefined
                        }
                    </Grid>
                </div>
            </ComponentProp>
        );
    }
}

const SymCustomTableWithStyles = withStyles(customTableStyles, { name: "MuiCustomTable" })(SymCustomTableClass);

export class SymCustomTable extends SymCustomTableWithStyles {
}
