import React from 'react';
import { withStyles } from "@ui-lib/core/styles";
import Grid from "@ui-lib/core/Grid";
import Icon from "@ui-lib/core/Icon";
import Checkbox from "@ui-lib/core/Checkbox";
import FormControlLabel from "@ui-lib/core/FormControlLabel";


const styles = theme => ({
  root: {
    padding: "10px 8px",
    zIndex: "10000",
    backgroundColor: "white",
    "& [class*=MuiCheckbox]":
    {
      borderColor: theme.palette.disableText
    },
    "& [class*=MuiCheckbox-checked]":
    {
      color: theme.palette.main,
      borderColor: theme.palette.main,
      "& [class*=MuiIconButton-label]":
      {
        backgroundColor: "#fff",

      }
    }
  },
  disabledItem:
  {
    "& [class*=MuiFormControlLabel-disabled]":
    {
      color: "#333!important"
    },
    "& [class*=MuiCheckbox-checked]":
    {
      borderColor: "#BDBDBD!important",
      color: "#fff!important",
      "& [class*=MuiIconButton-label]":
      {
        backgroundColor: "#BDBDBD!important",

      },
    }
  }
});


const ToggleItem = ({ classes, getLabelFn, ...props }) => {
  return (
    <Grid item xs={12} style={{}} key={props.recordIndex} className={props.disabled ? `${classes.root} ${classes.disabledItem}` : classes.root}>
      <FormControlLabel
        key={props.recordIndex}
        style={{ marginLeft: "0px" }}
        control={
          <Checkbox
            key={props.recordIndex}
            checkedIcon={<Icon style={{ fontSize: "16px" }}>done</Icon>}
            icon={false}
            checked={props.checkedVal}
            onChange={() => props.handleChange(props.filterKey)}
            disabled={props.disabled}
          />
        }
        label={getLabelFn ? getLabelFn(props.column.i18nKey, props.column.label) : props.column.label}
      />
    </Grid>
  )
}

export default withStyles(styles)(ToggleItem);
