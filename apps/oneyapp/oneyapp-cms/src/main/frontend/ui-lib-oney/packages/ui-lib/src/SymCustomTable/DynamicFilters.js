import React from 'react';
import PropTypes from 'prop-types';
import { filterStyles } from './styles';
import {withStyles} from "@ui-lib/core/styles";
import Typography from "@ui-lib/core/Typography";
import Grid from "@ui-lib/core/Grid";
import Collapse from "@ui-lib/core/Collapse";
import Button from "@ui-lib/core/Button";


class DynamicFilters extends React.Component {

  handleClick = (e) => {
    if (!(this.node && (this.node.contains(e.target)) ||
      (this.props.relatedIcon && (e.target.parentNode.id == this.props.relatedIcon)) ||
      e.target.id.indexOf("downshift-simple-item") > 0)) {
      this.props.hideCompFn();
    }
  };

  render() {

    const { getLabelFn } = this.props;

    return (
      <div ref={node => this.node = node}>
        <Collapse in={this.props.showFilter} timeout="auto" className={this.props.showFilter ? this.props.classes["allow-overflow"] : this.props.classes["not-allow-overflow"]}>
          <Grid container spacing={16} className={this.props.classes.root} id={this.props.prefix + "filter-root"}>
            <Grid item xs={12}>
              <Typography variant="h6">{getLabelFn ? getLabelFn("table_component_filter_title", "Filter") : "Filter"}</Typography>
            </Grid>
            <Grid item xs={9}>
              <Grid container spacing={40}>
                {this.props.filters}
              </Grid>
            </Grid>
            <Grid item xs={3} className={this.props.classes.buttons}>
              <div style={{ marginRight: "16px" }}>
                <Button variant={"outlined"} color={"secondary"} onClick={() => this.props.clearFilters()} className={this.props.classes.filterBtn}>
                  {getLabelFn ? getLabelFn("table_component_clear_filters_action", "Clear") : "Clear"}
                </Button>
              </div>
              <div>
                <Button variant={"raised"} color={"primary"} onClick={() => this.props.handleFilter()}> {getLabelFn ? getLabelFn("table_component_filter_action", "Filter") : "Filter"}
                </Button>
              </div>
            </Grid>
          </Grid>
        </Collapse>
      </div>
    );
  }
};

//const DynamicFilters = ({ classes, clearFilters, filters, handleFilter, showFilter }) => {};

DynamicFilters.propTypes = {
  clearFilters: PropTypes.func.isRequired,
  //filters: PropTypes.array.isRequired,
  handleFilter: PropTypes.func.isRequired,
  showFilter: PropTypes.bool.isRequired
};

export default withStyles(filterStyles, { name: "filters" })(DynamicFilters);
