//DOWNLOAD DATA TABLE AS CSV RELATED ACTIONS

//CREATE NEW ARRAY WITH ONLY TABLE VISIBLE DATA
export const getVisibleData = (allItems, cols) => {

  let items = [];

  for (let itemIndex = 0; itemIndex < allItems.length; itemIndex++) {

    let currentItem = allItems[itemIndex];

    //create new object with item current visible data 
    let visibleDataItem = {};

    for (let columnIndex = 0; columnIndex < cols.length; columnIndex++) {

      let currentColumn = cols[columnIndex];
      let currentColumnName = currentColumn.name;

      //check if there is a key-value pair in table data that matches the current visible column name
      let matchingKey = currentItem[currentColumnName];

      //if there is an item in data object that matches a visible column push key value pair to current data item
      if (matchingKey || matchingKey === 0 || matchingKey === null) {

        visibleDataItem[currentColumnName] = matchingKey;

      }
    }
    items.push(visibleDataItem)
  }
  return items;
}


//DOWNLOAD CSV FILE WITH VISIBLE DATA
export const downloadCSV = (fileTitle, allItems, cols) => {

  let items = getVisibleData(allItems, cols);

  const replacer = (key, value) => value === null ? '' : value // specify how you want to handle null values here
  const header = Object.keys(items[0])

  //convert items JSON array to CSV
  let csv = items.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','))
  csv.unshift(header.join(','))
  csv = csv.join('\r\n')

  //get exported file name
  let exportedFilename = fileTitle + '.csv' || 'export.csv';

  //convert CSV data to readable fiile
  var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });

  //download file
  if (navigator.msSaveBlob) { // IE 10+
    navigator.msSaveBlob(blob, exportedFilename);
  } else {
    var link = document.createElement("a");
    if (link.download !== undefined) { // feature detection
      // Browsers that support HTML5 download attribute
      var url = URL.createObjectURL(blob);
      link.setAttribute("href", url);
      link.setAttribute("download", exportedFilename);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
};

