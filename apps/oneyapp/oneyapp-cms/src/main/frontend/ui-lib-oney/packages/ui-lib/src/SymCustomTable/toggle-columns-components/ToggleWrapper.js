import React from 'react';
import {withStyles} from "@ui-lib/core/styles";
import Grid from "@ui-lib/core/Grid";
import ClickAwayListener from "@ui-lib/core/ClickAwayListener";

const styles = theme => ({
  root: {
    marginRight: "60px",
    width: "200px",
    float: "right",
    border: "1px solid #BDBDBD",
    backgroundColor: "#fff"
  }
});

class ToggleWrapper extends React.Component {

  render() {
    return (
      <ClickAwayListener onClickAway={() => this.props.hideColumnsSel()}>
        <div ref={node => this.node = node}>
          <Grid container direction="column" className={this.props.classes.root}> {this.props.children} </Grid>
        </div>
      </ClickAwayListener>
    )
  }
}

export default withStyles(styles, { name: "toggle-wrapper" })(ToggleWrapper);
