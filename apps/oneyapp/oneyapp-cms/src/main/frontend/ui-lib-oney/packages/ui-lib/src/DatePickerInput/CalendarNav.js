import React from 'react';


import Icon from "@ui-lib/core/Icon";
import {withStyles} from "@ui-lib/core/styles";
import classNames from "@ui-lib/core/classNames";

//import component styles
import {navStyles} from './styles';

//customize navbar for calendar
const CalendarNav = ({onPreviousClick, onNextClick, classes}) => {
  return (
    <div className={classes.navBar}>
      <Icon color="primary" className={classNames(classes.prevBtn)} onClick={() => onPreviousClick()}>keyboard_arrow_left</Icon>
      <Icon color="primary" className={classNames(classes.nextBtn)} onClick={() => onNextClick()}>keyboard_arrow_right</Icon>
    </div>
  );
};

export default withStyles(navStyles)(CalendarNav);
