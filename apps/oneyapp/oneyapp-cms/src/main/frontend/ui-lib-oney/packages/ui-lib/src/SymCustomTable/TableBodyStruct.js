import React from "react";
import { tableStyles } from "./styles";
import moment from "moment/moment.js";
import TableBody from "@ui-lib/core/TableBody";
import TableRow from "@ui-lib/core/TableRow";
import TableCell from "@ui-lib/core/TableCell";
import Checkbox from "@ui-lib/core/Checkbox";
import ClickAwayListener from "@ui-lib/core/ClickAwayListener";
import Icon from "@ui-lib/core/Icon";
import Button from "@ui-lib/core/Button";
import IconButton from "@ui-lib/core/IconButton";
import MenuList from "@ui-lib/core/MenuList";
import MenuItem from "@ui-lib/core/MenuItem";
import Paper from "@ui-lib/core/Paper";
import { withStyles } from "@ui-lib/core/styles";

import numeral from "numeral";

export const customTableStyles = tableStyles;

export class TableStructClass extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentActionsMenuIndex: -1,
            currentActionsMenuAnchor: null,
            actionsMenuOpened: false
        };
    }

    renderCell = (header, row, classes, rowIndex, index, onRowClick) => {
        let value = this.formatCell(header, row[header.name]);

        if (header.formatCellValue) {
            value = header.formatCellValue(row);
        }

        return (
            <TableCell
                style={{ textAlign: header.align ? header.align : "inherit" }}
                onClick={event => {
                    if (onRowClick) {
                        onRowClick(event, row);
                    }
                }}
                className={classes.headerCell}
                key={`r-${rowIndex}-c-${index}`}
            >
                {this.buildCellContent(classes, header, value, row)}
            </TableCell>
        );
    };

    /**
     * Apply a format from header to a cell value.
     */
    formatCell = (header, value) => {
        let formatedValue = value;

        if (header.applyDateFormat && value) {
            try {
                formatedValue = moment(value).format(header.applyDateFormat);
            } catch (e) {}
        }
        if (header.applyNumberFormat && value) {
            try {
                formatedValue = numeral(value).format(header.applyNumberFormat);
            } catch (e) {}
        }

        return formatedValue;
    };

    /**
     * Only reload table rows in the following situations:
     * - Changes in the table rows ;
     * - Changes in the table header structure ;
     * - When user click on an action
     */

    shouldComponentUpdate(nextProps, nextState) {
        let currentHeaderCols = [];
        let nextHeaderCols = [];

        for (
            let arrayIndex = 0;
            arrayIndex < this.props.headers.length;
            arrayIndex++
        ) {
            currentHeaderCols.push(this.props.headers[arrayIndex].name);
        }

        for (
            let arrayIndex = 0;
            arrayIndex < nextProps.headers.length;
            arrayIndex++
        ) {
            nextHeaderCols.push(nextProps.headers[arrayIndex].name);
        }

        if (
            nextProps.newDataRecords === this.props.newDataRecords &&
            JSON.stringify(currentHeaderCols) ===
                JSON.stringify(nextHeaderCols) &&
            this.state.currentActionsMenuIndex ===
                nextState.currentActionsMenuIndex &&
            this.props.selectedRows === nextProps.selectedRows
        ) {
            return false;
        } else {
            return true;
        }
    }

    onOpenActionsMenu = (event, index) => {
        this.setState({
            currentActionsMenuIndex: index,
            currentActionsMenuAnchor: event.currentTarget,
            actionsMenuOpened: true,
            timestamp: new Date().getTime()
        });
    };

    onCloseActionsMenu = () => {
        this.setState({
            currentActionsMenuIndex: -1,
            currentActionsMenuAnchor: null,
            actionsMenuOpened: false,
            timestamp: new Date().getTime()
        });
    };

    /**
     * Build cell content.
     */
    buildCellContent = (classes, header, value, row) => {
        let cellConfig = this.props.customCellsConfig;

        if (cellConfig && cellConfig()[header.name]) {
            return cellConfig()[header.name](value, row);
        } else {
            let truncatedValue = value;
            if (truncatedValue !== 0 && !truncatedValue) {
                truncatedValue = "";
            }

            if (header.maxChars) {
                if (truncatedValue.length > header.maxChars) {
                    truncatedValue =
                        truncatedValue.substring(0, header.maxChars) + "...";
                } else {
                    for (
                        let arrayIndex = 0;
                        arrayIndex < header.maxChars - truncatedValue.length;
                        arrayIndex++
                    ) {
                        truncatedValue += " "; // + truncatedValue;
                    }
                }
            }

            //This code is only used in report detail errors, in the future is to remove...
            let cellContent = (
                <span style={{ whiteSpace: "pre" }} title={value}>
                    {truncatedValue}
                </span>
            );

            if (
                this.props.conditionalPrefixes &&
                Object.keys(this.props.conditionalPrefixes).length > 0 &&
                (value !== undefined || value !== null)
            ) {
                let className = this.props.conditionalPrefixes[
                    header.name.toUpperCase() + "_" + ("" + value).toUpperCase()
                ];

                if (className) {
                    cellContent = (
                        <span
                            className={classes[className.toLowerCase()]}
                            title={value}
                        >
                            {value}
                        </span>
                    );
                } else if (
                    header.defaultConditionalPrefix &&
                    header.defaultConditionalPrefix.cssClass
                ) {
                    //OTHERS TEST
                    cellContent = (
                        <span
                            className={
                                classes[
                                    header.defaultConditionalPrefix.cssClass.toLowerCase()
                                ]
                            }
                            title={value}
                        >
                            {value}
                        </span>
                    );
                }
            }
            return cellContent;
        }
    };

    render() {
        const {
            classes,
            rowsSelectable,
            emptyText,
            onRowClick,
            actions,
            newDataRecords,
            classNames,
            allCols,
            selectedRows,
            getLabelFn,
            displayRowWarningCondition
        } = this.props;

        let disableMenu = rowInfo => {
            if (this.props.disableMenuCondition) {
                let key = this.props.disableMenuCondition.key;
                let value = this.props.disableMenuCondition.value;
                let disable = rowInfo[key] === value;

                return disable;
            } else {
                return false;
            }
        };
        return (
            <TableBody>
                {newDataRecords && newDataRecords.length > 0 ? (
                    newDataRecords.map((row, rowIndex) => {
                        let rowClasses = classNames(
                            onRowClick
                                ? `${classes.rowClickable} ${classes.row}`
                                : classes.row
                        );

                        let isSelected =
                            selectedRows.find(e => e.id === row.id) !==
                            undefined;

                        return (
                            <TableRow
                                className={
                                    displayRowWarningCondition &&
                                    displayRowWarningCondition(row)
                                        ? rowClasses + " " + classes.warningRow
                                        : rowClasses
                                }
                                id={`r-${row.id}`}
                                key={`r-${rowIndex}-${row.id}`}
                                selected={isSelected}
                                style={
                                    displayRowWarningCondition &&
                                    displayRowWarningCondition(row)
                                        ? { outline: "1px solid #FF6859" }
                                        : {}
                                }
                            >
                                {rowsSelectable && (
                                    <TableCell className={classes.checkboxCell}>
                                        <Checkbox
                                            checked={isSelected}
                                            onChange={event =>
                                                this.props.onToggleSelect(
                                                    event,
                                                    row
                                                )
                                            }
                                            icon={false}
                                            checkedIcon={
                                                <Icon
                                                    style={{ fontSize: "16px" }}
                                                >
                                                    done
                                                </Icon>
                                            }
                                        />
                                    </TableCell>
                                )}
                                {allCols.map((header, index) => {
                                    return this.renderCell(
                                        header,
                                        row,
                                        classes,
                                        rowIndex,
                                        index,
                                        onRowClick
                                    );
                                })}
                                {/*show row actions if prop is set to true*/}
                                {actions && actions.length > 0 && (
                                    <TableCell
                                        className={`${classes.actions} ${
                                            actions.length === 1
                                                ? classes.singleAction
                                                : ""
                                        }`}
                                    >
                                        {actions.length === 1 && (
                                            <Button
                                                className={
                                                    classes.singleOptionButton
                                                }
                                                size={"small"}
                                                variant={"outlined"}
                                                color={"secondary"}
                                                onClick={
                                                    actions[0].onActionClick &&
                                                    actions[0].onActionClick.bind(
                                                        this,
                                                        row
                                                    )
                                                }
                                                disabled={actions[0].disabled}
                                            >
                                                {actions[0]["label"]}
                                            </Button>
                                        )}
                                        {actions.length > 1 && (
                                            <span
                                                className={classes.optionsSpan}
                                            >
                                                <IconButton
                                                    onClick={event =>
                                                        this.onOpenActionsMenu.bind(
                                                            this
                                                        )(event, rowIndex)
                                                    }
                                                    disabled={
                                                        this.props
                                                            .disableMenuCondition
                                                            ? disableMenu(row)
                                                            : false
                                                    }
                                                >
                                                    <Icon
                                                        className={
                                                            this.state
                                                                .currentActionsMenuIndex ===
                                                            rowIndex
                                                                ? classes.optionsButtonSelected
                                                                : ""
                                                        }
                                                    >
                                                        more_vert
                                                    </Icon>
                                                </IconButton>
                                            </span>
                                        )}
                                        {this.state.currentActionsMenuIndex ===
                                            rowIndex && (
                                            <div
                                                className={classes.optionsList}
                                            >
                                                <ClickAwayListener
                                                    onClickAway={this.onCloseActionsMenu.bind(
                                                        this
                                                    )}
                                                >
                                                    <Paper>
                                                        <MenuList>
                                                            {actions.map(
                                                                (
                                                                    action,
                                                                    actionIndex
                                                                ) => {
                                                                    return (
                                                                        <MenuItem
                                                                            style={{
                                                                                justifyContent:
                                                                                    "left",
                                                                                padding:
                                                                                    "8px",
                                                                                fontFamily:
                                                                                    "SourceSansPro-Regular",
                                                                                fontSize:
                                                                                    "14px"
                                                                            }}
                                                                            onClick={
                                                                                action.onActionClick &&
                                                                                action.onActionClick.bind(
                                                                                    this,
                                                                                    row
                                                                                )
                                                                            }
                                                                            key={`action-${actionIndex}-for-row-${rowIndex}`}
                                                                        >
                                                                            {
                                                                                action.label
                                                                            }
                                                                        </MenuItem>
                                                                    );
                                                                }
                                                            )}
                                                        </MenuList>
                                                    </Paper>
                                                </ClickAwayListener>
                                            </div>
                                        )}
                                    </TableCell>
                                )}
                            </TableRow>
                        );
                    })
                ) : (
                    <tr className={classes.noDataRow}>
                        {this.props.status &&
                        this.props.status !== "undefined" &&
                        this.props.status === "loading" ? (
                            <td colSpan={"100%"}>
                                {" "}
                                {/* <CircularProgress className={classes.loader} />
                                Loading  */}
                            </td>
                        ) : this.props.status &&
                          this.props.status === "error" ? (
                            <td colSpan={"100%"}>
                                <Icon
                                    className={classNames(
                                        classes.loader,
                                        classes.errorIcon,
                                        "icon-warning"
                                    )}
                                />
                                {getLabelFn
                                    ? getLabelFn(
                                          "tables_load_fail",
                                          "Data failed to load"
                                      )
                                    : "Data failed to load"}{" "}
                            </td>
                        ) : (
                            <td colSpan={"100%"}>
                                {getLabelFn
                                    ? getLabelFn("tables_no_data", emptyText)
                                    : emptyText}
                            </td>
                        )}
                    </tr>
                )}
            </TableBody>
        );
    }
}

const TableStructClassWithStyles = withStyles(customTableStyles)(
    TableStructClass
);

export default class TableStructure extends TableStructClassWithStyles {}
