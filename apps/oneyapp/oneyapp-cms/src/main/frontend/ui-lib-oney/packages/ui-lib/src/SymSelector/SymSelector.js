import React from "react";
import classNames from "@ui-lib/core/classNames";
import { withStyles } from "@ui-lib/core/styles";
import Icon from "@ui-lib/core/Icon";
import InputLabel from "@ui-lib/core/InputLabel";
import Tooltip from "@ui-lib/core/Tooltip";

export const selectorStyles = (theme) => {

    return {
        root: {
            paddingLeft: 1,
            display: "inline-block"
        },
        label: {
            display: "block",
            marginBottom: 10
        },
        container: {
            display: "inline-block",
            whiteSpace: "no-wrap"
        },
        item: {
          color: "#333333",
          height: "24px",
          cursor: "pointer",
          display: "inline-block",
          position: "relative",
          fontSize: "13px",
          minWidth: "120px",
          textAlign: "center",
          lineHeight: "21px",
          marginLeft: "-1px",
          paddingTop: "2px",
          borderStyle: "solid",
          borderWidth: "1px",
          borderColor: "#E0E0E0",
          paddingLeft: "10px",
          paddingRight: "10px",
          backgroundColor: "#ffffff"
        },
        itemSelected: {
          color: "#333333",
          zIndex: "2",
          borderColor: "#ff994d",
          backgroundColor: "#ffe6d3"
        },
        itemIcon: {
            fontSize: 13,
            position: "absolute",
            lineHeight: "24px",
            top: 4,
            right: 5,
            display: "none!important"
        },
        itemDisabled: {
            cursor: "default",
            border: "1px solid #E0E0E0",
            backgroundColor: "#EEEEEE",
            color: "#AEAEAE"
        }
    };
};


class SymSelector extends React.Component {

    constructor(props) {
        super(props);
    
        this.state = {
          selectedIndex: null
        };
      }

    componentDidMount() {

        let index = false;
        let value = this.props.value;

        this.props.options.map((item, itemIndex) => {
            if (item.value === value) index = itemIndex;
        });

        this.setState({ selectedIndex: index });
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (this.props.value !== nextProps.value) {
            let index = false;
            let value = nextProps.value;
            this.props.options.map((item, itemIndex) => {
                if (item.value === value) {
                    index = itemIndex;
                }
            });
            if (this.state.selectedIndex !== index) {
                this.setState({ selectedIndex: index });
                this.props.onChange && this.props.onChange.bind(this, this.props.options[index], index)();
            }
        }
    }

    render() {

        const {
            classes,
            className: classNameProp,
            square,
            elevation,
            ...other
          } = this.props;


    const className = classNames(
        classes.root,
        classes[`shadow${elevation >= 0 ? elevation : 0}`],
        {
          [classes.rounded]: !square,
        },
        classNameProp,
      );
  

        return (
            <div className={className}>

                {this.props.label && (
                    <InputLabel htmlFor={this.props.id} className={classes.label}
                        required={this.props.required}>{this.props.label}</InputLabel>)}

                {this.props.options && <div className={classes.container}>
                    {this.props.options.map((item, index) => {

                        let selected = index === this.state.selectedIndex;
                        let classNames = classes.item + (selected ? " " + classes.itemSelected : "");
                        if (this.props.disabled && !selected) {
                            return;
                        }
                        if (item.hidden) {
                            return;
                        }
                        if (item.disabled) {
                            return (
                                <span key={"item" + this.props.id + "-" + index}
                                    className={classNames + " " + classes.itemDisabled}>
                                    {item.label}
                                    {selected && <Icon className={classes.itemIcon}>done</Icon>}
                                </span>
                            );
                        }
                        if (item.tooltip) {
                            return (
                                <Tooltip title={item.tooltip} placement="bottom">
                                    <span key={"item" + this.props.id + "-" + index}
                                        onClick={this.onItemClick.bind(this, index)}
                                        className={classNames}>
                                        {item.label}
                                        {selected && <Icon className={classes.itemIcon}>done</Icon>}
                                    </span>
                                </Tooltip>
                            )
                        }
                        return (
                            <span key={"item" + this.props.id + "-" + index}
                                onClick={this.onItemClick.bind(this, index)}
                                className={classNames}>
                                {item.label}
                                {selected && <Icon className={classes.itemIcon}>done</Icon>}
                            </span>
                        );
                    })}
                </div>}
            </div>
        );
    }

    onItemClick(index) {
        this.setState({ selectedIndex: index });
        this.props.onChange && this.props.onChange.bind(this, this.props.options[index], index)();
    }
}

export default withStyles(selectorStyles, { withName: "MuiSelector" })(SymSelector)