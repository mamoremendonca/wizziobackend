import React from 'react';
import PropTypes from 'prop-types';
import Downshift from 'downshift';
import TextField from "@ui-lib/core/TextField";
import Icon from "@ui-lib/core/Icon";
import MenuItem from "@ui-lib/core/MenuItem";
import Paper from "@ui-lib/core/Paper";
import {withStyles} from "@ui-lib/core/styles";
import { ComponentName, styles } from "./AutocompleteInputStyles";

function renderInput(inputProps) {
  const { InputProps, classes, ref, ...other } = inputProps;
  return (
    <TextField
      InputProps={{
        inputRef: ref,
        classes: {
          root: classes.inputRoot,
          input: classes.inputInput,
        },
        ...InputProps,
      }}
      {...other}
    />
  );
}

function renderSuggestion({ suggestion, index, itemProps, highlightedIndex, selectedItem }) {
  const isHighlighted = highlightedIndex === index;
  const isSelected = selectedItem ? ((selectedItem.key || '').indexOf(suggestion.key) > -1) : false;

  return (
    <MenuItem
      {...itemProps}
      key={suggestion.key}
      selected={isHighlighted}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400,
      }}
    >
      {suggestion.value}
    </MenuItem>
  );
}
renderSuggestion.propTypes = {
  highlightedIndex: PropTypes.number,
  index: PropTypes.number,
  itemProps: PropTypes.object,
  selectedItem: PropTypes.string,
  suggestion: PropTypes.shape({ label: PropTypes.string }).isRequired,
};


class AutocompleteInput extends React.Component {

  readOptions = (services, optionsUrl, dropdown, getLabelFn) => {

    let requestInst = {
      serviceRequest: {
        dropdown: dropdown
      }
    }

    if (services) {
      services.HttpClient.request({
        method: 'post',
        url: window.location.origin.replace("3000","4500") + optionsUrl,
        data: requestInst
      }).then(response => {

        let suggestionsArray = [];
	      let tempResp = (response.data ? response.data : response);

        if (tempResp.responseObtained && tempResp.responseObtained.data) {
          for (let arrayIndex = 0; arrayIndex < tempResp.responseObtained.data.length; arrayIndex++ ) {
            let optionI18nKey = (tempResp.responseObtained.data[arrayIndex].key+"_description").toLowerCase().replace(/ /g, '_');
            let optionI18nValue = getLabelFn(optionI18nKey, tempResp.responseObtained.data[arrayIndex].value);

            tempResp.responseObtained.data[arrayIndex].value = optionI18nValue;
          }
          suggestionsArray = tempResp.responseObtained.data;

          this.setState({
            suggestions: suggestionsArray
          });

        }
      }).catch(err => {
          console.log(err);
        });
    };
  };

  constructor(props) {
    super(props);

    let tempOptions = [];
    if (props.options && props.options.length > 0) {
      tempOptions = props.options;
    }

    this.state = {
      suggestions: tempOptions,
      resetFlag: false,
      currSelectedKey: ""
    }

    if (props.optionsURL && props.dropdown) {
      this.readOptions(props.servicesApi, props.optionsURL, props.dropdown, (key, defaultLabel) => this.props.getLabelFn(key, defaultLabel));
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.defaultValue === "") {
      this.setState({ resetFlag: true })
    }
  }

  componentDidMount() {

  }

  render() {
    const { classes } = this.props;

    const getSuggestions = (suggestions, value) => {

      let inputValue = "";
      
      if (value) {
        inputValue = value.trim().toLowerCase();
      }
      
      let matchedSuggestions = [];

      for (let suggestionIndex = 0; suggestionIndex < suggestions.length; suggestionIndex++) {
        let currSuggestion = suggestions[suggestionIndex];

        if (inputValue.length === 0 || (currSuggestion.value.trim()).toLowerCase().indexOf(inputValue) >= 0) {
          matchedSuggestions.push(currSuggestion);
        }

        if (this.props.maxVisible && ((this.props.maxVisible - 1) <= suggestionIndex)) {
          break;
        }
      }
      return matchedSuggestions;
    };

    const applyReset = (clearSelectionFn) => {
      if (this.state.resetFlag && (this.state.currSelectedKey !== this.props.defaultValue)) {
        clearSelectionFn();
        this.setState({
          resetFlag: false,
          currSelectedKey: ""
        });
      }
    };

    const applySelection = (selection) => {
      if (selection) {
        this.setState({
          currSelectedKey: selection.key,
          resetFlag: false
        });
        this.props.setValueFn(selection.key);
      }
    };

    return (
      <div className={classes.root}>
        <Downshift
          itemToString={item => (item ? item.value : '')}
          onSelect={selection => applySelection(selection)}
          id={this.props.prefix ? (this.props.prefix + "-downshift-simple") : "downshift-simple"}
        >

          {({
            getInputProps,
            getItemProps,
            getMenuProps,
            highlightedIndex,
            inputValue,
            isOpen,
            selectedItem,
            clearSelection,
            openMenu
          }) => (
              <div className={classes.container}>
                {
                  applyReset(clearSelection)
                }
                {renderInput({
                  fullWidth: true,
                  classes,
                  label: this.props.label,
                  InputLabelProps: { shrink: true },
                  placeholder: `${this.props.getLabelFn ? this.props.getLabelFn('table_filters_insert_sring', 'Insert') : 'Insert'}  ${this.props.label ? this.props.label.toLowerCase() : ' '}...`,
                  InputProps: getInputProps({
                    onFocus: (e) => {
                      openMenu();
                    },
                    onClick : (e) => {
                      if (!this.state.suggestions || this.state.suggestions.length === 0) {
                        this.readOptions(
                          this.props.servicesApi, 
                          this.props.optionsURL, 
                          this.props.dropdown, 
                          (key, defaultLabel) => this.props.getLabelFn(key, defaultLabel));
                      }
                    },
                    onChange: e => {
                      if (e.target.value === '' || this.props.defaultValue === '') {
                        clearSelection();
                        this.setState({
                          resetFlag: false,
                          currSelectedKey: ""
                        });
                        if (this.props.setValueFn) {
                          this.props.setValueFn("");
                        }
                      }
                    },
                  }),
                })}
                <Icon 
                  color={"primary"} 
                  style={{ position: 'relative', float: 'right', top: "-28px", fontSize: "20px" }}
                  className={"icon-chevron-down " + classes.iconColor}
                  />
                <div {...getMenuProps()}>
                  {isOpen ? (
                    <Paper className={classes.paper} square>
                      {getSuggestions(this.state.suggestions, inputValue).map((suggestion, index) =>
                        renderSuggestion({
                          suggestion,
                          index,
                          itemProps: getItemProps({ item: suggestion }),
                          highlightedIndex,
                          selectedItem
                        }),
                      )}
                    </Paper>
                  ) : null}
                </div>
              </div>
            )}
        </Downshift>
      </div>
    );
  };
};

/*AutocompleteInput.propTypes = {
  classes: PropTypes.object.isRequired,
};*/


/*const AutocompleteInputWithStyles = withStyles(styles)(AutocompleteInputClass);

export class AutocompleteInput extends AutocompleteInputWithStyles { }*/


export default withStyles(styles, { withName: ComponentName })(AutocompleteInput);