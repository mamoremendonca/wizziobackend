import React from 'react';
import DayPicker  from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import PropTypes from 'prop-types';
import moment from 'moment'
import CalendarNav from '../DatePickerInput/CalendarNav';
import Icon from "@ui-lib/core/Icon";
import TextField from "@ui-lib/core/TextField";
import ClickAwayListener from "@ui-lib/core/ClickAwayListener";
import Selector from "../SymSelector";
import { withStyles } from "@ui-lib/core/styles";
//import component styles
import { calendarStyles } from './styles';

//customize calendar day rendering
function renderDay(day) {
    const date = day.getDate();
    return <span>
        {date}
    </span>;
}

class DatePickerInputClass extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            visibleCalendar: false,
            selectedDays: null,
            isUnlimited: this.props.unlimitedByDefault && this.props.unlimitedByDefault
        }

    }

    updateSelectDays = (selectedDaysVal) => {
        let tempSelectedDaysVal = null;
        if (selectedDaysVal instanceof Date) {
            tempSelectedDaysVal = selectedDaysVal;
        }
        else {
            if (selectedDaysVal) {
                try {
                    tempSelectedDaysVal = new Date(selectedDaysVal);
                }
                catch (e) {
                    tempSelectedDaysVal = selectedDaysVal;
                }
            }
        }

        this.setState({
            selectedDays: tempSelectedDaysVal
        });
    };

    componentDidMount() {
        if (this.props.currentFromFieldValue) {
            this.updateSelectDays(moment(this.props.currentFromFieldValue, 'DD-MM-YYYY').format('MM-DD-YYYY'));
        }
    }

    //set current selected day to currentFromFieldValue prop
    componentWillReceiveProps(nextProps) {

        if ((this.props.currentFromFieldValue && this.props.currentFromFieldValue === null &&
            this.props.currentFromFieldValue !== nextProps.currentFromFieldValue)
            ||
            (!this.props.currentFromFieldValue && this.props.forTableFilter)) {
            this.updateSelectDays(nextProps.currentFromFieldValue);
        }
        if (this.props.isUnlimited && this.props.isUnlimited === null && this.props.isUnlimited !== nextProps.isUnlimited) {
            this.setState({
                isUnlimited: nextProps.unlimitedByDefault,
            })
        }
    }

    /**
     * Handle click operation in the click icon.
     */
    handleCalendarIconOnClick() {

        if (this.props.onlyCalendar || this.props.onlySingleDay) {
            this.setState({
                visibleCalendar: !this.state.visibleCalendar
            });
        } else {
            this.setState({ visibleCalendar: false });
        }
    }

    /**
     * Handle day click in the calendar.
     * @param {*} selectedDate
     */


    handleClick(day, modifiers) {

        if (modifiers && modifiers.disabled) {
            return;
        }

        this.updateSelectDays(day);
        this.handleClickAway();

        if (this.props.handleDayClick) {
            let currDate = moment(new Date(day)).format(this.props.applySpecificFormat);
            this.props.handleDayClick(currDate);
        }
    }

    capitalizeFirstLetter = (string) => {
        return string ? (string
            .charAt(0)
            .toUpperCase() + string.slice(1)) : "";
    }

    showDatePresentation = (dateStr) => {
        let datePresentation = "";
        if (dateStr) {
            datePresentation = moment(new Date(dateStr)).format(this.props.applySpecificFormat);
        }
        return <TextField
            onFocus={this.props.onFocus ? (event) => this.props.onFocus() : null}
            fullWidth
            InputLabelProps={{ shrink: this.props.isToggle ? this.props.isToggle : true }}
            id={this.props.prefix + "-day-picker-text-input"}
            key={this.props.prefix + "-day-picker-text-input"}
            label={!this.props.isToggle
                ? this.capitalizeFirstLetter(this.props.label)
                : null}
            margin="normal"
            placeholder={`${this.props.getLabelFn ? this.props.getLabelFn('table_filters_insert_sring', 'Insert') : 'Insert'}  ${this.props.label ? this.props.label.toLowerCase() : ' '}...`}
            value={datePresentation} />
    };

    handleClickAway = () => {
        this.setState({ visibleCalendar: false });
    }

    onKeyPressed = (key) => {
        if (key.key === "Delete" || key.key === "Backspace") {
            this.handleClick(null)
            if (this.props.handleDayClick) {
                this.props.handleDayClick(null);
            }
        }
    }

    handleValueChange = (value) => {
        this.setState({
            isUnlimited: value.value === 1 ? true : false,
            selectedDays: value.value === 1 ?
                moment(this.props.unlimitedDate, 'DD-MM-YYYY').format('MM-DD-YYYY')
                :
                this.props.defaultLimitedDate ?

                    moment(this.props.defaultLimitedDate, 'DD-MM-YYYY').format('MM-DD-YYYY')

                    : null
        })
        this.props.onChangeToggleValue(value)
    }

    render() {

        const { prefix, isToggle } = this.props;

        const classes = this.props.classes;
        const WEEKDAYS_LONG = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday'
        ];

        /* let selectedDays = this.props.currentFromFieldValue ? new Date(this.props.currentFromFieldValue) :  new Date(); //, to }; */

        //console.log(this.state.selectedDays instanceof Date);

        const modifiers = {
            start: this.state.selectedDays,
            end: this.state.selectedDays
        };

        const WEEKDAYS_SHORT = [
            'S',
            'M',
            'T',
            'W',
            'T',
            'F',
            'S'
        ];
        return <ClickAwayListener
            onClickAway={() => this.handleClickAway()}
            key={`${prefix} -wrapper`}>
            <div
                onKeyDown={(key) => this.onKeyPressed(key)}
                id={`${prefix} -wrapper`}
                className={classes.root}>
                {isToggle &&
                    <Selector
                        label={this.capitalizeFirstLetter(this.props.label)}
                        selectedValue={this.props.unlimitedByDefault ? 1 : 2}
                        className={isToggle && isToggle ? classes.withToggle : ''}
                        options={[
                            {
                                label: this.props.selectorLabel1,
                                value: 1,
                                onClick: (option) => this.handleValueChange(option.value)
                            }, {
                                label: this.props.selectorLabel2,
                                value: 2,
                                onClick: (option) => this.handleValueChange(option.value)
                            }
                        ]} />}

                {!isToggle || (isToggle && !this.state.isUnlimited)
                    ? <div>
                        <div onClick={() => this.handleCalendarIconOnClick()}>
                            {this.showDatePresentation(this.state.selectedDays)}
                            <Icon
                                color={"primary"}
                                style={{
                                    position: 'relative',
                                    float: 'right',
                                    top: "-28px",
                                    fontSize: "20px",
                                }}>date_range</Icon>
                        </div>
                        <div className={classes['modal-section']}>

                            <div
                                style={{
                                    display: (this.state.visibleCalendar
                                        ? 'block'
                                        : 'none'),
                                    height: '300px',
                                    position: 'relative'
                                }}>
                                <DayPicker
                                    locale="pt"
                                    weekdaysLong={WEEKDAYS_LONG}
                                    weekdaysShort={WEEKDAYS_SHORT}
                                    onDayClick={(selectedDate, modifiers) => this.handleClick(selectedDate, modifiers)}
                                    fixedWeeks={false}
                                    disabledDays={this.props.disabledDays ? this.props.disabledDays : null}
                                    selectedDays={this.state.selectedDays}
                                    className={classes.calendar}
                                    renderDay={renderDay}
                                    modifiers={modifiers}
                                    month={this.props.month ? this.props.month : null}
                                    navbarElement={<CalendarNav />}
                                />
                            </div>
                        </div>
                    </div>
                    : ''}
            </div>
        </ClickAwayListener>
    }
};

DatePickerInputClass.defaultProps = {
    isToggle: false,
    selectorLabel1: 'Unlimited',
    selectorLabel2: 'Specific date',
    unlimitedByDefault: true,
    // unlimitedDate: '12/31/9999',
    // defaultLimitedDate:  moment(new Date()).add(2,'years').format('MM-DD-YYYY')
};

DatePickerInputClass.propTypes = {
    label: PropTypes.string,
    currentFromFieldValue: PropTypes.string,
    applySpecificFormat: PropTypes.string,
    onlyCalendar: PropTypes.bool,
    onlySingleDay: PropTypes.bool,
    handleDayClick: PropTypes.func,
    prefix: PropTypes.string,
    unlimitedDate: PropTypes.string,
    defaultLimitedDate: PropTypes.string
};

const DatePickerInputWithStyles = withStyles(calendarStyles)(DatePickerInputClass);

export class DatePickerInput extends DatePickerInputWithStyles { }
