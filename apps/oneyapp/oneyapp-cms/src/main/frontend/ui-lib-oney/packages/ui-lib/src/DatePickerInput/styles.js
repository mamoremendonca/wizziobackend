export const calendarStyles = theme => ({
  'root':
  {
    /* maxWidth: 250, */
    width: '100%',
    position: 'relative',
    cursor: 'pointer',
    zIndex: 100,
    "& [class*=MuiFormControl]":
    {
      marginBottom: "-1px",
      marginTop: "18px"
    },
    "& [class*=MuiSelector-root]": {
      width: '100%',
      "& [class*=MuiSelector-label]":{
        marginBottom: "2px"
      }
    },
    "& [class*=MuiSelector-container]": {
      width: '100%',
      whiteSpace: 'nowrap'
    },
    "& [class*=MuiSelector-item]": {
      width: '50%',
      borderColor: '#E0E0E0',
      paddingTop: "2px",
      textAlign: 'center',
      display: 'inline-block',
      paddingLeft: "0px",
      paddingRight: "0px",
      "& [class*=MuiIcon-root]": {display: 'none'}
    },
    "& [class*=MuiSelector-itemSelected]": {
      backgroundColor: '#FFE6D3',
      borderColor: theme.palette.primary.main,
      zIndex: 2
    },
  },
  withToggle: {
    marginTop: "16px"
  },
  'root-for-filter':
  {
    zIndex: 1,
  },
  'selected-date-section': {
    width: '100%',
    padding: '7px 0px',
    borderBottom: `1px solid ${theme.palette.text.disabled}`,
    '& div':
    {
      display: "inline-block"
    }
  },
  'modal-section': {
    width: '100%',
    position: 'absolute',
  },
  'vertical-menu': {
    border: `1px solid ${theme.palette.text.disabled}`,
    padding: '0px 0px',
    margin: '0px 0px',
    backgroundColor: "white",
    '& li': {
      backgroundColor: "white",
      color: 'black',
      display: 'block',
      padding: '5px 8px 5px',
      textDecoration: 'none',
      '&:hover': {
        backgroundColor: '#ccc'
      }
    }
  },
  calendarWrapper: {
    width: '100%',
    display: 'inline-block',
    minWidth: "228px"
  },
  calendarHeader: {
    borderBottom: '1px solid #eaeaea',
    padding: "5px",
    backgroundColor: 'rgba(240, 240, 240)'
  },
  calendar: {
    backgroundColor: 'white',
    border: '1px solid #BDBDBD',
    width: '100%',
    maxWidth: '250px',
    minWidth: '228px',
    '& .DayPicker-Caption': {
      textAlign: 'center',
      marginBottom: "22px",
      '& > div':
      {
        fontSize: "18px"
      }
    },
    '& .DayPicker-Month': {
      borderCollapse: 'separate',
      borderSpacing: "4px",
      margin: "0px",
      width: '100%'
    },
    '& [class*=DayPicker-Day]': {
      width: 'calc(14% - 8px)',
      padding: "0px",
      lineHeight: "0px",
      outline: 'none!important',
      position: 'relative',
      '&::after': {
        content: '""',
        display: 'block',
        marginTop: 'calc(100% - 1px)'
      },
      '&:not(.DayPicker-Day--selected):not(.DayPicker-Day--outside):hover': {
        backgroundColor: "rgb(246, 246, 246)!important"
      },
      '& span': {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translateX(-50%) translateY(-50%)',
        width: "16px",
        textAlign: 'center',
        fontSize: "14px"
      }
    },
    '& .DayPicker-Day--important:not(.DayPicker-Day--disabled):not(.DayPicker-Day--outside):not(.DayPicker-Day--selected):not(.DayPicker-Day--today)': {
      backgroundColor: `transparent!important`,
      color: '#333333',
      '&::before': {
        content: '""',
        width: "6px",
        height: "6px",
        borderRadius: 5,
        backgroundColor: `${theme.palette.primary.main}!important`,
        position: 'absolute',
        bottom: 2,
        left: '50%',
        transform: 'translateX(-50%)'
      }
    },
    '& .DayPicker-Day--today:not(.DayPicker-Day--selected)': {
      color: `${theme.palette.primary.main}!important`,
      '&::before': {
        content: '""',
        width: '100%',
        height: '100%',
        position: 'absolute',
        border: `1px solid ${theme.palette.primary.main}!important`,
        left: "0px",
        top: "0px",
        borderRadius: '50%',
        boxSizing: 'border-box'
      }
    },
    '& .DayPicker-Day--selected:not(.DayPicker-Day--outside)': {
      backgroundColor: 'transparent !important',
      color: '#fff!important',
      '&::before': {
        content: '""',
        width: '100%',
        height: '100%',
        position: 'absolute',
        backgroundColor: `${theme.palette.primary.main}!important`,
        top: "0px",
        borderRadius: '100%',
        boxSizing: 'border-box',
        opacity: '1',
        left: "0px"
      },
      '&.DayPicker-Day--end': {
        backgroundColor: `${theme.palette.primary.main}!important`,
        '&::before': {
          width: '50%',
          left: 0
        }
      },
      '&.DayPicker-Day--start': {
        backgroundColor: `${theme.palette.primary.main}!important`,
        '&::before': {
          width: '50%',
          left: '50%'
        },
        '&:not:(:last-of-type)::before': {
          display: 'none'
        },
        '& + .DayPicker-Day--selected:not(.DayPicker-Day--end)': {
          '&::before': {
            width: 'calc(150% + 2px)',
            left: '-50%'
          }
        }
      },
      '&.DayPicker-Day--start.DayPicker-Day--end::before': {
        display: 'none'
      }
    },
    '& [class*=DayPicker-Weekday]': {
      width: '14%',
      '& abbr':
      {
        fontSize: "14px"
      }
    },
    '& .DayPicker-wrapper': {
      outline: 'none!important',
      padding: '25px 16px!important'
    },
    '& .DayPicker-NavButton': {
      outline: 'none!important'
    }
  }
});

export const navStyles = theme => ({
  navBar: {
    width: "0px",
    position: 'absolute',
    transform: 'translateX(-50%)',
    left: '50%',
    '& span': {
      position: 'absolute',
      top: "2px",
      cursor: 'pointer',
      fontSize: 20
    }
  },
  prevBtn: {
    float: 'left',
    left: '-85px',
    cursor: 'pointer',
  },
  nextBtn: {
    float: 'right',
    right: '-85px',
    cursor: 'pointer',
  }
});
