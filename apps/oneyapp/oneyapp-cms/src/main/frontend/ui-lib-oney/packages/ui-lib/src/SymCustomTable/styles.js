export const tableStyles = theme => ({
    root: {
        marginTop: 40,
        "& [class*=MuiTable]": {
            fontFamily: "SourceSansPro-Regular"
        },
        "& [class*=MuiTableCell-head]": {
            fontFamily: "Bariol-Bold",
            fontSize: 16,
            color: theme.palette.primary.main
        },
        "& [class*=MuiTableCell-root]": {
            padding: "0px 10px",
            borderBottom: 'none'
        }
    },
    row: {
        "& td:first-of-type": {
            paddingLeft: "15px"
        },
        "& td:last-of-type": {
            paddingRight: "15px"
        },
        "&:nth-child(even)": {
            backgroundColor: "#fff",
            "&:hover": {
                backgroundColor: "#dedede"
            }
        },
        "&:nth-child(odd)": {
            // backgroundColor: theme.palette.common.bg,
            backgroundColor: '#f5f5f5',
            "&:hover": {
                backgroundColor: "#dedede"
            }
        }
    },
    rowClickable: {
        cursor: "pointer",
        "&:hover": {
            backgroundColor: "#dedede"
        }
    },
    checkboxCell: {
        paddingRight: "0px",
        paddingLeft: "15px!important",
        width: "56px",
        textAlign: "center"
    },
    actionsCell: {
        textAlign: "right",
        paddingRight: "8px !important",
        "& [class*=MuiIconButton]": {
            width: "28px",
            height: "28px"
        }
    },
    optionsButtonSelected: {
        color: theme.palette.primary.main
    },
    optionsSpan: {
        float: "right",
        "& [class*=MuiIconButton-root]": {
            "&:hover": {
                background: "transparent"
            },
            "& [class*=MuiIcon-root]": {
                fontSize: "24px !important",
                "&:hover": {
                    color: theme.palette.primary.main
                }
            }
        }
    },
    optionsList: {
        "& [class*=MuiMenuItem-root]": {
            backgroundColor: "#fff",
            height: "14px",
            "&:hover": {
                backgroundColor: "#f5f5f5"
            }
        },
        "& [class*=MuiPaper-root]": {
            backgroundColor: "#fff",
            border: "1px solid #ddd",
            borderRadius: "0px",
            padding: "0px"
        },
        "& [class*=MuiList-root]": {
            marginTop: "8px"
        },
        position: "absolute",
        top: "33px",
        right: "37px",
        zIndex: 999,
        minWidth: "140px"
    },
    loadMoreCell: {
        cursor: "pointer",
        border: `1px solid ${theme.palette.primary.main}`,
        display: "flex",
        marginTop: "26px",
        transition: "all 0.3s ease-out",
        alignItems: "center",
        justifyContent: "center",
        padding: "11px !important",
        backgroundColor: "transparent"
    },
    loadMoreIcon: {
        marginLeft: "10px",
        color: theme.palette.primary.main
    },
    loadMoreLabel: {
        color: theme.palette.primary.main
    },
    headerSorted: {
        opacity: 1,
        "& [class *=MuiCustomTable-placeholderIcon]": {
            display: "none"
        }
    },
    headerNonSorted: {
        opacity: 0.9,
        "& [class *=MuiCustomTable-placeholderIcon]": {
            opacity: 0
        },
        "&:hover": {
            "& [class *=MuiCustomTable-placeholderIcon]": {
                opacity: 1,
                transition: "opacity 0.3s ease-out"
            }
        }
    },
    counter: {
        textAlign: "right",
        width: "100%",
        marginBottom: "14px !important",
        marginTop: "24px"
    },
    filterIcon: {
        fontSize: 24,
        cursor: "pointer",
        marginLeft: "20px",
        "&:hover": {
            color: theme.palette.primary.main
        }
    },
    filterIconSelected: {
        fontSize: "24px",
        marginLeft: "20px",
        cursor: "pointer",
        color: theme.palette.primary.main,
        "&:hover": {
            color: theme.palette.primary.main
        }
    },
    sortIcon: {
        fontSize: "14px",
        marginLeft: "5px",
        position: "absolute",
        left: "100%",
        top: "initial",
        bottom: "2px"
    },
    hideColumnIcon: {
        marginRight: "10px",
        position: "absolute",
        right: "100%",
        top: "initial",
        bottom: "2px"
    },
    placeholderIcon: {
        display: "inlineBlock"
    },
    sortIconNumber: {
        marginRight: "16px",
        position: "absolute",
        right: "100%",
        top: "-3px"
    },
    filters: {
        "& [class*=MuiFormControlLabel]": {
            fontSize: "14px"
        },
        "& [class*=ilters-root]": {
            backgroundColor: "#F5F5F5"
        }
    },
    noDataRow: {
        height: "45px",
        "& td": {
            padding: "8px 10px",
            backgroundColor: "#fff",
            fontSize: "14px"
        }
    },
    loader: {
        width: "24px !important",
        height: "24px !important",
        verticalAlign: "middle"
    },
    errorIcon: {
        color: theme.palette.negative,
        marginRight: "5px"
    },
    critical: {
        "&::before": {
            content: '""',
            height: "8px",
            width: "8px",
            backgroundColor: "#FF6859",
            display: "inline-block",
            borderRadius: "0px",
            margin: " 0px 8px 1px 0px"
        }
    },
    active: {
        "&::before": {
            content: '""',
            height: "8px",
            width: "8px",
            backgroundColor: "#1EB980",
            display: "inline-block",
            borderRadius: "50%",
            margin: " 0px 8px 1px 0px"
        }
    },
    inactive: {
        "&::before": {
            content: '""',
            height: "8px",
            width: "8px",
            backgroundColor: "#FF6859",
            display: "inline-block",
            borderRadius: "50%",
            margin: " 0px 8px 1px 0px"
        }
    },
    "non-critical": {
        "&::before": {
            content: '""',
            height: "8px",
            width: "8px",
            backgroundColor: "#FFC107",
            display: "inline-block",
            borderRadius: "50%",
            margin: " 0px 8px 1px 0px"
        }
    },
    other: {
        "&::before": {
            content: '""',
            height: "8px",
            width: "8px",
            border: `2px solid #9C34DD`,
            display: "inline-block",
            margin: " 0px 8px 1px 0px",
            borderRadius: "50%"
        }
    },
    single_event: {
        "&::before": {
            content: '""',
            height: "8px",
            width: "8px",
            backgroundColor: "#38ABFF",
            display: "inline-block",
            borderRadius: "50%",
            margin: " 0px 8px 1px 0px"
        }
    },
    every_year: {
        "&::before": {
            content: '""',
            height: "8px",
            width: "8px",
            backgroundColor: "#9C34DD",
            display: "inline-block",
            borderRadius: "50%",
            margin: " 0px 8px 1px 0px"
        }
    },
    tableLoader: {
        width: "100%",
        height: "calc(100% - 28px)",
        top: "28px",
        backgroundColor: "rgba(255, 255, 255, 0.7)",
        position: "absolute",
        zIndex: 1000,
        alignItems: "center",
        display: "flex",
        justifyContent: "center"
    },
    loaderHasSubheaders: {
        height: "calc(100% - 58px)",
        top: "58px"
    },
    rowTooltips: {
        color: "#fff",
        fontSize: "14px",
        padding: "4px"
    },
    headerCell: {
        "& [class*=MuiTooltip-tooltip]": {
            opacity: 1,
            backgroundColor: "#f00"
        },
        "& [class*=MuiTooltip-open]": {
            opacity: 1,
            backgroundColor: "#f00"
        }
    },
    singleAction: {
        width: "auto!important",
        height: "initial",
        padding: "0px 16px 0px 16px !important",
        "& button": {
            width: "auto!important",
            height: "initial",
            marginRight: "10px",
            fontSize: "16px",
            float: "right"
        }
    },
    warningRow: {
        backgroundColor: "#FFF0EE!important"
    }
});

export const filterStyles = theme => ({
    "allow-overflow": {
        overflow: "initial"
    },
    "not-allow-overflow": {
        overflow: "hidden"
    },
    root: {
        backgroundColor: "#fff",
        margin: 0,
        marginBottom: "25px",
        marginTop: "25px",
        width: "100%",
        padding: "24px 16px",
        paddingBottom: "16px"
    },
    buttons: {
        display: "flex",
        alignContent: "flex-end",
        justifyContent: "flex-end",
        paddingBottom: "16px!important",
        "& div": {
            marginTop: "auto"
        }
    },
    filterBtn: {
        border: 0
    },
    actions: {
        position: "absolute",
        padding: "0px !important",
        right: "0px",
        "& button": {
            width: "43px",
            height: "43px"
        }
    },
    singleAction: {
        width: "auto!important",
        height: "initial",
        padding: "0px 10px 0px 10px!important",
        "& button": {
            width: "auto!important",
            height: "initial",
            marginTop: "6px",
            fontSize: "16px",
            float: "right"
        }
    }
});
