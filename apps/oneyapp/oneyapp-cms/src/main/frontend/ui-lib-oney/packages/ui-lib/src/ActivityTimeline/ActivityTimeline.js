import React from 'react';
import {withStyles} from "@ui-lib/core/styles";
import Grid from "@ui-lib/core/Grid";
import Typography from "@ui-lib/core/Typography";
import Icon from "@ui-lib/core/Icon";
import ExpansionPanel from "@ui-lib/core/ExpansionPanel";
import ExpansionPanelSummary from "@ui-lib/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@ui-lib/core/ExpansionPanelDetails";
import moment from 'moment'
import {styles} from './styles'



class ActivityTimeline extends React.Component {


  render() {


    const { classes, data } = this.props;
    return (
      <div className={classes.root}>
        <Grid container style={{backgroundColor: "#f5f5f5", padding: "40px "}}>
            <Grid item xs={12} style={{ position: 'relative' }}>
              <div className={classes.mapContainer}>
              {data.content.map((reports, index) => {
                return (
                  <div key={index} className={classes.expansions}>
                      <div className={classes.contentContainer}>
                        <div className={classes.dataContainer}>
                          {reports.icon ?
                          <div className={reports.selected ? `${classes.selectedBallIcon} ${classes.ballIcon}` : classes.ballIcon}>
                            <Icon>{reports.icon}</Icon>
                          </div>
                          :
                          <div className={reports.selected ? `${classes.selectedBallIcon} ${classes.ballIcon}` : classes.ballIcon}>
                            <Icon>lens</Icon>
                          </div>
                          }
                        </div>
                        <div className={classes.vl} style={{borderStyle: reports.borderStyle, display: (index === data.content.length-1 ? 'none' : "inheirt")}}>
                        </div>
                        <div className={classes.infoContainer}></div>
                      </div>
                    <ExpansionPanel  key={index} className={index === 0 ? classes.firstEventItem : classes.eventItem}>
                    <div className={classes.arrowLeft}></div>

                      <ExpansionPanelSummary expandIcon={<Icon>expand_more</Icon>}>
                        <div>
                        <Typography variant='h5' style={{marginBottom: 10}}>{reports.title}</Typography>
                          {reports.annexes && reports.annexes.length > 0 && 
                            <div className={classes.annexIconContainer}>
                              <Icon className={classes.annexExpandIcon}>attachment_outline</Icon>
                              <div className={classes.annexExpandIconBall}>
                                <p>{reports.annexes.length}</p>
                              </div>
                            </div>
                          }
                          {reports.user && reports.date && <div style={{display: "inline-flex", marginBottom: 5}}>
                            <span style={{position: 'relative', color: "#8d8d8d"}}>
                              <Typography>{moment(reports.date).format('DD/MM/YYYY')}</Typography>
                            </span>
                              <div style={{height: 4, width: 4, borderRadius: '50%', backgroundColor: '#8d8d8d', alignSelf: "center", margin: '0 15px'}}>
                              </div>
                            <span style={{position: 'relative', color: "#8d8d8d"}}>
                             <Typography>{reports.user}</Typography>
                            </span>
                          </div>}
                          {reports.date && !reports.user && <div style={{position: 'relative', color: "#8d8d8d", display: "inline-flex", marginBottom: 5}}><Typography>{moment(reports.date).format('DD/MM/YYYY')}</Typography></div>}
                          {reports.user && !reports.date && <div style={{position: 'relative', color: "#8d8d8d", display: "inline-flex", marginBottom: 5}}><Typography>{reports.user}</Typography></div>}
                          {reports.items && reports.items.length > 0 && reports.items.map((item, index) => {
                            return (
                            <div style={{color: "#8d8d8d", marginBottom: 5}}><Typography>{item.header}: {item.content}</Typography></div>
                          )})}

                        </div>
                      </ExpansionPanelSummary >
                      <ExpansionPanelDetails style={{display: 'block'}}>
                      
                      {reports.annexes && reports.annexes.length > 0 && reports.annexes.map((annex, index) => {
                        return (
                        <div style={{color: "#8d8d8d", display: "flex", alignItems: "center"}}>

                         {annex.editMode && annex.editMode===true ?
                         <React.Fragment>
                          <div style={{display: "inline-flex", alignItems: "center", padding: '10px', backgroundColor:'#f5f5f5', color: "#8d8d8d"}}>
                          <Icon className={classes.anexIcon}>attachment_outline</Icon>
                          <Typography className={classes.anexLabel} onClick={()=> this.props.downloadClick(annex)}>{annex.fileName}</Typography>
                          {<Icon className={classes.anexCloseIcon} onClick={()=> this.props.closeClick(annex)}>close</Icon>}
                          </div>
                          </React.Fragment>
                          :
                          <React.Fragment>
                            <div style={{display: "inline-flex", alignItems: "center", padding: '10px', color: "#8d8d8d"}}>
                              <Icon style={{transform: "translateY(0%) rotate(135deg)", position: "relative", color: "#8d8d8d", marginRight: 10}}>attachment_outline</Icon>
                              <Typography className={classes.anexLabel} onClick={()=> this.props.downloadClick(annex)}>{annex.fileName}</Typography>
                            </div>
                          </React.Fragment>}
                        </div>
                      )})}
                      </ExpansionPanelDetails>
                    </ExpansionPanel>
                  </div>
                )}
              )} 
             </div>
            </Grid>  
        </Grid>
      </div>
    );
  }

}




export default withStyles(styles)(ActivityTimeline);

