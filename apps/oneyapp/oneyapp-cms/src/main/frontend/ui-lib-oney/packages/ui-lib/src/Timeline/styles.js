export const styles = (theme) => ({
  root: {
    "& [class*=MuiExpansionPanel-root]": {
      backgroundColor: "#fff",
      "&:before": {
        content: "none",
      },
    },
    "& [class*=MuiExpansionPanel-disabled]": {
      backgroundColor: "#fff",
    },
    "& [class*=MuiExpansionPanelSummary-disabled]": {
      opacity: 1,
    },
    "& [class*=MuiExpansionPanel-expanded]": {},
    "& [class*=MuiExpansionPanelSummary-expanded]": {
      // margin: 0
    },
    "& [class*=MuiExpansionPanelSummary-expandIcon]": {
      top: 22,
    },
    "& [class*=MuiExpansionPanelDetails-root]": {
      top: 22,
    },
    "& [class*=MuiPaper-elevation1]": {
      boxShadow: "none",
    },
  },
  rootGrid: {
    backgroundColor: "#f5f5f5",
    padding: "40px",
  },
  rootGridOuter: {
    position: "relative",
  },
  expansions: {
    marginBottom: 20,
    position: "relative",
  },
  dataContainer: {},
  eventItem: {
    margin: "-42px 0 0 60px",
  },
  firstEventItem: {
    margin: "-42px 0 0 60px",
  },
  vl: {
    width: 2,
    height: "calc(100% + 20px)",
    display: "inline-block",
    zIndex: 1,
    position: "absolute",
  },
  vlInitial: {
    borderLeft: `1px solid ${theme.palette.main}`,
    position: "absolute",
    height: "100%",
    marginLeft: -29,
    top: 0,
    width: 1,
  },
  contentContainer: {
    width: "100%",
    zIndex: 10,
  },
  dataContainer: {
    margin: "0px -5px 0px 0px",
    display: "inline-block",
    position: "relative",
    verticalAlign: "top",
  },
  ballIcon: {
    backgroundColor: "#fff",
    borderRadius: "50%",
    justifyContent: "center",
    alignItems: "center",
    fontSize: 40,
    marginRight: -16,
    zIndex: 2,
    display: "flex",
    position: "relative",
    width: "40px",
    height: "40px",
  },
  selectedBallIcon: {
    color: "#fff",
    backgroundColor: theme.palette.primary.main,
    borderColor: theme.palette.primary.main,
  },
  loader: {
    "& svg": {
      width: 18,
      marginBottom: -3,
    },
  },
  arrowLeft: {
    width: 0,
    height: 0,
    borderTop: "8px solid transparent",
    borderBottom: "8px solid transparent",
    borderRight: "8px solid white",
    position: "absolute",
    left: -8,
    top: 12,
    zIndex: 500,
  },
  arrowLeftNonExpandable: {
    width: 0,
    height: 0,
    borderTop: "8px solid transparent",
    borderBottom: "8px solid transparent",
    borderRight: "8px solid white",
    position: "absolute",
    left: 52,
    top: 12,
    zIndex: 500,
  },
  annexActionContainer: {
    display: "flex",
    alignItems: "center",
    position: "absolute",
    right: 50,
    top: 10,
    fontWeight: "100",
  },
  annexExpandIcon: {
    transform: "translateY(0%) rotate(135deg)",
    position: "relative",
    color: "#8d8d8d",
  },
  annexExpandIconBall: {
    height: 16,
    width: 16,
    backgroundColor: theme.palette.primary.main,
    borderRadius: "50%",
    border: "1px solid white",
    margin: "-25px 0 0 15px",
    position: "relative",
    "& p": {
      color: "white",
      fontSize: "11px",
      textAlign: "center",
      fontWeight: 800,
      margin: 0,
    },
  },
  anexLabel: {
    marginRight: 10,
    cursor: "pointer",
    "&:hover": {
      textDecoration: "underline",
      color: theme.palette.primary.main,
    },
  },
  anexIcon: {
    transform: "translateY(0%) rotate(135deg)",
    position: "relative",
    color: "#8d8d8d",
    marginRight: 10,
  },
  anexCloseIcon: {
    cursor: "pointer",
    "&:hover": {
      color: theme.palette.primary.main,
    },
  },
  expansionPanelSummaryContentContainer: {
    background: "#fff",
    paddingLeft: "24px",
    paddingRight: "24px",
    paddingTop: "12px",
    paddingBottom: "12px",
  },
  expansionPanelDetailsContentContainer: {
    display: "block",
  },
  expansionPanelDetailsContentInnerContainer: {
    color: "#8d8d8d",
    display: "flex",
    alignItems: "center",
  },
  editableAnnexContainer: {
    display: "inline-flex",
    alignItems: "center",
    padding: "10px",
    backgroundColor: "#f5f5f5",
    color: "#8d8d8d",
  },
  nonEditableAnnexContainer: {
    display: "inline-flex",
    alignItems: "center",
    padding: "10px",
    color: "#8d8d8d",
  },
  nonEditableAnnexContainerIcon: {
    transform: "translateY(0%) rotate(135deg)",
    position: "relative",
    color: "#8d8d8d",
    marginRight: 10,
  },
  panelSummaryTitle: {
    display: "flex",
    marginBottom: 10,
  },
  statusContainer: {
    background: "#8d8d8d",
    marginLeft: "10px",
    marginRight: "10px",
    paddingLeft: "10px",
    paddingRight: "10px",
    borderRadius: "10px",
    fontSize: "10px",
    color: "#fff",
    display: "flex",
    alignItems: "center",
  },
  actionContainer: {
    marginLeft: "10px",
  },
  actionButton: {
    background: "#fff",
    border: "1px solid #333333",
    padding: "5px",
  },
  actionButtonIcon: {
    fontSize: "16px",
    marginRight: "5px",
  },
  userContainer: {
    display: "inline-flex",
    marginBottom: 5,
  },
  dateFooterContainer: {
    marginTop: 10,
    display: "inline-flex",
  },
  userDateTitle: {
    position: "relative",
    color: "#8d8d8d",
  },
  singleUserDateTitle: {
    position: "relative",
    color: "#8d8d8d",
    display: "inline-flex",
    marginBottom: 5,
  },
  dotContainer: {
    height: 4,
    width: 4,
    borderRadius: "50%",
    backgroundColor: "#8d8d8d",
    alignSelf: "center",
    margin: "0 15px",
  },
  itemsContainer: {
    color: "#8d8d8d",
    marginBottom: 5,
  },
  footerRow: {
    position: "relative",
    color: "#8d8d8d",
    display: "flex",
    alignItems: "center",
  },
  footerIcon: {
    fontSize: "15px",
    marginRight: "5px",
  },
});
