const ComponentName = "HeaderBar";

const styles = (theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    height: "28px",
  },
  titleContainer: {
    marginRight: "12px",
    minWidth: "99px",
    // width: "100%"
  },
  noTitleContainer: {
    marginRight: "18px",
    minWidth: "110px",
  },
  childrenContainer: {
    marginLeft: theme.spacing.unit,
    width: "100%",
  },
});

export { styles, ComponentName };
