import React from "react";
import Grid from "@ui-lib/core/Grid";
import Typography from "@ui-lib/core/Typography";
import { withStyles } from "@ui-lib/core/styles";
import Icon from "@ui-lib/core/Icon";
import classNames from "@ui-lib/core/classNames";
import PropTypes from "prop-types";
import isEmpty from "lodash/isEmpty";

const styles = theme => ({
    fileIcon: {
        fontSize: 34,
        marginRight: theme.spacing.unit * 2,
        color: theme.palette.text.disabled
    },

    divider: {
        width: "100%",
        height: 2,
        backgroundColor: theme.palette.text.disabled,
        marginTop: theme.spacing.unit,
        marginBottom: theme.spacing.unit
    },

    errorFgColor: {
        color: theme.palette.common.negative
    },
    successFgColor: {
        color: theme.palette.common.positive
    },
    warningFgColor: {
        color: theme.palette.common.medium
    },
    errorBgColor: {
        backgroundColor: theme.palette.common.negative
    },
    successBgColor: {
        backgroundColor: theme.palette.common.positive
    },
    warningBgColor: {
        backgroundColor: theme.palette.common.medium
    }
});

export const FileUploadItemStatus = {
    ERROR: "ERROR",
    SUCCESS: "SUCCESS",
    WARNING: "WARNING"
};

const FileUploadItem = ({
    iconString,
    fileName,
    supportText,
    endAdornment,
    status,
    ...props
}) => {
    const { classes } = props;
    return (
        <Grid container direction="row" alignItems="flex-start">
            <Grid item>
                <Icon
                    className={classNames(
                        classes.fileIcon,
                        status === FileUploadItemStatus.ERROR ? classes.errorFgColor : null,
                        status === FileUploadItemStatus.SUCCESS
                            ? classes.successFgColor
                            : null,
                        status === FileUploadItemStatus.WARNING
                            ? classes.warningFgColor
                            : null
                        , iconString)}
                >
                </Icon>
            </Grid>
            <Grid item xs zeroMinWidth>
                <Grid container alignItems="center">
                    <Grid item xs={!isEmpty(endAdornment)} zeroMinWidth>
                        <Typography variant="subheading" noWrap title={fileName}>
                            {fileName}
                        </Typography>
                    </Grid>
                    {!isEmpty(endAdornment) && <Grid item>{endAdornment}</Grid>}
                    <Grid item xs={12}>
                        <div
                            className={classNames(
                                classes.divider,
                                status === FileUploadItemStatus.ERROR
                                    ? classes.errorBgColor
                                    : null,
                                status === FileUploadItemStatus.SUCCESS
                                    ? classes.successBgColor
                                    : null,
                                status === FileUploadItemStatus.WARNING
                                    ? classes.warningBgColor
                                    : null
                            )}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="caption">{supportText}</Typography>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

FileUploadItem.propTypes = {
    iconString: PropTypes.string.isRequired,
    fileName: PropTypes.string.isRequired,
    supportText: PropTypes.string,
    endAdornment: PropTypes.node,
    status: PropTypes.oneOf([
        FileUploadItemStatus.ERROR,
        FileUploadItemStatus.WARNING,
        FileUploadItemStatus.SUCCESS
    ])
};

export default withStyles(styles)(FileUploadItem);
