import React from 'react';
import Moment from 'react-moment';
import { DateUtils } from 'react-day-picker';

export function generateIntervalPresentation(from, to) {
    if (DateUtils.isSameDay(from, to)) {
        return <div><Moment format="DD MMM">{from}</Moment></div>
    }
    else if (DateUtils.isSameMonth(from, to)) {
        return <div><Moment format="DD">{from}</Moment>&nbsp;-&nbsp;<Moment format="DD MMM">{to}</Moment></div>
    }
    else if (from.getFullYear() !== to.getFullYear()) {
        return <div><Moment format="DD MMM YY">{from}</Moment>&nbsp;-&nbsp;<Moment format="DD MMM YY">{to}</Moment></div>
    }
    else {
        return <div><Moment format="DD MMM">{from}</Moment>&nbsp;-&nbsp;<Moment format="DD MMM">{to}</Moment></div>
    }
};

export function generatePresentation(fromDate, toDate) {
    if (fromDate && toDate) {
        return generateIntervalPresentation(fromDate, toDate);
    }
    else if (fromDate && !toDate) {
        return <div><Moment format="DD MMM">{fromDate}</Moment></div>;
    }
    else if (!fromDate && toDate) {
        return <div><Moment format="DD MMM">{toDate}</Moment></div>;
    }
    return <div>&nbsp;</div>;
};

export function generatePresentationFromFormat(date, dateFormat) {
    return <div><Moment format={dateFormat}>{date}</Moment></div>;
};
