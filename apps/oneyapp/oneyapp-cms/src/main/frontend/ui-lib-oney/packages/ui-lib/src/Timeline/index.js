import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import Grid from "@ui-lib/core/Grid";
import Icon from "@ui-lib/core/Icon";
import ExpansionPanel from "@ui-lib/core/ExpansionPanel";
import ExpansionPanelSummary from "@ui-lib/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@ui-lib/core/ExpansionPanelDetails";
import { styles } from "./styles";
import ExpansionPanelDetailsContent from "./ExpansionPanelDetailsContent";
import ExpansionPanelSummaryContent from "./ExpansionPanelSummaryContent";

const Timeline = (props) => {
  const { classes, data, downloadClick, closeClick } = props;
  return (
    <div className={classes.root}>
      <Grid container className={classes.rootGrid}>
        <Grid item xs={12} className={classes.rootGridOuter}>
          <div className={classes.mapContainer}>
            {data.content.map((reports, index) => {
              return (
                <div key={index} className={classes.expansions}>
                  <div className={classes.contentContainer}>
                    <div className={classes.dataContainer}>
                      {reports.icon ? (
                        <div
                          style={{
                            border: `1px solid ${
                              reports.borderColor ? reports.borderColor : "#333"
                            }`,
                          }}
                          className={
                            reports.selected
                              ? `${classes.selectedBallIcon} ${classes.ballIcon}`
                              : classes.ballIcon
                          }
                        >
                          <Icon
                            style={{
                              color: reports.iconColor,
                            }}
                          >
                            {reports.icon}
                          </Icon>
                        </div>
                      ) : (
                        <div
                          style={{
                            border: `1px solid ${
                              reports.borderColor ? reports.borderColor : "#333"
                            }`,
                          }}
                          className={
                            reports.selected
                              ? `${classes.selectedBallIcon} ${classes.ballIcon}`
                              : classes.ballIcon
                          }
                        >
                          <Icon
                            style={{
                              color: reports.iconColor,
                            }}
                          >
                            lens
                          </Icon>
                        </div>
                      )}
                    </div>
                    <div
                      className={classes.vl}
                      style={{
                        border: `1px solid ${
                          reports.lineColor ? reports.lineColor : "#333"
                        }`,
                        borderStyle: reports.borderStyle,
                        display:
                          index === data.content.length - 1
                            ? "none"
                            : "inheirt",
                      }}
                    ></div>
                    <div className={classes.infoContainer}></div>
                  </div>
                  {reports.expandable === false ? (
                    <div
                      className={
                        index === 0 ? classes.firstEventItem : classes.eventItem
                      }
                    >
                      <div className={classes.arrowLeftNonExpandable}></div>
                      <div
                        className={
                          classes.expansionPanelSummaryContentContainer
                        }
                      >
                        <ExpansionPanelSummaryContent
                          classes={classes}
                          reports={reports}
                        />
                      </div>
                    </div>
                  ) : (
                    <ExpansionPanel
                      key={index}
                      className={
                        index === 0 ? classes.firstEventItem : classes.eventItem
                      }
                    >
                      <div className={classes.arrowLeft}></div>
                      <ExpansionPanelSummary
                        expandIcon={<Icon>expand_more</Icon>}
                      >
                        <ExpansionPanelSummaryContent
                          classes={classes}
                          reports={reports}
                        />
                      </ExpansionPanelSummary>
                      <ExpansionPanelDetails
                        className={
                          classes.expansionPanelDetailsContentContainer
                        }
                      >
                        <ExpansionPanelDetailsContent
                          classes={classes}
                          reports={reports}
                          downloadClick={downloadClick}
                          closeClick={closeClick}
                        />
                      </ExpansionPanelDetails>
                    </ExpansionPanel>
                  )}
                </div>
              );
            })}
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default withStyles(styles)(Timeline);
