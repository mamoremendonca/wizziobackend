// @ts-ignore
import babel from 'rollup-plugin-babel';
import replace from 'rollup-plugin-replace';
import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';
import {terser} from 'rollup-plugin-terser';
import {sizeSnapshot} from 'rollup-plugin-size-snapshot';
import scss from 'rollup-plugin-scss' // compatible with both scss and vanilla css

const input = './src/index.js';
const globals = {
    '@ui-lib/core/Icon': 'UiLibCore.Icon',
    '@ui-lib/core/styles': 'UiLibCore.styles',
    '@ui-lib/core/Table': 'UiLibCore.Table',
    '@ui-lib/core/TableRow': 'UiLibCore.TableRow',
    '@ui-lib/core/TextField': 'UiLibCore.TextField',
    '@ui-lib/core/TableHead': 'UiLibCore.TableHead',
    '@ui-lib/core/TableBody': 'UiLibCore.TableBody',
    '@ui-lib/core/ClickAwayListener': 'UiLibCore.ClickAwayListener',
    '@ui-lib/core/TableSortLabel': 'UiLibCore.TableSortLabel',
    '@ui-lib/core/CircularProgress': 'UiLibCore.CircularProgress',
    '@ui-lib/core/TableCell': 'UiLibCore.TableCell',
    '@ui-lib/core/utils/helpers': 'UiLibCore',
    '@ui-lib/core/InputLabel': 'UiLibCore.InputLabel',
    '@ui-lib/core/Grid': 'UiLibCore.Grid',
    '@ui-lib/core/Paper': 'UiLibCore.Paper',
    '@ui-lib/core/IconButton': 'UiLibCore.IconButton',
    '@ui-lib/core/ExpansionPanel': 'UiLibCore.ExpansionPanel',
    '@ui-lib/core/ExpansionPanelSummary': 'UiLibCore.ExpansionPanelSummary',
    '@ui-lib/core/ExpansionPanelDetails': 'UiLibCore.ExpansionPanelDetails',
    '@ui-lib/core/MenuItem': 'UiLibCore.MenuItem',
    '@ui-lib/core/MenuList': 'UiLibCore.MenuList',
    '@ui-lib/core/Collapse': 'UiLibCore.Collapse',
    '@ui-lib/core/classNames': 'UiLibCore.classNames',
    '@ui-lib/core/Tooltip': 'UiLibCore.Tooltip',
    '@ui-lib/core/Typography': 'UiLibCore.Typography',
    '@ui-lib/core/Checkbox': 'UiLibCore.Checkbox',
    '@ui-lib/core/Button': 'UiLibCore.Button',
    '@ui-lib/core/List': 'UiLibCore.List',
    '@ui-lib/core/ListItem': 'UiLibCore.ListItem',
    '@ui-lib/core/ListItemIcon': 'UiLibCore.ListItemIcon',
    '@ui-lib/core/ListItemText': 'UiLibCore.ListItemText',
    '@ui-lib/core/Link': 'UiLibCore.Link',
    '@ui-lib/core/FormControlLabel': 'UiLibCore.FormControlLabel',
    '@ui-lib/core/styles/colorManipulator': 'UiLibCore.styles.colorManipulator',
};

const getBabelOptions = () => ({exclude: /node_modules/, runtimeHelpers: true, configFile: '../../scripts/babel.config.js'});

const commonjsOptions = {
    /*   ignoreGlobal: true, */
    include: /node_modules/,
    namedExports: {
        '../../node_modules/react/index.js': ['Component', 'cloneElement','useCallback','useReducer','useState','useEffect', 'useRef', 'createContext', 'forwardRef', 'createElement', 'Fragment', 'PureComponent'],
        '../../node_modules/react-dom/index.js': [
            'render', 'hydrate', 'createPortal', 'findDOMNode'
        ],
        '../../node_modules/react-is/index.js' : [
            'isForwardRef'
        ],
        '../../node_modules/prop-types/index.js': [
            'elementType',
            'bool',
            'func',
            'object',
            'oneOfType',
            'element'
        ]
    }
};

function onwarn(warning) {
    throw Error(warning.message);
}

export default[
    {
        input,
        external : Object.keys(globals),
        //onwarn,
        output : {
            globals,
            format: 'umd',
            name: "Oney",
            file: 'build/umd/ui-lib-oney.umd.js'
        },
        plugins : [
            nodeResolve(),
            scss(),
            babel(getBabelOptions()),
            commonjs(commonjsOptions),
            replace({
                'process.env.NODE_ENV': JSON.stringify('development')
            })
        ]
    }, {
        input,
        external : Object.keys(globals),
       // onwarn,
        output : {
            globals,
            format: 'umd',
            name: "Oney",
            file: 'build/umd/ui-lib-oney.umd.js'
        },
        plugins : [
            nodeResolve(),
            scss(),
            babel(getBabelOptions()),
            commonjs(commonjsOptions),
            replace({
                'process.env.NODE_ENV': JSON.stringify('production')
            }),
            sizeSnapshot({snapshotPath: 'size-snapshot.json'}),
            terser()
        ]
    }
]