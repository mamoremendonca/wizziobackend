import React from 'react';
import { tableStyles } from './styles';
import { downloadCSV } from './table-actions.js';
import Tooltip from "@ui-lib/core/Tooltip";
import Grid from "@ui-lib/core/Grid";
import Icon from "@ui-lib/core/Icon";
import {withStyles} from "@ui-lib/core/styles";
import classNames from "@ui-lib/core/classNames";

//Table actions toolbar component
const ActionsBar = ({
  addRecordTitle,
  allowAddRecord,
  addRecordFn,
  classes,
  getLabelFn,
  allowRefresh,
  refreshData,
  allowDownloadCSV,
  allowShowHideCols,
  downloadTitle = "data",
  onlyFilterableCols,
  allowFilterRecs,
  handleToggleFilterState,
  headers,
  showColumnsSel,
  allDataRecords,
  getVisibleCols,
  showFilter,
  toggleColumnsSelState,
  ...props }
) => {

  return (

    <Grid item xs={12} style={{ textAlign: "right" }}>

      {/*ADD NEW RECORD TO TABLE*/}
      <Tooltip title={addRecordTitle ? addRecordTitle : "Add record"} placement='top'>
        <div id={props.prefix + "-add-record-button"} style={{ display: "inline-block" }}>
          <Icon style={{ display: (allowAddRecord ? "" : "none") }} className={classNames(classes.filterIcon,'icon-add')} color='secondary' onClick={() => addRecordFn()}/>
        </div>
      </Tooltip>

      {/*REFRESH TABLE DATA*/}
      <Tooltip title={getLabelFn ? getLabelFn('table_component_refresh', 'Refresh') : 'Refresh'} placement='top'>
        <div id={props.prefix + "-refresh-button"} style={{ display: "inline-block" }}>
          <Icon style={{ display: (allowRefresh ? "" : "none") }} className={classNames(classes.filterIcon,"icon-lastupdate")} color='secondary' onClick={() => refreshData()}/>
        </div>
      </Tooltip>

      {/*DOWNLOAD CSV*/}
      <Tooltip title={getLabelFn ? getLabelFn('table_component_download_tooltip', 'Download CSV') : 'Download CSV'} placement='top'>
        <div id={props.prefix + "-download-CSV-button"} style={{ display: "inline-block" }}>
          <Icon style={{ display: (allowDownloadCSV ? "" : "none") }} className={classNames(classes.filterIcon,'icon-download')} color='secondary'
            onClick={() => downloadCSV(downloadTitle, allDataRecords, getVisibleCols(headers, true, false))}></Icon>
        </div>
      </Tooltip>

      <Tooltip title={getLabelFn ? getLabelFn('table_component_toggle_columns_tooltip', 'Show/Hide Columns') : 'Show/Hide Columns'} placement='top'>
        <div id={props.prefix + "-edit-visible-button"} style={{ display: "inline-block" }}>
          <Icon style={{ display: (allowShowHideCols ? "" : "none") }} className={classNames((showColumnsSel ? classes.filterIconSelected : classes.filterIcon),"icon-ic_add_column")} color='secondary' onClick={() => toggleColumnsSelState()}/>
        </div>
      </Tooltip>


      {/*FILTER TABLE RECORDS*/}
      <Tooltip title={getLabelFn ? getLabelFn('table_component_filter_title', 'Filter') : 'Filter'} placement='top'>
        <div id={props.prefix + "-filter-button"} style={{ display: (onlyFilterableCols.length > 0) ? "inline-block" : "none" }}>
          <Icon style={{ display: (allowFilterRecs ? "" : "none") }} className={classNames(classes.filterIcon, 'icon-filter')} color='secondary' onClick={() => handleToggleFilterState()}/>
        </div>

      </Tooltip>

    </Grid>

  )
}

export default withStyles(tableStyles, { name: "tableActions" })(ActionsBar);
