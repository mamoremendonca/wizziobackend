const ComponentName = 'AutocompleteInput';
const styles = theme => ({
    root: {
        marginTop: '16px',
        marginBottom: '8px'
    },
    container: {
        flexGrow: 1,
        position: 'relative'
    },
    paper: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing.unit,
        left: '0px',
        right: '0px',
        padding: '0px',
        maxHeight: '370px',
        overflowY: 'auto',
        overflowX: 'hidden'
    },
    chip: {
        margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`
    },
    inputRoot: {
        flexWrap: 'wrap'
    },
    inputInput: {
        width: 'auto',
        flexGrow: 1
    },
    divider: {
        height: theme.spacing.unit * 2
    },
    iconColor: {
        '&:before':{
            color: theme.palette.primary.main + " !important"
        }
    }

});

export {ComponentName, styles};
