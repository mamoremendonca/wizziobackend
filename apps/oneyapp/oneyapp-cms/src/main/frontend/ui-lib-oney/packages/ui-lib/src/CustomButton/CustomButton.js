import React from "react";
import PropTypes from "prop-types";
import classNames from "@ui-lib/core/classNames";
import { withStyles } from "@ui-lib/core/styles";
import Button from "@ui-lib/core/Button";
import { ComponentName, styles } from "./CustomButtonStyles";

function ClassNames(props) {
    const {classes, children, className, ...other} = props;

    return (
        <Button className={classNames(classes.root, className)} {...other}>
            {children || "Hello World"}
        </Button>
    );
}

ClassNames.propTypes = {
    children : PropTypes.node,
    className : PropTypes.string,
};

export default withStyles(styles, {withName: ComponentName})(ClassNames);