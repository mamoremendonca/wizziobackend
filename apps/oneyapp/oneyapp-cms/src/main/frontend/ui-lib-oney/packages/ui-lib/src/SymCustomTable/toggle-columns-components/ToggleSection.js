import React from 'react';
import { withStyles } from "@ui-lib/core/styles";
import Grid from "@ui-lib/core/Grid";

const styles = theme => ({
  root: {
    borderBottom: `1px solid ${theme.palette.common.bg}`,
    backgroundColor: "#fff",
    zIndex: "10000",
    paddingTop: "5px",
    paddingBottom: "5px",
    '&:last-of-type':
    {
      border: 0,
      paddingBottom: "10px"
    }
  }
});

const ToggleSection = ({ classes, ...props }) => {
  return (
    <Grid item xs={12} className={classes.root}>
      <Grid container>
        {props.children}
      </Grid>
    </Grid>
  )
}

export default withStyles(styles)(ToggleSection);
