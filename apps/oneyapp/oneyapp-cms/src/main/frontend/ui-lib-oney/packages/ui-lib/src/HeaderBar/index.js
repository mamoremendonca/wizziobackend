import React from "react";
import HeaderRender from "./HeaderBarRender";
import PropTypes from "prop-types";

const HeaderBar = (props) => {
  const { children, title } = props;
  return <HeaderRender title={title} children={children} />;
};

HeaderBar.propTypes = {
  children: PropTypes.object,
  title: PropTypes.string,
};

export default HeaderBar;
