import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { styles, ComponentName } from "./HeaderBarStyles";
import Typography from "@ui-lib/core/Typography";
import PropTypes from "prop-types";

const HeaderBarRender = ({ classes, title, children,  }) => {
  return (
    <div className={classes.root}>
      {title ? (
        <Typography
          variant="h2"
          color={"primary"}
          className={classes.titleContainer}
        >
          {title}
        </Typography>
      ) : <span className={classes.noTitleContainer}></span>}

      <div className={classes.childrenContainer}>{children}</div>
    </div>
  );
};

HeaderBarRender.propTypes = {
  classes: PropTypes.object,
  title: PropTypes.string,
  children: PropTypes.array,
};

export default withStyles(styles, { name: ComponentName })(HeaderBarRender);
