import React from 'react';
import { tableStyles } from './styles';
import {withStyles} from "@ui-lib/core/styles";
import Grid from "@ui-lib/core/Grid";
import ToggleWrapper from './toggle-columns-components/ToggleWrapper';
import ToggleSection from './toggle-columns-components/ToggleSection';
import ToggleForm from './toggle-columns-components/ToggleForm';
import ToggleItem from './toggle-columns-components/ToggleItem';

export class ToggleColumnsForm extends React.Component {

  buildColumnConfig = (recordIndex, column, disabled) => {
    let filterKey = column.name;
    let checkedVal = this.props.getColumnStateVal(filterKey)

    return (
      <ToggleItem key={recordIndex} recordIndex={recordIndex} checkedVal={checkedVal} handleChange={() => this.changeColumnState(filterKey)} column={column} disabled={disabled} getLabelFn={this.props.getLabelFn ? (key, defaultLabel) => this.props.getLabelFn(key, defaultLabel) : null}>
      </ToggleItem>
    );
  };

  buildColumnsConfig = () => {
    //Check if is necessary to show table columns that can not be toggled.
    let showMandatoryCols = false;
    let cols = this.props.colToggleList;
    for (let i = 0; i < cols.length; i++) {
      if (!cols[i].toggable) {
        showMandatoryCols = true;
        break;
      }
    }

    //Check if is necessary to show table columns that can be toggled.
    let showToggableCols = false;
    for (let i = 0; i < cols.length; i++) {
      if (cols[i].toggable === true) {
        showToggableCols = true;
        break;
      }
    }
    if (!this.props.headers) {
      return <div></div>
    }
    else {
      return <div style={{ position: 'absolute', width: "100%", zIndex: 100, display: (this.props.showColumnsSel ? "block" : "none") }}>
        <ToggleWrapper hideColumnsSel={() => this.props.hideColumnsSel()}
          relatedIcon={this.props.prefix + "-edit-visible-button"}>
          <ToggleSection>
            <ToggleForm handleSearch={this.props.handleColsSearch} getLabelFn={this.props.getLabelFn ? (key, defaultLabel) => this.props.getLabelFn(key, defaultLabel) : null}></ToggleForm>
          </ToggleSection>
          {showMandatoryCols &&
            <ToggleSection>
              {this.props.colToggleList.map((column, index) => {
                if (!column.toggable || column.toggable === false) {
                  return this.buildColumnConfig(index, column, true)
                }
              })}
            </ToggleSection>
          }
          {
            <ToggleSection>
              {
                this.props.buildSelectAll(!showToggableCols)
              }
              {this.props.colToggleList.map((column, index) => {
                if (column.toggable === true) {
                  return this.buildColumnConfig(index, column, false)
                }
              })
              }
            </ToggleSection>
          }
        </ToggleWrapper>
      </div>
    }
  };

  render() {

    const { colToggleList, showColumnsSel, headers, hideColumnsSel, handleColsSearch, getLabelFn, allowShowHideCols, classes, buildSelectAll } = this.props;


    return (
      <Grid item xs={12} className={classes.filters}>
        {allowShowHideCols ? this.buildColumnsConfig() : <div></div>}
      </Grid>)
  };
};

export default withStyles(tableStyles, { name: "toggleForm" })(ToggleColumnsForm);
