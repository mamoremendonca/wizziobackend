import React from "react";
import isEmpty from "lodash/isEmpty";
import { withStyles } from "@ui-lib/core/styles";
import Typography from "@ui-lib/core/Typography";
import FilesDropper from "./FilesDropper";

const styles = theme => ({
  DragAndDropEmpty: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    border: "2px dashed",
    borderColor: theme.palette.primary.main,
    backgroundColor: theme.palette.bg,
    height: "168px"
  },
  draggingClass: {
    opacity: 0.5
  }
});

const SymFileDropper = props => {
  const {
    classes,
    files,
    children,
    getLabelFn,
    handleFilesAdded,
    accept,
    componentId
  } = props;

  return (
    <FilesDropper
      handleDrop={handleFilesAdded}
      draggingClass={classes.draggingClass}
      style={{ padding: "1px" }}
      accept={accept}
      componentId={componentId}
      files={files}
    >
      {isEmpty(files) ? (
        <div className={classes.DragAndDropEmpty}>
          <Typography variant="subtitle1" color="primary">
            {getLabelFn!== undefined ? getLabelFn(
              "drop_files_here_to_upload",
              "Drop files here to upload"
            ): "Drop files here to upload"}
          </Typography>
          <Typography variant="subtitle1">{getLabelFn ? getLabelFn("or", "or") : "or"}</Typography>
          <Typography variant="subtitle1" color="primary">
            {getLabelFn ? getLabelFn("click_to_choose_file", "Click to choose file") : "Click to choose file"}
          </Typography>
        </div>
      ) : (
          children
        )}
    </FilesDropper>
  );
};

export default withStyles(styles)(SymFileDropper);
