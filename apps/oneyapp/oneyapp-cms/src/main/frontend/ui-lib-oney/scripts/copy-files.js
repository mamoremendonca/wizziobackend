/* eslint-disable no-console */

const path = require('path');
const fse = require('fs-extra');
const packagePath = process.cwd();

async function copyFile(file) {
  console.log(packagePath);
  const buildPath = path.resolve(packagePath, './build/', path.basename(file));
  await fse.copy(file, buildPath);
  console.log(`Copied ${file} to ${buildPath}`);
}

async function run() {
  await Promise.all(
    ['./package.json'].map(file => copyFile(file)),
  );
}

run();
