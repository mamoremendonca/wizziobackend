#!/usr/bin/env bash

echo "Starting Init on Omni-ui-lib"
cd ..

echo "Removing modules"
rm yarn.lock
rm -rf node_modules

echo "Installing dependencies on ui-lib"
yarn install

cd packages/ui-lib
echo "Unlink and Linking @ui-lib/oney"
yarn unlink
yarn link


cd ../../ui-demo
echo "Removing modules ui-demo"
rm -rf node_modules
rm yarn.lock
echo "Installing dependencies on ui-demo"
yarn install
yarn link @ui-lib/oney
cd ../scripts
echo "Done"