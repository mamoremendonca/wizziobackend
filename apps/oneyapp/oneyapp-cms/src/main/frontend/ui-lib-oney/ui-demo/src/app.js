import React, { Component } from 'react';
import Journey from "./journey";
import { Provider } from "react-redux";
import { name as packageName, parent, contentVersion } from '../package.json';
import { createStoreSingleton } from "omni-journey";
import { FinalJourney } from "omni-journey";
/**
 * REDUCERS TO IMPORT
 */
import demoReducer from './reducers/DemoReducer'
let reducersToMerge = { demo: demoReducer };

let Store = createStoreSingleton(reducersToMerge);

export default class App extends Component {

    render = () => {
        return (
            <Provider store={Store}>
                <div>
                    <RenderJourney />
                </div>
            </Provider>
        );
    };
}

let RenderJourney = FinalJourney(Journey, Store, packageName, parent, contentVersion);