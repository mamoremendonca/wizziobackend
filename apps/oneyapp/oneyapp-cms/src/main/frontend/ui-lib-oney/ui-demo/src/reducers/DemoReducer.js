import { SET_VALUE } from "../actions/DemoActions";

const initialState = {
    value: ""
};

export default function demoReducer(state, action) {

    if (state === undefined)
        return initialState;

    switch (action.type) {
        case SET_VALUE:
            return { ...state, value: action.value }
        default:
            return state;
    }
}
