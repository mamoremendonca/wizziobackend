import moment from 'moment'
import React from "react";

export let timelineDemoData = {
    iconSize: "16",
    content:[
    {
        title: "Registo",
        date: new Date(),
        icon: 'info',
        borderStyle: "solid",
        borderColor: "#0f0",
        items:[
            {
                header: "Motivos",
                content: "Porque sim.",
            }
        ],
        annexes: [
            {
                id: "200300401",
                fileName: "asdsad.pdf",
                uploader: "Joana Matias",
                uploadDate: "20/12/2020"
            },
            {
                id: "100300402",
                fileName: "whatever.pdf",
                uploader: "António Matias",
                uploadDate: "20/12/2020"
            }
        ]
    },
    {
        title: "Rejeitada",
        date: new Date(),
        user: 'Ana Queiroz',
        icon: 'close',
        iconSize: "16",
        borderStyle: "solid",
        borderColor: "#0f0",
        items:[
            {
                header: "Motivos",
                content: "Porque sim.",
            },
            {
                header: "Comentários",
                content: "Yolo 4 life.",
            }
        ],
        annexes: [
            {
                id: "100300403",
                fileName: "whatever.pdf",
                uploader: "António Matias",
                uploadDate: "20/12/2020",
                editMode: true
            }
        ]
    },
    {
        title: "Aprovada",
        date: new Date(),
        user: 'Ana Queiroz',
        icon: 'check',
        iconSize: "16",
        selected: true,
        borderStyle: "dashed",
        borderColor: "#0f0",
        items:[
            {
                header: "Motivos",
                content: "Porque sim.",
            },
            {
                header: "Comentários",
                content: "Yolo 4 life.",
            }
        ],
        annexes: [

            {
                id: "100300404",
                fileName: "whatever.pdf",
                uploader: "António Matias",
                uploadDate: "20/12/2020"
            }
        ]
    },
    {
        title: "Aprovação",
        date: new Date(),
        user: 'Ana Queiroz',
        iconSize: "16",
        borderStyle: "dashed",
        borderColor: "#0f0",
        items:[
            {
                header: "Motivos",
                content: "Porque sim.",
            },
            {
                header: "Comentários",
                content: "Yolo 4 life.",
            }
        ],
        annexes: [

            {
                id: "100300405",
                fileName: "whatever.pdf",
                uploader: "António Matias",
                uploadDate: "20/12/2020"
            }
        ]
    },
    {
        title: "Planeado",
        date: new Date(),
        user: 'Ana Queiroz',
        iconSize: "16",
        borderStyle: "dashed",
        borderColor: "#0f0",
        items:[
            {
                header: "Motivos",
                content: "Porque sim.",
            },
            {
                header: "Comentários",
                content: "Yolo 4 life.",
            }
        ],
        annexes: [
            {
                id: "100300406",
                fileName: "whatever.pdf",
                uploader: "António Matias",
                uploadDate: "20/12/2020"
            }
        ]
    },
]
}


export let tableData =  [
    {
        "id": 1,
        "type": "OC",
        "documentId": 12345679,
        "supplierId": "Fornecedor 1",
        "value": 0,
        "creationDate": "2019-09-24 00:00:00",
        "endDate": "2019-09-24 00:00:00",
        "department": 577,
        "project": "",
        "user": "A. Barros",
        "status": 1
    },
    {
        "id": 2,
        "type": "OC",
        "documentId": 12684320,
        "supplierId": "Fornecedor 2",
        "value": 11110.00,
        "creationDate": "2019-09-24 00:00:00",
        "endDate": "2019-12-25 00:00:00",
        "department": 577,
        "project": "",
        "user": "A. Barros",
        "status": 2
    },
    {
        "id": 3,
        "type": "OC",
        "documentId": 12684320,
        "supplierId": "Fornecedor 2",
        "value": 11110.00,
        "creationDate": "2019-09-24 00:00:00",
        "endDate": new Date(),
        "department": 577,
        "project": "",
        "user": "A. Barros",
        "status": 3
    },
]

export let tableHeader = {
    "columns": [
            {
                    "name": "id",
                    "i18nKey": "blyat",
                    "label": "id",
                    "sortable": true,
                    "visible": false,
                    "dataType": "VARCHAR",
                    "maxChars": 80
            },
            {
                    "name": "status",
                    "label": "Status",
                    "sortable": true,
                    "visible": true,
                    "dataType": "VARCHAR",
                    "maxChars": 10,
                    "align": "center"
            },
            {
                    "name": "type",
                    "label": "Type",
                    "dataType": "VARCHAR",
                    "maxChars": 100,
                    "visible": false,
                    "sortable": true
            },
            {
                    "name": "documentId",
                    "label": "Nº Documento",
                    "sortable": true,
                    "visible": true,
                    "dataType": "VARCHAR",
                    "maxChars": 36
            },
            {
                    "name": "supplierId",
                    "label": "Fornecedor",
                    "sortable": true,
                    "visible": true,
                    "dataType": "VARCHAR",
                    "maxChars": 20
            },
            {
                    "name": "value",
                    "label": "Valor",
                    "sortable": true,
                    "visible": true,
                    "dataType": "VARCHAR",
                    "maxChars": 20
            },
            {
                    "name": "creationDate",
                    "label": "Data de criação",
                    "sortable": true,
                    "visible": true,
                    "dataType": "DATE",
                    "applyDateFormat": "DD/MM/YYYY",
                    "maxChars": 20
            },
            {
                    "name": "endDate",
                    "label": "Data de vencimento",
                    "sortable": true,
                    "visible": true,
                    "dataType": "DATE",
                    "applyDateFormat": "DD/MM/YYYY",
                    "maxChars": 20
            },
            {
                    "name": "department",
                    "label": "Departamento",
                    "sortable": true,
                    "visible": true,
                    "dataType": "VARCHAR",
                    "maxChars": 20
            },
            {
                    "name": "project",
                    "label": "Projecto",
                    "sortable": true,
                    "visible": true,
                    "dataType": "VARCHAR",
                    "maxChars": 20
            },
            {
                    "name": "user",
                    "label": "Atribuído a",
                    "sortable": true,
                    "visible": true,
                    "dataType": "VARCHAR",
                    "maxChars": 20
            },
        

    ]
}


export let solveStatecolor = (value) =>{
    switch(value) {
        case 1:
            return '#26AEE5'
        case 2:
            return '#E79208'
        case 3:
            return '#E0D011'
        default:
            return '#4237C4'
      }
}

export let numberWithSpaces = (x) => {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return parts.join(".") + ' €';
}

export let calcDayToDeliver = (value, endDate) => {
    let  deliveryDate = moment(endDate)
    let today = moment(new Date())
    let differenceDays = deliveryDate.diff(today, 'days')
    let chipStyle = { marginLeft: 10, padding: 5, borderRadius:25, color: '#fff', fontSize: '12px'}
    if (differenceDays > 0){
    return (<div><span>{value}</span><span style={{backgroundColor: '#E79208',...chipStyle}}>+{differenceDays} days</span></div>)
    }
    else if (differenceDays === 0){
        return (<div><span>{value}</span><span style={{backgroundColor: '#E79208',...chipStyle}}>Today</span></div>)
    }
    else if (differenceDays === -1){
        return (<div><span>{value}</span><span style={{backgroundColor: '#D92727',...chipStyle}}>{differenceDays} day</span></div>)
    }
    else{
        return (<div><span>{value}</span><span style={{backgroundColor: '#D92727',...chipStyle}}>{differenceDays} days</span></div>)
    }
}

/**
 * Format a number to Byte units (Bytes, KB, MB,...) depending on number of bytes.
 * @param {Number} bytes the value of bytes
 * @param {Number} [decimals] Optional argument (default 2) to specify the maximum length of decimal places
 *
 * @returns {String} the formatted value.
 */
export const formatBytes = (bytes, decimals) => {
    if (bytes === 0) return "0 Bytes";
    var k = 1024,
      dm = decimals <= 0 ? 0 : decimals || 2,
      sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
      i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  };
  
  /**
   * Promise to convert a file to Base64
   * @param {File} file the file to convert to Base64
   *
   * @returns {Promise} a promise to convert the file asynchronously
   */
  export const getBase64 = file =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        let encoded = reader.result.replace(/^data:(.*;base64,)?/, "");
        if (encoded.length % 4 > 0) {
          encoded += "=".repeat(4 - (encoded.length % 4));
        }
        resolve(encoded);
      };
      reader.onerror = error => reject(error);
    });

