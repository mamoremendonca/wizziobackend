const path = require("path");

module.exports = {
    webpack: {
        alias: {
            "@ui-lib/oney": path.resolve(__dirname + "/node_modules/@ui-lib/oney/src")
        },
        configure: (webpackConfig) => {
            webpackConfig.resolve.plugins.pop();
            webpackConfig.module.rules[2].oneOf[2] = {
                test: /\.(js|mjs)$/,
                exclude: /@babel(?:\/|\\{1,2})runtime/,
                loader: require.resolve('babel-loader'),
                options: {
                    babelrc: false,
                    configFile: false,
                    compact: false,
                    presets: [
                        [
                            "@babel/preset-env",
                            {
                                "modules": "commonjs"
                            }
                        ],
                        "@babel/preset-react",
                        "@babel/preset-flow"
                    ],
                    plugins: [
                        "@babel/plugin-proposal-class-properties",
                        "@babel/plugin-transform-runtime"
                    ],
                    cacheDirectory: false,
                    // See #6846 for context on why cacheCompression is disabled
                    cacheCompression: false,

                    // If an error happens in a package, it's possible to be
                    // because it was compiled. Thus, we don't want the browser
                    // debugger to show the original code. Instead, the code
                    // being evaluated would be much more helpful.
                    sourceMaps: true
                }
            };
            webpackConfig.externals = [
                function (context, request, callback) {
                    if (/@ui-lib\/core\/.*/.test(request)) {
                        if (request.replace("@ui-lib/core", "") === "") {
                            return callback(null, "var " + 'UiLibCore');
                        } else {
                            return callback(null, "var " + 'UiLibCore.' + request.replace("@ui-lib/core/", "").replace('/', '.'));
                        }
                    }
                    if (/@ui-lib\/custom-components\/.*/.test(request)) {
                        if (request.replace("@ui-lib/custom-components", "") === "") {
                            return callback(null, "var " + 'UiLibCustomComponents');
                        } else {
                            return callback(null, "var " + 'UiLibCustomComponents.' + request.replace("@ui-lib/custom-components/", "").replace('/', '.'));
                        }
                    }
                    callback();
                },
                { "omni-journey": "OmniJourney" }
            ];
            return webpackConfig;
        }
    }
};