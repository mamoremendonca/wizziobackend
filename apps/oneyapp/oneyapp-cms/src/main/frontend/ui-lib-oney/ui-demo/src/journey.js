import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRootHoc } from "omni-journey"
import SymCustomTable from "@ui-lib/oney/SymCustomTable"
import ActivityTimeline from "@ui-lib/oney/ActivityTimeline"
import Icon from "@ui-lib/core/Icon"
import CircularProgress from '@ui-lib/core/CircularProgress';
import Tooltip from "@ui-lib/core/Tooltip"
import Typography from "@ui-lib/core/Typography"
import DatePickerInput from "@ui-lib/oney/DatePickerInput"
import Grid from "@ui-lib/core/Grid"
import FileDropper from "@ui-lib/oney/FileDropper"
import FileUploadItem from "@ui-lib/oney/FileUploadItem"
import { FileStatus } from "@ui-lib/oney/FileUploadItem"
import { withStyles } from "@ui-lib/core/styles";
import _ from "lodash";
import { styles } from './styles'
import { PDFReader } from 'reactjs-pdf-reader';
import Button from "@ui-lib/core/Button";
// import moment from 'moment'
import { calcDayToDeliver, numberWithSpaces, solveStatecolor, timelineDemoData, tableData, tableHeader, formatBytes } from './utils/demo-utils'
import AutocompleteInput from "@ui-lib/oney/AutocompleteInput"
import AutocompleteSelect from "@ui-lib/oney/AutocompleteSelect"

class Journey extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      file: null,
    };
  }
      
      showComponent = event => {
        event.preventDefault();
        const elementId = event.target.closest("button").id
        this.setState({component: elementId})
        console.log(elementId);

      }

  handleComponentChange = () => value => {
    if (value) {
      this.setState({
        component: value,
      });
    } else {
      this.setState({
        component: undefined
      })
    }

    };



  getFileStatus = () => {
    if (_.isNil(this.state.isValidFile)) return null;
    else return this.state.isValidFile ? FileStatus.SUCCESS : FileStatus.ERROR;
  };

  // --------------------------------------------------------------------------------------------------------- Customize Table Cells
  getCustomHeaderRender = () => {
    let cellRenderStructure = {
      status: (value, row) => <span style={{ positon: 'relative' }}><Tooltip style={{ position: 'relative' }}><Icon>info</Icon></Tooltip></span>
    };

    return cellRenderStructure;
  }



  getCustomCellRenderStruc = () => {
    let cellRenderStructure = {
      status: (value, row) => <div onClick={() => console.log(row)} style={{ display: "block", height: 8, width: 8, borderRadius: '50%', margin: 'auto', backgroundColor: solveStatecolor(value) }}></div>,
      value: (value, row) => numberWithSpaces(value),
      endDate: (value, row) => calcDayToDeliver(value, row.endDate)
    };
    return cellRenderStructure;
  }
  // ------------------------------------------------------------------------------------------------------- Click functions
  clickDlFun = (annex) => {
    console.log('download' + annex.id)
  }

  clickCloseFun = (annex) => {
    console.log('delete' + annex.id)
  }

  // ----------------------------------- FILE DROP ____________________________--------------------

  handleDrop = files => {
    if (!_.isEmpty(files)) {
      const fileList = Object.keys(files).map(key => files.item(key));
      this.setState({ file: fileList[0] }, () =>
        // this.props.onFileChanged(this.state.file)
        console.log(files)
      );
    }
  };

  resetOpenFileDlg = ev => {
    ev.preventDefault();

    let inputFile = document.getElementById("input_open_file_ref");
    if (inputFile) {
      inputFile.value = "";
    }
    this.setState({ file: null }
    );
  };


  // ----------------------------------- FILE DROP *END*____________________________--------------------

  componentDidMount() {
    // For the table to work with dummy data, you have to update the state after the component is mounted.
    this.setState({
    });
  }

  render() {

    const components = [
      { value: 'Autocomplete Input', label: 'Autocomplete Input' },
      { value: 'File Dropper', label: 'File Dropper' },
      { value: 'Date Picker', label: 'Date Picker' },
      { value: 'Custom Table', label: 'Custom Table' },
      { value: 'Timeline', label: 'Timeline' },
      { value: 'PDF Reader', label: 'PDF Reader' },
    ];


    return (

      <div style={{ padding: 20 }}>

        <Grid item xs={12}>
          <Typography variant='h1'>Oney UI Demo</Typography>
          <Typography variant='h3'>Escolha um componente a ser exibido</Typography>
        </Grid>

        <div style={{paddingBottom: '30px', paddingTop: '20px'}}>
        <AutocompleteSelect 
          formControlProps={{ fullWidth: true }}
          label='Componentes'//TODO Refactor
          options={components}
          onChange={this.handleComponentChange()}
          placeholder={'Escolha uma opção...'}
        />
        </div>
        

        {this.state.component && this.state.component.value === 'Autocomplete Input' && <AutocompleteInput />}

        {this.state.component && this.state.component.value === 'File Dropper' &&
          <FileDropper
            files={this.state.file ? [this.state.file] : []}
            handleFilesAdded={this.handleDrop}
            getLabelFn={this.getLabelFn}
            // accept={".csv"}
            componentId="holidays-uploader"
          >
            {this.state.file && (
              <FileUploadItem
                fileName={this.state.file.name}
                iconString={"ic_tasks_check"}
                supportText={formatBytes(this.state.file.size)}
                status={this.getFileStatus()}
                endAdornment={
                  this.state.isFileUploading ? (
                    <CircularProgress
                      //   className={classes.progress}
                      size={24}
                    />
                  ) : (
                      <Icon
                        onClick={this.resetOpenFileDlg}
                      //   className={classes.button}
                      >
                        {"close"}
                      </Icon>
                    )
                }
              />
            )}
          </FileDropper>
        }

        {this.state.component && this.state.component.value === 'Date Picker' &&
          <Grid container spacing={40}>
            <Grid item xs={3}>
              <DatePickerInput onlyCalendar={true} applyFormat={true} applySpecificFormat={"DD/MM/YYYY"} onlySingleDay={true}></DatePickerInput>
            </Grid>
            <Grid item xs={3}>
              <DatePickerInput onlyCalendar={true} applyFormat={true} applySpecificFormat={"DD/MM/YYYY"} onlySingleDay={true}></DatePickerInput>
            </Grid>
          </Grid>
        }

        {this.state.component && this.state.component.value === 'Custom Table' &&
          <Grid item xs={12}>
            <SymCustomTable
              tableTitle={"Fila de trabalho"}
              data={tableData}
              headers={tableHeader.columns}
              // getLabelFn={(key, defaultLabel) => this.props.getLabel(key, defaultLabel)}
              rowsPerPage={10}
              rowsPerPageAll={500}
              totalOfRows={tableData.length}
              loadMoreLabel={"Load more"}
              loadAllLabel={"Load all"}
              emptyText={"Sem dados"}
              onRowClick={(event, row) => {
              }}
              customCellsConfig={this.getCustomCellRenderStruc}
              customHeaderConfig={this.getCustomHeaderRender}
              prefix={'ui-demo-table'}
              // status={this.props.errorsTableStatus}
              resetPageOnSort={true}
              onlyVisibleCols={true}
              allowFilterRecs={true}
              allowShowHideCols={false}
            />
          </Grid>
        }

        {this.state.component && this.state.component.value === 'Timeline' &&
          <ActivityTimeline downloadClick={(annex) => { this.clickDlFun(annex) }} closeClick={(annex) => { this.clickCloseFun(annex) }} data={timelineDemoData}></ActivityTimeline>
        }

        {this.state.component && this.state.component.value === 'PDF Reader' &&
          <PDFReader url="https://arxiv.org/pdf/quant-ph/0410100.pdf" />
        }
        
      </div>
    );
  }
}

const mapStateToProps = () => ({})

const mapDispatchToProps = dispatch => bindActionCreators(
  {},
  dispatch
);

export default withRootHoc(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Journey)));
