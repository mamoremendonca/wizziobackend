import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRootHoc } from "omni-journey"
import Typography from "@ui-lib/core/Typography"
import CustomButton from "@ui-lib/oney/CustomButton"

class Journey extends React.Component {

    render() {
        return (
            <div>
                <Typography component={"div"} variant={"h2"}>
                    Welcome to you {`oney2`} ui lib package
                </Typography>
                <br />
                <Typography variant={"h4"} component={"div"}>
                    You can import your components from the ui lib and see real time changes with this journey, ex: CustomButton ->
                    <CustomButton>Demo button</CustomButton>
                    
                </Typography>
                <br />
                <Typography variant={"body2"} component={"div"}>
                    The lib name you gave was {`ui-lib-oney`}. In order to use umd releases build the lib and upload the umd file to the content server {`(build/umd/ui-lib-oney2.umd.js)`}.
                    <br />
                    After that you need to change the craco.config and the index.html of the journey to support you ui lib package.
                    <br />
                    First  go to the craco.config.js of your journey and add the following code to the externals config before the 'callback()':
                    <br />
                    {`if (/@ui-lib\/oney\/.*/.test(request)) {
                        if (request.replace("@ui-lib/oney2", "") === "") {
                            return callback(null, "var " + 'ui-lib-oney');
                        } else {
                            return callback(null, "var " + 'ui-lib-oney.' + request.replace("@ui-lib/oney/", "").replace('/', '.'));
                        }
                    }`}
                    <br />
                    Secondly go to the index.html in the public folder of your journey and add a script with the location of the umd script uploaded early ex:
                    <br />
                    {`<script src="http://localhost:4500/content/wizzio/foundation/deliverables/ui-lib-oney2/1.0.0/ui-lib-oney.umd.js"></script>`}
                </Typography>
            </div>
        );
    }
}

const mapStateToProps = function ({ }) {
    return {
    };
};

const mapDispatchToProps = dispatch => bindActionCreators(
    {},
    dispatch
);

export default withRootHoc(connect(mapStateToProps, mapDispatchToProps)(Journey));
