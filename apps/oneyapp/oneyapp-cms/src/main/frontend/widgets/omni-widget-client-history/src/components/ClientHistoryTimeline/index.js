import React from "react";
import Timeline from "@ui-lib/oney/Timeline";
import Typography from "@ui-lib/core/Typography";
import { withStyles } from "@ui-lib/core/styles";
import { styles, ComponentName } from "./ClientHistoryTimelineStyles";

const ChannelType = {
  PERSONAL: 0,
  PHONE: 1,
  EMAIL: 2,
  LETTER: 3,
};

export const getChannelIcon = (channel) => {
  switch (channel) {
    case ChannelType.PERSONAL:
      return "person_outlined";
    case ChannelType.PHONE:
      return "call_outlined";
    case ChannelType.EMAIL:
      return "alternate_email";
    case ChannelType.LETTER:
      return "mail_outlined";
    default:
      return "smartphone";
  }
};

const getTimelineContent = (timelineData) => {
  return timelineData.length > 0
    ? {
        iconSize: "16",
        content: timelineData.map((item) => ({
          title: <Typography variant="h5">Análise de Reclamação</Typography>,
          date: "2020-06-22T15:00:41.076Z",
          selected: false,
          user: "ID#5123123",
          status: "Concluiiido",
          expandable: false,
          icon: getChannelIcon(ChannelType.PERSONAL),
          borderStyle: "solid", // or dashed
          iconColor: "#81BC00",
          borderColor: "#81bc00",
          lineColor: "#8d8d8d",
          items: [
            {
              header: "Atribuído a",
              content: "-",
            },
            {
              header: "Data limite",
              content: "13/09/20 às 18:00",
            },
          ],
          footer: {
            card: "Cartão Must 003002581",
            value: "1.203,20 €",
          },
          action: {
            title: "Nova Relacionada",
            callback: () => {
              alert("Criar nova reclamação");
            },
          },
        })),
      }
    : { iconSize: "16", content: [] };
};

const ClientHistoryTimeline = ({ classes, timelineData }) => {
  return (
    <div className={classes.root}>
      <Timeline data={getTimelineContent(timelineData)}></Timeline>
    </div>
  );
};

export default withStyles(styles, { name: ComponentName })(
  ClientHistoryTimeline
);
