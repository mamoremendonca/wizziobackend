const ComponentName = "ClientHistoryStyles";

const styles = (theme) => ({
  root: {
    width: "100%",
  },
  refresh: {
    transitionDuration: "0.8s",
    transitionProperty: "transform",
    "&:hover": {
      transform: "rotate(90deg)",
    },
  },
  utilityIcon: {
    marginLeft: 5,
    fontSize: 24,
    cursor: "pointer",
    padding: 5,
    "&:hover": { color: theme.palette.primary.main },
  },
  headerContainer: {
    margin: 16,
  },
});

export { styles, ComponentName };
