import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRootHoc } from "omni-widget";
import {getWidgetSettings, getReferenceData, getSuppliers, setWidgetDependencies} from "./actions";
import ClientHistoryWidget from "./components/ClientHistoryWidget";

class Widget extends React.Component {

	/**
	 * It will request all server dependencies.
	 */
	componentDidMount(){
		const { getWidgetSettings, getReferenceData, getSuppliers, HttpClient, widget } = this.props;
		getWidgetSettings(HttpClient, widget);
		getReferenceData(HttpClient);
		getSuppliers(HttpClient);
		widget.localSpinnerOpen = true;
	}

	/**
	 * To chek if all dependencies are loaded
	 * @param {*} prevProps 
	 */ 
	componentDidUpdate(prevProps){
		const {referenceData, suppliers, dependencies, setWidgetDependencies} = this.props;
		if(referenceData !== prevProps.referenceData && !dependencies.get('referenceData')){	
			setWidgetDependencies('referenceData');
		} else if (suppliers !== prevProps.suppliers && !dependencies.get('suppliers')){
			setWidgetDependencies('suppliers');
		}

	}

	render() {
		const {dependencies, widget}=this.props
		if(dependencies.every((v) => v)){
			widget.localSpinnerOpen = false;
		}
		return (
			<div  id="workqueue" style={{ display: 'flex', height: '100%' }}>
				{dependencies.every((v) => v) && <ClientHistoryWidget />}
			</div>
		)
	}
}

const mapStateToProps = ({
	widget,
	widget: { services, contentUrl},
	widgetStore: { dependencies},
	lovReducer:{referenceData, suppliers}	
  }) => ({
	widget,
	dependencies,
	contentUrl,
	referenceData,
	suppliers,
	HttpClient: services.HttpClient
  });

const mapDispatchToProps= dispatch => bindActionCreators({
    getWidgetSettings,
	getReferenceData,
	getSuppliers,
	setWidgetDependencies
}, dispatch);


export default withRootHoc(connect(mapStateToProps, mapDispatchToProps)(Widget));