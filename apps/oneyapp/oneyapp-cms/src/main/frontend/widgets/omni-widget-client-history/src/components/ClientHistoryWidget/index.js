// React & Others
import React, { Fragment, useEffect } from "react";
import ClientHistoryTimeline from "../ClientHistoryTimeline";
import HeaderBar from "@ui-lib/oney/HeaderBar";
import Grid from "@ui-lib/core/Grid";
import { withStyles } from "@ui-lib/core/styles";
import { styles, ComponentName } from "./ClientHistoryStyles";
import ClientHistoryNavigationBar from "../ClientHistoryNavigationBar";
import IconButton from "@ui-lib/core/IconButton";
import Icon from "@ui-lib/core/Icon";
import classNames from "@ui-lib/core/classNames";
import Typography from "@ui-lib/core/Typography";
import { withRootHoc } from "omni-widget";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getClaimList, getClaimTimeline } from "../../actions";
import moment from "moment";

const ClientHistoryWidget = ({
  HttpClient,
  I18nProvider,
  Notifications,
  claimList,
  claimTimeline,
  lastTimelineUpdate,
  getClaimList,
  getClaimTimeline,
  classes,
}) => {
  useEffect(() => {
    updateTimeline();
  }, []);

  const updateTimeline = () => {
    console.log("UPDATE TIMELINE");
    getClaimList(HttpClient);
    getClaimTimeline(HttpClient, "23huf23f");
  };

  return (
    <Fragment>
      <Grid className={classes.root}>
        <Grid className={classes.headerContainer}>
          <HeaderBar title={"Claim History"} primaryColor={false}>
            <Grid container justify={"flex-end"} alignItems={"center"}>
              <Typography>
                {moment(lastTimelineUpdate).format("HH:mm")}
              </Typography>
              <IconButton
                onClick={updateTimeline}
                className={classNames(classes.refresh, classes.utilityIcon)}
              >
                <Icon>autorenew</Icon>
              </IconButton>
            </Grid>
          </HeaderBar>
        </Grid>
        <ClientHistoryNavigationBar />
        <ClientHistoryTimeline timelineData={claimTimeline} />
      </Grid>
    </Fragment>
  );
};

const mapStateToProps = ({
  widget: { services },
  widgetClientHistoryReducer: { claimList, claimTimeline, lastTimelineUpdate },
}) => ({
  HttpClient: services.HttpClient,
  I18nProvider: services.I18nProvider,
  Notifications: services.Notifications,
  claimList,
  claimTimeline,
  lastTimelineUpdate,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getClaimList,
      getClaimTimeline,
    },
    dispatch
  );

export default withRootHoc(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(styles, { name: ComponentName })(ClientHistoryWidget))
);
