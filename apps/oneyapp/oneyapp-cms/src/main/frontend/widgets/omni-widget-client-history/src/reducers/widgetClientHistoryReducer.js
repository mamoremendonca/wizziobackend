import { SET_CLAIM_LIST, SET_CLAIM_TIMELINE } from "../constants";

const initialState = {
  claimList: {},
  claimTimeline: {},
  lastTimelineUpdate: new Date(),
};

export default function widgetClientHistoryReducer(
  state = initialState,
  action
) {
  switch (action.type) {
    case SET_CLAIM_LIST:
      return { ...state, claimList: action.claimList };
    case SET_CLAIM_TIMELINE:
      return {
        ...state,
        claimTimeline: action.claimTimeline,
        lastTimelineUpdate: new Date(),
      };
    default:
      return state;
  }
}
