import { SET_CLAIM_LIST, SET_CLAIM_TIMELINE } from "../constants";
import {
  getClaimListRequest,
  getClaimTimelineRequest,
} from "./services/widgetServices";

export const getClaimList = (HttpClient) => (dispatch) =>
  getClaimListRequest(HttpClient)
    .then((res) => {
      dispatch(setClaimList(res.data));
    })
    .catch(() => {});

const setClaimList = (data) => {
  return { type: SET_CLAIM_LIST, claimList: data };
};

export const getClaimTimeline = (HttpClient, clientId) => (dispatch) =>
  getClaimTimelineRequest(HttpClient, clientId)
    .then((res) => {
      dispatch(setClaimTimeline(res.data));
    })
    .catch(() => {});

const setClaimTimeline = (data) => {
  return { type: SET_CLAIM_TIMELINE, claimTimeline: data };
};
