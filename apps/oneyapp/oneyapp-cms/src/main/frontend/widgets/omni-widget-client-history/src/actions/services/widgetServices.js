import { Config } from "../../configs";

export const getClaimListRequest = (HttpClient) => {
  const config = {
    method: Config.METHOD_POST,
    url: `${Config.CLAIM_BASE_URL}${Config.CLAIM_REPOSITORY}${Config.CLAIM_LIST}`,
    data: {},
  };
  return HttpClient.request(config);
};

export const getClaimTimelineRequest = (HttpClient, clientId) => {
  const config = {
    method: Config.METHOD_GET,
    url: `${Config.CLAIM_BASE_URL}${Config.CLAIM_REPOSITORY}${Config.CLAIM_TIMELINE}${clientId}`,
  };
  return HttpClient.request(config);
};
