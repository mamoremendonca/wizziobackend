export const Config = {
  METHOD_POST: "post",
  METHOD_GET: "get",

  CLAIM_BASE_URL: "/bin/mvc.do/claims/v1",
  CLAIM_REPOSITORY: "/repository",
  CLAIM_LIST: "/list",
  CLAIM_TIMELINE: "/history?clientId=",
};
