import React from "react";
import Tabs from "@ui-lib/core/Tabs";
import Tab from "@ui-lib/core/Tab";
import Button from "@ui-lib/core/Button";
import Icon from "@ui-lib/core/Icon";
import Typography from "@ui-lib/core/Typography";
import Divider from "@ui-lib/core/Divider";

const ClientHistoryNavigationBar = () => {
  const [selectedTab, setSelectedTab] = React.useState(0);

  const handleChangeTab = (event, newTab) => {
    setSelectedTab(newTab);
  };

  return (
    <div>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "flex-end",
        }}
      >
        <div style={{ width: "100%", marginLeft: 16 }}>
          <Tabs
            value={selectedTab}
            onChange={handleChangeTab}
            indicatorColor="primary"
            textColor="primary"
            scrollButtons="auto"
            style={{ borderBottom: "none" }}
          >
            <Tab label="Todas (3)" />
            <Tab label="A decorrer (0)" />
            <Tab label="Concluídas (3)" />
          </Tabs>
        </div>
        <div style={{ paddingRight: 16 }}>
          <Button
            variant="contained"
            color="primary"
            style={{
              padding: "5px",
              minWidth: "145px",
              marginBottom: "15px",
            }}
          >
            <Icon
              style={{
                fontSize: "16px",
                marginRight: "5px",
              }}
            >
              add_circle_outline
            </Icon>
            <Typography variant="h5">Nova Reclamação</Typography>
          </Button>
        </div>
      </div>
      <Divider />
    </div>
  );
};

export default ClientHistoryNavigationBar;
