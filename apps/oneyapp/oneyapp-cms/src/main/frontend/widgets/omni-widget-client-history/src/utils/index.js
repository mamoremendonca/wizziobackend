export function prepareHeight(height) {
  if (typeof height === "string" && height.includes("px")) {
    let measure = parseInt(height.replace("px", ""));
    return !isNaN(measure) ? measure : 50;
  }

  return 50;
}
