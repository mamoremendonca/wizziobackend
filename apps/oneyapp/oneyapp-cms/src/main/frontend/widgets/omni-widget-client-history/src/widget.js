import React from 'react';
import { connect } from 'react-redux';
import { withRootHoc } from "omni-widget";
import ClientHistoryWidget from "./components/ClientHistoryWidget";

class Widget extends React.Component {

    render() {
        return (
            <div style={{ display: 'flex', height: '100%' }}>
               {<ClientHistoryWidget initialProps={this.props.initialProps} />}
            </div>
        )
    }

}

const mapStateToProps = (state, ownProps) => {
    return {
        JourneyActions: state.widget.services.JourneyActions,
        i18nProvider: state.widget.i18nProvider
    };
};

const mapDispatchToProps = function (dispatch, ownProps) {
    return {}
};

export default withRootHoc(connect(mapStateToProps, mapDispatchToProps)(Widget));