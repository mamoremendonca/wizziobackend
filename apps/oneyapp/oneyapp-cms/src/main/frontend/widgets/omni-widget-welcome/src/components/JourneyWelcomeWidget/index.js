// React & Others
import React, { Component } from "react";
import { connect } from "react-redux";
import JourneyWelcomeWidgetRender from "./journeyWelcomeWidgetRender";

class JourneyDialWidget extends Component {
  handleClick = () => {
    const { JourneyActions, initialProps } = this.props;
    JourneyActions.openJourneyWithArgs(
      initialProps && initialProps.journeyId,
      undefined
    );
  };

  render() {
    return (
      this.props.initialProps && (
        <JourneyWelcomeWidgetRender
          {...this.props}
          parentRef={this.welcome}
          user={JSON.parse(localStorage.getItem("currentLoggedUser"))}
          imageOnly={this.props.initialProps.journeyi18n === undefined}
          handleClick={this.handleClick}
        />
      )
    );
  }
}

const mapStateToProps = ({ widget: { services, height } }) => ({
  height,
  I18nProvider: services.I18nProvider,
  JourneyActions: services.JourneyActions,
});

const mapDispatchToProps = function (dispatch, ownProps) {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(JourneyDialWidget);
