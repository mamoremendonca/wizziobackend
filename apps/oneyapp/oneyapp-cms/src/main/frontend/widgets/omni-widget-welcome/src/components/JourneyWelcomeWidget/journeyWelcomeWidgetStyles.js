// React & Others
import { WIDGET_PADDING } from "../../constants";

const componentName = "JourneyWelcome";
const styles = (theme) => ({
  imageDiv: {
    backgroundSize: "cover",
    width: "100%" /* for IE 6 */,
  },
  image: {
    width: "100%",
    height: "100%",
  },
  welcomeDiv: {
    paddingLeft: "108px",
    paddingTop: "40px",
  },
  welcomeHeader: {
    fontSize: "36px",
    color: "#FFFFFF",
  },
  welcomeBody: {
    paddingTop: "50px",
    color: "#FFFFFF",
  },
});

export { componentName };
export { styles };
