// React & Others
import React, { useState, useEffect } from "react";

// UI
import Typography from "@ui-lib/core/Typography";
import { withStyles } from "@ui-lib/core/styles";
import { componentName, styles } from "./journeyWelcomeWidgetStyles";
import classNames from "@ui-lib/core/classNames";
import { WIDGET_PADDING, WIDGET_ICON_SIZE } from "../../constants";
import { prepareHeight } from "../../utils";

function JourneyWelcomeWidgetRender({
  height,
  imageOnly,
  initialProps: { imageURL, journeyi18n },
  I18nProvider,
  handleClick,
  classes,
  user,
}) {
  const [headerLabel, setHeaderLabel] = useState(<></>);
  const [bodyLabel, setBodyLabel] = useState(<></>);

  useEffect(() => {
    loadMessages();
    setInterval(loadMessages(), 300000);
  }, []);

  async function loadMessages() {
    try {
      // const res = await fetch("https://api.apijson.com/...");
      // const blocks = await res.json();

      let response = {
        // Boa tarde {0} // em que vai buscar o valor do array values que esteja na posição zero.
        headerLabel: {
          message: "welcome.message", //chave i18n
          attributes: [{ type: "string", value: user.firstName }], // cada value no array ira ser posiciando na mensagem
        },
        // para ficar em linhas diferentes3
        // Voce atingiu {0} da produtividade esta semana // em que vai buscar o valor do array values que esteja na posição zero.
        bodyLabel: {
          messages: [
            "productivity_message_part1",
            "productivity_message_part2",
          ], //chaves i18n
          attributes: [
            {
              type: "custom_string",
              value: 80,
              color: "#228B22",
              fontSize: 40,
            },
          ],
        },
      };

      setHeaderLabel(getHeaderLabel(response.headerLabel));
      // setBodyLabel(getBodyLabel(response.bodyLabel));

    } catch (e) {
      console.log(e);
    }
  }

  function getHeaderLabel(res) {
    let message = res.message || "no_value";
    let attribute = res.attributes[0].value || "no_value";

    return (
      <Typography
        variant="h1"
        color="primary"
        className={classes.welcomeHeader}
      >
        {/* {I18nProvider.Labeli18n(message, { name: attribute })} */}
        {I18nProvider.Texti18n(message)} {attribute}
      </Typography>
    );
  }

  // NOT FINISHED, awaiting client for further instructions
  function getBodyLabel(res) {
    let attributes = [];

    // format all attributes returned by the backend
    res.attributes.forEach((att) => {
      if (att.type === "custom_string") {
        attributes.push(
          <span style={{ color: att.color, fontSize: att.fontSize }}>
            {att.value}%
          </span>
        );
      }
    });


    return (
      <Typography variant="h2" color="primary" className={classes.welcomeBody}>
        {res.messages.forEach((msg, index) => {
          return (
            <>
              {index + 1 >= attributes.length
                ? I18nProvider.Labeli18n(msg, attributes[index])
                : I18nProvider.Labeli18n(msg)}{" "}
              <br />{" "}
            </>
          );
        })}
      </Typography>
    );
  }

  return (
    <div
      className={classes.imageDiv}
      style={{
        backgroundImage: `url(${imageURL})`,
      }}
    >
      <div className={classes.welcomeDiv}>
        {headerLabel}
        {bodyLabel}

        {/* <Typography
          variant="h1"
          color="primary"
          className={classes.welcomeHeader}
        >
          {I18nProvider.Labeli18n(journeyi18n)} {user.firstName}
        </Typography>

        <Typography
          variant="h2"
          color="primary"
          className={classes.welcomeBody}
        >
          {I18nProvider.Labeli18n(journeyi18n)}
        </Typography> */}
      </div>
    </div>
  );
}

export default withStyles(styles, { name: componentName })(
  JourneyWelcomeWidgetRender
);
