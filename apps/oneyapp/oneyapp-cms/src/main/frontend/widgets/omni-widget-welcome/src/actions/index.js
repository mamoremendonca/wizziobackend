import { SET_EXAMPLE } from "../constants";

export function example(value) {
    return {type : SET_EXAMPLE, payload : value};
}
