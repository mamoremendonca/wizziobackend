export const UserWorkQueueMock =
{
    "all": [
        {id : 1, status : "creating", processCategory:"PO", instanceId : "PO 000000001", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "11/02/2020", approvalDateLimit:'20/02/2020', department:"Departamento 110"},
        {id : 2, status : "approving", processCategory:"PO", instanceId : "PO 000000002", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "12/02/2020", approvalDateLimit:'25/02/2020', department:"Departamento 200"},
        {id : 3, status : "creating", processCategory:"PO", instanceId : "PO 000000003", entity: "Fornecedor 3", totalPrice: '5.0000€', createdAt: "13/02/2020", approvalDateLimit:'19/02/2020', department:"Departamento 234"},
        {id : 4, status : "approving", processCategory:"PO", instanceId : "PO 000000004", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "14/02/2020", approvalDateLimit:'31/02/2020', department:"Departamento 335"},
        {id : 5, status : "creating", processCategory:"PO", instanceId : "PO 000000005", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "30/01/2020", approvalDateLimit:'10/02/2020', department:"Departamento 223"},
        {id : 6, status : "approving", processCategory:"PO", instanceId : "PO 000000006", entity: "Fornecedor 3", totalPrice: '5.0000€', createdAt: "22/01/2020", approvalDateLimit:'05/02/2020', department:"Departamento 113"},
        {id : 7, status : "creating", processCategory:"PO", instanceId : "PO 000000007", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "16/02/2020", approvalDateLimit:'27/02/2020', department:"Departamento 478"},
        {id : 8, status : "approving", processCategory:"PO", instanceId : "PO 000000008", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "17/01/2020", approvalDateLimit:'30/01/2020', department:"Departamento 223"},
        {id : 9, status : "creating", processCategory:"PO", instanceId : "PO 000000009", entity: "Fornecedor 3", totalPrice: '5.0000€', createdAt: "21/12/2019", approvalDateLimit:'05/01/2020', department:"Departamento 100"},
        {id : 10, status : "approving", processCategory:"PO", instanceId : "PO 000000010", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "02/01/2020", approvalDateLimit:'02/01/2020', department:"Departamento 500"},
        {id : 11, status : "creating", processCategory:"PO", instanceId : "PO 0000000011", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "09/01/2020", approvalDateLimit:'09/01/2020', department:"Departamento 666"},
        {id : 12, status : "creating", processCategory:"DF", instanceId : "DF 000000001", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "11/02/2020", approvalDateLimit:'20/02/2020', department:"Departamento 787"},
        {id : 13, status : "approving", processCategory:"DF", instanceId : "DF 000000002", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "12/02/2020", approvalDateLimit:'25/02/2020', department:"Departamento 356"},
        {id : 14, status : "creating", processCategory:"DF", instanceId : "DF 000000003", entity: "Fornecedor 3", totalPrice: '5.0000€', createdAt: "13/02/2020", approvalDateLimit:'19/02/2020', department:"Departamento 100"},
        {id : 15, status : "approving", processCategory:"DF", instanceId : "DF 000000004", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "14/02/2020", approvalDateLimit:'31/02/2020', department:"Departamento 232"},
        {id : 16, status : "creating", processCategory:"DF", instanceId : "DF 000000005", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "30/01/2020", approvalDateLimit:'10/02/2020', department:"Departamento 122"},
        {id : 17, status : "approving", processCategory:"DF", instanceId : "DF 000000006", entity: "Fornecedor 3", totalPrice: '5.0000€', createdAt: "22/01/2020", approvalDateLimit:'05/02/2020', department:"Departamento 111"},
        {id : 18, status : "creating", processCategory:"DF", instanceId : "DF 000000007", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "16/02/2020", approvalDateLimit:'27/02/2020', department:"Departamento 100"},
        {id : 19, status : "approving", processCategory:"DF", instanceId : "DF 000000008", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "17/01/2020", approvalDateLimit:'30/01/2020', department:"Departamento 678"},
        {id : 20, status : "creating", processCategory:"DF", instanceId : "DF 000000009", entity: "Fornecedor 3", totalPrice: '5.0000€', createdAt: "21/12/2019", approvalDateLimit:'05/01/2020', department:"Departamento 454"},
        {id : 21, status : "approving", processCategory:"DF", instanceId : "DF 000000010", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "02/01/2020", approvalDateLimit:'02/01/2020', department:"Departamento 232"},
        {id : 22, status : "creating", processCategory:"DF", instanceId : "DF 0000000011", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "09/01/2020", approvalDateLimit:'09/01/2020', department:"Departamento 100"}
    ],
    "purchaseOrders": [
        {id : 1, status : "creating", processCategory:"PO", instanceId : "PO 000000001", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "11/02/2020", approvalDateLimit:'20/02/2020', department:"Departamento 110"},
        {id : 2, status : "approving", processCategory:"PO", instanceId : "PO 000000002", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "12/02/2020", approvalDateLimit:'25/02/2020', department:"Departamento 200"},
        {id : 3, status : "creating", processCategory:"PO", instanceId : "PO 000000003", entity: "Fornecedor 3", totalPrice: '5.0000€', createdAt: "13/02/2020", approvalDateLimit:'19/02/2020', department:"Departamento 234"},
        {id : 4, status : "approving", processCategory:"PO", instanceId : "PO 000000004", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "14/02/2020", approvalDateLimit:'31/02/2020', department:"Departamento 335"},
        {id : 5, status : "creating", processCategory:"PO", instanceId : "PO 000000005", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "30/01/2020", approvalDateLimit:'10/02/2020', department:"Departamento 223"},
        {id : 6, status : "approving", processCategory:"PO", instanceId : "PO 000000006", entity: "Fornecedor 3", totalPrice: '5.0000€', createdAt: "22/01/2020", approvalDateLimit:'05/02/2020', department:"Departamento 113"},
        {id : 7, status : "creating", processCategory:"PO", instanceId : "PO 000000007", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "16/02/2020", approvalDateLimit:'27/02/2020', department:"Departamento 478"},
        {id : 8, status : "approving", processCategory:"PO", instanceId : "PO 000000008", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "17/01/2020", approvalDateLimit:'30/01/2020', department:"Departamento 223"},
        {id : 9, status : "creating", processCategory:"PO", instanceId : "PO 000000009", entity: "Fornecedor 3", totalPrice: '5.0000€', createdAt: "21/12/2019", approvalDateLimit:'05/01/2020', department:"Departamento 100"},
        {id : 10, status : "approving", processCategory:"PO", instanceId : "PO 000000010", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "02/01/2020", approvalDateLimit:'02/01/2020', department:"Departamento 500"},
        {id : 11, status : "creating", processCategory:"PO", instanceId : "PO 0000000011", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "09/01/2020", approvalDateLimit:'09/01/2020', department:"Departamento 666"}
    ],
    "financialDocs": [
        {id : 1, status : "creating", processCategory:"PO", instanceId : "PO 0000000011", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "09/01/2020", approvalDateLimit:'09/01/2020', department:"Departamento 666"},
        {id : 2, status : "creating", processCategory:"DF", instanceId : "DF 000000001", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "11/02/2020", approvalDateLimit:'20/02/2020', department:"Departamento 787"},
        {id : 3, status : "approving", processCategory:"DF", instanceId : "DF 000000002", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "12/02/2020", approvalDateLimit:'25/02/2020', department:"Departamento 356"},
        {id : 4, status : "creating", processCategory:"DF", instanceId : "DF 000000003", entity: "Fornecedor 3", totalPrice: '5.0000€', createdAt: "13/02/2020", approvalDateLimit:'19/02/2020', department:"Departamento 100"},
        {id : 5, status : "approving", processCategory:"DF", instanceId : "DF 000000004", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "14/02/2020", approvalDateLimit:'31/02/2020', department:"Departamento 232"},
        {id : 6, status : "creating", processCategory:"DF", instanceId : "DF 000000005", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "30/01/2020", approvalDateLimit:'10/02/2020', department:"Departamento 122"},
        {id : 7, status : "approving", processCategory:"DF", instanceId : "DF 000000006", entity: "Fornecedor 3", totalPrice: '5.0000€', createdAt: "22/01/2020", approvalDateLimit:'05/02/2020', department:"Departamento 111"},
        {id : 8, status : "creating", processCategory:"DF", instanceId : "DF 000000007", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "16/02/2020", approvalDateLimit:'27/02/2020', department:"Departamento 100"},
        {id : 9, status : "approving", processCategory:"DF", instanceId : "DF 000000008", entity: "Fornecedor 2", totalPrice: '4.0000€', createdAt: "17/01/2020", approvalDateLimit:'30/01/2020', department:"Departamento 678"},
        {id : 10, status : "creating", processCategory:"DF", instanceId : "DF 000000009", entity: "Fornecedor 3", totalPrice: '5.0000€', createdAt: "21/12/2019", approvalDateLimit:'05/01/2020', department:"Departamento 454"},
        {id : 11, status : "approving", processCategory:"DF", instanceId : "DF 000000010", entity: "Fornecedor 1", totalPrice: '2.0000€', createdAt: "02/01/2020", approvalDateLimit:'02/01/2020', department:"Departamento 232"}
    ]
}; 