import React from "react";
import Tooltip from "@ui-lib/core/Tooltip";
import Icon from "@ui-lib/core/Icon";
import classNames from "@ui-lib/core/classNames";
import Typography from "@ui-lib/core/Typography";
import ProgressTooltip from "../ProgressTooltip";
import SortArrow from "../SortArrow";
import { i18nLabel } from "../../actions/utils";

function renderRows(
	data,
	cols,
	batchSize,
	hasMore,
	onClick,
	endMessage,
	classes
) {
	let renderedRows = data.map((row, index) => {
		return (
			<tr
				key={index}
				onClick={() => onClick(row)}
				className={classNames(classes.row, {
					[classes.evenRow]: index % 2 === 0
				})}
			>
				{cols.map((col, index) => {
					return (
						col.show && (
							<td
								key={index}
								className={classes.cell}
								style={{ width: col.width }}
							>
								{col.name ==='status'||col.name ==='approvalDateLimit'?(row[col.name]):(<Typography variant="body2">{row[col.name]}</Typography>)}
								
							</td>
						)
					);
				})}
			</tr>
		);
	});

	if (!hasMore && renderedRows.length > batchSize) {
		renderedRows.push(
			<tr key="no-more-items" style={{ height: 34 }}>
				<td colSpan={cols.length}>
					<Typography variant="h6" className={classes.endMessage}>
						{endMessage}
					</Typography>
				</td>
			</tr>
		);
	}

	return renderedRows;
}

function renderHeaders(
	cols,
	sortedField,
	sortFunc,
	i18nProvider,
	classes
) {
	return (
		<tr className={classes.headerRow}>
			{cols.map((col, index) => {
				if (col.show) {
					// if (col.name === "status") {
					// 	return (
					// 		<td
					// 			key={index}
					// 			className={classes.headerCellRoot}
					// 			style={{ width: col.width }}
					// 		>
					// 			<div className={classes.headerCellStatus}>
					// 				<Tooltip
					// 					title={<ProgressTooltip i18nProvider={i18nProvider} />}
					// 					placement="bottom-start"
					// 				>
					// 					<div
					// 						className={classes.ripple}
					// 						onClick={() => sortFunc(col.name, col.isTarget)}
					// 					>
					// 						<div className={classes.headerCellContent}>
					// 							<Icon color="primary" className={classes.infoIcon}>
					// 								info_outlined
                    //     </Icon>
					// 							{sortedField && sortedField.orderField === col.name && (
					// 								<SortArrow order={sortedField.orderType} />
					// 							)}
					// 						</div>
					// 					</div>
					// 				</Tooltip>
					// 			</div>
					// 		</td>
					// 	);
					// } else {
						return (
							<td
								key={index}
								className={classes.headerCellRoot}
								style={{ width: col.width }}
							>
								{/* <Tooltip
									title={
									i18nLabel(i18nProvider, `${col.i18n}_order`)}

									placement="bottom-start"
								> */}
								<Tooltip
									title={
										<Typography>
											{i18nLabel(i18nProvider, `${col.i18n}_order`)}
										</Typography>
									}

									placement="bottom-start"
								>
									<div
										className={classes.ripple}
										onClick={() => sortFunc(col.name, col.isTarget)}
									>
										<div className={classes.headerCellContent}>
											<Typography variant="h5" color="primary">
												{i18nLabel(i18nProvider, col.i18n)}
											</Typography>
											{sortedField && sortedField.orderField === col.name && (
												<SortArrow order={sortedField.orderType} />
											)}
										</div>
									</div>
								</Tooltip>
							</td>
						);
					// }
				} else {
					return null;
				}
			})}
		</tr>
	);
}

export { renderRows };
export { renderHeaders };
