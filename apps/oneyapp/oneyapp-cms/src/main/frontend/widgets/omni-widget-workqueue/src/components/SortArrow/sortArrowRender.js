import React from "react";
import Icon from "@ui-lib/core/Icon";
import { styles, ComponentName } from "./sortArrowStyles";
import { withStyles } from "@ui-lib/core/styles";
import { SORTING_ORDER } from "../../constants";

const SortArrowRender = ({ direction, classes }) => {
  switch (direction) {
    case SORTING_ORDER.DESC:
      return <Icon className={classes.sortIcon}>arrow_downward</Icon>;
    default:
      return <Icon className={classes.sortIcon}>arrow_upward</Icon>;
  }
};

export default withStyles(styles, { name: ComponentName })(SortArrowRender);
