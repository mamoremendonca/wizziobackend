export const SuppliersMock = [
{	id: "133701",
	code: "133701",
	name: "Fornecedor 1",
	description: "Fornecedor 1",
	tax: "12345678901"
}, {
	id: "133702",
	code: "133702",
	name: "Fornecedor 2",
	description: "Fornecedor 2",
	tax: "12345678902"
}, {
	id: "133703",
	code: "133703",
	name: "Fornecedor 3",
	description: "Fornecedor 3",
	tax: "12345678903"
}, {
	id: "133704",
	code: "133704",
	name: "Fornecedor 4",
	description: "Fornecedor 4",
	tax: "12345678904"
}, {
	id: "133705",
	code: "133705",
	name: "Fornecedor 5",
	description: "Fornecedor 5",
	tax: "12345678905"
}, {
	id: "133706",
	code: "133706",
	name: "Fornecedor 6",
	description: "Fornecedor 6",
	tax: "12345678906"
}, {
	id: "133707",
	code: "133707",
	name: "Fornecedor 7",
	description: "Fornecedor 7",
	tax: "12345678907"
}
];