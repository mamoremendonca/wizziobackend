import React, { Component } from "react";
import { connect } from "react-redux";
import UtilityPanelRender from "./utilityPanelRender";
import {
  clearWidgetData,
  updateFilter,
  setPanelHeight,
  togglePanel,
} from "../../actions";
import { getProcesses } from "../../actions/services/widgetServices";

/**
 * HOC to render a component with custom fields that is used to collect the filters used in the server request to get the filtered data.
 */
class UtilityPanel extends Component {
  // fetchProcesses = () => {
  // 	const { HttpClient, searchCriteria, getProcesses} = this.props;
  // 	getProcesses(HttpClient, searchCriteria);
  // };

  // /**
  //  * To change the filter fields in the component state.
  //  * For numeric values is not allowed other characters.
  //  */
  // refreshProcesses = () => {
  // 	const { clearWidgetData } = this.props;
  // 	clearWidgetData();
  // 	this.fetchProcesses();
  // };

  render() {
    return (
      <UtilityPanelRender
        {...this.props}
        // refreshProcesses={this.refreshProcesses}
      />
    );
  }
}

const mapStateToProps = ({
  widget: { services, isPrivate, i18nProvider, contentUrl },
  widgetStore: { isPanelOpen, settings, size, searchCriteria },
}) => ({
  HttpClient: services.HttpClient,
  i18nProvider,
  isPrivate,
  settings,
  size,
  searchCriteria,
  isPanelOpen,
  contentUrl,
});

const mapDispatchToProps = function (dispatch, ownProps) {
  return {
    togglePanel: () => dispatch(togglePanel()),
    getProcesses: (HttpClient, batchSize, searchCriteria) =>
      dispatch(getProcesses(HttpClient, batchSize, searchCriteria)),
    clearWidgetData: () => dispatch(clearWidgetData()),
    setPanelHeight: (height) => dispatch(setPanelHeight(height)),
    updateFilter: (newFilter) => dispatch(updateFilter(newFilter)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UtilityPanel);
