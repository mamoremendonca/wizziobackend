import { Config } from "../../config";
import { setProcesses, setWidgetSettings } from "../../actions";
import { prepareSearchCriteria, prepareSortCriteria } from "../utils"; //under folder actions

/**
 * Get the work queue for the logged user
 * The work queue has more than one type of process.
 * For example it could be purchase orders, financial documents or others.
 * @param {*} httpClient the widget services HttpClient that uses ufe axios implementation
 * @param {*} criteria  the criteria used to search by the suppliers like { TODO}
 */
export const getProcesses = (httpClient, criteria, widget) => {
  const config = {
    method: "post",
    url: Config.POST_WORK_LIST_URL + prepareSortCriteria(criteria.sortCriteria),
    data: prepareSearchCriteria(criteria),
  };
  return (dispatch) => {
    return httpClient
      .request(config)
      .then((res) => {
        const payLoad = {
          processes: { all: res.data },
          totals: { all: res.data.length },
        };
        dispatch(setProcesses(payLoad));
      })
      .catch((err) => {
        //TODO: httpErrorHandler(widget, err, false);
      });
  };
};

/**
 * Get widget settings located under the widget folder in the content SLING.
 * @param {*} httpClient the widget services HttpClient that uses ufe axios implementation
 * @param {*} widget
 */
export const getWidgetSettings = (httpClient, widget) => {
  const config = {
    method: "get",
    url: widget.contentUrl + Config.GET_SETTINGS_URL,
  };
  return (dispatch) => {
    return httpClient
      .request(config)
      .then((res) => {
        dispatch(setWidgetSettings(res.data));
      })
      .catch((err) => {
        //TODO: create a httpErrorHandler(widget, err, false);
      });
  };
};
