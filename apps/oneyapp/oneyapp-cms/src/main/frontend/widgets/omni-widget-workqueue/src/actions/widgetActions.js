import * as constants from '../constants';

export const setProcesses = (processes) => ({ type: constants.SET_PROCESSES, payload: processes });

export const setJourneyTypes = (journeyTypes) => ({ type: constants.SET_JOURNEY_TYPES, payload: journeyTypes });

export const setWidgetSettings = (widgetSettings) => ({ type: constants.SET_WIDGET_SETTINGS, payload: widgetSettings });

export const setTotals = (totals) => ({ type: constants.SET_TOTALS, payload: totals });

export const togglePanel = () => ({ type: constants.TOGGLE_PANEL})

export const clearWidgetData = () =>({type: constants.CLEAR_WIDGET_DATA});

export const setSize = (size) =>({ type: constants.SET_SIZE, payload: size });
  
export const setPanelHeight = (panelHeight) => ({ type: constants.SET_PANEL_HEIGHT, payload: panelHeight });

export const updateFilter = newFilter => ({ type: constants.UPDATE_FILTER_CRITERIA, payload: newFilter });

export const updateSorting = (fieldToSort) => ({ type: constants.UPDATE_SORTING_CRITERIA, payload: fieldToSort });

export const setWidgetDependencies = dependency =>({type: constants.SET_WIDGET_DEPENDENCY, payload:dependency});