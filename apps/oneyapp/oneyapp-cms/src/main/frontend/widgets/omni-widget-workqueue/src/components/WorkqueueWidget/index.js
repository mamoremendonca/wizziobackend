import React, { Component } from "react";
import { connect } from "react-redux";
import WorkqueueWidgetRender from "./workqueueWidgetRender";
import { setSize } from "../../actions"
//import { getFilterProviders, getJourneyTypes } from "../../actions/services";
import { prepareSize } from "../../actions/utils";

class WorkqueueWidget extends Component {
  constructor(props) {
    super(props);
    const {
      width,
      height,
      setSize,
      HttpClient
    } = props;
    setSize(prepareSize(width, height));
  }

  componentDidUpdate(prevProps) {
    const { width, height } = this.props;
    let newSize = prepareSize(width, height);

    if (
      newSize.width !== prevProps.width ||
      newSize.height !== prevProps.height
    ) {
      setSize(newSize.width, newSize.height);
    }
  }

  render() {
    const { size } = this.props;

    if (size && !isNaN(size.height) && !isNaN(size.width)) {
      return <WorkqueueWidgetRender />;
    } else {
      return <div />;
    }
  }
}

const mapStateToProps = ({
  widget: { services, height, width },
  widgetStore: { size, settings }
}) => ({
  HttpClient: services.HttpClient,
  height,
  width,
  size,
  settings
});

const mapDispatchToProps = function(dispatch, ownProps) {
  return {
    setSize: size => dispatch(setSize(size))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(WorkqueueWidget);
