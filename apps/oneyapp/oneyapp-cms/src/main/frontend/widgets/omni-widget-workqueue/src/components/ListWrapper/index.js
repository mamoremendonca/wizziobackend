import React, { Component } from "react";
import { connect } from "react-redux";
import ListWrapperRender from "./listWrapperRender";
import { getProcesses } from "../../actions/services/widgetServices";
import { updateSorting } from "../../actions";
import { formatData, getTabSpecificProcesses } from "../../actions/utils";

class ListWrapper extends Component {
  handleClick = (row) => {
    if (row.journeyId !== 0 && !isNaN(row.journeyId)) {
      this.props.JourneyActions.openJourneyWithArgs(row.journeyId, {
        journey: {
          instanceId: row.id,
          status: row.statusValue,
        },
      });
    }
  };

  handleSort = (fieldToSort) => {
    this.props.updateSorting(fieldToSort);
  };

  handleLoadMore = (batchSize) => {
    // const { settings, total } = this.props;

    // if (total + settings.batchSize > batchSize) {
    // 	this.requestData(batchSize);
    // }
    this.requestData(batchSize);
  };

  requestData = (batchSize) => {
    const {
      searchCriteria,
      HttpClient,
      getProcesses,
      //category
    } = this.props;

    // getProcesses(HttpClient, {...searchCriteria, filterCriteria: { ...searchCriteria.filterCriteria, category }});
    getProcesses(HttpClient, searchCriteria);
  };

  componentDidUpdate(prevProps) {
    const {
      searchCriteria,
      HttpClient,
      //  category,
      getProcesses,
    } = this.props;

    if (
      searchCriteria.sortCriteria.orderField !==
        prevProps.searchCriteria.sortCriteria.orderField ||
      searchCriteria.sortCriteria.orderType !==
        prevProps.searchCriteria.sortCriteria.orderType
    ) {
      getProcesses(HttpClient, searchCriteria);
    }
  }

  render() {
    const {
      settings,
      isPrivate,
      size: { width, height },
      processes,
      category,
      i18nProvider,
      total,
      isActive,
      isPanelOpen,
      lovReducer,
    } = this.props;

    let effectiveSize = {
      height:
        height -
        (48 + 16 * isPanelOpen) - // Widget Header
        40 - // Category Tabs height
        2 * 16 - // Widget Top and Bottom Margins
        getPanelHeight() * isPanelOpen, // Fitler Panel Height
      width,
    };

    let formattedData = formatData(
      getTabSpecificProcesses(processes, category),
      settings,
      i18nProvider,
      lovReducer
    );
    let newCols = computeColWidth(settings.queue.columns, effectiveSize);
    return total !== undefined ? (
      <div ref={this.listRef}>
        <ListWrapperRender
          i18nProvider={i18nProvider}
          isPrivate={isPrivate}
          data={formattedData}
          cols={newCols}
          batchSize={settings.batchSize}
          hasMore={formattedData.length < total}
          handleSort={this.handleSort}
          handleLoadMore={this.handleLoadMore}
          handleClick={this.handleClick}
          autoRefresh={settings.autoRefresh}
          isActive={isActive}
          size={effectiveSize}
        />
      </div>
    ) : (
      <div />
    );
  }
}

const mapStateToProps = ({
  widget: { services, isPrivate, isActive, i18nProvider },
  widgetStore: {
    settings,
    size,
    processes,
    isPanelOpen,
    filterPanelHeight,
    searchCriteria,
  },
  lovReducer,
}) => ({
  i18nProvider,
  HttpClient: services.HttpClient,
  JourneyActions: services.JourneyActions,
  isPrivate,
  isActive,
  settings,
  size,
  processes,
  searchCriteria,
  isPanelOpen,
  filterPanelHeight,
  lovReducer,
});

const mapDispatchToProps = function (dispatch, ownProps) {
  return {
    // TODO create methods to getTasks, getTotatlTasks and updateSorting
    getProcesses: (HttpClient, searchCriteria) =>
      dispatch(getProcesses(HttpClient, searchCriteria)),
    updateSorting: (field) => dispatch(updateSorting(field)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListWrapper);

function computeColWidth(cols, { width }) {
  let explicitWidth = 0;
  let newCols = [];

  cols.forEach((col) => {
    if (typeof col.width === "number") {
      explicitWidth += col.width;
      newCols.push({ ...col });
    }
  });

  let remainingWidth = width - explicitWidth;

  cols.forEach((col) => {
    if (typeof col.width === "string") {
      let percentage = 0.1;
      let stringPercentage = col.width.split("%");
      if (stringPercentage.length > 0) {
        percentage = parseInt(stringPercentage[0]) / 100;
      }
      newCols.push({ ...col, width: percentage * remainingWidth });
    }
  });

  return newCols;
}

function getPanelHeight() {
  let panelEl = document.getElementById("filter-panel-details");

  if (panelEl) {
    let rect = panelEl.getBoundingClientRect();
    if (rect) {
      return rect.height;
    }
  }

  return 120;
}
