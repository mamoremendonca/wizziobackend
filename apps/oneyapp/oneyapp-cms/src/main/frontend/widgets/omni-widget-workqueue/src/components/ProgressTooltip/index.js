import React from "react";
import ProgressTooltipRender from "./progressTooltipRender";

const ProgressTooltip = ({ i18nProvider, theme }) => {
  return <ProgressTooltipRender i18nProvider={i18nProvider} theme={theme} />;
};

export default ProgressTooltip;
