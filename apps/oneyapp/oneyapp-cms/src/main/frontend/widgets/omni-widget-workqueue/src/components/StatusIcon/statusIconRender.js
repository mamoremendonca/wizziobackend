import React from "react";
import { styles, ComponentName } from "./statusIconStyles";
import { withStyles } from "@ui-lib/core/styles";
import classNames from "@ui-lib/core/classNames";
import { PROCESS_STATUS } from "../../constants";

const StatusIconRender = ({ status, classes }) => {
  let colorClass;
  switch (status) {
    case PROCESS_STATUS.DIGITAL_TEAM:
      colorClass = classes.digitalTeam;
      break;
    case PROCESS_STATUS.CREATING:
    case PROCESS_STATUS.IN_CREATION:
      colorClass = classes.active;
      break;
    case PROCESS_STATUS.APPROVING:
      colorClass = classes.approving;
      break;
    case PROCESS_STATUS.REJECTED:
      colorClass = classes.rejected;
      break;
    default:
  }

  return <div className={classNames(classes.base, colorClass)} />;
};

export default withStyles(styles, { name: ComponentName })(StatusIconRender);
