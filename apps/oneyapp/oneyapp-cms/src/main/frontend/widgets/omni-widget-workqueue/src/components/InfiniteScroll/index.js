// React
import React, { Component, Fragment } from "react";

// UI
import PropTypes from "react-proptypes";
import { renderHeaders, renderRows } from "./infiniteScrollRender";
import { styles, ComponentName } from "../InfiniteScroll/infiniteScrollStyles";
import { withStyles } from "@ui-lib/core/styles";
import Typography from "@ui-lib/core/Typography";

// Utils
import { nextBatchSize } from "./utils";

// Constants
import { SORTING_ORDER, DEFAULT_SORTED_FIELD } from "../../constants";

class InfiniteScroll extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sortedField: props.defaultSortedField
        ? props.defaultSortedField
        : { orderField: DEFAULT_SORTED_FIELD["orderField"], orderType: DEFAULT_SORTED_FIELD["orderType"]}
    };

    this.tableRef = React.createRef();
    this.refreshInterval = null;
  }

  loadMoreData = (force = false) => {
    const { data, batchSize, hasMore, loadMore } = this.props;
        
    if (hasMore === true || force === true) {
      loadMore(nextBatchSize(data.length, batchSize));
    }
  };

  handleScroll = () => {
    if (
      this.tableRef.current.scrollTop + this.tableRef.current.clientHeight >=
      this.tableRef.current.scrollHeight
    ) {
      this.loadMoreData();
    }
  };

  sortField = (fieldName, isTarget) => {
    const { data, sort } = this.props;
    //hack to no apply sort in certain columns TODO: change render to use a prop isSortAble
    if (fieldName !== 'entity' && fieldName !== 'department'){
      this.setState(
        {
          sortedField: {
            orderField: fieldName,
            orderType:
              this.state.sortedField.orderType === SORTING_ORDER.DESC
                ? SORTING_ORDER.ASC
                : SORTING_ORDER.DESC,
          }
        },
        () => {
          sort(this.state.sortedField, data.length);
        }
      );
    }    
  };

  render() {
    const {
      data,
      cols,
      batchSize,
      size,
      hasMore,
      onClick,
      endMessage,
      i18nProvider,
      classes
    } = this.props;

    const renderedRows = renderRows(
      data,
      cols,
      batchSize,
      hasMore,
      onClick,
      endMessage,
      classes
    );
    const renderedHeaders = renderHeaders(
      cols,
      this.state.sortedField,
      this.sortField,
      i18nProvider,
      classes
    );


    let noResults = renderedRows.length === 0;

    return (
      <Fragment>
        <table className={classes.table}>
          <thead className={classes.tableHeader}>
            {renderedHeaders}
          </thead>
          <tbody
            ref={this.tableRef}
            className={classes.tableBody}
            style={{ height: noResults ? 0 : size.height - 50 }}
          >
            {!noResults && renderedRows}
          </tbody>
        </table>
        {noResults && (
          <div className={classes.noResultsMessage}>
            <Typography variant="h4">{endMessage}</Typography>
          </div>
        )}
      </Fragment>
    );
  }

  componentDidMount() {
    const { batchSize, autoRefresh } = this.props;
    if (isFinite(batchSize) && batchSize > 0) {
      if (
        autoRefresh &&
        Object.keys(autoRefresh).length > 0 &&
        autoRefresh.active === true &&
        this.refreshInterval === null
      ) {
        this.triggerAutoRefresh(true);
      } else {
        this.loadMoreData();
      }
    }

    this.tableRef.current.addEventListener("scroll", this.handleScroll);
  }

  componentDidUpdate(prevProps) {
    const { autoRefresh, data } = this.props;

    if (prevProps.data.length > data.length) {
      this.tableRef.current.scrollTop = 0;
    }

    if (prevProps.autoRefresh.active !== autoRefresh.active) {
      this.triggerAutoRefresh(autoRefresh.active);
    }
  }

  triggerAutoRefresh = status => {
    const {
      autoRefresh: { interval, active }
    } = this.props;
    if (status === true) {
      if (active === true) {
        this.loadMoreData(true);

        if (isFinite(interval) && interval > 0) {
          this.refreshInterval = window.setInterval(
            () => this.loadMoreData(true),
            interval
          );
        }
      }
    } else if (this.refreshInterval !== null) {
      window.clearInterval(this.refreshInterval);
      this.refreshInterval = null;
    }
  };

  componentWillUnmount() {
    this.tableRef.current.removeEventListener("scroll", this.handleScroll);
    this.triggerAutoRefresh(false);
  }
}

InfiniteScroll.propTypes = {
  data: PropTypes.array.isRequired,
  cols: PropTypes.array.isRequired,
  size: PropTypes.object.isRequired,
  sort: PropTypes.func.isRequired,
  loadMore: PropTypes.func.isRequired,
  i18nProvider: PropTypes.any.isRequired
};

export default withStyles(styles, { name: ComponentName })(InfiniteScroll);
