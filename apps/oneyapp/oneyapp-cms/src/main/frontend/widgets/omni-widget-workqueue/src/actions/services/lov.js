import { Config } from "../../config";
import { setSuppliers, setReferenceData } from "../../actions";
import { REFERENCE_DATA_MAPPER } from "../../constants";
/**
 * This maps the reference data as is from backend and fill the properties value and label to be used on the AutocompleteSelect component
 */
const mappingLovToDataSources = (lov) => {
  const dataSource = [];
  lov.forEach((element) => {
    dataSource.push({
      ...element,
      value: element.code,
      label: element.description,
    });
  });
  return dataSource;
};
/**
 * This maps the supplierList data as is from backend and fill the properties value and label to be used on the AutocompleteSelect component
 * @param {*} suppliersList
 */
const mappingSuppliersToDataSources = (suppliersList) => {
  const dataSource = [];
  suppliersList.forEach((element) => {
    dataSource.push({ ...element, value: element.code, label: element.name });
  });
  return dataSource;
};

/**
 * This maps the departments data as is from backend and fill the properties value and label to be used on the AutocompleteSelect component
 * @param {*} departments
 */
const mappingDepartmentsToDataSources = (departments) => {
  const dataSource = [];
  departments.forEach((element) => {
    dataSource.push({ ...element, value: element.code, label: element.description });
  });
  return dataSource;
};

/**
 *  To get the LOVS or also called reference data
 *  Project status
 *  Categories used on the tabs
 *  OwnerPositions like if is manager, chief financial officer, director general
 * @param {*} httpClient the widget services HttpClient that uses ufe axios implementation
 * @param {*} criteria  the criteria used to search by the suppliers like { TODO}
 */
export const getReferenceData = (httpClient) => {
  const config = {
    method: "post",
    url: Config.POST_REFERENCE_DATA_URL,
    data: Config.POST_REFERENCE_DATA_BODY,
  };
  return (dispatch) => {
    httpClient
      .request(config)
      .then((res) => {
        const newReferenceData = {};
        newReferenceData.status = mappingLovToDataSources(
          res.data[REFERENCE_DATA_MAPPER.STATUS]
        );
        newReferenceData.journeyType = mappingLovToDataSources(
          res.data[REFERENCE_DATA_MAPPER.JOURNEY_TYPE]
        );

        newReferenceData.department = mappingDepartmentsToDataSources(
          res.data[REFERENCE_DATA_MAPPER.DEPARTMENT]
        );
        dispatch(setReferenceData(newReferenceData));
      })
      .catch((err) => {
        //TODO: httpErrorHandler(widget, err, false);
      });
  };
};
/**
 * To get all available suppliers.
 * @param {*} httpClient
 */
export const getSuppliers = (httpClient) => {
  const config = {
    method: "post",
    url: Config.POST_SUPPLIERS_URL,
    data: {},
  };
  return (dispatch) => {
    httpClient
      .request(config)
      .then((res) => {
        const suppliersData = mappingSuppliersToDataSources(res.data);
        dispatch(setSuppliers(suppliersData));
      })
      .catch((err) => {
        //TODO: httpErrorHandler(widget, err, false);
      });
  };
};
