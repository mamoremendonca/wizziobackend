import * as constants from '../constants';

const initialState = {
    referenceData: {},
    suppliers: []
};

export const lovReducer = (state, action) => {

    if (state === undefined)
        return initialState;
    switch (action.type) {
        case constants.SET_REFERENCE_DATA: {
            return { ...state, referenceData: action.payload };
        } case constants.SET_SUPPLIERS: {            
            return { ...state, suppliers: action.payload };
        }
        default:
            return state;
    }
}
