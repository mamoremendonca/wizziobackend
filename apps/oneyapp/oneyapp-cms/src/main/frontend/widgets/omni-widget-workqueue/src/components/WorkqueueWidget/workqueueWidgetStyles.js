const ComponentName = "WorkqueueWidget";
const styles = theme => ({
  root: {
    width: "100%",
    height: "100%",
    padding: 16,
    boxSizing: "border-box",
    backgroundColor: theme.palette.background.default,
    display: "flex",
    flexDirection: "column",
    position: "relative",
    overflow: "hidden"
  }
});

export { ComponentName };
export { styles };
