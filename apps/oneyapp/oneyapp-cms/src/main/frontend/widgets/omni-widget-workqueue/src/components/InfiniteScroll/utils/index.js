/**
 * Computes the size of the next batch of entries to request
 * @param {Number} currentBatchSize
 * @param {Number} batchSize
 */
export function nextBatchSize(currentBatchSize, batchSize) {
    let fraction = currentBatchSize / batchSize;
    let remainder = currentBatchSize % batchSize;
  
    if (remainder === 0 || currentBatchSize === 0) {
      return (Math.ceil(fraction) + 1) * batchSize;
    } else {
      return Math.ceil(fraction) * batchSize;
    }
  }