const ComponentName = "UtiliyPanel";
const styles = theme => ({
  root: {
    width: "100%",
    boxShadow: "none"
  },
  title: {
    /*     position: "absolute",
    zIndex: 1000,
    top: 10 */
  },
  panelSummaryContent: {
    backgroundColor: theme.palette.background.default,
    padding: 0,
    margin: 0,
    "& > :last-child": {
      paddingRight: 0
    },
    minHeight: 48
  },
  panelSummaryRoot: {
    backgroundColor: theme.palette.background.default,
    padding: 0,
    margin: 0,
    cursor: "initial !important", //because mui not disabled
    minHeight: 48
  },
  panelDetailsRoot: {
    backgroundColor: theme.palette.background.default,
    padding: "0p, 16px",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  panelSummaryExpanded: {
    minHeight: 48
  },
  utilities: {
    padding: 0
  },
  utilityIcon: {
    fontSize: 24,
    cursor: "pointer",
    padding: 5,
    "&:hover": { color: theme.palette.primary.main }
  },
  private: {
    backgroundColor: theme.palette.grey[200]
  },
  inputField: {
    minWidth: 50,
    maxWidth: 200,
    margin: 8
  },
  panelButtons: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  button: {
    padding: "8px 0px",
    marginLeft:"20px"
  },
  filterButon: {
    marginLeft: 8
  },
  collapsible: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%"
  },
  refresh: {
    transitionDuration: "0.8s",
    transitionProperty: "transform",
    "&:hover": {
      transform: "rotate(90deg)"
    }
  },
  header: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  headerTitle: {},
  filterPanel: {
    width: "100%",
    display: "flex",
    flexDirection: "column"
  },
  filterActionsRoot: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end"
  },
  filterActionsContent: {
    display: "flex",
    maxHeight: 40,
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  filterFormRoot: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    marginRight: 16
  },
  filterFormContent: {
    width: "100%",
    display: "flex",
    flexWrap: "wrap"
  },
  dateInputField: {
    width: 200,
    paddingTop: 11,
    margin: 8
  }
});

export { styles };
export { ComponentName };
