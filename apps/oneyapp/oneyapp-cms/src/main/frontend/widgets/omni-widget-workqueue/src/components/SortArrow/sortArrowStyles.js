const ComponentName = "SortArrow";
const styles = theme => ({
  sortIcon: {
    fontSize: 16,
    color: theme.palette.primary[700],
    marginLeft: 5
  }
});

export { ComponentName };
export { styles };
