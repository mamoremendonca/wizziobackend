import React from "react";
import SortArrowRender from "./sortArrowRender";

const SortArrow = ({ order, services }) => {
  return <SortArrowRender direction={order} services={services} />;
};

export default SortArrow;
