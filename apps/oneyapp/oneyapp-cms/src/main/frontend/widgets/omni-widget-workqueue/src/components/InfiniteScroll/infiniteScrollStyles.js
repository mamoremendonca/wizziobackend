const ComponentName = "InfiniteScroll";
const styles = theme => ({
  table: {
    width: "100%",
    borderSpacing: 0
  },
  tableHeader: {
    display: "block"
  },
  endMessage: {
    display: "flex",
    justifyContent: "center"
  },
  noResultsMessage: {
    width: "100%",
    height: 80,
    alignItems: "center",
    display: "flex",
    justifyContent: "center"
  },
  tableBody: {
    width: "100%",
    overflowY: "auto",
    display: "block"
  },
  headerRow: {
    height: 48
  },
  evenRow: {
    backgroundColor: theme.palette.grey[100]
  },
  row: {
    "&:hover": {
      backgroundColor: theme.palette.grey[300],
      cursor: "pointer"
    }
  },
  headerCellStatus: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    height: "100%"
  },
  headerCellRoot: {
    height: 48,
    borderBottom: `1px solid ${theme.palette.grey[300]}`,
    padding:0
  },
  headerCellContent: {
    width: "100%",
    padding: 0,
    display: "flex"
  },
  cell: {
    padding: "8px 0px",
    margin: 0
  },
  infoIcon: {
    fontSize: 16
  },
  ripple: {
    backgroundPosition: "center",
    transition: "background 1.2s",
    cursor: "pointer",
    display: "flex",
    alignItems: "center",
    width: "100%",
    height: "100%",

    "&:hover": {
      background: `${theme.palette.grey[50]} radial-gradient(circle, transparent 1%, ${theme.palette.grey[300]} 1%) center/15000%`
    },
    "&:active": {
      backgroundColor: theme.palette.grey[100],
      backgroundSize: "100%",
      transition: "background 0s"
    }
  }
});

export { ComponentName };
export { styles };
