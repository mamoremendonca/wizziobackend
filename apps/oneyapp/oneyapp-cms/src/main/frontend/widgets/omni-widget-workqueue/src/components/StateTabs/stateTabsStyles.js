const ComponentName = "StateTabs";
const styles = theme => ({
  root: {
    width: "100%"
  },
  tabHeader: {
    display:"flex", 
    flexDirection:"row", 
    justifyContent:"space-between", 
    alignItems:"center"
  },
  toolTipRecordNumber: {
    display:"flex",
    flexDirection:"row",
    justifyContent:"flexStart",
    alignItems:"center"
  },
  tabsRoot: {
    minHeight: 40
  },
  tab: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  activeTab: {
    fontWeight: "bold"
  },
  indicator: {
    marginLeft: 3
  },
  tabRoot: {
    minHeight: 40
  }
});

export { ComponentName };
export { styles };
