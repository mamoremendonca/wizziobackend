import { REFERENCE_DATA_MAPPER } from "./constants";

export const Config = {
  USE_MOCKS: true,
  POST_SUPPLIERS_URL: "/bin/mvc.do/oneyapp/v1/reference/supplier",
  POST_WORK_LIST_URL: "/bin/mvc.do/oneyapp/v1/worklist",
  POST_REFERENCE_DATA_URL: "/bin/mvc.do/oneyapp/v1/reference/lists",
  POST_REFERENCE_DATA_BODY: [
    REFERENCE_DATA_MAPPER.STATUS,
    REFERENCE_DATA_MAPPER.JOURNEY_TYPE,
    REFERENCE_DATA_MAPPER.DEPARTMENT,
  ],
  GET_SETTINGS_URL:
    "/foundation/v3/widgets/omni-widget-workqueue/settings.json",
};
