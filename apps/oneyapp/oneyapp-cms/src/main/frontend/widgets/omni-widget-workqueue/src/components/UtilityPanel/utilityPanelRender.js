import React from "react";
import Typography from "@ui-lib/core/Typography";
import ExpansionPanel from "@ui-lib/core/ExpansionPanel";
import ExpansionPanelDetails from "@ui-lib/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@ui-lib/core/ExpansionPanelSummary";
import Icon from "@ui-lib/core/Icon";
import IconButton from "@ui-lib/core/IconButton";
import classNames from "@ui-lib/core/classNames";
import { styles, ComponentName } from "./utilityPanelStyles";
import { withStyles } from "@ui-lib/core/styles";
import FilterFields from "../FilterFields";


const UtilityPanelRender = ({
	i18nProvider,
	isPrivate,
	isPanelOpen,
	togglePanel,
	applyFilter,
	clearFilter,
	classes
}) => {

	return (
		<ExpansionPanel expanded={isPanelOpen} className={classes.root}>
			<ExpansionPanelSummary
				style={{ minHeight: 48, height: 48 }}
				classes={{
					root: classes.panelSummaryRoot,
					content: classes.panelSummaryContent,
					expanded: classes.panelSummaryExpanded
				}}
			>
				<div
					className={classes.collapsible}
					style={{
						justifyContent: isPrivate && "unset"
					}}
				>
					<Typography variant="h2" className={classes.title}>
						{/* {i18nLabel(i18nProvider, "workqueue_title")} */}
						{i18nProvider.Texti18n("workqueue_title")}
					</Typography>
					<div className={classes.utilities}>
						<IconButton
							onClick={togglePanel}
							color={isPanelOpen ? "primary" : undefined}
							className={classes.utilityIcon}
						>
							<Icon className="icon-filter" />
						</IconButton>
					</div>
				</div>
			</ExpansionPanelSummary>
			<ExpansionPanelDetails
				id="filter-panel-details"
				classes={{ root: classes.panelDetailsRoot }}
			>
				<div className={classes.filterPanel}>
					<FilterFields />
				</div>
			</ExpansionPanelDetails>
		</ExpansionPanel>
	);
};

export default withStyles(styles, { name: ComponentName })(UtilityPanelRender);
