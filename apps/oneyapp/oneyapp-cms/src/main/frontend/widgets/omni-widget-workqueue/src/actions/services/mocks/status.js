export const StatusMock = [
    {
        "code": "Draft",
        "description": "Em Criação",
    },
    {
        "code": "In_Approval",
        "description": "Em aprovação",
    },
    {
        "code": "Rejected",
        "description": "Rejeitado",
    }
];