export const SET_REFERENCE_DATA = "SET_REFERENCE_DATA";
export const SET_SUPPLIERS = "SET_SUPPLIERS";

//Unknown
export const SET_SIZE = "SET_SIZE";
export const SET_PANEL_HEIGHT = "SET_PANEL_HEIGHT";
//Search Criteria

export const SORTING_ORDER = {
  ASC: "ASC",
  DESC: "DESC",
};

export const DEFAULT_SORTED_FIELD = {
  orderField: "approvalDateLimit",
  orderType: SORTING_ORDER.ASC,
};

export const SORT_FIELDS_MAPPER = {
  status: "STATUS",
  processCategory: "JOURNEY_TYPE",
  instanceId: "NUMBER",
  entity: "ENTITY_CODE",
  totalPrice: "VALUE",
  createdAt: "CREATION_DATE",
  approvalDateLimit: "EXPIRATION_DATE",
  department: "DEPARTMENT_CODE",
};

//Filter Area
export const CLEAR_WIDGET_DATA = "CLEAR_WIDGET_DATA";
export const TOGGLE_PANEL = "TOGGLE_PANEL";
export const SET_FILTER_SUPPLIERS_DATA = "SET_FILTER_SUPPLIERS_DATA";
export const UPDATE_FILTER_CRITERIA = "UPDATE_FILTER_CRITERIA";
export const UPDATE_SORTING_CRITERIA = "UPDATE_SORTING_CRITERIA";
export const SET_FILTER_PROVIDERS = "SET_FILTER_PROVIDERS";

//Widget Management
export const SET_PROCESSES = "SET_PROCESSES";
export const SET_JOURNEY_TYPES = "SET_JOURNEY_TYPES";
export const SET_WIDGET_SETTINGS = "SET_WIDGET_SETTINGS";
export const SET_TOTALS = "SET_TOTALS";

//Business
export const PROCESS_CATEGORY = {
  PURCHASE_ORDERS: "PURCHASE_ORDERS",
  FINANCIAL_DOCS: "FINANCIAL_DOCS",
  ALL: "ALL",
};

export const PROCESS_CATEGORY_SINGULAR = {
  PO: "PURCHASE_ORDER",
  FD: "FINANCIAL_DOCUMENT",
};

export const PROCESS_STATUS = {
  DIGITAL_TEAM: "DigitalTeam",
  CREATING: "Draft",
  IN_CREATION: "In Creation",
  APPROVING: "In Approval",
  REJECTED: "Rejected",
  ALL: "all",
};

//Tab Navigation
export const CATEGORY_TABS = {
  [PROCESS_CATEGORY.ALL]: 0,
  [PROCESS_CATEGORY.PURCHASE_ORDERS]: 1,
  [PROCESS_CATEGORY.FINANCIAL_DOCS]: 2,
};

export const REFERENCE_DATA_MAPPER = {
  STATUS: "PURCHASE_ORDER_WORK_LIST_STATUS",
  JOURNEY_TYPE: "JOURNEY_TYPE",
  DEPARTMENT: "DEPARTMENT",
};

export const SET_WIDGET_DEPENDENCY = "SET_WIDGET_DEPENDENCY";
