import * as constants from "../constants";
import { Map } from "immutable";
const initialFilterCriteria = {
  status: "",
  number: "",
  supplier: "",
  minValue: "",
  maxValue: "",
  creationDate: "",
  expirationDate: "",
  department: "",
};

const initialState = {
  dependencies: Map({
    settings: false,
    suppliers: false,
    referenceData: false,
  }),
  settings: {}, // Widget settings (on content server)
  size: {}, // Size of the widget on the Dashboard (width and height)
  totals: { purchaseOrders: 0, financialDocs: 0, all: 0 }, // Total number of purchase order, financial docs processes
  processes: [], // Processes (from server)
  journeyTypes: {}, // Process types (from server)

  //Filter Panel
  searchCriteria: {
    filterCriteria: { ...initialFilterCriteria },
    sortCriteria: {
      orderField: constants.DEFAULT_SORTED_FIELD["orderField"],
      orderType: constants.DEFAULT_SORTED_FIELD["orderType"],
    },
  }, // Process search criteria
  isPanelOpen: false, // Whether the utility panel is open or not
  filterPanelHeight: 0, // Height (in px) of the utility panel
};
/**
 * This is a store used to handle the state of the widget
 * For example to know wich filter has been applied.
 * The timestamp of the last time that data was refreshed.
 * To control the layout of the infinite scroll table.
 * @param {*} state
 * @param {*} action
 */
export const widgetReducer = (state, action) => {
  if (state === undefined) return initialState;

  switch (action.type) {
    case constants.SET_PROCESSES:
      return {
        ...state,
        processes: action.payload.processes,
        totals: action.payload.totals,
      };
    case constants.SET_TOTALS:
      return { ...state, totals: action.payload };
    case constants.SET_WIDGET_SETTINGS:
      return {
        ...state,
        settings: action.payload,
        dependencies: state.dependencies.set("settings", true),
      };
    case constants.SET_JOURNEY_TYPES:
      return { ...state, workQueueTypes: action.payload };
    case constants.SET_SIZE:
      return { ...state, size: action.payload };
    case constants.CLEAR_WIDGET_DATA:
      return {
        ...state,
        processes: [],
        totals: { purchaseOrders: 0, financialDocs: 0, all: 0 },
      };
    case constants.TOGGLE_PANEL:
      return { ...state, isPanelOpen: !state.isPanelOpen };
    case constants.SET_PANEL_HEIGHT:
      return { ...state, filterPanelHeight: action.payload };
    case constants.UPDATE_FILTER_CRITERIA:
      return {
        ...state,
        searchCriteria: {
          ...state.searchCriteria,
          filterCriteria: action.payload,
        },
      };
    case constants.UPDATE_SORTING_CRITERIA:
      return {
        ...state,
        searchCriteria: {
          ...state.searchCriteria,
          sortCriteria: action.payload,
        },
      };
    case constants.SET_FILTER_PROVIDERS:
      return {
        ...state,
        filterProviders: action.payload,
      };
    case constants.SET_WIDGET_DEPENDENCY: {
      return {
        ...state,
        dependencies: state.dependencies.set(action.payload, true),
      };
    }
    default:
      return state;
  }
};
