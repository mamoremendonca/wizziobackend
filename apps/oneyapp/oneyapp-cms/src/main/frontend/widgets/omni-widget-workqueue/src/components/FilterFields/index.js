import React, { Component } from "react";
import FilterListRender from "./FilterFieldsRender";
import { connect } from "react-redux";
import { updateFilter } from "../../actions";
import { getProcesses } from "../../actions/services/widgetServices";
import { bindActionCreators } from "redux";
import { Map } from "immutable";
import {
  isDecimal,
  numberEndsWithDot,
  isNumeric,
  isDecimalWithComma,
  allowDecimalWithComma,
} from "../../utils";
import { PROCESS_CATEGORY_SINGULAR } from "../../constants";

const initialFilters = {
  status: "",
  journeyType: "",
  number: "",
  entityCode: "",
  minValue: "",
  maxValue: "",
  creationDate: "",
  expirationDate: "",
  department: "",
};

/**
 * HOC to render a component with custom fields that is used to collect the filters used in the server request to get the filtered data.
 */
class FilterFields extends Component {
  state = {
    //need to create a clone of initialfilters otherwise, the const will keep the changes.
    filterFieldsValue: { ...initialFilters },
    invalidFields: Map({ minValue: false, maxValue: false }),
    errorMessages: Map({ minValueErrorMessage: "", maxValueErrorMessage: "" }),
  };

  /**
   * To change the filter fields in the component state.
   * For numeric values is not allowed other characters.
   */
  handleChange = (newValue, fieldName) => {
    if (this.isValidInput(newValue, fieldName)) {
      if (fieldName === "journeyType") {
        this.setState({
          filterFieldsValue: {
            ...this.state.filterFieldsValue,
            journeyType: newValue,
            number: "",
          },
        });
      } else {
        const newState = this.state.filterFieldsValue;
        newState[fieldName] = newValue;
        this.setState({ filterFieldsValue: newState });
      }
    }
  };

  isValidInput = (value, fieldName) => {
    switch (fieldName) {
      case "minValue":
      case "maxValue":
        return (
          !value ||
          isDecimal.test(value) ||
          numberEndsWithDot.test(value) ||
          allowDecimalWithComma.test(value)
        );
      case "number":
        const { journeyType } = this.state.filterFieldsValue;
        if (journeyType === PROCESS_CATEGORY_SINGULAR.PO) {
          return !value || isNumeric.test(value);
        } else {
          return true;
        }

      default:
        return true;
    }
  };
  // TODO: refactor create a method to handle all validations or use React Final Form
  isValidMinAndMaxFields = () => {
    const { i18nProvider } = this.props;
    const { filterFieldsValue } = this.state;

    const maxValueState = {
      name: "maxValue",
      isInvalid: false,
      errorMessage: "",
    };
    const minValueState = {
      name: "minValue",
      isInvalid: false,
      errorMessage: "",
    };
    //Data Input validations
    if (numberEndsWithDot.test(filterFieldsValue.maxValue)) {
      maxValueState.isInvalid = true;
      maxValueState.errorMessage = "filter_value_missing_a_digit";
    } else if (isDecimalWithComma.test(filterFieldsValue.maxValue)) {
      maxValueState.isInvalid = true;
      maxValueState.errorMessage = "filter_value_dot_separator";
    }

    if (numberEndsWithDot.test(filterFieldsValue.minValue)) {
      minValueState.isInvalid = true;
      minValueState.errorMessage = "filter_value_missing_a_digit";
    } else if (isDecimalWithComma.test(filterFieldsValue.minValue)) {
      minValueState.isInvalid = true;
      minValueState.errorMessage = "filter_value_dot_separator";
    }

    //Business validations
    if (
      !(minValueState.isInvalid || maxValueState.isInvalid) &&
      filterFieldsValue.minValue &&
      filterFieldsValue.maxValue
    ) {
      if (
        parseFloat(filterFieldsValue.minValue) >
        parseFloat(filterFieldsValue.maxValue)
      ) {
        minValueState.isInvalid = true;
        maxValueState.isInvalid = true;
        minValueState.errorMessage =
          i18nProvider.Texti18n("filter_value_out_of_bounds") +
          filterFieldsValue.maxValue;
      }
    }
    this.updateErrorFieldStates([minValueState, maxValueState]);
    return !(minValueState.isInvalid || maxValueState.isInvalid);
  };

  updateErrorFieldStates = (fields) => {
    const { invalidFields, errorMessages } = this.state;
    if (fields.length > 0) {
      const field = fields.pop();
      this.setState(
        {
          invalidFields: invalidFields.set(field.name, field.isInvalid),
          errorMessages: errorMessages.set(field.name, field.errorMessage),
        },
        () => {
          this.updateErrorFieldStates(fields);
        }
      );
    }
  };

  clearFilters = () => {
    this.setState(
      { filterFieldsValue: { ...initialFilters } },
      this.fetchProcesses
    );
  };

  fetchProcesses = () => {
    const {
      HttpClient,
      searchCriteria,
      getProcesses,
      updateFilter,
    } = this.props;
    if (this.isValidMinAndMaxFields()) {
      updateFilter({ ...this.state.filterFieldsValue });
      getProcesses(HttpClient, {
        filterCriteria: { ...this.state.filterFieldsValue },
        sortCriteria: { ...searchCriteria.sortCriteria },
      });
    }
  };

  /**
   * Used to translate all reference data.
   * This function should be added to utils folder and be imported.
   */
  translateReferenceData = () => {
    const { i18nProvider, referenceData } = this.props;
    const newReferenceData = {};
    Object.entries(referenceData).forEach(([key]) => {
      newReferenceData[key] = [];
      referenceData[key].forEach((element) => {
        newReferenceData[key].push({
          ...element,
          label: i18nProvider.Texti18n(element.label),
        });
      });
    });
    return newReferenceData;
  };

  render() {
    const { i18nProvider, suppliers } = this.props;
    return (
      <FilterListRender
        filterFieldsValue={this.state.filterFieldsValue}
        onChangeFilter={this.handleChange}
        I18nProvider={i18nProvider}
        referenceData={this.translateReferenceData()}
        suppliers={suppliers}
        clearFilters={this.clearFilters}
        applyFilter={this.fetchProcesses}
        invalidFields={this.state.invalidFields}
        errorMessages={this.state.errorMessages}
      />
    );
  }
}

const mapStateToProps = ({
  widget: { services, i18nProvider },
  widgetStore: { searchCriteria },
  i18n,
  lovReducer: { referenceData, suppliers },
}) => ({
  //localSpinnerOpen, //maybe to be used
  HttpClient: services.HttpClient,
  i18nProvider,
  searchCriteria,
  i18n,
  referenceData,
  suppliers,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getProcesses,
      updateFilter,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(FilterFields);
