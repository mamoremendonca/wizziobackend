const ComponentName = "FilterFields";
const styles = theme => ({
    root :{
        display: "flex",
        alignItems: "right",
        justifyContent: "flex-start",
        flexDirection: "column",          
    },
    filterFieldsContent:{
        display: "flex",
        margin: "0px 8%"
    },
    formControl:{
        width:'100%'
    },
    filterTextField:{
        marginTop: '16px'
    },
    filterLimitTextField:{
        marginTop: '32px'
    },
    filterCalendarField:{
        marginTop: '30px'
    },
    filterDashSplitter:{
        marginTop: '36px',
        textAlign:'center'
    },
    filterActionsRoot: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-end"
    },
    filterActionsContent: {
        display: "flex",
        maxHeight: 40,
        flexDirection: "row",
        justifyContent: "flex-end"
    },
    button: {
        padding: "8px 0px",
        marginLeft:"20px"
    }
});

export { styles, ComponentName };