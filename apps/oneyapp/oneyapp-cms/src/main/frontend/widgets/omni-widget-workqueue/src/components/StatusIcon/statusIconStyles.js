const ComponentName = "StatusIcon";
const styles = (theme) => ({
  base: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginLeft: 3,
  },
  digitalTeam: {
    backgroundColor: "#FEE8A1",
  },
  active: {
    backgroundColor: "#E0D011",
  },
  approving: {
    backgroundColor: "#E79208",
  },
  rejected: {
    backgroundColor: "#26AEE5",
  },
});

export { ComponentName };
export { styles };
