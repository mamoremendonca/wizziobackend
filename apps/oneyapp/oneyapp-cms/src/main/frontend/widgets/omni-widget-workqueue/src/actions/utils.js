import React from "react";
import StatusIcon from "../components/StatusIcon";
import Typography from "@ui-lib/core/Typography";
import classNames from "@ui-lib/core/classNames";
import qs from "qs";
import Countdown from "../components/Countdown";

import {
  PROCESS_CATEGORY,
  PROCESS_CATEGORY_SINGULAR,
  CATEGORY_TABS,
  SORT_FIELDS_MAPPER,
  PROCESS_STATUS,
} from "../constants";

export function i18nText(i18nProvider, label) {
  let i18n = i18nProvider.Texti18n(label);
  return i18n ? i18n : `<${label}>`;
}

export function i18nLabel(i18nProvider, label) {
  let i18n = i18nProvider.Labeli18n(label);
  return i18n ? i18n : `<${label}>`;
}

export function isFilterActive(filter) {
  let condition = false;

  Object.keys(filter).forEach((param) => {
    if (filter[param] !== "" && filter[param] !== " ") {
      condition = true;
    }
  });

  return condition;
}

/**
 *
 * @param {} width
 * @param {*} height
 */
export function prepareSize(width, height) {
  let preparedSize = { width: null, height: null };
  let rect = document.getElementById("workqueue").getBoundingClientRect();

  if (!isNil(width) && !isNil(height)) {
    preparedSize.width =
      width.indexOf("px", 1) > 0
        ? Number(width.slice(0, width.indexOf("px", 1)))
        : Number(width.slice(0, width.indexOf("%", 1)) / 100) *
          window.innerWidth;

    preparedSize.height =
      height.split("px", 1) > 0
        ? Number(height.slice(0, height.indexOf("px", 1)))
        : Number(height.slice(0, height.indexOf("%", 1)) / 100) *
          window.innerHeight;
  }

  return rect && isFinite(rect.width)
    ? { ...preparedSize, width: rect.width }
    : preparedSize;
}

export function getTabSpecificProcesses(processes, tabState) {
  if (processes && Object.keys(processes).length > 0) {
    switch (tabState) {
      case PROCESS_CATEGORY.PURCHASE_ORDERS:
        return processes.purchaseOrders;
      case PROCESS_CATEGORY.FINANCIAL_DOCS:
        return processes.financialDocs;
      default:
        return processes.all;
    }
  }
  return [];
}

export function getStateByTab(tab) {
  return Object.keys(CATEGORY_TABS).find((key) => CATEGORY_TABS[key] === tab);
}

/**
 * Will return a i18n string for the description or for the code.
 * We assume that the labe has always the description
 * @param {*} i18nProvider
 * @param {*} code
 * @param {*} lov
 */
const getDescriptionByCode = (i18nProvider, code, lov) => {
  const elements = lov.filter((entry) => entry.code === code);
  if (elements.length > 0) {
    return i18nProvider.Texti18n(elements[0].label);
  } else {
    return i18nProvider.Texti18n(code);
  }
};

/**
 * Gets the value that came from the response and format it.
 * @param {*} fieldValue
 * @param {*} format
 * @param {*} lov
 */
const parseCellValues = (i18nProvider, fieldValue, format, lov) => {
  switch (format) {
    case "date":
      return fieldValue ? toLocalDate(fieldValue) : "-";
    case "countdown":
      return fieldValue ? (
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            position: "relative",
          }}
        >
          <Typography variant="body2">{toLocalDate(fieldValue)}</Typography>
          <Countdown i18nProvider={i18nProvider} dateToCompare={fieldValue} />
        </div>
      ) : (
        "-"
      );
    case "referenceData":
      return fieldValue
        ? getDescriptionByCode(i18nProvider, fieldValue, lov)
        : "-";
    case "euro":
      return fieldValue
        ? new Intl.NumberFormat("es-ES", {
            style: "currency",
            currency: "EUR",
          }).format(fieldValue)
        : "-";
    case "instanceId":
      return fieldValue.friendlyNumber
        ? fieldValue.friendlyNumber
        : fieldValue.instanceId
        ? fieldValue.processCategory === PROCESS_CATEGORY_SINGULAR.PO
          ? "TPO" + fieldValue.instanceId.toString().padStart(4, "0")
          : "TDF" + fieldValue.instanceId.toString().padStart(4, "0")
        : "-";
    case "status":
      return fieldValue ? (
        <Typography variant="body2">
          {auxGetState(fieldValue, i18nProvider)}
        </Typography>
      ) : (
        "-"
      );
    default:
      return fieldValue ? i18nProvider.Texti18n(fieldValue) : "-";
  }
};

/**
 *
 * @param {*} data
 * @param {*} settings
 * @param {*} i18nProvider
 * @param {*} lovs
 */
function auxGetState(status, i18nProvider) {
  let label = "";

  for (const [key, value] of Object.entries(PROCESS_STATUS)) {
    if (status === value) {
      label = i18nProvider.Texti18n(key);
    }
  }

  return label;
}

/**
 *  função usada para fazer o parse da data.
 * @param {*} data
 * @param {*} i18nProvider
 * @param {*} settings
 */
export function formatData(data, settings, i18nProvider, lovs) {
  return data.map((proc) => {
    let journeyType;
    let journeyId;

    journeyType = settings.queue.journeyTypes.find(
      (journeyType) => journeyType.processCategory === proc.journeyType
    );

    if (journeyType) {
      journeyId = journeyType.journeyId;
    } else {
      journeyId = 1; // redirect to dashboard if journeyType not defined
    }

    let requestedTargets = getTargetInfo(proc, settings);
    return Object.assign(
      {
        status:
          journeyType.processCategory === "FINANCIAL_DOCUMENT"
            ? proc.status === "Draft"
              ? i18nProvider.Texti18n("DIGITAL_TEAM")
              : parseCellValues(i18nProvider, proc.status, "status")
            : parseCellValues(i18nProvider, proc.status, "status"),
        // <StatusIcon status={proc.status} />
        processCategory: parseCellValues(i18nProvider, journeyType.i18n), //change to PO)
        instanceId: parseCellValues(
          i18nProvider,
          {
            friendlyNumber: proc.friendlyNumber,
            instanceId: proc.instanceId,
            processCategory: journeyType.processCategory,
          },
          "instanceId"
        ),
        entity: parseCellValues(
          i18nProvider,
          proc.entityCode,
          "referenceData",
          lovs.suppliers
        ), //get referenceData for the suppliers
        totalPrice: parseCellValues(i18nProvider, proc.value, "euro"),
        createdAt: parseCellValues(i18nProvider, proc.creationDate, "date"), //
        approvalDateLimit: parseCellValues(
          i18nProvider,
          proc.expirationDate,
          "countdown"
        ),
        department: parseCellValues(
          i18nProvider,
          proc.departmentCode,
          "referenceData",
          lovs.referenceData.department
        ),
        journeyId: journeyId, // used on navigate
        id: proc.instanceId, // used on navigate
        statusValue: proc.status, // used on navigate
      },
      requestedTargets
    );
  });
}

/**
 * função usada para por a label nas tabs.
 * @param {*} i18nProvider
 * @param {*} total
 * @param {*} label
 * @param {*} param3
 * @param {*} selectedTab
 * @param {*} thisTab
 */
export function composeLabel(
  i18nProvider,
  total,
  label,
  { tab, activeTab, indicator },
  selectedTab,
  thisTab
) {
  return (
    <div className={tab}>
      <Typography
        className={classNames({ [activeTab]: selectedTab === thisTab })}
      >
        {i18nLabel(i18nProvider, label)}
      </Typography>
      <Typography
        className={classNames(indicator, {
          [activeTab]: selectedTab === thisTab,
        })}
      >{`(${fallback(total, 0)})`}</Typography>
    </div>
  );
}

// LOCAL UTILITIES

function fallback(value, placeholder) {
  return isNaN(value) ? placeholder : value;
}

function isNil(value) {
  return value === undefined || value === null;
}

/**
 * Get Target Info requested to be represented under one or more
 * columns of the Workqueue Widget
 * @param {Object} process Object that holds the process info
 * @param {Object} settings Workqeue Widget settings
 * @returns {Object} Object containing the targets requested to be represented in the Workqueue Widget
 */
function getTargetInfo(process, settings) {
  let targetInfo = {};
  if (process.targets) {
    const targetKeys = Object.keys(process.targets);
    targetKeys.forEach((key) => {
      if (wasKeyRequested(key, settings)) {
        targetInfo[key] = process.targets[key];
      }
    });
  }
  return targetInfo;
}

/**
 * Checks if the target was requested to be
 * represented under a column in the work queue
 * @param {String} key Target key
 * @param {Object} settings Workqeue Widget settings
 * @returns {boolean} Whether or not the target was requested
 */
function wasKeyRequested(key, settings) {
  if (settings && settings.queue && settings.queue.columns) {
    return settings.queue.columns.some((col) => col.name === key);
  }
  return false;
}

function toLocalDate(date) {
  return `${new Date(date).toLocaleDateString()}`;
}

export function prepareSearchCriteria(searchCriteria) {
  const serverSearchCriteria = {};
  if (searchCriteria.filterCriteria) {
    let filterParams = Object.keys(searchCriteria.filterCriteria);
    filterParams.forEach((param) => {
      serverSearchCriteria[param] =
        searchCriteria.filterCriteria[param] &&
        searchCriteria.filterCriteria[param].length !== 0
          ? searchCriteria.filterCriteria[param]
          : null;
    });
  }
  return serverSearchCriteria;
}

export const prepareSortCriteria = (sortCriteria) => {
  const sort = {
    orderField: SORT_FIELDS_MAPPER[sortCriteria.orderField],
    orderType: sortCriteria.orderType,
  };
  return "?" + qs.stringify(sort, { filter: ["orderField", "orderType"] });
};

export function sortByCategory(processes) {
  let sortedProcesses = {
    purchaseOrders: [],
    financialDocs: [],
    all: [],
  };

  processes.forEach((proc) => {
    switch (proc.state) {
      case PROCESS_CATEGORY.PURCHASE_ORDERS:
        sortedProcesses.purchaseOrders.push(proc);
        break;
      case PROCESS_CATEGORY.FINANCIAL_DOCS:
        sortedProcesses.financialDocs.push(proc);
        break;
      default:
        break;
    }
  });

  sortedProcesses.all = processes;

  return sortedProcesses;
}

/**
 * Determines whether the given object is empty or not
 * @param {*} obj
 */
export function isEmpty(obj) {
  return obj !== undefined ? Object.keys(obj).length > 0 : undefined;
}
