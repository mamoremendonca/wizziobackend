import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { styles, ComponentName } from "./FilterFieldsStyles";
import Grid from "@ui-lib/core/Grid";
import Button from "@ui-lib/core/Button";
import TextField from "@ui-lib/core/TextField";
import Typography from "@ui-lib/core/Typography";
import AutocompleteSelect from "@ui-lib/oney/AutocompleteSelect";

function FilterListRender({
  classes,
  I18nProvider,
  filterFieldsValue,
  referenceData,
  suppliers,
  onChangeFilter,
  applyFilter,
  clearFilters,
  invalidFields,
  errorMessages,
}) {
  return (
    <div className={classes.root}>
      <div className={classes.filterFieldsContent}>
        <Grid container spacing={24}>
          <Grid
            id="firstRow"
            container
            alignItems="flex-start"
            spacing={16}
            item
            xs={12}
          >
            <Grid item xs={3}>
              <div style={{ marginTop: "11px", height: "40px" }}>
                {" "}
                {/* Hammer Fall refactor onHover push all items only height is required */}
                <AutocompleteSelect
                  inputKey="statusSelect"
                  label={I18nProvider.Texti18n("status_icon")}
                  placeholder={I18nProvider.Labeli18n(
                    "select_component.placeholder.label"
                  )}
                  formControlProps={{ fullWidth: true }}
                  options={referenceData.status}
                  value={
                    filterFieldsValue.status
                      ? referenceData.status.find(
                          (data) => data.value === filterFieldsValue.status
                        )
                      : null
                  }
                  onChange={(value) =>
                    onChangeFilter(
                      value && value.value ? value.value : undefined,
                      "status"
                    )
                  }
                  noOptionsMessage={() =>
                    I18nProvider.Labeli18n("select_component.no_matches.label")
                  }
                />
              </div>
            </Grid>
            <Grid item xs={3}>
              <div style={{ marginTop: "11px", height: "40px" }}>
                {" "}
                {/* Hammer Fall refactor onHover push all items only height is required */}
                <AutocompleteSelect
                  inputKey="docTypeSelect"
                  label={I18nProvider.Texti18n("category")}
                  placeholder={I18nProvider.Labeli18n(
                    "select_component.placeholder.label"
                  )}
                  formControlProps={{ fullWidth: true }}
                  options={referenceData.journeyType}
                  value={
                    filterFieldsValue.journeyType
                      ? referenceData.journeyType.find(
                          (data) => data.value === filterFieldsValue.journeyType
                        )
                      : null
                  }
                  onChange={(value) =>
                    onChangeFilter(
                      value && value.value ? value.value : undefined,
                      "journeyType"
                    )
                  }
                  noOptionsMessage={() =>
                    I18nProvider.Labeli18n("select_component.no_matches.label")
                  }
                />
              </div>
            </Grid>
            <Grid item xs={3}>
              <div className={classes.filterTextField}>
                <TextField
                  disabled={!filterFieldsValue.journeyType}
                  fullWidth
                  InputLabelProps={{ shrink: true }}
                  label={I18nProvider.Texti18n("work_queue_journey_instanceId")}
                  value={filterFieldsValue.number}
                  onChange={(e) => onChangeFilter(e.target.value, "number")}
                />
              </div>
            </Grid>
            <Grid item xs={3}>
              <div style={{ marginTop: "11px", height: "40px" }}>
                {" "}
                {/* Hammer Fall refactor onHover push all items only height is required */}
                <AutocompleteSelect
                  inputKey="entitySelect"
                  label={I18nProvider.Texti18n("work_queue_journey_entity")}
                  placeholder={I18nProvider.Labeli18n(
                    "select_component.placeholder.label"
                  )}
                  formControlProps={{ fullWidth: true }}
                  options={suppliers}
                  value={
                    filterFieldsValue.entityCode
                      ? suppliers.find(
                          (data) => data.value === filterFieldsValue.entityCode
                        )
                      : null
                  }
                  onChange={(value) =>
                    onChangeFilter(
                      value && value.value ? value.value : undefined,
                      "entityCode"
                    )
                  }
                  noOptionsMessage={() =>
                    I18nProvider.Labeli18n("select_component.no_matches.label")
                  }
                />
              </div>
            </Grid>
            <Grid item xs={3}>
              <Grid
                id="secondRow"
                container
                alignItems="flex-start"
                spacing={0}
                item
                xs={12}
              >
                <Grid item xs={6}>
                  <div className={classes.filterTextField}>
                    <TextField
                      fullWidth
                      InputLabelProps={{ shrink: true }}
                      label={I18nProvider.Texti18n(
                        "work_queue_journey_total_price"
                      )}
                      placeholder={I18nProvider.Texti18n("capitalized_of")}
                      value={filterFieldsValue.minValue}
                      helperText={
                        invalidFields.get("minValue")
                          ? I18nProvider.Texti18n(errorMessages.get("minValue"))
                          : undefined
                      }
                      error={invalidFields.get("minValue")}
                      onChange={(e) =>
                        onChangeFilter(e.target.value, "minValue")
                      }
                    />
                  </div>
                </Grid>
                <Grid item xs={1}>
                  <div className={classes.filterDashSplitter}>
                    <Typography color="textPrimary" variant="subtitle2">
                      -
                    </Typography>
                  </div>
                </Grid>
                <Grid item xs={5}>
                  <div className={classes.filterLimitTextField}>
                    <TextField
                      fullWidth
                      InputLabelProps={{ shrink: true }}
                      placeholder={I18nProvider.Texti18n("capitalized_until")}
                      value={filterFieldsValue.maxValue}
                      helperText={
                        invalidFields.get("maxValue")
                          ? I18nProvider.Texti18n(errorMessages.get("maxValue"))
                          : undefined
                      }
                      error={invalidFields.get("maxValue")}
                      onChange={(e) =>
                        onChangeFilter(e.target.value, "maxValue")
                      }
                    />
                  </div>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={3}>
              <div className={classes.filterTextField}>
                <TextField
                  fullWidth
                  id="date"
                  label={I18nProvider.Texti18n("work_queue_journey_created_at")}
                  type="date"
                  inputProps={{ max: new Date().toISOString().slice(0, 10) }}
                  value={filterFieldsValue.creationDate}
                  onChange={(e) =>
                    onChangeFilter(e.target.value, "creationDate")
                  }
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </div>
            </Grid>
            <Grid item xs={3}>
              <div className={classes.filterTextField}>
                <TextField
                  fullWidth
                  id="date"
                  label={I18nProvider.Texti18n(
                    "work_queue_journey_approval_date_limit"
                  )}
                  type="date"
                  value={filterFieldsValue.expirationDate}
                  onChange={(e) =>
                    onChangeFilter(e.target.value, "expirationDate")
                  }
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </div>
            </Grid>
          </Grid>
          <Grid
            id="secondRow"
            container
            alignItems="flex-start"
            spacing={16}
            item
            xs={12}
          ></Grid>
        </Grid>
      </div>
      <div className={classes.filterActionsRoot}>
        <div className={classes.filterActionsContent}>
          <Button
            variant="text"
            onClick={clearFilters}
            classes={{ root: classes.button }}
          >
            {I18nProvider.Texti18n("workqueue_filter_clear")}
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={applyFilter}
            classes={{ root: classes.button }}
          >
            {I18nProvider.Texti18n("workqueue_filter_search")}
          </Button>
        </div>
      </div>
    </div>
  );
}

export default withStyles(styles, { name: ComponentName })(FilterListRender);
