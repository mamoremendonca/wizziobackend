import React from "react";
import Typography from "@ui-lib/core/Typography";
import { i18nLabel } from "../../actions/utils";
import { styles, ComponentName } from "./progressTooltipStyles";
import { withStyles } from "@ui-lib/core/styles";

const ProgressTooltipRender = ({ i18nProvider, classes }) => {
  return (
    <div className={classes.container}>
      <Typography variant="body2" className={classes.title}>
        {i18nLabel(i18nProvider, "workqueue_journey_status")}
      </Typography>
      <div className={classes.item}>
        <div
          style={{ backgroundColor: stallColor("DIGITAL_TEAM") }}
          className={classes.statusIndicator}
        />
        <Typography variant="caption">
          {i18nLabel(i18nProvider, "workqueue_digital_team")}
        </Typography>
      </div>
      <div className={classes.item}>
        <div
          style={{ backgroundColor: stallColor("ACTIVE") }}
          className={classes.statusIndicator}
        />
        <Typography variant="caption">
          {i18nLabel(i18nProvider, "workqueue_status_active")}
        </Typography>
      </div>
      <div className={classes.item}>
        <div
          style={{ backgroundColor: stallColor("APPROVING") }}
          className={classes.statusIndicator}
        />
        <Typography variant="caption">
          {i18nLabel(i18nProvider, "workqueue_status_approving")}
        </Typography>
      </div>
      <div className={classes.item}>
        <div
          style={{ backgroundColor: stallColor("REJECTED") }}
          className={classes.statusIndicator}
        />
        <Typography variant="caption">
          {i18nLabel(i18nProvider, "workqueue_status_rejected")}
        </Typography>
      </div>
    </div>
  );
};

export default withStyles(styles, { name: ComponentName })(
  ProgressTooltipRender
);

function stallColor(stall) {
  switch (stall) {
    case "DIGITAL_TEAM":
      return "#FEE8A1";
    case "ACTIVE":
      return "#E1AD01";
    case "APPROVING":
      return "#E79208";
    case "REJECTED":
      return "#26AEE5";
  }
}
