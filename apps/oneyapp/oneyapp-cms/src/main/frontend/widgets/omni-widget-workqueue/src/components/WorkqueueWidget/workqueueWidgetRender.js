import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { styles, ComponentName } from "./workqueueWidgetStyles";
import StateTabs from "../StateTabs";
import UtilityPanel from "../UtilityPanel";

const WorkqueueWidgetRender = ({ classes }) => {
  return (
    <div className={classes.root}>
      <UtilityPanel />
      <StateTabs />
    </div>
  );
};

export default withStyles(styles, { name: ComponentName })(
  WorkqueueWidgetRender
);
