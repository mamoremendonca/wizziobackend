export const DepartmentsMock = [
    {
      "id": 1,
      "description": "Departamento 1",
      "code":"001"
    },
    {
      "id": 2,
      "description": "Departamento 2",
      "code":"002"
    },
    {
      "id": 3,
      "description": "Departamento 3",
      "code": "003"
    },
    {
      "id": 4,
      "description": "Departamento 4",
      "code": "004"
    }
];