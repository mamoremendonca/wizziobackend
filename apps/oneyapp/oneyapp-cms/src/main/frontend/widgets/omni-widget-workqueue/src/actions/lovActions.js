import * as constants from '../constants';

export const setSuppliers = suppliersData => ({ type: constants.SET_SUPPLIERS, payload: suppliersData });
export const setReferenceData = referenceData => ({ type: constants.SET_REFERENCE_DATA, payload: referenceData });