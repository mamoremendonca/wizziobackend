import React from "react";
import Tabs from "@ui-lib/core/Tabs";
import Tab from "@ui-lib/core/Tab";
import Divider from "@ui-lib/core/Divider";
import Typography from "@ui-lib/core/Typography";
import Tooltip from "@ui-lib/core/Tooltip";
import Icon from "@ui-lib/core/Icon";
import { withStyles } from "@ui-lib/core/styles";
import ListWrapper from "../ListWrapper";
import { styles, ComponentName } from "./stateTabsStyles";
import { composeLabel } from "../../actions/utils";
import { PROCESS_CATEGORY, CATEGORY_TABS } from "../../constants";

const StateTabsRender = ({
	meta,
	tab,
	i18nProvider,
	handleChange,
	classes
}) => {
	return (
		<div className={classes.root}>
			<div className={classes.tabHeader}>
				{/* <Tabs
					value={tab}
					classes={{ root: classes.tabsRoot }}
					onChange={handleChange}
					indicatorColor="primary"
					textColor="primary"
				>
					<Tab
						classes={{ root: classes.tabRoot, selected: classes.activeTab }}
						label={composeLabel(
							i18nProvider,
							meta.all,
							"workqueue_category_all",
							classes,
							tab,
							0
						)}
					/>
					{/* <Tab
						classes={{ root: classes.tabRoot, selected: classes.activeTab }}
						label={composeLabel(
							i18nProvider,
							meta.purchaseOrdersTotal,
							"workqueue_category_po",
							classes,
							tab,
							1
						)}
					/>
					<Tab
						classes={{ root: classes.tabRoot, selected: classes.activeTab }}
						label={composeLabel(
							i18nProvider,
							meta.financialDocsTotal,
							"workqueue_category_df",
							classes,
							tab,
							2
						)}
					/>*/} 
				{/* </Tabs>  */}
				{/* <div className={classes.toolTipRecordNumber}>	
					<Tooltip 
						title={<Typography>
									{i18nProvider.Texti18n('tabs.header.tooltip.record_numbers')}
								</Typography>}>
						<Icon className="icon-information" style = {{fontSize: 16}} />
					</Tooltip>					
					<Typography color="textPrimary" style={{marginLeft:"10px"}}>
						{i18nProvider.Texti18n('showing')} {meta.all} {i18nProvider.Texti18n('of')} {meta.all} {i18nProvider.Texti18n('results')}.
					</Typography>  
				</div> */}
				
			</div>			
			<Divider/>
			{tab === CATEGORY_TABS[PROCESS_CATEGORY.ALL] && (
				<ListWrapper category={PROCESS_CATEGORY.ALL} total={meta.all} />
			)}
			{/* {tab === CATEGORY_TABS[PROCESS_CATEGORY.PURCHASE_ORDERS] && (
				<ListWrapper category={PROCESS_CATEGORY.PURCHASE_ORDERS} total={meta.purchaseOrdersTotal} />
			)}
			{tab === CATEGORY_TABS[PROCESS_CATEGORY.FINANCIAL_DOCS] && (
				<ListWrapper category={PROCESS_CATEGORY.FINANCIAL_DOCS} total={meta.financialDocsTotal} />
			)} */}
		</div>
	);
};

export default withStyles(styles, { name: ComponentName })(StateTabsRender);
