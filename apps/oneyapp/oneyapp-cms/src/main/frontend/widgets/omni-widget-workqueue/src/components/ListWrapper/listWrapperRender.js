import React from "react";
import InfiniteScroll from "../InfiniteScroll";
import { withStyles } from "@ui-lib/core/styles";
import { styles, ComponentName } from "./listWrapperStyles";
import { i18nLabel } from "../../actions/utils";

const ListWrapperRender = ({
  isPrivate,
  hasMore,
  data,
  size,
  cols,
  batchSize,
  i18nProvider,
  handleSort,
  handleLoadMore,
  handleClick,
  autoRefresh,
  isActive,
  classes
}) => {
  let message = i18nLabel(
    i18nProvider,
    data.length > 0
      ? "workqueue_no_more_journeys"
      : "workqueue_no_search_results"
  );
  return (
    <div className={classes.root} style={{ height: size.height }}>
      <InfiniteScroll
        data={data}
        cols={cols}
        size={size}
        hasMore={hasMore}
        batchSize={batchSize}
        sort={handleSort}
        loadMore={handleLoadMore}
        onClick={handleClick}
        endMessage={message}
        i18nProvider={i18nProvider}
        autoRefresh={{
          ...autoRefresh,
          active: autoRefresh.active ? isActive : autoRefresh.active
        }}
      />

    </div>
  );
};
export default withStyles(styles, { name: ComponentName })(ListWrapperRender);

