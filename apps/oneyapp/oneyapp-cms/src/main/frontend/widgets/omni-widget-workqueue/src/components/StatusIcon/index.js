import React from "react";
import StatusIconRender from "./statusIconRender";

const StatusIcon = props => {
  return <StatusIconRender {...props} />;
};

export default StatusIcon;
