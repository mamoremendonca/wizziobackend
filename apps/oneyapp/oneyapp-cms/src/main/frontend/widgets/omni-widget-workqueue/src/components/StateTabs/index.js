import React, { Component } from "react";
import { connect } from "react-redux";
import StateTabsRender from "./stateTabsRender";
import { getProcesses} from "../../actions/services/widgetServices";


class StateTabs extends Component {
	constructor(props) {
		super(props);
		const { getProcesses, HttpClient, searchCriteria } = props;
		this.state = {
			tab: 0
		};
		getProcesses(HttpClient, searchCriteria);
	}

	handleTabChange = (e, tab) => {
		const { isPrivate } = this.props;

		if (!isPrivate) {
			this.setState({ tab });
		}
	};

	render() {
		const { i18nProvider, totals } = this.props;
		if (totals && Object.keys(totals).length > 0) {
			const { financialDocs, purchaseOrders, all } = totals;
			let processMeta = {
				all: all,
				purchaseOrdersTotal: purchaseOrders,
				financialDocsTotal: financialDocs
			}
			return (
				<StateTabsRender
					tab={this.state.tab}
					meta={processMeta}
					i18nProvider={i18nProvider}
					handleChange={this.handleTabChange}
				/>
			);
		}
		return null;
	}
}

const mapStateToProps = ({
	widget: { services, isPrivate, i18nProvider },
	widgetStore: {totals, searchCriteria}
}) => ({
	i18nProvider,
	HttpClient: services.HttpClient,
   	searchCriteria,
	isPrivate,
	totals
});

const mapDispatchToProps = function (dispatch, ownProps) {
	return {
		getProcesses: (HttpClient, batchSize, searchCriteria) =>
			dispatch(getProcesses(HttpClient, batchSize, searchCriteria)),
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(StateTabs);
