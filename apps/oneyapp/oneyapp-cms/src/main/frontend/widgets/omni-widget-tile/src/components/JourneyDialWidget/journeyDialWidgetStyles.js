// React & Others
import { WIDGET_PADDING } from "../../constants";

const componentName = "JourneyDialWidget";
const styles = (theme) => ({
  imageOnly: {
    // objectFit: "contain",
    height: 30,
  },
  full: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#81BC00", //palette.primaryLight.main
    "&:hover": {
      backgroundColor: "#689f38",
    },
    // backgroundColor: theme.palette.background.paper,
    // "&:hover": {
    //   backgroundColor: theme.palette.background.default,
    // },
    cursor: "pointer",
  },
  root: {
    padding: WIDGET_PADDING,
    boxSizing: "border-box",
    overflow: "hidden",
    height: "100%",
    width: "100%",
    display: "flex",
    flexDirection: "row",
  },
  icon: {
    marginRight: 10,
    color: "#FFFFFF",
  },
  tileTitle: {
    color: "#FFFFFF",
    fontSize: 20,
  },
});

export { componentName };
export { styles };
