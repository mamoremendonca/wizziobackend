// React & Others
import React, { Component } from "react";
import { connect } from "react-redux";
import JourneyDialWidgetRender from "./journeyDialWidgetRender";

class JourneyDialWidget extends Component {
  handleClick = () => {
    const { JourneyActions, initialProps } = this.props;
    JourneyActions.openJourneyWithArgs(
      initialProps && initialProps.journeyId,
      undefined
    );
  };

  render() {
    return (
      this.props.initialProps && (
        <JourneyDialWidgetRender
          {...this.props}
          parentRef={this.tile}
          imageOnly={this.props.initialProps.journeyi18n === undefined}
          handleClick={this.handleClick}
        />
      )
    );
  }
}

const mapStateToProps = ({ widget: { services, height } }) => ({
  height,
  I18nProvider: services.I18nProvider,
  JourneyActions: services.JourneyActions
});

const mapDispatchToProps = function(dispatch, ownProps) {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(JourneyDialWidget);
