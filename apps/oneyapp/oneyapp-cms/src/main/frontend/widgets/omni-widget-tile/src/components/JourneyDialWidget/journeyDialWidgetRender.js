// React & Others
import React from "react";

// UI
import Typography from "@ui-lib/core/Typography";
import { withStyles } from "@ui-lib/core/styles";
import { componentName, styles } from "./journeyDialWidgetStyles";
import classNames from "@ui-lib/core/classNames";
import { WIDGET_PADDING, WIDGET_ICON_SIZE } from "../../constants";
import { prepareHeight } from "../../utils";

const JourneyDialWidgetRender = ({
  height,
  imageOnly,
  initialProps: { imageURL, journeyi18n },
  I18nProvider,
  handleClick,
  classes
}) => (
  <div
    className={classNames(classes.root, {
      [classes.full]: !imageOnly
    })}
    onClick={handleClick}
  >
    <img
      alt="journey_icon"
      src={imageURL}
      className={classNames(classes.imageOnly, {
        [classes.icon]: !imageOnly
      })}
      height={
        imageOnly
          ? prepareHeight(height) - 2 * WIDGET_PADDING
          : WIDGET_ICON_SIZE
      }
    />
    {imageOnly === false && (
      <Typography variant="h5" color="primary" className={classes.tileTitle}>
        {I18nProvider.Labeli18n(journeyi18n)}
      </Typography>
    )}
  </div>
);

export default withStyles(styles, { name: componentName })(
  JourneyDialWidgetRender
);
