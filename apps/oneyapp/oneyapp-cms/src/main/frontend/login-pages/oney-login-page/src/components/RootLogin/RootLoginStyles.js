const ComponentName = "RootLogin";
const styles = theme => ({
    loginGrids : {
        height : "100vh",
        backgroundColor : 'rgba(255,255,255,0.9)'
    },
    root: {
        backgroundImage : `url(${theme.customSettings.hostPort}${theme.customSettings.contentUrl}/foundation/v3/canvas/ufe/themes/default/images/${theme.palette.type}/loginBackImage.png)`,
        backgroundSize : "cover",
        backgroundRepeat : "no-repeat",
        backgroundPosition : "right center",
        padding : 20, //Grids Negative margins fix,
        width : "100%" // When session expires and basic login appears
    },

});
export {styles, ComponentName} ;