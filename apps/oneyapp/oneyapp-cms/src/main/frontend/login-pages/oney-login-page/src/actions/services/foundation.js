import CommonHTTPService from './common-http-service';
import * as constants from '../../constants/foundation';

/**
 * To consume the Foundation REST API
 * e.g. authentication
 */
export class APIFoundationServices extends CommonHTTPService {
    
    /**
     * To call the service that request for the authentication token.
     * Returns a promis with the call
     * @param {*} username 
     * @param {*} password 
     */
    getAuthenticationToken (username, password){
        const headers = {Accept : "application/json"};
        const endPoint = this.serverURL + constants.OAUTH_AUTHORIZE;        
        return this.httpClientProvider.post(endPoint,headers,null,null,null,null,{username: username, password:password});
    }
}
