import {AxiosHttpClientProvider}  from '../../providers';
import * as contentServerHandler from '../../utils/content-server-handler';

/**
 * Common service to get the server url and the http client provider.
 * Used to extend API Services.
 */
export default class CommonHTTPService {
    constructor (){
        this.serverURL = contentServerHandler.getCurrentHostAndPort();
        this.httpClientProvider = new AxiosHttpClientProvider();
    }

}