/**
 * the server url used in Development 
 */
// export const HOST_PORT = "http://localhost:4500";
export const HOST_PORT = 'http://wizzio-dev.oney.pt:4501'
/**
 * the root of the content folder in the Sling.
 */
export const CONTENT_BASE_URL = "/content/oney";
