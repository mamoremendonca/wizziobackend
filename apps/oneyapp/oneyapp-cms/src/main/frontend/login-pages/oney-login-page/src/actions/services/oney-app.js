import CommonHTTPService from './common-http-service';
import * as constants from '../../constants/oney-app';

/**
 * To consume the Oney App REST API
 * e.g. memberlistroups
 */
export class APIOneyAPPServices extends CommonHTTPService {
    
    /**
     * To call the service that request for the authentication token.
     * Returns a promis with the call
     * @param {*} headers Object like {Authorization : "Bearer " + accessToken} 
     */
    getUserGroups (headers){
        const endPoint = this.serverURL + constants.USER_MEMBERSHIPS;        
        return this.httpClientProvider.get(endPoint,headers);
    }

    /**
     * To associate an user to a group.
     * @param {*} headers Object like {Authorization : "Bearer " + accessToken} 
     * @param {*} userGroup String with the name of the group
     */
    setUserGroup (headers, userGroupName){
        const endPoint = this.serverURL + constants.USER_GROUP;        
        return this.httpClientProvider.post(endPoint,headers, null,{name: userGroupName});
    }
}