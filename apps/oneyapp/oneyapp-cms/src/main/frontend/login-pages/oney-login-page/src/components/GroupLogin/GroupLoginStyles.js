const ComponentName = "GroupLogin";

const styles = theme => ({
    root : {
        display : "flex",
        justifyContent : "center",
        alignItems : "center",
        height : "100%",
        flexDirection : "column",
        width : "100%",
    },
    loginLogo : {
        backgroundImage :`url(${theme.customSettings.hostPort}${theme.customSettings.contentUrl}/foundation/v3/canvas/ufe/themes/default/images/${theme.palette.type}/logoLogin.png)`, //"url(" + theme.customSettings.hostPort +  + "/foundation/v3/canvas/ufe/themes/default/images/" + theme.palette.type + "/logoLogin.png" + ")",
        backgroundSize : "contain",
        display : "block",
        backgroundRepeat : "no-repeat",
        height : "120px",
        backgroundPosition : "center center",
        marginBottom : "60px",
        width : "90%"
    },
    textFieldsRoot : {
        display : "flex",
        width : "100%",
        alignItems : "flex-end",
        flexDirection : "row",
        justifyContent : "center",
        marginBottom : "20px"
    },
    icon : {
        flex : "0 0 auto",
        marginLeft: "4px"
    },
    textFields : {
        flex : "1",
        marginLeft : "22px"
    },
    loginButton : {
        marginTop : "20px",
        display : "flex",
        flexDirection : "row",
        flexWrap : "nowrap",
        padding : "3px 20px",
        justifyContent : "space-between"
    },
    loginButtonLoading : {
        "&:hover" : {
            backgroundColor : theme.palette.primary.main,
            cursor : "default"
        },
        "&:active" : {
            boxShadow : "none"
        }
    },
    loadingSpinnerContainer : {
        height : "32px !important",
        width : "32px !important"
    },
    errorContainer : {
        marginTop : "15px",
        width : "100%",
        height : "38px",
        display : "flex"
    },
    errorPanel : {
        backgroundColor : theme.palette.error[300],
        boxShadow : "0 1px 3px 0 rgba(0, 0, 0, 0.29)",
        display : "flex",
        alignItems : "center",
        justifyContent : "center"
    },
    errorText : {
        color : theme.palette.common.black,
        flex : "1",
        alignItems : "center",
        display : "flex",
        height : "100%"
    },
    errorIcon : {
        color : theme.palette.error.main,
        flex : "0 0 auto",
        width : "20px",
        fontSize : "20px",
        alignItems : "center",
        display : "flex",
        height : "100%",
        marginRight : "20px",
        marginLeft : "20px"
    },
    changeUser : {
        marginTop : "20px",
        marginBottom : "30px",
    },
    disableIcon: {
        flex : "0 0 auto",
        marginLeft: "4px",
        color : theme.palette.action.disabled
    }
});

export { styles, ComponentName};
