import React from "react";
import GroupLogin from "./components/GroupLogin";
import RootLogin from "./components/RootLogin";
import { ThemeProvider } from "@ui-lib/core/styles";
import theme from "./theme";
import * as utils from "./utils";
import * as services from "./actions/services";
import GroupSelect from "./components/GroupSelect";

//StateFullComponent used to control the states of the login page and orchestrator.
export default class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hostPort: utils.getCurrentHostAndPort(),
      contentUrl: utils.getContentBaseURL(),
      username: "",
      password: "",
      showPassword: false,
      errorMessage: "",
      isLoginLoading: false,
      authData: undefined,
      userGroups: [],
      userGroupName: "",
      loginButtonDescription: "Iniciar sessão",
      isAuthenticatedAndHasGroups: false,
    };

    this.foundationServices = new services.APIFoundationServices();
    this.oneyAPPServices = new services.APIOneyAPPServices();
    this.userNameTextFieldRef = React.createRef();
  }

  //TODO call i18n dictionary
  componentDidMount() {
    /* To allow enter press in the login area and the group login area,
    the event listner has to be attached to the document for several reasons:
    In the login area the user textbox can be autofocusable but on the group area none of the inputs can;
    A onPress event on the div will not be called in the group area since no elements are focused by default
    */
    document.addEventListener("keydown", this.handleOnKeyPress);
  }

  /**
   * Logic to know when the authentication was made
   * To show the list of available groups.
   * @param {*} prevProps
   * @param {*} prevState
   */
  componentDidUpdate(prevProps, prevState) {}

  handleClickShowPassword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };
  /**
   * to capture the key enter
   * @param {*} event
   */
  handleOnKeyPress = (event) => {
    if (event.key === "Enter") {
      this.login();
    }
  };

  handleChange = (event) => {
    const {
      target: { name, value },
    } = event;
    this.setState({ [name]: value });
  };

  loadUserGroups = () => {
    const headers = {
      Authorization: "Bearer " + this.state.authData.accessToken,
    };
    this.oneyAPPServices
      .getUserGroups(headers)
      .then((response) => {
        if (!response.data || response.data.length === 0) {
          this.setState({
            isLoginLoading: false,
            errorMessage:
              "O utilizador não pertence a qualquer grupo autorizado",
          }); //todo use i18n
        } else if (response.data.length === 1) {
          this.setState(
            { userGroupName: response.data[0].name },
            this.submitAuthentication
          );
        } else {
          this.setState({
            isLoginLoading: false,
            isAuthenticatedAndHasGroups: true,
            userGroups: response.data.map((el) => ({
              value: el.name,
              description: el.description,
            })),
            userGroupName: response.data[0].name,
            loginButtonDescription: "Entrar",
          });
        }
      })
      .catch((error) => {
        this.setState({
          isLoginLoading: false,
          errorMessage: "Ocorreu um erro ao obter os grupos",
        }); //todo use i18n
        console.log(error);
      });
  };

  login = () => {
    const { isLoginLoading } = this.state;
    if (isLoginLoading) {
      return;
    }

    const { username, password, userGroups, userGroupName } = this.state;

    if (!username.trim() || !password.trim()) {
      this.setState({ errorMessage: "Utilizador ou Palavra-Passe em falta." }); //todo use i18n
    } else if (userGroups.length > 1 && userGroupName) {
      this.submitAuthentication();
    } else {
      this.setState({ isLoginLoading: true }, () => {
        this.foundationServices
          .getAuthenticationToken(username, password)
          .then((response) => {
            this.setState(
              { authData: response.data, errorMessage: "" },
              this.loadUserGroups
            );
          })
          .catch((error) => {
            if (error.response && error.response.status === 401) {
              this.setState({
                errorMessage: "Utilizador ou Palavra-Passe inválida.",
                isLoginLoading: false,
                authData: undefined,
              }); //todo use i18n
            } else {
              this.setState({
                errorMessage:
                  "Ocorreu um erro durante a comunicação com o servidor.",
                isLoginLoading: false,
                authData: undefined,
              }); //todo use i18n
            }
            console.log(error);
          });
      });
    }
  };
  /**
   * To put the login form ready to collect data and perform a new login.
   */
  resetLoginForm = () =>
    this.setState(
      {
        username: "",
        password: "",
        userGroups: [],
        authData: {},
        errorMessage: "",
        isLoginLoading: false,
        showPassword: false,
        userGroupName: "",
        isAuthenticatedAndHasGroups: false,
        loginButtonDescription: "Iniciar sessão",
      },
      () => this.userNameTextFieldRef.current.focus()
    );

  submitAuthentication = () => {
    // call set group
    const headers = {
      Authorization: "Bearer " + this.state.authData.accessToken,
    };
    this.oneyAPPServices
      .setUserGroup(headers, this.state.userGroupName)
      .then(() => {
        const { authData } = this.state;
        services.sendAuthDataToUFE(authData);
        this.resetLoginForm();
      })
      .catch((error) => {
        this.setState({
          errorMessage: "Ocorreu um erro ao associar o utilizador ao grupo.",
          isLoginLoading: false,
        });
      });
  };
  /**
   * Method used when it's pretended to change user.
   */
  changeUser = () => this.resetLoginForm();

  render() {
    return (
      <ThemeProvider
        type={"light"}
        direction={"ltr"}
        theme={theme}
        themeOverrides={{}}
        settings={{
          hostPort: this.state.hostPort,
          contentUrl: this.state.contentUrl,
        }}
        useGlobalCss={true}
      >
        <RootLogin>
          <GroupLogin
            username={this.state.username}
            password={this.state.password}
            showPassword={this.state.showPassword}
            errorMessage={this.state.errorMessage}
            isLoginLoading={this.state.isLoginLoading}
            handleChange={this.handleChange}
            handleClickShowPassword={this.handleClickShowPassword}
            login={this.login}
            loginButtonDescription={this.state.loginButtonDescription}
            isAuthenticatedAndHasGroups={this.state.isAuthenticatedAndHasGroups}
            changeUser={this.changeUser}
            userNameTextFieldRef={this.userNameTextFieldRef}
          >
            {this.state.userGroups.length > 1 && (
              <GroupSelect
                label="Grupo"
                name="userGroupName"
                idName="group-simple-select"
                itemsList={this.state.userGroups}
                value={this.state.userGroupName}
                handleChange={this.handleChange}
              />
            )}
          </GroupLogin>
        </RootLogin>
      </ThemeProvider>
    );
  }
}
