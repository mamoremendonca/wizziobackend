import React from 'react';
import { withStyles } from "@ui-lib/core/styles";
import InputLabel from '@ui-lib/core/InputLabel';
import MenuItem from '@ui-lib/core/MenuItem';
import Select from '@ui-lib/core/Select';
import FormControl from '@ui-lib/core/FormControl';
import {styles, ComponentName} from './SimpleSelectStyles';

const SimpleSelect = ({classes, value, name, label, idName, handleChange, itemsList, noEntry}) => {
    return (
        <FormControl fullWidth className={classes.formControl}>
          <InputLabel htmlFor="group-simple">{label}</InputLabel>
          <Select
            value={value}
            onChange={handleChange}
            inputProps={{
              classes: {
                icon: classes.icon
              },
              name: name,
              id: idName,
              icon: classes.icon
            }}
          >
            {noEntry && <MenuItem value="">
              <em>-</em>
            </MenuItem>}
            {
                itemsList.map((el,index)=>{
                    return <MenuItem key={idName+'-item-'+index} value={el.value}>{el.description}</MenuItem>;
                }) 
            }
          </Select>
        </FormControl>
    );
}

export default withStyles(styles, {name : ComponentName, withTheme : true})(SimpleSelect);
        