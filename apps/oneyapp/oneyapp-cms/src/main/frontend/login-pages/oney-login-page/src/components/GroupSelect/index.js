import React from 'react';
import {styles, ComponentName} from './GroupSelectStyles';
import { withStyles } from "@ui-lib/core/styles";
import classnames from '@ui-lib/core/classNames';
import Icon from '@ui-lib/core/Icon';
import SimpleSelect from './SimpleSelect';

const GroupSelect = ({classes, value, name, label, idName, handleChange, itemsList, noEntry}) => {
    return (
        <div className={classes.root} >
            <Icon className={classnames("icon-tree",classes.icon)}/>            
            <SimpleSelect
                label={label}
                name={name}
                idName = {idName}
                itemsList={itemsList}
                value={value}
                handleChange={handleChange}
                noEntry={noEntry}
            />
        </div>        
    );
}

export default withStyles(styles, {name : ComponentName, withTheme : true})(GroupSelect);