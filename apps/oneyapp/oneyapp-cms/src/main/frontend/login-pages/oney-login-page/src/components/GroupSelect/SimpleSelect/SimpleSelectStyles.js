const ComponentName = "SimpleSelect";

const styles = theme => ({
    formControl: {
        marginLeft: "16px",
        minWidth: 120,
    },
    icon: {
        fill: theme.palette.primary.main,
    },
});

export {styles, ComponentName} ;