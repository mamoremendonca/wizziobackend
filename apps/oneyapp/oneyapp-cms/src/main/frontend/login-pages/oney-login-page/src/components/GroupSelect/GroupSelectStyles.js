const ComponentName = "GroupSelect";

const styles = theme => ({
    root:{
        display : "flex",
        width : "100%",
        alignItems : "flex-end",
        flexDirection : "row",
        justifyContent : "center",
        marginBottom : "20px"
    },   
    icon : {
        fontSize: "34px",
        flex : "0 0 auto"
    },
});

export{ComponentName, styles};