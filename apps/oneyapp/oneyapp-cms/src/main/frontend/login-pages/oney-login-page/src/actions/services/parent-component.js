/**
 * Sends the auth data through a post message to the UFE Container
 * @param {*} username 
 * @param {*} authData 
 */
export const sendAuthDataToUFE = (authData) => {
    const message = Object.assign(authData, {type:'authentication', username: authData.name})
    window.parent.postMessage(JSON.stringify(message),"*");
}