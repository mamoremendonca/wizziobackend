import React from "react";
import Button from "@ui-lib/core/Button";
import FormControl from "@ui-lib/core/FormControl";
import Icon from "@ui-lib/core/Icon";
import IconButton from "@ui-lib/core/IconButton";
import Input from "@ui-lib/core/Input";
import InputAdornment from "@ui-lib/core/InputAdornment";
import InputLabel from "@ui-lib/core/InputLabel";
import TextField from "@ui-lib/core/TextField";
import Typography from "@ui-lib/core/Typography";
import classnames from "@ui-lib/core/classNames";
import { withStyles } from "@ui-lib/core/styles";
import CircularSpinner from "@ui-lib/custom-components/CircularSpinner";
import { styles, ComponentName } from "./GroupLoginStyles";

const GroupLogin = ({
  classes,
  handleChange,
  username,
  password,
  showPassword,
  handleClickShowPassword,
  login,
  children,
  errorMessage,
  theme,
  isLoginLoading,
  loginButtonDescription,
  isAuthenticatedAndHasGroups,
  changeUser,
  userNameTextFieldRef,
}) => {
  let buttonsClassName = isLoginLoading
    ? classnames(classes.loginButton, classes.loginButtonLoading)
    : classes.loginButton;
  return (
    <div className={classes.root}>
      <div id={"login-logo"} className={classes.loginLogo} />
      <div className={classes.textFieldsRoot}>
        <Icon
          className={classnames(
            "icon-user",
            isAuthenticatedAndHasGroups ? classes.disableIcon : classes.icon
          )}
        />
        <TextField
          inputRef={userNameTextFieldRef}
          title="Domínio\Utilizador"
          fullWidth
          name={"username"}
          label="Utilizador"
          className={classes.textFields}
          onChange={handleChange}
          value={username}
          margin="none"
          autoFocus
          disabled={isAuthenticatedAndHasGroups}
        />
      </div>
      <div className={classes.textFieldsRoot}>
        <Icon
          className={classnames(
            "icon-lock",
            isAuthenticatedAndHasGroups ? classes.disableIcon : classes.icon
          )}
        />
        <FormControl fullWidth className={classes.textFields}>
          <InputLabel htmlFor="adornment-password">
            {"Palavra-Passe"}
          </InputLabel>
          <Input
            title="Palavra-Passe"
            name="password"
            type={
              showPassword && !isAuthenticatedAndHasGroups ? "text" : "password"
            }
            value={password}
            onChange={handleChange}
            disabled={isAuthenticatedAndHasGroups}
            endAdornment={
              <InputAdornment
                key={"password-input-end-adornment"}
                position="end"
              >
                <IconButton
                  color="primary"
                  disabled={isAuthenticatedAndHasGroups}
                  aria-label="Toggle password visibility"
                  onClick={handleClickShowPassword}
                >
                  {showPassword && !isAuthenticatedAndHasGroups ? (
                    <Icon>visibility_off</Icon>
                  ) : (
                    <Icon>visibility</Icon>
                  )}
                </IconButton>
              </InputAdornment>
            }
            margin="none"
          />
        </FormControl>
      </div>
      {isAuthenticatedAndHasGroups && (
        <Button
          variant="contained"
          color="primary"
          fullWidth
          className={classes.changeUser}
          onClick={changeUser}
          title="Mudar de Utilizador"
        >
          Mudar de Utilizador
        </Button>
      )}
      {children}
      <Button
        variant="contained"
        color="primary"
        disableRipple={isLoginLoading}
        fullWidth
        className={buttonsClassName}
        onClick={login}
        title="Iniciar Sessão"
      >
        <div className={classes.loadingSpinnerContainer} />
        <div>{loginButtonDescription}</div>
        <div className={classes.loadingSpinnerContainer}>
          {isLoginLoading && (
            <CircularSpinner
              size={30}
              singleColor={theme.palette.common.white}
            />
          )}
        </div>
      </Button>
      <div className={classes.errorContainer}>
        {errorMessage && (
          <div
            className={classnames(classes.errorContainer, classes.errorPanel)}
          >
            <Icon className={classnames("icon-warning", classes.errorIcon)} />
            <Typography varient={"caption"} className={classes.errorText}>
              {errorMessage}
            </Typography>
          </div>
        )}
      </div>
    </div>
  );
};
export default withStyles(styles, { name: ComponentName, withTheme: true })(
  GroupLogin
);
