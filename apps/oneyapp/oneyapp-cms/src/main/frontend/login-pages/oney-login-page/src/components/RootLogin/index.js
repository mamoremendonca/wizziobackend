import React from 'react';
import { withStyles } from "@ui-lib/core/styles";
import Grid from '@ui-lib/core/Grid';
import {styles, ComponentName }  from "./RootLoginStyles";

const  RootLogin = ({classes,children}) => {
    return (        
        <div className={classes.root}>
            <Grid container
                  direction="row"
                  justify="center"
                  alignItems="stretch"
                  spacing={40}
            >
                <Grid item xs={1} className={classes.loginGrids}/>
                <Grid item xs={3} className={classes.loginGrids}>
                    {children}                   
                </Grid>
                <Grid item xs={1} className={classes.loginGrids}/>
                <Grid item xs={7}/>
            </Grid>
        </div>       
    );
}

export default withStyles(styles, {name : ComponentName, withTheme : true})(RootLogin);