import axios from 'axios';

/**
 * Private method only use as auxiliary to create an object that
 * will be passed as argument in the request.
 * @param {*} params 
 * @param {*} headers 
 * @param {*} timeout 
 * @param {*} responseType 
 * @param {*} auth 
 */
const generateConfigObj = (params, headers, timeout, responseType, auth) => {
    let configObj = {};
    if (params) {
        configObj.params = params;
    }
    if (headers) {
        configObj.headers = headers;
    }
    if (timeout) {
        configObj.timeout = timeout;
    }
    if (responseType) {
        configObj.responseType = responseType;
    }
    if (auth) {
        configObj.auth = auth;
    }
    return configObj;
};

const setHeaders = headers =>{
    let defaultHeaders = {
        Accept : "application/json",
        Pragma: "no-cache",
        Expires: -1,
        "Cache-Control": 'no-store no-cache'

    }

    return Object.assign(defaultHeaders, headers);
}

/**
 * To create independent instances of the http client.
 * Don't forget to use the new keyword.
 */
export class AxiosHttpClientProvider {

    constructor() {
        this.axios = axios;
    }    

    /**
     *
     * @param url
     * @param headers
     * @param params
     * @param timeout
     * @param responseType
     */
    get = (url, headers, params, timeout, responseType) => {
        const mergedHeaders = setHeaders(headers)
        const configObj = generateConfigObj(params, mergedHeaders, timeout, responseType);
        return this.axios.get(url, configObj);
    };


    /**
     *
     * @param url
     * @param headers
     * @param params
     * @param data
     * @param timeout
     * @param responseType
     */
    post = (url, headers, params, data, timeout, responseType, auth) => {
        const mergedHeaders = setHeaders(headers)
        const configObj = generateConfigObj(params, mergedHeaders, timeout, responseType, auth);
        return this.axios.post(url, data, configObj);    
    };
}
