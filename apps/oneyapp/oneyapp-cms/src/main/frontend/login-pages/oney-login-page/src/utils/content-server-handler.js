import * as constants from '../constants';
/**
 *  check in the index.html for the  boot property "runMode" value.
 *  To know if we are in develop ou in another environment.
 */
export const isDevEnv = () => document.getElementById("runMode").innerText === "dev";

/**
 *  check in the index.html for the  boot property "contentUrl" value.
 *  To know if we are in develop ou in another environment.
 */
export const contentBaseURL = () => document.getElementById("baseUrl").innerText;

/**
 * Return the a string with the host and the port concatenated.
 * As a default returns "http://localhost:4500"
 */
export const getCurrentHostAndPort = () => {
    if (isDevEnv()) return constants.HOST_PORT;
    const arr = window.location.href.split("/");
    return arr[0] + "//" + arr[2];
};

/**
 * Returns the path of the root folder in the content server.
 * As a default returns "/content/oney"
 */
export const getContentBaseURL = () => {
    const slingRootContentFolder = contentBaseURL();
    if (isDevEnv() || !slingRootContentFolder) return constants.CONTENT_BASE_URL;
    return slingRootContentFolder;
};




