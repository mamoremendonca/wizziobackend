package com.novabase.omni.oney.apps.threesixtyoney.mapper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.apps.threesixtyoney.dto.mock.MockDataClient;
import com.novabase.omni.oney.apps.threesixtyoney.dto.mock.MockDataPlatfond;
import com.novabase.omnichannel.fs.plugins.apps.threesixty.provider.spi.common.Field;

@Mapper
public interface DataProviderMapper {

    public static final DataProviderMapper INSTANCE = Mappers.getMapper(DataProviderMapper.class);
    
    
    public default List<Field> toFieldListClientData(MockDataClient mockDataClient) {
	
	final List<Field> returnData = new ArrayList<Field>();
	
	returnData.add(new Field("string", "identifier", mockDataClient.getIdentifier()));
	returnData.add(new Field("string", "name", mockDataClient.getName()));
	returnData.add(new Field("string", "shortName", mockDataClient.getShortName()));
	returnData.add(new Field("string", "satisfactionLevel", mockDataClient.getSatisfactionLevel()));
	returnData.add(new Field("string", "clientType", mockDataClient.getClientType().name()));
	returnData.add(new Field("mailto", "email", mockDataClient.getEmail()));
	returnData.add(new Field("call", "homePhone", mockDataClient.getHomePhone()));
	returnData.add(new Field("call", "personalPhone", mockDataClient.getPersonalPhone()));
	returnData.add(new Field("string", "nif", mockDataClient.getNif()));
	returnData.add(new Field("string", "birthDateLabel", mockDataClient.getBirthDateLabel()));
	returnData.add(new Field("string", "address", mockDataClient.getAddress()));
	
	returnData.add(new Field("string", "phoneStatus", mockDataClient.getPhoneStatus()));
	returnData.add(new Field("call", "workPhone", mockDataClient.getWorkPhone()));
	returnData.add(new Field("string", "clientStatus", mockDataClient.getClientStatus()));
	returnData.add(new Field("string", "clientDate", mockDataClient.getClientDate()));
	returnData.add(new Field("boolean", "insolvent", Boolean.toString(mockDataClient.isInsolvent())));
	returnData.add(new Field("string", "birthDate", mockDataClient.getBirthDate()));
	returnData.add(new Field("string", "age", mockDataClient.getAge()));
	returnData.add(new Field("string", "ratingStatus", mockDataClient.getRating().getStatus()));
	returnData.add(new Field("string", "ratingValue", mockDataClient.getRating().getValue()));
	returnData.add(new Field("string", "peap", mockDataClient.getPeap()));
	returnData.add(new Field("string", "pari", mockDataClient.getPari()));
	
	return returnData;
	
    }
    
    public default List<Field> toFieldListProduct(MockDataClient mockDataClient) {
	
	final List<Field> returnData = new ArrayList<Field>();
	mockDataClient.getProduct().stream().forEach(product -> {
	    
	    returnData.add(new Field("string", "cardStatus", product.getCardStatus()));
	    returnData.add(new Field("string", "contractDate", product.getContractDate()));
	    returnData.add(new Field("string", "titularity", product.getTitularity()));
	    returnData.add(new Field("boolean", "protectionInsurance", Boolean.toString(product.isProtectionInsurance())));
	    returnData.add(new Field("string", "paymentMethod", product.getPaymentMethod()));
	    returnData.add(new Field("string", "entity", product.getEntity()));

	    int i = 1;
	    for (String another : product.getAnotherInsurances()) {
		returnData.add(new Field("string", "anotherInsurance" + i++, another));
	    }
	    	    
	    i = 1;
	    for (MockDataPlatfond platfond : product.getPlatfonds()) {
		
		i++;
		returnData.add(new Field("string", "creditLimitUsed" + i, platfond.getCreditLimitUsed()));
		returnData.add(new Field("string", "creditLimitAvailable" + i, platfond.getCreditLimitAvailable()));
		returnData.add(new Field("string", "cashLimitBlockUsed" + i, platfond.getCashLimitBlockUsed()));
		returnData.add(new Field("string", "cashLimitBlockAvailable" + i, platfond.getCashLimitBlockAvailable()));
		returnData.add(new Field("string", "PAIBlockUsed" + i, platfond.getPAIBlockUsed()));
		returnData.add(new Field("string", "PAIBlockAvailable" + i, platfond.getPAIBlockAvailable()));
		
	    }
	    
	});
	
	return returnData;
	
    }
        
}
