package com.novabase.omni.oney.apps.threesixtyoney.provider.search;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/threesixty-search-provider", icon = @Icon(resource = "OSGI-INF/icon/resource.png", size = 32))
public @interface SearchProviderDAOConfig {
 
    public static final String CPID = "fs.base.modules.threesixtyoney.search.provider";
    
    @AttributeDefinition(name = "%providername.name", description = "%providername.description", required = true)
    String providerName() default "data-provider";
    
    @AttributeDefinition(name = "%emailregex.name", description = "%emailregex.description", required = true)
    String emailRegex() default "^[a-zA-Z0-9_!#$%&â€™*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$";
    
    @AttributeDefinition(name = "%mobileregex.name", description = "%mobileregex.description", required = true)
    String mobileRegex() default "^(\\+\\d{1,3}[- ]?)?\\d{9,10}$";
    
    @AttributeDefinition(name = "%idnumberregex.name", description = "%idnumberregex.description", required = true)
    String idnumberRegex() default "^[0-9]{8}$";
    
    @AttributeDefinition(name = "%taxregex.name", description = "%taxregex.description", required = true)
    String taxRegex() default "^[123568]\\d{8}$";
    
    @AttributeDefinition(name = "%customernumberregex.name", description = "%customernumberregex.description", required = true)
    String customernumberRegex() default "^12\\d{9}$";
    
    @AttributeDefinition(name = "%nifcostumerregex.name", description = "%nifcostumerregex.description", required = true)
    String nifCustomerRegex() default "[0-9]+";
}