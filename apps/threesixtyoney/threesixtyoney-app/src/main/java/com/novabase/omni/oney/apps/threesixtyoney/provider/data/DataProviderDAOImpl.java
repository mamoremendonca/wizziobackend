package com.novabase.omni.oney.apps.threesixtyoney.provider.data;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.apps.threesixtyoney.dto.mock.MockDataClient;
import com.novabase.omni.oney.apps.threesixtyoney.dto.mock.MockDataPlatfond;
import com.novabase.omni.oney.apps.threesixtyoney.dto.mock.MockDataProduct;
import com.novabase.omni.oney.apps.threesixtyoney.dto.mock.MockDataRating;
import com.novabase.omni.oney.apps.threesixtyoney.mapper.DataProviderMapper;
import com.novabase.omni.oney.apps.threesixtyoney.provider.data.datasource.SearchDataSource;
import com.novabase.omni.oney.apps.threesixtyoney.provider.data.enums.ClientType;
import com.novabase.omnichannel.fs.plugins.apps.threesixty.provider.spi.common.Field;
import com.novabase.omnichannel.fs.plugins.apps.threesixty.provider.spi.data.ThreesixtyDataProviderDAO;
import com.novabase.omnichannel.fs.plugins.apps.threesixty.provider.spi.entity.DataProviderDTO;

import io.digitaljourney.platform.modules.commons.AbstractConfigurableComponent;

@Component(
        configurationPid = DataProviderDAOConfig.CPID,
        configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = DataProviderDAOConfig.class)
public class DataProviderDAOImpl extends AbstractConfigurableComponent<DataProviderDAOConfig> implements ThreesixtyDataProviderDAO{


// Generate javadoc
    
    @Activate
    public void activated(DataProviderDAOConfig config) {
        prepare(config);
    }

 
    @Modified
    public void modified(DataProviderDAOConfig config) {
        prepare(config);
    }
    
//    @Reference
//    private NpsResource nps;
    
    @Override
    public String name() {
        return getConfig().providerName();
    }
    
    private List<MockDataProduct> initResourceProduct () { 
    	
	List<String> otherInsurancesList = Arrays.asList("Nseguros", "Fidelidade", "Tranquilidade");
        	
    	MockDataPlatfond mockDataPlatfond1 = new MockDataPlatfond();
    	mockDataPlatfond1.setCreditLimitAvailable("1200");
    	mockDataPlatfond1.setCreditLimitUsed("700");
    	mockDataPlatfond1.setCashLimitBlockAvailable("1100");
    	mockDataPlatfond1.setCashLimitBlockUsed("600");
    	mockDataPlatfond1.setPAIBlockAvailable("1000");
    	mockDataPlatfond1.setPAIBlockUsed("500");
    	
    	MockDataPlatfond mockDataPlatfond2 = new MockDataPlatfond();
    	mockDataPlatfond2.setCreditLimitAvailable("1200");
    	mockDataPlatfond2.setCreditLimitUsed("700");
    	mockDataPlatfond2.setCashLimitBlockAvailable("1100");
    	mockDataPlatfond2.setCashLimitBlockUsed("600");
    	mockDataPlatfond2.setPAIBlockAvailable("1000");
    	mockDataPlatfond2.setPAIBlockUsed("500");
    	
    	MockDataPlatfond mockDataPlatfond3 = new MockDataPlatfond();
    	mockDataPlatfond3.setCreditLimitAvailable("1200");
    	mockDataPlatfond3.setCreditLimitUsed("700");
    	mockDataPlatfond3.setCashLimitBlockAvailable("1100");
    	mockDataPlatfond3.setCashLimitBlockUsed("600");
    	mockDataPlatfond3.setPAIBlockAvailable("1000");
    	mockDataPlatfond3.setPAIBlockUsed("500");
    	
    	List<MockDataPlatfond> dataPlatfondList = Arrays.asList(mockDataPlatfond1, mockDataPlatfond2, mockDataPlatfond3);
    	
    	MockDataProduct mockDataProduct = new MockDataProduct();
    	
    	mockDataProduct.setCardStatus("Active");
    	mockDataProduct.setContractDate("01-01-2012");
    	mockDataProduct.setTitularity("1ºTitular");
    	mockDataProduct.setProtectionInsurance(true);
    	mockDataProduct.setAnotherInsurances(otherInsurancesList);
    	mockDataProduct.setPaymentMethod("Através de cartão");
    	mockDataProduct.setPlatfonds(dataPlatfondList);
    	mockDataProduct.setEntity("DP1");
    	
    	List<MockDataProduct> dataProductList = Arrays.asList(mockDataProduct);
    	
    	return dataProductList;
    }

    
    private List<MockDataClient> initResource (){
    	
    	MockDataRating rating = new MockDataRating();
    	rating.setStatus("Active");
    	rating.setValue("0.05");
    	
    	List<MockDataProduct> mockDataProductList = this.initResourceProduct();
   
    	MockDataClient mock1 = new MockDataClient();
    	mock1.setIdentifier("MC1");
    	mock1.setName("Zé das Cenouras");
    	mock1.setShortName("Zé Cenouras");
    	mock1.setClientType(ClientType.PREMIUM);
    	mock1.setEmail("zedascenouras@mail.com");
    	mock1.setPhoneStatus("Active");
    	mock1.setHomePhone("211466779");
    	mock1.setWorkPhone("311473739");
    	mock1.setClientStatus("Active");
    	mock1.setClientDate("01-01-2012");
    	mock1.setSatisfactionLevel("Feliz");
    	mock1.setInsolvent(true);
    	mock1.setPersonalPhone("961466779");
    	mock1.setNif("123456789");
    	mock1.setBirthDate("01-01-1990");
    	mock1.setAge("30");
    	mock1.setAddress("Rua das Cenouras N1");
    	mock1.setRating(rating);
    	mock1.setPeap("0.07");
    	mock1.setPari("0.35");
    	mock1.setProduct(mockDataProductList);
    	
    	MockDataClient mock2 = new MockDataClient();
    	mock2.setIdentifier("MC2");
    	mock2.setName("António das Couves");
    	mock2.setShortName("António Couves");
    	mock2.setClientType(ClientType.PREMIUM);
    	mock2.setEmail("antoniodascouves@mail.com");
    	mock2.setPhoneStatus("Active");
    	mock2.setHomePhone("211466779");
    	mock2.setWorkPhone("311476739");
    	mock2.setSatisfactionLevel("Infeliz");
    	mock2.setClientStatus("Active");
    	mock2.setClientDate("01-01-2012");
    	mock2.setInsolvent(true);
    	mock2.setPersonalPhone("961466779");
    	mock2.setNif("987654321");
    	mock2.setBirthDate("01-01-1990");
    	mock2.setAge("30");
    	mock2.setAddress("Rua das Couves N1");
    	mock2.setRating(rating);
    	mock2.setPeap("0.07");
    	mock2.setPari("0.35");
    	mock2.setProduct(mockDataProductList);
    	
    	MockDataClient mock3 = new MockDataClient();
    	mock3.setIdentifier("MC3");
    	mock3.setName("Castro das Batatas");
    	mock3.setShortName("Castro Batatas");
    	mock3.setClientType(ClientType.PREMIUM);
    	mock3.setEmail("castrodasbatatas@mail.com");
    	mock3.setPhoneStatus("Active");
    	mock3.setHomePhone("211466779");
    	mock3.setWorkPhone("311476779");
    	mock3.setSatisfactionLevel("Indiferente");
    	mock3.setClientStatus("Active");
    	mock3.setClientDate("01-01-2012");
    	mock3.setInsolvent(true);
    	mock3.setPersonalPhone("961466779");
    	mock3.setNif("456789123");
    	mock3.setBirthDate("01-01-1990");
    	mock3.setAge("30");
    	mock3.setAddress("Rua das Batatas N1");
    	mock3.setRating(rating);
    	mock3.setPeap("0.07");
    	mock3.setPari("0.35");
    	mock3.setProduct(mockDataProductList);

    	List<MockDataClient> mockList = Arrays.asList(mock1, mock2, mock3);
    	
    	return mockList;
    	
    }
 
    @Override
    public List<DataProviderDTO> getData(List<Field> fields) {

    	List<MockDataClient> mockDataClient = this.initResource();
    	List<DataProviderDTO> data = new ArrayList<DataProviderDTO>();
    	
    	Field found = fields.stream().filter(field -> field.key.equals("id")).findAny().orElse(null);
    	String identifier = found != null ? found.value : null;
    	
    	if (StringUtils.isEmpty(identifier) == false) {

    	    MockDataClient mockFound = mockDataClient.stream().filter(mock -> mock.getIdentifier().equals(identifier)).findAny().orElse(null);
    	    if (mockFound != null) {
    		
    		DataProviderDTO dataProviderClientData = new DataProviderDTO();
                dataProviderClientData.dataSource = SearchDataSource.CLIENTDATA.name();
                dataProviderClientData.fields = new ArrayList<Field>();
                dataProviderClientData.fields.addAll(this.maskClientData(DataProviderMapper.INSTANCE.toFieldListClientData(mockFound)));
                
                
                DataProviderDTO dataProviderClientProduct = new DataProviderDTO();
                dataProviderClientProduct.dataSource = SearchDataSource.PRODUCTS.name();
                dataProviderClientProduct.fields = new ArrayList<Field>();
                dataProviderClientProduct.fields.addAll(DataProviderMapper.INSTANCE.toFieldListProduct(mockFound));

                data.addAll(Arrays.asList(dataProviderClientData, dataProviderClientProduct));
    		
    	    }
    	        	    
    	}
        
        return data;
    
    }

    private List<Field> maskClientData(List<Field> fields) {
        
        fields.stream().forEach(f -> {
            if(Arrays.stream(getConfig().clientDataFieldsToHide()).anyMatch(f.key::equals))
                f.key = getConfig().hidingPrefix() + f.key;
        });
        
        return fields;
        
    }
    
}