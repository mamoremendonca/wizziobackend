package com.novabase.omni.oney.apps.threesixtyoney.dto.mock;

import org.osgi.dto.DTO;

public class MockDataRating extends DTO {

	public String status;
	public String value;
	
	public MockDataRating() {
		super();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
