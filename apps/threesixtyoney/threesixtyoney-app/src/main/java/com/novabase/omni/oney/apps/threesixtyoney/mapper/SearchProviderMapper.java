package com.novabase.omni.oney.apps.threesixtyoney.mapper;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.apps.threesixtyoney.dto.mock.MockClient;
import com.novabase.omnichannel.fs.plugins.apps.threesixty.provider.spi.common.Field;
import com.novabase.omnichannel.fs.plugins.apps.threesixty.provider.spi.entity.SearchProviderDTO;

@Mapper
public interface SearchProviderMapper {
	
	public static final SearchProviderMapper INSTANCE = Mappers.getMapper(SearchProviderMapper.class);
	
	@Mapping(source = "identifier", target = "id")
	@Mapping(source = "name", target = "primaryName")
	@Mapping(expression = "java(mockClient.getName().substring(0,1))", target = "shortIdentifier")
	@Mapping(expression = "java(\"supplier\")", target = "identifierType")
	@Mapping(source = "nif", target = "number")
	@Mapping(expression = "java(this.toFieldList(mockClient))", target = "fields")
	public SearchProviderDTO toSearchProviderDTO (MockClient mockClient);
	
	@IterableMapping(elementTargetType = SearchProviderDTO.class)
	public List<SearchProviderDTO> toSearchProviderDTOList (List<MockClient> mockClientList);
	
	public default List<Field> toFieldList (MockClient mockClient) {
		
		List<Field> fields = new ArrayList<Field>();
		
		fields.add(new Field("String", "region", mockClient.getRegion()));
		
		return fields;
		
	}

}
