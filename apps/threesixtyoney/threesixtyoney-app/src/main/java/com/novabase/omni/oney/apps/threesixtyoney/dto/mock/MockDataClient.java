package com.novabase.omni.oney.apps.threesixtyoney.dto.mock;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.apps.threesixtyoney.provider.data.enums.ClientType;

public class MockDataClient extends DTO{

	private String identifier;
	private String name;
	private String shortName;
	private ClientType clientType;
	private String satisfactionLevel;
	private String email;
	private String phoneStatus;
	private String homePhone;
	private String workPhone;
	private String clientStatus;
	private String clientDate;
	private boolean insolvent;
	private String personalPhone;
	private String nif;
	private String birthDate;
	private String age;
	private String address;
	private MockDataRating rating;
	private String peap;
	private String pari;
	private List<MockDataProduct> product;
	
	public MockDataClient() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getWorkPhone() {
	    return workPhone;
	}

	// method to calculate client age
	public String getBirthDateLabel() {
	   
	    DateTimeFormatter inputformatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	    final LocalDate birthDate = LocalDate.parse(this.birthDate, inputformatter);
	    
	    DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("d MMM yyyy");
	    final StringBuilder output = new StringBuilder();
	    output.append(birthDate.format(outputFormatter));
	    
	    final LocalDate today = LocalDate.now();
	    output.append(" (");
	    // counting year between today and birthdate
	    output.append(Period.between(birthDate, today).getYears());
	    output.append(" anos)");
	    return output.toString();
	    
	}

	public void setWorkPhone(String workPhone) {
	    this.workPhone = workPhone;
	}

	public String getShortName() {
	    return shortName;
	}

	public void setShortName(String shortName) {
	    this.shortName = shortName;
	}

	public String getSatisfactionLevel() {
	    return satisfactionLevel;
	}

	public void setSatisfactionLevel(String satisfactionLevel) {
	    this.satisfactionLevel = satisfactionLevel;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ClientType getClientType() {
		return clientType;
	}

	public void setClientType(ClientType clientType) {
		this.clientType = clientType;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneStatus() {
		return phoneStatus;
	}

	public void setPhoneStatus(String phoneStatus) {
		this.phoneStatus = phoneStatus;
	}

	public String getHomePhone() {
	    return homePhone;
	}

	public void setHomePhone(String homePhone) {
	    this.homePhone = homePhone;
	}

	public String getClientStatus() {
		return clientStatus;
	}

	public void setClientStatus(String clientStatus) {
		this.clientStatus = clientStatus;
	}

	public String getClientDate() {
		return clientDate;
	}

	public void setClientDate(String clientDate) {
		this.clientDate = clientDate;
	}

	public boolean isInsolvent() {
		return insolvent;
	}

	public void setInsolvent(boolean insolvent) {
		this.insolvent = insolvent;
	}

	public String getPersonalPhone() {
		return personalPhone;
	}

	public void setPersonalPhone(String personalPhone) {
		this.personalPhone = personalPhone;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public MockDataRating getRating() {
		return rating;
	}

	public void setRating(MockDataRating rating) {
		this.rating = rating;
	}

	public String getPeap() {
		return peap;
	}

	public void setPeap(String peap) {
		this.peap = peap;
	}

	public String getPari() {
		return pari;
	}

	public void setPari(String pari) {
		this.pari = pari;
	}

	public List<MockDataProduct> getProduct() {
		return product;
	}

	public void setProduct(List<MockDataProduct> product) {
		this.product = product;
	}
	
}
