package com.novabase.omni.oney.apps.threesixtyoney.provider.data;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/threesixty-data-provider", icon = @Icon(resource = "OSGI-INF/icon/resource.png", size = 32))
public @interface DataProviderDAOConfig {

     public static final String CPID = "fs.base.modules.threesixtyoney.data.provider";
    
    @AttributeDefinition(name = "%providername.name", description = "%providername.description", required = true)
    String providerName() default "data-provider";

    // Shouldn't be optional?
    @AttributeDefinition(name = "%hidingPrefix.name", description = "%hidingPrefix.description", required = true)
    String hidingPrefix() default "_";
    
    // Shouldn't be optional?
    @AttributeDefinition(name = "%npsfieldstohide.name", description = "%npsfieldstohide.description", required = true)
    String[] npsFieldsToHide() default {"customer_id"};
    
    // Shouldn't be optional?
    @AttributeDefinition(name = "%clientDataFieldsToHide.name", description = "%clientDataFieldsToHide.description", required = true)
    String[] clientDataFieldsToHide() default {"identifier", "clientType", "satisfactionLevel", "ratingStatus", "ratingValue"};
    
}