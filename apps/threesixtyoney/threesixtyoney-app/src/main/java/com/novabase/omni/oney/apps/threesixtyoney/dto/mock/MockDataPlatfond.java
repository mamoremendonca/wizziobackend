package com.novabase.omni.oney.apps.threesixtyoney.dto.mock;

import org.osgi.dto.DTO;

public class MockDataPlatfond extends DTO {
	
	private String creditLimitUsed;
	private String creditLimitAvailable;
	private String cashLimitBlockUsed;
	private String cashLimitBlockAvailable;
	private String PAIBlockUsed;
	private String PAIBlockAvailable;
	
	public MockDataPlatfond() {
		super();
	}
	
	public String getCreditLimitUsed() {
		return creditLimitUsed;
	}
	public void setCreditLimitUsed(String creditLimitUsed) {
		this.creditLimitUsed = creditLimitUsed;
	}
	public String getCreditLimitAvailable() {
		return creditLimitAvailable;
	}
	public void setCreditLimitAvailable(String creditLimitAvailable) {
		this.creditLimitAvailable = creditLimitAvailable;
	}
	public String getCashLimitBlockUsed() {
		return cashLimitBlockUsed;
	}
	public void setCashLimitBlockUsed(String cashLimitBlockUsed) {
		this.cashLimitBlockUsed = cashLimitBlockUsed;
	}
	public String getCashLimitBlockAvailable() {
		return cashLimitBlockAvailable;
	}
	public void setCashLimitBlockAvailable(String cashLimitBlockAvailable) {
		this.cashLimitBlockAvailable = cashLimitBlockAvailable;
	}
	public String getPAIBlockUsed() {
		return PAIBlockUsed;
	}
	public void setPAIBlockUsed(String pAIBlockUsed) {
		PAIBlockUsed = pAIBlockUsed;
	}
	public String getPAIBlockAvailable() {
		return PAIBlockAvailable;
	}
	public void setPAIBlockAvailable(String pAIBlockAvailable) {
		PAIBlockAvailable = pAIBlockAvailable;
	}
	
	

	
}
