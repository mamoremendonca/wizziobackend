package com.novabase.omni.oney.apps.threesixtyoney.provider.data.datasource;

public enum SearchDataSource {
	
	CLIENTDATA, PRODUCTS

}
