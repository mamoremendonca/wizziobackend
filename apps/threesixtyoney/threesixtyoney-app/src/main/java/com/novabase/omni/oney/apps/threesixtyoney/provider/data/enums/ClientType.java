package com.novabase.omni.oney.apps.threesixtyoney.provider.data.enums;

public enum ClientType {

	NORMAL, PREMIUM, CONSERVATIVE
	
}
