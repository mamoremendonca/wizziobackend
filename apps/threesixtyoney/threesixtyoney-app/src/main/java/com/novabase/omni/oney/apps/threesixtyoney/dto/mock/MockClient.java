package com.novabase.omni.oney.apps.threesixtyoney.dto.mock;

import org.osgi.dto.DTO;

public class MockClient extends DTO {
	
	private String name;
	private String nif;
	private String identifier;
	private String region;
	
	public MockClient() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getIdentifier() {
		return identifier;
	}
	
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}
	
	

}
