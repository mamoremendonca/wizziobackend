package com.novabase.omni.oney.apps.threesixtyoney.dto.mock;

import java.util.List;

import org.osgi.dto.DTO;

public class MockDataProduct extends DTO {

	private String cardStatus;
	private String contractDate;
	private String titularity;
	private boolean protectionInsurance;
	private List<String> anotherInsurances;
	private String paymentMethod;
	private List<MockDataPlatfond> platfonds;
	private String entity;
	
	public MockDataProduct() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCardStatus() {
		return cardStatus;
	}

	public void setCardStatus(String cardStatus) {
		this.cardStatus = cardStatus;
	}

	public String getContractDate() {
		return contractDate;
	}

	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}

	public String getTitularity() {
		return titularity;
	}

	public void setTitularity(String titularity) {
		this.titularity = titularity;
	}

	public boolean isProtectionInsurance() {
		return protectionInsurance;
	}

	public void setProtectionInsurance(boolean protectionInsurance) {
		this.protectionInsurance = protectionInsurance;
	}

	public List<String> getAnotherInsurances() {
	    return anotherInsurances;
	}

	public void setAnotherInsurances(List<String> anotherInsurances) {
	    this.anotherInsurances = anotherInsurances;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public List<MockDataPlatfond> getPlatfonds() {
		return platfonds;
	}

	public void setPlatfonds(List<MockDataPlatfond> platfonds) {
		this.platfonds = platfonds;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}



}
