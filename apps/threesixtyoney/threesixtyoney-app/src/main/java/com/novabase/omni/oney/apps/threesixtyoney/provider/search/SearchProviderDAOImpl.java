package com.novabase.omni.oney.apps.threesixtyoney.provider.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.cxf.common.util.StringUtils;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.apps.threesixtyoney.dto.mock.MockClient;
import com.novabase.omni.oney.apps.threesixtyoney.mapper.SearchProviderMapper;
import com.novabase.omnichannel.fs.plugins.apps.threesixty.provider.spi.entity.SearchProviderDTO;
import com.novabase.omnichannel.fs.plugins.apps.threesixty.provider.spi.search.ThreesixtySearchProviderDAO;

import io.digitaljourney.platform.modules.commons.AbstractConfigurableComponent;

@Component(
        configurationPid = SearchProviderDAOConfig.CPID,
        configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = SearchProviderDAOConfig.class)
public class SearchProviderDAOImpl extends AbstractConfigurableComponent<SearchProviderDAOConfig> implements ThreesixtySearchProviderDAO {

//  Generate javadoc
    
    @Activate
    public void activated(SearchProviderDAOConfig config) {
        prepare(config);
    }


    @Modified
    public void modified(SearchProviderDAOConfig config) {
        prepare(config);
    }
    
//    @Reference
//    private PartyManagementResource partyManagement;
    
    @Override
    public String name() {
        return getConfig().providerName();
    }

 
    private List<MockClient> initResource (){
    	
    	MockClient mock1 = new MockClient();
    	mock1.setName("Zé das Cenouras");
    	mock1.setNif("123456789");
    	mock1.setIdentifier("MC1");
    	mock1.setRegion("Algarve");
    	
    	MockClient mock2 = new MockClient();
    	mock2.setName("António das Couves");
    	mock2.setNif("987654321");
    	mock2.setIdentifier("MC2");
    	mock2.setRegion("Porto");
    	
    	MockClient mock3 = new MockClient();
    	mock3.setName("Castro das Batatas");
    	mock3.setNif("456789123");
    	mock3.setIdentifier("MC3");
    	mock3.setRegion("Lisboa");
    	
    	List<MockClient> mockList = Arrays.asList(mock1, mock2, mock3);
    	
    	return mockList;
    }
    
    @Override
    public List<SearchProviderDTO> search(String param) {
        
    	List<SearchProviderDTO> result = new ArrayList<>();
        List<MockClient> mockClientList = this.initResource();
        
        if (StringUtils.isEmpty(param) == false) {	
			
			boolean isNumber = param.matches(this.getConfig().nifCustomerRegex());

			List<MockClient> searchResult = mockClientList.stream().
					filter(mock -> isNumber ? mock.getNif().startsWith(param) : mock.getName().toLowerCase().contains(param.toLowerCase())).
					collect(Collectors.toList());
			
			result.addAll(SearchProviderMapper.INSTANCE.toSearchProviderDTOList(searchResult));		
			
        }
           
        return result;
        
    }

}    