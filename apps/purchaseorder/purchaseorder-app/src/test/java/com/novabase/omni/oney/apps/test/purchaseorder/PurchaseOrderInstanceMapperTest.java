package com.novabase.omni.oney.apps.test.purchaseorder;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import java.time.LocalDateTime;
import org.junit.Test;

import com.novabase.omni.oney.apps.purchaseorder.dto.ItemDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderHeaderDTO;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.Item;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.PurchaseOrderHeader;
import com.novabase.omni.oney.apps.purchaseorder.enums.PurchaseOrderType;
import com.novabase.omni.oney.apps.purchaseorder.mapper.PurchaseOrderInstanceMapper;

import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.api.trigger.instance.JourneyInstanceHeader;

public class PurchaseOrderInstanceMapperTest {

    @Test
    public void mergeItemDTO() {
	ItemDTO itemDTO = new ItemDTO();
	itemDTO.index = 4;
	itemDTO.amount = 1D;
	itemDTO.code = "CODE ARTICLE DTO";
	itemDTO.description = "ARTICLE DESCRIPTION DTO";
	itemDTO.costCenterCode = "CODE COAST CENTER DTO";
	itemDTO.comment = "COMMENT DTO";
	itemDTO.startDate = LocalDateTime.of(2019, 2, 15, 20, 30, 30);
	itemDTO.endDate = LocalDateTime.of(2020, 2, 15, 20, 30, 30);
	itemDTO.taxCode = "CODE TAX DTO";
	itemDTO.unitPriceWithoutTax = 10.00;

	Item item = new Item();
	item.index = 1;
	item.amount = 2D;
	item.code = "CODE ARTICLE ENTITY";
	item.description = "ARTICLE DESCRIPTION ENTITY";
	item.costCenterCode = "CODE COAST CENTER ENTITY";
	item.comment = "COMMENT ENTITY";
	item.startDate = LocalDateTime.of(2019, 5, 15, 20, 30, 30);
	item.endDate = LocalDateTime.of(2020, 5, 15, 20, 30, 30);
	item.taxCode = "CODE TAX ENTITY";
	item.unitPriceWithoutTax = 12.00;

	PurchaseOrderInstanceMapper.INSTANCE.mergeItemDTO(item, itemDTO);

	assertEquals(1, item.index);
	assertEquals(new Double(1), item.amount);
	assertEquals("CODE ARTICLE DTO", item.code);
	assertEquals("ARTICLE DESCRIPTION DTO", item.description);
	assertEquals("CODE COAST CENTER DTO", item.costCenterCode);
	assertEquals("COMMENT DTO", item.comment);
	assertEquals(LocalDateTime.of(2019, 2, 15, 20, 30, 30), item.startDate);
	assertEquals(LocalDateTime.of(2020, 2, 15, 20, 30, 30), item.endDate);
	assertEquals("CODE TAX DTO", item.taxCode);
	assertEquals(new Double(10.00), item.unitPriceWithoutTax);

    }

    @Test
    public void mergePurchaseHeaderDTOtoInstance() {
	PurchaseOrderHeaderDTO purchaseOrderHeaderDTO = new PurchaseOrderHeaderDTO();

	purchaseOrderHeaderDTO.number = 3222L;
	purchaseOrderHeaderDTO.version = 2L;
	purchaseOrderHeaderDTO.type = PurchaseOrderType.CONTRACT;
	purchaseOrderHeaderDTO.supplierCode = "SP23552";
	purchaseOrderHeaderDTO.description = "description description description ";
	purchaseOrderHeaderDTO.departmentCode = "DP66345";
	purchaseOrderHeaderDTO.projectCode = "PJ3123";
	purchaseOrderHeaderDTO.internalOrderCode = "IO6634";
	purchaseOrderHeaderDTO.expirationDate = LocalDateTime.of(2020, 2, 16, 20, 30, 30);

	PurchaseOrderInstance instance = new PurchaseOrderInstance();
	instance.setHeader(new JourneyInstanceHeader());
	instance.getHeader().instanceId = 1L;
	instance.purchaseOrderHeader = new PurchaseOrderHeader();
	instance.purchaseOrderHeader.repositoryId = 22L;
	instance.purchaseOrderHeader.version = 4L;
	instance.purchaseOrderHeader.number = 33L;

	PurchaseOrderInstanceMapper.INSTANCE.mergePurchaseOrderHeaderDTO(instance, purchaseOrderHeaderDTO);

	assertEquals(new Long(22), instance.purchaseOrderHeader.repositoryId);
	assertEquals(new Long(33), instance.purchaseOrderHeader.number);
	assertEquals(new Long(4), instance.purchaseOrderHeader.version);
	assertEquals(purchaseOrderHeaderDTO.type, instance.purchaseOrderHeader.type);
	assertEquals(purchaseOrderHeaderDTO.supplierCode, instance.purchaseOrderHeader.supplierCode);
	assertEquals(purchaseOrderHeaderDTO.description, instance.purchaseOrderHeader.description);
	assertEquals(purchaseOrderHeaderDTO.departmentCode, instance.purchaseOrderHeader.departmentCode);
	assertEquals(purchaseOrderHeaderDTO.projectCode, instance.purchaseOrderHeader.projectCode);
	assertEquals(purchaseOrderHeaderDTO.internalOrderCode, instance.purchaseOrderHeader.internalOrderCode);
	assertEquals(LocalDateTime.of(2020, 2, 16, 20, 30, 30), instance.purchaseOrderHeader.expirationDate);
	assertEquals(new Long(1), instance.getHeader().instanceId);

    }

    @Test
    public void instanceToDTO() {
	PurchaseOrderInstance instance = new PurchaseOrderInstance();
	instance.purchaseOrderHeader = new PurchaseOrderHeader();
	instance.items = new ArrayList<>();

	instance.purchaseOrderHeader.repositoryId = 3L;
	instance.purchaseOrderHeader.number = 123L;
	instance.purchaseOrderHeader.version = 2L;
	instance.purchaseOrderHeader.type = PurchaseOrderType.CONTRACT;
	instance.purchaseOrderHeader.supplierCode = "SP23552";
	instance.purchaseOrderHeader.description = "description description description ";
	instance.purchaseOrderHeader.departmentCode = "DP66345";
	instance.purchaseOrderHeader.projectCode = "PJ3123";
	instance.purchaseOrderHeader.internalOrderCode = "IO6634";
	instance.setHeader(new JourneyInstanceHeader());
	instance.getHeader().createdOn = LocalDateTime.now();
	instance.purchaseOrderHeader.expirationDate = LocalDateTime.now();

	Item item1 = new Item();
	Item item2 = new Item();

	item1.index = 1;
	item1.code = "AC4234";
	item1.description = "Article description";
	item1.costCenterCode = "IOC8833";
	item1.amount = 2D;
	item1.unitPriceWithoutTax = 20.0;
	item1.taxCode = "TC8833";
	item1.startDate = LocalDateTime.of(2020, 2, 15, 20, 30, 30);
	item1.endDate = LocalDateTime.of(2020, 2, 16, 20, 30, 30);
	item1.comment = "Comment comment comment comment comment";

	item2.index = 2;
	item2.code = "AC4238";
	item2.description = "Article2 description2";
	item2.costCenterCode = "2IOC8833";
	item2.amount = 2D;
	item2.unitPriceWithoutTax = 10.0;
	item2.taxCode = "2TC8833";
	item2.startDate = LocalDateTime.of(2020, 2, 16, 20, 30, 30);
	item2.endDate = LocalDateTime.of(2020, 2, 17, 20, 30, 30);
	item2.comment = "Comment2 comment2 comment2 comment2 comment2";

	instance.items.add(item1);
	instance.items.add(item2);

	PurchaseOrderDTO processDTO = PurchaseOrderInstanceMapper.INSTANCE.toProcessDTO(instance);

	// assert Header
	assertEquals(instance.purchaseOrderHeader.number, processDTO.purchaseOrderHeader.number);
	assertEquals(instance.purchaseOrderHeader.version, processDTO.purchaseOrderHeader.version);
	assertEquals(instance.purchaseOrderHeader.type, processDTO.purchaseOrderHeader.type);
	assertEquals(instance.purchaseOrderHeader.supplierCode, processDTO.purchaseOrderHeader.supplierCode);
	assertEquals(instance.purchaseOrderHeader.description, processDTO.purchaseOrderHeader.description);
	assertEquals(instance.purchaseOrderHeader.departmentCode, processDTO.purchaseOrderHeader.departmentCode);
	assertEquals(instance.purchaseOrderHeader.projectCode, processDTO.purchaseOrderHeader.projectCode);
	assertEquals(instance.purchaseOrderHeader.internalOrderCode, processDTO.purchaseOrderHeader.internalOrderCode);

	// assert item 1
	assertEquals(instance.items.get(0).index, processDTO.items.get(0).index);
	assertEquals(instance.items.get(0).code, processDTO.items.get(0).code);
	assertEquals(instance.items.get(0).description, processDTO.items.get(0).description);
	assertEquals(instance.items.get(0).costCenterCode, processDTO.items.get(0).costCenterCode);
	assertEquals(instance.items.get(0).amount, processDTO.items.get(0).amount);
	assertEquals(instance.items.get(0).unitPriceWithoutTax, processDTO.items.get(0).unitPriceWithoutTax);
	assertEquals(instance.items.get(0).taxCode, processDTO.items.get(0).taxCode);
	assertEquals(instance.items.get(0).startDate, processDTO.items.get(0).startDate);
	assertEquals(instance.items.get(0).endDate, processDTO.items.get(0).endDate);
	assertEquals(instance.items.get(0).comment, processDTO.items.get(0).comment);

	// assert item 2
	assertEquals(instance.items.get(1).index, processDTO.items.get(1).index);
	assertEquals(instance.items.get(1).code, processDTO.items.get(1).code);
	assertEquals(instance.items.get(1).description, processDTO.items.get(1).description);
	assertEquals(instance.items.get(1).costCenterCode, processDTO.items.get(1).costCenterCode);
	assertEquals(instance.items.get(1).amount, processDTO.items.get(1).amount);
	assertEquals(instance.items.get(1).unitPriceWithoutTax, processDTO.items.get(1).unitPriceWithoutTax);
	assertEquals(instance.items.get(1).taxCode, processDTO.items.get(1).taxCode);
	assertEquals(instance.items.get(1).startDate, processDTO.items.get(1).startDate);
	assertEquals(instance.items.get(1).endDate, processDTO.items.get(1).endDate);
	assertEquals(instance.items.get(1).comment, processDTO.items.get(1).comment);
    }

}
