package com.novabase.omni.oney.apps.purchaseorder;

import java.lang.reflect.Method;

import org.aspectj.lang.reflect.MethodSignature;

import com.novabase.omni.oney.apps.purchaseorder.annotations.CurrentGroupAction;
import com.novabase.omni.oney.apps.purchaseorder.controller.ri.AbstractAppController;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance;

import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyMethod;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.session.JourneySession;

public aspect ValidateUserAgainstCurrentActionUserGroupAspect {

    private pointcut currentGroupMethod(final AbstractAppController controller) : execution(@CurrentGroupAction * *(..)) && this(controller);

    Object around(AbstractAppController controller) : currentGroupMethod(controller) {

	MethodSignature ms = (MethodSignature) thisJoinPoint.getSignature();
	Method m = ms.getMethod();

	JourneyMethod journeyMethod = m.getAnnotation(JourneyMethod.class);

	PurchaseOrderInstance instance = (PurchaseOrderInstance) JourneySession.getInstance(controller);

	if (controller.isCurrentAllowedToModifyInstace(instance) == false) {
	    throw controller.currentGroupActionUserCannotExecuteActionException(instance.getHeader().instanceId, journeyMethod.value(), instance.currentAction.userGroup);
	}
	return proceed(controller);
    }
}
