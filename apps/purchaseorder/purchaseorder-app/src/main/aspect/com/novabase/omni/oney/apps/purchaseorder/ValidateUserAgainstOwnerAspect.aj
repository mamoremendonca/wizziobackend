package com.novabase.omni.oney.apps.purchaseorder;

import java.lang.reflect.Method;

import org.aspectj.lang.reflect.MethodSignature;

import com.novabase.omni.oney.apps.purchaseorder.annotations.OwnerAction;
import com.novabase.omni.oney.apps.purchaseorder.controller.ri.AbstractAppController;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance;

import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyMethod;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.session.JourneySession;

public aspect ValidateUserAgainstOwnerAspect {

    private pointcut ownerMethod(final AbstractAppController controller) : execution(@OwnerAction * *(..)) && this(controller);

    Object around(AbstractAppController controller) : ownerMethod(controller) {
	
	MethodSignature ms = (MethodSignature) thisJoinPoint.getSignature();
	Method m = ms.getMethod();
	
	JourneyMethod journeyMethod = m.getAnnotation(JourneyMethod.class);

	PurchaseOrderInstance instance = (PurchaseOrderInstance) JourneySession.getInstance(controller);
	
	if (controller.isCurrentUserTheInstanceOwner(instance) == false) {
	    throw controller.ownerActionUserCannotExecuteActionException(instance.getHeader().instanceId, journeyMethod.value());
	}
	return proceed(controller);
    }

}