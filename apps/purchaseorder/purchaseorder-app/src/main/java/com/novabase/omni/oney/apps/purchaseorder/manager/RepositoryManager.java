package com.novabase.omni.oney.apps.purchaseorder.manager;

import java.util.Arrays;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.purchaseorder.AppContext;
import com.novabase.omni.oney.apps.purchaseorder.agent.core.CoreAgentImpl;
import com.novabase.omni.oney.apps.purchaseorder.agent.purchaseorderrepository.PurchaseOrderRepositoryAgentImpl;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderHeaderDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderRepositoryDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.SearchCriteriaRepositoryDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.TimeLineStepDTO;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.CurrentAction;
import com.novabase.omni.oney.apps.purchaseorder.enums.TimeLineStepType;
import com.novabase.omni.oney.apps.purchaseorder.mapper.PurchaseOrderInstanceMapper;
import com.novabase.omni.oney.apps.purchaseorder.mapper.PurchaseOrderRepositoryMapper;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.repository.FilterRepositoryAttribute;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.OrderType;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.OrderByMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.SearchCriteriaMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.OrderTypeMs;

import io.digitaljourney.platform.modules.security.api.principal.PrincipalDTO;

@Component(service = { RepositoryManager.class })
public class RepositoryManager {

    @Reference
    private AppContext ctx;

    @Reference
    private CoreAgentImpl coreAgent;

    @Reference
    private CoreManager coreManager;

    @Reference
    private PurchaseOrderRepositoryAgentImpl repositoryAgent;

    public List<PurchaseOrderHeaderDTO> searchPurchaseOrders(SearchCriteriaRepositoryDTO searchCriteira, FilterRepositoryAttribute orderFilter, OrderType orderType, PrincipalDTO principal) {

	SearchCriteriaMsDTO searchCriteriaMs = PurchaseOrderRepositoryMapper.INSTANCE.toSearchCriteriaMs(searchCriteira);
	String currentUserGroup = coreManager.getUserGroup(principal.name);

	searchCriteriaMs.scope.currentUserId = principal.id;
	searchCriteriaMs.scope.currentUserGroup = (currentUserGroup != null ? currentUserGroup : "");
	searchCriteriaMs.orderBy = new OrderByMsDTO();
	searchCriteriaMs.orderBy.field = orderFilter.getFilterAttribute();
	searchCriteriaMs.orderBy.type = OrderTypeMs.valueOf(orderType.getCode());

	return repositoryAgent.search(searchCriteriaMs);
    }

    //TODO: when implement the Purchase Order versions we must pass here the version in order to on repository return the correct version.
    public PurchaseOrderRepositoryDTO getPurchaseOrder(Long purchaseOrderId, String userGroup) {

	PurchaseOrderRepositoryDTO purchaseOrder = repositoryAgent.getPurchaseOrder(purchaseOrderId, userGroup);

	if (purchaseOrder == null) {
	    throw ctx.purchaseOrderNotFoundException(purchaseOrderId);
	}

	if (purchaseOrder.canClaim) {
	    purchaseOrder.currentInstanceId = coreAgent.findInstanceIdByRepositoryId(purchaseOrderId);
	}

	// Double check
	if (purchaseOrder.currentInstanceId == null) {
	    purchaseOrder.canClaim = false;
	}

	return purchaseOrder;
    }

    public List<TimeLineStepDTO> getTimeLine(Long repositoryId, CurrentAction currentAction, Long userId) {

	List<TimeLineStepDTO> timeLine;

	if (repositoryId != null) {
	    timeLine = repositoryAgent.getTimeLine(repositoryId);
	} else {
	    TimeLineStepDTO draftStep = new TimeLineStepDTO();
	    draftStep.userId = userId;
	    draftStep.type = TimeLineStepType.DRAFT;
	    timeLine = Arrays.asList(draftStep);
	}

	populateTimeLineCurrentAction(currentAction, timeLine);

	//@formatter:off
	// Verify if the current user is the owner of the attachments and 
	// if this is the current step in order to evaluate if the attachment can be deleted
	timeLine.stream()
		.filter(step -> 
			(step.type.equals(TimeLineStepType.TO_APPROVE)
			|| step.type.equals(TimeLineStepType.DRAFT))
			&& step.attachments != null 
			&& step.attachments.isEmpty() == false
			)
		.flatMap(step -> step.attachments.stream())
		.forEach(attachment -> attachment.canBeDeletedByCurrentUser = (attachment.ownerId.equals(userId)));
	
	//populate the step user informations
	timeLine.stream()
		.filter(step -> step.userId != null)
		.forEach(step -> step.setuserInformation(coreAgent.getUserInformations(step.userId)));
	
	//populate the attachment user informations
	timeLine.stream()
		.filter(step -> 
			step.attachments != null 
			&& step.attachments.isEmpty() == false
			)
        	.flatMap(step -> step.attachments.stream())
        	.forEach(attachment -> attachment.setuserInformation(coreAgent.getUserInformations(attachment.ownerId)));

	//@formatter:on

	return timeLine;
    }

    private void populateTimeLineCurrentAction(CurrentAction currentAction, List<TimeLineStepDTO> timeLine) {
	//@formatter:off
	// find the current action on timeline, is the first step without userId or 
	// the step with type DRAFT in case of P.O that hasn't submitted to approval process
	TimeLineStepDTO currentActionTimeline = 
		timeLine.stream()
			.filter(step -> step.userId == null || TimeLineStepType.DRAFT.equals(step.type))
			.findFirst()
			.orElse(null);

	// if the current action timeline is not null, we must set the attachments with
	// the attachments that are on instance currentAction
	if (currentActionTimeline != null && currentAction != null) {
	    currentActionTimeline.isCurrentStep = true;
	    currentActionTimeline.attachments = PurchaseOrderInstanceMapper.INSTANCE.toAttachmentDTOList(currentAction.attachments);
	}
	//@formatter:on
    }
}
