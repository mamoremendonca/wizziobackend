package com.novabase.omni.oney.apps.purchaseorder.dto;

import java.time.LocalDateTime;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.purchaseorder.enums.DocumentClassType;
import com.novabase.omni.oney.apps.purchaseorder.json.serializer.NBLocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class AttachmentDTO extends DTO {

    @ApiModelProperty(readOnly = true)
    public int index;
    @ApiModelProperty(readOnly = true)
    public String name;
    @ApiModelProperty(readOnly = true)
    public String ownerName;
    @ApiModelProperty(readOnly = true)
    public DocumentClassType documentClass;
    @ApiModelProperty(readOnly = true)
    public boolean canBeDeletedByCurrentUser;
    @ApiModelProperty(readOnly = true)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    public LocalDateTime date;

    /**
     * Used to find the ownerName, this attribute is not visible on the response
     */
    @ApiModelProperty(hidden = true)
    public transient Long ownerId;

    public void setuserInformation(UserInformationDTO userInformationDTO) {
	this.ownerName = userInformationDTO.getCompleteName();
    }
}
