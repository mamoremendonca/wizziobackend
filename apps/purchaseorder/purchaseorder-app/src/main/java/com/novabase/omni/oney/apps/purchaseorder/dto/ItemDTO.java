package com.novabase.omni.oney.apps.purchaseorder.dto;


import java.time.LocalDateTime;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.purchaseorder.json.serializer.NBLocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ItemDTO extends DTO {

    @ApiModelProperty(readOnly = true)
    public int index;

    @ApiModelProperty
    public String code;

    @ApiModelProperty
    public String description;

    @ApiModelProperty
    public String costCenterCode;

    @ApiModelProperty
    public Double amount;

    @ApiModelProperty
    public Double unitPriceWithoutTax;

    @ApiModelProperty
    public String taxCode;

    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    public LocalDateTime startDate;

    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    public LocalDateTime endDate;

    @ApiModelProperty
    public String comment;
}
