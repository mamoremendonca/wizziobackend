package com.novabase.omni.oney.apps.purchaseorder.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.apps.purchaseorder.dto.WorkflowDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.WorkflowStepDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.WorkflowMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.WorkflowStepMsDTO;

@Mapper
public interface WorkflowMapper {

    public static final WorkflowMapper INSTANCE = Mappers.getMapper(WorkflowMapper.class);

    WorkflowDTO toWorkflowDTO(WorkflowMsDTO msDTO);
    
    WorkflowStepDTO toWorkflowStepDTO(WorkflowStepMsDTO msDTO);

    List<WorkflowStepDTO> toWorkflowStepListDTO(List<WorkflowStepMsDTO> msDTO);
}