package com.novabase.omni.oney.apps.purchaseorder.dto;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import io.digitaljourney.platform.plugins.modules.uam.service.api.type.UserStatusType;

public class UserInformationDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -577387182307459754L;
    
    public Long id;
    public UserStatusType status;
    public String firstName;
    public String lastName;
    public String email;
    public String userName;
    public String domain;

    public String getCompleteName() {
	String completeName = "";

	if (StringUtils.isNotBlank(firstName)) {
	    completeName += firstName;
	}

	if (StringUtils.isNotBlank(lastName)) {
	    completeName += (" " + lastName);
	}
	
	//@formatter:off
	return StringUtils.isNotBlank(completeName) 
		? completeName 
		: userName;
	//@formatter:on
    }

}
