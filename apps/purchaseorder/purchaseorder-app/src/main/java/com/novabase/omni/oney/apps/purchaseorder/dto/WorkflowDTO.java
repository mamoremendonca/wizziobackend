package com.novabase.omni.oney.apps.purchaseorder.dto;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.purchaseorder.enums.PurchaseOrderStatus;
import com.novabase.omni.oney.apps.purchaseorder.json.serializer.NBLocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class WorkflowDTO extends DefaultDTO {

    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    public LocalDateTime creationDate;
    @ApiModelProperty
    public int currentIndex;
    @ApiModelProperty
    public String currentUserGroup;
    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    public LocalDateTime limitDateToNextStep;
    @ApiModelProperty
    public boolean hasNextStep;
    
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    public PurchaseOrderStatus purchaseOrderStatus;

    @ApiModelProperty
    public List<WorkflowStepDTO> steps;

    //@formatter:off
    public WorkflowStepDTO getStepByIndex(int index) {
	return steps != null 
		? steps.stream()
			.filter(step -> step.index == index)
			.findFirst()
			.orElse(null) 
		: null;
    }
    //@formatter:on
}
