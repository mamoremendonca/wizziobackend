/*-
 * #%L
 * Archetype :: APP Vault
 * %%
 * Copyright (C) 2016 - 2018 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.purchaseorder.controller.ri;

import org.eclipse.gemini.blueprint.extensions.annotation.ServiceReference;

import com.novabase.omni.oney.apps.purchaseorder.AppContext;
import com.novabase.omni.oney.apps.purchaseorder.AppProperties;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderUserCannotExecuteActionException;
import com.novabase.omni.oney.apps.purchaseorder.manager.CoreManager;

import io.digitaljourney.platform.modules.security.api.principal.PrincipalDTO;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.api.JourneyWorkflowEngineResource;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.JourneyProcess;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.controller.AbstractJourneyController;

/**
 * Abstract application controller which extends an
 * {@link AbstractJourneyController Abstract Journey Controller}.
 */
public abstract class AbstractAppController extends AbstractJourneyController<AppContext> implements JourneyProcess<PurchaseOrderInstance> {

    @ServiceReference
    private AppContext ctx;

    @ServiceReference
    private CoreManager coreManager;

    /**
     * Gets the application context.
     *
     * @return Context
     */
    @Override
    protected AppContext getCtx() {
	return this.ctx;
    }

    @Override
    public String journeyName() {
	return AppProperties.JOURNEY_NAME;
    }

    @Override
    public int majorVersion() {
	return AppProperties.JOURNEY_VERSION;
    }

    @Override
    public Class<PurchaseOrderInstance> getInstanceClass() {
	return PurchaseOrderInstance.class;
    }

    @Override
    public JourneyWorkflowEngineResource getJourneyEngine() {
	return journeyEngine();
    }

    protected Long currentLoggedUserId() {
	PrincipalDTO principal = currentPrincipal();
	return new Long(principal.id);
    }

    protected String currentLoggedUsername() {
	PrincipalDTO principal = currentPrincipal();
	return principal.name;
    }

    protected String currentLoggedUserGroup() {
	return coreManager.getUserGroup(currentLoggedUsername());
    }

    public boolean isCurrentUserTheInstanceOwner(PurchaseOrderInstance instance) {
	return instance.getHeader().createdBy.equals(currentUser()); // TODO: create a configuration in order to bypass
								     // @OwnerAction
	// and @CurrentGroupAction aspects
    }

    public boolean isCurrentAllowedToModifyInstace(PurchaseOrderInstance instance) {
	return true; // TODO: uncomment and create a configuration in order to bypass @OwnerAction
	// and @CurrentGroupAction aspects
//	 return instance.currentAction.userGroup.equals(currentLoggedUserGroup());
    }

    public PurchaseOrderUserCannotExecuteActionException ownerActionUserCannotExecuteActionException(Long instanceId, String action) {
	return ctx.userCannotExecuteActionException(instanceId, currentUser(), action);
    }

    public PurchaseOrderUserCannotExecuteActionException currentGroupActionUserCannotExecuteActionException(Long instanceId, String action, String instanceCurrentUserGroup) {
	return ctx.userCannotExecuteActionException(instanceId, currentUser(), action, instanceCurrentUserGroup, currentLoggedUserGroup());
    }

}
