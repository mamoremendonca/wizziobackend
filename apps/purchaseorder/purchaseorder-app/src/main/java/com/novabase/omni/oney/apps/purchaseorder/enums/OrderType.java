package com.novabase.omni.oney.apps.purchaseorder.enums;

public enum OrderType {
    ASC, DESC;
    
    @SuppressWarnings("unchecked")
    public static <T extends Enum<OrderType>> T getEnum(String name){

	Enum<OrderType> value = OrderType.ASC;
	try {
	    value = OrderType.valueOf(name);
	} catch (Throwable t) {
	    // DO nothing
	}
	return (T) value;
    }
}
