package com.novabase.omni.oney.apps.purchaseorder.dto;

import java.io.Serializable;
import java.util.List;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class PurchaseOrderRepositoryDTO extends DTO implements Serializable {

    private static final long serialVersionUID = 2710847434963660265L;

    @ApiModelProperty
    public PurchaseOrderHeaderDTO purchaseOrderHeader;
    
    @ApiModelProperty
    public boolean canClaim;
    
    @ApiModelProperty
    public Long currentInstanceId;

    @ApiModelProperty
    public List<ItemDTO> items;
    
    @JsonIgnore
    public WorkflowDTO workflow;
}
