package com.novabase.omni.oney.apps.purchaseorder.exception;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderFailedToUploadBlobException extends PurchaseOrderException {

    private static final long serialVersionUID = -9106812614274743213L;

    public PurchaseOrderFailedToUploadBlobException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.INTERNAL_SERVER_ERROR, errorCode, ctx, args);
    }

    public static PurchaseOrderFailedToUploadBlobException of(Context ctx,Long instanceId, String message) {
	return new PurchaseOrderFailedToUploadBlobException(AppProperties.FAILED_TO_UPLOAD_BLOB_EXCEPTION, ctx, instanceId, message);
    }

}
