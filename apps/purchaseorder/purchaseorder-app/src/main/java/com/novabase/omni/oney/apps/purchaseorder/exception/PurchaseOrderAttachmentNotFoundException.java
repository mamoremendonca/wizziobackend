package com.novabase.omni.oney.apps.purchaseorder.exception;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderAttachmentNotFoundException extends PurchaseOrderException {

    private static final long serialVersionUID = -4455977178519122903L;

    public PurchaseOrderAttachmentNotFoundException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.NOT_FOUND, errorCode, ctx, args);
    }

    public static PurchaseOrderAttachmentNotFoundException of(Context ctx, int attachmentIndex, Long instanceId) {
	return new PurchaseOrderAttachmentNotFoundException(AppProperties.ATTACHMENT_NOT_FOUND_EXCEPTION, ctx, attachmentIndex, instanceId);
    }

}
