package com.novabase.omni.oney.apps.purchaseorder.exception;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderCouldNotClaimException extends PurchaseOrderException {

    private static final long serialVersionUID = 2496875620154392002L;

    public PurchaseOrderCouldNotClaimException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.AUTHORIZATION_ERROR, errorCode, ctx, args);
    }

    public static PurchaseOrderCouldNotClaimException of(Context ctx, Long instanceId, String userGroup) {
	return new PurchaseOrderCouldNotClaimException(AppProperties.COULD_NOT_CLAIM_EXCEPTION, ctx, instanceId, userGroup);
    }

}
