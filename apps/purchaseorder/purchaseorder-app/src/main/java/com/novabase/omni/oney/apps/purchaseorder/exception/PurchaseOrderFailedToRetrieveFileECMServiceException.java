package com.novabase.omni.oney.apps.purchaseorder.exception;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderFailedToRetrieveFileECMServiceException extends PurchaseOrderException {

    private static final long serialVersionUID = -2442647502436613709L;

    public PurchaseOrderFailedToRetrieveFileECMServiceException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.INTERNAL_SERVER_ERROR, errorCode, ctx, args);
    }

    public static PurchaseOrderFailedToRetrieveFileECMServiceException of(Context ctx, int attachmentIndex, Long purchaseOrderId, String message) {
	return new PurchaseOrderFailedToRetrieveFileECMServiceException(AppProperties.FAILED_TO_RETRIEVE_FILE_ECM_EXCEPTION, ctx, attachmentIndex, purchaseOrderId, message);
    }

}
