package com.novabase.omni.oney.apps.purchaseorder.manager;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.purchaseorder.AppContext;
import com.novabase.omni.oney.apps.purchaseorder.agent.attachment.AttachmentAgentImpl;
import com.novabase.omni.oney.apps.purchaseorder.agent.core.CoreAgentImpl;
import com.novabase.omni.oney.apps.purchaseorder.agent.ecmservice.ECMServiceAgentImpl;
import com.novabase.omni.oney.apps.purchaseorder.agent.purchaseorderrepository.PurchaseOrderRepositoryAgentImpl;
import com.novabase.omni.oney.apps.purchaseorder.dto.AttachmentDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.AttachmentRepositoryDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.FileDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.UserInformationDTO;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.Attachment;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.CurrentAction;
import com.novabase.omni.oney.apps.purchaseorder.enums.DocumentClassType;
import com.novabase.omni.oney.apps.purchaseorder.mapper.PurchaseOrderInstanceMapper;

import io.digitaljourney.platform.plugins.modules.blob.service.api.exception.BlobNotFoundException;

@Component(service = { AttachmentManager.class })
public class AttachmentManager {

    @Reference
    private AppContext ctx;

    @Reference
    private AttachmentAgentImpl attachmentAgentImpl;

    @Reference
    private CoreAgentImpl coreAgentImpl;

    @Reference
    private PurchaseOrderRepositoryAgentImpl repositoryAgent;
    
    @Reference
    private ECMServiceAgentImpl ecmServiceAgent;
    
    public AttachmentDTO saveAttachment(PurchaseOrderInstance instance, InputStream attachmentStream, String attachmentName, String mimeType, DocumentClassType type, long size, Long userId) {

	if (instance.currentAction == null) {
	    instance.currentAction = new CurrentAction();
	}

	Attachment attachment = new Attachment();
	attachment.mimeType = mimeType;
	attachment.ownerId = userId;
	attachment.name = attachmentName;
	attachment.date = LocalDateTime.now();
	attachment.documentClass = type;

	try {
	    attachment.blobId = attachmentAgentImpl.saveBlob(attachmentStream, attachmentName, new Float(size));
	} catch (Exception e) {
	    throw ctx.failedToUploadBlobException(instance.getHeader().instanceId, e.getMessage());
	}

	if (instance.currentAction.attachments == null) {
	    instance.currentAction.attachments = new ArrayList<>();
	}

	attachment.index = getNextAttachmentIndex(instance);

	instance.currentAction.attachments.add(attachment);

	AttachmentDTO attachmentDTO = PurchaseOrderInstanceMapper.INSTANCE.toAttachmentDTO(attachment);

	UserInformationDTO userInformation = coreAgentImpl.getUserInformations(attachment.ownerId);

	attachmentDTO.ownerName = userInformation.getCompleteName();
	attachmentDTO.canBeDeletedByCurrentUser = true;

	return attachmentDTO;
    }

    public FileDTO getAttachment(PurchaseOrderInstance instance, int attachmentIndex) {
	Attachment attachment = getAttachmentByIndex(instance, attachmentIndex);
	InputStream blobInputStream = null;
	try {
	    blobInputStream = attachmentAgentImpl.readFileFromBlob(attachment.blobId);
	} catch (Exception e) {
	    throw ctx.failedToRetrieveBlobException(attachmentIndex, instance.getHeader().instanceId, e.getMessage());
	}

	FileDTO fileDTO = new FileDTO();
	fileDTO.inputStream = blobInputStream;
	fileDTO.mimeType = attachment.mimeType;
	fileDTO.fileName = attachment.name;

	return fileDTO;
    }

    public FileDTO getAttachmentByPurchaseOrderId(Long purchaseOrderId, int attachmentIndex) {
	
	final AttachmentRepositoryDTO attachment = this.repositoryAgent.getPurchaseOrderAttachment(purchaseOrderId, attachmentIndex);

	InputStream fileInputStream = null;
	if (StringUtils.isBlank(attachment.getExternalId())) {
	
	    try {
		fileInputStream = this.attachmentAgentImpl.readFileFromBlob(attachment.getBlobId());
	    } catch (Exception e) {
		throw ctx.failedToRetrieveBlobException(attachmentIndex, purchaseOrderId, e.getMessage());
	    }
	    
	} else {
	    
	    try {
		fileInputStream = this.ecmServiceAgent.getDocument(attachment.getExternalId());
	    } catch (Exception e) {
		throw ctx.failedToRetrieveFileECMException(attachmentIndex, purchaseOrderId, e.getMessage());
	    }
	    
	}
	
	FileDTO fileDTO = new FileDTO();
	fileDTO.inputStream = fileInputStream;
	fileDTO.mimeType = attachment.getMimeType();
	fileDTO.fileName = attachment.getName();

	return fileDTO;
	
    }

    public void removeAttachment(PurchaseOrderInstance instance, int attachmentIndex, Long loggedUserId) {
	Attachment attachment = getAttachmentByIndex(instance, attachmentIndex);

	if (attachment.ownerId.equals(loggedUserId) == false) {

	    UserInformationDTO currentUserInformation = coreAgentImpl.getUserInformations(loggedUserId);

	    throw ctx.user_couldNotRemoveAttachmentException(currentUserInformation.userName, attachmentIndex, instance.getHeader().instanceId);
	}

	if (instance.currentAction.attachments.contains(attachment)) {
	    instance.currentAction.attachments.remove(attachment);
	} else {

	    UserInformationDTO currentUserInformation = coreAgentImpl.getUserInformations(loggedUserId);
	    // Is not possible remove attachments that are already submitted to the
	    // repository, if this became possible, we have to update this information on
	    // repository to
	    throw ctx.step_couldNotRemoveAttachmentException(currentUserInformation.userName, attachmentIndex, instance.getHeader().instanceId);
	}

	try {
	    attachmentAgentImpl.removeFileFromBlob(attachment.blobId);
	} catch (BlobNotFoundException e) {
	    ctx.logError(e.getMessage());
	} catch (Exception e) {
	    throw ctx.failedToRemoveBlobException(attachmentIndex, instance.getHeader().instanceId, e.getMessage());
	}

    }

    /**
     * Finds and return the attachment on instance
     * 
     * @param instance
     * @param attachmentIndex
     * @return
     */
    private Attachment getAttachmentByIndex(PurchaseOrderInstance instance, int attachmentIndex) {

	// verify if the attachment is on currentAction
	if (instance.currentAction != null && instance.currentAction.attachments != null) {
	    for (Attachment attachment : instance.currentAction.attachments) {
		if (attachment.index == attachmentIndex) {
		    return attachment;
		}
	    }
	}

	// if the attachment is not on current action, we verify on the attachment list
	// that already were submitted to the repository
	if (instance.attachments != null) {
	    for (Attachment attachment : instance.attachments) {
		if (attachment.index == attachmentIndex) {
		    return attachment;
		}
	    }
	}

	// throw exception if the attachment does not exists on the current instance
	throw ctx.attachmentNotFoundException(attachmentIndex, instance.getHeader().instanceId);
    }

    private int getNextAttachmentIndex(PurchaseOrderInstance instance) {

	int nextIndex = 1;

	// verify the max value of attachmentIndex on the currentAction
	if (instance.currentAction != null && instance.currentAction.attachments != null && instance.currentAction.attachments.isEmpty() == false) {
	    Attachment attachment = instance.currentAction.attachments.stream().sorted((a1, a2) -> a2.index - a1.index).findFirst().get();
	    if (attachment.index >= nextIndex) {
		nextIndex = attachment.index + 1;
	    }
	}

	// verify the max value of attachmentIndex on the attachment list that already were
	// submitted to the repository
	if (instance.attachments != null && instance.attachments.isEmpty() == false) {
	    Attachment attachment = instance.attachments.stream().sorted((a1, a2) -> a2.index - a1.index).findFirst().get();
	    if (attachment.index >= nextIndex) {
		nextIndex = attachment.index + 1;
	    }
	}

	return nextIndex;
    }

}
