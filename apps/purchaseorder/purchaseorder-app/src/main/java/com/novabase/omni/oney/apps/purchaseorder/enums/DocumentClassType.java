package com.novabase.omni.oney.apps.purchaseorder.enums;

import java.util.Arrays;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.DocumentClassTypeMs;

public enum DocumentClassType {

    //@formatter:off
    PURCHASE_ORDER(DocumentClassTypeMs.PURCHASE_ORDER), 
    FINANCIAL_DOCUMENT(DocumentClassTypeMs.FINANCIAL_DOCUMENT); 
//    GENERIC(DocumentClassTypeMs.GENERIC);
    //@formatter:on

    private DocumentClassTypeMs msType;

    private DocumentClassType(DocumentClassTypeMs msType) {
	this.msType = msType;
    }

    public DocumentClassTypeMs getMsType() {
	return msType;
    }

    //@formatter:off
    public static DocumentClassType valueOf(DocumentClassTypeMs msType){
	return Arrays.asList(DocumentClassType.values())
		.stream()
		.filter(type -> type.getMsType().equals(msType))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on
}
