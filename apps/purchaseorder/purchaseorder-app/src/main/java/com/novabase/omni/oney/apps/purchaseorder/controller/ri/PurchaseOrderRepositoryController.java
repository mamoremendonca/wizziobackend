package com.novabase.omni.oney.apps.purchaseorder.controller.ri;
/*-
 * #%L
 * Apps :: Purchase Orders App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.eclipse.gemini.blueprint.extensions.annotation.ServiceReference;
import org.osgi.service.component.annotations.Component;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;
import com.novabase.omni.oney.apps.purchaseorder.controller.api.PurchaseOrderRepositoryResource;
import com.novabase.omni.oney.apps.purchaseorder.dto.FileDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderHeaderDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderRepositoryDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.SearchCriteriaRepositoryDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.TimeLineStepDTO;
import com.novabase.omni.oney.apps.purchaseorder.manager.AttachmentManager;
import com.novabase.omni.oney.apps.purchaseorder.manager.CoreManager;
import com.novabase.omni.oney.apps.purchaseorder.manager.RepositoryManager;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.repository.FilterRepositoryAttribute;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.OrderType;

import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;

@Component
@RSProvider(value = AppProperties.ADDRESS_PURCHASE_ORDER_REPOSITORY_CONTROLLER)
@RestController
@RequestMapping(AppProperties.ADDRESS_PURCHASE_ORDER_REPOSITORY_CONTROLLER)
public class PurchaseOrderRepositoryController extends AbstractAppController implements PurchaseOrderRepositoryResource {

    @ServiceReference
    private AttachmentManager attachmentManager;

    @ServiceReference
    private CoreManager coreManager;

    @ServiceReference
    private RepositoryManager repositoryManager;

    /**
     * Retrieves all Purchase Orders from Repository, based on specific search
     * criterias
     */
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public List<PurchaseOrderHeaderDTO> list(
    //@formatter:off
	    @RequestBody SearchCriteriaRepositoryDTO searchCriteriaDTO, 
	    @RequestParam(required = false) FilterRepositoryAttribute orderField, 
	    @RequestParam(required = false) OrderType orderType) {
	//@formatter:on

	return repositoryManager.searchPurchaseOrders(searchCriteriaDTO, orderField, orderType, currentPrincipal());
    }

    /**
     * Retrieves a specific Purchase Order from Repository
     * 
     * @return PurchaseOrderRepositoryDTO
     */
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    @RequestMapping(value = "/{purchaseOrderId}", method = RequestMethod.GET)
    public PurchaseOrderRepositoryDTO get(@PathVariable long purchaseOrderId) {

	return repositoryManager.getPurchaseOrder(purchaseOrderId, currentLoggedUserGroup());
    }

    @Override
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    @RequestMapping(value = "/{repositoryId}/timeline", method = RequestMethod.GET)
    public List<TimeLineStepDTO> getTimeLine(@PathVariable long repositoryId) {

	return repositoryManager.getTimeLine(repositoryId, null, currentLoggedUserId());
    }

    @RequestMapping(path = "/{repositoryId}/attachment/{attachmentIndex}", method = RequestMethod.GET
    //,produces = MediaType.APPLICATION_OCTET_STREAM TODO: uncomment this, we have a issue with cors when the produces is octet stream. Validate with Wizzio product team how to solve it.
    )
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    public byte[] getAttachment(@PathVariable long repositoryId, @PathVariable int attachmentIndex, HttpServletResponse response) {

	// PurchaseOrderInstance instance = JourneySession.getInstance(this);
	// retrieve instanceId from repository
	FileDTO fileDTO = attachmentManager.getAttachmentByPurchaseOrderId(repositoryId, attachmentIndex);

	response.addHeader("Content-Disposition", "attachment; filename=" + fileDTO.fileName);
	response.setContentType(fileDTO.mimeType);

	byte[] byteArray = null;
	try {
	    byteArray = StreamUtils.copyToByteArray(fileDTO.inputStream);
	} catch (IOException e) {
	    throw getCtx().attachmentFailedReadStreamException(repositoryId);
	}

	return byteArray;
    }
}
