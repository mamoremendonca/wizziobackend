/*-
 * #%L
 * Apps :: Purchase Order App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.purchaseorder.controller.ri;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.eclipse.gemini.blueprint.extensions.annotation.ServiceReference;
import org.osgi.service.component.annotations.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;
import com.novabase.omni.oney.apps.purchaseorder.annotations.CurrentGroupAction;
import com.novabase.omni.oney.apps.purchaseorder.annotations.OwnerAction;
import com.novabase.omni.oney.apps.purchaseorder.controller.api.PurchaseOrderResource;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderHeaderDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderRepositoryDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.TimeLineStepDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.WorkflowDTO;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.CurrentAction;
import com.novabase.omni.oney.apps.purchaseorder.enums.WorkFlowActionType;
import com.novabase.omni.oney.apps.purchaseorder.manager.CoreManager;
import com.novabase.omni.oney.apps.purchaseorder.manager.RepositoryManager;
import com.novabase.omni.oney.apps.purchaseorder.manager.WorkflowManager;
import com.novabase.omni.oney.apps.purchaseorder.mapper.PurchaseOrderInstanceMapper;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.TargetType;

import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyMethod;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyReference;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.api.trigger.ActionMode;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.session.JourneySession;
import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;

@Component
@RSProvider(value = AppProperties.ADDRESS_PURCHASE_ORDER_CONTROLLER)
@RestController
@RequestMapping(AppProperties.ADDRESS_PURCHASE_ORDER_CONTROLLER)
public class PurchaseOrderController extends AbstractAppController implements PurchaseOrderResource {

    // TODO criar scheduler para apagar Processos do tipo PurchaseOrder que foram
    // cancelados ou aprovados, isso por que ja temos eles registrados com todo o
    // historico no repositorio de P.Os

    @ServiceReference
    private WorkflowManager workflowManager;

    @ServiceReference
    private RepositoryManager repositoryManager;

    @ServiceReference
    private CoreManager coreManager;

    @RequiresPermissions(AppProperties.PERMISSION_EXECUTE)
    @RequestMapping(value = "/init", method = RequestMethod.GET)
    public void init() {
	coreManager.createJourney();
    }

    @JourneyMethod(value = "Create Process", mode = ActionMode.CREATE)
    @RequiresPermissions(AppProperties.PERMISSION_CREATE)
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public PurchaseOrderDTO createProcess(@RequestBody PurchaseOrderHeaderDTO purchaseOrderHeaderDTO) {

	List<String> userDepartments = workflowManager.getUserDepartmentsByUserGroup(currentLoggedUserGroup());

	purchaseOrderHeaderDTO.creationDate = LocalDateTime.now();

	//@formatter:off
	purchaseOrderHeaderDTO.departmentCode = 
		userDepartments != null && userDepartments.isEmpty() == false 
			? userDepartments.get(0) 
			: null;
	//@formatter:on

	PurchaseOrderInstance instance = JourneySession.getInstance(this);
	PurchaseOrderInstanceMapper.INSTANCE.mergePurchaseOrderHeaderDTO(instance, purchaseOrderHeaderDTO);
	instance.purchaseOrderHeader.userGroup = this.currentLoggedUserGroup();
	instance.purchaseOrderHeader.userId = currentLoggedUserId();
	instance.currentAction = new CurrentAction();
	instance.currentAction.startDate = LocalDateTime.now();
	instance.workFlowAvailableActions = Arrays.asList(WorkFlowActionType.CANCEL); // When the Purchase Order is created the only action available is cancel.

	if (instance.getHeader().targets == null) {
	    instance.getHeader().targets = new HashMap<String, String>();
	}

	instance.refreshTargets();

	return PurchaseOrderInstanceMapper.INSTANCE.toProcessDTO(instance);
    }

    @JourneyMethod(value = "Create Process", mode = ActionMode.CREATE)
    @RequiresPermissions(AppProperties.PERMISSION_CREATE)
    @RequestMapping(value = "/copy/{repositoryId}/{version}", method = RequestMethod.GET)
    public PurchaseOrderDTO copyPurchaseOrder(@PathVariable Long repositoryId, @PathVariable Long version) {

	// TODO: when implement the Purchase Order versions we must pass here the
	// version in order to on repository return the correct version.

	PurchaseOrderRepositoryDTO purchaseOrder = repositoryManager.getPurchaseOrder(repositoryId, null);

	PurchaseOrderInstance instance = JourneySession.getInstance(this);

	PurchaseOrderInstanceMapper.INSTANCE.mergePurchaseOrderHeaderDTO(instance, purchaseOrder.purchaseOrderHeader);
	instance.items = PurchaseOrderInstanceMapper.INSTANCE.toItemList(purchaseOrder.items);

	instance.purchaseOrderHeader.userGroup = currentLoggedUserGroup();
	instance.purchaseOrderHeader.userId = currentLoggedUserId();
	instance.currentAction = new CurrentAction();
	instance.currentAction.startDate = LocalDateTime.now();
	instance.workFlowAvailableActions = Arrays.asList(WorkFlowActionType.CANCEL); // When the Purchase Order is created the only action available is cancel.

	if (instance.getHeader().targets == null) {
	    instance.getHeader().targets = new HashMap<String, String>();
	}

	instance.refreshTargets();
	instance.refreshTotal();

	instance.getHeader().targets.put(TargetType.COPY_FROM.name(), TargetType.formatCopyFrom(repositoryId, version));

	return PurchaseOrderInstanceMapper.INSTANCE.toProcessDTO(instance);
    }

    @OwnerAction
    @JourneyMethod(value = "Update Process Header")
    @RequiresPermissions(AppProperties.PERMISSION_CREATE)
    @RequestMapping(value = "/{instanceId}", method = RequestMethod.PUT)
    public PurchaseOrderDTO updateProcessHeader(@PathVariable @JourneyReference long instanceId, @RequestBody PurchaseOrderHeaderDTO purchaseOrderHeaderDTO) {

	PurchaseOrderInstance instance = JourneySession.getInstance(this);

//	Temporary comment
//	if (StringUtils.stripToEmpty(purchaseOrderHeaderDTO.departmentCode).equals(StringUtils.stripToEmpty(instance.purchaseOrderHeader.departmentCode)) == false
//		|| StringUtils.stripToEmpty(purchaseOrderHeaderDTO.projectCode).equals(StringUtils.stripToEmpty(instance.purchaseOrderHeader.projectCode)) == false
//		|| StringUtils.stripToEmpty(purchaseOrderHeaderDTO.internalOrderCode).equals(StringUtils.stripToEmpty(instance.purchaseOrderHeader.internalOrderCode)) == false
//		|| (purchaseOrderHeaderDTO.type != null && instance.purchaseOrderHeader.type != null && purchaseOrderHeaderDTO.type.equals(instance.purchaseOrderHeader.type) == false)) {
//
//	    instance.items = new ArrayList<>();
//	    getCtx().logInfo("Removing all items from PurchaseOrderInstance with id " + instanceId + " because the projectCode or/and internalOrderCode or/and departmendCode or/and PurchaseOrderType was modified");
//	}

	PurchaseOrderInstanceMapper.INSTANCE.mergePurchaseOrderHeaderDTO(instance, purchaseOrderHeaderDTO);

	PurchaseOrderDTO processDTO = PurchaseOrderInstanceMapper.INSTANCE.toProcessDTO(JourneySession.getInstance(this));

	instance.refreshTargets();

	return processDTO;
    }

    @JourneyMethod(value = "Get Process")
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    @RequestMapping(value = "/{instanceId}", method = RequestMethod.GET)
    public PurchaseOrderDTO getProcess(@PathVariable @JourneyReference long instanceId) {

	PurchaseOrderDTO processDTO = PurchaseOrderInstanceMapper.INSTANCE.toProcessDTO(JourneySession.getInstance(this));

	return processDTO;
    }

    @OwnerAction
    @JourneyMethod(value = "Submit")
    @RequiresPermissions(AppProperties.PERMISSION_EXECUTE)
    @RequestMapping(value = "/{instanceId}/submit", method = RequestMethod.PUT)
    public PurchaseOrderHeaderDTO submit(@PathVariable @JourneyReference long instanceId) {

	PurchaseOrderInstance instance = JourneySession.getInstance(this);

	PurchaseOrderHeaderDTO purchaseOrderHeaderDTO = workflowManager.generateWorkflowAndSubmit(instance);

	instance.refreshTargets();

	return purchaseOrderHeaderDTO;
    }

    @CurrentGroupAction
    @JourneyMethod(value = "Approve")
    @RequiresPermissions(AppProperties.PERMISSION_EXECUTE)
    @RequestMapping(value = "/{instanceId}/approve", method = RequestMethod.PUT)
    public WorkflowDTO approvePurchaseOrder(@PathVariable @JourneyReference long instanceId, @RequestBody String comment) {

	PurchaseOrderInstance instance = JourneySession.getInstance(this);
	instance.currentAction.comment = comment;
	WorkflowDTO workflowDTO = workflowManager.approvePurchaseOrder(instance, currentLoggedUserId());

	instance.refreshTargets();

	return workflowDTO;
    }

    @CurrentGroupAction
    @JourneyMethod(value = "Return")
    @RequiresPermissions(AppProperties.PERMISSION_EXECUTE)
    @RequestMapping(value = "/{instanceId}/return", method = RequestMethod.PUT)
    public WorkflowDTO returnPurchaseOrder(@PathVariable @JourneyReference long instanceId, @RequestBody String comment) {

	// TODO: garantir que so rejeita se nao for o primeiro da cadeia de aprovação,
	// quando for feito claim tambem tem que garantir esse cenario

	PurchaseOrderInstance instance = JourneySession.getInstance(this);
	instance.currentAction.comment = comment;
	WorkflowDTO workflowDTO = workflowManager.returnPurchaseOrder(instance, currentLoggedUserId());

	instance.refreshTargets();

	return workflowDTO;
    }

    @CurrentGroupAction
    @JourneyMethod(value = "Reject")
    @RequiresPermissions(AppProperties.PERMISSION_EXECUTE)
    @RequestMapping(value = "/{instanceId}/reject", method = RequestMethod.PUT)
    public WorkflowDTO rejectPurchaseOrder(@PathVariable @JourneyReference long instanceId, @RequestBody String comment) {

	PurchaseOrderInstance instance = JourneySession.getInstance(this);
	instance.currentAction.comment = comment;
	WorkflowDTO workflowDTO = workflowManager.rejectPurchaseOrder(instance, currentLoggedUserId());

	instance.refreshTargets();

	return workflowDTO;
    }

    @OwnerAction
    @CurrentGroupAction
    @JourneyMethod(value = "Cancel API")
    @RequiresPermissions(AppProperties.PERMISSION_UPDATE)
    @RequestMapping(value = "/{instanceId}/cancel", method = RequestMethod.PUT)
    public void cancelPurchaseOrder(@PathVariable @JourneyReference long instanceId) {

	PurchaseOrderInstance instance = JourneySession.getInstance(this);

	if (instance.getHeader().status.equalsIgnoreCase("draft") == false) {
	    workflowManager.cancelPurchaseOrder(instance, currentLoggedUserId());
	}

	instance.refreshTargets();
    }

    @Override
    @JourneyMethod(value = "Timeline")
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    @RequestMapping(value = "/{instanceId}/timeline", method = RequestMethod.GET)
    public List<TimeLineStepDTO> getTimeLine(@PathVariable @JourneyReference long instanceId) {

	PurchaseOrderInstance instance = JourneySession.getInstance(this);

	//@formatter:off
	return repositoryManager.getTimeLine(
		instance.purchaseOrderHeader.repositoryId, 
		instance.currentAction, 
		currentLoggedUserId());
	//@formatter:on
    }

    @Override
    @JourneyMethod(value = "Claim")
    @RequiresPermissions(AppProperties.PERMISSION_EXECUTE)
    @RequestMapping(value = "/{instanceId}/claim", method = RequestMethod.PUT)
    public void claimProcess(@PathVariable @JourneyReference long instanceId) {

	PurchaseOrderInstance instance = JourneySession.getInstance(this);

	//@formatter:off
	workflowManager.claimProcess(
		instance, 
		currentLoggedUserId(),
		currentLoggedUserGroup());
	//@formatter:on
	
	instance.refreshTargets();
    }
}