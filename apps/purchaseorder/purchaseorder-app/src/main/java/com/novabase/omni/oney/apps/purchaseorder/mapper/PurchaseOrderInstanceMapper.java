package com.novabase.omni.oney.apps.purchaseorder.mapper;

import java.time.LocalDateTime;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.apps.purchaseorder.dto.AttachmentDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.ItemDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderHeaderDTO;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.Attachment;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.Item;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.PurchaseOrderHeader;

@Mapper
public abstract class PurchaseOrderInstanceMapper {

    public static final PurchaseOrderInstanceMapper INSTANCE = Mappers.getMapper(PurchaseOrderInstanceMapper.class);

    public PurchaseOrderDTO toProcessDTO(PurchaseOrderInstance instance) {
	if (instance == null) {
	    return null;
	}

	PurchaseOrderDTO purchaseOrderDTO = new PurchaseOrderDTO();

	purchaseOrderDTO.workFlowAvailableActions = instance.workFlowAvailableActions;
	purchaseOrderDTO.purchaseOrderHeader = toPurchaseOrderHeaderDTO(instance.purchaseOrderHeader);
	purchaseOrderDTO.items = toItemDTOList(instance.items);

	if (purchaseOrderDTO.purchaseOrderHeader == null) {
	    purchaseOrderDTO.purchaseOrderHeader = new PurchaseOrderHeaderDTO();
	}

	//@formatter:off
	purchaseOrderDTO.purchaseOrderHeader.creationDate = 
		instance.getHeader() != null && instance.getHeader().createdOn != null 
        		? instance.getHeader().createdOn
        		: LocalDateTime.now(); // When the PO is created we must set the localDateTime.now() because the instance.header does not exists yet
	//@formatter:on

	return purchaseOrderDTO;
    }

    public abstract PurchaseOrderHeaderDTO toPurchaseOrderHeaderDTO(PurchaseOrderHeader purchaseOrderHeader);

    public abstract Item toItem(ItemDTO itemDTO);
    
    public abstract List<Item> toItemList(List<ItemDTO> items);

    public abstract List<ItemDTO> toItemDTOList(List<Item> items);

    public abstract ItemDTO toItemDTO(Item item);

    public abstract AttachmentDTO toAttachmentDTO(Attachment attachment);

    public abstract List<AttachmentDTO> toAttachmentDTOList(List<Attachment> attachments);

    @Mapping(ignore = true, source = "number", target = "purchaseOrderHeader.number")
    @Mapping(ignore = true, source = "version", target = "purchaseOrderHeader.version")
    @Mapping(source = "type", target = "purchaseOrderHeader.type")
    @Mapping(source = "supplierCode", target = "purchaseOrderHeader.supplierCode")
    @Mapping(source = "description", target = "purchaseOrderHeader.description")
    @Mapping(source = "departmentCode", target = "purchaseOrderHeader.departmentCode")
    @Mapping(source = "projectCode", target = "purchaseOrderHeader.projectCode")
    @Mapping(source = "internalOrderCode", target = "purchaseOrderHeader.internalOrderCode")
    @Mapping(source = "expirationDate", target = "purchaseOrderHeader.expirationDate")
    public abstract void mergePurchaseOrderHeaderDTO(@MappingTarget PurchaseOrderInstance instance, PurchaseOrderHeaderDTO purchaseOrderHeaderDTO);

    @Mapping(ignore = true, source = "index", target = "index")
    public abstract void mergeItemDTO(@MappingTarget Item item, ItemDTO itemDTO);

}
