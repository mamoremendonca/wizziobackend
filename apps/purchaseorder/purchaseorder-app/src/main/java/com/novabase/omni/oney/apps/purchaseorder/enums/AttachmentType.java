package com.novabase.omni.oney.apps.purchaseorder.enums;

public enum AttachmentType {
    CREATION_PROCESS, APPROVEMENT_PROCESS
}
