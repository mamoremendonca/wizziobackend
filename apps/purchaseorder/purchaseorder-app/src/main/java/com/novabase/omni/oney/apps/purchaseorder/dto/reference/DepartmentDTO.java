package com.novabase.omni.oney.apps.purchaseorder.dto.reference;

import org.apache.commons.lang3.StringUtils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("DepartmentDTO")
public class DepartmentDTO extends ReferenceDataDTO {

    @ApiModelProperty(value = "CostCenter")
    public String defaultCostCenterCode;
    
    @Override
    public boolean equals(Object obj) {
        if((obj instanceof DepartmentDTO) == false) {
            return false;
        }
        
        DepartmentDTO department = (DepartmentDTO) obj;
        
        //@formatter:off
        return StringUtils.stripToEmpty(code)
        		.equals(StringUtils.stripToEmpty(department.code));
        //@formatter:on
    }
    
}
