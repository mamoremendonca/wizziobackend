/*-
 * #%L
 * Apps :: Purchase Orders App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.purchaseorder.controller.ri;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.eclipse.gemini.blueprint.extensions.annotation.ServiceReference;
import org.osgi.service.component.annotations.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;
import com.novabase.omni.oney.apps.purchaseorder.controller.api.ReferenceDataResource;
import com.novabase.omni.oney.apps.purchaseorder.dto.ReferenceDataDTO;
import com.novabase.omni.oney.apps.purchaseorder.manager.ReferenceDataManager;

import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;

@Component
@RSProvider(value = AppProperties.ADDRESS_REFERENCE)
@RestController
@RequestMapping(AppProperties.ADDRESS_REFERENCE)
public class ReferenceDataController extends AbstractAppController implements ReferenceDataResource {

    //   TODO: Departments: only return departments that the logged user have access
    //   TODO: CoastCenter : filter by department
    //   TODO: InternalOrder : filter by department
    //   TODO: Projects : ???? filter by department?
    //   TODO: Item : filter by CoastCenter/InternalOrder/Projects(?)
    //   TODO: Assets : filter by CoastCenter/InternalOrder/Projects(?)
    //   TODO: TaxCode : no filter
    //   TODO: RetentionCode: filter by TaxCode
    //   TODO: POType : No filter
    //   TODO: DocumentClass: No filter    
    
    @ServiceReference
    private ReferenceDataManager referenceDataManager;

    @Override
    @RequestMapping(path = "/lists", method = RequestMethod.POST)
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    public Map<String, List<ReferenceDataDTO>> getReferenceData(@RequestBody List<String> referenceList) {
	return this.referenceDataManager.getReferenceData(referenceList, currentUser());
    }
    
}
