package com.novabase.omni.oney.apps.purchaseorder.exception;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderFailedToRemoveBlobException extends PurchaseOrderException {

    private static final long serialVersionUID = -3328724774019201311L;

    public PurchaseOrderFailedToRemoveBlobException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.INTERNAL_SERVER_ERROR, errorCode, ctx, args);
    }

    public static PurchaseOrderFailedToRemoveBlobException of(Context ctx, int attachmentIndex, Long instanceId, String message) {
	return new PurchaseOrderFailedToRemoveBlobException(AppProperties.FAILED_TO_REMOVE_BLOB_EXCEPTION, ctx, attachmentIndex, instanceId, message);
    }

}
