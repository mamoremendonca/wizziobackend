package com.novabase.omni.oney.apps.purchaseorder.exception;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderAttachmentFailedReadStreamException extends PurchaseOrderException {

    private static final long serialVersionUID = -6536364574772186973L;

    public PurchaseOrderAttachmentFailedReadStreamException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.INTERNAL_SERVER_ERROR, errorCode, ctx, args);
    }

    public static PurchaseOrderAttachmentFailedReadStreamException of(Context ctx, Long instanceId) {
	return new PurchaseOrderAttachmentFailedReadStreamException(AppProperties.FAILED_READ_STREAM_EXCEPTION, ctx, instanceId);
    }

}
