package com.novabase.omni.oney.apps.purchaseorder.controller.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;
import com.novabase.omni.oney.apps.purchaseorder.dto.DefaultDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderHeaderDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.TimeLineStepDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.WorkflowDTO;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderCouldNotClaimException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderFieldValidationException;

import io.digitaljourney.platform.modules.commons.type.HttpStatusCode;
import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.ApiKeyAuthDefinition.ApiKeyLocation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.BasicAuthDefinition;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;

//@formatter:off
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path(AppProperties.ADDRESS_PURCHASE_ORDER_CONTROLLER)
@SwaggerDefinition(
	securityDefinition = @SecurityDefinition(
		basicAuthDefinitions = {
			@BasicAuthDefinition(key = RSProperties.SWAGGER_BASIC_AUTH),
		},
		apiKeyAuthDefinitions = {
			@ApiKeyAuthDefinition(
				key = RSProperties.SWAGGER_BEARER_AUTH,
				name = RSProperties.HTTP_HEADER_API_KEY,
				in = ApiKeyLocation.HEADER)
			}
	),
	schemes = {
		SwaggerDefinition.Scheme.HTTP,
		SwaggerDefinition.Scheme.HTTPS,
		SwaggerDefinition.Scheme.DEFAULT
	},
	info = @Info(
		title = "Purchase Order API",
		description = "The Purchase Order API.",
		version = AppProperties.CURRENT_VERSION,
		license = @License(name = "Digital Journey License", url = "http://www.digitaljourney.io/license")
	),
	basePath = "bin/mvc.do/"+AppProperties.ADDRESS_PURCHASE_ORDER_CONTROLLER
)
@Api(
	value = "Purchase Order API",
	authorizations = {
		@Authorization(value = RSProperties.SWAGGER_BASIC_AUTH),
		@Authorization(value = RSProperties.SWAGGER_BEARER_AUTH)
	}
)
@ApiResponses(
	value = {
		@ApiResponse(code = HttpStatusCode.UNAUTHORIZED_CODE, message = RSProperties.SWAGGER_UNAUTHORIZED_MESSAGE),
		@ApiResponse(code = HttpStatusCode.FORBIDDEN_CODE, message = RSProperties.SWAGGER_FORBIDDEN_MESSAGE)
	}
)
public interface PurchaseOrderResource {

    @GET
    @Path("/init")
    @ApiOperation(value = "Init Journey", notes = "Initializes Purchase Order Journey")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK")})
    public void init();
    
    @POST
    @Path("/")
    @ApiOperation(value = "Creates a process", notes = "Creates a new process instance", response = PurchaseOrderDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = PurchaseOrderDTO.class, message = "OK"),
	    @ApiResponse(code = 400, response = PurchaseOrderFieldValidationException.class, message = "Bad Request"),
	    @ApiResponse(code = 500, response = PurchaseOrderException.class, message = "Internal Server Error") })
    public PurchaseOrderDTO createProcess(@ApiParam(value = "The PurchaseOrderHeader to be created", required = true) PurchaseOrderHeaderDTO purchaseOrderHeaderDTO);
    
    @GET
    @Path("/copy/{repositoryId}/{version}")
    @ApiOperation(value = "Copy a process", notes = "Creates a new process instance based on existing Purchase Order", response = PurchaseOrderDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = PurchaseOrderDTO.class, message = "OK"),
	    @ApiResponse(code = 400, response = PurchaseOrderFieldValidationException.class, message = "Bad Request"),
	    @ApiResponse(code = 500, response = PurchaseOrderException.class, message = "Internal Server Error") })
    public PurchaseOrderDTO copyPurchaseOrder(
	    @ApiParam(value = "The PurchaseOrder repository Id to be copy", required = true) @PathParam("repositoryId") Long repositoryId,
	    @ApiParam(value = "The PurchaseOrder repository version to be copy", required = true) @PathParam("version") Long version);
    
    @PUT
    @Path("/{instanceId}")
    @ApiOperation(value = "Update Process Header", notes = "Updates an existing process header", response = PurchaseOrderDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = PurchaseOrderDTO.class, message = "OK"),
	    @ApiResponse(code = 400, response = PurchaseOrderFieldValidationException.class, message = "Bad Request"),
	    @ApiResponse(code = 500, response = PurchaseOrderException.class, message = "Internal Server Error") })
    public PurchaseOrderDTO updateProcessHeader(
	    @ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") long instanceId,
	    @ApiParam(value = "The PurchaseOrderHeader to be created", required = true) PurchaseOrderHeaderDTO purchaseOrderHeaderDTO);

    @GET
    @Path("/{instanceId}")
    @ApiOperation(value = "Get Process Data", notes = "Retrieves the process data", response = PurchaseOrderDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = PurchaseOrderDTO.class, message = "OK")})
    public PurchaseOrderDTO getProcess(@PathParam("instanceId") long instanceId);
    
    @PUT
    @Path("/{instanceId}/submit")
    @ApiOperation(value = "Submit process to approval", notes = "Submit an existing process to approval workflow", response = WorkflowDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = PurchaseOrderHeaderDTO.class ,message = "OK"),
	    @ApiResponse(code = 400, response = PurchaseOrderFieldValidationException.class, message = "Bad Request"),
	    @ApiResponse(code = 500, response = PurchaseOrderException.class, message = "Internal Server Error") })
    public PurchaseOrderHeaderDTO submit(
	    @ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") long instanceId);
    
    @PUT
    @Path("/{instanceId}/approve")
    @ApiOperation(value = "Approve the process to the next step", notes = "Approve the process to the next step in the workflow", response = DefaultDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = WorkflowDTO.class, message = "OK"),
	    @ApiResponse(code = 400, response = PurchaseOrderFieldValidationException.class, message = "Bad Request"),
	    @ApiResponse(code = 500, response = PurchaseOrderException.class, message = "Internal Server Error") })
    public WorkflowDTO approvePurchaseOrder(
	    @ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") long instanceId,
	    @ApiParam(value = "Comments of the evaluation", required = true) String comment);
    
    @PUT
    @Path("/{instanceId}/return")
    @ApiOperation(value = "Return the process to the previous step", notes = "Approve the process to the previous step in the workflow", response = DefaultDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = WorkflowDTO.class, message = "OK"),
	    @ApiResponse(code = 400, response = PurchaseOrderFieldValidationException.class, message = "Bad Request"),
	    @ApiResponse(code = 500, response = PurchaseOrderException.class, message = "Internal Server Error") })
    public WorkflowDTO returnPurchaseOrder(
	    @ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") long instanceId,
	    @ApiParam(value = "Comments of the evaluation", required = true) String comment);
    
    @PUT
    @Path("/{instanceId}/reject")
    @ApiOperation(value = "Reject the process", notes = "Reject the process and finalize approval workflow", response = DefaultDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = DefaultDTO.class, message = "OK"),
	    @ApiResponse(code = 400, response = PurchaseOrderFieldValidationException.class, message = "Bad Request"),
	    @ApiResponse(code = 500, response = PurchaseOrderException.class, message = "Internal Server Error") })
    public DefaultDTO rejectPurchaseOrder(
	    @ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") long instanceId,
	    @ApiParam(value = "Comments of the evaluation", required = true) String comment);
    
    @PUT
    @Path("/{instanceId}/cancel")
    @ApiOperation(value = "Cancel the process", notes = "Cancel the process", response = DefaultDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
	    @ApiResponse(code = 400, response = PurchaseOrderFieldValidationException.class, message = "Bad Request"),
	    @ApiResponse(code = 500, response = PurchaseOrderException.class, message = "Internal Server Error") })
    public void cancelPurchaseOrder(
	    @ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") long instanceId);
    
    @GET
    @Path("/{instanceId}/timeline")
    @ApiOperation(value = "Get Process Timeline", notes = "Retrieves the Timeline", responseContainer = "List", response = TimeLineStepDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, responseContainer = "List", response = TimeLineStepDTO.class, message = "OK")})
    public List<TimeLineStepDTO> getTimeLine(
	    @ApiParam(value = "The unique identifier of the process instance", required = true, example = "1")@PathParam("instanceId") long instanceId);
    
    @PUT
    @Path("/{instanceId}/claim")
    @ApiOperation(value = "Claim Process to the Group of current user", notes = "Claim Process to the Group of current user")
    @ApiResponses(value = { 
	    @ApiResponse(code = 200, message = "OK"),
	    @ApiResponse(code = 400, response = PurchaseOrderCouldNotClaimException.class, message = "NOTOK")})
    public void claimProcess(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1")@PathParam("instanceId") long instanceId);
    
}
