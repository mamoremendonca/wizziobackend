package com.novabase.omni.oney.apps.purchaseorder.enums;

import java.util.Arrays;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.TimeLineStepTypeMs;

public enum TimeLineStepType {

    //@formatter:off
    DRAFT(null),
    SUBMITTED(TimeLineStepTypeMs.SUBMITTED), 
    TO_APPROVE(TimeLineStepTypeMs.TO_APPROVE), 
    APPROVED(TimeLineStepTypeMs.APPROVED), 
    RETURNED(TimeLineStepTypeMs.RETURNED), 
    REJECTED(TimeLineStepTypeMs.REJECTED),
    SKIPPED(TimeLineStepTypeMs.SKIPPED),
    CANCELED(TimeLineStepTypeMs.CANCELED), 
    SAP_SUBMITTED(TimeLineStepTypeMs.SAP_SUBMITTED);
    //@formatter:on

    private TimeLineStepTypeMs msType;

    private TimeLineStepType(TimeLineStepTypeMs msType) {
	this.msType = msType;
    }

    public TimeLineStepTypeMs getMsType() {
	return msType;
    }
    
    //@formatter:off
    public static TimeLineStepType valueOf(TimeLineStepTypeMs msType){
	return Arrays.asList(TimeLineStepType.values())
		.stream()
		.filter(type -> type.getMsType() != null && type.getMsType().equals(msType))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on
}
