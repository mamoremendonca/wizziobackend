package com.novabase.omni.oney.apps.purchaseorder.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class WorkflowStepDTO {

    @ApiModelProperty
    public int index;
    @ApiModelProperty
    public String userGroup;
    @ApiModelProperty
    public boolean hasNextStep;
    @ApiModelProperty
    public boolean active;

}
