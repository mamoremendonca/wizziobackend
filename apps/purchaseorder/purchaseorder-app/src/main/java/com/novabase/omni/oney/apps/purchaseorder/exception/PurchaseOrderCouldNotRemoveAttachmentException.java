package com.novabase.omni.oney.apps.purchaseorder.exception;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderCouldNotRemoveAttachmentException extends PurchaseOrderException {

    private static final long serialVersionUID = -7101687158595359865L;

    public PurchaseOrderCouldNotRemoveAttachmentException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.AUTHORIZATION_ERROR, errorCode, ctx, args);
    }

    public static PurchaseOrderCouldNotRemoveAttachmentException of(Context ctx, String userName, int attachmentIndex, Long instanceId) {
	return new PurchaseOrderCouldNotRemoveAttachmentException(AppProperties.USER_COULD_NOT_REMOVE_ATTACHMENT_EXCEPTION, ctx, userName, attachmentIndex, instanceId);
    }
    
    public static PurchaseOrderCouldNotRemoveAttachmentException ofStep(Context ctx, String userName, int attachmentIndex, Long instanceId) {
	return new PurchaseOrderCouldNotRemoveAttachmentException(AppProperties.STEP_COULD_NOT_REMOVE_ATTACHMENT_EXCEPTION, ctx, userName, attachmentIndex, instanceId);
    }

}
