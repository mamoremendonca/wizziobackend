package com.novabase.omni.oney.apps.purchaseorder.dto;

import java.util.Date;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.apps.purchaseorder.enums.PurchaseOrderType;
import com.novabase.omni.oney.apps.purchaseorder.enums.SearchScope;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("SearchFilterDTO")
public class SearchCriteriaRepositoryDTO extends DTO {
    
    @ApiModelProperty
    public String status;

    @ApiModelProperty
    public Long number;

    @ApiModelProperty
    public PurchaseOrderType type;

    @ApiModelProperty
    public String entityCode;			// Code or NIF?

    @ApiModelProperty
    public String internalOrderCode;

    @ApiModelProperty
    public String projectCode;
    
    @ApiModelProperty
    public String departmentCode;
    
    @ApiModelProperty
    public String owner;

    @ApiModelProperty
    public SearchScope scope;		// Mine, Team, All
    
    @ApiModelProperty
    public Long minVATValue;
    @ApiModelProperty
    public Long maxVATValue;

    @ApiModelProperty
    public Date createdDate;
    @ApiModelProperty
    public Date approvalDate;
    @ApiModelProperty
    public Date expirationDate;

}
