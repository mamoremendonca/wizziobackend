package com.novabase.omni.oney.apps.purchaseorder.enums;

import java.util.ArrayList;
import java.util.List;

import com.novabase.omni.oney.apps.purchaseorder.agent.reference.ReferenceAgentImpl;
import com.novabase.omni.oney.apps.purchaseorder.dto.reference.ReferenceDataDTO;
import com.novabase.omni.oney.apps.purchaseorder.provider.ReferenceDataProvider;

public enum ReferenceDataType {
    
    PURCHASE_ORDER_TYPE() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getPurchaseOrderType();
	}
	
    },
    
    PURCHASE_ORDER_LIST_STATUS() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getPurchaseOrderListStatus();
	}
	
    },
    
    DEPARTMENT() {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceAgentImpl agent) {
	    return new ArrayList<>();
	}
	
    };

    public abstract <T> List<T> getData(final ReferenceAgentImpl agent);
    
}
