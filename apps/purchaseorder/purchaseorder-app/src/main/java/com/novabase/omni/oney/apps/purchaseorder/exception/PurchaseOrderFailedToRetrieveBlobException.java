package com.novabase.omni.oney.apps.purchaseorder.exception;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderFailedToRetrieveBlobException extends PurchaseOrderException {

    private static final long serialVersionUID = -244264750240314709L;

    public PurchaseOrderFailedToRetrieveBlobException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.INTERNAL_SERVER_ERROR, errorCode, ctx, args);
    }

    public static PurchaseOrderFailedToRetrieveBlobException of(Context ctx, int attachmentIndex, Long instanceId, String message) {
	return new PurchaseOrderFailedToRetrieveBlobException(AppProperties.FAILED_TO_RETRIEVE_BLOB_EXCEPTION, ctx, attachmentIndex, instanceId, message);
    }

}
