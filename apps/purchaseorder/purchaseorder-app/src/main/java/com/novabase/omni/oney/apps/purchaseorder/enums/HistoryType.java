package com.novabase.omni.oney.apps.purchaseorder.enums;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.TimeLineStepTypeMs;

public enum HistoryType {

    //@formatter:off
    SUBMITTED(TimeLineStepTypeMs.SUBMITTED), 
    APPROVED(TimeLineStepTypeMs.APPROVED), 
    RETURNED(TimeLineStepTypeMs.RETURNED), 
    REJECTED(TimeLineStepTypeMs.REJECTED), 
    CANCELED(TimeLineStepTypeMs.CANCELED);
    //@formatter:on

    private TimeLineStepTypeMs msType;

    private HistoryType(TimeLineStepTypeMs msType) {
	this.msType = msType;
    }

    public TimeLineStepTypeMs getMsType() {
	return msType;
    }

}
