package com.novabase.omni.oney.apps.purchaseorder.manager;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.purchaseorder.AppContext;
import com.novabase.omni.oney.apps.purchaseorder.agent.core.CoreAgentImpl;
import com.novabase.omni.oney.apps.purchaseorder.agent.purchaseorderrepository.PurchaseOrderRepositoryAgentImpl;
import com.novabase.omni.oney.apps.purchaseorder.agent.workflow.WorkflowAgentImpl;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderHeaderDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.UserInformationDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.WorkflowDTO;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.CurrentAction;
import com.novabase.omni.oney.apps.purchaseorder.enums.PurchaseOrderStatus;
import com.novabase.omni.oney.apps.purchaseorder.enums.WorkFlowActionType;

import io.digitaljourney.platform.modules.commons.type.tuple.Pair;

@Component(service = { WorkflowManager.class })
public class WorkflowManager {

    @Reference
    private AppContext ctx;

    @Reference
    private CoreAgentImpl coreAgentImpl;

    @Reference
    private WorkflowAgentImpl workflowAgentImpl;

    @Reference
    private PurchaseOrderRepositoryAgentImpl purchaseOrderRepositoryAgentImpl;

    public PurchaseOrderHeaderDTO generateWorkflowAndSubmit(PurchaseOrderInstance instance) {

	validateInstance(instance);

	WorkflowDTO workflow = workflowAgentImpl.generateWorkFlowByPosition(instance.purchaseOrderHeader.userGroup, instance.purchaseOrderHeader.userId, instance.purchaseOrderHeader.totalWithoutTax);

	Pair<PurchaseOrderHeaderDTO, WorkflowDTO> submitPurchaseOrderToRepository = purchaseOrderRepositoryAgentImpl.submitPurchaseOrderToRepository(instance, workflow);

	PurchaseOrderHeaderDTO purchaseOrderHeader = submitPurchaseOrderToRepository.getValue0();
	WorkflowDTO workflowRepository = submitPurchaseOrderToRepository.getValue1();

	instance.purchaseOrderHeader.repositoryId = purchaseOrderHeader.repositoryId;
	instance.purchaseOrderHeader.number = purchaseOrderHeader.number;
	instance.purchaseOrderHeader.friendlyNumber = purchaseOrderHeader.friendlyNumber;
	instance.purchaseOrderHeader.version = purchaseOrderHeader.version;
	
	updateInstace(instance, workflowRepository);

	return purchaseOrderHeader;

    }

    public WorkflowDTO approvePurchaseOrder(PurchaseOrderInstance instance, Long userId) {

	WorkflowDTO workflowDTO = purchaseOrderRepositoryAgentImpl.approvePurchaseOrder(instance.purchaseOrderHeader.repositoryId, instance.currentAction, userId);
	updateInstace(instance, workflowDTO);

	if (workflowDTO.currentIndex == 0) {

	    // TODO: IMPORTANTE: AS REGRAS NA JOURNEY SOMENTE PODEM VALIDAR A INSTANCIA, O
	    // RETORNO DO METODO É IRRELEVANTE,
	    // QUANDO FOR CORRIGIDO TEMOS DE MUDAR A REGRA NA JORUNEY PARA AVALIAR O
	    // WORKFLOWDTO QUE É DEVOLVIDO NA APROVAÇÃO, REJEIÇÃO, RETORNO OU CANCELAMENTO
	    instance.currentAction = null; // if the P.O is approved and don't have more steps, we need to set
					   // currentAction = null in order to validate on post condition that the approval
					   // process is finished

	    // Send Mail to owner
	    sendNotification(instance.purchaseOrderHeader.userId, instance.getHeader().instanceId, instance.purchaseOrderHeader.friendlyNumber, WorkFlowActionType.APPROVE);
	}

	return workflowDTO;
    }

    public WorkflowDTO returnPurchaseOrder(PurchaseOrderInstance instance, Long userId) {

	WorkflowDTO workflowDTO = purchaseOrderRepositoryAgentImpl.returnPurchaseOrder(instance.purchaseOrderHeader.repositoryId, instance.currentAction, userId);
	updateInstace(instance, workflowDTO);
	return workflowDTO;
    }

    public WorkflowDTO rejectPurchaseOrder(PurchaseOrderInstance instance, Long userId) {

	WorkflowDTO workflowDTO = purchaseOrderRepositoryAgentImpl.rejectPurchaseOrder(instance.purchaseOrderHeader.repositoryId, instance.currentAction, userId);
	updateInstace(instance, workflowDTO);

	// Send Mail to owner
	sendNotification(instance.purchaseOrderHeader.userId, instance.getHeader().instanceId, instance.purchaseOrderHeader.friendlyNumber, WorkFlowActionType.REJECT);

	return workflowDTO;
    }

    private void sendNotification(Long userId, Long instanceId, String purchaseOrderNumber, WorkFlowActionType actionType) {
	try {
	    UserInformationDTO currentUserInformation = coreAgentImpl.getUserInformations(userId);
	    coreAgentImpl.sendMail(Arrays.asList(currentUserInformation.email), actionType, null, instanceId, purchaseOrderNumber);
	} catch (Exception e) {
	    ctx.logError("ERROR trying to send notification [" + actionType + "] for instanceId [" + instanceId + "]: " + e.getMessage());
	}
    }

    public WorkflowDTO cancelPurchaseOrder(PurchaseOrderInstance instance, Long userId) {

	WorkflowDTO workflowDTO = purchaseOrderRepositoryAgentImpl.cancelPurchaseOrder(instance.purchaseOrderHeader.repositoryId, instance.currentAction, userId);
	updateInstace(instance, workflowDTO);
	return workflowDTO;
    }

    public void claimProcess(PurchaseOrderInstance instance, Long userId, String userGroup) {
	WorkflowDTO workflow;
	try {
	    workflow = purchaseOrderRepositoryAgentImpl.claimPurchaseOrder(instance.purchaseOrderHeader.repositoryId, userId, userGroup, instance.currentAction);
	} catch (Exception e) {
	    throw ctx.couldNotClaimException(instance.getHeader().instanceId, userGroup);
	}

	updateInstace(instance, workflow);
    }

    public List<String> getUserDepartmentsByUserGroup(String userGroup) {
	return workflowAgentImpl.getDepartments(userGroup);
    }

    private void updateInstace(PurchaseOrderInstance instance, WorkflowDTO workflowDTO) {

	updateCurrentAction(instance, workflowDTO);
	updateAvailableActions(instance, workflowDTO);
    }

    private void updateCurrentAction(PurchaseOrderInstance instance, WorkflowDTO workflowDTO) {
	if (instance.attachments == null) {
	    instance.attachments = new ArrayList<>();
	}

	if (instance.currentAction.attachments != null && instance.currentAction.attachments.isEmpty() == false) {
	    instance.attachments.addAll(instance.currentAction.attachments);
	}

	instance.currentAction = new CurrentAction();
	instance.currentAction.startDate = LocalDateTime.now();
	instance.currentAction.userGroup = workflowDTO.currentUserGroup;
	instance.currentAction.limitDateToNextStep = workflowDTO.limitDateToNextStep;
    }

    private void updateAvailableActions(PurchaseOrderInstance instance, WorkflowDTO workflowDTO) {

	//@formatter:off
	int previousStepIndex = 
		workflowDTO.steps
			.stream()
			.sorted((step1, step2) -> step2.index - step1.index)
			.filter(step -> 
				step.active 
				&& step.index < workflowDTO.currentIndex)
			.map(step -> step.index)
			.findFirst()
			.orElse(0);
	//@formatter:on

	//@formatter:off
	if(PurchaseOrderStatus.REJECTED.equals(workflowDTO.purchaseOrderStatus)) {
	    
	    //When a Purchase Order is REJECTED the only possible action is CANCEL
	    instance.workFlowAvailableActions = 
		    Arrays.asList(WorkFlowActionType.CANCEL);
	
	}else if(PurchaseOrderStatus.IN_APPROVAL.equals(workflowDTO.purchaseOrderStatus) 
		&& previousStepIndex <= 1) {
	    
	    // When a Purchase Order is IN_APPROVAL and the current workflow index is 2 is only possible APPROVE or REJECT,
	    // because index 2 is the first index of the approval workflow the index 1 is the submission to workflow.   
	    instance.workFlowAvailableActions = 
		    Arrays.asList(WorkFlowActionType.APPROVE, WorkFlowActionType.REJECT);
	
	}else if(PurchaseOrderStatus.IN_APPROVAL.equals(workflowDTO.purchaseOrderStatus) 
		&& previousStepIndex >= 2) {
	   
	    // When a Purchase Order is IN_APPROVAL and the current workflow index is greater than 2 is possible APPROVE, RETURN or REJECT.
	    instance.workFlowAvailableActions = 
		    Arrays.asList(WorkFlowActionType.APPROVE, WorkFlowActionType.RETURN, WorkFlowActionType.REJECT);
	
	}else {
	    
	    // Otherwise the Purchase Order is APPROVED or CANCELED, is not possible perform any action.
	    instance.workFlowAvailableActions = null;
	}
	//@formatter:on
    }

    // TODO: create rules to validate P.O. and remove this code from here.
    private void validateInstance(PurchaseOrderInstance instance) {
	if (instance.purchaseOrderHeader == null) {
	    throw ctx.fieldValidationException("purchaseOrderHeader");
	}

	if (instance.items == null) {
	    throw ctx.fieldValidationException("items");
	}

	if (instance.purchaseOrderHeader.type == null) {
	    throw ctx.fieldValidationException("purchaseOrderHeader.type");
	}

	if (StringUtils.isBlank(instance.purchaseOrderHeader.departmentCode)) {
	    throw ctx.fieldValidationException("purchaseOrderHeader.departmentCode");
	}

//	if (StringUtils.isBlank(instance.purchaseOrderHeader.internalOrderCode) && StringUtils.isBlank(instance.purchaseOrderHeader.projectCode)) {
//	    throw ctx.fieldValidationException("purchaseOrderHeader.internalOrderCode && purchaseOrderHeader.projectCode");
//	}

	if (StringUtils.isBlank(instance.purchaseOrderHeader.supplierCode)) {
	    throw ctx.fieldValidationException("purchaseOrderHeader.supplierCode");
	}

	if (instance.purchaseOrderHeader.expirationDate == null) {
	    throw ctx.fieldValidationException("purchaseOrderHeader.expirationDate");
	}
    }
}
