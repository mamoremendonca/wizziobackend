/*-
 * #%L
 * Apps :: Purchase Order App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.purchaseorder.agent.workflow;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.google.common.collect.ImmutableMap;
import com.novabase.omni.oney.apps.purchaseorder.AppContext;
import com.novabase.omni.oney.apps.purchaseorder.AppProperties;
import com.novabase.omni.oney.apps.purchaseorder.dto.WorkflowDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.WorkflowStepDTO;
import com.novabase.omni.oney.apps.purchaseorder.manager.CoreManager;
import com.novabase.omni.oney.apps.purchaseorder.mapper.WorkflowMapper;
import com.novabase.omni.oney.modules.workflow.service.api.WorkflowResource;
import com.novabase.omni.oney.modules.workflow.service.api.dto.SearchWorkflowMsDTOBuilder;
import com.novabase.omni.oney.modules.workflow.service.api.enums.NodeAttributeType;

import io.digitaljourney.platform.modules.mvc.api.agent.AbstractCoreAgent;

/**
 * Implementation of a Core Agent to communicate with micro-service.
 *
 */
// @formatter:off
@Component(
	service = { Object.class, WorkflowAgentImpl.class },
	configurationPid = WorkflowAgentConfig.CPID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	reference = {
		@Reference(
			name = AppProperties.REF_CONTEXT,
			service = AppContext.class,
			cardinality = ReferenceCardinality.MANDATORY
		)
	}
)
@Designate(ocd = WorkflowAgentConfig.class)
// @formatter:on
public class WorkflowAgentImpl extends AbstractCoreAgent<AppContext, WorkflowAgentConfig> {

    @Reference
    public CoreManager coreManager;

    @Reference
    public WorkflowResource workflow;

    /**
     * Method called whenever the component is activated.
     *
     * @param ctx    Component context
     * @param config Component configuration
     */
    @Activate
    public void activate(ComponentContext ctx, WorkflowAgentConfig config) {
	prepare(ctx, config);
    }

    /**
     * Method called whenever the component configuration changes.
     *
     * @param config Component configuration
     */
    @Modified
    public void modified(WorkflowAgentConfig config) {
	prepare(config);
    }

    public WorkflowDTO generateWorkFlowByPosition(String position, Long userId, Double totalWithoutTax) {
	
	
	if(totalWithoutTax > 999999999) {
	    totalWithoutTax = 999999999D; // The DB script limits the amount to 999.999.999,00
	}

	WorkflowDTO workflowDTO = null;
	
	// Offline mode (Mock results)
	if (getConfig().offlineMode()) {
	    // Dummy start
	    workflowDTO = new WorkflowDTO();
	    workflowDTO.steps = new ArrayList<>();
	    for (int i = 1; i <= 5; i++) {
		WorkflowStepDTO step = new WorkflowStepDTO();
		step.index = i;
		step.hasNextStep = i <= 3;
		step.active = i <= 4;
		step.userGroup = "GROUP" + i;
		workflowDTO.steps.add(step);
	    }
	    // Dummy end
	} else {
	    workflowDTO = WorkflowMapper.INSTANCE.toWorkflowDTO(
		    workflow.generateWorkflowWith2ActiveSteps(
			    position, 
			    new SearchWorkflowMsDTOBuilder()
			    		.withVariables(ImmutableMap.of("value", String.valueOf(((int)Math.round(totalWithoutTax)))))
			    		.build()
			    ));
	}

	workflowDTO.creationDate = LocalDateTime.now();
	WorkflowStepDTO currentStep = workflowDTO.getStepByIndex(2); // the workflow starts always at the second step because the first is the
								     // submit action

	workflowDTO.currentUserGroup = currentStep.userGroup;
	workflowDTO.currentIndex = currentStep.index;
	workflowDTO.hasNextStep = currentStep.hasNextStep;

	return workflowDTO;
    }

    /**
     * Based on the current userGroup, it searches for the subtreee userGroups and
     * returns the list of departments associated with each userGroup
     * 
     * @param userGroup
     * @return List of Departments in the userGroup subtree (Organizational
     *         Structure)
     */
    public List<String> getDepartments(String userGroup) {
	return workflow.getDescendants(userGroup, NodeAttributeType.DEPARTMENT).stream().filter(d -> !d.equals("")).distinct().collect(Collectors.toList());
    }
}
