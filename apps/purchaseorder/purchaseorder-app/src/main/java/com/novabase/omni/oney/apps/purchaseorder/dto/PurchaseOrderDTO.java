package com.novabase.omni.oney.apps.purchaseorder.dto;

import java.io.Serializable;
import java.util.List;

import com.novabase.omni.oney.apps.purchaseorder.enums.WorkFlowActionType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class PurchaseOrderDTO extends DefaultDTO implements Serializable {

    private static final long serialVersionUID = 5500978832182866605L;

    @ApiModelProperty
    public PurchaseOrderHeaderDTO purchaseOrderHeader;

    @ApiModelProperty
    public List<ItemDTO> items;
    
    public List<WorkFlowActionType> workFlowAvailableActions;

}
