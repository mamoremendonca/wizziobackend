package com.novabase.omni.oney.apps.purchaseorder.exception;

import com.novabase.omni.oney.apps.purchaseorder.AppContext;
import com.novabase.omni.oney.apps.purchaseorder.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderUserCannotExecuteActionException extends PurchaseOrderException {

    private static final long serialVersionUID = 9178136962609680355L;

    public PurchaseOrderUserCannotExecuteActionException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.AUTHORIZATION_ERROR, errorCode, ctx, args);
    }

    public static PurchaseOrderUserCannotExecuteActionException of(Context ctx, Long instanceId, String userName, String action) {
	return new PurchaseOrderUserCannotExecuteActionException(AppProperties.OWNER_ACTION_USER_CANNOT_EXECUTE_ACTION_EXCEPTION, ctx, instanceId, userName, action);
    }

    public static PurchaseOrderUserCannotExecuteActionException of(AppContext ctx, Long instanceId, String userName, String action, String instanceCurrentUserGroup, String sessionCurrentUserGroup) {
	return new PurchaseOrderUserCannotExecuteActionException(AppProperties.CURRENT_GROUP_ACTION_USER_CANNOT_EXECUTE_ACTION_EXCEPTION, ctx, instanceId, userName, action, instanceCurrentUserGroup, sessionCurrentUserGroup);
    }

}
