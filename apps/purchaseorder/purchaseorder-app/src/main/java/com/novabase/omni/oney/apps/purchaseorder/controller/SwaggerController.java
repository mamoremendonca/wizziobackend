package com.novabase.omni.oney.apps.purchaseorder.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.osgi.annotation.versioning.ProviderType;
import org.osgi.service.component.annotations.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;
import com.novabase.omni.oney.apps.purchaseorder.controller.ri.AbstractAppController;

import io.digitaljourney.platform.modules.commons.type.HttpStatusCode;
import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;
//import io.digitaljourney.platform.plugins.providers.rsprovider.annotations.CustomRsProvider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.ApiKeyAuthDefinition.ApiKeyLocation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.BasicAuthDefinition;
import io.swagger.annotations.ExternalDocs;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;

//@formatter:off
/**
* Purchase Order Base Resource, this is only used for documentation purposes.
* *
*/
@Controller
//@CustomRsProvider(value=AppProperties.ADDRESS, exportedIntents = {})
@Component(
	service = { SwaggerController.class }
)
@RequestMapping(AppProperties.ADDRESS )
@ProviderType
@Path(AppProperties.ADDRESS)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@SwaggerDefinition(
	securityDefinition = @SecurityDefinition(
		basicAuthDefinitions = {
			@BasicAuthDefinition(key = RSProperties.SWAGGER_BASIC_AUTH),
		},
		apiKeyAuthDefinitions = {
			@ApiKeyAuthDefinition(
				key = RSProperties.SWAGGER_BEARER_AUTH,
				name = RSProperties.HTTP_HEADER_API_KEY,
				in = ApiKeyLocation.HEADER)
			}
	),
	schemes = {
		SwaggerDefinition.Scheme.HTTP,
		SwaggerDefinition.Scheme.HTTPS,
		SwaggerDefinition.Scheme.DEFAULT
	},
	info = @Info(
		title = "Purchase Order Base API",
		description = "The Purchase Order Base API.",
		version = AppProperties.CURRENT_VERSION,
		license = @License(name = "Digital Journey License", url = "http://www.digitaljourney.io/license")
	),
	basePath = "bin/mvc.do/"+AppProperties.ADDRESS,
	tags = {
		@Tag(name=AppProperties.ADDRESS_REFERENCE),
		@Tag(name=AppProperties.ADDRESS)
	}
	
)
@Api(
	value = "Purchase Order Base API",
	authorizations = {
		@Authorization(value = RSProperties.SWAGGER_BASIC_AUTH),
		@Authorization(value = RSProperties.SWAGGER_BEARER_AUTH)
	},
	tags =
	{
		AppProperties.ADDRESS_REFERENCE,
		AppProperties.ADDRESS
	}
)
@ApiResponses(
	value = {
		@ApiResponse(code = HttpStatusCode.UNAUTHORIZED_CODE, message = RSProperties.SWAGGER_UNAUTHORIZED_MESSAGE),
		@ApiResponse(code = HttpStatusCode.FORBIDDEN_CODE, message = RSProperties.SWAGGER_FORBIDDEN_MESSAGE),
		@ApiResponse(code = HttpStatusCode.INTERNAL_SERVER_ERROR_CODE, message = AppProperties.INTERNAL_SERVER_ERROR_MSG)
	}
)
//@formatter:on
public class SwaggerController extends AbstractAppController {

    // nothing here we just fake some endpoints
    /** Base CXF path */
    public static final String BASE_CXF_PATH = "cms";

    // TODO: rever utilização de BASE_CXF_PATH com valor "cms"
    // TODO: actualizar nas definições abaixo os respectivos endereços/paths de hardcoded strings para Constants
    
    /**
     * Fake endpoint
     */
    @GET
    @Path("/item")
    @ApiOperation(value = "Base endpoint for Items related operations. See documentation for more information.", notes = "This is a mock endpoint")
    @ExternalDocs(url = BASE_CXF_PATH + AppProperties.ADDRESS + "/items/api-docs?url=../swagger.json", value = "Items API")
    public void getItems() {}

    /**
     * Fake endpoint
     */
    @GET
    @Path("/reference")
    @ApiOperation(value = "Base endpoint for ReferenceData related operations. See documentation for more information.", notes = "This is a mock endpoint")
    @ExternalDocs(url = BASE_CXF_PATH + AppProperties.ADDRESS + "/reference/api-docs?url=../swagger.json", value = "Reference Data API")
    public void getReferenceData() {}

    /**
     * Fake endpoint
     */
    @GET
    @Path("/purchaseorders")
    @ApiOperation(value = "Base endpoint for Purchase Orders related operations. See documentation for more information.", notes = "This is a mock endpoint")
    @ExternalDocs(url = BASE_CXF_PATH + AppProperties.ADDRESS + "/purchaseorder/api-docs?url=../swagger.json", value = "Purchase Orders API")
    public void getPurchaseOrders() {}

    /**
     * Fake endpoint
     */
    @GET
    @Path("/attachments")
    @ApiOperation(value = "Base endpoint for Attachments related operations. See documentation for more information.", notes = "This is a mock endpoint")
    @ExternalDocs(url = BASE_CXF_PATH + AppProperties.ADDRESS + "/attachments/api-docs?url=../swagger.json", value = "Attachments API")
    public void getAttachments() {}
}
