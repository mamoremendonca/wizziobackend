package com.novabase.omni.oney.apps.purchaseorder.dto;

import java.io.InputStream;

import org.osgi.dto.DTO;

public class FileDTO extends DTO {

    public String mimeType;
    public InputStream inputStream;
    public String fileName;
}
