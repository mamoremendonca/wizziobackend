package com.novabase.omni.oney.apps.purchaseorder.enums;

public enum SearchScope {
    MINE, TEAM, ALL;
}
