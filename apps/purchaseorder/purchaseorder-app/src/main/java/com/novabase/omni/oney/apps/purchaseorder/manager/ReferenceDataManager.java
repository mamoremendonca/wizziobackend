package com.novabase.omni.oney.apps.purchaseorder.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.util.CollectionUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.purchaseorder.agent.reference.ReferenceAgentImpl;
import com.novabase.omni.oney.apps.purchaseorder.agent.workflow.WorkflowAgentImpl;
import com.novabase.omni.oney.apps.purchaseorder.dto.reference.DepartmentDTO;
import com.novabase.omni.oney.apps.purchaseorder.enums.ReferenceDataType;
import com.novabase.omni.oney.apps.purchaseorder.mapper.ReferenceDataMapper;

@Component(service = { ReferenceDataManager.class })
public class ReferenceDataManager {

    @Reference
    private CoreManager coreManager;

    @Reference
    private WorkflowAgentImpl workflowAgent;

    @Reference
    private ReferenceAgentImpl referenceAgent;

    public <T> Map<String, List<T>> getReferenceData(List<String> referenceList, String username) {
	
	final Map<String, List<T>> resultMap = new HashMap<>();
	if (!CollectionUtils.isEmpty(referenceList)) {
	    
	    referenceList.stream().forEach(referenceName -> {

		resultMap.put(referenceName, ReferenceDataType.valueOf(referenceName) == ReferenceDataType.DEPARTMENT ? this.getDepartments(username) 
														      : ReferenceDataType.valueOf(referenceName).getData(this.referenceAgent));
		
	    });
	    
	}
	
	return resultMap;
	
    }
    
    @SuppressWarnings("unchecked")
    private <T> List<T> getDepartments(final String username) {
	
        final List<DepartmentDTO> departmentsFromService = ReferenceDataMapper.INSTANCE.departmnetMsDTOtoDepartmentDTOList(this.referenceAgent.getDepartments());
        final List<DepartmentDTO> userDepartments = ReferenceDataMapper.INSTANCE.stringToDepartmentDTOList(this.workflowAgent.getDepartments(this.coreManager.getUserGroup(username)));
        
        if(userDepartments == null || userDepartments.isEmpty()) {
            return (List<T>) new ArrayList<>();
        } else if(departmentsFromService == null || departmentsFromService.isEmpty()) {
            return (List<T>) userDepartments;
        } else {
            
            final List<DepartmentDTO> departments = new ArrayList<>();
            userDepartments.stream().
        			forEach(department -> {
    	 			    
        			    final int departmentIndex  = departmentsFromService.indexOf(department); 
        			    departments.add(departmentIndex >= 0 ? departmentsFromService.get(departmentIndex) : department);
            	        	    
        			});
            return (List<T>) departments;
    	 
        }
	
    }
    
}
