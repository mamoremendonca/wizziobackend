package com.novabase.omni.oney.apps.purchaseorder.dto;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.apps.purchaseorder.enums.OrderType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("OrderBy")
public class OrderByDTO extends DTO {

    @ApiModelProperty
    public String field;

    @ApiModelProperty
    public OrderType type;
    
    public OrderByDTO(String field, String type) {
	this.field = field;
	this.type = OrderType.getEnum(type);
    }
    
    public OrderByDTO(String field, OrderType type) {
	this.field = field;
	this.type = type;
    }
}
