package com.novabase.omni.oney.apps.purchaseorder.dto;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ReferenceDataDTO extends DTO {

    @ApiModelProperty
    public String code;

    @ApiModelProperty
    public String description;

    @ApiModelProperty
    public boolean active;
}
