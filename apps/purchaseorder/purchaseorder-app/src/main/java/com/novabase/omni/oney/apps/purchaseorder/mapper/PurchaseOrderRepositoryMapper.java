package com.novabase.omni.oney.apps.purchaseorder.mapper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.apps.purchaseorder.dto.AttachmentDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.AttachmentRepositoryDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.ItemDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.OrderByDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderHeaderDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderRepositoryDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.SearchCriteriaRepositoryDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.TimeLineStepDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.WorkflowDTO;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.Attachment;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.CurrentAction;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.Item;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.AttachmentMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.HistoryMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.ItemMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.OrderByMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.PurchaseOrderMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.SearchCriteriaMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.TimeLineStepMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.WorkflowActionMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.WorkflowMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.TimeLineStepTypeMs;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public abstract class PurchaseOrderRepositoryMapper {

    public static final PurchaseOrderRepositoryMapper INSTANCE = Mappers.getMapper(PurchaseOrderRepositoryMapper.class);

    public PurchaseOrderMsDTO toPurchaseOrderMsDTO(PurchaseOrderInstance instance, WorkflowDTO workflow) {

	PurchaseOrderMsDTO purchaseOrder = new PurchaseOrderMsDTO();
	purchaseOrder.instanceId = instance.getHeader().instanceId;
	purchaseOrder.number = instance.purchaseOrderHeader.number;
	purchaseOrder.version = instance.purchaseOrderHeader.version;
	purchaseOrder.type = instance.purchaseOrderHeader.type.getMsType();
	purchaseOrder.supplierCode = instance.purchaseOrderHeader.supplierCode;
	purchaseOrder.description = instance.purchaseOrderHeader.description;
	purchaseOrder.departmentCode = instance.purchaseOrderHeader.departmentCode;
	purchaseOrder.projectCode = instance.purchaseOrderHeader.projectCode;
	purchaseOrder.internalOrderCode = instance.purchaseOrderHeader.internalOrderCode;
	purchaseOrder.creationDate = instance.getHeader().createdOn;
	purchaseOrder.expirationDate = instance.purchaseOrderHeader.expirationDate;
	purchaseOrder.userGroup = instance.purchaseOrderHeader.userGroup;
	purchaseOrder.userId = instance.purchaseOrderHeader.userId;
	purchaseOrder.totalWithoutTax = instance.purchaseOrderHeader.totalWithoutTax;

	purchaseOrder.histories = new ArrayList<>();
	HistoryMsDTO history = toHistoryMsDTO(instance.currentAction);
	history.date = LocalDateTime.now();
	history.userGroup = purchaseOrder.userGroup;
	history.userId = purchaseOrder.userId;
	history.type = TimeLineStepTypeMs.SUBMITTED;

	purchaseOrder.histories.add(history);
	purchaseOrder.items = toItemMsDTOList(instance.items);

	purchaseOrder.workflow = toWorkflowMsDTO(workflow);

	return purchaseOrder;
    }

    public abstract ItemMsDTO toItemMsDTO(Item item);

    public abstract List<ItemMsDTO> toItemMsDTOList(List<Item> items);

    @Mapping(source = "ownerId", target = "userId")
    @Mapping(target = "documentClass", expression = "java(attachment.documentClass.getMsType())")
    public abstract AttachmentMsDTO toAttachmentMsDTO(Attachment attachment);

    public abstract List<AttachmentMsDTO> toAttachmentMsDTOList(List<Attachment> attachments);

    public abstract WorkflowMsDTO toWorkflowMsDTO(WorkflowDTO workflow);

    public abstract HistoryMsDTO toHistoryMsDTO(CurrentAction currentAction);

    @Mapping(source = "id", target = "repositoryId")
    @Mapping(target = "type", expression = "java(com.novabase.omni.oney.apps.purchaseorder.enums.PurchaseOrderType.valueOf(purchaseOrderMsDTO.type))")
    @Mapping(target = "repositoryStatus", expression = "java(com.novabase.omni.oney.apps.purchaseorder.enums.PurchaseOrderStatus.valueOf(purchaseOrderMsDTO.status))")
    @Mapping(target = "currentUserGroup", source = "workflow.currentUserGroup")
    public abstract PurchaseOrderHeaderDTO toPurchaseOrderHeaderDTO(PurchaseOrderMsDTO purchaseOrderMsDTO);

    public abstract WorkflowActionMsDTO toWorkflowActionMsDTO(CurrentAction action);

    @Mapping(target = "purchaseOrderStatus", expression = "java(com.novabase.omni.oney.apps.purchaseorder.enums.PurchaseOrderStatus.valueOf(workFlow.purchaseOrderStatus))")
    public abstract WorkflowDTO toWorkflowDTO(WorkflowMsDTO workFlow);

    @Mapping(source = "scope", target = "scope.scopeType")
    @Mapping(source = "owner", target = "scope.userGroup")
    public abstract SearchCriteriaMsDTO toSearchCriteriaMs(SearchCriteriaRepositoryDTO searchCriteira);

    @Mapping(source = "id", target = "purchaseOrderHeader.repositoryId")
    @Mapping(source = "number", target = "purchaseOrderHeader.number")
    @Mapping(source = "friendlyNumber", target = "purchaseOrderHeader.friendlyNumber")
    @Mapping(source = "version", target = "purchaseOrderHeader.version")
    //@Mapping(source = "status", ignore = true, target = "")
    @Mapping(source = "type", target = "purchaseOrderHeader.type")
    @Mapping(source = "supplierCode", target = "purchaseOrderHeader.supplierCode")
    @Mapping(source = "description", target = "purchaseOrderHeader.description")
    @Mapping(source = "departmentCode", target = "purchaseOrderHeader.departmentCode")
    @Mapping(source = "projectCode", target = "purchaseOrderHeader.projectCode")
    @Mapping(source = "internalOrderCode", target = "purchaseOrderHeader.internalOrderCode")
    @Mapping(source = "creationDate", target = "purchaseOrderHeader.creationDate")
    @Mapping(source = "expirationDate", target = "purchaseOrderHeader.expirationDate")
    @Mapping(source = "approvalDate", target = "purchaseOrderHeader.approvalDate")
    @Mapping(source = "totalWithoutTax", target = "purchaseOrderHeader.totalWithoutTax")
    @Mapping(source = "canClaim", target = "canClaim")
    //@Mapping(source = "userGroup", ignore = true, target = "")
    //@Mapping(source = "userId", ignore = true, target = "")
    @Mapping(source = "items", target = "items")
    //@Mapping(source = "histories", ignore = true, target = "")
    //@Mapping(source = "workflow", ignore = true, target = "")
    @Mapping(target = "purchaseOrderHeader.repositoryStatus", expression = "java(com.novabase.omni.oney.apps.purchaseorder.enums.PurchaseOrderStatus.valueOf(purchaseOrderMsDTO.status))")
    public abstract PurchaseOrderRepositoryDTO toPurchaseOrderRepositoryDTO(PurchaseOrderMsDTO purchaseOrder);

    public abstract List<ItemDTO> toItemListDTO(List<ItemMsDTO> item);
    public abstract ItemDTO toItemDTO(ItemMsDTO item);
    
    
    public abstract OrderByMsDTO toOrderByMs(OrderByDTO orderBy);

    @Mapping(target = "type", expression = "java(com.novabase.omni.oney.apps.purchaseorder.enums.TimeLineStepType.valueOf(timelineStep.type))")
    public abstract TimeLineStepDTO toTimeLineStepDTO(TimeLineStepMsDTO timelineStep);

    public abstract List<TimeLineStepDTO> toTimeLineStepDTOList(List<TimeLineStepMsDTO> timelineSteps);

    @Mapping(target = "ownerId", source = "userId")
    public abstract AttachmentDTO toAttachmentMsDTO(AttachmentMsDTO attachment);

    @Mapping(target = "ownerId", source = "userId")
    @Mapping(target = "documentClass", expression = "java(com.novabase.omni.oney.apps.purchaseorder.enums.DocumentClassType.valueOf(purchaseOrderAttachment.documentClass))")
    public abstract Attachment toAttachment(AttachmentMsDTO purchaseOrderAttachment);
    public abstract List<Attachment> toAttachmentList(List<AttachmentMsDTO> purchaseOrderAttachment);

    @Mapping(target = "ownerId", source = "userId")
    @Mapping(target = "documentClass", expression = "java(com.novabase.omni.oney.apps.purchaseorder.enums.DocumentClassType.valueOf(attachment.documentClass))")
    public abstract AttachmentRepositoryDTO toAttachmentRepositoryDTO(final AttachmentMsDTO attachment); 
    
    // TOD: Mapper PurchaseOrderMsDTO to PurchaseOrderDTO
}
