package com.novabase.omni.oney.apps.purchaseorder.entity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.purchaseorder.enums.DocumentClassType;
import com.novabase.omni.oney.apps.purchaseorder.enums.PurchaseOrderType;
import com.novabase.omni.oney.apps.purchaseorder.enums.WorkFlowActionType;
import com.novabase.omni.oney.apps.purchaseorder.json.serializer.NBLocalDateTimeSerializer;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.TargetType;

import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.session.JourneyInstance;

public class PurchaseOrderInstance extends JourneyInstance {

	public PurchaseOrderHeader purchaseOrderHeader; // o nome é purchaseOrderHeader porque temos na classe extendida
	// "JourneyInstance" um atributo chamado header
	public List<Item> items;
	/**
	 * The current action on instance, when some action that are persisted on
	 * repository is done, the data on this object is send to the repository and
	 * this object became new again to the next action.
	 */
	public CurrentAction currentAction;
	/**
	 * Attachments that already are on PurchaseOrderRepository
	 */
	public List<Attachment> attachments;

	/**
	 * List of available actions on current status and workflow position.
	 */
	public List<WorkFlowActionType> workFlowAvailableActions;

	public static class PurchaseOrderHeader {
		public Long repositoryId;
		public Long number;
		public String friendlyNumber;
		public Long version;
		public PurchaseOrderType type;
		public String supplierCode;
		public String description;
		public String departmentCode;
		public String projectCode;
		public String internalOrderCode;
		@JsonDeserialize(using = LocalDateTimeDeserializer.class)
		@JsonSerialize(using = NBLocalDateTimeSerializer.class)
		public LocalDateTime expirationDate;
		public Double totalWithoutTax = 0d; // default value
		public String userGroup;
		public Long userId;
	}

	public static class Item {
		public int index;
		public String code;
		public String description;
		public String costCenterCode;
		public Double amount;
		public Double unitPriceWithoutTax;
		public String taxCode;
		@JsonDeserialize(using = LocalDateTimeDeserializer.class)
		@JsonSerialize(using = NBLocalDateTimeSerializer.class)
		public LocalDateTime startDate;
		@JsonDeserialize(using = LocalDateTimeDeserializer.class)
		@JsonSerialize(using = NBLocalDateTimeSerializer.class)
		public LocalDateTime endDate;
		public String comment;
	}

	public static class Attachment {
		public int index;
		@JsonDeserialize(using = LocalDateTimeDeserializer.class)
		@JsonSerialize(using = NBLocalDateTimeSerializer.class)
		public LocalDateTime date;
		public String mimeType;
		public String name;
		public Long blobId;
		public Long ownerId;
		public DocumentClassType documentClass;
	}

	public static class CurrentAction {
		public String comment;
		@JsonDeserialize(using = LocalDateTimeDeserializer.class)
		@JsonSerialize(using = NBLocalDateTimeSerializer.class)
		public LocalDateTime startDate;
		public String userGroup;
		@JsonDeserialize(using = LocalDateTimeDeserializer.class)
		@JsonSerialize(using = NBLocalDateTimeSerializer.class)
		public LocalDateTime limitDateToNextStep;
		public List<Attachment> attachments;
	}

	// @formatter:off
	public void refreshTotal() {
		if (this.purchaseOrderHeader == null) {
			this.purchaseOrderHeader = new PurchaseOrderHeader();
		}

		if (this.items == null) {
			this.items = new ArrayList<>();
		}

		this.purchaseOrderHeader.totalWithoutTax = this.items.stream().filter(item -> item != null)
				.map(item -> item.amount * item.unitPriceWithoutTax).reduce(0d, Double::sum);
	}
	// @formatter:on

	/**
	 * We must refresh the targets on the process instance in order to obtain this
	 * fields on worklist filter, the payload is not accessible on search methods of
	 * processcontinuity. All fields that are on the instance and does not is a
	 * default field on JourneyInstance must be on target to be accessible to get on
	 * search methods.
	 */
	public void refreshTargets() {
		// @formatter:off

		Long number = purchaseOrderHeader.number != null ? purchaseOrderHeader.number : 0;

		String friendlyNumber = purchaseOrderHeader.friendlyNumber != null ? purchaseOrderHeader.friendlyNumber : "";

		Double totalWithoutTax = purchaseOrderHeader.totalWithoutTax != null ? purchaseOrderHeader.totalWithoutTax : 0;

		String departmentCode = purchaseOrderHeader.departmentCode != null ? purchaseOrderHeader.departmentCode : "";

		String supplierCode = purchaseOrderHeader.supplierCode != null ? purchaseOrderHeader.supplierCode : "";

		String repositoryId = purchaseOrderHeader.repositoryId != null ? purchaseOrderHeader.repositoryId.toString()
				: "";

		String expirationDate = purchaseOrderHeader.expirationDate != null
				? DateTimeFormatter.ofPattern("yyyy-MM-dd").format(purchaseOrderHeader.expirationDate)
				: "";

		String currentGroup = currentAction != null && currentAction.userGroup != null ? currentAction.userGroup : "";

		String createByGroup = purchaseOrderHeader != null && purchaseOrderHeader.userGroup != null
				? purchaseOrderHeader.userGroup
				: "";

		String creationUserId = purchaseOrderHeader != null && purchaseOrderHeader.userId != null
				? purchaseOrderHeader.userId.toString()
				: "";
		// @formatter:on

		getHeader().targets.put(TargetType.ENTITY_CODE.name(), supplierCode);
		getHeader().targets.put(TargetType.DEPARTMENT_CODE.name(), departmentCode);
		getHeader().targets.put(TargetType.VALUE.name(), TargetType.formatValue(totalWithoutTax));
		getHeader().targets.put(TargetType.NUMBER.name(), TargetType.formatNumber(number));
		getHeader().targets.put(TargetType.FRIENDLY_NUMBER.name(), friendlyNumber);
		getHeader().targets.put(TargetType.EXPIRATION_DATE.name(), expirationDate);
		getHeader().targets.put(TargetType.REPOSITORY_ID.name(), repositoryId);
		getHeader().targets.put(TargetType.CREATION_USER_ID.name(), creationUserId);

		// @formatter:off
		if (workFlowAvailableActions != null && workFlowAvailableActions.isEmpty() == false
				&& workFlowAvailableActions.contains(WorkFlowActionType.APPROVE)) {

			getHeader().targets.put(TargetType.CURRENT_GROUP.name(), currentGroup);

		} else {
			// The CURRENT_GROUP target is set when the PurchaseOrder is on the
			// Approval workflow with the currentGroup, if the PurchaseOrder status is Draft
			// or Rejected
			// the CURRENT_GROUP must be set with the createByGroup

			getHeader().targets.put(TargetType.CURRENT_GROUP.name(), createByGroup);
		}
		// @formatter:on

	}
}
