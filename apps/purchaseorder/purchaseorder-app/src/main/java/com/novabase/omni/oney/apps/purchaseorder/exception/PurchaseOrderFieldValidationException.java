package com.novabase.omni.oney.apps.purchaseorder.exception;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderFieldValidationException extends PurchaseOrderException {

    private static final long serialVersionUID = 106822106503971206L;

    public PurchaseOrderFieldValidationException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
    }

    public static PurchaseOrderFieldValidationException of(Context ctx, String field) {
	return new PurchaseOrderFieldValidationException(AppProperties.FIELD_VALIDATION_EXCEPTION, ctx, field);
    }

}
