package com.novabase.omni.oney.apps.purchaseorder.controller.ri;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.eclipse.gemini.blueprint.extensions.annotation.ServiceReference;
import org.osgi.service.component.annotations.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;
import com.novabase.omni.oney.apps.purchaseorder.annotations.OwnerAction;
import com.novabase.omni.oney.apps.purchaseorder.controller.api.ItemResource;
import com.novabase.omni.oney.apps.purchaseorder.dto.ItemDTO;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance;
import com.novabase.omni.oney.apps.purchaseorder.manager.ItemManager;

import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyMethod;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyReference;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.session.JourneySession;
import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;

@Component
@RSProvider(value=AppProperties.SERVICE_ADDRESS_ITEM_CONTROLLER) 
@RestController
@RequestMapping(AppProperties.ADDRESS_ITEM_CONTROLLER)
public class ItemController extends AbstractAppController implements ItemResource {

    @ServiceReference
    private ItemManager itemManager;

    @OwnerAction
    @RequestMapping(path = "/", method = RequestMethod.POST)
    @JourneyMethod(value = "Add Item")
    @RequiresPermissions(AppProperties.PERMISSION_UPDATE)
    public ItemDTO addItem(@PathVariable @JourneyReference long instanceId, @RequestBody ItemDTO item) {

	PurchaseOrderInstance instance = JourneySession.getInstance(this);
	ItemDTO newItem = itemManager.addItem(instance, item);
	
	instance.refreshTargets();
	
	return newItem;
    }

    @OwnerAction
    @RequestMapping(path = "/{itemIndex}", method = RequestMethod.PUT)
    @JourneyMethod(value = "Update Item")
    @RequiresPermissions(AppProperties.PERMISSION_UPDATE)
    public void updateItem(@PathVariable @JourneyReference long instanceId, @PathVariable int itemIndex, @RequestBody ItemDTO item) {

	PurchaseOrderInstance instance = JourneySession.getInstance(this);
	itemManager.updateItem(instance, itemIndex, item);
	
	instance.refreshTargets();
    }

    @OwnerAction
    @RequestMapping(path = "/{itemIndex}", method = RequestMethod.DELETE)
    @JourneyMethod(value = "Remove Item")
    @RequiresPermissions(AppProperties.PERMISSION_UPDATE)
    public void removeItem(@PathVariable @JourneyReference long instanceId, @PathVariable int itemIndex) {

	PurchaseOrderInstance instance = JourneySession.getInstance(this);
	itemManager.removeItem(instance, itemIndex);
	
	instance.refreshTargets();
    }

    @OwnerAction
    @RequestMapping(path = "/all", method = RequestMethod.DELETE)
    @JourneyMethod(value = "Remove Item")
    @RequiresPermissions(AppProperties.PERMISSION_UPDATE)
    public void removeAllItems(@PathVariable @JourneyReference  long instanceId) {
	
	PurchaseOrderInstance instance = JourneySession.getInstance(this);
	itemManager.removeAllItem(instance);
	
	instance.refreshTargets();
    }

}
