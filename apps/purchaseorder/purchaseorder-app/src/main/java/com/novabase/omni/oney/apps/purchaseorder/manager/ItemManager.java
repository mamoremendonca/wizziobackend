package com.novabase.omni.oney.apps.purchaseorder.manager;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.purchaseorder.AppContext;
import com.novabase.omni.oney.apps.purchaseorder.dto.ItemDTO;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.Item;
import com.novabase.omni.oney.apps.purchaseorder.mapper.PurchaseOrderInstanceMapper;

@Component(service = { ItemManager.class })
public class ItemManager {

    @Reference
    private AppContext ctx;

    public ItemDTO addItem(PurchaseOrderInstance instance, ItemDTO itemDTO) {
	validateItem(itemDTO);

	Item newItem = PurchaseOrderInstanceMapper.INSTANCE.toItem(itemDTO);

	if (instance.items == null) {
	    instance.items = new ArrayList<>();
	}

	//@formatter:off
	Item lastItemById = instance.items
		.stream()
			.max((it1, it2) -> it1.index - it2.index)
			.orElse(null);
	
	newItem.index = 
		lastItemById != null 
			? lastItemById.index + 1 
			: 1;
	
	
	instance.items.add(newItem);
	
	instance.refreshTotal();
	//@formatter:on

	return PurchaseOrderInstanceMapper.INSTANCE.toItemDTO(newItem);
    }

    public void updateItem(PurchaseOrderInstance instance, int index, ItemDTO itemDTO) {
	validateItem(itemDTO);

	Item instanceItem = getItemByIndex(instance, index);
	PurchaseOrderInstanceMapper.INSTANCE.mergeItemDTO(instanceItem, itemDTO);

	instance.refreshTotal();
    }

    public void removeItem(PurchaseOrderInstance instance, int index) {

	Item instanceItem = getItemByIndex(instance, index);
	instance.items.remove(instanceItem);

	instance.refreshTotal();
    }
    
    public void removeAllItem(PurchaseOrderInstance instance) {
   	instance.items = new ArrayList<>();

   	instance.refreshTotal();
       }

    // TODO: create rules to validate Item and remove this code from here.
    // TODO: validate all fields and then return a list with all of then
    private void validateItem(ItemDTO itemDTO) {
	if (itemDTO == null) {
	    throw ctx.fieldValidationException("item");
	}

	if (StringUtils.isBlank(itemDTO.code)) {
	    throw ctx.fieldValidationException("code");
	}

	if (StringUtils.isBlank(itemDTO.description)) {
	    throw ctx.fieldValidationException("description");
	}

	if (StringUtils.isBlank(itemDTO.taxCode)) {
	    throw ctx.fieldValidationException("taxCode");
	}

	if (itemDTO.amount <= 0) {
	    throw ctx.fieldValidationException("amount");
	}

	if (itemDTO.unitPriceWithoutTax == null || itemDTO.unitPriceWithoutTax <= 0d) {
	    throw ctx.fieldValidationException("unitPriceWithoutTax");
	}
    }

    private Item getItemByIndex(PurchaseOrderInstance instance, int index) {

	if (instance.items == null) {
	    throw ctx.itemNotFoundException(index, instance.getHeader().instanceId);
	}
	//@formatter:off
	Item instanceItem = 
		instance.items
			.stream()
				.filter(item -> item.index == index)
				.findFirst()
				.orElse(null);
	//@formatter:on
	if (instanceItem == null) {
	    throw ctx.itemNotFoundException(index, instance.getHeader().instanceId);
	}

	return instanceItem;
    }

}
