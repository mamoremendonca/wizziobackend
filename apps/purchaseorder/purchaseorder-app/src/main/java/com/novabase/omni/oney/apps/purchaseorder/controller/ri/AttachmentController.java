package com.novabase.omni.oney.apps.purchaseorder.controller.ri;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.eclipse.gemini.blueprint.extensions.annotation.ServiceReference;
import org.osgi.service.component.annotations.Component;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;
import com.novabase.omni.oney.apps.purchaseorder.controller.api.AttachmentResource;
import com.novabase.omni.oney.apps.purchaseorder.dto.AttachmentDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.FileDTO;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance;
import com.novabase.omni.oney.apps.purchaseorder.enums.DocumentClassType;
import com.novabase.omni.oney.apps.purchaseorder.manager.AttachmentManager;

import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyMethod;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyReference;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.session.JourneySession;
import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;

@Component
@RSProvider(value = AppProperties.SERVICE_ADDRESS_ATTACHMENT_CONTROLLER)
@RestController
@RequestMapping(AppProperties.ADDRESS_ATTACHMENT_CONTROLLER)
public class AttachmentController extends AbstractAppController implements AttachmentResource {

    @ServiceReference
    private AttachmentManager attachmentManager;

    @RequestMapping(path = "/", method = RequestMethod.POST)
    @JourneyMethod(value = "Add Attachment")
    @RequiresPermissions(AppProperties.PERMISSION_UPDATE)
    public AttachmentDTO uploadAttachment(@PathVariable @JourneyReference long instanceId, @QueryParam("type") DocumentClassType type, MultipartFile attachment) {

	type = DocumentClassType.PURCHASE_ORDER; //Only have one possible type for purchaseOrder attachments
	
	PurchaseOrderInstance instance = JourneySession.getInstance(this);
	InputStream inputStream = null;
	try {
	    inputStream = attachment.getInputStream();
	} catch (IOException e) {
	    throw getCtx().attachmentFailedReadStreamException(instanceId);
	}
	//@formatter:off
	return attachmentManager.saveAttachment(
		instance, 
		inputStream, 
		attachment.getOriginalFilename(), 
		attachment.getContentType(),  
		type, 
		attachment.getSize(),
		currentLoggedUserId());
	//@formatter:on
    }

    @RequestMapping(path = "/{attachmentIndex}", method = RequestMethod.GET 
//    		,produces = MediaType.APPLICATION_OCTET_STREAM TODO: uncomment this, we have a issue with cors when the produces is octet stream. Validate with Wizzio product team how to solve it.
    		)
    @JourneyMethod(value = "Get Attachment")
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    public byte[] getAttachment(@PathVariable @JourneyReference long instanceId, @PathVariable int attachmentIndex, HttpServletResponse response) {

	PurchaseOrderInstance instance = JourneySession.getInstance(this);

	FileDTO fileDTO = attachmentManager.getAttachment(instance, attachmentIndex);

	response.addHeader("Content-Disposition", "attachment; filename=" + fileDTO.fileName);
	response.setContentType(fileDTO.mimeType);

	byte[] byteArray = null;
	try {
	    byteArray = StreamUtils.copyToByteArray(fileDTO.inputStream);
	} catch (IOException e) {
	    throw getCtx().attachmentFailedReadStreamException(instanceId);
	}

	return byteArray;
    }

    @RequestMapping(path = "/{attachmentIndex}", method = RequestMethod.DELETE)
    @JourneyMethod(value = "Add Attachment")
    @RequiresPermissions(AppProperties.PERMISSION_UPDATE)
    public void removeAttachment(@PathVariable @JourneyReference long instanceId, @PathVariable int attachmentIndex) {

	PurchaseOrderInstance instance = JourneySession.getInstance(this);
	attachmentManager.removeAttachment(instance, attachmentIndex, currentLoggedUserId());
    }

}
