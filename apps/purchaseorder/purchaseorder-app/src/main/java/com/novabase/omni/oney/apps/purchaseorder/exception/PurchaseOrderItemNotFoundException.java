package com.novabase.omni.oney.apps.purchaseorder.exception;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderItemNotFoundException extends PurchaseOrderException {

    private static final long serialVersionUID = 9009094786557873837L;

    public PurchaseOrderItemNotFoundException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.NOT_FOUND, errorCode, ctx, args);
    }

    public static PurchaseOrderItemNotFoundException of(Context ctx, int index, Long instanceId) {
	return new PurchaseOrderItemNotFoundException(AppProperties.ITEM_NOT_FOUND_EXCEPTION, ctx, index, instanceId);
    }

}
