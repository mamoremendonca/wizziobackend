package com.novabase.omni.oney.apps.purchaseorder.manager;

import java.util.Properties;

import org.apache.cxf.common.util.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;
import com.novabase.omni.oney.apps.purchaseorder.agent.core.CoreAgentImpl;

import io.digitaljourney.platform.modules.cache.api.Cache;

@Component(service = { CoreManager.class })
public class CoreManager {

	private static final String REFRESH_TOKEN_CACHE_REF_NAME = "refreshTokenCache"; // TODO; validar se o nome tem que
																					// ser refreshtoken

	@Reference(name = REFRESH_TOKEN_CACHE_REF_NAME, target = "(cacheName=" + AppProperties.CACHE_USER_PROPERTIES + ")")
	private volatile Cache cache;

	@Reference
	private CoreAgentImpl coreAgent;

	public void createJourney() {
		coreAgent.createBlueprint(AppProperties.JOURNEY_NAME, AppProperties.JOURNEY_VERSION, "/purchaseorder.json");
	}

	public String getUserGroup(String username) {

		if (StringUtils.isEmpty(username))
			return "";

		// Retrieve (if exists) User Properties
		Properties props = getProperties(username);

		// Returns Group if found
		return (props != null && !props.isEmpty() && props.containsKey(AppProperties.CACHE_KEY_GROUP)
				? (String) props.get(AppProperties.CACHE_KEY_GROUP)
				: null);
		// (GroupDTO)props.get(AppProperties.CACHE_KEY_GROUP) : null);
	}

	private Properties getProperties(String username) {
		Properties props = cache.get(username, Properties.class);
		return (props != null ? props : new Properties());
	}
}
