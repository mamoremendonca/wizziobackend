package com.novabase.omni.oney.apps.purchaseorder.enums;

public enum WorkFlowActionType {

    //@formatter:off
    APPROVE, 
    RETURN, 
    REJECT, 
    CANCEL;
    //@formatter:on

}
