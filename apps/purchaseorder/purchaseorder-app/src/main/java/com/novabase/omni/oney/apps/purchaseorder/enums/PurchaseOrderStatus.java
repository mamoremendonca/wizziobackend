package com.novabase.omni.oney.apps.purchaseorder.enums;

import java.util.Arrays;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.PurchaseOrderStatusMs;

public enum PurchaseOrderStatus {

    //@formatter:off
    IN_APPROVAL(PurchaseOrderStatusMs.IN_APPROVAL), 
    APPROVED(PurchaseOrderStatusMs.APPROVED), 
    REJECTED(PurchaseOrderStatusMs.REJECTED), 
    CANCELED(PurchaseOrderStatusMs.CANCELED),
    SAP_SUBMITTED(PurchaseOrderStatusMs.SAP_SUBMITTED);
    //@formatter:on

    private PurchaseOrderStatusMs msType;

    private PurchaseOrderStatus(PurchaseOrderStatusMs msType) {
	this.msType = msType;
    }

    public PurchaseOrderStatusMs getMsType() {
	return msType;
    }
    
    //@formatter:off
    public static PurchaseOrderStatus valueOf(PurchaseOrderStatusMs msType){
	return Arrays.asList(PurchaseOrderStatus.values())
		.stream()
		.filter(type -> type.getMsType().equals(msType))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on

}
