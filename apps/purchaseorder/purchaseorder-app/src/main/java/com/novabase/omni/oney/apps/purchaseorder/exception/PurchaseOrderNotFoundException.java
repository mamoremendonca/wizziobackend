package com.novabase.omni.oney.apps.purchaseorder.exception;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderNotFoundException extends PurchaseOrderException {

    private static final long serialVersionUID = 2496875620154392002L;

    public PurchaseOrderNotFoundException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.NOT_FOUND, errorCode, ctx, args);
    }

    public static PurchaseOrderNotFoundException of(Context ctx, Long id) {
	return new PurchaseOrderNotFoundException(AppProperties.NOT_FOUND_EXCEPTION, ctx, id);
    }

}
