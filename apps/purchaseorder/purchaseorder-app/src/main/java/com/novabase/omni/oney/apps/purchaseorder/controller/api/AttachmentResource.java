package com.novabase.omni.oney.apps.purchaseorder.controller.api;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.web.multipart.MultipartFile;

import com.novabase.omni.oney.apps.purchaseorder.AppProperties;
import com.novabase.omni.oney.apps.purchaseorder.dto.AttachmentDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderDTO;
import com.novabase.omni.oney.apps.purchaseorder.enums.DocumentClassType;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderAttachmentNotFoundException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderFieldValidationException;

import io.digitaljourney.platform.modules.commons.type.HttpStatusCode;
import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.ApiKeyAuthDefinition.ApiKeyLocation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.BasicAuthDefinition;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;

//@formatter:off
@Path(AppProperties.SERVICE_ADDRESS_ATTACHMENT_CONTROLLER)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@SwaggerDefinition(
	securityDefinition = @SecurityDefinition(
		basicAuthDefinitions = {
			@BasicAuthDefinition(key = RSProperties.SWAGGER_BASIC_AUTH),
		},
		apiKeyAuthDefinitions = {
			@ApiKeyAuthDefinition(
				key = RSProperties.SWAGGER_BEARER_AUTH,
				name = RSProperties.HTTP_HEADER_API_KEY,
				in = ApiKeyLocation.HEADER)
			}
	),
	schemes = {
		SwaggerDefinition.Scheme.HTTP,
		SwaggerDefinition.Scheme.HTTPS,
		SwaggerDefinition.Scheme.DEFAULT
	},
	info = @Info(
		title = "Purchase Orders Attachments API",
		description = "The Purchase Orders Attachments API.",
		version = AppProperties.CURRENT_VERSION,
		license = @License(name = "Digital Journey License", url = "http://www.digitaljourney.io/license")
	),
	basePath = "bin/mvc.do/"+AppProperties.SERVICE_ADDRESS_ATTACHMENT_CONTROLLER
)
@Api(
	value = "Purchase Orders Attachments API",
	authorizations = {
		@Authorization(value = RSProperties.SWAGGER_BASIC_AUTH),
		@Authorization(value = RSProperties.SWAGGER_BEARER_AUTH)
	}
)
@ApiResponses(
	value = {
		@ApiResponse(code = HttpStatusCode.UNAUTHORIZED_CODE, message = RSProperties.SWAGGER_UNAUTHORIZED_MESSAGE),
		@ApiResponse(code = HttpStatusCode.FORBIDDEN_CODE, message = RSProperties.SWAGGER_FORBIDDEN_MESSAGE)
	}
)
public interface AttachmentResource {
    
    @POST
    @Path("/")
    @ApiOperation(value = "Upload an Attachment", notes = "Upload an Attachment and save the reference on the process", response = PurchaseOrderDTO.class)
    @ApiResponses(value = { 
	    @ApiResponse(code = 200, response = PurchaseOrderDTO.class, message = "OK"),
	    @ApiResponse(code = 400, response = PurchaseOrderFieldValidationException.class, message = "Bad Request"),
	    @ApiResponse(code = 500, response = PurchaseOrderException.class, message = "Internal Server Error") })
    public AttachmentDTO uploadAttachment(
	    @ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") long instanceId,
	    @ApiParam(value = "The attachment type", required = false) @QueryParam("type") DocumentClassType type,
	    @ApiParam(value = "The attachment", required = true) MultipartFile attachment);
    
    @GET
    @Path("/{attachmentIndex}")
    @ApiOperation(value = "Get File ", notes = "Get file byte[] from blob")
    @ApiResponses(value = { 
	    @ApiResponse(code = 200, message = "OK", response = byte.class, responseContainer = "Array"),
	    @ApiResponse(code = 400, response = PurchaseOrderAttachmentNotFoundException.class, message = "Bad Request"),
	    @ApiResponse(code = 500, response = PurchaseOrderException.class, message = "Internal Server Error") })
    public byte[] getAttachment(  
	    @ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") long instanceId,
	    @ApiParam(value = "The attachment index", required = true) @PathParam("attachmentIndex") int attachmentIndex,
	    HttpServletResponse response);
    
    @DELETE
    @Path("/{attachmentIndex}")
    @ApiOperation(value = "Remove an Attachment", notes = "Remove a file from blob and remove the reference on the process", response = PurchaseOrderDTO.class)
    @ApiResponses(value = { 
	    @ApiResponse(code = 200, message = "OK"),
	    @ApiResponse(code = 400, response = PurchaseOrderFieldValidationException.class, message = "Bad Request"),
	    @ApiResponse(code = 500, response = PurchaseOrderException.class, message = "Internal Server Error") })
    public void removeAttachment(
	    @ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") long instanceId,
	    @ApiParam(value = "The attachment index", required = true) @PathParam("attachmentIndex") int attachmentIndex);

}
