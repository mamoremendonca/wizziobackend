package com.novabase.omni.oney.apps.purchaseorder.enums;

import java.util.Arrays;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.PurchaseOrderTypeMs;

public enum PurchaseOrderType {
    
    //@formatter:off
    CAPEX(PurchaseOrderTypeMs.CAPEX), 
    OPEX(PurchaseOrderTypeMs.OPEX), 
    CONTRACT(PurchaseOrderTypeMs.CONTRACT);
    //@formatter:on
    
    private PurchaseOrderTypeMs msType;

    private PurchaseOrderType(PurchaseOrderTypeMs msType) {
	this.msType = msType;
    }

    public PurchaseOrderTypeMs getMsType() {
	return msType;
    }
    
    //@formatter:off
    public static PurchaseOrderType valueOf(PurchaseOrderTypeMs msType){
	return Arrays.asList(PurchaseOrderType.values())
		.stream()
		.filter(type -> type.getMsType().equals(msType))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on

}
