package com.novabase.omni.oney.apps.purchaseorder.dto;


import java.time.LocalDateTime;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.purchaseorder.enums.PurchaseOrderStatus;
import com.novabase.omni.oney.apps.purchaseorder.enums.PurchaseOrderType;
import com.novabase.omni.oney.apps.purchaseorder.json.serializer.NBLocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class PurchaseOrderHeaderDTO extends DTO {

    @ApiModelProperty(readOnly = true)
    public Long repositoryId;

    @ApiModelProperty
    public PurchaseOrderStatus repositoryStatus;
    @ApiModelProperty(readOnly = true)
    public Long number;
    @ApiModelProperty(readOnly = true)
    public String friendlyNumber;
    @ApiModelProperty(readOnly = true)
    public Long version;
    @ApiModelProperty
    public PurchaseOrderType type;
    @ApiModelProperty
    public String supplierCode;
    @ApiModelProperty
    public String description;
    @ApiModelProperty
    public String departmentCode;
    @ApiModelProperty
    public String projectCode;
    @ApiModelProperty
    public String internalOrderCode;
    @ApiModelProperty(readOnly = true)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    public LocalDateTime creationDate;
    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    public LocalDateTime expirationDate;
    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    public LocalDateTime approvalDate; //TODO: change to finish date
    @ApiModelProperty
    public Double totalWithoutTax;
    @ApiModelProperty
    public String userId;
    @ApiModelProperty
    public String userGroup;
    @ApiModelProperty
    public String currentUserGroup;
}
