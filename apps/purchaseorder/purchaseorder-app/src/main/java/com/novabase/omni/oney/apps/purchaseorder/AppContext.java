/*-
 * #%L
 * Apps :: Purchase Order App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.purchaseorder;

import java.util.Date;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.apps.purchaseorder.agent.core.CoreAgentConfig;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderAttachmentFailedReadStreamException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderAttachmentNotFoundException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderCouldNotClaimException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderCouldNotRemoveAttachmentException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderFailedToRemoveBlobException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderFailedToRetrieveBlobException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderFailedToRetrieveFileECMServiceException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderFailedToUploadBlobException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderFieldValidationException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderItemNotFoundException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderNotFoundException;
import com.novabase.omni.oney.apps.purchaseorder.exception.PurchaseOrderUserCannotExecuteActionException;

import io.digitaljourney.platform.modules.async.api.PlatformAsyncManager;
import io.digitaljourney.platform.modules.cache.api.PlatformCacheManager;
import io.digitaljourney.platform.modules.commons.annotation.DocumentationResource;
import io.digitaljourney.platform.modules.invocation.api.PlatformInvocationManager;
import io.digitaljourney.platform.modules.invocation.api.model.EventLog;
import io.digitaljourney.platform.modules.invocation.api.model.type.StatusType;
import io.digitaljourney.platform.modules.security.api.PlatformSecurityManager;
import io.digitaljourney.platform.modules.security.api.SystemSecurityManager;

import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.api.JourneyWorkflowEngineResource;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.context.AbstractJourneyContext;

/**
 * Purchase Order App implementation of an {@link AbstractMVCContext Journey
 * Context}.
 * 
 */
//@formatter:off
@Component(service = { Object.class, AppContext.class }, reference = {
		@Reference(name = AppProperties.REF_JOURNEY_ENGINE, service = JourneyWorkflowEngineResource.class, cardinality = ReferenceCardinality.MANDATORY),
		@Reference(name = AppProperties.REF_PLATFORM_SECURITY_MANAGER, service = PlatformSecurityManager.class, policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL),
		@Reference(name = AppProperties.REF_SYSTEM_SECURITY_MANAGER, service = SystemSecurityManager.class, cardinality = ReferenceCardinality.MANDATORY),
		@Reference(name = AppProperties.REF_PLATFORM_INVOCATION_MANAGER, service = PlatformInvocationManager.class, cardinality = ReferenceCardinality.MANDATORY),
		@Reference(name = AppProperties.REF_ASYNC_MANAGER, service = PlatformAsyncManager.class, cardinality = ReferenceCardinality.MANDATORY),
		@Reference(name = AppProperties.REF_CACHE_MANAGER, service = PlatformCacheManager.class, cardinality = ReferenceCardinality.MANDATORY) })
@DocumentationResource(AppProperties.DOCS_ADDRESS)
@Designate(ocd = CoreAgentConfig.class)
//@formatter:on
public class AppContext extends AbstractJourneyContext {

    /**
     * Method called whenever the component is activated.
     *
     * @param ctx Component context
     */
    @Activate
    public void activate(ComponentContext ctx) {
	prepare(ctx);
    }

    public void logError(String message) {
	EventLog event = new EventLog();
	event.setEvent_message(message);
	event.setStart_event_time(new Date());
	getPlatformInvocationManager().logNewEvent(event, null, StatusType.ERROR);
	getLogger().error(message);
    }

    public void logInfo(String message) {
	EventLog event = new EventLog();
	event.setEvent_message(message);
	event.setStart_event_time(new Date());
	getPlatformInvocationManager().logNewEvent(event, null, StatusType.SUCCESS);
	getLogger().info(message);
    }

    /**
     * Creates a new Purchase Order Exception (500 - Internal Server Error) with the
     * given error message.
     *
     * @param message Error message
     * @return Created exception
     */
    public PurchaseOrderException exception(String message) {
	return PurchaseOrderException.of(this, message);
    }

    /**
     * Creates a new Purchase Order Exception (500 - Internal Server Error) with the
     * given error cause.
     *
     * @param cause Error cause
     * @return Created exception
     */
    public PurchaseOrderException exception(Throwable cause) {
	return PurchaseOrderException.of(this, cause);
    }

    public PurchaseOrderFieldValidationException fieldValidationException(String field) {
	return PurchaseOrderFieldValidationException.of(this, field);
    }

    public PurchaseOrderNotFoundException purchaseOrderNotFoundException(Long purchaseOrderId) {
	return PurchaseOrderNotFoundException.of(this, purchaseOrderId);
    }

    public PurchaseOrderItemNotFoundException itemNotFoundException(int index, Long instanceId) {
	return PurchaseOrderItemNotFoundException.of(this, index, instanceId);
    }

    public PurchaseOrderAttachmentNotFoundException attachmentNotFoundException(int attachmentIndex, Long instanceId) {
	return PurchaseOrderAttachmentNotFoundException.of(this, attachmentIndex, instanceId);
    }

    public PurchaseOrderAttachmentFailedReadStreamException attachmentFailedReadStreamException(Long instanceId) {
	return PurchaseOrderAttachmentFailedReadStreamException.of(this, instanceId);
    }

    public PurchaseOrderFailedToRetrieveBlobException failedToRetrieveBlobException(int attachmentIndex, Long instanceId, String message) {
	return PurchaseOrderFailedToRetrieveBlobException.of(this, attachmentIndex, instanceId, message);
    }

    public PurchaseOrderFailedToRemoveBlobException failedToRemoveBlobException(int attachmentIndex, Long instanceId, String message) {
	return PurchaseOrderFailedToRemoveBlobException.of(this, attachmentIndex, instanceId, message);
    }

    public PurchaseOrderFailedToUploadBlobException failedToUploadBlobException(Long instanceId, String message) {
	return PurchaseOrderFailedToUploadBlobException.of(this, instanceId, message);
    }

    public PurchaseOrderCouldNotRemoveAttachmentException user_couldNotRemoveAttachmentException(String userName, int attachmentIndex, Long instanceId) {
	return PurchaseOrderCouldNotRemoveAttachmentException.of(this, userName, attachmentIndex, instanceId);
    }

    public PurchaseOrderCouldNotRemoveAttachmentException step_couldNotRemoveAttachmentException(String userName, int attachmentIndex, Long instanceId) {
	return PurchaseOrderCouldNotRemoveAttachmentException.ofStep(this, userName, attachmentIndex, instanceId);
    }

    public PurchaseOrderUserCannotExecuteActionException userCannotExecuteActionException(Long instanceId, String userName, String action) {
	return PurchaseOrderUserCannotExecuteActionException.of(this, instanceId, userName, action);
    }

    public PurchaseOrderUserCannotExecuteActionException userCannotExecuteActionException(Long instanceId, String userName, String action, String instanceCurrentUserGroup,
	    String sessionCurrentUserGroup) {
	return PurchaseOrderUserCannotExecuteActionException.of(this, instanceId, userName, action, instanceCurrentUserGroup, sessionCurrentUserGroup);
    }

    public PurchaseOrderCouldNotClaimException couldNotClaimException(Long instanceId, String userGroup) {
	return PurchaseOrderCouldNotClaimException.of(this, instanceId, userGroup);
    }

    public PurchaseOrderFailedToRetrieveFileECMServiceException failedToRetrieveFileECMException(final int attachmentIndex, final Long purchaseOrderId, final String message) {
	return PurchaseOrderFailedToRetrieveFileECMServiceException.of(this, attachmentIndex, purchaseOrderId, message);
    }

}
