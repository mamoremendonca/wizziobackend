package com.novabase.omni.oney.apps.purchaseorder.dto;

import java.time.LocalDateTime;
import java.util.List;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.purchaseorder.enums.HistoryType;
import com.novabase.omni.oney.apps.purchaseorder.json.serializer.NBLocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class HistoryDTO extends DTO {

    @ApiModelProperty(readOnly = true)
    public int index;
    @ApiModelProperty(readOnly = true)
    public HistoryType type;
    @ApiModelProperty(readOnly = true)
    public String userCompleteName;
    @ApiModelProperty(readOnly = true)
    public String group;
    @ApiModelProperty(readOnly = true)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    public LocalDateTime date;
    @ApiModelProperty(readOnly = true)
    public String comment;
    @ApiModelProperty(readOnly = true)
    public List<AttachmentDTO> attachments;
    
    /**
     * Used to find the ownerName, this attribute is not visible on the response
     */
    @ApiModelProperty(hidden =  true)
    public transient Long userId;
    
    
    
}
