package com.novabase.omni.oney.apps.purchaseorder.dto;

import java.io.Serializable;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class GroupDTO extends DTO implements Serializable {

    private static final long serialVersionUID = -1668414747064146951L;

    public GroupDTO() {
	this.name = "";
	this.description = "";
    };
    
    public GroupDTO(String name, String description) {
	this.name = name;
	this.description = description;
    }
    
    @ApiModelProperty
    public String name;

    @ApiModelProperty
    public String description;
}
