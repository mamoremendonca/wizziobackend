/*-
 * #%L
 * Apps :: Purchase Order App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.purchaseorder.agent.purchaseorderrepository;

import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.apps.purchaseorder.AppContext;
import com.novabase.omni.oney.apps.purchaseorder.AppProperties;
import com.novabase.omni.oney.apps.purchaseorder.dto.AttachmentRepositoryDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderHeaderDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.TimeLineStepDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.PurchaseOrderRepositoryDTO;
import com.novabase.omni.oney.apps.purchaseorder.dto.WorkflowDTO;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.Attachment;
import com.novabase.omni.oney.apps.purchaseorder.entity.PurchaseOrderInstance.CurrentAction;
import com.novabase.omni.oney.apps.purchaseorder.mapper.PurchaseOrderRepositoryMapper;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.PurchaseOrderRepositoryResource;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.PurchaseOrderMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.SearchCriteriaMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.WorkflowActionMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.WorkflowMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.WorkFlowActionTypeMs;

import io.digitaljourney.platform.modules.commons.type.tuple.Pair;
import io.digitaljourney.platform.modules.mvc.api.agent.AbstractCoreAgent;

/**
 * Implementation of a Core Agent to communicate with micro-service.
 *
 */
// @formatter:off
@Component(service = { Object.class,
		PurchaseOrderRepositoryAgentImpl.class }, configurationPid = PurchaseOrderRepositoryAgentConfig.CPID, configurationPolicy = ConfigurationPolicy.REQUIRE, reference = {
				@Reference(name = AppProperties.REF_CONTEXT, service = AppContext.class, cardinality = ReferenceCardinality.MANDATORY) })
@Designate(ocd = PurchaseOrderRepositoryAgentConfig.class)
// @formatter:on
public class PurchaseOrderRepositoryAgentImpl
		extends AbstractCoreAgent<AppContext, PurchaseOrderRepositoryAgentConfig> {

	@Reference
	private PurchaseOrderRepositoryResource purchaseOrderRepositoryResource;

	/**
	 * Method called whenever the component is activated.
	 *
	 * @param ctx    Component context
	 * @param config Component configuration
	 */
	@Activate
	public void activate(ComponentContext ctx, PurchaseOrderRepositoryAgentConfig config) {
		prepare(ctx, config);
	}

	/**
	 * Method called whenever the component configuration changes.
	 *
	 * @param config Component configuration
	 */
	@Modified
	public void modified(PurchaseOrderRepositoryAgentConfig config) {
		prepare(config);
	}

	public Pair<PurchaseOrderHeaderDTO, WorkflowDTO> submitPurchaseOrderToRepository(PurchaseOrderInstance instance,
			WorkflowDTO workflow) {

		PurchaseOrderMsDTO purchaseOrderDTO = PurchaseOrderRepositoryMapper.INSTANCE.toPurchaseOrderMsDTO(instance,
				workflow);

		purchaseOrderDTO = purchaseOrderRepositoryResource.addPurchaseOrder(purchaseOrderDTO);
		// @formatter:off
		return new Pair<PurchaseOrderHeaderDTO, WorkflowDTO>(
				PurchaseOrderRepositoryMapper.INSTANCE.toPurchaseOrderHeaderDTO(purchaseOrderDTO),
				PurchaseOrderRepositoryMapper.INSTANCE.toWorkflowDTO(purchaseOrderDTO.workflow));
		// @formatter:on
	}

	public WorkflowDTO approvePurchaseOrder(Long purchaseOrderRepositoryId, CurrentAction action, Long userId) {
		return updateWorkFlow(purchaseOrderRepositoryId, action, userId, WorkFlowActionTypeMs.APPROVE);
	}

	public WorkflowDTO returnPurchaseOrder(Long purchaseOrderRepositoryId, CurrentAction action, Long userId) {
		return updateWorkFlow(purchaseOrderRepositoryId, action, userId, WorkFlowActionTypeMs.RETURN);
	}

	public WorkflowDTO rejectPurchaseOrder(Long purchaseOrderRepositoryId, CurrentAction action, Long userId) {
		return updateWorkFlow(purchaseOrderRepositoryId, action, userId, WorkFlowActionTypeMs.REJECT);
	}

	public WorkflowDTO cancelPurchaseOrder(Long purchaseOrderRepositoryId, CurrentAction action, Long userId) {
		return updateWorkFlow(purchaseOrderRepositoryId, action, userId, WorkFlowActionTypeMs.CANCEL);
	}

	public List<TimeLineStepDTO> getTimeLine(Long repositoryId) {
		return PurchaseOrderRepositoryMapper.INSTANCE
				.toTimeLineStepDTOList(purchaseOrderRepositoryResource.getTimeLine(repositoryId));
	}

	// TODO: when implement the Purchase Order versions we must pass here the
	// version in order to on repository return the correct version.
	public PurchaseOrderRepositoryDTO getPurchaseOrder(Long purchaseOrderId, String userGroup) {

		return PurchaseOrderRepositoryMapper.INSTANCE.toPurchaseOrderRepositoryDTO(
				purchaseOrderRepositoryResource.getPurchaseOrder(purchaseOrderId, userGroup));
	}

	public WorkflowDTO claimPurchaseOrder(Long repositoryId, Long userId, String userGroup, CurrentAction action) {

		WorkflowActionMsDTO workflowAction = null;

		if (action != null && action.attachments != null && action.attachments.isEmpty() == false) {
			workflowAction = PurchaseOrderRepositoryMapper.INSTANCE.toWorkflowActionMsDTO(action);
			workflowAction.type = WorkFlowActionTypeMs.SKIP;
		}

		WorkflowMsDTO workFlow = purchaseOrderRepositoryResource.claimProcess(repositoryId, userId, userGroup,
				workflowAction);
		return PurchaseOrderRepositoryMapper.INSTANCE.toWorkflowDTO(workFlow);
	}

	public List<PurchaseOrderHeaderDTO> search(SearchCriteriaMsDTO searchCriteria) {

		List<PurchaseOrderMsDTO> purchaseOrdersMsDTO = purchaseOrderRepositoryResource.search(searchCriteria);

		// @formatter:off
		return purchaseOrdersMsDTO.stream()
				.map(po -> PurchaseOrderRepositoryMapper.INSTANCE.toPurchaseOrderHeaderDTO(po))
				.collect(Collectors.toList());
		// @formatter:on
	}

	public List<Attachment> getPurchaseOrderAttachments(Long purchaseOrderId) {

		return PurchaseOrderRepositoryMapper.INSTANCE
				.toAttachmentList(purchaseOrderRepositoryResource.getPurchaseOrderAttachments(purchaseOrderId));
	}

	public AttachmentRepositoryDTO getPurchaseOrderAttachment(Long purchaseOrderId, int index) {
		return PurchaseOrderRepositoryMapper.INSTANCE.toAttachmentRepositoryDTO(
				this.purchaseOrderRepositoryResource.getPurchaseOrderAttachment(purchaseOrderId, index));
	}

	private WorkflowDTO updateWorkFlow(Long purchaseOrderRepositoryId, CurrentAction action, Long userId,
			WorkFlowActionTypeMs actionType) {

		WorkflowActionMsDTO workflowAction = PurchaseOrderRepositoryMapper.INSTANCE.toWorkflowActionMsDTO(action);
		workflowAction.type = actionType;
		workflowAction.userId = userId;

		WorkflowMsDTO workFlow = purchaseOrderRepositoryResource.updateWorkflow(purchaseOrderRepositoryId,
				workflowAction);

		return PurchaseOrderRepositoryMapper.INSTANCE.toWorkflowDTO(workFlow);
	}
}
