package com.novabase.omni.oney.apps.purchaseorder.provider;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.novabase.omni.oney.apps.purchaseorder.agent.reference.ReferenceAgentImpl;
import com.novabase.omni.oney.apps.purchaseorder.dto.reference.ReferenceDataDTO;
import com.novabase.omni.oney.apps.purchaseorder.enums.PurchaseOrderStatus;
import com.novabase.omni.oney.apps.purchaseorder.enums.PurchaseOrderType;
import com.novabase.omni.oney.apps.purchaseorder.mapper.ReferenceDataMapper;

public class ReferenceDataProvider {

    private ReferenceAgentImpl referenceAgent;
    
    public ReferenceDataProvider(final ReferenceAgentImpl referenceAgent) {
	this.referenceAgent = referenceAgent;
    }
    
    public List<ReferenceDataDTO> getPurchaseOrderType() {
	
	return ReferenceDataMapper.INSTANCE.toReferenceDTOList(Arrays.asList(PurchaseOrderType.values()).parallelStream().
														map(enumParam -> enumParam.name()).
														collect(Collectors.toList()));
	
    }

    public List<ReferenceDataDTO> getPurchaseOrderListStatus() {
	
	return ReferenceDataMapper.INSTANCE.toReferenceDTOList(Arrays.asList(PurchaseOrderStatus.values()).parallelStream().
														map(enumParam -> enumParam.name()).
														collect(Collectors.toList()));
	
    }
    
}
