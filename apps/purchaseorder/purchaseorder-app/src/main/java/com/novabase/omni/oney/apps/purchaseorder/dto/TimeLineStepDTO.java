package com.novabase.omni.oney.apps.purchaseorder.dto;

import java.time.LocalDateTime;
import java.util.List;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.purchaseorder.enums.TimeLineStepType;
import com.novabase.omni.oney.apps.purchaseorder.json.serializer.NBLocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("TimeLineStep")
public class TimeLineStepDTO extends DTO {

    @ApiModelProperty(readOnly = true)
    public int index;

    @ApiModelProperty(readOnly = true)
    public TimeLineStepType type;

    @ApiModelProperty(readOnly = true)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    public LocalDateTime date;

    @ApiModelProperty(readOnly = true)
    public String comment;

    public transient Long userId;
    
    @ApiModelProperty(readOnly = true)
    public String ownerName;

    @ApiModelProperty(readOnly = true)
    public String userGroup;

    @ApiModelProperty(readOnly = true)
    public boolean hasNext;
    
    @ApiModelProperty(readOnly = true)
    public boolean isCurrentStep;

    @ApiModelProperty(readOnly = true)
    public List<AttachmentDTO> attachments;
    
    public void setuserInformation(UserInformationDTO userInformationDTO) {
	this.ownerName = userInformationDTO.getCompleteName();
    }

}
