const styles = theme => ({
  root: {
    display: "flex",
    flexDirection: "row",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px 0px 0px`
  },
  tabTitleSelected: {
    color: theme.palette.primary.main,
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 5}px ${
      theme.spacing.unit
    }px  ${theme.spacing.unit * 2}px `,
    borderBottom: `2px solid ${theme.palette.primary.main}`
  },
  tabTitle: {
    color: theme.palette.text.primary,
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 5}px ${
      theme.spacing.unit
    }px  ${theme.spacing.unit * 2}px `
  },
  tabDataContainer: {
    padding: theme.spacing.unit * 2
  }
});

export default styles;
