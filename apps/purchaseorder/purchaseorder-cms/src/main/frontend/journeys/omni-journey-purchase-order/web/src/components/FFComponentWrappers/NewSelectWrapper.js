import React, { Component } from 'react';
import PropTypes from "prop-types";
import Select from "react-select";
import FormControl from "@ui-lib/core/FormControl";
import InputLabel from "@ui-lib/core/InputLabel";
import FormHelperText from "@ui-lib/core/FormHelperText";
import { withTheme } from "@ui-lib/core/styles";
import { connect } from "react-redux";

const IndicatorSeparator = ({ innerProps }) => {
    return <span {...innerProps} />;
};

const customStyles = theme => ({
    option: (provided, state) => ({

        ...provided,
        fontFamily: theme.typography.fontFamily,
        backgroundColor: state.isSelected ? theme.palette.grey[400] : "transparent",
        "&:hover": {
          backgroundColor: theme.palette.grey[400],
          cursor: "pointer",
          color: theme.palette.common.white
        }
    }),
    control: (provided, state) => {
        return state.isDisabled
            ? {
                ...provided,
                marginTop: 12,
                borderTop: 0,
                borderLeft: 0,
                borderRight: 0,
                borderBottom: "none",
                boxShadow: 0,
                backgroundColor: "transparent",
                borderRadius: 0,
                padding: "-2px -8px"
            }
            : {
                ...provided,
                marginTop: 12,
                borderTop: state.isSelected || state.isFocused ? 0 : 0,
                borderLeft: 0,
                borderRight: 0,
                borderBottom: "1px solid #8e8e8e;",
                boxShadow: state.isSelected || state.isFocused ? 0 : 0,
                backgroundColor: "transparent",
                borderRadius: 0,
                "&:hover": {
                    borderBottom: "2px solid black",
                    cursor: "pointer"
                },
                padding: "-2px -8px"
            };
    },
    dropdownIndicator: (provided, state) => ({
        ...provided,
        marginBottom: 4,
        color: state.isDisabled ? "transparent" : theme.palette.primary.main
    }),
    singleValue: provided => ({
        ...provided,
        backgroundColor: "transparent",
        color: theme.palette.text.primary,
        fontFamily: theme.typography.fontFamily
    }),
    root: provided => ({ ...provided, width: "100%" }),
    placeholder: provided => ({
        ...provided,
        fontFamily: theme.typography.fontFamily
    }),
    valueContainer: provided => ({ ...provided, padding: "-2px -8px" }),
    menu: provided => ({ ...provided, backgroundColor: theme.palette.grey[100] })
});

class NewWrapper extends Component {
    render() {

        const {
            theme,
            I18nProvider,
            inputLabelProps,
            formControlProps,
            disabled, required,
            label,
            error,
            errorMessage,
            inputKey,
            onChange,
            options,
            value,
            ...rest
        } = this.props

        return (
            <FormControl
                {...formControlProps}
                error={error}
                className={customStyles(theme).root}
            >
                <InputLabel htmlFor={inputKey}  {...inputLabelProps} shrink={true}>
                    {label} {required && !disabled ? "*" : ""}
                </InputLabel>
                <Select
                    {...rest}
                    name={inputKey}
                    onChange={onChange}
                    value={value}
                    styles={customStyles(theme)}
                    components={{ IndicatorSeparator }}
                    placeholder={disabled ? "-" : I18nProvider.Labeli18n('select_component.placeholder.label')}
                    isDisabled={disabled}
                    isClearable={true}
                    options={options}
                    noOptionsMessage={() => I18nProvider.Labeli18n('select_component.no_matches.label')}
                />
                {error && (
                    <FormHelperText>{errorMessage}</FormHelperText>
                )}
            </FormControl>
        );
    }
}

NewWrapper.defaultProps = {
    required: false,
    disabled: false,
    error: false,
}
//formControlProps={{fullWidth:true}}
NewWrapper.propTypes = {
    label: PropTypes.string,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    formControlProps: PropTypes.shape({
        fullWidth: PropTypes.bool
    }),
    inputLabelProps: PropTypes.object,
    error: PropTypes.bool,
    errorMessage: PropTypes.node,
    onChange: PropTypes.func.isRequired,
    inputKey: PropTypes.string,
    options: PropTypes.array
    
};

const mapStateToProps = ({
    journey: { services: { I18nProvider } }, i18n
}) => ({
    I18nProvider, i18n
});

export default connect(mapStateToProps)(withTheme()(NewWrapper));;