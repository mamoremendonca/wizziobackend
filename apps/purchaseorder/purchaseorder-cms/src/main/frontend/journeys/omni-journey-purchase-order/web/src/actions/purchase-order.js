import * as constants from "../constants";
import { Config } from "../config";
import {
  handleErrorRequest,
  ErrorMessages,
  handleSuccessNotificationRequest,
} from "../utils/notificationHandler";

export const setPurchaseOrder = (purchaseOrder) => {
  return {
    type: constants.SET_PURCHASE_ORDER,
    payload: purchaseOrder,
  };
};

export const clearPurchaseOrder = () => {
  return {
    type: constants.CLEAR_PURCHASE_ORDER,
  };
};

export const createPurchaseOrder = (services, data) => {
  const { Notifications, HttpClient } = services;
  let config = {
    method: Config.METHOD_POST,
    url: Config.URL_PURCHASE_ORDER,
    data: { ...data },
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        dispatch(setPurchaseOrder(res.data));
      })
      .catch(() => {
        handleErrorRequest(Notifications, ErrorMessages.PO_CREATE_ERROR);
      });
  };
};

export const createPurchaseOrderSuccess = (payload) => {
  return {
    type: constants.CREATE_PURCHASE_ORDER_SUCESS,
    payload,
  };
};

export const createPurchaseOrderError = (error) => {
  return {
    type: constants.CREATE_PURCHASE_ORDER_ERROR,
    payload: error,
  };
};

export const updatePurchaseOrder = (HttpClient, body) => {
  const { purchaseOrderNumber } = body;
  // Update the PO only if there is a purchaseOrderNumber
  if (purchaseOrderNumber) {
    let config = {
      method: Config.METHOD_PUT,
      url: `${Config.URL_PURCHASE_ORDER}${purchaseOrderNumber}`,
      data: body,
    };
    return (dispatch) => {
      return HttpClient.request(config)
        .then((res) => {
          dispatch(setPurchaseOrder(res.data));
        })
        .catch((error) => {
          dispatch(updatePurchaseOrderError(error));
        });
    };
  } else {
    return (dispatch) => {
      dispatch(updatePurchaseOrderError(constants.UPDATE_PURCHASE_ORDER_ERROR));
    };
  }
};

export const updatePurchaseOrderSucess = (response) => {
  return {
    type: constants.UPDATE_PURCHASE_ORDER_SUCCESS,
    payload: response,
  };
};

export const updatePurchaseOrderError = (error) => {
  return {
    type: constants.UPDATE_PURCHASE_ORDER_ERROR,
    payload: error,
  };
};

// When we want to add attachments to the PO we have to call this method
export const addPurchaseOrderAttachment = (
  HttpClient,
  purchaseOrderNumber,
  attachment
) => {
  const data = new FormData();
  data.append("attachment", attachment);
  let config = {
    method: Config.METHOD_POST,
    url: `${Config.URL_PURCHASE_ORDER}${purchaseOrderNumber}${Config.ATTACHMENT_URL}`,
    data,
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        dispatch(addPurchaseOrderAttachmentSucess(res.data));
      })
      .catch((error) => {
        dispatch(addPurchaseOrderAttachmentError(error));
      });
  };
};

export const addPurchaseOrderAttachmentSucess = (response) => {
  return {
    type: constants.ADD_PURCHASE_ORDER_ATTACHMENT_SUCCESS,
    payload: response,
  };
};

export const addPurchaseOrderAttachmentError = (error) => {
  return {
    type: constants.ADD_PURCHASE_ORDER_ATTACHMENT_ERROR,
    payload: error,
  };
};

// When we want delete attachments from the PO we have to call this method
export const removePurchaseOrderAttachment = (
  HttpClient,
  purchaseOrderNumber,
  attachmentId
) => {
  let config = {
    method: Config.METHOD_DELETE,
    url: `${Config.URL_PURCHASE_ORDER}${purchaseOrderNumber}${Config.ATTACHMENT_URL}${attachmentId}`,
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        dispatch(removePurchaseOrderAttachmentSucess(res.data));
      })
      .catch((error) => {
        dispatch(removePurchaseOrderAttachmentError(error));
      });
  };
};

export const removePurchaseOrderAttachmentSucess = (response) => {
  return {
    type: constants.REMOVE_PURCHASE_ORDER_ATTACHMENT_SUCCESS,
    payload: response,
  };
};

export const removePurchaseOrderAttachmentError = (error) => {
  return {
    type: constants.REMOVE_PURCHASE_ORDER_ATTACHMENT_ERROR,
    payload: error,
  };
};

// When we send the PO to be aproved we have to call this method
export const submitPurchaseOrder = (services, purchaseOrderNumber, goBack) => {
  const { Notifications, HttpClient } = services;
  let config = {
    method: Config.METHOD_PUT,
    url: `${Config.URL_PURCHASE_ORDER}${purchaseOrderNumber}${Config.SUBMIT_PO_URL}`,
  };
  // return (dispatch) => {
  return HttpClient.request(config)
    .then((res) => {
      handleSuccessNotificationRequest(
        Notifications,
        "A sua Purchase Order com o nº POXXXX foi submetida para aprovação. Será notificado quando a aprovação for finalizada."
      );
      //  dispatch(submitPurchaseOrderSucess(res.data));
    })
    .catch((error) => {
      handleErrorRequest(Notifications, ErrorMessages.PO_SUBMIT_ERROR);
      //    dispatch(submitPurchaseOrderError(error));
    });
  //  };
};

export const submitPurchaseOrderSucess = (response) => {
  return {
    type: constants.SUBMIT_PURCHASE_ORDER_SUCCESS,
    payload: response,
  };
};

export const submitPurchaseOrderError = (error) => {
  return {
    type: constants.SUBMIT_PURCHASE_ORDER_ERROR,
    payload: error,
  };
};

// GET PO INFO
export const getPurchaseOrderData = (
  HttpClient,
  Notifications,
  purchaseOrderNumber
) => {
  let config = {
    method: Config.METHOD_GET,
    url: `${Config.URL_PURCHASE_ORDER}${purchaseOrderNumber}`,
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        dispatch(setPurchaseOrder(res.data));
      })
      .catch((error) => {
        handleErrorRequest(Notifications, ErrorMessages.PO_GET_ERROR);
        //    dispatch(submitPurchaseOrderError(error));
      });
  };
};

//GET COPY PO INFO

export const getCopyPurchaseOrderData = (
  HttpClient,
  Notifications,
  repositoryId,
  history
) => {
  let config = {
    method: Config.METHOD_GET,
    url: `${Config.URL_COPY_PURCHASE_ORDER}/${repositoryId}/1`,
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        history.push(`/create/${res.data.jwcontext.id}`);
        dispatch(setPurchaseOrder(res.data));
      })
      .catch((error) => {
        handleErrorRequest(Notifications, ErrorMessages.PO_GET_ERROR);
      });
  };
};

export const setPurchaseOrderList = (newPurchaseOrderList) => {
  return {
    type: constants.SET_PO_LIST_PROCESSES,
    payload: newPurchaseOrderList,
  };
};

export const setPurchaseOrdersDependencies = (dependency) => {
  return { type: constants.SET_PURCHASE_ORDER_DEPENDENCY, payload: dependency };
};

export const updateFilter = (newFilter) => ({
  type: constants.UPDATE_FILTER_CRITERIA,
  payload: newFilter,
});

export const updateSorting = (fieldToSort) => ({
  type: constants.UPDATE_SORTING_CRITERIA,
  payload: fieldToSort,
});
