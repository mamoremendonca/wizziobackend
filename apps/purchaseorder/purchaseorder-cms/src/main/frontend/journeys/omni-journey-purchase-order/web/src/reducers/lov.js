import * as constants from "../constants";

const initialState = {
  lovs: {},
  suppliers: {}
}

export const lovReducer = (state, action) => {
  if (state === undefined) return initialState;

  switch (action.type) {
    case constants.SET_LOVS: {
      return { ...state, lovs: action.payload };
    }
    case constants.SET_SUPPLIERS: {
      return { ...state, suppliers: action.payload };
    }
    default:
      return state;
  }
};
