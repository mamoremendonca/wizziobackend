import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@ui-lib/core/styles";
import Table from "@ui-lib/core/Table";
import TableBody from "@ui-lib/core/TableBody";
import TableHead from "@ui-lib/core/TableHead";
import TableRow from "@ui-lib/core/TableRow";
import Paper from "@ui-lib/core/Paper";
import styles from "./styles";
import ItemTableHeaders from "./ItemTableHeaders";
import { TOTAL_AMOUNT_WO_VAT } from "../ItemForm/constants";
import ItemTableRows from "./ItemTableRows";

const ItemsTable = ({
  classes,
  values,
  fields,
  headers,
  handleEdit,
  handleCopy,
  handleTotalValue,
  isDetails,
  type,
  allDisabledFields,
  ...props
}) => {
  handleTotalValue &&
    handleTotalValue(
      values.reduce((acc, value) => acc + value[TOTAL_AMOUNT_WO_VAT], 0)
    );
  return (
    fields && fields.length > 0 ?
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <ItemTableHeaders
              type={type}
              isDetails={isDetails}
            ></ItemTableHeaders>
          </TableRow>
        </TableHead>
        <TableBody>
          <ItemTableRows
            type={type}
            handleEdit={allDisabledFields ? undefined : handleEdit}
            handleCopy={handleCopy}
            values={values}
            isDetails={isDetails}
          ></ItemTableRows>
        </TableBody>
      </Table>
    </Paper> : <></>
  );
};

ItemsTable.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ItemsTable);
