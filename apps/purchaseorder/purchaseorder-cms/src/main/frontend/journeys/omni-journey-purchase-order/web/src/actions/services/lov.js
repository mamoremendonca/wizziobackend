import * as constants from "../../constants";
import { Config } from "../../config";
import { handleErrorRequest, ErrorMessages } from "../../utils";
import { setLovs } from "../../actions";
import { mapBackendDTO } from "../../utils";

export const getAllLovs = (services) => {
  const { Notifications, HttpClient, I18nProvider } = services;

  const oneyReferenceDataConfig = {
    method: Config.METHOD_POST,
    url: Config.ONEY_REFERENCE_LISTS_URL,
    data: constants.ONEY_REFERENCE_DATA_BODY,
  };

  const purchaseOrderReferenceDataconfig = {
    method: Config.METHOD_POST,
    url: Config.PURCHASE_ORDER_REFERENCE_LISTS_URL,
    data: constants.PURCHASE_ORDER_REFERENCE_DATA_BODY,
  };
  return (dispatch) => {
    return Promise.all([
      HttpClient.request(oneyReferenceDataConfig),
      HttpClient.request(purchaseOrderReferenceDataconfig),
    ])
      .then((res) => {
        const oneyReferenceData = res[0].data;
        const purchaserOrderReferenceData = res[1].data;
        const lovs = {};

        lovs.items = mapBackendDTO(oneyReferenceData.ITEM, I18nProvider);
        lovs.internalOrders = mapBackendDTO(
          oneyReferenceData.INTERNAL_ORDER,
          I18nProvider
        );
        lovs.projects = mapBackendDTO(oneyReferenceData.PROJECT, I18nProvider);
        lovs.assets = mapBackendDTO(oneyReferenceData.ASSET, I18nProvider);
        lovs.costCenters = mapBackendDTO(
          oneyReferenceData.COST_CENTER,
          I18nProvider
        );
        lovs.tax = mapBackendDTO(oneyReferenceData.TAX, I18nProvider);
        lovs.documentType = mapBackendDTO(
          oneyReferenceData.DOCUMENT_TYPE,
          I18nProvider
        );
        lovs.itemTypes = mapBackendDTO(
          oneyReferenceData.ITEM_TYPE,
          I18nProvider
        );
        lovs.allDepartments = mapBackendDTO(
          oneyReferenceData.DEPARTMENT,
          I18nProvider
        );

        lovs.purchaseOrderTypes = mapBackendDTO(
          purchaserOrderReferenceData.PURCHASE_ORDER_TYPE,
          I18nProvider
        );
        lovs.departments = mapBackendDTO(
          purchaserOrderReferenceData.DEPARTMENT,
          I18nProvider
        );

        lovs.status = mapBackendDTO(
          purchaserOrderReferenceData.PURCHASE_ORDER_LIST_STATUS,
          I18nProvider
        );

        dispatch(setLovs(lovs));
      })
      .catch((error) => {
        handleErrorRequest(Notifications, ErrorMessages.PO_LOADING_RESOURCES);
      });
  };
};
