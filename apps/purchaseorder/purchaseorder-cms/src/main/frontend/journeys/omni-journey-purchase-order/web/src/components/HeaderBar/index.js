import React, {Component} from "react";
import { withRouter } from "react-router";
import HeaderRender from "./HeaderBarRender";


class HeaderBar extends Component {
  
  localBackClickHandler = ev => {
    const {backClickHandler, history}= this.props;
    if (backClickHandler) {
      backClickHandler();
    } else {
      history.goBack();
    }
  };
  
  render() {
      const {children, title, hideBackButton}= this.props;
      return (
         <HeaderRender
         hideBackButton={hideBackButton}
         title={title}
         localBackClickHandler={this.localBackClickHandler}
         children = {children}
         />
      );
  }
}

export default withRouter(HeaderBar);