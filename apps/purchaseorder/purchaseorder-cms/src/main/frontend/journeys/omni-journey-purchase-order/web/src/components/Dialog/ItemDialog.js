import React from "react";
import Dialog from "@ui-lib/core/Dialog";

import DialogTitle from "@ui-lib/core/DialogTitle";
import DialogContent from "@ui-lib/core/DialogContent";
import DialogActions from "@ui-lib/core/DialogActions";
import Button from "@ui-lib/core/Button";
import Icon from "@ui-lib/core/Icon";

import { withStyles } from "@ui-lib/core/styles";
import styles from "./styles";

import * as constants from "./constants";

const ItemDialog = (props) => {
  const {
    open,
    close,
    children,
    title,
    save,
    valid,
    handleDelete,
    itemIndex,
    classes,
    action,
  } = props;
  return (
    <Dialog open={open} onClose={close} fullWidth={true} maxWidth={"l"}>
      <DialogTitle>
        <div className={classes.headerContainer}>
          <div>{title}</div>
          <Icon className={classes.Icon} onClick={close}>
            close
          </Icon>
        </div>
      </DialogTitle>
      <DialogContent style={{ overflowY: "visible" }}>{children}</DialogContent>
      <DialogActions>
        {action === constants.ITEM_DIALOG_ACTIONS.EDIT_ITEM && (
          <Button
            type="submit"
            onClick={() => {
              handleDelete(itemIndex);
              close();
            }}
          >
            Apagar
          </Button>
        )}
        <Button
          type="submit"
          variant="contained"
          color="primary"
          onClick={save}
          disabled={!valid}
        >
          Guardar
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default withStyles(styles)(ItemDialog);
