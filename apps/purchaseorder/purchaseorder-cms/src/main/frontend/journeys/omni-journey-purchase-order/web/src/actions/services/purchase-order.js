import {Config} from '../../config';

/**
 * Gets the attachment
 * @param {*} instanceId 
 * @param {*} index 
 * @param {*} services 
 * @param {*} dataSource
 * @param {*} repositoryId
 */
export const downloadAttachmentRequest = async (instanceId, index, services, dataSource) =>{

    const config = {
        method: Config.METHOD_GET,
        url: `${
          dataSource === 'process_continuity'
            ? Config.URL_PURCHASE_ORDER
            : Config.URL_PURCHASE_REPOSITORY
        }${instanceId}${Config.ATTACHMENT_URL}${index}`
      };
    
    try {
        return await services.HttpClient.request(config);
    } catch (error) {
        return '';
    }
}

/**
 * To delete all items added in the purchase order
 * In case of success no response body is sent.
 * @param {int} instanceId 
 * @param {object} services 
 */
export const clearAllItemsRequest = async (instanceId, HttpClient) => {
    const config = {
        method: Config.METHOD_DELETE,
        url: `${Config.URL_PURCHASE_ORDER}${instanceId}${Config.ITEM_ALL_URL}`
    };

    try {
        return await HttpClient.request(config);
    } catch (error) {
        return error;
	}
};

/**
 * To cancel the purchase order.
 * @param {*} HttpClient 
 * @param {*} id 
 */
export const deletePurchaseOrderRequest = async (HttpClient, id) => {
    const config = {
      method: Config.METHOD_PUT,
      url: `${Config.URL_PURCHASE_ORDER}${id}${Config.CANCEL_PO_URL}`
    };
    try {
        await HttpClient.request(config);
    } catch (error) {
        return error
    }
};
/**
 * adsfasdfhlhl lksjf lkjsf
 * @param {*} HttpClient 
 * @param {*} supplierCode 
 */
export const getSupplierDetailsRequest = (HttpClient, supplierCode) =>{
    const config = {
        method: Config.METHOD_GET,
        url: `${Config.SUPPLIER_URL}/${supplierCode}`
    }
    return  HttpClient.request(config);
}
