import React from "react";
import { connect } from "react-redux";
import { withRootHoc } from "omni-journey";
import { withStyles } from "@ui-lib/core/styles";

import { MemoryRouter as Router, Switch, Route } from "react-router-dom";
import AppRoutes from "./Routes";
import * as actions from "./actions";
import POListScreen from "./containers/POListScreen";
import CreatePOScreen from "./containers/CreatePOScreen";
import DetailsPOScreen from "./containers/DetailsPOScreen";
import styles from './styles';

class Journey extends React.Component {
	constructor(props) {
		super(props);
		this.state={
			allDependenciesResolved: false
		}
	}

	componentDidUpdate(prevProps) {
		//update current dependencies
		const {lovs, suppliers, dependencies, services:{Loading}, setPurchaseOrdersDependencies} = this.props;
		if(lovs !== prevProps.lovs && !dependencies.get('lov')){	
			setPurchaseOrdersDependencies('lov');
		}else if(suppliers !== prevProps.suppliers && !dependencies.get('suppliers')){
			setPurchaseOrdersDependencies('suppliers');
		} else if(!this.state.allDependenciesResolved && dependencies.every((v) => v)){
			Loading.request('stopLoading').then( () =>{
				//all dependencies are load so you can do the render.
				this.setState({allDependenciesResolved:true});
			});
		}
	}

	componentDidMount() {
		const {services, services:{Loading}, getAllLovs, getSuppliers} = this.props
		Loading.request('setLoading','JOURNEY').then( () =>{
			//call all network services here and update reducer properties
			getAllLovs(services);
			getSuppliers(services);
		});
				
	}

	routerInitialEntries = initialArgs => {
		//TODO: refactor fo status
		if (initialArgs && initialArgs.journey) {
			//here the purchase order came from the work list so is process continuity
			switch (initialArgs.journey.status) {
				case 'Draft':
					return [AppRoutes.DRAFT.basePath +'/'+ initialArgs.journey.instanceId];
				case 'In Approval':
					return [AppRoutes.APPROVE.basePath +'/'+ initialArgs.journey.instanceId];
				case 'Rejected':
					return [AppRoutes.CANCEL.basePath +'/'+ initialArgs.journey.instanceId];
				default:
					return ["/"];
			}
		}
		return ["/"]
	}

	//TODO: see if I need to pass the index of path route.
	render() {
		const { initialArgs } = this.props;
		return (
			<React.Fragment>
				{this.state.allDependenciesResolved && <Router initialEntries={this.routerInitialEntries(initialArgs)} initialIndex={0}>
					<Switch>
						<Route exact={AppRoutes.HOME.exact} path={AppRoutes.HOME.path}>
							<POListScreen className={this.props.classes.root} />
						</Route>
						<Route exact={AppRoutes.CREATE.exact} path={AppRoutes.CREATE.path}>
							<CreatePOScreen className={this.props.classes.root} />
						</Route>
						<Route exact={AppRoutes.DETAILS.exact} path={AppRoutes.DETAILS.path}>
							<DetailsPOScreen className={this.props.classes.root} />
						</Route>
						<Route exact={AppRoutes.DRAFT.exact} path={AppRoutes.DRAFT.path}>
							<CreatePOScreen className={this.props.classes.root} />
						</Route>
						<Route exact={AppRoutes.APPROVE.exact} path={AppRoutes.APPROVE.path}>
							<DetailsPOScreen className={this.props.classes.root} />
						</Route>
						<Route exact={AppRoutes.CANCEL.exact} path={AppRoutes.CANCEL.path}>
							<CreatePOScreen className={this.props.classes.root} />
						</Route>						
					</Switch>
				</Router>}
			</React.Fragment>			
		);
	}
}

const mapStateToProps = state => {
	return {
		lovs: state.lovReducer.lovs,
		suppliers: state.lovReducer.suppliers,
		dependencies: state.purchaseOrderReducer.dependencies,
		initialArgs: state.journey.initialArgs,
		services: state.journey.services,
	};
};


const mapDispatchToProps = dispatch => {
	return {
	  getAllLovs: (services) => {
		dispatch(actions.getAllLovs(services));
	  },
	  getSuppliers: (services) => {
		dispatch(actions.getSuppliers(services));
	  },
	  setPurchaseOrdersDependencies: dependency => {
		dispatch(actions.setPurchaseOrdersDependencies(dependency));
	  }
	};

};

export default withStyles(styles)(withRootHoc(
	connect(mapStateToProps, mapDispatchToProps)(Journey)
));
