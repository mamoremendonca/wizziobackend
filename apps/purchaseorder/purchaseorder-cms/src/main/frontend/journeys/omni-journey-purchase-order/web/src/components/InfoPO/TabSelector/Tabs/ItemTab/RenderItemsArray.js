import React from "react";

import { withStyles } from "@ui-lib/core/styles";
import { styles } from "./styles";
import ItemsTable from "../../../../ItemsTable/ItemsTable";
import {
  ITEM,
  DESCRIPTION,
  QUANTITY,
  UNIT_VALUE,
  TOTAL_AMOUNT_WO_VAT,
  TOTAL_AMOUNT_W_VAT,
  TAX,
  COST_CENTER,
  COMMENT,
  START_DATE,
  END_DATE,
  FIXEDASSET,
  INDEX,
  VERSION,
  ITEM_TYPE,
} from "../../../../ItemForm/constants";
import { getLovOptionFromValue } from "../../../../../utils/lovUtils";
import * as constants from "../../../../../constants";

const RenderItemsArray = ({ purchaseOrderItems, lovs, purchaseOrderType }) => {
  //TODO Refactor
  const mapItemDetails = () => {
    return purchaseOrderItems
      ? purchaseOrderItems.map((purchaseOrderItem) => {
          let item;
          let itemType = { description: "-" };
          let costCenter;

          if (purchaseOrderType === constants.PO_CAPEX) {
            item = getLovOptionFromValue(lovs.assets, purchaseOrderItem.code);
          } else {
            item =
              getLovOptionFromValue(lovs.items, purchaseOrderItem.code) || "-";
            if (item !== "-") {
              itemType =
                getLovOptionFromValue(lovs.itemTypes, item.itemTypeCode) || "-";
            } else {
              itemType = "-";
            }
            costCenter = lovs.costCenters.find(
              (c) => c.code === purchaseOrderItem.costCenterCode
            );
            if (!costCenter) {
              costCenter = lovs.projects.find(
                (c) => c.code === purchaseOrderItem.costCenterCode
              );
            }
          }
          return {
            [FIXEDASSET]: purchaseOrderType === constants.PO_CAPEX,
            [VERSION]: purchaseOrderItem.version || 1,
            [ITEM_TYPE]: {
              description:
                itemType && itemType.description ? itemType.description : "-",
            },
            [ITEM]: {
              description: item.description,
              value: item.code,
              code: item.code,
            },
            [DESCRIPTION]: purchaseOrderItem.description,
            [QUANTITY]: purchaseOrderItem.amount,
            [UNIT_VALUE]: purchaseOrderItem.unitPriceWithoutTax,
            [TOTAL_AMOUNT_WO_VAT]:
              purchaseOrderItem.unitPriceWithoutTax * purchaseOrderItem.amount,
            [TOTAL_AMOUNT_W_VAT]:
              purchaseOrderItem.amount *
              purchaseOrderItem.unitPriceWithoutTax *
              (1 +
                parseFloat(
                  getLovOptionFromValue(lovs.tax, purchaseOrderItem.taxCode)
                    .taxPercentage || 0
                ) /
                  100), //bug aqui
            [TAX]: getLovOptionFromValue(lovs.tax, purchaseOrderItem.taxCode),
            [COST_CENTER]: costCenter || "-",
            [COMMENT]: purchaseOrderItem.comment,
            [START_DATE]: purchaseOrderItem.startDate || null,
            [END_DATE]: purchaseOrderItem.endDate || null,
            [INDEX]: purchaseOrderItem.index || null,
          };
        })
      : [];
  };

  return (
    <div>
      {/*HEADER*/}
      <ItemsTable
        type={purchaseOrderType}
        isDetails={true}
        values={mapItemDetails()}
        fields={mapItemDetails()}
      />
    </div>
  );
};

export default withStyles(styles)(RenderItemsArray);
