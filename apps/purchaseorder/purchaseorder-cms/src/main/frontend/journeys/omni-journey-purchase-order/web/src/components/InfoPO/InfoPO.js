import React from "react";

import styles from "./styles";
import { withStyles } from "@ui-lib/core/styles";

import Grid from "@ui-lib/core/Grid";
import TextField from "@ui-lib/core/TextField";
import {
  getLovLabelFromOption,
  getLovOptionFromValue,
} from "../../utils/lovUtils";
import { getDateDiff, dateFormater } from "./utils";
import { getFormattedNumber } from "../../utils";

const renderInfoField = (field, classes) => {
  return (
    field && (
      <Grid
        item
        xs={3}
        key={field.label}
        className={classes.fieldContainerGrid}
      >
        <TextField
          fullWidth
          name={field.label}
          label={field.label}
          value={field.value}
          disabled
          className={classes.disabledInfoField}
        />
        {field.helperComponent && field.helperComponent}
      </Grid>
    )
  );
};

const InfoPO = ({ classes, lovs, suppliers, purchaseOrder }) => {
  const supplier = getLovOptionFromValue(
    suppliers,
    purchaseOrder.purchaseOrderHeader.supplierCode
  );
  const infoPOFields = (lovs, supplier, purchaseOrder) =>
    (purchaseOrder && [
      {
        label: "Tipo",
        value: getLovLabelFromOption(
          lovs.purchaseOrderTypes,
          purchaseOrder.type || ""
        ),
      },
      { label: "Código Fornecedor", value: supplier.code },
      {
        label: "Fornecedor",
        value: supplier.description,
      },
      { label: "NIF", value: supplier.tax },
      { label: "PO", value: purchaseOrder.friendlyNumber || "-" },
      {
        label: "Valor s/ IVA",
        value: getFormattedNumber(purchaseOrder.totalWithoutTax, true, "EUR"),
      },
      { label: "Descrição", value: purchaseOrder.description || "-" },
    ]) ||
    [];

  const oneyPOFields = (lovs, purchaseOrder) =>
    (purchaseOrder && [
      {
        label: "Departamento",
        value: getLovLabelFromOption(
          lovs.departments,
          purchaseOrder.departmentCode || ""
        ),
      },
      (purchaseOrder.projectCode && {
        label: "Projeto",
        value: getLovLabelFromOption(
          lovs.projects,
          purchaseOrder.projectCode || ""
        ),
      }) ||
        undefined,
      {
        label: "Data de Criação",
        value: dateFormater(purchaseOrder.creationDate),
      },
      {
        label: "Data limite aprovação",
        value: dateFormater(purchaseOrder.expirationDate),
        helperComponent:
          (purchaseOrder.expirationDate &&
            getDateDiff(purchaseOrder.expirationDate, classes)) ||
          undefined,
      },
      (purchaseOrder.internalOrderCode && {
        label: "Ordem interna",
        value: getLovLabelFromOption(
          lovs.internalOrders,
          purchaseOrder.internalOrderCode || ""
        ),
      }) ||
        undefined,
    ]) ||
    [];

  return (
    <div className={classes.root}>
      <div>Dados da PO</div>
      <hr></hr>
      <Grid container className={classes.container}>
        {infoPOFields(
          lovs,
          supplier,
          purchaseOrder.purchaseOrderHeader
        ).map((field) => renderInfoField(field, classes))}
      </Grid>
      <div>Oney</div>
      <hr></hr>
      <Grid container className={classes.container}>
        {oneyPOFields(lovs, purchaseOrder.purchaseOrderHeader).map((field) =>
          renderInfoField(field, classes)
        )}
      </Grid>
    </div>
  );
};

export default withStyles(styles)(InfoPO);
