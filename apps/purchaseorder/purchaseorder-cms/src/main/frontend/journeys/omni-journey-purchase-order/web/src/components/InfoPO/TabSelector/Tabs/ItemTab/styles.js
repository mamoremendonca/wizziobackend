const styles = theme => ({
  header: {
    ...theme.typography.subtitle1,
    color: theme.palette.primary.main,
    fontFamily: "inherit",
    border: "none"
  },
  footerLabel: {
    ...theme.typography.subtitle1,
    color: theme.palette.primary.main
  },
  footerContainer: {
    margin: `${theme.spacing.unit * 2}px 0`,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  }
});

export { styles };
