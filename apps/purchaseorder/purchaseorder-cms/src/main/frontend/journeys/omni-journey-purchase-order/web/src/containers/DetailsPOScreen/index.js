import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import withContainerWrapper from "../../components/ContainerWrapper";
import DetailsPOScreenRender from "./DetailsPOScreenRender";
import {
  getPurchaseOrderData,
  getCopyPurchaseOrderData,
  clearPurchaseOrder,
} from "../../actions/purchase-order";
import {
  getPurchaseOrderDataFromRepository,
  getTimelineFromRepository,
} from "../../actions/purchase-order-repository";
import { APPROVAL_DECISIONS } from "../../constants";
import { Config } from "../../config";
import {
  setTimeline,
  getTimelineData,
  addTimelineAttachmentRequest,
  removeTimelineAttachmentRequest,
} from "../../actions/timeline";

import { handleApprovalDecisionRequest, handleManagePoRequest } from "./utils";
import { verifyIfLastStepWasReturned } from "../../utils/timeline";
import {
  handleWarningNotificationRequest,
  WarningMessages,
} from "../../utils/notificationHandler";
import {
  handleErrorRequest,
  ErrorMessages,
} from "../../utils/notificationHandler";

//TODO Refactor for upload and download attachments
class DetailsPOScreen extends Component {
  state = {
    po_id: "",
    isFromWorkList: "",
    isApproving: false,
    approvalDecision: "",
    approvalComment: "",
    showConfirmApprovalDecisionDialog: false,
    confirmationDialogOptions: {
      title: "",
      isOpen: false,
      closeHandler: undefined,
    },
    showError: false,
    returnWarning: false,
    dependencies: new Map([
      ["purchaseOrder", false],
      ["timeline", false],
    ]),
    showReturnReason: true,
    loadingAttachments: [],
    canRefreshTimeline: true,
    timelineData: undefined,
  };

  componentDidMount() {
    const {
      match,
      getPurchaseOrderData,
      getPurchaseOrderDataFromRepository,
      getTimelineFromRepository,
      HttpClient,
      Notifications,
      Loading,
    } = this.props;

    if (match && match.params) {
      if (this.props.history.length >= 2) {
        this.setState({ po_id: match.params.id.replace(":id", "") }, () =>
          Loading.request("setLoading", "JOURNEY").then(() => {
            getPurchaseOrderDataFromRepository(
              HttpClient,
              Notifications,
              this.state.po_id
            );
            getTimelineFromRepository(
              HttpClient,
              Notifications,
              this.state.po_id
            );
          })
        );
      } else {
        this.setState({ isFromWorkList: true, po_id: match.params.id }, () =>
          Loading.request("setLoading", "JOURNEY").then(() => {
            getPurchaseOrderData(HttpClient, Notifications, this.state.po_id);
            this.getTimelineData();
          })
        );
      }
    }
  }

  componentDidUpdate(prevProps) {
    const {
      purchaseOrderData,
      timelineData,
      Loading,
      Notifications,
    } = this.props;
    const { showReturnReason, isFromWorkList } = this.state;

    if (
      purchaseOrderData &&
      prevProps.purchaseOrderData !== purchaseOrderData &&
      !this.state.dependencies.get("purchaseOrder")
    ) {
      this.setState({
        dependencies: this.state.dependencies.set("purchaseOrder", true),
      });
    } else if (
      timelineData &&
      prevProps.timelineData !== timelineData &&
      !this.state.dependencies.get("timeline")
    ) {
      this.setState({
        dependencies: this.state.dependencies.set("timeline", true),
        timelineData,
      });
    } else if (timelineData && timelineData !== prevProps.timelineData) {
      this.setState({ timelineData: timelineData });
    }
    if ([...this.state.dependencies.values()].every((t) => t)) {
      Loading.request("stopLoading").then();
      if (showReturnReason && isFromWorkList) {
        const wasReturned = verifyIfLastStepWasReturned(timelineData);
        if (wasReturned) {
          handleWarningNotificationRequest(
            Notifications,
            WarningMessages(
              purchaseOrderData.purchaseOrderHeader.friendlyNumber,
              wasReturned
            ).PO_RETURNED_WARNING
          );
        }
        this.setState({ showReturnReason: false });
      }
    }

    if (
      purchaseOrderData !== prevProps.purchaseOrderData &&
      purchaseOrderData.jwcontext &&
      this.state.po_id !== purchaseOrderData.jwcontext.id
    ) {
      this.setState({
        po_id: purchaseOrderData.jwcontext.id,
      });
    }
  }

  /**
   * to handle the refresh timeline
   */
  updateTimeLineData = () => {};

  handleBackClick = () => {
    const { history, JourneyActions, JourneySettings } = this.props;
    const { isFromWorkList } = this.state;

    if (isFromWorkList) {
      JourneyActions.closeJourney(JourneySettings.instance_id);
    } else {
      history.goBack();
      this.props.clearPurchaseOrder();
    }
  };

  handleClickAction = (isContinue) => {
    if (isContinue) {
      if (
        (this.state.approvalDecision &&
          this.state.approvalDecision === "APPROVE") ||
        (this.state.approvalDecision && this.state.approvalComment)
      ) {
        this.showSubmitConfirmApprovalDecisionDialog();
      } else {
        this.setState({ showError: true });
      }
    } else {
      this.showCancelConfirmApprovalDecisionDialog();
    }
  };

  showSubmitConfirmApprovalDecisionDialog = () => {
    this.setState({
      ...this.state,
      confirmationDialogOptions: {
        title: "Pretende prosseguir com esta ação?", //TODO I18n
        isOpen: true,
        closeHandler: this.submitApprovalDecision,
      },
    });
  };

  showCancelConfirmApprovalDecisionDialog = () => {
    this.setState({
      ...this.state,
      confirmationDialogOptions: {
        title:
          "Tem a certeza que pretende sair do ecrã de aprovação? \nTodas as alterações serão ignoradas", //TODO I18n
        isOpen: true,
        closeHandler: this.cancelApprovalDecision,
      },
    });
  };

  changeApprovalComment = (comment) => {
    this.setState({ approvalComment: comment });
  };

  changeApprovalDecision = (decision) => {
    this.setState({ approvalDecision: decision });
  };

  closeDialog = () => {
    this.setState({
      ...this.state,
      confirmationDialogOptions: {
        title: "",
        isOpen: false,
        closeHandler: undefined,
      },
    });
  };

  cancelApprovalDecision = (accept) => {
    this.setState(
      {
        ...this.state,
        confirmationDialogOptions: {
          title: "",
          isOpen: false,
          closeHandler: undefined,
        },
      },
      () => accept && this.handleBackClick()
    );
  };

  handleManagePoEvent = (value) => {
    const {
      HttpClient,
      Notifications,
      purchaseOrderData,
      getPurchaseOrderData,
      getTimelineData,
    } = this.props;
    if (value && purchaseOrderData) {
      handleManagePoRequest(
        HttpClient,
        purchaseOrderData.currentInstanceId,
        this.closeDialog,
        Notifications,
        getPurchaseOrderData,
        getTimelineData
      );
    } else {
      this.cancelApprovalDecision();
    }
  };

  handleManagePo = () => {
    this.setState({
      ...this.state,
      confirmationDialogOptions: {
        title: "Pretende prosseguir com a gestão da PO?", //TODO I18n
        isOpen: true,
        closeHandler: this.handleManagePoEvent,
      },
    });
  };

  handleCopyPo = () => {
    const {
      HttpClient,
      Notifications,
      purchaseOrderData,
      getCopyPurchaseOrderData,
      history,
    } = this.props;
    getCopyPurchaseOrderData(
      HttpClient,
      Notifications,
      purchaseOrderData.purchaseOrderHeader.repositoryId,
      history
    );
  };

  submitApprovalDecision = (value) => {
    const { HttpClient, Notifications } = this.props;
    const { approvalComment } = this.state;

    if (value) {
      switch (this.state.approvalDecision) {
        case APPROVAL_DECISIONS.APPROVE:
          handleApprovalDecisionRequest(
            HttpClient,
            this.state.po_id,
            Config.APPROVE_PO_URL,
            approvalComment,
            this.handleBackClick,
            Notifications
          );
          break;
        case APPROVAL_DECISIONS.RETURN:
          handleApprovalDecisionRequest(
            HttpClient,
            this.state.po_id,
            Config.RETURN_PO_URL,
            approvalComment,
            this.handleBackClick,
            Notifications
          );
          break;
        case APPROVAL_DECISIONS.REJECT:
          handleApprovalDecisionRequest(
            HttpClient,
            this.state.po_id,
            Config.REJECT_PO_URL,
            approvalComment,
            this.handleBackClick,
            Notifications
          );
          break;
        default:
          break;
      }
    } else {
      this.setState({
        ...this.state,
        confirmationDialogOptions: {
          title: "",
          isOpen: false,
          closeHandler: undefined,
        },
      });
    }
  };

  getTimelineData = () => {
    const {
      getTimelineData,
      HttpClient,
      Notifications,
      Loading,
      getTimelineFromRepository,
    } = this.props;
    //TODO REFACTOR
    if ([...this.state.dependencies.values()].every((t) => t)) {
      Loading.request("setLoading", "JOURNEY").then(() => {
        if (this.state.isFromWorkList) {
          getTimelineData(HttpClient, Notifications, this.state.po_id);
        } else {
          getTimelineFromRepository(
            HttpClient,
            Notifications,
            this.state.po_id
          );
        }
      });
    } else {
      if (this.state.isFromWorkList) {
        getTimelineData(HttpClient, Notifications, this.state.po_id);
      } else {
        getTimelineFromRepository(HttpClient, Notifications, this.state.po_id);
      }
    }
  };

  addAttachment = (files) => {
    const {
      purchaseOrderData: {
        jwcontext: { id },
      },
      HttpClient,
      Notifications,
    } = this.props;
    if (files[0]) {
      const attachmentFile = files[0];
      let copyTimelineData = [...this.state.timelineData];
      let currentTimeline = copyTimelineData.pop();

      const newAttachment = {
        index: currentTimeline.attachments
          ? currentTimeline.attachments.length
          : 0,
        name: attachmentFile.name,
        date: new Date().toISOString(),
        canBeDeletedByCurrentUser: true,
        isLoading: true,
      };
      if (currentTimeline.attachments) {
        currentTimeline.attachments.push(newAttachment);
      } else {
        currentTimeline.attachments = [newAttachment];
      }
      copyTimelineData.push(currentTimeline);
      this.setState({ timelineData: copyTimelineData }, () =>
        addTimelineAttachmentRequest(
          HttpClient,
          attachmentFile,
          id,
          "PURCHASE_ORDER"
        )
          .then((result) => {
            newAttachment.isLoading = false;
            newAttachment.index = result.data.index;
            copyTimelineData = [...this.state.timelineData];
            currentTimeline = copyTimelineData.pop();

            const index = currentTimeline.attachments.findIndex(
              (attachment) => attachment.index === newAttachment.index
            );
            currentTimeline.attachments[index] = newAttachment;
            copyTimelineData.push(currentTimeline);
            this.setState({ timelineData: copyTimelineData });
          })
          .catch((error) => {
            copyTimelineData = [...this.state.timelineData];
            currentTimeline = copyTimelineData.pop();
            const newAttachmentList = [];
            currentTimeline.attachments.forEach((attachment) => {
              if (attachment.index !== newAttachment.index) {
                newAttachmentList.push(attachment);
              }
            });

            currentTimeline.attachments = newAttachmentList;
            copyTimelineData.push(currentTimeline);
            this.setState({ timelineData: copyTimelineData });
            handleErrorRequest(
              Notifications,
              ErrorMessages.PO_ATTACHMENT_SUBMIT_ERROR
            );
          })
      );
    }
  };

  removeAttachment = (attachmentId) => {
    const {
      purchaseOrderData: {
        jwcontext: { id },
      },
      HttpClient,
      Notifications,
    } = this.props;

    let copyTimelineData = [...this.state.timelineData];
    let currentTimeline = copyTimelineData.pop();
    let index = currentTimeline.attachments.findIndex(
      (attachment) => attachment.index === attachmentId
    );

    currentTimeline.attachments[index].isLoading = true;
    copyTimelineData.push(currentTimeline);
    this.setState({ timelineData: copyTimelineData }, () =>
      //change the state of the attachment to be on loading
      removeTimelineAttachmentRequest(HttpClient, attachmentId, id)
        .then((result) => {
          //change the state to delete the current the attachment to be on loading
          copyTimelineData = [...this.state.timelineData];
          currentTimeline = copyTimelineData.pop();

          const newAttachmentList = [];
          currentTimeline.attachments.forEach((attachment) => {
            if (attachment.index !== attachmentId) {
              newAttachmentList.push(attachment);
            }
          });

          currentTimeline.attachments = newAttachmentList;
          copyTimelineData.push(currentTimeline);
          this.setState({ timelineData: copyTimelineData });
        })
        .catch((error) => {
          copyTimelineData = [...this.state.timelineData];
          currentTimeline = copyTimelineData.pop();

          index = currentTimeline.attachments.findIndex(
            (attachment) => attachment.index === attachmentId
          );
          currentTimeline.attachments[index].isLoading = false;

          copyTimelineData.push(currentTimeline);
          this.setState({ timelineData: copyTimelineData });

          handleErrorRequest(
            Notifications,
            ErrorMessages.PO_ATTACHMENT_DELETE_ERROR
          );
        })
    );
  };

  someAttachmentIsLoading = () => {
    if (this.state.timelineData) {
      const copyTimelineData = [...this.state.timelineData];
      const currentTimeline = copyTimelineData.pop();
      if (currentTimeline.attachments) {
        return currentTimeline.attachments.some(
          (attachment) => attachment.isLoading
        );
      }
    }
    return false;
  };

  render() {
    const { I18nProvider, purchaseOrderData, lovs, suppliers } = this.props;

    return (
      ([...this.state.dependencies.values()].every((t) => t) && (
        <DetailsPOScreenRender
          I18nProvider={I18nProvider}
          purchaseOrderId={this.state.po_id}
          purchaseOrder={purchaseOrderData}
          lovs={lovs}
          suppliers={suppliers}
          isApproving={this.state.isApproving}
          approvalDecisionChangeHandler={this.changeApprovalDecision}
          actionClickHandler={this.handleClickAction}
          commentDecisionChangeHandler={this.changeApprovalComment}
          confirmationDialogOptions={this.state.confirmationDialogOptions}
          timelineData={this.state.timelineData}
          handleRefreshTimeline={this.getTimelineData}
          showError={this.state.showError}
          comment={this.state.approvalComment}
          decision={this.state.approvalDecision}
          handleBackClick={this.handleBackClick}
          isFromWorkList={this.state.isFromWorkList}
          handleManagePo={this.handleManagePo}
          handleCopyPo={this.handleCopyPo}
          addAttachment={this.addAttachment}
          removeAttachment={this.removeAttachment}
          someAttachmentIsLoading={this.someAttachmentIsLoading()}
        />
      )) || <div></div>
    );
  }
}

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      //*add the import actions
      getPurchaseOrderData,
      clearPurchaseOrder,
      getPurchaseOrderDataFromRepository,
      getTimelineFromRepository,
      setTimeline,
      getTimelineData,
      getCopyPurchaseOrderData,
    },
    dispatch
  );

const mapStateTopProps = ({
  journey: {
    services: { I18nProvider, HttpClient, Notifications, Loading },
  },
  lovReducer: { lovs, suppliers },
  purchaseOrderReducer: { purchaseOrderData },
  timelineReducer: { timelineData },
  i18n,
}) => ({
  I18nProvider,
  i18n,
  HttpClient,
  Notifications,
  purchaseOrderData,
  lovs,
  suppliers,
  timelineData,
  Loading,
});

export default connect(
  mapStateTopProps,
  mapDispatchToProps
)(withContainerWrapper(DetailsPOScreen));
