import { matchPath } from "react-router-dom";

const AppRoutes = {
	HOME: {
		path: "/",
		defaultTitle: "",
		exact: true
	},
	CREATE: {
		path: "/create",
		basePath: "/create",
		defaultTitle: "route.subtitle_create_purchase_order",
		exact: true
	},
	DETAILS: {
		path: "/details/:id",
		basePath: "/details",
		defaultTitle: "route.subtitle_purchase_order_details",
		exact: true
	},
	DRAFT: {
		path: "/create/:id",
		basePath: "/create",
		defaultTitle: "route.subtitle_create_purchase_order",
		exact: true
	},
	APPROVE: {
		path: "/approve/:id",
		basePath: "/details",
		defaultTitle: "route.subtitle_purchase_order_details_approving",
		exact: true
	},
	CANCEL: {
		path: "/cancel/:id",
		basePath: "/cancel",
		defaultTitle: "route.subtitle_cancel_purchase_order",
		exact: true
	}
};

export const getPathTitle = aPath => {
	let title = aPath;
	const pathKeysArray = Object.keys(AppRoutes);

	for (let i = 0; i < pathKeysArray.length; i++) {
		const pathKey = pathKeysArray[i];
		const pathInfo = AppRoutes[pathKey];

		const matchResult = matchPath(aPath, {
			path: pathInfo.path,
			exact: pathInfo.exact,
			strict: false
		});

		if (matchResult) {
			title = pathInfo.defaultTitle;
		}
	}
	return title;
};

export const updateJourneySubtitle = (
	path,
	JourneyActions,
	JourneySettings,
	purchaseOrderData,
	i18nProvider
) => {
	let pathTitle = getPathTitle(path);
	let subtitle = '';
	
	if (pathTitle){ //Empty path title means that you are in the home.
		if (purchaseOrderData && purchaseOrderData.jwcontext && purchaseOrderData.jwcontext.id) {
			if (pathTitle === 'route.subtitle_purchase_order_details') {
				//override path to approve because of action "Gerir/Manage"
				pathTitle = 'route.subtitle_purchase_order_details_approving';
			}
			subtitle = i18nProvider.Texti18n(pathTitle) + ' '
				+ (purchaseOrderData.purchaseOrderHeader.friendlyNumber ? purchaseOrderData.purchaseOrderHeader.friendlyNumber : 'TPO' + purchaseOrderData.jwcontext.id.toString().padStart(4, '0'))
		} else if (purchaseOrderData) {
			subtitle = i18nProvider.Texti18n(pathTitle) + ' ' + purchaseOrderData.purchaseOrderHeader.friendlyNumber;
		}
	}
	

	let journey_instance_id = JourneySettings.instance_id;
	JourneyActions.setJourneySubtitle(journey_instance_id, subtitle);
};

export default AppRoutes;
