export const isDecimal = /^\d+(\.\d+)?$/;
export const allowDecimalWithComma = /^\d+(,\d+)?$/; //allow introduce, but will be banned at submit the filter
export const isDecimalWithComma = /[0-9]+[,]\d+/;
export const numberEndsWithDot = /[0-9]+[.,]$/;
export const isNumeric = /[0-9]+$/; 