export const Config = {
  URL_PURCHASE_ORDER: "/bin/mvc.do/purchaseorder/v1/instance/", //get PO from process continuity
  URL_PURCHASE_REPOSITORY: "/bin/mvc.do/purchaseorder/v1/repository/", //get PO from purchase order repository
  URL_COPY_PURCHASE_ORDER: "/bin/mvc.do/purchaseorder/v1/instance/copy/",
	
	ATTACHMENT_URL: "/attachment/",
	ITEM_URL: "/item/",
	ITEM_ALL_URL: "/item/all",
	TIMELINE_URL: "/timeline",
	
	//#region PURCHASE ORDER ACTION METHODS
	SUBMIT_PO_URL: "/submit",
	APPROVE_PO_URL: "/approve",
	RETURN_PO_URL: "/return",
	REJECT_PO_URL: "/reject",
	CANCEL_PO_URL: "/cancel",	
	CLAIM_PO_URL: "/claim",
	//#endregion
	
	//#region REST METHODS 
	METHOD_POST: "post",
	METHOD_GET: "get",
	METHOD_PUT: "put",
	METHOD_DELETE: "delete",
	//#endregion

	//LIST METHODS  
	POST_SEARCH_REPOSITORY_URL: "/bin/mvc.do/purchaseorder/v1/repository/list",

	//#region Reference Data Request 
	ONEY_REFERENCE_LISTS_URL: "/bin/mvc.do/oneyapp/v1/reference/lists",
	SUPPLIER_URL: "/bin/mvc.do/oneyapp/v1/reference/supplier",
	PURCHASE_ORDER_REFERENCE_LISTS_URL: "/bin/mvc.do/purchaseorder/v1/reference/lists",
	//#endregion
};
