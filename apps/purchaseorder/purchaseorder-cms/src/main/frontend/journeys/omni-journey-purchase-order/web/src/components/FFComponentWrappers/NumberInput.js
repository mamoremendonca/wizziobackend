import * as React from "react";

import TextField from "@ui-lib/core/TextField";
import NumberFormat from "react-number-format";

import { withStyles } from "@ui-lib/core/styles";
import styles from "./styles";

const NumberInputWrapper = ({
  input: { name, onChange, value, ...restInput },
  meta,
  disabled,
  currency,
  hasDecimalPlaces,
  classes,
  ...rest
}) => {

  const showError =
    ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) &&
    meta.touched;
  const className = disabled ? classes.textFieldDisabled : undefined;
  const decimalPlaces = currency ? 2 : hasDecimalPlaces === false ? 0 : 2;
  const currencySymbol = currency ? ` ${currency}` : "";

  return (
    <React.Fragment>
      <NumberFormat
        {...rest}
        name={name}
        helperText={showError ? meta.error || meta.submitError : undefined}
        error={showError}
        inputProps={restInput}
        onValueChange={(values) => {
          onChange({
            target: {
              value: values.value,
            },
          });
        }}
        className={className}
        value={disabled && (value ? parseFloat(value) : 0)}
        defaultValue={disabled ? parseFloat(value) | "-" : parseFloat(value)}
        disabled={disabled}
        style={{ paddingTop: 5 }}
        customInput={TextField}
        thousandSeparator={"."}
        decimalSeparator={","}
        decimalScale={decimalPlaces}
        isNumericString={true}
        suffix={currencySymbol}
      />
    </React.Fragment>
  );
};

export default withStyles(styles)(NumberInputWrapper);
