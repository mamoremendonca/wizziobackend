export const handleErrorRequest = (Notifications, message) => {
  Notifications.notify(message, "error", "Process",5000);
};

export const handleSuccessNotificationRequest = (Notifications, message) => {
  Notifications.notify(message, "success", "Process",2000);
};

export const handleWarningNotificationRequest = (Notifications,message) => {
  Notifications.notify(message, "warning", "Process", 15000);
}
//TODO I18n
export const ErrorMessages = {
  PO_GET_ERROR:
    "Não foi possivel obter a informação da Ordem de Compra. Por favor tente novamente",
  PO_CREATE_ERROR:
    "Não foi possivel criar a Ordem de Compra. Por favor tente novamente.",
  PO_DELETE_ERROR:
    "Não foi possivel apagar a Ordem de Compra. Por favor tente novamente.",
  PO_SUBMIT_ERROR:
    "Não foi possivel submeter a Ordem de Compra. Por favor tente novamente.",
  SEARCH_ERROR:
    "Não foi possível concluir a sua pesquisa, por favor tente mais tarde",
  PO_ITEM_SUBMIT_ERROR:
    "Não foi possivel submeter o item. Por favor tente novamente.",
  PO_ITEM_EDIT_ERROR:
    "Não foi possivel atualizar o Item. Por favor tente novamente.",
  PO_ITEM_DELETE_ERROR:
    "Não foi possivel remover o item. Por favor tente novamente.",
  PO_ATTACHMENT_SUBMIT_ERROR:
    "Não foi possivel submeter o anexo. Por favor tente novamente.",
  PO_ATTACHMENT_DELETE_ERROR:
    "Não foi possivel remover o anexo. Por favor tente novamente.",
  TIMELINE_GET_ERROR:
    "Não foi possivel obter a informação sobre o histórico. Por favor tente novamente",
  PO_APPROVE_DECISION_ERROR:
    "Não foi possível submeter a sua decisão. Por favor tente novamente",
  PO_MANAGE_ERROR:
    "Não foi possível atribuir aprovação do processo ao utilizador. Por favor tente novamente",
  PO_LOADING_RESOURCES:
    "Não foi possível carregar os dados de referência"
};

//TODO I18n
export const WarningMessages = (identificator, reason) => { return {
  TPO_REJECTED_WARNING: `${identificator} Rejeitada/${reason}`,
  PO_REJECTED_WARNING: `${identificator} Rejeitada/${reason}`,
  PO_RETURNED_WARNING: `${identificator} Devolvida/${reason}`
}}

//TODO I18n
const SubmitRequestMessage = purchaseOrderNumber => {
  return `A sua Purchase Order com o nº ${purchaseOrderNumber} foi submetida para aprovação. Será notificado quando a aprovação for finalizada.`;
};

export const SuccessMessages = {
  SubmitRequestMessage: SubmitRequestMessage
};
