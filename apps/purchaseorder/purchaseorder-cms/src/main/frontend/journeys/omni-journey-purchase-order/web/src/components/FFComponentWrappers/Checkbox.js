import * as React from "react";
import Checkbox from "@ui-lib/core/Checkbox";

const CheckboxWrapper = ({
  input: { checked, name, onChange, ...restInput },
  meta,
  ...rest
}) => (
  <Checkbox
    {...rest}
    name={name}
    inputProps={restInput}
    onChange={onChange}
    checked={checked}
  />
);

export default CheckboxWrapper;
