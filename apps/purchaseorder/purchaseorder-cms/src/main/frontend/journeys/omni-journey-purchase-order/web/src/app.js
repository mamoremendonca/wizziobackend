import React from "react";
import Journey from "./journey";
import { Provider } from "react-redux";
import { name as packageName, parent, contentVersion } from "./../package.json";
import { createStoreSingleton } from "omni-journey";
import { FinalJourney } from "omni-journey";

/**
 * REDUCERS TO IMPORT
 */
import { lovReducer } from "./reducers/lov.js";
import { purchaseOrderReducer } from "./reducers/purchaseOrder";
import { timelineReducer } from "./reducers/timeline";

let reducersToMerge = { lovReducer, purchaseOrderReducer, timelineReducer };

let Store = createStoreSingleton(reducersToMerge);

export default class App extends React.Component {
  render = () => {
    return (
      <Provider store={Store}>
        <div>
          <RenderJourney handleReceiveMessage={this.handleReceiveMessage} />
        </div>
      </Provider>
    );
  };
}

let RenderJourney = FinalJourney(
  Journey,
  Store,
  packageName,
  parent,
  contentVersion
);
