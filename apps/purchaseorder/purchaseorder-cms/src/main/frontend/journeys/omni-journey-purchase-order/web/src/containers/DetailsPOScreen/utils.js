import { Config } from "../../config";
import {
  handleErrorRequest,
  ErrorMessages,
} from "../../utils/notificationHandler";

export const handleApprovalDecisionRequest = (
  HttpClient,
  purchaseOrderId,
  urlParam,
  comment,
  handleBackClick,
  Notifications
) => {
  let config = {
    method: Config.METHOD_PUT,
    url: `${Config.URL_PURCHASE_ORDER}${purchaseOrderId}${urlParam}`,
    data: comment ? comment : " ",
    headers: { "Content-Type": "text/html; charset=utf-8" },
  };
  return HttpClient.request(config)
    .then((res) => {
      handleBackClick();
    })
    .catch(() =>
      handleErrorRequest(Notifications, ErrorMessages.PO_APPROVE_DECISION_ERROR)
    );
};

export const handleManagePoRequest = (
  HttpClient,
  instanceId,
  closeDialog,
  Notifications,
  getPurchaseOrderData,
  getTimelineData
) => {
  //TODO refactor
  let config = {
    method: Config.METHOD_PUT,
    url: `${Config.URL_PURCHASE_ORDER}${instanceId}${Config.CLAIM_PO_URL}`,
  };

  return HttpClient.request(config)
    .then((res) => {
      getPurchaseOrderData(HttpClient, Notifications, instanceId);
      getTimelineData(HttpClient, Notifications, instanceId);
      closeDialog();
    })
    .catch(() => {
      handleErrorRequest(Notifications, ErrorMessages.PO_MANAGE_ERROR);
      closeDialog();
    });
};
