import React, { Fragment } from "react";
import styles from "./styles";
import { withStyles } from "@ui-lib/core/styles";
import TableCell from "@ui-lib/core/TableCell";
import TableRow from "@ui-lib/core/TableRow";
import TableActions from "./TableActions";
import {
  ITEM,
  DESCRIPTION,
  QUANTITY,
  UNIT_VALUE,
  TOTAL_AMOUNT_WO_VAT,
  TOTAL_AMOUNT_W_VAT,
  TAX,
  COST_CENTER,
  COMMENT,
  FIXEDASSET,
  START_DATE,
  END_DATE,
  VERSION,
  ITEM_TYPE,
} from "../ItemForm/constants";
import { PO_CAPEX } from "../../constants";
import { dateFormater } from "../InfoPO/utils";
import { PO_CONTRACT } from "../FormPO/constants";
import { getFormattedNumber } from "../../utils/numberFormat";

const ItemTableRows = ({
  classes,
  values,
  handleEdit,
  handleCopy,
  type,
  isDetails,
  ...rest
}) => {
  const isFixedAsset = type === PO_CAPEX ? true : false;
  const isContract = type === PO_CONTRACT ? true : false;
  return (
    <Fragment>
      {values &&
        values.length > 0 &&
        values.map((item, index) => (
          <TableRow key={index} hover className={classes.row}>
            {isDetails && (
              <TableCell component="th" scope="row" align="center">
                {item[VERSION]}
              </TableCell>
            )}
            {isFixedAsset && item[FIXEDASSET] ? (
              <TableCell component="th" scope="row" align="center">
                {item[ITEM] ? item[ITEM].value : item[FIXEDASSET].description}
              </TableCell>
            ) : (
              item[ITEM] && (
                <>
                  {((isDetails && !isFixedAsset) || !isDetails) && (
                    <TableCell component="th" scope="row" align="center">
                      {item[ITEM_TYPE] && item[ITEM_TYPE].description
                        ? item[ITEM_TYPE].description
                        : "-"}
                    </TableCell>
                  )}
                  <TableCell component="th" scope="row" align="center">
                    {item[ITEM].code}
                  </TableCell>
                </>
              )
            )}
            <TableCell align="center">{item[DESCRIPTION]}</TableCell>
            <TableCell align="center">
              {getFormattedNumber(item[QUANTITY], false)}
            </TableCell>
            <TableCell align="center">
              {getFormattedNumber(item[UNIT_VALUE], true, "EUR")}
            </TableCell>
            <TableCell align="center">
              {getFormattedNumber(item[TOTAL_AMOUNT_WO_VAT], true, "EUR")}
            </TableCell>
            <TableCell align="center">
              {item[TAX] ? `${parseFloat(item[TAX].taxPercentage)} %` : "-"}
            </TableCell>
            <TableCell align="center">
              {getFormattedNumber(item[TOTAL_AMOUNT_W_VAT], true, "EUR")}
            </TableCell>
            {!isFixedAsset && (
              <Fragment>
                <TableCell align="center">
                  {item[COST_CENTER] && item[COST_CENTER].description
                    ? item[COST_CENTER].description
                    : "-"}
                </TableCell>
                {isContract && (
                  <>
                    <TableCell align="center">
                      {dateFormater(item[START_DATE]) || "-"}
                    </TableCell>
                    <TableCell align="center">
                      {dateFormater(item[END_DATE]) || "-"}
                    </TableCell>
                  </>
                )}
              </Fragment>
            )}
            <TableCell align="center">
              {item[COMMENT] ? item[COMMENT] : "-"}
            </TableCell>
            {handleEdit && (
              <TableCell align="right">
                <TableActions
                  item={item}
                  index={index}
                  editItem={handleEdit}
                  copyItem={handleCopy}
                />
              </TableCell>
            )}
          </TableRow>
        ))}
    </Fragment>
  );
};

export default withStyles(styles)(ItemTableRows);
