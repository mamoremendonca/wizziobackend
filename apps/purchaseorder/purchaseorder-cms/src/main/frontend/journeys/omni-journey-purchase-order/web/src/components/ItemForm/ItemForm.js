import React, { Fragment } from "react";
import { withStyles } from "@ui-lib/core/styles";
import Grid from "@ui-lib/core/Grid";
import { connect } from "react-redux";
import * as Constants from "./constants";
import styles from "./styles";
import { Field } from "react-final-form";
import {
  TextField,
  Select,
  BasicDateField,
  NumberInput,
} from "../FFComponentWrappers";
import { OnChange } from "react-final-form-listeners";
import { PO_CAPEX, PO_CONTRACT, PO_OPEX } from "../../constants";
import { CurrencyType } from "../../utils";

const required = (I18nProvider) => (value) =>
  value
    ? undefined
    : I18nProvider.Texti18n("create_po.item.dialog.form.error.required");
const mustBeNumber = (I18nProvider) => (value) =>
  isNaN(value)
    ? I18nProvider.Texti18n("create_po.item.dialog.form.error.not_a_number")
    : undefined;
const minValue = (min, I18nProvider) => (value) =>
  isNaN(value) || value >= min
    ? undefined
    : `${I18nProvider.Texti18n(
        "create_po.item.dialog.form.error.value_below_limit"
      )} ${min}`;

const composeValidators = (...validators) => (value) =>
  validators.reduce((error, validator) => error || validator(value), undefined);

/**
 * Return the default cost center of a project or of a internal order.
 *
 * @param {*} costCenterValues filtered costCenters of department
 * @param {*} purchaseOrderData the current purchase order
 * @param {*} internalOrders the list of all internal orders to get the cost center code
 * @param {*} projects the list of all projects to get the cost center code
 */
const getCostCenterDefaultValue = (costCenterValues, values) => {
  if (costCenterValues.length === 1) {
    return costCenterValues[0];
  }

  if (values && values.item) {
    for (const costCenter of costCenterValues) {
      if (costCenter.code === values.item.defaultCostCenterCode) {
        return costCenter;
      }
    }
  }
  return undefined;
};

const filterSelectInputValues = (
  options,
  purchaseOrderData,
  filterName,
  internalOrders,
  projects
) => {
  let optionsFiltered = [];

  if (filterName === Constants.DEPARTMENT) {
    options.forEach((option) => {
      if (
        !option.departmentCode ||
        option.departmentCode === purchaseOrderData.departmentCode
      ) {
        optionsFiltered.push(option);
      }
    });
  }

  if (filterName === Constants.COST_CENTER) {
    // get cost center from internal order
    if (purchaseOrderData.internalOrderCode) {
      let costCenterCode = "";

      for (const internalOrder of internalOrders) {
        if (internalOrder.code === purchaseOrderData.internalOrderCode) {
          costCenterCode = internalOrder.costCenterCode;
          break; //* just to not take too long
        }
      }

      if (costCenterCode) {
        for (const option of options) {
          if (option.code === costCenterCode) {
            optionsFiltered.push(option);
            break; //* just to not take too long
          }
        }
      } else {
        optionsFiltered = [...options];
      }
    }
    // get cost center from project
    else if (purchaseOrderData.projectCode) {
      for (const project of projects) {
        if (project.code === purchaseOrderData.projectCode) {
          optionsFiltered.push(project);
          break; //* just to not take too long
        }
      }
    } else {
      options.forEach((option) => {
        if (
          !option.departmentCode ||
          option.departmentCode === purchaseOrderData.departmentCode
        ) {
          optionsFiltered.push(option);
        }
      });
    }
  }
  return optionsFiltered;
};

//#region //* Aux Functions
const getVariableFields = (
  type,
  classes,
  purchaseOrderData,
  props,
  values,
  I18nProvider
) => {
  switch (type) {
    case PO_CAPEX: {
      break;
    }
    case PO_OPEX: {
      if (!purchaseOrderData.internalOrderCode) {
        const options = filterSelectInputValues(
          props.costCenters,
          purchaseOrderData,
          Constants.COST_CENTER,
          props.internalOrders,
          props.projects
        );
        const defaultValue = getCostCenterDefaultValue(options, values);
        return (
          <Grid item className={classes.Grid} xs={3}>
            <Field
              name={Constants.COST_CENTER}
              validate={required(I18nProvider)}
              label={I18nProvider.Labeli18n(
                "create_po.item.dialog.form.label.cost_center"
              )}
              component={Select}
              defaultValue={defaultValue}
              fullWidth
              formControlProps={{ fullWidth: true }}
              InputLabelProps={{ shrink: true }}
              options={options}
              required
              disabled={options.length === 1}
            />
          </Grid>
        );
      }
      break;
    }
    case PO_CONTRACT: {
      let costCenterField = undefined;
      if (!purchaseOrderData.internalOrderCode) {
        const options = filterSelectInputValues(
          props.costCenters,
          purchaseOrderData,
          Constants.COST_CENTER,
          props.internalOrders,
          props.projects
        );
        const defaultValue = getCostCenterDefaultValue(options, values);
        costCenterField = (
          <Grid item className={classes.Grid} xs={3}>
            <Field
              name={Constants.COST_CENTER}
              validate={required(I18nProvider)}
              label={I18nProvider.Labeli18n(
                "create_po.item.dialog.form.label.cost_center"
              )}
              component={Select}
              defaultValue={defaultValue}
              fullWidth
              formControlProps={{ fullWidth: true }}
              InputLabelProps={{ shrink: true }}
              options={options}
              required
              disabled={options.length === 1}
            />
          </Grid>
        );
      }
      const minDateApproval = new Date().toISOString().slice(0, 10);
      return (
        <Fragment>
          {costCenterField}
          <Grid item className={classes.Grid} xs={3}>
            <Field
              name={"startDate"}
              validate={required(I18nProvider)}
              label={I18nProvider.Labeli18n(
                "create_po.item.dialog.form.label.initial_date"
              )}
              min={minDateApproval}
              component={BasicDateField}
              fullWidth
              InputLabelProps={{ shrink: true }}
              required
            />
          </Grid>
          <Grid item className={classes.Grid} xs={3}>
            <Field
              name={"endDate"}
              validate={required(I18nProvider)}
              label={I18nProvider.Labeli18n(
                "create_po.item.dialog.form.label.final_date"
              )}
              min={minDateApproval}
              component={BasicDateField}
              fullWidth
              InputLabelProps={{ shrink: true }}
              required
            />
          </Grid>
        </Fragment>
      );
    }
    default: {
      return null;
    }
  }
};

//#endregion

const getItemType = (
  type,
  classes,
  loadItems,
  loadFixedAssets,
  itemTypes,
  purchaseOrderData,
  values,
  I18nProvider
) => {
  const isFixedAsset = type === PO_CAPEX ? true : false;

  if (isFixedAsset) {
    const options = filterSelectInputValues(
      loadFixedAssets(),
      purchaseOrderData,
      Constants.DEPARTMENT
    );
    return (
      <Grid item className={classes.Grid} xs={3}>
        <Field
          name={Constants.ITEM}
          validate={required(I18nProvider)}
          label={I18nProvider.Labeli18n(
            "create_po.item.dialog.form.label.asset"
          )}
          component={Select}
          fullWidth
          options={options}
          InputLabelProps={{ shrink: true }}
          formControlProps={{ fullWidth: true }}
          required
        />
      </Grid>
    );
  } else {
    const itemOptions = [];
    if (values.itemType) {
      loadItems().forEach((item) => {
        if (
          (!item.itemTypeCode || values.itemType.code === item.itemTypeCode) &&
          (!item.departmentCode ||
            purchaseOrderData.departmentCode === item.departmentCode)
        ) {
          itemOptions.push(item);
        }
      });
    }
    return (
      <>
        <Grid item className={classes.Grid} xs={3}>
          <Field
            name={Constants.ITEM_TYPE}
            validate={required(I18nProvider)}
            label={I18nProvider.Labeli18n(
              "create_po.item.dialog.form.label.item_type"
            )}
            component={Select}
            options={itemTypes}
            fullWidth
            formControlProps={{ fullWidth: true }}
            InputLabelProps={{ shrink: true }}
            required
          />
        </Grid>
        <Grid item className={classes.Grid} xs={3}>
          <Field
            name={Constants.ITEM}
            validate={required(I18nProvider)}
            label={I18nProvider.Labeli18n(
              "create_po.item.dialog.form.label.item"
            )}
            component={Select}
            fullWidth
            options={itemOptions}
            formControlProps={{ fullWidth: true }}
            InputLabelProps={{ shrink: true }}
            disabled={!values[Constants.ITEM_TYPE] ? true : false}
            required
          />
        </Grid>
      </>
    );
  }
};

const WhenFieldChanges = ({ field, becomes, set, to }) => (
  <Field name={set}>
    {({ input: { onChange } }) => (
      <OnChange name={field}>
        {() => {
          if (becomes) {
            const toModyfied = mapChangedValues(set, to);
            onChange(toModyfied);
          }
        }}
      </OnChange>
    )}
  </Field>
);

const shouldChange = (fieldName, values) => {
  switch (fieldName) {
    case Constants.TOTAL_AMOUNT_WO_VAT: {
      if (values[Constants.QUANTITY] >= 0 && values[Constants.UNIT_VALUE]) {
        return true;
      } else {
        return false;
      }
    }
    case Constants.TOTAL_AMOUNT_W_VAT: {
      if (
        values[Constants.QUANTITY] >= 0 &&
        values[Constants.UNIT_VALUE] &&
        values[Constants.TAX] &&
        values[Constants.TAX].value
      ) {
        return true;
      } else {
        return false;
      }
    }
    case Constants.DESCRIPTION: {
      if (
        (values[Constants.ITEM] && values[Constants.ITEM].description) ||
        (values[Constants.FIXEDASSET] && values[Constants.FIXEDASSET].name)
      ) {
        return true;
      } else {
        return false;
      }
    }
    default: {
      return false;
    }
  }
};

const shouldChangeCostCenter = (props, purchaseOrderData) => {
  const costCenters = filterSelectInputValues(
    props.costCenters,
    purchaseOrderData,
    Constants.COST_CENTER,
    props.internalOrders,
    props.projects
  );
  return costCenters.length > 1;
};

const mapChangedValues = (fieldName, values) => {
  switch (fieldName) {
    case Constants.TOTAL_AMOUNT_WO_VAT: {
      return values;
    }
    case Constants.TOTAL_AMOUNT_W_VAT: {
      return values;
    }
    default: {
      return values;
    }
  }
};
const parsePercentage = (value) => (value ? parseFloat(value) / 100 : 0);
const setItemValueWOVAT = (quantity, value) =>
  parseFloat(quantity) >= 0 && parseFloat(value) >= 0
    ? parseFloat(quantity) * parseFloat(value)
    : 0;
const setItemValueWVAT = (quantity, value, vat) =>
  parseFloat(quantity) >= 0 && parseFloat(value) >= 0 && parseFloat(vat) >= 0
    ? parseFloat(quantity) * parseFloat(value) * parseFloat(1 + vat)
    : 0;

const setTax = (item, taxesRefData) => {
  if (item) {
    const tax = taxesRefData.find((tax) => tax.code === item.defaultTaxCode);
    if (tax) return tax;
  }
  return null;
};

const ItemForm = (props) => {
  const {
    classes,
    loadItems,
    loadFixedAssets,
    type,
    values,
    itemTypes,
    purchaseOrderData,
    services: { I18nProvider },
    marketType,
  } = props;
  const itemType = getItemType(
    type,
    classes,
    loadItems,
    loadFixedAssets,
    itemTypes,
    purchaseOrderData.purchaseOrderHeader,
    values,
    I18nProvider
  );
  const variableFields = getVariableFields(
    type,
    classes,
    purchaseOrderData.purchaseOrderHeader,
    props,
    values,
    I18nProvider
  );
  const filteredTaxes = props.tax.filter(
    (t) => !t.marketCode || !marketType || t.marketCode === marketType
  );
  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        <WhenFieldChanges
          field={Constants.UNIT_VALUE}
          becomes={shouldChange(Constants.TOTAL_AMOUNT_WO_VAT, values)}
          set={Constants.TOTAL_AMOUNT_WO_VAT}
          to={setItemValueWOVAT(
            values[Constants.QUANTITY],
            values[Constants.UNIT_VALUE]
          )}
        />
        <WhenFieldChanges
          field={Constants.QUANTITY}
          becomes={shouldChange(Constants.TOTAL_AMOUNT_WO_VAT, values)}
          set={Constants.TOTAL_AMOUNT_WO_VAT}
          to={setItemValueWOVAT(
            values[Constants.QUANTITY],
            values[Constants.UNIT_VALUE]
          )}
        />
        <WhenFieldChanges
          field={Constants.UNIT_VALUE}
          becomes={shouldChange(Constants.TOTAL_AMOUNT_W_VAT, values)}
          set={Constants.TOTAL_AMOUNT_W_VAT}
          to={setItemValueWVAT(
            values[Constants.QUANTITY],
            values[Constants.UNIT_VALUE],
            values[Constants.TAX]
              ? parsePercentage(values[Constants.TAX].taxPercentage)
              : 0
          )}
        />
        <WhenFieldChanges
          field={Constants.QUANTITY}
          becomes={shouldChange(Constants.TOTAL_AMOUNT_W_VAT, values)}
          set={Constants.TOTAL_AMOUNT_W_VAT}
          to={setItemValueWVAT(
            values[Constants.QUANTITY],
            values[Constants.UNIT_VALUE],
            values[Constants.TAX]
              ? parsePercentage(values[Constants.TAX].taxPercentage)
              : 0
          )}
        />
        <WhenFieldChanges
          field={Constants.TAX}
          becomes={shouldChange(Constants.TOTAL_AMOUNT_W_VAT, values)}
          set={Constants.TOTAL_AMOUNT_W_VAT}
          to={setItemValueWVAT(
            values[Constants.QUANTITY],
            values[Constants.UNIT_VALUE],
            values[Constants.TAX]
              ? parsePercentage(values[Constants.TAX].taxPercentage)
              : 0
          )}
        />
        <WhenFieldChanges
          field={Constants.ITEM}
          becomes={shouldChange(Constants.DESCRIPTION, values)}
          set={Constants.DESCRIPTION}
          to={
            values[Constants.ITEM]
              ? values[Constants.ITEM].description
              : values[Constants.DESCRIPTION]
          }
        />
        <WhenFieldChanges
          field={Constants.FIXEDASSET}
          becomes={shouldChange(Constants.DESCRIPTION, values)}
          set={Constants.DESCRIPTION}
          to={
            values[Constants.FIXEDASSET]
              ? values[Constants.FIXEDASSET].name
              : values[Constants.DESCRIPTION]
          }
        />
        <WhenFieldChanges
          field={Constants.ITEM_TYPE}
          becomes={true}
          set={Constants.ITEM}
          to={null}
        />
        <WhenFieldChanges
          field={Constants.ITEM_TYPE}
          becomes={true}
          set={Constants.DESCRIPTION}
          to={null}
        />
        <WhenFieldChanges
          field={Constants.ITEM}
          becomes={shouldChangeCostCenter(
            props,
            purchaseOrderData.purchaseOrderHeader
          )}
          set={Constants.COST_CENTER}
          to={null}
        />
        <WhenFieldChanges
          field={Constants.ITEM}
          becomes={true}
          set={Constants.TAX}
          to={setTax(values[Constants.ITEM], filteredTaxes)}
        />
      </Grid>
      <Grid container spacing={24}>
        {itemType}
        <Grid item className={classes.Grid} xs={3}>
          <Field
            name={Constants.DESCRIPTION}
            validate={required(I18nProvider)}
            label={I18nProvider.Labeli18n(
              "create_po.item.dialog.form.label.describe"
            )}
            component={TextField}
            fullWidth
            InputLabelProps={{ shrink: true }}
            required
            disabled={
              type !== PO_CAPEX
                ? !values[Constants.ITEM_TYPE]
                  ? true
                  : false
                : false
            }
          />
        </Grid>
        <Grid item className={classes.Grid} xs={3}>
          <Field
            name={Constants.QUANTITY}
            validate={composeValidators(
              required(I18nProvider),
              mustBeNumber(I18nProvider),
              minValue(0, I18nProvider)
            )}
            label={I18nProvider.Labeli18n(
              "create_po.item.dialog.form.label.qty"
            )}
            component={NumberInput}
            fullWidth
            InputLabelProps={{ shrink: true }}
            required
          />
        </Grid>
        <Grid item className={classes.Grid} xs={3}>
          <Field
            name={Constants.UNIT_VALUE}
            validate={composeValidators(
              required(I18nProvider),
              mustBeNumber(I18nProvider),
              minValue(0, I18nProvider)
            )}
            label={I18nProvider.Labeli18n(
              "create_po.item.dialog.form.label.unit_value"
            )}
            component={NumberInput}
            currency={CurrencyType.EUR}
            fullWidth
            InputLabelProps={{ shrink: true }}
            required
          />
        </Grid>
        <Grid item className={classes.Grid} xs={3}>
          <Field
            name={Constants.TOTAL_AMOUNT_WO_VAT}
            label={I18nProvider.Labeli18n(
              "create_po.item.dialog.form.label.total_amount_value"
            )}
            component={NumberInput}
            currency={CurrencyType.EUR}
            fullWidth
            disabled
            InputLabelProps={{ shrink: true }}
          />
        </Grid>
        <Grid item className={classes.Grid} xs={3}>
          <Field
            name={Constants.TAX}
            label={I18nProvider.Labeli18n(
              "create_po.item.dialog.form.label.tax"
            )}
            component={Select}
            fullWidth
            formControlProps={{ fullWidth: true }}
            InputLabelProps={{ shrink: true }}
            options={filteredTaxes}
            disabled
          />
        </Grid>
        <Grid item className={classes.Grid} xs={3}>
          <Field
            name={Constants.TOTAL_AMOUNT_W_VAT}
            label={I18nProvider.Labeli18n(
              "create_po.item.dialog.form.label.total_amount_w_vat"
            )}
            component={NumberInput}
            currency={CurrencyType.EUR}
            fullWidth
            disabled
            InputLabelProps={{ shrink: true }}
          />
        </Grid>
        {variableFields}
        <Grid item className={classes.Grid} xs={3}>
          <Field
            name={Constants.COMMENT}
            label={I18nProvider.Labeli18n(
              "create_po.item.dialog.form.label.comment"
            )}
            component={TextField}
            fullWidth
            InputLabelProps={{ shrink: true }}
          />
        </Grid>
      </Grid>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    purchaseOrderTypes: state.lovReducer.lovs.purchaseOrderTypes,
    departments: state.lovReducer.lovs.departments,
    projects: state.lovReducer.lovs.projects,
    tax: state.lovReducer.lovs.tax,
    costCenters: state.lovReducer.lovs.costCenters,
    itemTypes: state.lovReducer.lovs.itemTypes,
    internalOrders: state.lovReducer.lovs.internalOrders,
    purchaseOrderData: state.purchaseOrderReducer.purchaseOrderData,
    services: state.journey.services,
  };
};

export default connect(mapStateToProps)(withStyles(styles)(ItemForm));
