import * as constants from "../constants";

const initialState = {
  timelineData: {},
}

export const timelineReducer = (state, action) => {
  if (state === undefined) return initialState;
 
  switch (action.type) {
    case constants.SET_TIMELINE_DATA : {
      return { ...state, timelineData: action.payload};
    }
    default:
      return state;
  }
};
