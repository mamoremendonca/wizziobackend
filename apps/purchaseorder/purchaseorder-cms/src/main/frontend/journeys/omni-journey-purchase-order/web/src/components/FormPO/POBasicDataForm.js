import React from "react";
import { connect } from "react-redux";
import { Field } from "react-final-form";
import Grid from "@ui-lib/core/Grid";
import {
  Select,
  TextField,
  BasicDateField,
  NumberInput
} from "../FFComponentWrappers";
import * as Constants from "./constants";
import { PO_CAPEX } from "../../constants";
import { OnChange } from "react-final-form-listeners";
import { CurrencyType } from "../../utils";

const handleInputEditableStatus = (values, name) => {
  if (values) {
    if (values.type && values.type.value === PO_CAPEX) {
      return true;
    }
    if (name === Constants.PROJECT) {
      return values[Constants.INTERNAL_ORDER] ? true : false;
    } else if (name === Constants.INTERNAL_ORDER) {
      return values[Constants.PROJECT] ? true : false;
    }
  }

  return false;
};

const filterSelectInputValues = (values, name, options) => {
  if (values[Constants.DEPARTMENT]) {
    if (name === Constants.INTERNAL_ORDER || name === Constants.PROJECT) {
      return options.filter(
        (option) =>
          !option.departmentCode ||
          option.departmentCode === values[Constants.DEPARTMENT].value
      );
    }
  }
  return options;
};
// Be careful to used this because of loading data.
const WhenFieldChanges = ({ field, becomes, set, to }) => (
  <Field name={set}>
    {({ input: { onChange } }) => (
      <OnChange name={field}>
        {() => {
          if (becomes) {
            const toModyfied = to;
            onChange(toModyfied);
          }
        }}
      </OnChange>
    )}
  </Field>
);

const required = (I18nProvider) => (value) =>
  value
    ? undefined
    : I18nProvider.Texti18n("create_po.item.dialog.form.error.required");

const POBasicDataForm = ({
  values,
  errors,
  loadOptions,
  loadSupplierCodeOptions,
  loadSupplierNameOptions,
  loadSupplierTaxOptions,
  field,
  classes,
  allDisabledFields,
  handleClearItems,
  parentMutator,
  I18nProvider,
  ...props
}) => {
  const actualDate = new Date();
  const minDateAproval = actualDate.toISOString().slice(0, 10);
  return (
    <Grid container spacing={24}>
      <WhenFieldChanges
        field={Constants.PURCHASE_ORDER_TYPE}
        becomes={values.type && values.type.value === PO_CAPEX}
        set={Constants.INTERNAL_ORDER}
        to={undefined}
      />
      <WhenFieldChanges
        field={Constants.PURCHASE_ORDER_TYPE}
        becomes={values.type && values.type.value === PO_CAPEX}
        set={Constants.PROJECT}
        to={undefined}
      />
      <Grid item className={classes.Grid} xs={2}>
        <Field
          name={Constants.PURCHASE_ORDER_TYPE}
          component={Select}
          label="Tipo"
          formControlProps={{ fullWidth: true }}
          InputLabelProps={{ shrink: true }}
          options={props.purchaseOrderTypes}
          validate={required(I18nProvider)}
          required
          disabled={allDisabledFields}
        />
        <OnChange name={Constants.PURCHASE_ORDER_TYPE}>
          {(value, previous) => {
            handleClearItems(
              parentMutator,
              Constants.PURCHASE_ORDER_TYPE,
              previous,
              value
            );
          }}
        </OnChange>
      </Grid>
      <Grid item className={classes.Grid} xs={2}>
        <Field
          name={Constants.SUPPLIER_CODE}
          component={Select}
          label="Código Fornecedor"
          options={loadSupplierCodeOptions()}
          formControlProps={{ fullWidth: true }}
          InputLabelProps={{ shrink: true }}
          validate={required(I18nProvider)}
          required
          disabled={allDisabledFields}
        ></Field>
      </Grid>
      <Grid item className={classes.Grid} xs={2}>
        <Field
          name={Constants.SUPPLIER_NAME}
          component={Select}
          label="Fornecedor"
          options={loadSupplierNameOptions()}
          formControlProps={{ fullWidth: true }}
          InputLabelProps={{ shrink: true }}
          validate={required(I18nProvider)}
          required
          disabled={allDisabledFields}
        />
      </Grid>
      <Grid item className={classes.Grid} xs={2}>
        <Field
          name={Constants.SUPPLIER_TAX_NUMBER}
          component={Select}
          label="NIF"
          options={loadSupplierTaxOptions()}
          formControlProps={{ fullWidth: true }}
          InputLabelProps={{ shrink: true }}
          validate={required(I18nProvider)}
          required
          disabled={allDisabledFields}
        />
      </Grid>
      <Grid item className={classes.Grid} xs={2}>
        <Field
          name={Constants.VALUES_WO_VAT}
          component={NumberInput}
          label="Valor sem IVA"
          fullWidth
          currency={CurrencyType.EUR}
          InputLabelProps={{ shrink: true }}
          disabled
        />
      </Grid>
      <Grid item className={classes.Grid} xs={2}>
        <Field
          name={Constants.DESCRIPTION}
          label="Descrição"
          component={TextField}
          fullWidth
          InputLabelProps={{ shrink: true }}
          disabled={allDisabledFields}
        />
      </Grid>
      <Grid item className={classes.Grid} xs={2}>
        {/*TODO still need to assign own department */}
        <Field
          name={Constants.DEPARTMENT}
          component={Select}
          label="Departamento"
          formControlProps={{ fullWidth: true }}
          InputLabelProps={{ shrink: true }}
          options={props.departments}
          disabled={allDisabledFields}
          validate={required(I18nProvider)}
          required
        />
        <OnChange name={Constants.DEPARTMENT}>
          {(value, previous) => {
            handleClearItems(
              parentMutator,
              Constants.DEPARTMENT,
              previous,
              value
            );
          }}
        </OnChange>
      </Grid>
      <Grid item className={classes.Grid} xs={2}>
        <Field
          name={Constants.PROJECT}
          component={Select}
          label="Projeto"
          formControlProps={{ fullWidth: true }}
          InputLabelProps={{ shrink: true }}
          options={filterSelectInputValues(
            values,
            Constants.PROJECT,
            props.projects
          )}
          disabled={
            allDisabledFields
              ? allDisabledFields
              : handleInputEditableStatus(values, Constants.PROJECT)
          }
        />
        <OnChange name={Constants.PROJECT}>
          {(value, previous) => {
            handleClearItems(parentMutator, Constants.PROJECT, previous, value);
          }}
        </OnChange>
      </Grid>
      <Grid item className={classes.Grid} xs={2}>
        <Field
          name={Constants.INTERNAL_ORDER}
          component={Select}
          label="Ordem Interna"
          formControlProps={{ fullWidth: true }}
          InputLabelProps={{ shrink: true }}
          options={filterSelectInputValues(
            values,
            Constants.INTERNAL_ORDER,
            props.internalOrders
          )}
          disabled={
            allDisabledFields
              ? allDisabledFields
              : handleInputEditableStatus(values, Constants.INTERNAL_ORDER)
          }
        />
        <OnChange name={Constants.INTERNAL_ORDER}>
          {(value, previous) => {
            handleClearItems(
              parentMutator,
              Constants.INTERNAL_ORDER,
              previous,
              value
            );
          }}
        </OnChange>
      </Grid>
      <Grid item className={classes.Grid} xs={2}>
        <Field
          name={Constants.CREATION_DATE}
          label="Data de criação"
          component={TextField}
          type="date"
          fullWidth
          InputLabelProps={{ shrink: true }}
          disabled
        />
      </Grid>
      <Grid item className={classes.Grid} xs={2}>
        <Field
          name={Constants.LIMIT_DATE}
          label="Data limite aprovação"
          component={BasicDateField}
          min={minDateAproval}
          fullWidth
          InputLabelProps={{ shrink: true }}
          required
          validate={required(I18nProvider)}
          disabled={allDisabledFields}
        />
      </Grid>
    </Grid>
  );
};

const mapStateToProps = ({
  lovReducer: {
    lovs: { purchaseOrderTypes, departments, projects, tax, internalOrders },
  },
  journey: {
    services: { I18nProvider },
  },
}) => ({
  purchaseOrderTypes,
  departments,
  projects,
  tax,
  internalOrders,
  I18nProvider,
});

export default connect(mapStateToProps)(POBasicDataForm);
