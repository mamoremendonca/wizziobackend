import React, { Fragment } from "react";
import Icon from "@ui-lib/core/Icon";
import { withStyles } from "@ui-lib/core/styles";
import styles from "./styles";

const TableActions = props => {
  const { classes, editItem, deleteItem, copyItem, index } = props;

  return (
    <Fragment>
      {copyItem && (
        <Icon className={classes.Icon} onClick={() => copyItem(index)}>
          filter_none
        </Icon>
      )}
      <Icon className={classes.Icon} onClick={() => editItem(index)}>
        edit_outlined
      </Icon>
      {deleteItem && (
        <Icon className={classes.Icon} onClick={() => deleteItem(index)}>
          clear
        </Icon>
      )}
    </Fragment>
  );
};

export default withStyles(styles)(TableActions);
