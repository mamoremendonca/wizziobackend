import * as constants from "../constants";
import { Config } from "../config";
import {
  handleErrorRequest,
  ErrorMessages
} from "../utils/notificationHandler";

export const setTimeline = timelineData => {
  return {
    type: constants.SET_TIMELINE_DATA,
    payload: timelineData
  };
};

// GET PO INFO
export const getTimelineData = (
  HttpClient,
  Notifications,
  purchaseOrderNumber
) => {
  let config = {
    method: Config.METHOD_GET,
    url: `${Config.URL_PURCHASE_ORDER}${purchaseOrderNumber}${Config.TIMELINE_URL}`
  };
  return dispatch => {
    return HttpClient.request(config)
      .then(res => {
        dispatch(setTimeline(res.data));
      })
      .catch(() => {
        handleErrorRequest(Notifications, ErrorMessages.TIMELINE_GET_ERROR);
      });
  };
};

// ADD PO ATTACHMENT AND UPDATE TIMELINE
export const addTimelineAttachmentRequest = (
  HttpClient,
  attachment,
  purchaseOrderId,
  documentClass
) => {
  const data = new FormData();
  data.append("attachment", attachment);
  const config = {
    method: Config.METHOD_POST,
    url: `${Config.URL_PURCHASE_ORDER}${purchaseOrderId}${Config.ATTACHMENT_URL}?type=${documentClass}`,
    data
  };
  return HttpClient.request(config);
};

export const removeTimelineAttachmentRequest = (
  HttpClient,
  index,
  purchaseOrderId
) => {
  const config = {
    method: Config.METHOD_DELETE,
    url: `${Config.URL_PURCHASE_ORDER}${purchaseOrderId}${Config.ATTACHMENT_URL}${index}`
  };
  return HttpClient.request(config);
};
