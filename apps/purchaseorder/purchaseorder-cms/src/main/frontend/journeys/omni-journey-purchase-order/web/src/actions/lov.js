
import * as constants from '../constants';
/**
 * Used to load the common reference data that came from the service
 * @param {*} lovs 
 */
export const setLovs = (lovs) => {
	return {
		type: constants.SET_LOVS,
		payload: lovs
	}
}
/**
 * Used to load the suppliers that came from the service
 * @param {*} suppliers 
 */
export const setSuppliers= (suppliers) => {
    return {
      type: constants.SET_SUPPLIERS, 
      payload: suppliers
    }
}