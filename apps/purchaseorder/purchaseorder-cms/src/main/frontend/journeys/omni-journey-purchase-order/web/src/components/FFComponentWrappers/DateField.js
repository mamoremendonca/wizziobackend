import * as React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { connect } from "react-redux";
import Calendar from '@ui-lib/components/Calendar';
import styles from './styles';

const mapStateToProps = ({ journey: { services: { I18nProvider, I18nActions } }, i18n: { currentLanguage, languages } }) => ({ I18nProvider, I18nActions, languages });

const DateField = ({
    input: { name, onChange, value, ...restInput },
    meta,
    I18nProvider,
    languages,
    label,
    classes,
    ...rest
}) => {
    const showError =
        ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) &&
        meta.touched;

    return (
        <React.Fragment>
            <Calendar
                {...rest}
                className={classes.root}
                name={name}
                helperText={showError ? meta.error || meta.submitError : undefined}
                error={showError}
                inputProps={restInput}
                onChange={onChange}
                value={value}
                header={false}
                label={label}
                mode={"SINGLE"}
                collapsedDataFormat={"DD/MM/YYYY"}
                showNeighborMonthDates={true}
                openDatePickerOnClick={true}
                style={{ width: "unset" }}
                I18nProvider={I18nProvider}
            />
        </React.Fragment>
    );
};

export default connect(mapStateToProps)(withStyles(styles)(DateField));
