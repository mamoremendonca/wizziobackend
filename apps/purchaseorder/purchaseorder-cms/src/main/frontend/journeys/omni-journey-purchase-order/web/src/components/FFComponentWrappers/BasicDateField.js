import * as React from "react";

import TextField from "@ui-lib/core/TextField";

import { withStyles } from "@ui-lib/core/styles";
import styles from "./styles";

const BasicDateFieldWrapper = ({
  input: { name, onChange, value, ...restInput },
  meta,
  disabled,
  classes,
  required,
  min,
  ...rest
}) => {
  const showError =
    ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) &&
    meta.touched;

  return (
    <React.Fragment>
      <TextField
        {...rest}
        name={name}
        helperText={showError ? meta.error || meta.submitError : undefined}
        error={showError}
        inputProps={min ? { ...restInput, min: min } : restInput}
        onChange={onChange}
        type={"date"}
        required={disabled ? false : required}
        disabled={disabled}
        style={{ paddingTop: 5 }}
        value={value ? value : ""}
        className={disabled && classes.textFieldDisabled}
      />
    </React.Fragment>
  );
};

export default withStyles(styles)(BasicDateFieldWrapper);
