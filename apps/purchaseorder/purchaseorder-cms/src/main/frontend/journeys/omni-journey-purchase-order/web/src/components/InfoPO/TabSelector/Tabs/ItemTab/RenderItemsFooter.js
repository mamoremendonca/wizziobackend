import React from "react";
import Typography from "@ui-lib/core/Typography";
import { withStyles } from "@ui-lib/core/styles";
import { styles } from "./styles";
import { getLovOptionFromValue } from "../../../../../utils/lovUtils";
import { getFormattedNumber } from "../../../../../utils";

const RenderItemsFooter = ({ classes, items, taxLov }) => {
  let totalValue = 0;

  items &&
    items.forEach((item) => {
      const taxPercentage = parseFloat(
        getLovOptionFromValue(taxLov, item.taxCode).taxPercentage
      );
      totalValue =
        totalValue +
        item.amount * item.unitPriceWithoutTax * (1 + taxPercentage / 100);
    });

  return (
    <div className={classes.footerContainer}>
      <Typography className={classes.footerLabel}>Valor Total</Typography>
      <div>{getFormattedNumber(totalValue, true, "EUR")}</div>
    </div>
  );
};

export default withStyles(styles)(RenderItemsFooter);
