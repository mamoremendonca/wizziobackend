import React from "react";
import { withRouter } from "react-router";

import { withStyles } from "@ui-lib/core/styles";
import { styles, ComponentName } from "./DetailsPOScreenStyle";

import Grid from "@ui-lib/core/Grid";
import Button from "@ui-lib/core/Button";

import InfoPO from "../../components/InfoPO/InfoPO";
import TabSelector from "../../components/InfoPO/TabSelector";
import FooterPO from "../../components/InfoPO/FooterPO";
import RenderItemsArray from "../../components/InfoPO/TabSelector/Tabs/ItemTab/RenderItemsArray";
import RenderItemsFooter from "../../components/InfoPO/TabSelector/Tabs/ItemTab/RenderItemsFooter";
import ConfirmationDialog from "../../components/Dialog/ConfirmationDialog";

import Timeline from "../../components/InfoPO/Timeline";

function DetailsPOScreenRender({
  I18nProvider,
  classes,
  purchaseOrder,
  lovs,
  suppliers,
  actionClickHandler,
  approvalDecisionChangeHandler,
  commentDecisionChangeHandler,
  confirmationDialogOptions,
  timelineData,
  handleRefreshTimeline,
  showError,
  comment,
  decision,
  handleBackClick,
  isFromWorkList,
  handleManagePo,
  handleCopyPo,
  addAttachment,
  removeAttachment,
  someAttachmentIsLoading,
}) {
  const tabs = [
    I18nProvider.Texti18n("details_detail_tab"),
    /*  I18nProvider.Texti18n("details_invoice_tab"),
    I18nProvider.Texti18n("details_relatedPo_tab") */
  ];

  // const tabs = ["Detalhe", "Faturas", "PO Relacionadas"];

  const tabsData = {
    Detalhe: {
      title: I18nProvider.Texti18n("po_items"),
      render: (
        <RenderItemsArray
          lovs={lovs}
          purchaseOrderItems={purchaseOrder ? purchaseOrder.items : []}
          purchaseOrderType={
            purchaseOrder ? purchaseOrder.purchaseOrderHeader.type : "-"
          }
        />
      ),
      footer: (
        <RenderItemsFooter
          items={purchaseOrder ? purchaseOrder.items : []}
          taxLov={lovs.tax}
        />
      ),
    },
    /* Faturas: {
      title: I18nProvider.Texti18n("details_invoice_tab")
    },
    "PO Relacionadas": {
      title: I18nProvider.Texti18n("details_relatedPo_tab")
    } */
  };

  const isApproving =
    purchaseOrder.workFlowAvailableActions &&
    purchaseOrder.workFlowAvailableActions.length > 0;

  const canClaim = purchaseOrder.canClaim;

  return (
    <React.Fragment>
      <Grid container spacing={0}>
        {/* LEFT SIDE - PO INFO */}
        <Grid item xs={9} className={classes.leftContainer}>
          <div className={classes.footerButtonsContainer}>
            {canClaim && !isApproving && (
              <Button
                color="primary"
                variant="contained"
                onClick={handleManagePo}
              >
                {I18nProvider.Texti18n("po_manage")}
              </Button>
            )}

            {!isApproving && (
              <Button
                className={classes.copyButton}
                color="primary"
                variant="contained"
                onClick={handleCopyPo}
              >
                {I18nProvider.Texti18n("po_copy")}
              </Button>
            )}
          </div>

          <InfoPO
            purchaseOrder={purchaseOrder}
            lovs={lovs}
            suppliers={suppliers}
          />
          <TabSelector tabs={tabs} tabsData={tabsData} />
          <FooterPO
            isApproving={isApproving}
            showError={showError}
            comment={comment}
            decision={decision}
            approvalDecisionChangeHandler={approvalDecisionChangeHandler}
            commentDecisionChangeHandler={commentDecisionChangeHandler}
            availableActions={purchaseOrder.workFlowAvailableActions || []}
            purchaseOrderHeader={purchaseOrder.purchaseOrderHeader}
            I18nProvider={I18nProvider}
          />
          <div className={classes.footerButtonsContainer}>
            {!isApproving && (
              <Button
                variant="contained"
                color="primary"
                onClick={() => handleBackClick()}
              >
                {I18nProvider.Labeli18n("return")}
              </Button>
            )}
            {isApproving && (
              <div className={classes.approvalActionsContainer}>
                <Button
                  className={classes.cancelButton}
                  onClick={() => actionClickHandler(false)}
                >
                  {I18nProvider.Labeli18n("details_po.cancel.button")}
                </Button>
                {purchaseOrder.workFlowAvailableActions.length > 0 && (
                  <Button
                    disabled={
                      someAttachmentIsLoading ||
                      !decision ||
                      (decision !== "APPROVE" && !comment)
                    }
                    color="primary"
                    variant="contained"
                    onClick={() => actionClickHandler(true)}
                  >
                    {I18nProvider.Labeli18n("submit")}
                  </Button>
                )}
              </div>
            )}
          </div>
        </Grid>
        {/* RIGHT SIDE - TIMELINE */}
        <Grid item xs={3} className={classes.rightContainer}>
          {timelineData && (
            <Timeline
              timelineData={timelineData}
              I18nProvider={I18nProvider}
              handleRefreshTimeline={handleRefreshTimeline}
              isFromWorkList={isFromWorkList}
              addAttachment={addAttachment}
              removeAttachment={removeAttachment}
              someAttachmentIsLoading={someAttachmentIsLoading}
            />
          )}
        </Grid>
      </Grid>
      <ConfirmationDialog
        open={confirmationDialogOptions.isOpen}
        title={confirmationDialogOptions.title}
        onClose={(isOkButtonClick) =>
          confirmationDialogOptions.closeHandler(isOkButtonClick)
        }
      />
    </React.Fragment>
  );
}

export default withStyles(styles, { name: ComponentName })(
  withRouter(DetailsPOScreenRender)
);
