import React from "react";
import AsyncSelect from 'react-select/async';
import { components } from 'react-select';
import FormControl from "@ui-lib/core/FormControl";
import InputLabel from "@ui-lib/core/InputLabel";
import FormHelperText from "@ui-lib/core/FormHelperText";

const customStyles = {
  option: (provided) => ({
    ...provided,
  }),
  control: (provided, state) => {
    return (
      {
        ...provided,
        marginTop: 12,
        borderTop: state.isSelected || state.isFocused ? 0 : 0,
        borderLeft: 0,
        borderRight: 0,
        borderBottom: "1px solid #8e8e8e;",
        boxShadow: state.isSelected || state.isFocused ? 0 : 0,
        backgroundColor: "transparent",
        borderRadius: 0,
        '&:hover': {
          borderBottom: "2px solid black",
          cursor: "pointer"
        }
      })
  },
  singleValue: (provided) => (

    { ...provided, backgroundColor: "transparent" }
  ),
  valueContainer: (provided) => ({ ...provided, padding: '-2px -8px' })

}

const DropdownIndicator = props => {
  return (
    components.DropdownIndicator && (
      <components.DropdownIndicator {...props}>
        <i className="material-icons" style={{ color: "rgba(0, 0, 0, 0.54)" }}>search</i>
      </components.DropdownIndicator>
    )
  );
};

const IndicatorSeparator = ({ innerProps }) => {
  return <span {...innerProps} />;
};

const AsyncSelectWrapper = ({
  input: { name, value, onChange, ...restInput },
  meta,
  label,
  options,
  formControlProps,
  inputLabelProps,
  ...rest
}) => {
  const showError =
    ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) &&
    meta.touched;

  return (
    <FormControl {...formControlProps} error={showError}>
      <InputLabel htmlFor={name} {...inputLabelProps} shrink={true}>
        {label}
      </InputLabel>

      <AsyncSelect         {...rest}
        name={name}
        onChange={onChange}
        inputProps={restInput}
        value={value}
        loadOptions={options}
        styles={customStyles}
        placeholder={"Escolher ..."}
        components={{ DropdownIndicator, IndicatorSeparator }} />

      {showError && (
        <FormHelperText>{meta.error || meta.submitError}</FormHelperText>
      )}
    </FormControl>
  );
};


export default AsyncSelectWrapper;
