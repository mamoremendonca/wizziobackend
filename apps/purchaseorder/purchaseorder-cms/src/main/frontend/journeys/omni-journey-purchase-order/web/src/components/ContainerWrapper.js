/**
 * This component should be used has a wrapper (this is a HOC) for container
 * components.
 * It injects parts of the state provided by UFE, the Router props (history,
 * location, match,...), and sets the journey subtitle according to the current
 * path maintained internally by the MemoryRouter.
 */

import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { updateJourneySubtitle } from "../Routes";

export default (ComposedComponent) => {
  class ReduxContainer extends React.PureComponent {
    //To update the subtitle as soon as the component is mount.
    componentDidMount() {
      const { location, JourneyActions, JourneySettings } = this.props;
      updateJourneySubtitle(location.pathname, JourneyActions, JourneySettings);
    }

    //To update the the subtitle after the purchase order is loaded.
    componentDidUpdate(prevProps) {
      const {
        location,
        JourneyActions,
        JourneySettings,
        purchaseOrderData,
        I18nProvider,
      } = this.props;
      if (purchaseOrderData !== prevProps.purchaseOrderData) {
        updateJourneySubtitle(
          location.pathname,
          JourneyActions,
          JourneySettings,
          purchaseOrderData,
          I18nProvider
        );
      }
    }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }
  const mapStateToProps = ({
    journey: {
      services: { I18nProvider, JourneyActions },
      settings,
      i18n,
    },
    purchaseOrderReducer: { purchaseOrderData },
  }) => ({
    i18n, //too update textI18n when the current language changes.
    I18nProvider,
    JourneyActions,
    JourneySettings: settings,
    purchaseOrderData,
  });
  return connect(mapStateToProps)(withRouter(ReduxContainer));
};
