import { Config } from "../config";
import {
  handleErrorRequest,
  ErrorMessages
} from "../utils/notificationHandler";
import { setPurchaseOrder } from "./purchase-order";
import { setTimeline } from "./timeline";

export const getPurchaseOrderDataFromRepository = (
  HttpClient,
  Notifications,
  purchaseOrderNumber
) => {
  let config = {
    method: Config.METHOD_GET,
    url: `${Config.URL_PURCHASE_REPOSITORY}${purchaseOrderNumber}`
  };
  return dispatch => {
    return HttpClient.request(config)
      .then(res => {
        dispatch(setPurchaseOrder(res.data));
      })
      .catch(error => {
        handleErrorRequest(Notifications, ErrorMessages.PO_GET_ERROR);
      });
  };
};

export const getTimelineFromRepository = (
  HttpClient,
  Notifications,
  instanceId
) => {
  let config = {
    method: Config.METHOD_GET,
    url: `${Config.URL_PURCHASE_REPOSITORY}${instanceId}${Config.TIMELINE_URL}`
  };
  return dispatch => {
    return HttpClient.request(config)
      .then(res => {
        dispatch(setTimeline(res.data));
      })
      .catch(() => {
        handleErrorRequest(Notifications, ErrorMessages.TIMELINE_GET_ERROR);
      });
  };
};
