const styles = theme => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing.unit * 4,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  Grid: {
    minHeight: 100,
  }
});

export default styles;
