import { prepareSearchCriteria, prepareSortCriteria} from "../../utils";
import {setPurchaseOrderList} from "../../actions";
import {Config} from "../../config";

/// Add all the methods that requires to communicate with the server.
export const getPOListProcesses = (httpClient, criteria, widget) => {
    const config = {
        method: "post",
        url: Config.POST_SEARCH_REPOSITORY_URL + prepareSortCriteria(criteria.sortCriteria),
        data: prepareSearchCriteria(criteria)
    };
    return (dispatch) => {
        return httpClient.request(config).then((res) => {
            //Todo Logic of mapping processes and totals for each process
            const payLoad= {
                processes: res.data,
                numberOfRecords: res.data.length,
                totals: res.data.length
            }
            dispatch(setPurchaseOrderList(payLoad));
        }).catch((err) => {
            //TODO: httpErrorHandler(widget, err, false);
        });
    }
};
