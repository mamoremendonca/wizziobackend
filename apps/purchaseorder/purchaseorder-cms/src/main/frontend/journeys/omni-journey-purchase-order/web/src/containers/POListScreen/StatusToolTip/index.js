import React from "react";
import Typography from "@ui-lib/core/Typography";
import { styles, ComponentName } from "./StatusToolTipStyles";
import { withStyles } from "@ui-lib/core/styles";
import { PURCHASE_ORDER_STATUS } from "../../../constants";

const stallColor = stall => {
	switch (stall) {
		case PURCHASE_ORDER_STATUS.IN_APPROVAL:
			return "#E79208";
		case PURCHASE_ORDER_STATUS.APPROVED:
			return "#81BC00";
		case PURCHASE_ORDER_STATUS.SAP_SUBMITTED:
			return "#6F1D46";
		case PURCHASE_ORDER_STATUS.REJECTED:
			return "#26AEE5";
		case PURCHASE_ORDER_STATUS.CANCELED:
			return "#002B49";
		default:
	}
};

const StatusTooltip = ({ i18nProvider, classes }) => {
	return (
		<div key="status_description" className={classes.container}>
			<Typography variant="body2" className={classes.title}>
				{i18nProvider.Texti18n("list_po.column.status.header.tool_tip.status_description")}
			</Typography>
			<div key={PURCHASE_ORDER_STATUS.IN_APPROVAL} className={classes.item}>
				<div
					style={{ backgroundColor: stallColor(PURCHASE_ORDER_STATUS.IN_APPROVAL) }}
					className={classes.statusIndicator}
				/>
				<Typography variant="caption">
					{i18nProvider.Texti18n("list_po.column.status.header.tool_tip.status_in_approval")}
				</Typography>
			</div>
			<div key={PURCHASE_ORDER_STATUS.APPROVED} className={classes.item}>
				<div
					style={{ backgroundColor: stallColor(PURCHASE_ORDER_STATUS.APPROVED) }}
					className={classes.statusIndicator}
				/>
				<Typography variant="caption">
					{i18nProvider.Texti18n("list_po.column.status.header.tool_tip.status_approved")}
				</Typography>
			</div>
			<div key={PURCHASE_ORDER_STATUS.SAP_SUBMITTED}className={classes.item}>
				<div
					style={{ backgroundColor: stallColor(PURCHASE_ORDER_STATUS.SAP_SUBMITTED) }}
					className={classes.statusIndicator}
				/>
				<Typography variant="caption">
					{i18nProvider.Texti18n("list_po.column.status.header.tool_tip.status_sap_submitted")}
				</Typography>
			</div>
			<div key={PURCHASE_ORDER_STATUS.REJECTED} className={classes.item}>
				<div
					style={{ backgroundColor: stallColor(PURCHASE_ORDER_STATUS.REJECTED) }}
					className={classes.statusIndicator}
				/>
				<Typography variant="caption">
					{i18nProvider.Texti18n("list_po.column.status.header.tool_tip.status_rejected")}
				</Typography>
			</div>
			<div key={PURCHASE_ORDER_STATUS.CANCELED} className={classes.item}>
				<div
					style={{ backgroundColor: stallColor(PURCHASE_ORDER_STATUS.CANCELED) }}
					className={classes.statusIndicator}
				/>
				<Typography variant="caption">
					{i18nProvider.Texti18n("list_po.column.status.header.tool_tip.status_canceled")}
				</Typography>
			</div>
		</div>
	);
};

export default withStyles(styles, { name: ComponentName })(StatusTooltip);


