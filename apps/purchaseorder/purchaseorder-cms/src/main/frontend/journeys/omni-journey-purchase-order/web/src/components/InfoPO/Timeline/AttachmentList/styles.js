const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.grey[100],
    height: "100%"
  },
  attachmentStatusSection: {
    padding: theme.spacing.unit * 2
  },
  singleAttachmentContainer: {
    backgroundColor: theme.palette.common.white,
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px`,
    margin: `${theme.spacing.unit * 2}px 0px`
  },
  singleAttachmentHeader: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingBottom: theme.spacing.unit * 2
  },
  singleAttachmentTitle: {
    paddingLeft: theme.spacing.unit,
    "&:hover": {
      color: theme.palette.primary.main,
      textDecorationLine: "underline"
    }
  },
  statusHeader: {
    width: theme.spacing.unit / 2,
    height: theme.spacing.unit / 2,
    backgroundColor: theme.palette.grey[500],
    borderRadius: "50%",
    margin: `0px ${theme.spacing.unit * 3}px`
  },
  flexRowContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center"
  },
  deleteAttachment:{
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  closeIcon: {
    fontSize: 24,
    cursor: "pointer",
    padding: 5,
    bottom: 5,
    "&:hover": {
      color: theme.palette.primary.main 
    }
  },
  progress: {
    marginBottom: `${theme.spacing.unit * 2}px` ,
    width: `${theme.spacing.unit * 3}px !important`,
    height: `${theme.spacing.unit * 3}px !important`,
  }

});

export default styles;
