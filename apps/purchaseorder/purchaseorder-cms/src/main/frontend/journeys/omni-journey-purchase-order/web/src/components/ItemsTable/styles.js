const styles = theme => (
  {
    root: {
      width: "100%",
      overflowX: "auto",
      boxShadow: 'none'
    },
    table: {
      minWidth: 700
    },
    header: {
      ...theme.typography.subtitle1,
      color: theme.palette.primary.main,
      fontFamily: 'inherit',
      border: 'none',
      minWidth: 100
    },
    row: {
      backgroundColor: 'white',
      '&:hover': {
        backgroundColor: 'white !important'
      },
      border: 'none'
    },
    Icon: {
      cursor: 'pointer',
      margin: `0 ${theme.spacing.unit}px`
    }
  }
);

export default styles;
