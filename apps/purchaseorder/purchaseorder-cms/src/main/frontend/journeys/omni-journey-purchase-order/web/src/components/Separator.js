import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import Typography from "@ui-lib/core/Typography";

const styles = theme => {
  return {
    root: {
      width: "100%",
      borderBottom: `1px solid ${theme.palette.text.secondary}`,
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-start"
    },
    childrenContainer: {
      flexGrow: 1,
      marginLeft: theme.spacing.unit
    }
  };
};
const Separator = ({ text, classes, ...props }) => {
  return (
    <div className={classes.root}>
      <Typography variant="h4" color="textSecondary" gutterBottom>
        {text}
      </Typography>
      <div className={classes.childrenContainer}>{props.children}</div>
    </div>
  );
};

export default withStyles(styles)(Separator);
