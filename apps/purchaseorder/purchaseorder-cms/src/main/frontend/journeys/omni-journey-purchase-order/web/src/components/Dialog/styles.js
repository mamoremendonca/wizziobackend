const styles = theme => ({
  headerContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  Icon: {
    cursor: "pointer",
    margin: `0 ${theme.spacing.unit}px`
  }
});

export default styles;
