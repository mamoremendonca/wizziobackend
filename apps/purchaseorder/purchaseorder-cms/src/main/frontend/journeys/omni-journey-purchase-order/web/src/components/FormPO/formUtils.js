import {
  ITEM,
  DESCRIPTION,
  QUANTITY,
  UNIT_VALUE,
  TOTAL_AMOUNT_WO_VAT,
  TOTAL_AMOUNT_W_VAT,
  TAX,
  COST_CENTER,
  COMMENT,
  START_DATE,
  END_DATE,
  ITEM_TYPE,
  FIXEDASSET,
  INDEX
} from "../ItemForm/constants";
import { Config } from "../../config";

// Mapping item values to be sent to the Database
export const mapValuesToItem = purchaseOrderItem => ({
  code: purchaseOrderItem.item
    ? purchaseOrderItem.item.value
    : purchaseOrderItem.fixedAsset
    ? purchaseOrderItem.fixedAsset.value
    : undefined,
  description: purchaseOrderItem.description ? purchaseOrderItem.description : undefined,
  costCenterCode: purchaseOrderItem.costCenter ? purchaseOrderItem.costCenter.value : undefined,
  amount: purchaseOrderItem.quantity,
  unitPriceWithoutTax: purchaseOrderItem.unitValue,
  taxCode: purchaseOrderItem.tax ? purchaseOrderItem.tax.value : undefined,
  startDate: purchaseOrderItem.startDate ? new Date(purchaseOrderItem.startDate) : undefined,
  endDate: purchaseOrderItem.endDate ? new Date(purchaseOrderItem.endDate) : undefined,
  comment: purchaseOrderItem.comment ? purchaseOrderItem.comment : undefined
});

export const addItemToFields = (values, index, fields) => {
  fields.push({
    [FIXEDASSET]: values[FIXEDASSET],
    [ITEM_TYPE]: values[ITEM_TYPE],
    [ITEM]: values[ITEM],
    [DESCRIPTION]: values[DESCRIPTION],
    [QUANTITY]: values[QUANTITY],
    [UNIT_VALUE]: values[UNIT_VALUE],
    [TOTAL_AMOUNT_WO_VAT]: values[TOTAL_AMOUNT_WO_VAT],
    [TOTAL_AMOUNT_W_VAT]: values[TOTAL_AMOUNT_W_VAT],
    [TAX]: values[TAX],
    [COST_CENTER]: values[COST_CENTER],
    [COMMENT]: values[COMMENT],
    [START_DATE]: values[START_DATE],
    [END_DATE]: values[END_DATE],
    [INDEX]: index
  });
  return fields;
};

export const editItemField = (values, selectedItemIndex, fields) => {
  const updatedField = {
    [FIXEDASSET]: values[FIXEDASSET],
    [ITEM_TYPE]: values[ITEM_TYPE],
    [ITEM]: values[ITEM],
    [DESCRIPTION]: values[DESCRIPTION],
    [QUANTITY]: values[QUANTITY],
    [UNIT_VALUE]: values[UNIT_VALUE],
    [TOTAL_AMOUNT_WO_VAT]: values[TOTAL_AMOUNT_WO_VAT],
    [TOTAL_AMOUNT_W_VAT]: values[TOTAL_AMOUNT_W_VAT],
    [TAX]: values[TAX],
    [COST_CENTER]: values[COST_CENTER],
    [COMMENT]: values[COMMENT],
    [START_DATE]: values[START_DATE],
    [END_DATE]: values[END_DATE],
    [INDEX]: values[INDEX]
  };
  fields[selectedItemIndex] = updatedField;
  return fields;
};

// TODO MOVE SERVICE REQUESTS
export const addPurchaseOrderItemRequest = (
  HttpClient,
  item,
  purchaseOrderId
) => {
  let config = {
    method: Config.METHOD_POST,
    url: `${Config.URL_PURCHASE_ORDER}${purchaseOrderId}${Config.ITEM_URL}`,
    data: mapValuesToItem(item)
  };
  return HttpClient.request(config);
};

export const updatePurchaseOrderItemRequest = (
  HttpClient,
  item,
  purchaseOrderId
) => {
  let config = {
    method: Config.METHOD_PUT,
    url: `${Config.URL_PURCHASE_ORDER}${purchaseOrderId}${Config.ITEM_URL}${item.index}`,
    data: mapValuesToItem(item)
  };
  return HttpClient.request(config);
};

export const removePurchaseOrderItemRequest = (
  HttpClient,
  index,
  purchaseOrderId
) => {
  let config = {
    method: Config.METHOD_DELETE,
    url: `${Config.URL_PURCHASE_ORDER}${purchaseOrderId}${Config.ITEM_URL}${index}`
  };
  return HttpClient.request(config);
};

export const addPurchaseOrderAttachmentRequest = (
  HttpClient,
  attachment,
  purchaseOrderId,
  documentClass
) => {
  const data = new FormData();
  data.append("attachment", attachment);

  let config = {
    method: Config.METHOD_POST,
    url: `${Config.URL_PURCHASE_ORDER}${purchaseOrderId}${Config.ATTACHMENT_URL}?type=${documentClass}`,
    data
  };
  return HttpClient.request(config);
};

export const removePurchaseOrderAttachmentRequest = (
  HttpClient,
  index,
  purchaseOrderId
) => {
  let config = {
    method: Config.METHOD_DELETE,
    url: `${Config.URL_PURCHASE_ORDER}${purchaseOrderId}${Config.ATTACHMENT_URL}${index}`
  };
  return HttpClient.request(config);
};

/**
 * returns all the attachments added in the draft.
 * data[0] correspond to the first step of the process
 * @param {*} HttpClient 
 * @param {*} instanceId 
 */
export const  getDraftAttachmentsFromTimeline = async (HttpClient, instanceId) =>{
  const config = {
    method: Config.METHOD_GET,
    url: `${Config.URL_PURCHASE_ORDER}${instanceId}${Config.TIMELINE_URL}`
  };
  try {
    const response = await HttpClient.request(config);

    return response.data[0].attachments
  } catch (error) {
    return [];
  }
}