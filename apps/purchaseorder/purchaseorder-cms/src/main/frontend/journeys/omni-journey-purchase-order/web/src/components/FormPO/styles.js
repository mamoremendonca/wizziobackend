const styles = theme => ({
  root: {
    margin: theme.spacing.unit * 2
  },
  rootPaper: {
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.common.white,
  },
  formSection: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 6
  },
  formSectionItems: {
    marginTop: theme.spacing.unit,
  },
  formSectionAttachments: {
    marginTop: theme.spacing.unit * 3
  },
  buttonContainer: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 3
  },
  priceSubsection: {
    margin: `${theme.spacing.unit * 2}px 0`,
  },
  priceLabel: {
    ...theme.typography.subtitle1,
    color: theme.palette.primary.main,
  },
  priceValue: {
    ...theme.typography.subtitle2,
    display: 'flex'
  },
  Grid: {
    minHeight: 80,
    margin: `${theme.spacing.unit}px ${theme.spacing.unit * 17}px ${theme.spacing.unit}px 0`
  },
  buttonIcon: {
    cursor: "pointer",
    "&:hover": {
      color: theme.palette.primary.main
    }
  },
  InputFile: { display: "none", cursor: "pointer" },
  buttonIconDisabled: {
    pointerEvents: "none",
    cursor: "not-allowed",
    color: "#ccc",
    "&:hover": {
      color: "#ccc"
    }
  },
});

export default styles;
