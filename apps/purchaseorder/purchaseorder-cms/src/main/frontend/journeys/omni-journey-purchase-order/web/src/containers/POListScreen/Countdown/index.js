import React from "react";
import Typography from "@ui-lib/core/Typography";
import { styles, ComponentName } from "./CountdownStyles";
import { withStyles } from "@ui-lib/core/styles";
import { PURCHASE_ORDER_STATUS } from "../../../constants";

const CountdownRender = ({ classes, dateToCompare, i18nProvider, status }) => {
    const actualDate = new Date();
    const compareDate = new Date(dateToCompare);
    //Convert to UTC
    const actualDateUTC = Date.UTC(actualDate.getFullYear(), actualDate.getMonth(), actualDate.getDate());
    const compareDateUTC = Date.UTC(compareDate.getFullYear(), compareDate.getMonth(), compareDate.getDate());

    const diffTime = compareDateUTC - actualDateUTC;
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
   
    if (diffDays > 7 || status !== PURCHASE_ORDER_STATUS.IN_APPROVAL) {
        return <React.Fragment />;
    } else if (diffDays >= 0) {
        return (
            <Typography
                variant="h6"
                className={classes.dateDiffAbove}
            >{`${"+"} ${Math.abs(diffDays)} ${i18nProvider.Texti18n('countdown.days')}`}</Typography>
        );
    } else if (diffDays < 0) {
        return (
            <Typography
                variant="h6"
                className={classes.dateDiffBelow}
            >{`${"-"} ${Math.abs(diffDays)} ${i18nProvider.Texti18n('countdown.days')}`}</Typography>
        );
    }
};

export default withStyles(styles, { name: ComponentName })(CountdownRender);