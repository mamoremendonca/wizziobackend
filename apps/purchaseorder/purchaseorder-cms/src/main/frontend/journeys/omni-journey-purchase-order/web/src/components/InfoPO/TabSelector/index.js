import React, { Component } from "react";

import { withStyles } from "@ui-lib/core/styles";
import styles from "./styles";

// import Typography from "@ui-lib/core/Typography";

class TabSelector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: props.tabs[0]
    };
  }

  setSelectedTab = tabName => {
    this.setState({ selectedTab: tabName });
  };

  renderTab = tab => {
    // const { classes } = this.props;
    return (
      <div onClick={() => this.setSelectedTab(tab)} key={tab}>
        {/* <Typography
          variant="h3"
          className={
            tab === this.state.selectedTab
              ? classes.tabTitleSelected
              : classes.tabTitle
          }
        >
          {tab}
        </Typography> */}
      </div>
    );
  };

  render() {
    const { classes, tabs, tabsData } = this.props;
    return (
      <div>
        <div className={classes.root}>
          {tabs.map(tab => this.renderTab(tab))}
        </div>
        <div className={classes.tabDataContainer}>
          <div>{tabsData[this.state.selectedTab].title}</div>
          <hr></hr>
          <div>{tabsData[this.state.selectedTab].render}</div>
          {tabsData[this.state.selectedTab].footer && (
            <div>{tabsData[this.state.selectedTab].footer}</div>
          )}
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(TabSelector);
