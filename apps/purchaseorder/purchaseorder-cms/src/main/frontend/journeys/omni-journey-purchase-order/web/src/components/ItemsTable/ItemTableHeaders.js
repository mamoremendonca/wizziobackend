import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import styles from "./styles";
import TableCell from "@ui-lib/core/TableCell";
import { PO_CAPEX, PO_CONTRACT } from "../../constants";

const fixedAssetHeaders = [
  "Imobilizado",
  "Descritivo",
  "Quantidade",
  "Valor unit",
  "Valor s/IVA",
  "IVA (%)",
  "Valor c/IVA",
  "Comentário",
];
const itemHeaders = [
  "Tipo de Artigo",
  "Nº Artigo",
  "Descritivo",
  "Quantidade",
  "Valor unit",
  "Valor s/IVA",
  "IVA (%)",
  "Valor c/IVA",
  "Centro de Custo",
  "Comentário",
];

const contractHeaders = [
  "Tipo de Artigo",
  "Nº Artigo",
  "Descritivo",
  "Quantidade",
  "Valor unit",
  "Valor s/IVA",
  "IVA (%)",
  "Valor c/IVA",
  "Centro de Custo",
  "Data de Inicio",
  "Data de Fim",
  "Comentário",
];

const ItemTableHeaders = ({ classes, type, isDetails }) => {
  const tableHeaders =
    type === PO_CAPEX
      ? fixedAssetHeaders
      : type === PO_CONTRACT
      ? contractHeaders
      : itemHeaders;
  return (
    <>
      {isDetails && (
        <TableCell className={classes.header} align="center">
          Versão
        </TableCell>
      )}
      {tableHeaders.map((header) => (
        <TableCell className={classes.header} align="center" key={header}>
          {header}
        </TableCell>
      ))}
    </>
  );
};
export default withStyles(styles)(ItemTableHeaders);
