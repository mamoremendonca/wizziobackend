import React, { Component } from "react";
import withContainerWrapper from "../../components/ContainerWrapper";
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import POListScreenRender from "./POListScreenRender";
import { POListHeaders } from "./TableHeaders";
import AppRoutes from "../../Routes";
import {getPOListProcesses, updateSorting} from "../../actions";

/**
 * Screen used to display the created Purchase Orders.
 * In this screen you can:
 ** Create new POs  
 ** See the details of an existent PO.
 ** Filter the data.
 */
class POListScreen extends Component {

	state = {
		expandFilter: false,
		tableApi:undefined
	};

	componentDidMount () {
		const {searchCriteria} = this.props;
		this.getPurchaseOrdersFromServer(searchCriteria);
	};

	componentDidUpdate (prevProps, prevState){
		const {searchCriteria} = this.props;
		if(this.state.tableApi && prevState.tableApi !== this.state.tableApi){
			this.state.tableApi.setSortedFields([{
				key : searchCriteria.sortCriteria.orderField,
				direction: searchCriteria.sortCriteria.orderType
			}]);
		}
	};

	/**
	 * Logic to navigate between containers.
	 */
	navigateToPage = (page, payload) => {
		const { history } = this.props;
		switch (page) {
			case 'createPO':
				history.push(AppRoutes.CREATE.path);
				break;
			case 'detailsPO':
				history.push(AppRoutes.DETAILS.path + payload);
				break;
			default:
				break;
		}
	};

	/**
	 * toggle the expand filter
	 */
	handleExpandFilterClick = () => {
		this.setState({ expandFilter: !this.state.expandFilter });
	}

	/**
	 * To navigate to the PO details screen
	 */
	handleRowClick = (row) => {
		this.navigateToPage('detailsPO', row.id);
	}

	/**
	 * Makes a request to the server to fetch the PO data
	 * the service must return the total existent records.
	 */
	getPurchaseOrdersFromServer = (searchCriteria) => {
		const {getPOListProcesses, HttpClient} = this.props;
		getPOListProcesses(HttpClient, searchCriteria);
	};

	/**
	 * the onSort method returned by the custom table as
	 * has the follow arguments headerkey, directionResponseOk, directionResponseError
	 * direction can be asc or desc
	 */
	sortHandler = (headerKey, directionResponseOk, resolve, error) =>{
		const {updateSorting, searchCriteria} = this.props;
		const sorting = {...searchCriteria.sortCriteria};
		sorting.orderField = headerKey;
		// hack to solve problem with arrow sort.
		if(searchCriteria.sortCriteria.orderType === 'desc' && directionResponseOk === 'desc'){
			sorting.orderType= 'asc';
			error();
		} else {
			sorting.orderType= directionResponseOk
			resolve();
		}
		updateSorting({...sorting})
		resolve();
		this.getPurchaseOrdersFromServer({...searchCriteria, sortCriteria: {...sorting}});		
		
	}

	setTableApi = api => {
		this.setState({tableApi: api});
	}

	render() {
		const { I18nProvider, purchaseOrderList, totalPurchaseOrders, 
			numberOfRecords, lovs, suppliers 
		} = this.props;
		return (
			<POListScreenRender
				I18nProvider={I18nProvider}
				expandFilter={this.state.expandFilter}
				navigateToPage={this.navigateToPage}
				onRowClick={this.handleRowClick}
				onExpandFilterClick={this.handleExpandFilterClick}
				tableHeaders={POListHeaders(I18nProvider)}
				onFilterClick = {this.getPurchaseOrdersFromServer}
				sortHandler = {this.sortHandler}
				purchaseOrderList = {purchaseOrderList}
				totalPurchaseOrders = {totalPurchaseOrders}
				numberOfRecords = {numberOfRecords}
				lovs = {{referenceData:lovs, suppliers}}
				requestFilterData = {this.getPurchaseOrdersFromServer}
				setTableApi = {this.setTableApi}
				tableApi = {this.state.tableApi}
			/>
		);
	}
}
/**
 * i18n is been observed only to render when the current language is changed.
 * @param {*} param0 
 */
const mapStateToProps = ({ 
	journey: { services: { I18nProvider, HttpClient} }, 
	i18n,
	purchaseOrderReducer: {purchaseOrderList, searchCriteria, totalPurchaseOrders, numberOfRecords },
	lovReducer:{lovs, suppliers}
}) => ({ 
	I18nProvider, i18n, HttpClient, 
	purchaseOrderList, searchCriteria, totalPurchaseOrders, numberOfRecords,
	lovs, suppliers
});
const mapDispatchToProps= dispatch => bindActionCreators({
	getPOListProcesses,
	updateSorting
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withContainerWrapper(POListScreen));