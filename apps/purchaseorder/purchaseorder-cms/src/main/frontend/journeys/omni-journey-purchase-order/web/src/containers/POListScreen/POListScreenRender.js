import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { styles, ComponentName } from "./POListScreenStyle";
import Button from "@ui-lib/core/Button";
import Tooltip from "@ui-lib/core/Tooltip";
//import Tooltip from '@material-ui/core/Tooltip';
import Icon from "@ui-lib/core/Icon";
import IconButton from "@ui-lib/core/IconButton";
import Grid from "@ui-lib/core/Grid";
import HeaderBar from "../../components/HeaderBar";
import FilterList from "../../components/FilterList";
import classNames from "@ui-lib/core/classnames";
import CustomTable from "@ui-lib/custom-components/CustomTable";
import Collapse from "@ui-lib/core/Collapse";
import Typography from "@ui-lib/core/Typography";
import { PURCHASE_ORDER_STATUS } from "../../constants";
import Countdown from "./Countdown";
import { getFormattedNumber } from "../../utils";
/**
 * Return a jsx component for the status(Estado) column to add in the row of PO list table
 */

const getStatusVisualComponent = (classes, status) => {
  switch (status) {
    case PURCHASE_ORDER_STATUS.IN_APPROVAL:
      return (
        <div className={classes.statsAlign}>
          <div className={classes.waitingApprovalStatus} />
        </div>
      );
    case PURCHASE_ORDER_STATUS.SAP_SUBMITTED:
      return (
        <div className={classes.statsAlign}>
          <div className={classes.sap_submittedStatus} />
        </div>
      );
    case PURCHASE_ORDER_STATUS.APPROVED:
      return (
        <div className={classes.statsAlign}>
          <div className={classes.approvedStatus} />
        </div>
      );
    case PURCHASE_ORDER_STATUS.CANCELED:
      return (
        <div className={classes.statsAlign}>
          <div className={classes.canceledStatus} />
        </div>
      );
    case PURCHASE_ORDER_STATUS.REJECTED:
      return (
        <div className={classes.statsAlign}>
          <div className={classes.rejectedStatus} />
        </div>
      );
    default:
      return "-";
  }
};

const toLocalDate = (date) => `${new Date(date).toLocaleDateString()}`;

/**
 * Will return a i18n string for the description or for the code.
 *
 * @param {*} i18nProvider
 * @param {*} code
 * @param {*} lov
 */
const getDescriptionByCode = (i18nProvider, code, lov) => {
  const elements = lov.filter((entry) => entry.code === code);
  if (elements.length > 0) {
    return i18nProvider.Texti18n(elements[0].description);
  } else {
    return i18nProvider.Texti18n(code);
  }
};

/**
 * Gets the value that came from the response and format it.
 * @param {*} fieldValue
 * @param {*} format
 * @param {*} lov
 */
const parseCellValues = (i18nProvider, fieldValue, format, lov) => {
  switch (format) {
    case "date":
      return fieldValue ? toLocalDate(fieldValue) : "-";
    case "countdown":
      return fieldValue ? (
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            position: "relative",
          }}
        >
          {toLocalDate(fieldValue)}
          <Countdown
            i18nProvider={i18nProvider}
            dateToCompare={fieldValue}
            status={lov}
          />
        </div>
      ) : (
        "-"
      );
    case "referenceData":
      return fieldValue
        ? getDescriptionByCode(i18nProvider, fieldValue, lov)
        : "-";
    case "euro":
      return fieldValue ? getFormattedNumber(fieldValue, true, "EUR") : "-";
    default:
      return fieldValue ? i18nProvider.Texti18n(fieldValue) : "-";
  }
};

/**
 * Create a parsed row
 * Has the logic to transform data in visual content.
 */
const createTableRow = (classes, dataRow, i18nProvider, lovs) => {
  const row = {};
  row.id = dataRow.repositoryId;
  row.repositoryStatus = getStatusVisualComponent(
    classes,
    dataRow.repositoryStatus
  );
  row.friendlyNumber = parseCellValues(i18nProvider, dataRow.friendlyNumber);
  row.type = parseCellValues(i18nProvider, dataRow.type);
  row.supplierCode = parseCellValues(
    i18nProvider,
    dataRow.supplierCode,
    "referenceData",
    lovs.suppliers
  );
  row.totalWithoutTax = parseCellValues(
    i18nProvider,
    dataRow.totalWithoutTax,
    "euro"
  );
  row.creationDate = parseCellValues(
    i18nProvider,
    dataRow.creationDate,
    "date"
  );
  row.expirationDate = parseCellValues(
    i18nProvider,
    dataRow.expirationDate,
    "countdown",
    dataRow.repositoryStatus
  );
  row.departmentCode = parseCellValues(
    i18nProvider,
    dataRow.departmentCode,
    "referenceData",
    lovs.referenceData.allDepartments,
  );
  row.projectCode = parseCellValues(
    i18nProvider,
    dataRow.projectCode,
    "referenceData",
    lovs.referenceData.projects
  );
  row.internalOrderCode = parseCellValues(
    i18nProvider,
    dataRow.internalOrderCode,
    "referenceData",
    lovs.referenceData.internalOrders
  );
  row.userGroup = dataRow.currentUserGroup;
  return row;
};
/**
 * Used to parse the data that cames from the backend
 */
const tableParseData = (classes, data, i18nProvider, lovs) => {
  let parseData = [];
  data.forEach((element) => {
    parseData.push(createTableRow(classes, element, i18nProvider, lovs));
  });
  return parseData;
};

const POListScreenRender = ({
  classes,
  I18nProvider,
  tableHeaders,
  purchaseOrderList,
  totalPurchaseOrders,
  numberOfRecords,
  expandFilter,
  navigateToPage,
  onExpandFilterClick,
  requestFilterData,
  onRowClick,
  sortHandler,
  lovs,
  tableApi,
  setTableApi,
}) => {
  return (
    <div className={classes.root}>
      {
        <HeaderBar title={I18nProvider.Texti18n("po_list")} hideBackButton>
          <Grid container spacing={0}>
            <Grid container item xs={6}>
              <Grid container item xs={12}>
                <Button
                  id="CreateNewPO"
                  style={{ height: "40px", paddingBottom: "6px" }}
                  variant="contained"
                  color="primary"
                  onClick={() => navigateToPage("createPO")}
                >
                  {I18nProvider.Texti18n("create_po")}
                  <Icon className={classNames(classes.icon, "icon-add")} />
                </Button>
              </Grid>
            </Grid>
            <Grid container item xs={6}>
              <Grid container justify="flex-end" item xs={12}>
                <IconButton
                  id="filterIconButton"
                  className={classes.filterIcon}
                  onClick={onExpandFilterClick}
                >
                  <Icon className={"icon-filter"}></Icon>
                </IconButton>
              </Grid>
            </Grid>
          </Grid>
        </HeaderBar>
      }
      <div className={classes.mainContent}>
        <Grid container spacing={0}>
          <Grid container item xs={12} spacing={0}>
            <Collapse
              id="collapseFilterFields"
              className={classes.collapseFilter}
              in={expandFilter}
              timeout="auto"
            >
              <FilterList onFilterClick={requestFilterData} />
            </Collapse>
          </Grid>
          <Grid container item xs={12} spacing={0} justify="flex-end">
            <div className={classes.tableNumbers}>
              {/* TODO: Uncomment when the new ui-lib/core 2.2.0 is used*/}
              {/* <Tooltip  classes={{ tooltip: classes.increaseTooltip }}
								title={I18nProvider.Texti18n('listPO.table.header.tooltip.record_numbers')}>
								<Icon className="icon-information" style = {{fontSize: 16}} />
							</Tooltip> */}
              <Tooltip
                title={
                  <Typography>
                    {I18nProvider.Labeli18n(
                      "listPO.table.header.tooltip.record_numbers"
                    )}
                  </Typography>
                }
              >
                <Icon className="icon-information" style={{ fontSize: 16 }} />
              </Tooltip>
              <Typography
                variant="body2 Heading"
                color="textPrimary"
                style={{ marginLeft: "10px" }}
              >
                {I18nProvider.Texti18n("showing")} {numberOfRecords}{" "}
                {I18nProvider.Texti18n("of")} {totalPurchaseOrders}{" "}
                {I18nProvider.Texti18n("results")}.
              </Typography>
            </div>
          </Grid>
          <Grid container item xs={12} spacing={0}>
            <CustomTable
              onRowClick={onRowClick}
              id="TableListPO"
              singleSort={true}
              headers={tableHeaders}
              apiCallback={(api) => {
                if (!tableApi) setTableApi(api);
              }}
              noDataLabel={I18nProvider.Texti18n("no_results_found")}
              data={tableParseData(
                classes,
                purchaseOrderList,
                I18nProvider,
                lovs
              )}
              onSort={(headerKey, directionResponseOk) => {
                return new Promise((resolve, error) => {
                  sortHandler(headerKey, directionResponseOk, resolve, error);
                });
              }}
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default withStyles(styles, { name: ComponentName })(POListScreenRender);
