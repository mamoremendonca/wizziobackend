import { Config } from "../../config";
import { getLovOptionFromValue } from "../../utils/lovUtils";
import { dateFormater } from "../../components/InfoPO/utils";

const parsePercentage = (value) => (value ? parseFloat(value) / 100 : 0);

export const formatToDatePickerString = (dateString) => {
  if (dateString) {
    const dateToTransform = new Date(dateString);
    return dateToTransform.toISOString().slice(0, 10);
  }
};

export const mapValuesToPurchaseOrder = (values, purchaseOrderId) => ({
  purchaseOrderNumber: purchaseOrderId,
  version: 0,
  type: values.type ? values.type.value : null,
  supplierCode: values.supplierCode ? values.supplierCode.value : null,
  description: values.description ? values.description : null,
  departmentCode: values.department ? values.department.value : null,
  projectCode: values.project ? values.project.value : null,
  internalOrderCode: values.internalOrder ? values.internalOrder.value : null,
  creationDate: new Date(values.creationDate) || null,
  expirationDate: new Date(values.limitDate) || undefined,
});

export const mapPurchaseOrderToValues = (
  purchaseOrderData,
  lovs,
  suppliers
) => ({
  type: purchaseOrderData.type
    ? getLovOptionFromValue(
        lovs.purchaseOrderTypes,
        purchaseOrderData.type || "-"
      )
    : undefined,
  supplierCode: getSupplierField(
    suppliers,
    "code",
    purchaseOrderData.supplierCode
  ),
  supplierTaxNumber: getSupplierField(
    suppliers,
    "tax",
    purchaseOrderData.supplierCode
  ),
  supplierName: getSupplierField(
    suppliers,
    "name",
    purchaseOrderData.supplierCode
  ),
  valueWOVat: purchaseOrderData.totalWithoutTax,
  description: purchaseOrderData.description,
  department: getLovOptionFromValue(
    lovs.departments,
    purchaseOrderData.departmentCode || "-"
  ),
  project: purchaseOrderData.projectCode
    ? getLovOptionFromValue(lovs.projects, purchaseOrderData.projectCode || "-")
    : undefined,
  internalOrder: purchaseOrderData.internalOrderCode
    ? getLovOptionFromValue(
        lovs.internalOrders,
        purchaseOrderData.internalOrderCode || "-"
      )
    : undefined,
  creationDate: dateFormater(purchaseOrderData.creationDate),
  limitDate: purchaseOrderData.expirationDate
    ? formatToDatePickerString(purchaseOrderData.expirationDate)
    : undefined,
});

export const mapItemsToValues = (purchaseOrderData, lovs) => {
  return purchaseOrderData.items.length > 0
    ? purchaseOrderData.items.map((purchaseOrderItem) => {
        let item;
        let itemType;
        let costCenter;
        let fixedAsset = false;

        switch (purchaseOrderData.purchaseOrderHeader.type) {
          case "CAPEX":
            item = getLovOptionFromValue(
              loadFixedAssets(lovs.assets, purchaseOrderItem.code),
              purchaseOrderItem.code || "-"
            );
            itemType = "-";
            fixedAsset = true;
            break;
          case "CONTRACT":
          case "OPEX":
            item = getLovOptionFromValue(
              loadItems(lovs.items, purchaseOrderItem.code),
              purchaseOrderItem.code || "-"
            );
            if (item !== "-") {
              itemType =
                getLovOptionFromValue(lovs.itemTypes, item.itemTypeCode) || "-";
            } else {
              itemType = "-";
            }
            costCenter = lovs.costCenters.find(
              (c) => c.code === purchaseOrderItem.costCenterCode
            );
            if (!costCenter) {
              costCenter = lovs.projects.find(
                (c) => c.code === purchaseOrderItem.costCenterCode
              );
            }
            break;
          default:
            return {};
        }
        return {
          fixedAsset: fixedAsset,
          itemType: itemType,
          item: item,
          index: purchaseOrderItem.index,
          comment: purchaseOrderItem.comment,
          startDate: purchaseOrderItem.startDate
            ? new Date(purchaseOrderItem.startDate).toISOString().slice(0, 10)
            : null,
          endDate: purchaseOrderItem.endDate
            ? new Date(purchaseOrderItem.endDate).toISOString().slice(0, 10)
            : null,
          description: purchaseOrderItem.description,
          tax:
            getLovOptionFromValue(lovs.tax, purchaseOrderItem.taxCode) || "-",
          quantity: purchaseOrderItem.amount,
          unitValue: purchaseOrderItem.unitPriceWithoutTax,
          costCenter: costCenter || "-",
          unitValueWoVAT:
            purchaseOrderItem.amount * purchaseOrderItem.unitPriceWithoutTax,
          totalAmountWithVAT:
            purchaseOrderItem.amount *
            purchaseOrderItem.unitPriceWithoutTax *
            (1 +
              parsePercentage(
                getLovOptionFromValue(lovs.tax, purchaseOrderItem.taxCode)
                  .taxPercentage || 0
              )),
        };
      })
    : [];
};

export const submitPurchaseOrderRequest = (HttpClient, purchaseOrderId) => {
  let config = {
    method: Config.METHOD_PUT,
    url: `${Config.URL_PURCHASE_ORDER}${purchaseOrderId}${Config.SUBMIT_PO_URL}`,
  };
  return HttpClient.request(config);
};

//TODO Refactor remove this method soon as possible
const loadFixedAssets = (assets, inputValue) => {
  if (assets) {
    const newAssets = inputValue
      ? assets.filter((asset) => asset.code.includes(inputValue))
      : assets;
    return newAssets.map((asset) => ({
      value: asset.value,
      description: asset.description,
      defaultTaxCode: asset.defaultTaxCode,
      name: asset.label,
      label: `${asset.value} - ${asset.label}`,
    }));
  }
};

//TODO Refactor remove this method soon as possible
const loadItems = (items, inputValue) => {
  if (items) {
    const newItems = inputValue
      ? items.filter((item) => item.code.includes(inputValue))
      : items;
    return newItems.map((item) => ({
      value: item.code,
      description: item.description,
      itemTypeCode: item.itemTypeCode,
      departmentCode: item.departmentCode,
      defaultTaxCode: item.defaultTaxCode,
      defaultCostCenterCode: item.defaultCostCenterCode,
      label: item.code,
      code: item.code,
    }));
  }
};

//TODO Refactor remove this method soon as possible
const loadSupplierCodeOptions = (suppliers, inputValue) => {
  if (suppliers) {
    const newSuppliers = inputValue
      ? suppliers.filter((supplier) => supplier.id.includes(inputValue))
      : suppliers;
    return newSuppliers.map((supplier) => ({
      value: supplier.id,
      label: supplier.id,
      code: supplier.id,
      name: supplier.name,
      tax: supplier.tax,
    }));
  }
};

//TODO Refactor remove this method soon as possible
const loadSupplierNameOptions = (suppliers, inputValue) => {
  if (suppliers) {
    const newSuppliers = inputValue
      ? suppliers.filter((supplier) => supplier.name.includes(inputValue))
      : suppliers;
    return newSuppliers.map((supplier) => ({
      value: supplier.name,
      label: supplier.name,
      code: supplier.id,
      name: supplier.name,
      tax: supplier.tax,
    }));
  }
};

//TODO Refactor remove this method soon as possible
const loadSupplierTaxOptions = (suppliers, inputValue) => {
  if (suppliers) {
    const newSuppliers = inputValue
      ? suppliers.filter((supplier) => supplier.tax.includes(inputValue))
      : suppliers;

    return newSuppliers.map((supplier) => ({
      value: supplier.tax,
      label: supplier.tax,
      code: supplier.id,
      name: supplier.name,
      tax: supplier.tax,
    }));
  }
};

const getSupplierField = (suppliers, fieldName, supplierCode) => {
  return (
    suppliers.length > 0 &&
    suppliers.find((supplier) => {
      return (
        supplier.code === supplierCode && {
          value: supplier[fieldName],
          label: supplier[fieldName],
        }
      );
    })
  );
};

export const loadOptionsUtil = {
  loadFixedAssets,
  loadItems,
  loadSupplierCodeOptions,
  loadSupplierNameOptions,
  loadSupplierTaxOptions,
};
