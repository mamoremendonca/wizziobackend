import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import styles from "./styles";

import Icon from "@ui-lib/core/Icon";
import Typography from "@ui-lib/core/Typography";
import CircularProgress from "@ui-lib/core/CircularProgress";

const FileDeleteIcon = props => {
  const { classes, itemClick } = props;
  return (
    <Icon className={classes.DeleteIcon} onClick={itemClick}>
      clear
    </Icon>
  );
};

const FileChip = props => {
  const {
    classes,
    name,
    key,
    onDelete,
    documentClass,
    I18nProvider,
    isLoading,
    indexLoading,
    onDownload,
    canDelete
  } = props;
  return (
    <div className={classes.root} tabIndex={key} key={key}>
      <div
        style={{ display: "flex", flexDirection: "row", alignItems: "center" }}
      >
        <div>
          <div className={classes.Chip}>
            <img src="./Fill-1.svg" alt="File" />
            <Typography className={classes.chipText} onClick={onDownload}>{name}</Typography>
          </div>
          {documentClass && <div>
            <Typography className={classes.documentClassText}>
              {I18nProvider.Texti18n(`${documentClass}_ATTACHMENT`)}
            </Typography>
          </div>}
        </div>
        {isLoading.index === indexLoading && isLoading.isLoadingAttachment ? (
          <CircularProgress
            value={true}
            color="primary"
            className={classes.progress}
          />
        ) : (
          onDelete && canDelete && (
            <div className={classes.deleteIconContainer}>
              <FileDeleteIcon itemClick={onDelete} classes={classes} />
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default withStyles(styles)(FileChip);
