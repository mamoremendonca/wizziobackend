import React from "react";
import Dialog from "@ui-lib/core/Dialog";

import DialogTitle from "@ui-lib/core/DialogTitle";
import DialogContent from "@ui-lib/core/DialogContent";
import DialogActions from "@ui-lib/core/DialogActions";
import Button from "@ui-lib/core/Button";
//import { withStyles } from "@ui-lib/core/styles";

const ConfirmationDialog = (props) => {
  const { open, onClose, children, title, disabled } = props;
  return (
    <Dialog
      open={open}
      onClose={() => onClose(false)}
      fullWidth={true}
      maxWidth={"md"}
    >
      <DialogTitle>{title}</DialogTitle>
      <DialogContent style={{ overflowY: "visible" }}>{children}</DialogContent>
      <DialogActions>
        <Button type="submit" onClick={() => onClose(false)}>
          Cancelar
        </Button>
        <Button
          type="submit"
          variant="contained"
          color="primary"
          disabled={disabled}
          onClick={() => onClose(true)}
        >
          Continuar
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmationDialog;
