
// TODO -  need to review these filters with the backend

//#region For HTTP Requests
export const ONEY_REFERENCE_DATA_BODY = [
    "ASSET", // items used for capex purchase order type.
    "COST_CENTER",
    "DOCUMENT_TYPE", //used to attachment document class.
    "INTERNAL_ORDER",
    "ITEM", // used for Contract and OPEX purchase order types.
    "ITEM_TYPE",// used for Contract and OPEX purchase order types.
    "PROJECT",
    "TAX",
    "DEPARTMENT"
]

export const PURCHASE_ORDER_REFERENCE_DATA_BODY = [
    "DEPARTMENT",
    "PURCHASE_ORDER_LIST_STATUS",
    "PURCHASE_ORDER_TYPE"
]
//#region 

//#region Constants used in Reducer actions
export const SET_SUPPLIERS = 'SET_SUPPLIERS';
export const SET_LOVS = 'SET_LOVS';
//#region


export const PO_CAPEX = "CAPEX";
export const PO_CONTRACT = "CONTRACT";
export const PO_OPEX = 'OPEX';



