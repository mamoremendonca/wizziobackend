import * as React from "react";

import TextField from "@ui-lib/core/TextField";

import { withStyles } from "@ui-lib/core/styles";
import styles from "./styles";

const TextFieldWrapper = ({
  input: { name, onChange, value, ...restInput },
  meta,
  disabled,
  isMoney,
  classes,
  ...rest
}) => {
  const showError =
    ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) &&
    meta.touched;

  const className = disabled ? classes.textFieldDisabled : undefined;

  return (
    <React.Fragment>
      <TextField
        {...rest}
        multiline
        rowsMax="4"
        name={name}
        helperText={showError ? meta.error || meta.submitError : undefined}
        error={showError}
        inputProps={restInput}
        onChange={onChange}
        className={className}
        value={`${isMoney ? `${value} €` : value}`}
        disabled={disabled}
        style={{ paddingTop: 5 }}
      />
    </React.Fragment>
  );
};

export default withStyles(styles)(TextFieldWrapper);
