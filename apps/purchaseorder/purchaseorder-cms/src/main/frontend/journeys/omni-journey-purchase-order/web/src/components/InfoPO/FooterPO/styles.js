const styles = theme => ({
  container: {
    padding: `${theme.spacing.unit * 2}px 0px ${theme.spacing.unit * 3}px 0px`
  },
  disabledInfoField: {
    paddingBottom: theme.spacing.unit * 2,
    "& .MuiInputBase-root.MuiInputBase-disabled ": {
      color: theme.palette.text.primary
    },
    "& .MuiFormLabel-root.MuiFormLabel-disabled": {
      color: theme.palette.text.secondary
    },
    "& .MuiInput-underline.MuiInput-disabled:before": {
      borderBottomStyle: "none"
    }
  },
  buttonGroupContainer: {
    display: "flex",
    flexDirection: "row"
  },
  radioButton: {
    "& .MuiIconButton-label": {
      color: theme.palette.error.main
    }
  },
  radioButtonErrorText: {
    color: theme.palette.error.main
  }
});

export default styles;
