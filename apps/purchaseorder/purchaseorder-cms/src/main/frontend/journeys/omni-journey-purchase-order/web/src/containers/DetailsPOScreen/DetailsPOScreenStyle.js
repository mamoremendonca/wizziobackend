const ComponentName = "DetailsPOScreenStyle";

const styles = theme => ({
  titleContainer: {
    color: theme.palette.primary.main,
    paddingBottom: theme.spacing.unit * 2
  },
  leftContainer: {
    padding: "20px",
    borderRight: `2px solid ${theme.palette.primary.main}`
  },
  rightContainer: {
    backgroundColor: theme.palette.grey[100]
  },
  footerButtonsContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end"
  },
  approvalActionsContainer: {
    padding: theme.spacing.unit
  },
  cancelButton: {
    marginRight: theme.spacing.unit
  },
  copyButton: {
    marginLeft: "25px"
  }
});

export { styles };

export { ComponentName };
