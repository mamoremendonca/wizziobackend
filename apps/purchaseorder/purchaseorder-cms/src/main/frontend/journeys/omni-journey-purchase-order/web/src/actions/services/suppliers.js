import {Config} from "../../config";
import { handleErrorRequest, ErrorMessages } from "../../utils";
import {setSuppliers} from "../../actions"

const supplierToSelect = data  => data.map(element => ({
    id: element.code, 
    code: element.code, 
    name: element.name,  
    description: element.name,
    tax: element.nif,
    marketCode: element.marketCode   
}));


export const getSuppliers = (services) => {
 
    const {Notifications, HttpClient} = services;
    const config = {
        method : Config.METHOD_POST,
        url : Config.SUPPLIER_URL,
        data: {}
    };
    return (dispatch) => {
        return HttpClient.request(config).then((res) => {
            dispatch(setSuppliers(supplierToSelect(res.data)));
        }).catch(() => {
            handleErrorRequest(Notifications, ErrorMessages.SEARCH_ERROR)
        });
    }
}

