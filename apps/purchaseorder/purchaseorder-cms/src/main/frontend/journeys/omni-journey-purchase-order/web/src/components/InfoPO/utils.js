import React from "react";
import Typography from "@ui-lib/core/Typography";

export const getDateDiff = (dateToCompare, classes) => {
  const actualDate = new Date();
  const compareDate = new Date(dateToCompare);

  //Convert to UTC
  const actualDateUTC = Date.UTC(actualDate.getFullYear(), actualDate.getMonth(), actualDate.getDate());
  const compareDateUTC = Date.UTC(compareDate.getFullYear(), compareDate.getMonth(), compareDate.getDate());


  const diffTime = compareDateUTC - actualDateUTC;
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    
  if (diffDays > 7) {
    return undefined;
  } else if (diffDays >= 0) {
    return (
      <Typography
        variant="h6"
        className={classes.dateDiffAbove}
      >{`${"+"} ${Math.abs(diffDays)} dias`}</Typography>
    );
  } else if (diffDays < 0) {
    return (
      <Typography
        variant="h6"
        className={classes.dateDiffBelow}
      >{`${"-"} ${Math.abs(diffDays)} dias`}</Typography>
    );
  }
};

export const dateFormater = dateAsString => {

  if (dateAsString) {
    const poDate = new Date(dateAsString);
    return `${poDate.getDate()}/${poDate.getMonth() + 1}/${poDate.getFullYear()}`;
  
  }
  return "-";
};
