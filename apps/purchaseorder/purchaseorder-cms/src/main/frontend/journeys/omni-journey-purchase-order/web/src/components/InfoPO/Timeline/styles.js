const styles = theme => ({
  flexRowContainer: { display: "flex", flexDirection: "row" },
  headerContainer: {
    backgroundColor: theme.palette.common.white
  },
  headerTab: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: theme.spacing.unit * 2,
    cursor: 'pointer'
  },
  selectedHeaderTab: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: theme.spacing.unit * 2,
    color: theme.palette.primary.main,
    borderBottom: `2px solid ${theme.palette.primary.main}`,
    cursor: 'pointer'
  },
  iconContainer: { padding: 20, backgroundColor: theme.palette.primary.main },
  icon: {
    color: theme.palette.common.white
  },
  timelineActionsContainer: {
    position: "absolute",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit}px 0px 0px`,
    right: 0,
    display: "flex",
    flexDirection: "row"
  },
  refresh: {
    fontSize: 24,
    cursor: "pointer",
    padding: 5,
    bottom: 5,
    transitionDuration: "0.8s",
    transitionProperty: "transform",
    "&:hover": {
      transform: "rotate(90deg)",
      color: theme.palette.primary.main 
    }
  },
  contentComponentContainer: {
    paddingTop: theme.spacing.unit * 2
  }
});

export { styles };
