import React from 'react';
import FormControl from "@ui-lib/core/FormControl";
import InputLabel from "@ui-lib/core/InputLabel";
import FormHelperText from "@ui-lib/core/FormHelperText";

const DatePickerWrapper = ({ input: { name, value, onChange, ...restInput },
    meta,
    label,
    formControlProps,
    inputLabelProps,
    ...rest }) => {
    const showError =
        ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) &&
        meta.touched;
    return (
        <FormControl {...formControlProps} error={showError}>
            <InputLabel htmlFor={name} {...inputLabelProps} shrink={true}>
                {label}
            </InputLabel>

            {showError && (
                <FormHelperText>{meta.error || meta.submitError}</FormHelperText>
            )}
        </FormControl>);
}

export default DatePickerWrapper;