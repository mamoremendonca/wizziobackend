import React from "react";

import styles from "./styles";
import { withStyles } from "@ui-lib/core/styles";

import Typography from "@ui-lib/core/Typography";
import Icon from "@ui-lib/core/Icon";
import IconButton from "@ui-lib/core/IconButton";
import CircularProgress from "@ui-lib/core/CircularProgress";

const AttachmentList = props => {
  const { classes, attachmentsData, downloadAttachment, deleteAttachment } = props;

  const renderAttachmentStatus = statusData => {
    return (
      <div key={statusData.id} className={classes.attachmentStatusSection}>
        <div className={classes.flexRowContainer}>
          <Typography variant="h5">{statusData.status}</Typography>
          <div className={classes.statusHeader} />
          <Typography>{statusData.user} </Typography>
        </div>
        {statusData.attachments.map((attachment, index) => {
          return (
            <div
              key={`${attachment.id}${index}`}
              className={classes.singleAttachmentContainer}
            >
              {
                (attachment.isAbleToDelete || attachment.isLoading) && <div className={classes.deleteAttachment}>
                  { attachment.isLoading ?
                    (<CircularProgress
                      value={true}
                      color="primary"
                      className={classes.progress}
                    />)
                    :
                    (
                      <IconButton onClick={() => deleteAttachment(attachment.id)} className={classes.closeIcon}>
                        <Icon>clear</Icon>
                      </IconButton>
                    )
                  }
                  
                </div>
              }
              <div className={classes.singleAttachmentHeader}>
                <div className={classes.flexRowContainer}>
                  <img src="./Fill-1.svg" alt="File" />
                  <div
                    onClick={() =>
                      downloadAttachment(attachment.id, attachment.name)
                    }
                  >
                    <Typography
                      variant="h5"
                      className={classes.singleAttachmentTitle}
                    >
                      {attachment.name}
                    </Typography>
                  </div>
                </div>
                <Typography>{attachment.date}</Typography>
              </div>
            </div>
          );
        })}
      </div>
    );
  };

  return (
    <div className={classes.root}>
      {attachmentsData.map(
        statusData =>
          statusData.attachments.length > 0 &&
          renderAttachmentStatus(statusData)
      )}
    </div>
  );
};

export default withStyles(styles)(AttachmentList);
