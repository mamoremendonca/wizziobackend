export const CurrencyType = {
  EUR: "€",
  USD: "$",
};

export const getFormattedNumber = (num, decimal, currency) => {
  num = !decimal ? num : parseFloat(num).toFixed(2);
  return new Intl.NumberFormat(
    "es-ES",
    currency && {
      style: "currency",
      currency: currency,
    }
  ).format(num);
};
