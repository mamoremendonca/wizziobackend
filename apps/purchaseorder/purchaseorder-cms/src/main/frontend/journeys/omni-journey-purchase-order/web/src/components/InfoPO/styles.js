const styles = theme => ({
  root: {
    padding: theme.spacing.unit * 2
  },
  container: {
    padding: `${theme.spacing.unit * 2}px 0px ${theme.spacing.unit * 3}px 0px`
  },
  disabledInfoField: {
    paddingBottom: theme.spacing.unit * 2,
    "& .MuiInputBase-root.MuiInputBase-disabled ": {
      color: theme.palette.text.primary
    },
    "& .MuiFormLabel-root.MuiFormLabel-disabled": {
      color: theme.palette.text.secondary
    },
    "& .MuiInput-underline.MuiInput-disabled:before": {
      borderBottomStyle: "none"
    }
  },
  fieldContainerGrid: {
    display: "flex",
    flexDirection: "row",
    position: "relative"
  },
  dateDiffAbove: {
    display: "flex",
    alignSelf: "center",
    color: theme.palette.common.white,
    backgroundColor: "#E79208",
    padding: `0px ${theme.spacing.unit}px`,
    borderRadius: theme.spacing.unit * 3,
    position: "absolute",
    left: theme.spacing.unit * 10
  },
  dateDiffBelow: {
    display: "flex",
    alignSelf: "center",
    color: theme.palette.common.white,
    backgroundColor: "#D92727",
    padding: `0px ${theme.spacing.unit}px`,
    borderRadius: theme.spacing.unit * 3,
    position: "absolute",
    left: theme.spacing.unit * 10
  }
});

export default styles;
