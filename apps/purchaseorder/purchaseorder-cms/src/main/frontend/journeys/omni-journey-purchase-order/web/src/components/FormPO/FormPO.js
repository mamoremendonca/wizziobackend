import React, { Component } from "react";
import isEmpty from "lodash/isEmpty";
import { Form } from "react-final-form";
import { withRouter } from "react-router-dom";
import { withStyles } from "@ui-lib/core/styles";
import Grid from "@ui-lib/core/Grid";
import Paper from "@ui-lib/core/Paper";
import Icon from "@ui-lib/core/Icon";
import Button from "@ui-lib/core/Button";

import Separator from "../Separator";
import styles from "./styles";
import POBasicDataForm from "./POBasicDataForm";
import ItemsTable from "../ItemsTable/ItemsTable";
import ItemDialog from "../Dialog/ItemDialog";
import ItemForm from "../ItemForm/ItemForm";
import FileChip from "../SharedComponents/FileChip";
import Typography from "@ui-lib/core/Typography";
import FileDialog from "../Dialog/FileDialog";
import createDecorator from "final-form-calculate";

import {
  SUPPLIER_CODE,
  SUPPLIER_NAME,
  SUPPLIER_TAX_NUMBER,
  PO_CONTRACT,
  DEPARTMENT,
  PURCHASE_ORDER_FINAL_TYPE,
} from "./constants";
import * as dialogConstants from "../Dialog/constants";
import ConfirmationDialog from "../Dialog/ConfirmationDialog";
import {
  handleErrorRequest,
  ErrorMessages,
  getFormattedNumber,
} from "../../utils";

// Utils
import {
  addItemToFields,
  editItemField,
  addPurchaseOrderItemRequest,
  updatePurchaseOrderItemRequest,
  removePurchaseOrderItemRequest,
  addPurchaseOrderAttachmentRequest,
  removePurchaseOrderAttachmentRequest,
  getDraftAttachmentsFromTimeline,
} from "./formUtils";
import { TOTAL_AMOUNT_W_VAT } from "../ItemForm/constants";
import FileInput from "../SharedComponents/FileInput";

import {
  downloadAttachmentRequest,
  clearAllItemsRequest,
  getSupplierDetailsRequest,
} from "../../actions/services/purchase-order";
import { downloadFile } from "../../utils/timeline";

/**
 * Used to which values are to be used to show the change confirmation dialog
 */
let trackChanges = { trackerStarted: false, discardChange: false };
class FormPO extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isArticleDialogOpen: false,
      isItemModalOpen: false,
      files: [],
      values: {},
      fields: [],
      selectedItem: {},
      selectedItemIndex: undefined,
      totalItemValueWithVAT: 0,
      totalItemValueWithoutVAT: 0,
      initialValues: undefined,
      fileDialogOpen: false,
      fileDialogItems: undefined,
      isLoadingAttachment: { index: undefined, isLoading: false },
      allDisabledFields: false,
      changedField: false, //to know which field has changed and its previous value.
      isOnBlurSaveActive: true,
      itemDialogTitle: "",
      itemDialogAction: "",
      currentSupplier: {
        code: undefined,
        marketType: undefined,
      },
    };
  }

  componentDidMount() {
    trackChanges.trackerStarted = false;
    trackChanges.discardChange = false;
    const { initialPOValues, services, purchaseOrderId } = this.props;
    const initialValues = { ...this.props.initialPOValues.purchaseOrderData };
    initialValues.finalType = initialValues.type; //* This avoid problems related with the table items that depends on the PO type.
    const fields = this.props.initialPOValues.purchaseOrderItems;
    if (fields.length > 0) {
      const totalItemValueWithVAT = fields.reduce(
        (acc, field) => acc + parseFloat(field[TOTAL_AMOUNT_W_VAT]),
        0
      );
      this.setState({ initialValues, fields, totalItemValueWithVAT });
    } else {
      this.setState({ initialValues, fields });
    }

    if (
      initialPOValues.purchaseOrderHeader &&
      initialPOValues.purchaseOrderHeader.status === "Rejected"
    ) {
      this.setState({ allDisabledFields: true });
    }
    if (
      initialPOValues.purchaseOrderHeader &&
      initialPOValues.purchaseOrderHeader.status === "Draft"
    ) {
      getDraftAttachmentsFromTimeline(
        services.HttpClient,
        purchaseOrderId
      ).then((attachments) => {
        if (attachments && attachments.length > 0) {
          //* Only for DF
          // this.setState({
          // 	files: attachments.map(attachment => ({ content: attachment, index: attachment.index, documentClass: attachment.documentClass }))
          // })
          this.setState({
            files: attachments.map((attachment) => ({
              content: attachment,
              index: attachment.index,
            })),
          });
        }
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.initialValues && !trackChanges.trackerStarted) {
      //to be sure that changes were applied.
      trackChanges.trackerStarted = true;
    }
  }

  acceptFieldChanges(formMutator, fieldName, currentValue) {
    switch (fieldName) {
      case PURCHASE_ORDER_FINAL_TYPE:
        formMutator.setPurchaseOrderFinalType(currentValue);
        break;
      case DEPARTMENT:
        //need to clear the project and internalorder old values.
        formMutator.setDepartmentDependencies();
        break;
      default:
        //to keep the changes as been supposed;
        formMutator.setPurchaseOrderField({ fieldName, value: currentValue });
        break;
    }
  }

  discardFieldChanges(formMutator, fieldName, previousValue) {
    switch (fieldName) {
      case PURCHASE_ORDER_FINAL_TYPE:
        formMutator.setPurchaseOrderFinalType(previousValue);
        break;
      default:
        formMutator.setPurchaseOrderField({ fieldName, value: previousValue });
        break;
    }
  }

  isToAcceptTheNewChanges = (
    formMutator,
    fieldName,
    previousValue,
    currentValue
  ) => {
    //todo refactor
    if (
      !this.state.changedField &&
      trackChanges.trackerStarted &&
      !trackChanges.discardChange &&
      this.state.fields.length
    ) {
      trackChanges.discardChange = true;
      this.setState({
        changedField: { fieldName, previousValue, currentValue },
        confirmationDialogOpen: true,
        isOnBlurSaveActive: false,
      });
    }
    //* hack just to deliver
    else if (
      trackChanges.trackerStarted ||
      (fieldName === "type" && !trackChanges.discardChange)
    ) {
      //* !trackChanges.discardChange used because the didUpdate not always is fired
      //  and we need to have the finaltype filled to open the item form
      if (fieldName === "type") {
        //* Keep the type value and update the finalType.
        this.acceptFieldChanges(
          formMutator,
          PURCHASE_ORDER_FINAL_TYPE,
          currentValue
        );
      } else {
        this.acceptFieldChanges(formMutator, fieldName, currentValue);
      }
    }
  };

  validateHeader = (values) => {
    let errors = {};

    return errors;
  };

  validateItem = (values) => {
    const errors = {};

    return errors;
  };

  showArticleDialog = () => {
    this.setState({ isArticleDialogOpen: true });
  };

  hideArticleDialog = () => {
    this.setState({ isArticleDialogOpen: true });
  };

  isEqualOrHigherThenActualDate = (value) => {
    //TODO Refactor
    const actualDate = new Date();
    const compareDate = new Date(value);

    const actualDateUTC = Date.UTC(
      actualDate.getFullYear(),
      actualDate.getMonth(),
      actualDate.getDate()
    );
    const compareDateUTC = Date.UTC(
      compareDate.getFullYear(),
      compareDate.getMonth(),
      compareDate.getDate()
    );

    const diffTime = compareDateUTC - actualDateUTC;
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

    return diffDays >= 0;
  };

  onHeaderSubmit = async (values, form) => {
    const {
      services: { I18nProvider },
    } = this.props;
    // Call Backend to save Header
    await this.props.looseHeaderFocusHandler(values);

    if (values.limitDate) {
      if (!this.isEqualOrHigherThenActualDate(values.limitDate)) {
        return {
          limitDate: I18nProvider.Texti18n(
            "create_po.item.dialog.form.error.date_lower_than_today"
          ),
        };
      }
    }
    // Submit the PO to be aproved
    this.props.submitPurchaseOrder();
  };

  deleteItemHandler = (index) => {
    const fields = [...this.state.fields];
    this.removePurchaseOrderItem(fields[index].index);
  };

  editItemHandler = (index, formValues) => {
    //TODO refactor
    const {
      services: { I18nProvider, HttpClient, Loading, Notifications },
    } = this.props;
    const type = formValues.finalType.value;
    const fields = [...this.state.fields];
    if (
      formValues.supplierCode &&
      formValues.supplierCode.code &&
      this.state.currentSupplier.code === formValues.supplierCode.code &&
      this.state.currentSupplier.marketType
    ) {
      this.setState({
        isItemDialogOpen: true,
        selectedItem: fields[index],
        selectedItemIndex: index,
        type,
        itemDialogTitle: I18nProvider.Labeli18n(
          "create_po.dialog.edit_item.title"
        ),
        itemDialogAction: dialogConstants.ITEM_DIALOG_ACTIONS.EDIT_ITEM,
      });
    } else {
      Loading.request("setLoading", "JOURNEY").then(() => {
        getSupplierDetailsRequest(HttpClient, formValues.supplierCode.code)
          .then((result) => {
            this.setState(
              {
                isItemDialogOpen: true,
                selectedItem: fields[index],
                selectedItemIndex: index,
                type,
                itemDialogTitle: I18nProvider.Labeli18n(
                  "create_po.dialog.edit_item.title"
                ),
                itemDialogAction: dialogConstants.ITEM_DIALOG_ACTIONS.EDIT_ITEM,
                currentSupplier: {
                  code: result.data.code,
                  marketType: result.data.marketType,
                },
              },
              () => {
                Loading.request("stopLoading").then();
              }
            );
          })
          .catch((error) => {
            Loading.request("stopLoading").then();
            handleErrorRequest(
              Notifications,
              I18nProvider.Texti18n(
                "create_po.dialog.error.get_market_type.notification"
              )
            );
          });
      });
    }
  };

  openItemDialog = (formValues) => {
    //TODO refactor
    const {
      services: { I18nProvider, HttpClient, Loading, Notifications },
    } = this.props;

    const type = formValues.finalType.value;
    if (
      formValues.supplierCode &&
      formValues.supplierCode.code &&
      this.state.currentSupplier.code === formValues.supplierCode.code &&
      this.state.currentSupplier.marketType
    ) {
      this.setState({
        isItemDialogOpen: true,
        type,
        itemDialogTitle: I18nProvider.Labeli18n(
          "create_po.dialog.add_item.title"
        ),
        itemDialogAction: dialogConstants.ITEM_DIALOG_ACTIONS.ADD_ITEM,
      });
    } else {
      Loading.request("setLoading", "JOURNEY").then(() => {
        getSupplierDetailsRequest(HttpClient, formValues.supplierCode.code)
          .then((result) => {
            this.setState(
              {
                isItemDialogOpen: true,
                type,
                itemDialogTitle: I18nProvider.Labeli18n(
                  "create_po.dialog.add_item.title"
                ),
                itemDialogAction: dialogConstants.ITEM_DIALOG_ACTIONS.ADD_ITEM,
                currentSupplier: {
                  code: result.data.code,
                  marketType: result.data.marketType,
                },
              },
              () => {
                Loading.request("stopLoading").then();
              }
            );
          })
          .catch((error) => {
            Loading.request("stopLoading").then();
            handleErrorRequest(
              Notifications,
              I18nProvider.Texti18n(
                "create_po.dialog.error.get_market_type.notification"
              )
            );
          });
      });
    }
  };

  copyItemHandler = (index) => {
    const fields = [...this.state.fields];
    this.addPurchaseOrderItem(fields[index], undefined);
  };

  closeItemDialog = (form) => {
    form.reset();
    this.setState({
      isItemDialogOpen: false,
      selectedItem: {},
      selectedItemIndex: undefined,
    });
  };
  /* This should be moved */
  addPurchaseOrderItem = (item, form) => {
    const { HttpClient, Notifications } = this.props.services;
    let fields = null;

    addPurchaseOrderItemRequest(HttpClient, item, this.props.purchaseOrderId)
      .then((res) => {
        fields = this.addItem(item, res.data.index);
        const totalValueWithVAT = fields.reduce(
          (acc, field) => acc + parseFloat(field[TOTAL_AMOUNT_W_VAT]),
          0
        );
        this.setState(
          {
            ...this.state,
            fields,
            isItemDialogOpen: false,
            selectedItem: {},
            selectedItemIndex: 0,
            totalItemValueWithVAT: totalValueWithVAT,
          },
          () => form && form.reset()
        );
      })
      .catch((error) => {
        handleErrorRequest(Notifications, ErrorMessages.PO_ITEM_SUBMIT_ERROR);
      });
  };

  updatePurchaseOrderItem = (item) => {
    const { HttpClient, Notifications } = this.props.services;
    let fields = null;

    updatePurchaseOrderItemRequest(HttpClient, item, this.props.purchaseOrderId)
      .then(() => {
        fields = this.editItem(item);
        const totalValueWithVAT = fields.reduce(
          (acc, field) => acc + parseFloat(field[TOTAL_AMOUNT_W_VAT]),
          0
        );
        this.setState({
          ...this.state,
          fields,
          isItemDialogOpen: false,
          selectedItem: {},
          selectedItemIndex: 0,
          totalItemValueWithVAT: totalValueWithVAT,
        });
      })
      .catch(() => {
        handleErrorRequest(Notifications, ErrorMessages.PO_ITEM_UPDATE_ERROR);
      });
  };

  removePurchaseOrderItem = (index, isDeletePO) => {
    const { HttpClient, Notifications } = this.props.services;
    const fields = [...this.state.fields];
    removePurchaseOrderItemRequest(
      HttpClient,
      index,
      this.props.purchaseOrderId
    )
      .then(() => {
        let indexToDelete = fields.findIndex((field) => field.index === index);
        fields.splice(indexToDelete, 1);
        const totalValueWithVAT = fields.reduce(
          (acc, field) => acc + parseFloat(field[TOTAL_AMOUNT_W_VAT]),
          0
        );
        this.setState({ fields, totalItemValueWithVAT: totalValueWithVAT });
      })
      .catch(() => {
        handleErrorRequest(Notifications, ErrorMessages.PO_ITEM_DELETE_ERROR);
      });
  };
  /* Content on top should be moved */

  onItemSubmit = (values, form) => {
    const {
      services: { I18nProvider },
    } = this.props;
    if (values.startDate || values.endDate) {
      const errors = { startDate: "", endDate: "" };
      if (!this.isEqualOrHigherThenActualDate(values.startDate)) {
        errors.startDate = I18nProvider.Texti18n(
          "create_po.item.dialog.form.error.date_lower_than_today"
        );
      }
      if (!this.isEqualOrHigherThenActualDate(values.endDate)) {
        errors.endDate = I18nProvider.Texti18n(
          "create_po.item.dialog.form.error.date_lower_than_today"
        );
      }
      if (errors.startDate || errors.endDate) {
        return errors;
      }
    }

    if (
      values.startDate &&
      values.endDate &&
      values.startDate > values.endDate
    ) {
      return {
        startDate: `${I18nProvider.Texti18n(
          "create_po.item.dialog.form.error.date_range"
        )}`,
      };
    }
    if (isEmpty(this.state.selectedItem)) {
      this.addPurchaseOrderItem(values, form);
    } else {
      this.updatePurchaseOrderItem(values);
    }
  };

  addItem = (values, index) => {
    let { fields = [] } = this.state;
    //* Hack because of Capex items
    if (this.state.type === "CAPEX") {
      values.fixedAsset = true;
    }
    return addItemToFields(values, index, fields);
  };

  editItem = (values) => {
    let { fields, selectedItemIndex } = this.state;
    return editItemField(values, selectedItemIndex, fields);
  };

  emulateSaveItemClick = () => {
    document
      .getElementById("item-form")
      .dispatchEvent(new Event("submit", { cancelable: true }));
  };
  emulateSaveFormClick = () => {
    document
      .getElementById("header-form")
      .dispatchEvent(new Event("submit", { cancelable: true }));
  };

  /* This should be moved */
  addPurchaseOrderAttachment = (attachment, documentClass) => {
    const { HttpClient, Notifications } = this.props.services;
    const filesArray = [...this.state.files];

    addPurchaseOrderAttachmentRequest(
      HttpClient,
      attachment,
      this.props.purchaseOrderId,
      documentClass
    )
      .then((res) => {
        filesArray[filesArray.length - 1].index = res.data.index;
        this.setState({
          files: filesArray,
          isLoadingAttachment: { index: undefined, isLoadingAttachment: false },
        });
      })
      .catch(() => {
        filesArray.pop();
        this.setState({
          files: filesArray,
          isLoadingAttachment: { index: undefined, isLoadingAttachment: false },
        });
        handleErrorRequest(
          Notifications,
          ErrorMessages.PO_ATTACHMENT_SUBMIT_ERROR
        );
      });
  };

  downloadAttachmentFromProcessContinuity = (index) => {
    const { purchaseOrderId, services } = this.props;
    const dataSource = "process_continuity";
    downloadAttachmentRequest(
      purchaseOrderId,
      index,
      services,
      dataSource
    ).then((response) => {
      if (response) {
        downloadFile(services.FileSystem, response.data, response.headers);
      }
    });
  };

  removePurchaseOrderAttachment = (index) => {
    const { HttpClient, Notifications } = this.props.services;
    const filesArray = [...this.state.files].filter(
      (file) => file.index !== index
    );

    removePurchaseOrderAttachmentRequest(
      HttpClient,
      index,
      this.props.purchaseOrderId
    )
      .then(() => {
        this.setState({ files: filesArray });
      })
      .catch(() => {
        handleErrorRequest(
          Notifications,
          ErrorMessages.PO_ATTACHMENT_DELETE_ERROR
        );
      });
  };
  /* Content on top should be moved */

  /**
   *
   */
  clearAllItems = (successCase, errorCase) => {
    const {
      services: { Notifications, HttpClient },
      purchaseOrderId,
      I18nProvider,
    } = this.props;
    clearAllItemsRequest(purchaseOrderId, HttpClient)
      .then(() => {
        successCase();
      })
      .catch(() => {
        errorCase();
        handleErrorRequest(
          Notifications,
          I18nProvider.Texti18n("create_po.error.request.clear_all_items")
        );
      });
  };

  //* can be used for DF
  // handleFileSelection = (files) => {
  // 	if (files[0]) {
  // 		this.setState({ fileDialogOpen: true, fileDialogItems: files[0] })
  // 	}
  // }

  /**
   * Function used only for PO, because shows a modal with the documental classes.
   */
  handleFileSelection = (files) => {
    if (files[0]) {
      const filesArray = [...this.state.files];
      const fileToSubmit = files[0];
      fileToSubmit.canBeDeletedByCurrentUser = true; //to be equal to get attachments body data.
      filesArray.push({ content: fileToSubmit, index: undefined });
      this.setState(
        {
          files: filesArray,
          isLoadingAttachment: {
            index: filesArray.length - 1,
            isLoadingAttachment: true,
          },
        },
        () => this.addPurchaseOrderAttachment(fileToSubmit, "PURCHASE_ORDER")
      );
    }
  };

  /**
   * Function used combined with handleFileSelection for DF
   */
  handleCloseFileDialog = (ev, documentClass) => {
    if (ev && documentClass) {
      const filesArray = [...this.state.files];
      const fileToSubmit = this.state.fileDialogItems;
      fileToSubmit.canBeDeletedByCurrentUser = true; //to be equal to get attachments body data.
      //ADD NEW FILE TO ARRAY AND SET IT LOADING
      filesArray.push({
        content: fileToSubmit,
        index: undefined,
        documentClass: documentClass,
      });
      this.setState(
        {
          fileDialogOpen: false,
          fileDialogItems: undefined,
          files: filesArray,
          isLoadingAttachment: {
            index: filesArray.length - 1,
            isLoadingAttachment: true,
          },
        },
        () => this.addPurchaseOrderAttachment(fileToSubmit, documentClass)
      );
    } else if (!ev) {
      this.setState({ fileDialogOpen: false, fileDialogItems: undefined });
    }
  };

  looseHeaderFocusHandler = (values) => {
    if (this.state.isOnBlurSaveActive) {
      // Call Backend to save Header
      this.setState({ values });
      this.props.looseHeaderFocusHandler(values);
    }
  };

  handleSupplierSearch = createDecorator(
    {
      //TODO: Refactor
      field: SUPPLIER_CODE,
      isEqual: (nextValue, prevValue) =>
        (!prevValue && !nextValue) ||
        (prevValue && nextValue && nextValue.code === prevValue.code),
      updates: {
        [SUPPLIER_NAME]: (currentValue, allValues) => {
          if (
            (currentValue && !allValues[SUPPLIER_NAME]) ||
            (!currentValue && allValues[SUPPLIER_NAME]) ||
            (currentValue &&
              allValues[SUPPLIER_NAME] &&
              currentValue.code !== allValues[SUPPLIER_NAME].code)
          ) {
            //Here we know that the user changed the supplier code.
            if (this.state.fields.length) {
              if (!trackChanges.discardChange) {
                trackChanges.discardChange = true;
                const previousValue = {
                  ...allValues[SUPPLIER_NAME],
                  label: allValues[SUPPLIER_NAME].code,
                  value: allValues[SUPPLIER_NAME].code,
                };
                this.setState({
                  changedField: {
                    fieldName: SUPPLIER_CODE,
                    previousValue,
                    currentValue,
                  },
                  confirmationDialogOpen: true,
                  isOnBlurSaveActive: false,
                });
                //return {...allValues[SUPPLIER_NAME], label: allValues[SUPPLIER_NAME].name, value: allValues[SUPPLIER_NAME].name}
              }
            }
            return currentValue
              ? {
                  ...currentValue,
                  label: allValues[SUPPLIER_CODE].name,
                  value: allValues[SUPPLIER_CODE].name,
                }
              : undefined;
          } else if (allValues[SUPPLIER_NAME]) {
            //Here no changes were made by the user we are just loading data.
            //This is fired because the other three fields
            const oldValue = {
              ...allValues[SUPPLIER_NAME],
              label: allValues[SUPPLIER_NAME].name,
              value: allValues[SUPPLIER_NAME].name,
            };
            // if the current value is undefined all other fields must be undefined
            return currentValue ? oldValue : undefined;
          } else {
            //No changes were made by the user
            //This is fired because the other three fields
            //We need to keep the undefined value
            return undefined;
          }
        },
        [SUPPLIER_TAX_NUMBER]: (currentValue, allValues) => {
          if (
            (currentValue && !allValues[SUPPLIER_TAX_NUMBER]) ||
            (!currentValue && allValues[SUPPLIER_TAX_NUMBER]) ||
            (currentValue &&
              allValues[SUPPLIER_TAX_NUMBER] &&
              currentValue.code !== allValues[SUPPLIER_TAX_NUMBER].code)
          ) {
            return currentValue
              ? {
                  ...currentValue,
                  label: allValues[SUPPLIER_CODE].tax,
                  value: allValues[SUPPLIER_CODE].tax,
                }
              : undefined;
          } else if (allValues[SUPPLIER_TAX_NUMBER]) {
            // in the first iteration the supplierTaxNumber doesn't have label and value.
            const oldValue = {
              ...allValues[SUPPLIER_TAX_NUMBER],
              label: allValues[SUPPLIER_TAX_NUMBER].tax,
              value: allValues[SUPPLIER_TAX_NUMBER].tax,
            };
            return currentValue ? oldValue : undefined;
          } else {
            return undefined;
          }
        },
      },
    },
    {
      field: SUPPLIER_NAME,
      isEqual: (nextValue, prevValue) =>
        (!prevValue && !nextValue) ||
        (prevValue && nextValue && nextValue.code === prevValue.code),
      updates: {
        [SUPPLIER_CODE]: (currentValue, allValues) => {
          if (
            (currentValue && !allValues[SUPPLIER_CODE]) ||
            (!currentValue && allValues[SUPPLIER_CODE]) ||
            (currentValue &&
              allValues[SUPPLIER_CODE] &&
              currentValue.code !== allValues[SUPPLIER_CODE].code)
          ) {
            //Here we know that the user changed the supplier name.
            if (this.state.fields.length) {
              if (!trackChanges.discardChange) {
                trackChanges.discardChange = true;
                const previousValue = {
                  ...allValues[SUPPLIER_CODE],
                  label: allValues[SUPPLIER_CODE].name,
                  value: allValues[SUPPLIER_CODE].name,
                };
                this.setState({
                  changedField: {
                    fieldName: SUPPLIER_NAME,
                    previousValue,
                    currentValue,
                  },
                  confirmationDialogOpen: true,
                  isOnBlurSaveActive: false,
                });
                //return {...allValues[SUPPLIER_CODE], label: allValues[SUPPLIER_CODE].code, value: allValues[SUPPLIER_CODE].code}
              }
            }
            return currentValue
              ? {
                  ...currentValue,
                  label: allValues[SUPPLIER_NAME].code,
                  value: allValues[SUPPLIER_NAME].code,
                }
              : undefined;
          } else if (allValues[SUPPLIER_CODE]) {
            const oldValue = {
              ...allValues[SUPPLIER_CODE],
              label: allValues[SUPPLIER_CODE].code,
              value: allValues[SUPPLIER_CODE].code,
            };
            return currentValue ? oldValue : undefined;
          } else {
            return undefined;
          }
        },
        [SUPPLIER_TAX_NUMBER]: (currentValue, allValues) => {
          if (
            (currentValue && !allValues[SUPPLIER_TAX_NUMBER]) ||
            (!currentValue && allValues[SUPPLIER_TAX_NUMBER]) ||
            (currentValue &&
              allValues[SUPPLIER_TAX_NUMBER] &&
              currentValue.code !== allValues[SUPPLIER_TAX_NUMBER].code)
          ) {
            return currentValue
              ? {
                  ...currentValue,
                  label: allValues[SUPPLIER_NAME].tax,
                  value: allValues[SUPPLIER_NAME].tax,
                }
              : undefined;
          } else if (allValues[SUPPLIER_TAX_NUMBER]) {
            const oldValue = {
              ...allValues[SUPPLIER_TAX_NUMBER],
              ...{
                label: allValues[SUPPLIER_TAX_NUMBER].tax,
                value: allValues[SUPPLIER_TAX_NUMBER].tax,
              },
            };
            return currentValue ? oldValue : undefined;
          } else {
            return undefined;
          }
        },
      },
    },
    {
      field: SUPPLIER_TAX_NUMBER,
      isEqual: (nextValue, prevValue) =>
        (!prevValue && !nextValue) ||
        (prevValue && nextValue && nextValue.code === prevValue.code),
      updates: {
        [SUPPLIER_NAME]: (currentValue, allValues) => {
          if (
            (currentValue && !allValues[SUPPLIER_NAME]) ||
            (!currentValue && allValues[SUPPLIER_NAME]) ||
            (currentValue &&
              allValues[SUPPLIER_NAME] &&
              currentValue.code !== allValues[SUPPLIER_NAME].code)
          ) {
            //Here we know that the user changed the supplier name.
            if (this.state.fields.length) {
              if (!trackChanges.discardChange) {
                trackChanges.discardChange = true;
                const previousValue = {
                  ...allValues[SUPPLIER_NAME],
                  label: allValues[SUPPLIER_NAME].tax,
                  value: allValues[SUPPLIER_NAME].tax,
                };
                this.setState({
                  changedField: {
                    fieldName: SUPPLIER_NAME,
                    previousValue,
                    currentValue,
                  },
                  confirmationDialogOpen: true,
                  isOnBlurSaveActive: false,
                });
                //return {...allValues[SUPPLIER_NAME], label: allValues[SUPPLIER_NAME].name, value: allValues[SUPPLIER_NAME].name}
              }
            }
            return currentValue
              ? {
                  ...currentValue,
                  label: allValues[SUPPLIER_TAX_NUMBER].name,
                  value: allValues[SUPPLIER_TAX_NUMBER].name,
                }
              : undefined;
          } else if (allValues[SUPPLIER_NAME]) {
            const oldValue = {
              ...allValues[SUPPLIER_NAME],
              label: allValues[SUPPLIER_NAME].name,
              value: allValues[SUPPLIER_NAME].name,
            };
            return currentValue ? oldValue : undefined;
          } else {
            return undefined;
          }
        },
        [SUPPLIER_CODE]: (currentValue, allValues) => {
          if (
            (currentValue && !allValues[SUPPLIER_CODE]) ||
            (!currentValue && allValues[SUPPLIER_CODE]) ||
            (currentValue &&
              allValues[SUPPLIER_CODE] &&
              currentValue.code !== allValues[SUPPLIER_CODE].code)
          ) {
            return currentValue
              ? {
                  ...currentValue,
                  label: allValues[SUPPLIER_TAX_NUMBER].code,
                  value: allValues[SUPPLIER_TAX_NUMBER].code,
                }
              : undefined;
          } else if (allValues[SUPPLIER_CODE]) {
            const oldValue = {
              ...allValues[SUPPLIER_CODE],
              label: allValues[SUPPLIER_CODE].code,
              value: allValues[SUPPLIER_CODE].code,
            };
            return currentValue ? oldValue : undefined;
          } else {
            return undefined;
          }
        },
      },
    }
  );

  handleConfirmDialogClose = (acceptChanges, formMutator, headerValues) => {
    const {
      services: { Loading },
    } = this.props;
    if (acceptChanges) {
      Loading.request("setLoading", "JOURNEY").then();
      this.clearAllItems(
        //In case of success
        () => {
          //* If the type was the field changed we must update the finaltype as well.
          if (this.state.changedField.fieldName === "type") {
            this.acceptFieldChanges(
              formMutator,
              PURCHASE_ORDER_FINAL_TYPE,
              this.state.changedField.currentValue
            );
          }
          //TODO Refactor hammer of force update
          const currentValues = { ...headerValues };
          currentValues[
            this.state.changedField.fieldName
          ] = this.state.changedField.currentValue;

          this.setState(
            { fields: [], totalItemValueWithVAT: 0, changedField: false },
            () => {
              trackChanges.discardChange = false;
              //force update
              this.looseHeaderFocusHandler(currentValues);
            }
          );
          Loading.request("stopLoading", "JOURNEY").then();
        },
        //In case of error
        () => {
          const currentValues = { ...headerValues };
          currentValues[
            this.state.changedField.fieldName
          ] = this.state.changedField.previousValue;
          this.discardFieldChanges(
            formMutator,
            this.state.changedField.fieldName,
            this.state.changedField.previousValue
          );
          //TODO Refactor hammer of force update
          this.looseHeaderFocusHandler(currentValues);
          Loading.request("stopLoading").then();
        }
      );
      this.setState({
        confirmationDialogOpen: false,
        isOnBlurSaveActive: true,
      });
    } else {
      const currentValues = { ...headerValues };
      currentValues[
        this.state.changedField.fieldName
      ] = this.state.changedField.previousValue;
      this.discardFieldChanges(
        formMutator,
        this.state.changedField.fieldName,
        this.state.changedField.previousValue
      );
      this.setState(
        {
          confirmationDialogOpen: false,
          isOnBlurSaveActive: true,
          changedField: false,
        },
        () => {
          //TODO Refactor hammer of force update
          trackChanges.discardChange = false;
          this.looseHeaderFocusHandler(currentValues);
        }
      );
    }
  };

  handleTotalValue = (totalValue, updateTotalValue) => {
    if (totalValue >= 0 && totalValue !== this.state.totalItemValueWithoutVAT) {
      updateTotalValue(totalValue);
      this.setState({ totalItemValueWithoutVAT: totalValue });
    }
  };

  render() {
    const {
      classes,
      loadOptions,
      loadSupplierCodeOptions,
      loadSupplierNameOptions,
      loadSupplierTaxOptions,
      loadItems,
      loadFixedAssets,
      deletePurchaseOrder,
      services,
    } = this.props;
    return (
      <div className={classes.root}>
        <Form
          onSubmit={this.onHeaderSubmit}
          initialValues={this.state.initialValues}
          decorators={[this.handleSupplierSearch]}
          mutators={{
            setPurchaseOrderField: (args, state, { setIn }) => {
              //args = [{fieldName, previousValue}]
              //used only when the confirmation model is fired.
              const fieldName = args[0].fieldName;
              state.formState = setIn(
                state.formState,
                `values.${fieldName}`,
                args[0].value
              );
            },
            setPurchaseOrderFinalType: (args, state, { setIn }) => {
              state.formState = setIn(
                state.formState,
                "values.finalType",
                args[0]
              );
            },
            setTotalPurchaseOrderAmount: (args, state, { setIn }) => {
              const pastValue = `${args[0]}`;
              state.formState = setIn(
                state.formState,
                "values.valueWOVat",
                pastValue
              );
            },

            setDepartmentDependencies: (args, state, { setIn }) => {
              state.formState = setIn(
                state.formState,
                `values.internalOrder`,
                undefined
              );
              state.formState = setIn(
                state.formState,
                `values.project`,
                undefined
              );
            },
          }}
          render={({
            handleSubmit,
            values,
            errors,
            pristine,
            touched,
            form,
            invalid,
            hasValidationErrors,
            ...renderProps
          }) => {
            return (
              <form
                onSubmit={(ev, form) => handleSubmit(ev, form)}
                id="header-form"
              >
                <Paper elevation={0} square className={classes.rootPaper}>
                  <Grid container spacing={8}>
                    <Grid item xs={12}>
                      <Separator text="Dados da PO" />
                    </Grid>
                    <Grid
                      item
                      xs={12}
                      className={classes.formSection}
                      onBlur={() => this.looseHeaderFocusHandler(values)}
                    >
                      <POBasicDataForm
                        values={values}
                        errors={errors}
                        loadOptions={loadOptions}
                        loadSupplierCodeOptions={loadSupplierCodeOptions}
                        loadSupplierNameOptions={loadSupplierNameOptions}
                        loadSupplierTaxOptions={loadSupplierTaxOptions}
                        allDisabledFields={this.state.allDisabledFields}
                        classes={classes}
                        validate={this.validateHeader}
                        handleClearItems={this.isToAcceptTheNewChanges}
                        parentMutator={form.mutators}
                      />
                    </Grid>
                    <Grid item xs={12}>
                      <Separator text="Items">
                        {!this.state.allDisabledFields && (
                          <span
                            className={
                              !hasValidationErrors
                                ? classes.buttonIcon
                                : classes.buttonIconDisabled
                            }
                            onClick={() => this.openItemDialog(values)}
                          >
                            <Icon className="icon-add"></Icon>
                          </span>
                        )}
                      </Separator>
                    </Grid>
                    {values && values.finalType ? (
                      <Grid item xs={12} className={classes.formSectionItems}>
                        <ItemsTable
                          type={values.finalType.value}
                          values={this.state.fields}
                          handleEdit={(index) =>
                            this.editItemHandler(index, values)
                          }
                          handleCopy={
                            values.finalType.value === PO_CONTRACT
                              ? this.copyItemHandler
                              : undefined
                          }
                          fields={this.state.fields}
                          handleTotalValue={(ev, ...rest) =>
                            this.handleTotalValue(
                              ev,
                              form.mutators.setTotalPurchaseOrderAmount,
                              rest
                            )
                          }
                          allDisabledFields={this.state.allDisabledFields}
                        />
                        {touched.items && errors.items && (
                          <p className="MuiFormHelperText-root MuiFormHelperText-error">
                            {errors.items}
                          </p>
                        )}
                        {this.state.fields.length > 0 && (
                          <Grid container className={classes.priceSubsection}>
                            <Grid item xs={6}>
                              <Typography className={classes.priceLabel}>
                                Valor Total
                              </Typography>
                            </Grid>
                            <Grid
                              item
                              xs={6}
                              className={classes.priceValue}
                              justify="flex-end"
                            >
                              {this.state.totalItemValueWithVAT
                                ? getFormattedNumber(
                                    this.state.totalItemValueWithVAT,
                                    true,
                                    "EUR"
                                  )
                                : 0}
                            </Grid>
                          </Grid>
                        )}
                      </Grid>
                    ) : null}
                    {!this.state.allDisabledFields && (
                      <>
                        <Grid
                          item
                          xs={12}
                          className={classes.formSectionAttachments}
                        >
                          <FileDialog
                            open={this.state.fileDialogOpen}
                            handleCloseFileDialog={this.handleCloseFileDialog}
                          />
                          <Separator text="Anexos">
                            {!this.state.allDisabledFields && (
                              <FileInput
                                disabled={values && values.type ? false : true}
                                handleFileSelection={this.handleFileSelection}
                              />
                            )}
                          </Separator>
                        </Grid>
                        <Grid
                          item
                          xs={12}
                          className={classes.formSection}
                          style={{ display: "flex" }}
                        >
                          {this.state.files.map((file, index) => (
                            <FileChip
                              I18nProvider={services.I18nProvider}
                              indexLoading={index}
                              isLoading={this.state.isLoadingAttachment}
                              name={file.content.name}
                              key={file.index}
                              documentClass={file.documentClass}
                              onDelete={() =>
                                this.removePurchaseOrderAttachment(file.index)
                              }
                              onDownload={() =>
                                this.downloadAttachmentFromProcessContinuity(
                                  file.index
                                )
                              }
                              canDelete={file.content.canBeDeletedByCurrentUser}
                            />
                          ))}
                        </Grid>
                      </>
                    )}
                  </Grid>
                </Paper>

                <Grid container spacing={8} className={classes.buttonContainer}>
                  <Grid item xs></Grid>
                  {this.state.allDisabledFields ? (
                    <Grid item>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={deletePurchaseOrder}
                      >
                        Cancelar
                      </Button>
                    </Grid>
                  ) : (
                    <Grid item>
                      <Button onClick={deletePurchaseOrder}> Apagar</Button>
                    </Grid>
                  )}
                  {!this.state.allDisabledFields && (
                    <Grid item>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={this.emulateSaveFormClick}
                        disabled={
                          this.state.isLoadingAttachment.isLoadingAttachment ||
                          hasValidationErrors ||
                          this.state.allDisabledFields ||
                          this.state.fields.length === 0
                        }
                      >
                        Submeter
                      </Button>
                    </Grid>
                  )}
                </Grid>
                <ConfirmationDialog
                  open={this.state.confirmationDialogOpen}
                  title={services.I18nProvider.Texti18n(
                    "create_po.custom_dialog.title.confirm_clear_items"
                  )}
                  onClose={(ev) =>
                    this.handleConfirmDialogClose(ev, form.mutators, values)
                  }
                />
              </form>
            );
          }}
        />
        <Form
          onSubmit={this.onItemSubmit}
          initialValues={this.state.selectedItem}
          validate={this.validateItem}
          render={({
            handleSubmit,
            form,
            values,
            errors,
            pristine,
            touched,
            valid,
            hasValidationErrors,
            ...renderProps
          }) => {
            return (
              <form
                id="item-form"
                onSubmit={(ev, form) => {
                  handleSubmit(ev, form);
                }}
              >
                <ItemDialog
                  open={this.state.isItemDialogOpen}
                  close={() => this.closeItemDialog(form)}
                  handleDelete={this.deleteItemHandler}
                  save={this.emulateSaveItemClick}
                  title={this.state.itemDialogTitle}
                  errors={errors}
                  valid={!hasValidationErrors}
                  action={this.state.itemDialogAction}
                  itemIndex={this.state.selectedItemIndex}
                >
                  <ItemForm
                    values={values}
                    fieldValues={this.state.fields}
                    errors={errors}
                    loadItems={loadItems}
                    loadFixedAssets={loadFixedAssets}
                    type={values.finalType || this.state.type}
                    marketType={this.state.currentSupplier.marketType}
                  ></ItemForm>
                </ItemDialog>
              </form>
            );
          }}
        />
      </div>
    );
  }
}

export default withStyles(styles)(withRouter(FormPO));
