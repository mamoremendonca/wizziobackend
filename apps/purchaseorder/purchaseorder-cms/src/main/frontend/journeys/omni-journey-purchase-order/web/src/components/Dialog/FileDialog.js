import React, { Component } from "react";

import { withStyles } from "@ui-lib/core/styles";
import styles from "./styles";
import { Select } from "../FFComponentWrappers";
import ConfirmationDialog from "./ConfirmationDialog";

const documentClassOptions = [
  { value: "GENERIC", label: "Documento Geral" },
  { value: "PURCHASE_ORDER", label: "Documento da PO" }
];

class FileDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDocumentClass: ""
    };
  }

  handleDocumentSelection = event => {
    if (event && event.value) {
      this.setState({ selectedDocumentClass: event.value });
    }
  };

  render() {
    const { open, handleCloseFileDialog } = this.props;
    const { selectedDocumentClass } = this.state;
    return (
      <ConfirmationDialog
        open={open}
        title="Qual é a classe documental do anexo selecionado?"
        onClose={ev => handleCloseFileDialog(ev, selectedDocumentClass)}
        disabled={selectedDocumentClass ? false : true}
      >
        <Select
          input={{
            name: "selectedDocumentClass",
            onChange: this.handleDocumentSelection
          }}
          formControlProps={{ fullWidth: true }}
          InputLabelProps={{ shrink: true }}
          options={documentClassOptions}
          value={selectedDocumentClass}
        />
      </ConfirmationDialog>
    );
  }
}

export default withStyles(styles)(FileDialog);
