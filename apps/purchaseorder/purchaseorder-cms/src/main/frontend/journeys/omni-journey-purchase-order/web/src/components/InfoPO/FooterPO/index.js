import React from "react";

import styles from "./styles";
import { withStyles } from "@ui-lib/core/styles";

import Grid from "@ui-lib/core/Grid";
import TextField from "@ui-lib/core/TextField";
import Radio from "@ui-lib/core/Radio";
import RadioGroup from "@ui-lib/core/RadioGroup";
import FormControlLabel from "@ui-lib/core/FormControlLabel";
import FormHelperText from "@ui-lib/core/FormHelperText";
import { dateFormater } from "../utils";

const footerPOFields = (poHeader) => [
  { label: "Versão", value: poHeader.version },
  { label: "Data de Inicio", value: dateFormater(poHeader.creationDate) },
  { label: "Data de conclusão", value: dateFormater(poHeader.approvalDate) },
];

const renderFooterField = (field, classes) => {
  return (
    <Grid item key={field.label}>
      <TextField
        name={field.label}
        label={field.label}
        value={field.value}
        disabled
        className={classes.disabledInfoField}
      />
    </Grid>
  );
};

const FooterPO = ({
  classes,
  isApproving,
  approvalDecisionChangeHandler,
  commentDecisionChangeHandler,
  decision,
  comment,
  availableActions,
  I18nProvider,
  showError,
  purchaseOrderHeader,
}) => {
  return (
    <div className={classes.root}>
      {!isApproving && (
        <Grid container className={classes.container}>
          {footerPOFields(purchaseOrderHeader).map((field) =>
            renderFooterField(field, classes)
          )}
        </Grid>
      )}
      {isApproving && availableActions && availableActions.length > 0 && (
        <Grid container spacing={12}>
          {availableActions.length > 0 && (
            <Grid item>
              <RadioGroup
                onChange={(e) => approvalDecisionChangeHandler(e.target.value)}
                value={decision}
                className={classes.buttonGroupContainer}
                name={"handleChangeDecision"}
                aria-label="decision"
              >
                {availableActions.map((action) => (
                  <FormControlLabel
                    key={action}
                    value={action}
                    control={
                      <Radio
                        className={
                          (showError & !decision && classes.radioButton) ||
                          undefined
                        }
                        color="primary"
                      />
                    }
                    label={I18nProvider.Texti18n(action)}
                  />
                ))}
              </RadioGroup>
              {showError && !decision && (
                <FormHelperText className={classes.radioButtonErrorText}>
                  {"Selecione uma opção"}
                </FormHelperText>
              )}
            </Grid>
          )}
          <Grid item xs={2} />
          <Grid item xs={3}>
            <TextField
              label="Descrição"
              required={decision && decision !== "APPROVE"}
              value={comment}
              onChange={(e) => commentDecisionChangeHandler(e.target.value)}
              fullWidth
              InputLabelProps={{ shrink: true }}
              helperText={
                showError && !comment && decision && decision !== "APPROVE"
                  ? "Campo obrigatório"
                  : undefined
              }
              error={showError && !comment ? true : false}
            />
          </Grid>
        </Grid>
      )}
    </div>
  );
};

export default withStyles(styles)(FooterPO);
