import React, { Component } from "react";

import { styles } from "./styles";
import { withStyles } from "@ui-lib/core/styles";

// UI-LIB-ONEY
import ActivityTimeline from "@ui-lib/oney/ActivityTimeline";
import Icon from "@ui-lib/core/Icon";
import IconButton from "@ui-lib/core/IconButton";
import Grid from "@ui-lib/core/Grid";
import AttachmentList from "./AttachmentList";
import {
	getAttachmentListMapped,
	getTimelineDataMapped,
	downloadFile
} from "../../../utils/timeline";
import { connect } from "react-redux";
import FileInput from "../../SharedComponents/FileInput";
import FileDialog from "../../Dialog/FileDialog";
import {
	addTimelineAttachmentRequest,
	removeTimelineAttachmentRequest
} from "../../../actions/timeline";

import { downloadAttachmentRequest } from "../../../actions/services/purchase-order";

const ACTIVITY = "Actividades";
const ATTACHMENTS = "Anexos";

class Timeline extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedTab: ACTIVITY,
			fileDialogOpen: false,
			fileDialogItems: undefined,
			allowAddAttachments: false
		};
	}

	componentDidUpdate(prevProps, prevState) {
		const { purchaseOrderData } = this.props;
		
		if (purchaseOrderData && this.state !== prevState) {
			// * logic to allow or disable the add attachments.			
			if (purchaseOrderData && purchaseOrderData.jwcontext && purchaseOrderData.jwcontext.id && !this.state.allowAddAttachments) {
				this.setState({ allowAddAttachments: true });
			} else if (purchaseOrderData && !purchaseOrderData.jwcontext && this.state.allowAddAttachments){
				this.setState({ allowAddAttachments: false });
			}
		}
	}

	getTabClass = tabName => {
		const { classes } = this.props;
		return tabName === this.state.selectedTab
			? classes.selectedHeaderTab
			: classes.headerTab;
	};

	handleSelectedTab = tabName => {
		tabName !== this.state.selectedTab &&
			this.setState({ selectedTab: tabName });
	};

	downloadAttachment = (index) => {
		const { purchaseOrderId, services, isFromWorkList } = this.props;
		const dataSource = isFromWorkList ? 'process_continuity' : 'purchase_order_repository';

		downloadAttachmentRequest(purchaseOrderId, index, services, dataSource).then(response => {
			if (response) {
				downloadFile(services.FileSystem, response.data, response.headers);
			}
		});
	};

	//* can be used for DF
	// handleFileSelection = (files) => {
	// 	if (files[0]) {
	// 		this.setState({ fileDialogOpen: true, fileDialogItems: files[0] })
	// 	}
	// }

	// handleFileSelection = files => {
	// 	const { HttpClient, Notifications } = this.props.services;
	// 	const { purchaseOrderId, addTimelineAttachmentRequest } = this.props;
	// 	if (files[0]) {			
	// 		addTimelineAttachmentRequest(
	// 			HttpClient,
	// 			Notifications,
	// 			files[0],
	// 			purchaseOrderId,
	// 			'PURCHASE_ORDER'
	// 		)
	// 	}
	// };

	handleCloseFileDialog = (ev, documentClass) => {
		const { HttpClient, Notifications } = this.props.services;
		const { purchaseOrderId, addTimelineAttachmentRequest } = this.props;
		if (ev && documentClass) {
			const fileToSubmit = this.state.fileDialogItems;
			this.setState({ fileDialogOpen: false, fileDialogItems: undefined }, () =>
				addTimelineAttachmentRequest(
					HttpClient,
					Notifications,
					fileToSubmit,
					purchaseOrderId,
					documentClass
				)
			);
		} else if (!ev) {
			this.setState({ fileDialogOpen: false, fileDialogItems: undefined });
		}
	};

	removePurchaseOrderAttachment = index => {
		const { HttpClient, Notifications } = this.props.services;
		const { purchaseOrderId, removeTimelineAttachmentRequest } = this.props;
		removeTimelineAttachmentRequest(
			HttpClient,
			Notifications,
			index,
			purchaseOrderId
		);
	};

	render() {
		const {
			timelineData,
			classes,
			I18nProvider,
			handleRefreshTimeline,
			someAttachmentIsLoading,
			addAttachment,
			removeAttachment
		} = this.props;
		const { selectedTab } = this.state;
		return (
			<div>
				<div className={classes.headerContainer}>
					<div className={classes.flexRowContainer}>
						<div className={classes.iconContainer}>
							<Icon className={classes.icon}>list</Icon>
						</div>
						<div style={{ padding: 20 }}>Histórico</div>
					</div>
					<Grid container className={classes.flexRowContainer}>
						<Grid
							item
							xs={6}
							className={this.getTabClass(ACTIVITY)}
							onClick={() => this.handleSelectedTab(ACTIVITY)}
						>
							{ACTIVITY}
						</Grid>
						<Grid
							item
							xs={6}
							className={this.getTabClass(ATTACHMENTS)}
							onClick={() => this.handleSelectedTab(ATTACHMENTS)}
						>
							{ATTACHMENTS}
						</Grid>
					</Grid>
				</div>
				<div className={classes.timelineActionsContainer}>
					<IconButton
						disabled={someAttachmentIsLoading}
						onClick={handleRefreshTimeline}
						className={classes.refresh}
					>
						<Icon>autorenew</Icon>
					</IconButton>
					{selectedTab === ATTACHMENTS && (
						<div>
							{this.state.allowAddAttachments && 
								(<>
									<FileInput handleFileSelection={addAttachment} />
									<FileDialog
										open={this.state.fileDialogOpen}
										handleCloseFileDialog={this.handleCloseFileDialog}
									/>
								</>)
							}							
						</div>
					)}
				</div>
				<div className={classes.contentComponentContainer}>
					{selectedTab === ACTIVITY && timelineData.length > 0 && (
						<ActivityTimeline
							downloadClick={attachment => {
								this.downloadAttachment(attachment.id);
							}}
							closeClick={attachment =>
								removeAttachment(attachment.id)
							}
							data={getTimelineDataMapped(I18nProvider, timelineData)}
						></ActivityTimeline>
					)}
					{selectedTab === ATTACHMENTS && timelineData.length > 0 && (
						<AttachmentList
							attachmentsData={getAttachmentListMapped(
								timelineData,
								I18nProvider
							)}
							downloadAttachment={this.downloadAttachment}
							deleteAttachment = {removeAttachment}
						/>
					)}
				</div>
			</div>
		);
	}
}

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		addTimelineAttachmentRequest: (
			HttpClient,
			Notifications,
			attachment,
			purchaseOrderId,
			documentClass
		) => {
			dispatch(
				addTimelineAttachmentRequest(
					HttpClient,
					Notifications,
					attachment,
					purchaseOrderId,
					documentClass
				)
			);
		},
		removeTimelineAttachmentRequest: (
			HttpClient,
			Notifications,
			index,
			purchaseOrderId
		) => {
			dispatch(
				removeTimelineAttachmentRequest(
					HttpClient,
					Notifications,
					index,
					purchaseOrderId
				)
			);
		}
	};
};

const mapStateToProps = ({
	purchaseOrderReducer:{purchaseOrderId, purchaseOrderData}, 
	journey:{services}
}) => ({
	services,
	purchaseOrderId,
	purchaseOrderData		
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(withStyles(styles)(Timeline));
