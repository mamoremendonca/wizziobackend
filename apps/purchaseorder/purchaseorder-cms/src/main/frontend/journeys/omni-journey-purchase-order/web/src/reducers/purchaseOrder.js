import * as constants from "../constants";
import { Map } from "immutable";

const initialState = {
  status: undefined,
  purchaseOrderNumber: undefined,
  purchaseOrderType: undefined,
  supplierCode: undefined,
  purchaseOrderId: undefined,
  purchaseOrderData: undefined,
  purchaseOrderAttachments: undefined,
  purchseOrderCreationDate: undefined,
  purchaseOrderFiles: undefined,
  //Manager Purchase Order Resoruces
  dependencies: Map({ lov: false, suppliers: false }),
  ///PO List area
  purchaseOrderList: [],
  totalPurchaseOrders: 0,
  numberOfRecords: 0,
  // Filter area
  searchCriteria: {
    filterCriteria: { ...constants.INITIAL_FILTER_CRITERIA },
    sortCriteria: {
      orderField: constants.DEFAULT_SORTED_FIELD["orderField"],
      orderType: constants.DEFAULT_SORTED_FIELD["orderType"]
    }
  }
};


export const purchaseOrderReducer = (state, action) => {
 
  if (state === undefined) return initialState;
  switch (action.type) {
    case constants.SET_PURCHASE_ORDER: {
      const purchaseOrderData = action.payload;
      const purchaseOrderId = purchaseOrderData.jwcontext
        ? purchaseOrderData.jwcontext.id
        : purchaseOrderData.purchaseOrderHeader.repositoryId;
      return { ...state, purchaseOrderId, purchaseOrderData };
    }
    case constants.CLEAR_PURCHASE_ORDER: {
      return {
        ...state,
        purchaseOrderId: undefined,
        purchaseOrderData: undefined,
        purchaseOrderAttachments:undefined
      };
    }
    case constants.CREATE_PURCHASE_ORDER: {
      const message = action.payload;
      return { ...state, message };
    }
    case constants.CREATE_PURCHASE_ORDER_SUCESS: {
      const purchaseOrderId = action.payload.id;
      const purchaseOrderCreationDate = action.payload.creationDate;
      return { ...state, purchaseOrderId, purchaseOrderCreationDate };
    }
    case constants.CREATE_PURCHASE_ORDER_ERROR: {
      const message = action.payload;
      return { ...state, message };
    }
    case constants.DELETE_PURCHASE_ORDER: {
      const lovs = action.payload;
      return { ...state, lovs };
    }
    case constants.UPDATE_PURCHASE_ORDER: {
      const message = action.payload;
      return { ...state, message };
    }
    case constants.SET_PO_LIST_PROCESSES:
      return {
        ...state,
        purchaseOrderList: action.payload.processes,
        totalPurchaseOrders: action.payload.totals,
        numberOfRecords: action.payload.numberOfRecords
      };
    case constants.UPDATE_FILTER_CRITERIA: {
      return {
        ...state,
        searchCriteria: {
          ...state.searchCriteria,
          filterCriteria: action.payload
        }
      };
    }
    case constants.UPDATE_SORTING_CRITERIA: {
      return {
        ...state,
        searchCriteria: {
          ...state.searchCriteria,
          sortCriteria: action.payload
        }
      };
    }
    case constants.RESET_FILTER_CRITERIA: {
      return {
        ...state,
        searchCriteria: {
          ...state.searchCriteria,
          filterCriteria: { ...constants.INITIAL_FILTER_CRITERIA }
        }
      };
    }
    case constants.SET_PURCHASE_ORDER_DEPENDENCY: {
      return {
        ...state,
        dependencies: state.dependencies.set(action.payload, true)
      };
    }
    default:
      return state;
  }
};
