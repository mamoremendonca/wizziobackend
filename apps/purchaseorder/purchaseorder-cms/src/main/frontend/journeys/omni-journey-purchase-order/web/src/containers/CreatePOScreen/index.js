import React, { Component } from "react";
import { connect } from "react-redux";
// Components
import withContainerWrapper from "../../components/ContainerWrapper";
import FormPO from "../../components/FormPO/FormPO";
import ConfirmationDialog from "../../components/Dialog/ConfirmationDialog";
// Actions
import {
  createPurchaseOrder,
  updatePurchaseOrder,
  setPurchaseOrder,
  submitPurchaseOrder,
  getPurchaseOrderData,
  clearPurchaseOrder,
  getTimelineData,
} from "../../actions";

import {
  handleSuccessNotificationRequest,
  handleWarningNotificationRequest,
  handleErrorRequest,
  WarningMessages,
  ErrorMessages,
  SuccessMessages,
  verifyIfLastStepWasRejected,
} from "../../utils";
import {
  mapValuesToPurchaseOrder,
  submitPurchaseOrderRequest,
  mapPurchaseOrderToValues,
  mapItemsToValues,
  loadOptionsUtil,
} from "./utils";

import { bindActionCreators } from "redux";

import { deletePurchaseOrderRequest } from "../../actions/services/purchase-order";

class CreatePOScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isConfirmCancelVisible: false,
      PurchaseOrderId: 0,
      isMock: true,
      isFromWorkList: false,
      isFromCopy: false,
      showRejectReason: true,
      timeLineDataDependencyResolved: false,
    };
  }

  componentDidMount() {
    const {
      createPurchaseOrder,
      getPurchaseOrderData,
      Services,
      match,
    } = this.props;
    if (match && match.params && match.params.id) {
      if (this.props.history.length >= 3) {
        this.setState({ isFromCopy: true });
      } else {
        this.setState({ isFromWorkList: true }, () => {
          getPurchaseOrderData(
            Services.HttpClient,
            Services.Notifications,
            match.params.id
          );
        });
      }
    } else {
      createPurchaseOrder(Services);
    }
  }

  componentDidUpdate(prevProps) {
    //TODO Refactor to have loading like PurchaseOrderDetails.
    const {
      purchaseOrderData,
      timelineData,
      Services: { Notifications, HttpClient },
      getTimelineData,
    } = this.props;
    const {
      showRejectReason,
      isFromWorkList,
      timeLineDataDependencyResolved,
    } = this.state;
    //*this is only used for case we want to know the reject reason
    if (
      isFromWorkList &&
      purchaseOrderData &&
      prevProps.purchaseOrderData !== purchaseOrderData &&
      purchaseOrderData.jwcontext.status === "Rejected"
    ) {
      if (!timeLineDataDependencyResolved) {
        getTimelineData(
          HttpClient,
          Notifications,
          purchaseOrderData.jwcontext.id
        );
      }
    } else if (
      isFromWorkList &&
      timelineData &&
      prevProps.timelineData !== timelineData &&
      !timeLineDataDependencyResolved
    ) {
      this.setState({ timeLineDataDependencyResolved: true });
      if (showRejectReason) {
        const wasRejected = verifyIfLastStepWasRejected(timelineData);
        if (wasRejected) {
          handleWarningNotificationRequest(
            Notifications,
            WarningMessages(
              purchaseOrderData.purchaseOrderHeader.friendlyNumber,
              wasRejected
            ).TPO_REJECTED_WARNING
          );
        }
        this.setState({ showRejectReason: false });
      }
    }
  }

  handleBackClick = () => {
    const {
      history,
      JourneyActions,
      JourneySettings,
      clearPurchaseOrder,
    } = this.props;
    const { isFromWorkList, isFromCopy } = this.state;
    if (isFromWorkList) {
      JourneyActions.closeJourney(JourneySettings.instance_id);
    } else if (isFromCopy) {
      history.push("/");
    } else {
      clearPurchaseOrder();
      history.goBack();
    }
  };

  looseHeaderFocusHandler = async (values) => {
    const { HttpClient } = this.props.Services;
    const { updatePurchaseOrder } = this.props;
    const purchaseOrderMapped = mapValuesToPurchaseOrder(
      values,
      this.props.PurchaseOrderId
    );
    await updatePurchaseOrder(HttpClient, purchaseOrderMapped);
  };

  deletePurchaseOrder = () => {
    const {
      Services: { HttpClient, Notifications },
      PurchaseOrderId,
    } = this.props;
    deletePurchaseOrderRequest(HttpClient, PurchaseOrderId)
      .then(() => {
        this.setState({ isConfirmCancelVisible: false }, () =>
          this.handleBackClick()
        );
      })
      .catch((error) => {
        handleErrorRequest(Notifications, ErrorMessages.PO_DELETE_ERROR);
      });
  };

  submitPurchaseOrder = () => {
    const { Notifications, HttpClient } = this.props.Services;
    submitPurchaseOrderRequest(HttpClient, this.props.PurchaseOrderId)
      .then((res) => {
        handleSuccessNotificationRequest(
          Notifications,
          SuccessMessages.SubmitRequestMessage(res.data.friendlyNumber)
        );
        this.props.clearPurchaseOrder();
        this.handleBackClick();
      })
      .catch(() => {
        handleErrorRequest(Notifications, ErrorMessages.PO_SUBMIT_ERROR);
      });
  };

  handleFormCancel = (pristineStatus) => {
    if (!pristineStatus) this.showConfirmCancel();
    else {
      this.props.clearPurchaseOrder();
      this.props.history.goBack();
    }
  };

  showConfirmCancel = () => {
    this.setState({ isConfirmCancelVisible: true });
  };

  handleConfirmCancelClose = (isConfirmed) => {
    if (isConfirmed) {
      this.deletePurchaseOrder();
    } else {
      this.setState({ isConfirmCancelVisible: false });
    }
  };

  loadSupplierCodeOptions = (inputValue) => {
    const { suppliers } = this.props;
    return loadOptionsUtil.loadSupplierCodeOptions(suppliers, inputValue);
  };

  loadSupplierNameOptions = (inputValue) => {
    const { suppliers } = this.props;
    return loadOptionsUtil.loadSupplierNameOptions(suppliers, inputValue);
  };

  loadSupplierTaxOptions = (inputValue) => {
    const { suppliers } = this.props;
    return loadOptionsUtil.loadSupplierTaxOptions(suppliers, inputValue);
  };

  loadItems = (inputValue) => {
    const { items } = this.props;
    return loadOptionsUtil.loadItems(items, inputValue);
  };

  loadFixedAssets = (inputValue) => {
    const { assets } = this.props;
    return loadOptionsUtil.loadFixedAssets(assets, inputValue);
  };

  loadOptions = (inputValue) => {
    return this.props.suppliers.filter((supplier) =>
      supplier.label.includes(inputValue)
    );
  };

  render() {
    const {
      PurchaseOrderId,
      purchaseOrderData,
      Services,
      lovs,
      suppliers,
    } = this.props;

    return (
      <div>
        {PurchaseOrderId && purchaseOrderData && (
          <FormPO
            initialPOValues={{
              purchaseOrderData:
                purchaseOrderData &&
                mapPurchaseOrderToValues(
                  purchaseOrderData.purchaseOrderHeader,
                  lovs,
                  suppliers
                ),
              purchaseOrderHeader: purchaseOrderData.jwcontext,
              purchaseOrderItems:
                (purchaseOrderData.items &&
                  mapItemsToValues(purchaseOrderData, lovs)) ||
                [],
            }}
            loadSupplierCodeOptions={this.loadSupplierCodeOptions}
            loadSupplierNameOptions={this.loadSupplierNameOptions}
            loadSupplierTaxOptions={this.loadSupplierTaxOptions}
            loadItems={this.loadItems}
            loadFixedAssets={this.loadFixedAssets}
            looseHeaderFocusHandler={this.looseHeaderFocusHandler}
            deletePurchaseOrder={this.showConfirmCancel}
            purchaseOrderId={PurchaseOrderId}
            services={Services}
            submitPurchaseOrder={this.submitPurchaseOrder}
          />
        )}
        <ConfirmationDialog
          open={this.state.isConfirmCancelVisible}
          title="Pretende prosseguir com a eliminação? Esta ação não é reversível."
          onClose={this.handleConfirmCancelClose}
        ></ConfirmationDialog>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    Services: state.journey.services,
    PurchaseOrderId: state.purchaseOrderReducer.purchaseOrderId,
    purchaseOrderCreationDate:
      state.purchaseOrderReducer.purchaseOrderCreationDate,
    suppliers: state.lovReducer.suppliers,
    assets: state.lovReducer.lovs.assets,
    items: state.lovReducer.lovs.items,
    lovs: state.lovReducer.lovs,
    purchaseOrderData: state.purchaseOrderReducer.purchaseOrderData,
    timelineData: state.timelineReducer.timelineData,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      setPurchaseOrder,
      createPurchaseOrder,
      updatePurchaseOrder,
      submitPurchaseOrder,
      getPurchaseOrderData,
      clearPurchaseOrder,
      getTimelineData,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withContainerWrapper(CreatePOScreen));
