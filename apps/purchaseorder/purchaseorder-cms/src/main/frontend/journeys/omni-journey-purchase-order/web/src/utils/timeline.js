import { dateFormater } from "../components/InfoPO/utils";

export const getTimelineStepIcon = (status) => {
  switch (status) {
    case "SUBMITTED":
      return "view_headline";
    case "APPROVED":
      return "check";
    case "REJECTED":
      return "close";
    case "RETURNED":
      return "keyboard_return";
    case "DRAFT":
      return "lens";
    case "TO_APPROVE":
    case "PREVISION":
      return "access_time";
    case "JUMP":
      return "not_interested";
    default:
  }
};

const getTimelineItems = (stepData) => {
  const items = [];

  if (stepData.comment && stepData.comment.trim().length > 0) {
    items.push({
      header: "Motivo",
      content: stepData.comment,
    });
  }
  if (stepData.userGroup) {
    items.push({
      header: "Atribuido a",
      content: stepData.userGroup,
    });
  }
  return items;
};

export const getTimelineDataMapped = (I18nProvider, timelineData) => {
  return {
    iconSize: "16",
    content: timelineData.map((stepData, index) => {
      return {
        title: I18nProvider.Texti18n(stepData.type),
        date: stepData.date,
        selected: stepData.isCurrentStep,
        user: stepData.ownerName || "",
        icon: stepData.date
          ? getTimelineStepIcon(stepData.type)
          : getTimelineStepIcon("PREVISION"),
        borderStyle:
          stepData.date || stepData.isCurrentStep ? "solid" : "dashed",
        borderColor: "#0f0",
        items: getTimelineItems(stepData),
        annexes:
          stepData.attachments && stepData.attachments.length > 0
            ? stepData.attachments.map((attachmentRaw) => {
                return {
                  id: attachmentRaw.index,
                  fileName: attachmentRaw.name,
                  uploader: attachmentRaw.ownerName,
                  date: dateFormater(attachmentRaw.date),
                  editMode: false,
                };
              })
            : [],
      };
    }),
  };
};

export const getAttachmentListMapped = (timelineData, I18nProvider) => {
  return timelineData.map((stepData, index) => {
    return {
      id: `${stepData.type}${index}`,
      status: I18nProvider.Texti18n(stepData.type),
      user: stepData.ownerName,
      attachments:
        stepData.attachments && stepData.attachments.length > 0
          ? stepData.attachments.map((attachmentRaw) => {
              return {
                id: attachmentRaw.index,
                name: attachmentRaw.name,
                date: dateFormater(attachmentRaw.date),
                isAbleToDelete: attachmentRaw.canBeDeletedByCurrentUser,
                isLoading: attachmentRaw.isLoading,
              };
            })
          : [],
    };
  });
};

// TODO - ASK BACKEND TO SEND FIELD INSTEAD OF HAVING TO DO THESE VERIFY FUNCTIONS
export const verifyIfLastStepWasReturned = (timelineData) => {
  let wasReturned = undefined;

  timelineData &&
    timelineData.forEach((stepData, index) => {
      if (
        stepData.date &&
        timelineData[index + 1] &&
        !timelineData[index + 1].date
      ) {
        if (stepData.type === "RETURNED") {
          wasReturned = stepData.comment || "-";
        }
      }
    });

  return wasReturned;
};

export const verifyIfLastStepWasRejected = (timelineData) => {
  let wasReturned = undefined;

  timelineData &&
    timelineData.forEach((stepData) => {
      if (stepData.type === "REJECTED") {
        wasReturned = stepData.comment || "-";
      }
    });

  return wasReturned;
};
export const b64toBlob = (b64Data, contentType = "", sliceSize = 512) => {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);

    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  const blob = new Blob(byteArrays, { type: contentType });
  return blob;
};

export const downloadFile = (fileSystem, b64File, headers) => {
  let defaultNameFile = "document";

  if (headers["content-disposition"]) {
    defaultNameFile = headers["content-disposition"].replace(
      "attachment; filename=",
      ""
    );
  } else if (headers["content-type"]) {
    defaultNameFile = headers["content-type"].replace("application/", "");
  }

  fileSystem.downloadFile(b64toBlob(b64File), defaultNameFile);
};
