/**
 * Only use for @ui-lib/oney/Autocomplete 
* To translate the description using i18nProvider.
* In the autocomplete component the values are put in the keys and the descriptions are put in the value.
* @param {*} lov 
*/
export const autoCompleteTranslateLovToDataSources = (lov, i18nProvider) => {
	const dataSource = [];
	lov.forEach(element => {
		dataSource.push({ key: element.code, value: i18nProvider.Texti18n(element.description) });
	});
	return dataSource;
}

/**
* For other combos
* To translate the description using i18nProvider
* @param {*} lov 
*/
export const comboTranslateLovToDataSources = (lov, i18nProvider) => {
	const dataSource = [];
	lov.forEach(element => {
		dataSource.push({ value: element.code, label: i18nProvider.Texti18n(element.description) });
	});
	return dataSource; 
}
