package com.novabase.omni.oney.apps.financialdocument.manager;

import java.time.LocalDateTime;
import java.util.HashMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.novabase.omni.oney.apps.financialdocument.dto.ItemDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.entity.Item;
import com.novabase.omni.oney.apps.financialdocument.validator.AddItemValidator;

import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.api.trigger.instance.JourneyInstanceHeader;

public class ItemManagerTest {

    @InjectMocks
    private ItemManager itemManager;
    
    @Mock
    private AddItemValidator addItemValidator;
    
    private ItemDTO defaultItemDTO;
    
    private static final String DEFAULT_FINANCIAL_DOCUMENT_ITEM_ITEM_CODE = "Item1";
    private static final String DEFAULT_FINANCIAL_DOCUMENT_ITEM_TAX_CODE = "TaxCode1";
    private static final String DEFAULT_FINANCIAL_DOCUMENT_ITEM_COST_CENTER_CODE = "CostCenter1";
    private static final String DEFAULT_FINANCIAL_DOCUMENT_ITEM_DESCRIPTION = "Description1";
    
    @Before
    public void setUp() {
	
	MockitoAnnotations.initMocks(this);
	
	// ITEM DTO 
	this.defaultItemDTO = new ItemDTO();
	this.defaultItemDTO.setCode(DEFAULT_FINANCIAL_DOCUMENT_ITEM_ITEM_CODE);
	this.defaultItemDTO.setDescription(DEFAULT_FINANCIAL_DOCUMENT_ITEM_DESCRIPTION);
	this.defaultItemDTO.setTaxCode(DEFAULT_FINANCIAL_DOCUMENT_ITEM_TAX_CODE);
	this.defaultItemDTO.setCostCenterCode(DEFAULT_FINANCIAL_DOCUMENT_ITEM_COST_CENTER_CODE);
	this.defaultItemDTO.setAmount(1D);
	this.defaultItemDTO.setUnitPriceWithoutTax(1D);
	this.defaultItemDTO.setStartDate(LocalDateTime.now());
	this.defaultItemDTO.setEndDate(LocalDateTime.now().plusDays(2L));
	
    }
    
    @Test
    public void addItemTest() throws Exception {
	
	final FinancialDocumentInstance instance = new FinancialDocumentInstance();
	instance.setHeader(new JourneyInstanceHeader());
	instance.getHeader().targets = new HashMap<String, String>();
	final Item itemInstance = new Item();
	itemInstance.setIndex(1);
	instance.getItems().add(itemInstance);
	
	final ItemDTO itemDTOMapped = this.itemManager.addItem(instance, this.defaultItemDTO);
	
	Assert.assertEquals(instance.getItems().size(), 2);
	Assert.assertEquals(instance.getItems().get(0).getIndex(), 1);
	Assert.assertEquals(instance.getItems().get(1).getIndex(), 2);
	Assert.assertEquals(instance.getItems().get(1).getCode(), this.defaultItemDTO.getCode());
	Assert.assertEquals(instance.getItems().get(1).getDescription(), this.defaultItemDTO.getDescription());
	Assert.assertEquals(instance.getItems().get(1).getTaxCode(), this.defaultItemDTO.getTaxCode());
	Assert.assertEquals(instance.getItems().get(1).getCostCenterCode(), this.defaultItemDTO.getCostCenterCode());
	Assert.assertEquals(instance.getItems().get(1).getAmount(), this.defaultItemDTO.getAmount());
	Assert.assertEquals(instance.getItems().get(1).getUnitPriceWithoutTax(), this.defaultItemDTO.getUnitPriceWithoutTax());
	Assert.assertEquals(instance.getItems().get(1).getStartDate(), this.defaultItemDTO.getStartDate());
	Assert.assertEquals(instance.getItems().get(1).getEndDate(), this.defaultItemDTO.getEndDate());
	Assert.assertEquals(itemDTOMapped.getIndex(), 2);
	Assert.assertEquals(itemDTOMapped.getCode(), this.defaultItemDTO.getCode());
	Assert.assertEquals(itemDTOMapped.getDescription(), this.defaultItemDTO.getDescription());
	Assert.assertEquals(itemDTOMapped.getTaxCode(), this.defaultItemDTO.getTaxCode());
	Assert.assertEquals(itemDTOMapped.getCostCenterCode(), this.defaultItemDTO.getCostCenterCode());
	Assert.assertEquals(itemDTOMapped.getAmount(), this.defaultItemDTO.getAmount());
	Assert.assertEquals(itemDTOMapped.getUnitPriceWithoutTax(), this.defaultItemDTO.getUnitPriceWithoutTax());
	Assert.assertEquals(itemDTOMapped.getStartDate(), this.defaultItemDTO.getStartDate());
	Assert.assertEquals(itemDTOMapped.getEndDate(), this.defaultItemDTO.getEndDate());
	
    }
    
    @Test
    public void removeItemTest() {
	
	final FinancialDocumentInstance instance = new FinancialDocumentInstance();
	instance.setHeader(new JourneyInstanceHeader());
	instance.getHeader().targets = new HashMap<String, String>();
	final Item itemInstance = new Item();
	itemInstance.setIndex(1);
	instance.getItems().add(itemInstance);
	
	this.itemManager.removeItem(instance, 1);
	
	Assert.assertEquals(instance.getItems().size(), 0);
	
    }
    
}
