package com.novabase.omni.oney.apps.financialdocument.manager;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;

import com.novabase.omni.oney.apps.financialdocument.AppContext;
import com.novabase.omni.oney.apps.financialdocument.agent.blob.BlobAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.dto.FileDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.enums.DocumentClassType;

import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.api.trigger.instance.JourneyInstanceHeader;

public class DocumentManagerTest {
    
    private static final Long DEFAULT_DOCUMENT_ID = 1L;
    private static final String DEFAULT_DOCUMENT_FILE_NAME = "testFile.txt";
    private static final String DEFAULT_DOCUMENT_MIME_TYPE = "text/plain";
    private static final Long DEFAULT_DOCUMENT_FILE_SIZE = 100L;
    private static final Long DEFAULT_HEADER_INSTANCE_ID = 123456789L;
    
    @InjectMocks
    private DocumentManager documentManager;
    
    @Mock
    private BlobAgentImpl blobAgentImpl;
    
    @Mock
    private AppContext ctx;
    
    private FinancialDocumentInstance financialDocumentInstance;
    private InputStream defaultInputStream;
    private FileDTO defaultFileDTO;
    
    @Before 
    public void setUp() {
	
	MockitoAnnotations.initMocks(this);
	
	//  FinancialDocumentInstance
	this.financialDocumentInstance = new FinancialDocumentInstance();
	this.financialDocumentInstance.getFinancialDocumentHeader().setDocumentId(DEFAULT_DOCUMENT_ID);
	this.financialDocumentInstance.getFinancialDocumentHeader().setDocumentFileName(DEFAULT_DOCUMENT_FILE_NAME);
	this.financialDocumentInstance.getFinancialDocumentHeader().setDocumentMimeType(DEFAULT_DOCUMENT_MIME_TYPE);
	this.financialDocumentInstance.setHeader(new JourneyInstanceHeader());
	this.financialDocumentInstance.getHeader().targets = new HashMap<String, String>();
	this.financialDocumentInstance.getHeader().instanceId = DEFAULT_HEADER_INSTANCE_ID;
	
	this.financialDocumentInstance.refreshTargets();
	
	this.defaultInputStream = IOUtils.toInputStream("abc", Charset.defaultCharset());
	
	this.defaultFileDTO = new FileDTO();
	this.defaultFileDTO.setInputStream(this.defaultInputStream);
	this.defaultFileDTO.setMimeType(this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentMimeType());
	this.defaultFileDTO.setFileName(this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentFileName());
	
    }
    
    @Test
    public void saveDocumentSaveTest() throws Exception {

	this.getSaveDocumentMockitoBehavior(this.defaultInputStream, DEFAULT_DOCUMENT_FILE_NAME, new Float(DEFAULT_DOCUMENT_FILE_SIZE)).thenReturn(DEFAULT_DOCUMENT_ID);

	// force use of save/create method
	this.financialDocumentInstance.getFinancialDocumentHeader().setDocumentId(0L);
	
	final Long documentId = this.documentManager.saveDocument(this.financialDocumentInstance, this.defaultInputStream, DocumentClassType.FINANCIAL_DOCUMENT, DEFAULT_DOCUMENT_FILE_SIZE);
	
	Assert.assertTrue(documentId > 0);
	
    }
    
    @Test
    public void saveDocumentRemoveTest() throws Exception {
	
//	force Save.
	this.getSaveDocumentMockitoBehavior(this.defaultInputStream, DEFAULT_DOCUMENT_FILE_NAME, new Float(DEFAULT_DOCUMENT_FILE_SIZE)).thenReturn(DEFAULT_DOCUMENT_ID);
	
	// force remove
	this.documentManager.saveDocument(this.financialDocumentInstance, this.defaultInputStream, DocumentClassType.FINANCIAL_DOCUMENT, DEFAULT_DOCUMENT_FILE_SIZE);
	
	Mockito.verify(this.blobAgentImpl).removeFileFromBlob(this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentId());
	
    }
    
    @Test
    public void getDocumentTest() throws Exception {

	this.getGetDocumentMockitoBehavior(this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentId()).thenReturn(this.defaultInputStream);
	
	final FileDTO fileDTO = this.documentManager.getDocument(this.financialDocumentInstance);
	
	Assert.assertEquals(this.defaultFileDTO.getFileName(), fileDTO.getFileName());
	Assert.assertEquals(this.defaultFileDTO.getMimeType(), fileDTO.getMimeType());
	Assert.assertEquals(this.defaultFileDTO.getInputStream(), fileDTO.getInputStream());
	
    }

//    TODO: Review Exception Testing as thre is no way to mock AppContat.
//    @Test(expected = FinancialDocumentFailedToUploadBlobException.class)
//    public void saveDocumentSaveExceptionTest() throws Exception {
//  
//	this.getSaveDocumentMockitoBehavior().thenThrow(new Exception("The blobAgentImpl Service Returned Error When Saving a new Document..."));
//	// force use of save/create method
//	this.financialDocumentInstance.getFinancialDocumentHeader().setDocumentId(0L);
//	
//	this.documentManager.saveDocument(this.financialDocumentInstance, this.defaultInputStream, DocumentClassType.FINANCIAL_DOCUMENT, DEFAULT_DOCUMENT_FILE_SIZE);
//
//    }
//    
//    @Test(expected = FinancialDocumentFailedToUploadBlobException.class)
//    public void saveDocumentUpdateExceptionTest() throws Exception {
//
//	this.getUpdateDocumentMockitoBehavior().thenThrow(new Exception("The blobAgentImpl Service Returned Error When Updating the Document Info..."));
//	
//	this.documentManager.saveDocument(this.financialDocumentInstance, this.defaultInputStream, DocumentClassType.FINANCIAL_DOCUMENT, DEFAULT_DOCUMENT_FILE_SIZE);
//	
//    }
    
    private <T> OngoingStubbing<Long> getSaveDocumentMockitoBehavior(final InputStream inputStream, final String inputString, final Float inpurFloat) throws Exception {
	return Mockito.when(this.blobAgentImpl.saveBlob(inputStream, inputString, inpurFloat));
    }
    
    private <T> OngoingStubbing<InputStream> getGetDocumentMockitoBehavior(final Long inputLong) throws Exception {
	return Mockito.when(this.blobAgentImpl.readFileFromBlob(inputLong));
    }
    
}
