package com.novabase.omni.oney.apps.financialdocument.validator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;

import com.novabase.omni.oney.apps.financialdocument.agent.referencedata.ReferenceDataAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.dto.ItemDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.CostCenterMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ItemMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.TaxMsDTO;

public class AddItemValidatorTest {

    private static final String DEFAULT_FINANCIAL_DOCUMENT_ITEM_ITEM_CODE = "Item1";
    private static final String DEFAULT_FINANCIAL_DOCUMENT_ITEM_TAX_CODE = "TaxCode1";
    private static final String DEFAULT_FINANCIAL_DOCUMENT_ITEM_COST_CENTER_CODE = "CostCenter1";
    private static final String DEFAULT_FINANCIAL_DOCUMENT_ITEM_DESCRIPTION = "Description1";
    
    @InjectMocks
    private AddItemValidator validator;
    
    @Mock
    private ReferenceDataAgentImpl referenceDataAgentImpl;
    
    @Before
    public void setup() throws Exception {
	
	MockitoAnnotations.initMocks(this);
	
	final List<ItemMsDTO> items = new ArrayList<ItemMsDTO>();
	final ItemMsDTO item = new ItemMsDTO();
	item.code = DEFAULT_FINANCIAL_DOCUMENT_ITEM_ITEM_CODE;
	items.add(item);
	this.getGetItemsMockitoBehavior().thenReturn(items);
	
	final List<TaxMsDTO> taxes = new ArrayList<TaxMsDTO>();
	final TaxMsDTO tax = new TaxMsDTO();
	tax.code = DEFAULT_FINANCIAL_DOCUMENT_ITEM_TAX_CODE;
	taxes.add(tax);
	this.getGetTaxesMockitoBehavior().thenReturn(taxes);
	
	final List<CostCenterMsDTO> costCenters = new ArrayList<CostCenterMsDTO>();
	final CostCenterMsDTO costCenter = new CostCenterMsDTO();
	costCenter.code = DEFAULT_FINANCIAL_DOCUMENT_ITEM_COST_CENTER_CODE;
	costCenters.add(costCenter);
	this.getGetCostCentersMockitoBehavior().thenReturn(costCenters);
	
    }
    
    @Test
    public void getErrorFieldsValidTest() {
	
	ItemDTO itemDTO = new ItemDTO();
	itemDTO.setCode(DEFAULT_FINANCIAL_DOCUMENT_ITEM_ITEM_CODE);
	itemDTO.setDescription(DEFAULT_FINANCIAL_DOCUMENT_ITEM_DESCRIPTION);
	itemDTO.setTaxCode(DEFAULT_FINANCIAL_DOCUMENT_ITEM_TAX_CODE);
	itemDTO.setCostCenterCode(DEFAULT_FINANCIAL_DOCUMENT_ITEM_COST_CENTER_CODE);
	itemDTO.setAmount(1D);
	itemDTO.setUnitPriceWithoutTax(1D);

	List<String> errors = new ArrayList<String>(this.validator.getErrorFields(itemDTO));
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("itemCode")).findAny().orElse(null) == null);
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("description")).findAny().orElse(null) == null);
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("taxCode")).findAny().orElse(null) == null);
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("costcenter")).findAny().orElse(null) == null);
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("amount")).findAny().orElse(null) == null);
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("unitPriceWithoutTax")).findAny().orElse(null) == null);
	
    }
    
    @Test
    public void getErrorFieldsInvalidTest() {
	
	ItemDTO itemDTO = null;
	
	List<String> errors = new ArrayList<String>(this.validator.getErrorFields(itemDTO));
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("item")).findAny().orElse(null) != null);
	
	
	itemDTO = new ItemDTO();

	errors = new ArrayList<String>(this.validator.getErrorFields(itemDTO));
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("itemCode")).findAny().orElse(null) != null);
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("description")).findAny().orElse(null) != null);
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("taxCode")).findAny().orElse(null) != null);
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("costcenter")).findAny().orElse(null) != null);
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("amount")).findAny().orElse(null) != null);
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("unitPriceWithoutTax")).findAny().orElse(null) != null);
	
	
	itemDTO = new ItemDTO();
	itemDTO.setCode("wcec");
	itemDTO.setTaxCode("vwvwev");
	itemDTO.setCostCenterCode("advweve");
	itemDTO.setUnitPriceWithoutTax(0D);
	
	errors = new ArrayList<String>(this.validator.getErrorFields(itemDTO));
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("itemCode")).findAny().orElse(null) != null);
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("description")).findAny().orElse(null) != null);
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("taxCode")).findAny().orElse(null) != null);
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("costcenter")).findAny().orElse(null) != null);
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("amount")).findAny().orElse(null) != null);
	Assert.assertTrue(errors.parallelStream().filter(error -> error.contains("unitPriceWithoutTax")).findAny().orElse(null) != null);
	
    }
    
    private <T> OngoingStubbing<List<ItemMsDTO>> getGetItemsMockitoBehavior() throws Exception {
	return Mockito.when(this.referenceDataAgentImpl.getItems());
    }
    
    private <T> OngoingStubbing<List<TaxMsDTO>> getGetTaxesMockitoBehavior() throws Exception {
	return Mockito.when(this.referenceDataAgentImpl.getTaxes());
    }
    
    private <T> OngoingStubbing<List<CostCenterMsDTO>> getGetCostCentersMockitoBehavior() throws Exception {
	return Mockito.when(this.referenceDataAgentImpl.getCostCenters());
    }
    
}
