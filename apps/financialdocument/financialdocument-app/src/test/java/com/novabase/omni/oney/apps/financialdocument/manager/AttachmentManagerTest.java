package com.novabase.omni.oney.apps.financialdocument.manager;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;

import com.novabase.omni.oney.apps.financialdocument.agent.blob.BlobAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.agent.core.CoreAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.dto.AttachmentDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FileDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.UserInformationDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.enums.DocumentClassType;

import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.api.trigger.instance.JourneyInstanceHeader;

public class AttachmentManagerTest {

    private static final Long DEFAULT_ATTACHMENT_ID = 1L;
    private static final String DEFAULT_ATTACHMENT_FILE_NAME = "testFile.txt";
    private static final String DEFAULT_ATTACHMENT_MIME_TYPE = "text/plain";
    private static final Long DEFAULT_ATTACHMENT_FILE_SIZE = 100L;
    private static final Long DEFAULT_HEADER_INSTANCE_ID = 123456789L;
    private static final Long DEFAULT_USER_ID = 1L;
    private static final String DEFAULT_USER_FIRST_NAME = "first";
    private static final String DEFAULT_USER_LAST_NAME = "last";
    private static final DocumentClassType DEFAULT_DOCUMENT_CLASS_TYPE = DocumentClassType.FINANCIAL_DOCUMENT_ATTACHMENT;
    
    @InjectMocks
    private AttachmentManager attachmentManager;
    
    @Mock
    private BlobAgentImpl blobAgentImpl;

    @Mock
    private CoreAgentImpl coreAgentImpl;

    private FinancialDocumentInstance financialDocumentInstance;
    private InputStream defaultInputStream;
    private FileDTO defaultFileDTO;
    private UserInformationDTO defaultUserInformationDTO;
    
    @Before
    public void setUp () {
	
	MockitoAnnotations.initMocks(this);
	
	//  FinancialDocumentInstance
	this.financialDocumentInstance = new FinancialDocumentInstance();
	this.financialDocumentInstance.setHeader(new JourneyInstanceHeader());
	this.financialDocumentInstance.getHeader().targets = new HashMap<String, String>();
	this.financialDocumentInstance.getHeader().instanceId = DEFAULT_HEADER_INSTANCE_ID;
	
	this.financialDocumentInstance.refreshTargets();
	
	this.defaultInputStream = IOUtils.toInputStream("abc", Charset.defaultCharset());
	
	this.defaultFileDTO = new FileDTO();
	this.defaultFileDTO.setInputStream(this.defaultInputStream);
	this.defaultFileDTO.setMimeType(this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentMimeType());
	this.defaultFileDTO.setFileName(this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentFileName());
	
	this.defaultUserInformationDTO = new UserInformationDTO();
	this.defaultUserInformationDTO.setId(DEFAULT_USER_ID);
	this.defaultUserInformationDTO.setFirstName(DEFAULT_USER_FIRST_NAME);
	this.defaultUserInformationDTO.setLastName(DEFAULT_USER_LAST_NAME);
	
    }
    
    @Test
    public void saveAttachmentTest() throws Exception {
	
	this.getSaveAttachmentMockitoBehavior(this.defaultInputStream, DEFAULT_ATTACHMENT_FILE_NAME, new Float(DEFAULT_ATTACHMENT_FILE_SIZE)).thenReturn(DEFAULT_ATTACHMENT_ID);
	this.getUserInformationsMockitoBehavior(DEFAULT_USER_ID).thenReturn(this.defaultUserInformationDTO);

	this.financialDocumentInstance.setCurrentAction(null);
	Assert.assertNull(this.financialDocumentInstance.getCurrentAction());
	
	final AttachmentDTO resultAttachment = this.attachmentManager.saveAttachment(this.financialDocumentInstance, this.defaultInputStream, DEFAULT_DOCUMENT_CLASS_TYPE, DEFAULT_USER_ID, DEFAULT_ATTACHMENT_MIME_TYPE, DEFAULT_ATTACHMENT_FILE_NAME, DEFAULT_ATTACHMENT_FILE_SIZE);
	
	Assert.assertEquals(resultAttachment.getOwnerId(), DEFAULT_USER_ID);
	Assert.assertEquals(resultAttachment.getOwnerName(), this.defaultUserInformationDTO.getCompleteName());
	Assert.assertEquals(resultAttachment.getName(), DEFAULT_ATTACHMENT_FILE_NAME);
	Assert.assertEquals(resultAttachment.getDocumentClass(), DEFAULT_DOCUMENT_CLASS_TYPE);
	Assert.assertTrue(resultAttachment.isCanBeDeletedByCurrentUser());
	Assert.assertEquals(resultAttachment.getIndex(), 1);
	
	Assert.assertNotNull(this.financialDocumentInstance.getCurrentAction());
	Assert.assertFalse(this.financialDocumentInstance.getCurrentAction().getAttachments().isEmpty());
	Assert.assertEquals(this.financialDocumentInstance.getCurrentAction().getAttachments().get(resultAttachment.getIndex() - 1).getMimeType(), DEFAULT_ATTACHMENT_MIME_TYPE);
	Assert.assertEquals(this.financialDocumentInstance.getCurrentAction().getAttachments().get(resultAttachment.getIndex() - 1).getName(), DEFAULT_ATTACHMENT_FILE_NAME);
	Assert.assertEquals(this.financialDocumentInstance.getCurrentAction().getAttachments().get(resultAttachment.getIndex() - 1).getBlobId(), DEFAULT_ATTACHMENT_ID);
	Assert.assertEquals(this.financialDocumentInstance.getCurrentAction().getAttachments().get(resultAttachment.getIndex() - 1).getDocumentClass(), DEFAULT_DOCUMENT_CLASS_TYPE);
	Assert.assertEquals(this.financialDocumentInstance.getCurrentAction().getAttachments().get(resultAttachment.getIndex() - 1).getIndex(), resultAttachment.getIndex());
	Assert.assertEquals(this.financialDocumentInstance.getCurrentAction().getAttachments().get(resultAttachment.getIndex() - 1).getOwnerId(), DEFAULT_USER_ID);
	
    }
    
    @Test
    public void getAttachmentSingleTest() throws Exception {
	
//	save first attachment
	this.getSaveAttachmentMockitoBehavior(this.defaultInputStream, DEFAULT_ATTACHMENT_FILE_NAME, new Float(DEFAULT_ATTACHMENT_FILE_SIZE)).thenReturn(DEFAULT_ATTACHMENT_ID);
	this.getUserInformationsMockitoBehavior(DEFAULT_USER_ID).thenReturn(this.defaultUserInformationDTO);
	
	this.attachmentManager.saveAttachment(this.financialDocumentInstance, this.defaultInputStream, DEFAULT_DOCUMENT_CLASS_TYPE, DEFAULT_USER_ID, DEFAULT_ATTACHMENT_MIME_TYPE, DEFAULT_ATTACHMENT_FILE_NAME, DEFAULT_ATTACHMENT_FILE_SIZE);

//	first Get Attachment
	this.getReadFileFromBlobMockitoBehavior(1).thenReturn(this.defaultInputStream);
	final FileDTO result1 = this.attachmentManager.getAttachment(this.financialDocumentInstance, 1);
	
//	check first attachment info
	Assert.assertEquals(result1.getFileName(), DEFAULT_ATTACHMENT_FILE_NAME);
	Assert.assertEquals(result1.getMimeType(), DEFAULT_ATTACHMENT_MIME_TYPE);
	Assert.assertEquals(result1.getInputStream(), this.defaultInputStream);
	
    }
    
    @Test
    public void getAttachmentPluralTest() throws Exception {
	
//	save first attachment
	this.getSaveAttachmentMockitoBehavior(this.defaultInputStream, DEFAULT_ATTACHMENT_FILE_NAME, new Float(DEFAULT_ATTACHMENT_FILE_SIZE)).thenReturn(DEFAULT_ATTACHMENT_ID);
	this.getUserInformationsMockitoBehavior(DEFAULT_USER_ID).thenReturn(this.defaultUserInformationDTO);
	
	this.attachmentManager.saveAttachment(this.financialDocumentInstance, this.defaultInputStream, DEFAULT_DOCUMENT_CLASS_TYPE, DEFAULT_USER_ID, DEFAULT_ATTACHMENT_MIME_TYPE, DEFAULT_ATTACHMENT_FILE_NAME, DEFAULT_ATTACHMENT_FILE_SIZE);
	
//	save second attachment	
	final InputStream inputStream2 = IOUtils.toInputStream("def", Charset.defaultCharset());
	final String attachment2FileName = DEFAULT_ATTACHMENT_FILE_NAME + "1";
	final Long attachment2FileSizeLong = DEFAULT_ATTACHMENT_FILE_SIZE + 1L;
	final Float attachment2FileSize = new Float(attachment2FileSizeLong); 
	final Long attahcment2BlobId = DEFAULT_ATTACHMENT_ID + 1;
	final Long attachment2UserId = DEFAULT_USER_ID + 1;
	this.getSaveAttachmentMockitoBehavior(inputStream2, attachment2FileName, attachment2FileSize).thenReturn(attahcment2BlobId);
	
	final UserInformationDTO userInfo2 = new UserInformationDTO();
	userInfo2.setId(DEFAULT_USER_ID + 1);
	userInfo2.setFirstName(DEFAULT_USER_FIRST_NAME + "1");
	userInfo2.setLastName(DEFAULT_USER_LAST_NAME + "1");
	this.getUserInformationsMockitoBehavior(attachment2UserId).thenReturn(userInfo2);
	this.attachmentManager.saveAttachment(this.financialDocumentInstance, inputStream2, DEFAULT_DOCUMENT_CLASS_TYPE, attachment2UserId, DEFAULT_ATTACHMENT_MIME_TYPE, attachment2FileName, attachment2FileSizeLong);
	
//	first Get Attachment
	this.getReadFileFromBlobMockitoBehavior(1).thenReturn(this.defaultInputStream);
	final FileDTO result1 = this.attachmentManager.getAttachment(this.financialDocumentInstance, 1);
	
//	second Get Attachment
	this.getReadFileFromBlobMockitoBehavior(2).thenReturn(inputStream2);
	final FileDTO result2 = this.attachmentManager.getAttachment(this.financialDocumentInstance, 2);
	
	Assert.assertNotEquals(result1, result2);
	
//	check first attachment info
	Assert.assertEquals(result1.getFileName(), DEFAULT_ATTACHMENT_FILE_NAME);
	Assert.assertEquals(result1.getMimeType(), DEFAULT_ATTACHMENT_MIME_TYPE);
	Assert.assertEquals(result1.getInputStream(), this.defaultInputStream);
	 
//	check seconf attachment info
	Assert.assertEquals(result2.getFileName(), attachment2FileName);
	Assert.assertEquals(result2.getMimeType(), DEFAULT_ATTACHMENT_MIME_TYPE);
	Assert.assertEquals(result2.getInputStream(), inputStream2);
	
    }
    
    @Test
    public void removeAttachmentTest() throws Exception {
	
	Assert.assertTrue(this.financialDocumentInstance.getCurrentAction().getAttachments().isEmpty());
	
//	save first attachment
	this.getSaveAttachmentMockitoBehavior(this.defaultInputStream, DEFAULT_ATTACHMENT_FILE_NAME, new Float(DEFAULT_ATTACHMENT_FILE_SIZE)).thenReturn(DEFAULT_ATTACHMENT_ID);
	this.getUserInformationsMockitoBehavior(DEFAULT_USER_ID).thenReturn(this.defaultUserInformationDTO);
	
	this.attachmentManager.saveAttachment(this.financialDocumentInstance, this.defaultInputStream, DEFAULT_DOCUMENT_CLASS_TYPE, DEFAULT_USER_ID, DEFAULT_ATTACHMENT_MIME_TYPE, DEFAULT_ATTACHMENT_FILE_NAME, DEFAULT_ATTACHMENT_FILE_SIZE);

	Assert.assertFalse(this.financialDocumentInstance.getCurrentAction().getAttachments().isEmpty());
	Assert.assertEquals(this.financialDocumentInstance.getCurrentAction().getAttachments().get(0).getIndex(), 1);
	
	
	this.attachmentManager.removeAttachment(this.financialDocumentInstance, 1, DEFAULT_USER_ID);
	
	Assert.assertTrue(this.financialDocumentInstance.getCurrentAction().getAttachments().isEmpty());
	
	Mockito.verify(this.blobAgentImpl).removeFileFromBlob(DEFAULT_ATTACHMENT_ID);
	
    }
    
    private <T> OngoingStubbing<Long> getSaveAttachmentMockitoBehavior(final InputStream inputStream, final String inputString, final Float inputFloat) throws Exception {
	return Mockito.when(this.blobAgentImpl.saveBlob(inputStream, inputString, inputFloat));
    }
    
    private <T> OngoingStubbing<UserInformationDTO> getUserInformationsMockitoBehavior(final long inputLong) throws Exception {
	return Mockito.when(this.coreAgentImpl.getUserInformations(inputLong));
    }
    
    private <T> OngoingStubbing<InputStream> getReadFileFromBlobMockitoBehavior(final long inputLong) throws Exception {
	return Mockito.when(this.blobAgentImpl.readFileFromBlob(inputLong));
    }
    
}
