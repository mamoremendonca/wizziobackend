package com.novabase.omni.oney.apps.financialdocument.mapper;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.novabase.omni.oney.apps.financialdocument.dto.AttachmentDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentHeaderDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.ItemDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder.PurchaseOrderDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.Attachment;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.entity.Item;
import com.novabase.omni.oney.apps.financialdocument.enums.DocumentClassType;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentType;
import com.novabase.omni.oney.apps.financialdocument.enums.WorkFlowActionType;
import com.novabase.omni.oney.apps.financialdocument.enums.purchaseorder.PurchaseOrderType;

import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.api.trigger.instance.JourneyInstanceHeader;

@RunWith(PowerMockRunner.class)
@PrepareForTest(LocalDateTime.class)
public class FinancialDocumentInstanceMapperTest {

    private static final Long DEFAULT_USER_ID = 123L;
    private static final String DEFAULT_USER_GROUP = "UserGroup1";
    private static final List<WorkFlowActionType> INSTANCE_WORKFLOW_LIST_ACTIONS = Arrays.asList(WorkFlowActionType.CANCEL, WorkFlowActionType.APPROVE, WorkFlowActionType.REJECT, WorkFlowActionType.RETURN);
    private static final String FINANCIAL_DOCUMENT_NUMBER = "234";
    private static final Long FINANCIAL_DOCUMENT_DOCUMENT_ID = 345L;
    private static final String FINANCIAL_DOCUMENT_DESCRIPTION = "Decription2";
    private static final String FINANCIAL_DOCUMENT_DOCUMENT_MIME_TYPE = ".pdf";
    private static final String FINANCIAL_DOCUMENT_DOCUMENT_FILE_NAME = "DFDocument1";
    private static final String SUPPLIER_CODE = "SupplierCode1";
    
    private static final Long DEFAULT_ATTACHMENT_ID = 1L;
    private static final String DEFAULT_ATTACHMENT_FILE_NAME = "testFile.txt";
    private static final String DEFAULT_ATTACHMENT_MIME_TYPE = "text/plain";
    private static final DocumentClassType DEFAULT_DOCUMENT_CLASS_TYPE = DocumentClassType.FINANCIAL_DOCUMENT;
    
    private static final String DEFAULT_ASSOCIATE_PURCHASE_ORDER_DEPARTMENT_CODE = "DR-6400";
    private static final String DEFAULT_ASSOCIATE_PURCHASE_ORDER_INTERNAL_ORDER_CODE = "InternalOrder1";
    private static final String DEFAULT_ASSOCIATE_PURCHASE_ORDER_PROJECT_CODE = "CostCenter1";
    private static final String DEFAULT_ASSOCIATE_PURCHASE_ORDER_DESCRIPTION = "Description1";
    private static final Long DEFAULT_ASSOCIATE_PURCHASE_ORDER_NUMBER = 1L;
    private static final Double DEFAULT_ASSOCIATE_PURCHASE_ORDER_TAX = 1D;
    private static final Long DEFAULT_ASSOCIATE_PURCHASE_ORDER_ID= 1L;
    private static final String DEFAULT_ASSOCIATE_PURCHASE_ORDER_FRIENDY_NUMBER = "PO0000" + DEFAULT_ASSOCIATE_PURCHASE_ORDER_NUMBER;
    private static final PurchaseOrderType DEFAULT_ASSOCIATE_PURCHASE_ORDER_TYPE = PurchaseOrderType.CAPEX;
    
    private static final String DEFAULT_FINANCIAL_DOCUMENT_ITEM_ITEM_CODE = "Item1";
    private static final String DEFAULT_FINANCIAL_DOCUMENT_ITEM_TAX_CODE = "TaxCode1";
    private static final String DEFAULT_FINANCIAL_DOCUMENT_ITEM_COST_CENTER_CODE = "CostCenter1";
    private static final String DEFAULT_FINANCIAL_DOCUMENT_ITEM_DESCRIPTION = "Description1";
    
    private FinancialDocumentInstance financialDocumentInstance;
    private FinancialDocumentHeaderDTO financialDocumentHeader;
    private Attachment defaultAttachment;
    private com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder.PurchaseOrderDTO defaultAssociatePurchaseOrder;
    private ItemDTO defaultItemDTO;
        
    @Before
    public void setUp() {

	final LocalDateTime now = LocalDateTime.now();
	PowerMockito.mockStatic(LocalDateTime.class);
	
	this.getLocalDatetimeNowMockitoBehavior().thenReturn(now);
	
	//  FinancialDocumentInstance
	this.financialDocumentInstance = new FinancialDocumentInstance();
	this.financialDocumentInstance.getFinancialDocumentHeader().setUserId(DEFAULT_USER_ID);
	this.financialDocumentInstance.getFinancialDocumentHeader().setUserGroup(DEFAULT_USER_GROUP);
	this.financialDocumentInstance.getWorkFlowAvailableActions().addAll(INSTANCE_WORKFLOW_LIST_ACTIONS);
	this.financialDocumentInstance.getFinancialDocumentHeader().setGranting(Boolean.TRUE);
	this.financialDocumentInstance.setHeader(new JourneyInstanceHeader());
	this.financialDocumentInstance.getHeader().targets = new HashMap<String, String>();
	this.financialDocumentInstance.refreshTargets();
	
	// Items
	final Item item1 = new Item();
	item1.setIndex(1);
	item1.setCode("Asset1");
	item1.setItemPurchaseOrderId(1L);
	item1.setDescription("Asset 1");
	item1.setAmount(1D);
	item1.setUnitPriceWithoutTax(1D);
	item1.setTaxCode("TaxCode1");
	item1.setStartDate(null);
	item1.setEndDate(null);
	item1.setComment("1");
	this.financialDocumentInstance.getItems().add(item1);
	
	final Item item2 = new Item();
	item2.setIndex(2);
	item2.setCode("Asset2");
	item2.setItemPurchaseOrderId(1L);
	item2.setDescription("Asset 2");
	item2.setAmount(2D);
	item2.setUnitPriceWithoutTax(2D);
	item2.setTaxCode("TaxCode2");
	item2.setStartDate(null);
	item2.setEndDate(null);
	item2.setComment("2");
	this.financialDocumentInstance.getItems().add(item2);
	
	final Item item3 = new Item();
	item3.setIndex(3);
	item3.setCode("Asset3");
	item3.setItemPurchaseOrderId(1L);
	item3.setDescription("Asset 3");
	item3.setAmount(3D);
	item3.setUnitPriceWithoutTax(3D);
	item3.setTaxCode("TaxCode1");
	item3.setStartDate(null);
	item3.setEndDate(null);
	item3.setComment("3");
	this.financialDocumentInstance.getItems().add(item3);
	
	//  FinancialDocumentHeaderDTO
	this.financialDocumentHeader = new FinancialDocumentHeaderDTO();
	this.financialDocumentHeader.setNumber(FINANCIAL_DOCUMENT_NUMBER);
	this.financialDocumentHeader.setType(FinancialDocumentType.INVOICE);
	this.financialDocumentHeader.setSupplierCode(SUPPLIER_CODE);
	this.financialDocumentHeader.setEmissionDate(now);
	this.financialDocumentHeader.setDocumentId(FINANCIAL_DOCUMENT_DOCUMENT_ID);
	this.financialDocumentHeader.setDescription(FINANCIAL_DOCUMENT_DESCRIPTION);
	this.financialDocumentHeader.setDocumentFileName(FINANCIAL_DOCUMENT_DOCUMENT_FILE_NAME);
	this.financialDocumentHeader.setDocumentMimeType(FINANCIAL_DOCUMENT_DOCUMENT_MIME_TYPE);
	this.financialDocumentHeader.setPurchaseOrderId(1L);
	this.financialDocumentHeader.setPurchaseOrderFriendlyNumber("PO00001");
	this.financialDocumentHeader.setExpirationDate(now);
	this.financialDocumentHeader.setGranting(Boolean.TRUE);
	this.financialDocumentHeader.setAssociatedDocument("DF00001");
	this.financialDocumentHeader.setPurchaseOrderType(PurchaseOrderType.CAPEX);
	this.financialDocumentHeader.setDepartmentCode(DEFAULT_ASSOCIATE_PURCHASE_ORDER_DEPARTMENT_CODE);
	this.financialDocumentHeader.setProjectCode(DEFAULT_ASSOCIATE_PURCHASE_ORDER_PROJECT_CODE);
	this.financialDocumentHeader.setInternalOrderCode(DEFAULT_ASSOCIATE_PURCHASE_ORDER_INTERNAL_ORDER_CODE);
	
	// Attachments
	this.defaultAttachment = new Attachment();
	this.defaultAttachment.setMimeType(DEFAULT_ATTACHMENT_MIME_TYPE);
	this.defaultAttachment.setOwnerId(DEFAULT_USER_ID);
	this.defaultAttachment.setName(DEFAULT_ATTACHMENT_FILE_NAME);
	this.defaultAttachment.setDocumentClass(DEFAULT_DOCUMENT_CLASS_TYPE);
	this.defaultAttachment.setBlobId(DEFAULT_ATTACHMENT_ID);
	this.defaultAttachment.setIndex(1);
	this.financialDocumentInstance.getAttachments().add(this.defaultAttachment);
	 
	// Associate Purchase Order
	this.defaultAssociatePurchaseOrder = new PurchaseOrderDTO();
	this.defaultAssociatePurchaseOrder.setId(DEFAULT_ASSOCIATE_PURCHASE_ORDER_ID);
	this.defaultAssociatePurchaseOrder.setNumber(DEFAULT_ASSOCIATE_PURCHASE_ORDER_NUMBER);
	this.defaultAssociatePurchaseOrder.setFriendlyNumber(DEFAULT_ASSOCIATE_PURCHASE_ORDER_FRIENDY_NUMBER);
	this.defaultAssociatePurchaseOrder.setType(DEFAULT_ASSOCIATE_PURCHASE_ORDER_TYPE);
	this.defaultAssociatePurchaseOrder.setDepartmentCode(DEFAULT_ASSOCIATE_PURCHASE_ORDER_DEPARTMENT_CODE);
	this.defaultAssociatePurchaseOrder.setDescription(DEFAULT_ASSOCIATE_PURCHASE_ORDER_DESCRIPTION);
	this.defaultAssociatePurchaseOrder.setTotalWithoutTax(DEFAULT_ASSOCIATE_PURCHASE_ORDER_TAX);
	
	final com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder.ItemDTO itemPO1 = new com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder.ItemDTO();
	itemPO1.setIndex(1);
	itemPO1.setCode("Asset1");
	itemPO1.setDescription("Asset 1");
	itemPO1.setAmount(1D);
	itemPO1.setUnitPriceWithoutTax(1D);
	itemPO1.setTaxCode("TaxCode1");
	itemPO1.setStartDate(null);
	itemPO1.setEndDate(null);
	itemPO1.setComment("1");
	this.defaultAssociatePurchaseOrder.getItems().add(itemPO1);
	
	final com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder.ItemDTO itemPO2 = new com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder.ItemDTO();
	itemPO2.setIndex(2);
	itemPO2.setCode("Asset2");
	itemPO2.setDescription("Asset 2");
	itemPO2.setAmount(2D);
	itemPO2.setUnitPriceWithoutTax(2D);
	itemPO2.setTaxCode("TaxCode2");
	itemPO2.setStartDate(null);
	itemPO2.setEndDate(null);
	itemPO2.setComment("2");
	this.defaultAssociatePurchaseOrder.getItems().add(itemPO2);
	
	final com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder.ItemDTO itemPO3 = new com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder.ItemDTO();
	itemPO3.setIndex(3);
	itemPO3.setCode("Asset3");
	itemPO3.setDescription("Asset 3");
	itemPO3.setAmount(3D);
	itemPO3.setUnitPriceWithoutTax(3D);
	itemPO3.setTaxCode("TaxCode1");
	itemPO3.setStartDate(null);
	itemPO3.setEndDate(null);
	itemPO3.setComment("3");
	this.defaultAssociatePurchaseOrder.getItems().add(itemPO3);
	
	// ITEM DTO 
	this.defaultItemDTO = new ItemDTO();
	this.defaultItemDTO.setCode(DEFAULT_FINANCIAL_DOCUMENT_ITEM_ITEM_CODE);
	this.defaultItemDTO.setDescription(DEFAULT_FINANCIAL_DOCUMENT_ITEM_DESCRIPTION);
	this.defaultItemDTO.setTaxCode(DEFAULT_FINANCIAL_DOCUMENT_ITEM_TAX_CODE);
	this.defaultItemDTO.setCostCenterCode(DEFAULT_FINANCIAL_DOCUMENT_ITEM_COST_CENTER_CODE);
	this.defaultItemDTO.setAmount(1D);
	this.defaultItemDTO.setUnitPriceWithoutTax(1D);
	this.defaultItemDTO.setStartDate(LocalDateTime.now());
	this.defaultItemDTO.setEndDate(LocalDateTime.now().plusDays(2L));
	
    }
    
    @Test
    public void toProcessDTOTest() {

	Assert.assertNull(FinancialDocumentInstanceMapper.INSTANCE.toProcessDTO(null));
	
	final FinancialDocumentDTO mappedFinancialDocumentDTO = FinancialDocumentInstanceMapper.INSTANCE.toProcessDTO(this.financialDocumentInstance);
	
	Assert.assertEquals(Long.parseLong(mappedFinancialDocumentDTO.getFinancialDocumentHeader().getUserId()), this.financialDocumentInstance.getFinancialDocumentHeader().getUserId().longValue());
	Assert.assertEquals(mappedFinancialDocumentDTO.getWorkFlowAvailableActions(), this.financialDocumentInstance.getWorkFlowAvailableActions());
	Assert.assertFalse(mappedFinancialDocumentDTO.getItems().isEmpty());
	Assert.assertEquals(mappedFinancialDocumentDTO.getItems().size(), this.financialDocumentInstance.getItems().size());
	Assert.assertEquals(mappedFinancialDocumentDTO.getFinancialDocumentHeader().isGranting(), this.financialDocumentInstance.getFinancialDocumentHeader().isGranting());
	
	ItemDTO itemDTO = mappedFinancialDocumentDTO.getItems().get(0); 
	Item item = this.financialDocumentInstance.getItems().get(0);
	Assert.assertEquals(itemDTO.getIndex(), item.getIndex());
	Assert.assertEquals(itemDTO.getCode(), item.getCode());
	Assert.assertEquals(itemDTO.getItemPurchaseOrderId().longValue(), item.getItemPurchaseOrderId().longValue());
	Assert.assertEquals(itemDTO.getDescription(), item.getDescription());
	Assert.assertEquals(itemDTO.getAmount(), item.getAmount());
	Assert.assertEquals(itemDTO.getUnitPriceWithoutTax(), item.getUnitPriceWithoutTax());
	Assert.assertEquals(itemDTO.getTaxCode(), item.getTaxCode());
	Assert.assertEquals(itemDTO.getStartDate(), item.getStartDate());
	Assert.assertEquals(itemDTO.getEndDate(), item.getEndDate());
	Assert.assertEquals(itemDTO.getComment(), item.getComment());

	itemDTO = mappedFinancialDocumentDTO.getItems().get(1); 
	item = this.financialDocumentInstance.getItems().get(1);
	Assert.assertEquals(itemDTO.getIndex(), item.getIndex());
	Assert.assertEquals(itemDTO.getCode(), item.getCode());
	Assert.assertEquals(itemDTO.getItemPurchaseOrderId().longValue(), item.getItemPurchaseOrderId().longValue());
	Assert.assertEquals(itemDTO.getDescription(), item.getDescription());
	Assert.assertEquals(itemDTO.getAmount(), item.getAmount());
	Assert.assertEquals(itemDTO.getUnitPriceWithoutTax(), item.getUnitPriceWithoutTax());
	Assert.assertEquals(itemDTO.getTaxCode(), item.getTaxCode());
	Assert.assertEquals(itemDTO.getStartDate(), item.getStartDate());
	Assert.assertEquals(itemDTO.getEndDate(), item.getEndDate());
	Assert.assertEquals(itemDTO.getComment(), item.getComment());
	
	itemDTO = mappedFinancialDocumentDTO.getItems().get(2); 
	item = this.financialDocumentInstance.getItems().get(2);
	Assert.assertEquals(itemDTO.getIndex(), item.getIndex());
	Assert.assertEquals(itemDTO.getCode(), item.getCode());
	Assert.assertEquals(itemDTO.getItemPurchaseOrderId().longValue(), item.getItemPurchaseOrderId().longValue());
	Assert.assertEquals(itemDTO.getDescription(), item.getDescription());
	Assert.assertEquals(itemDTO.getAmount(), item.getAmount());
	Assert.assertEquals(itemDTO.getUnitPriceWithoutTax(), item.getUnitPriceWithoutTax());
	Assert.assertEquals(itemDTO.getTaxCode(), item.getTaxCode());
	Assert.assertEquals(itemDTO.getStartDate(), item.getStartDate());
	Assert.assertEquals(itemDTO.getEndDate(), item.getEndDate());
	Assert.assertEquals(itemDTO.getComment(), item.getComment());
	
    }
    
    @Test
    public void toFinancialDocumentHeaderDTOTest() {
	
	Assert.assertNull(FinancialDocumentInstanceMapper.INSTANCE.toFinancialDocumentHeaderDTO(null));
	
	final FinancialDocumentHeaderDTO mappedtoFinancialDocumentHeaderDTO = FinancialDocumentInstanceMapper.INSTANCE.toFinancialDocumentHeaderDTO(this.financialDocumentInstance.getFinancialDocumentHeader());
	
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getUserGroup(), this.financialDocumentInstance.getFinancialDocumentHeader().getUserGroup());
	Assert.assertEquals(Long.parseLong(mappedtoFinancialDocumentHeaderDTO.getUserId()), this.financialDocumentInstance.getFinancialDocumentHeader().getUserId().longValue());

	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getRepositoryId(), this.financialDocumentInstance.getFinancialDocumentHeader().getRepositoryId());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getAssociatedDocument(), this.financialDocumentInstance.getFinancialDocumentHeader().getAssociatedDocument());
	Assert.assertNull(mappedtoFinancialDocumentHeaderDTO.getCurrentUserGroup());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getNumber(), this.financialDocumentInstance.getFinancialDocumentHeader().getNumber());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getType(), this.financialDocumentInstance.getFinancialDocumentHeader().getType());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getSupplierCode(), this.financialDocumentInstance.getFinancialDocumentHeader().getSupplierCode());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getDescription(), this.financialDocumentInstance.getFinancialDocumentHeader().getDescription());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getDepartmentCode(), this.financialDocumentInstance.getFinancialDocumentHeader().getDepartmentCode());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getProjectCode(), this.financialDocumentInstance.getFinancialDocumentHeader().getProjectCode());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getInternalOrderCode(), this.financialDocumentInstance.getFinancialDocumentHeader().getInternalOrderCode());	
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getDocumentId(), this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentId());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getDocumentMimeType(), this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentMimeType());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getDocumentFileName(), this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentFileName());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getPurchaseOrderFriendlyNumber(), this.financialDocumentInstance.getFinancialDocumentHeader().getPurchaseOrderFriendlyNumber());
	Assert.assertNull(mappedtoFinancialDocumentHeaderDTO.getFinishDate());
	Assert.assertNotNull(mappedtoFinancialDocumentHeaderDTO.getCreationDate());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getEmissionDate(), this.financialDocumentInstance.getFinancialDocumentHeader().getEmissionDate());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getExpirationDate(), this.financialDocumentInstance.getFinancialDocumentHeader().getExpirationDate());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getPurchaseOrderType(), this.financialDocumentInstance.getFinancialDocumentHeader().getPurchaseOrderType());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getRepositoryId().longValue(), this.financialDocumentInstance.getFinancialDocumentHeader().getRepositoryId().longValue());
	Assert.assertNull(mappedtoFinancialDocumentHeaderDTO.getRepositoryStatus());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.getTotalWithoutTax(), this.financialDocumentInstance.getFinancialDocumentHeader().getTotalWithoutTax());
	Assert.assertEquals(mappedtoFinancialDocumentHeaderDTO.isGranting(), this.financialDocumentInstance.getFinancialDocumentHeader().isGranting());

    }
    
    @Test
    public void mergeFinancialDocumentHeaderDTODraftTest() {
	
	FinancialDocumentInstanceMapper.INSTANCE.mergeFinancialDocumentHeaderDTODraft(this.financialDocumentInstance, this.financialDocumentHeader);
	
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getNumber(), this.financialDocumentHeader.getNumber());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getType(), this.financialDocumentHeader.getType());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getSupplierCode(), this.financialDocumentHeader.getSupplierCode());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getEmissionDate(), this.financialDocumentHeader.getEmissionDate());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentId(), this.financialDocumentHeader.getDocumentId());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentMimeType(), this.financialDocumentHeader.getDocumentMimeType());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentFileName(), this.financialDocumentHeader.getDocumentFileName());
	
    }
    
    @Test
    public void mergeFinancialDocumentHeaderDTOCreationTest() {
	
	FinancialDocumentInstanceMapper.INSTANCE.mergeFinancialDocumentHeaderDTOCreation(this.financialDocumentInstance, this.financialDocumentHeader);
	
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().isGranting(), this.financialDocumentHeader.isGranting());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getDescription(), this.financialDocumentHeader.getDescription());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getDepartmentCode(), this.financialDocumentHeader.getDepartmentCode());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getProjectCode(), this.financialDocumentHeader.getProjectCode());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getInternalOrderCode(), this.financialDocumentHeader.getInternalOrderCode());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getExpirationDate(), this.financialDocumentHeader.getExpirationDate());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getPurchaseOrderId().longValue(), this.financialDocumentHeader.getPurchaseOrderId().longValue());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getPurchaseOrderFriendlyNumber(), this.financialDocumentHeader.getPurchaseOrderFriendlyNumber());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getPurchaseOrderType(), this.financialDocumentHeader.getPurchaseOrderType());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentId(), this.financialDocumentHeader.getDocumentId());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentMimeType(), this.financialDocumentHeader.getDocumentMimeType());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentFileName(), this.financialDocumentHeader.getDocumentFileName());
	
    }
    
    @Test
    public void mergeFinancialDocumentHeaderDTOCreationPOTest() {
	
	FinancialDocumentInstanceMapper.INSTANCE.mergeFinancialDocumentHeaderDTOCreationPO(this.financialDocumentInstance, this.financialDocumentHeader);
	
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().isGranting(), this.financialDocumentHeader.isGranting());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getDescription(), this.financialDocumentHeader.getDescription());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getExpirationDate(), this.financialDocumentHeader.getExpirationDate());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getDepartmentCode(), this.financialDocumentHeader.getDepartmentCode());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getProjectCode(), this.financialDocumentHeader.getProjectCode());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getInternalOrderCode(), this.financialDocumentHeader.getInternalOrderCode());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getPurchaseOrderId().longValue(), this.financialDocumentHeader.getPurchaseOrderId().longValue());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getPurchaseOrderFriendlyNumber(), this.financialDocumentHeader.getPurchaseOrderFriendlyNumber());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getPurchaseOrderType(), this.financialDocumentHeader.getPurchaseOrderType());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentId(), this.financialDocumentHeader.getDocumentId());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentMimeType(), this.financialDocumentHeader.getDocumentMimeType());
	Assert.assertNotEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getDocumentFileName(), this.financialDocumentHeader.getDocumentFileName());
	
    }
    
    @Test
    public void toAttachmentDTOTest() {
	
	final AttachmentDTO resultAttachament = FinancialDocumentInstanceMapper.INSTANCE.toAttachmentDTO(this.defaultAttachment);
	
	Assert.assertEquals(resultAttachament.getName(), DEFAULT_ATTACHMENT_FILE_NAME);
	Assert.assertEquals(resultAttachament.getDocumentClass(), DEFAULT_DOCUMENT_CLASS_TYPE);
	Assert.assertEquals(resultAttachament.getIndex(), 1);
	Assert.assertEquals(resultAttachament.getOwnerId(), DEFAULT_USER_ID);
	
    }
    
    @Test
    public void mergeFinancialDocumentInstanceTest() {
	
	this.financialDocumentInstance.getItems().clear();
	FinancialDocumentInstanceMapper.INSTANCE.mergeFinancialDocumentInstance(this.financialDocumentInstance, this.defaultAssociatePurchaseOrder);
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getPurchaseOrderId().longValue(), this.financialDocumentHeader.getPurchaseOrderId().longValue());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getPurchaseOrderFriendlyNumber(), this.defaultAssociatePurchaseOrder.getFriendlyNumber());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getDescription(), this.defaultAssociatePurchaseOrder.getDescription());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getPurchaseOrderType().name(), this.defaultAssociatePurchaseOrder.getType().name());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getDepartmentCode(), this.defaultAssociatePurchaseOrder.getDepartmentCode());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getProjectCode(), this.defaultAssociatePurchaseOrder.getProjectCode());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getInternalOrderCode(), this.defaultAssociatePurchaseOrder.getInternalOrderCode());
	Assert.assertEquals(this.financialDocumentInstance.getFinancialDocumentHeader().getTotalWithoutTax(), this.defaultAssociatePurchaseOrder.getTotalWithoutTax());
	
	com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder.ItemDTO itemDTO = this.defaultAssociatePurchaseOrder.getItems().get(0); 
	Item item = this.financialDocumentInstance.getItems().get(0);
	Assert.assertEquals(itemDTO.getIndex(), item.getIndex());
	Assert.assertEquals(itemDTO.getCode(), item.getCode());

	Assert.assertEquals(itemDTO.getDescription(), item.getDescription());
	Assert.assertEquals(itemDTO.getAmount(), item.getAmount());
	Assert.assertEquals(itemDTO.getUnitPriceWithoutTax(), item.getUnitPriceWithoutTax());
	Assert.assertEquals(itemDTO.getTaxCode(), item.getTaxCode());
	Assert.assertEquals(itemDTO.getStartDate(), item.getStartDate());
	Assert.assertEquals(itemDTO.getEndDate(), item.getEndDate());
	Assert.assertEquals(itemDTO.getComment(), item.getComment());

	itemDTO = this.defaultAssociatePurchaseOrder.getItems().get(1); 
	item = this.financialDocumentInstance.getItems().get(1);
	Assert.assertEquals(itemDTO.getIndex(), item.getIndex());
	Assert.assertEquals(itemDTO.getCode(), item.getCode());
	Assert.assertEquals(itemDTO.getDescription(), item.getDescription());
	Assert.assertEquals(itemDTO.getAmount(), item.getAmount());
	Assert.assertEquals(itemDTO.getUnitPriceWithoutTax(), item.getUnitPriceWithoutTax());
	Assert.assertEquals(itemDTO.getTaxCode(), item.getTaxCode());
	Assert.assertEquals(itemDTO.getStartDate(), item.getStartDate());
	Assert.assertEquals(itemDTO.getEndDate(), item.getEndDate());
	Assert.assertEquals(itemDTO.getComment(), item.getComment());
	
	itemDTO = this.defaultAssociatePurchaseOrder.getItems().get(2); 
	item = this.financialDocumentInstance.getItems().get(2);
	Assert.assertEquals(itemDTO.getIndex(), item.getIndex());
	Assert.assertEquals(itemDTO.getCode(), item.getCode());
	Assert.assertEquals(itemDTO.getDescription(), item.getDescription());
	Assert.assertEquals(itemDTO.getAmount(), item.getAmount());
	Assert.assertEquals(itemDTO.getUnitPriceWithoutTax(), item.getUnitPriceWithoutTax());
	Assert.assertEquals(itemDTO.getTaxCode(), item.getTaxCode());
	Assert.assertEquals(itemDTO.getStartDate(), item.getStartDate());
	Assert.assertEquals(itemDTO.getEndDate(), item.getEndDate());
	Assert.assertEquals(itemDTO.getComment(), item.getComment());
	
    }
    
    @Test
    public void toItemTest() {
	
	final Item item = FinancialDocumentInstanceMapper.INSTANCE.toItem(this.defaultItemDTO);
	Assert.assertEquals(item.getCode(), this.defaultItemDTO.getCode());
	Assert.assertEquals(item.getDescription(), this.defaultItemDTO.getDescription());
	Assert.assertEquals(item.getTaxCode(), this.defaultItemDTO.getTaxCode());
	Assert.assertEquals(item.getCostCenterCode(), this.defaultItemDTO.getCostCenterCode());
	Assert.assertEquals(item.getAmount(), this.defaultItemDTO.getAmount());
	Assert.assertEquals(item.getUnitPriceWithoutTax(), this.defaultItemDTO.getUnitPriceWithoutTax());
	Assert.assertEquals(item.getStartDate(), this.defaultItemDTO.getStartDate());
	Assert.assertEquals(item.getEndDate(), this.defaultItemDTO.getEndDate());
	
    }
    
    @Test
    public void toItemDTOTest() {
	
	final Item item = FinancialDocumentInstanceMapper.INSTANCE.toItem(this.defaultItemDTO);
	final ItemDTO itemDTO = FinancialDocumentInstanceMapper.INSTANCE.toItemDTO(item);
	
	Assert.assertEquals(itemDTO.getCode(), item.getCode());
	Assert.assertEquals(itemDTO.getDescription(), item.getDescription());
	Assert.assertEquals(itemDTO.getTaxCode(), item.getTaxCode());
	Assert.assertEquals(itemDTO.getCostCenterCode(), item.getCostCenterCode());
	Assert.assertEquals(itemDTO.getAmount(), item.getAmount());
	Assert.assertEquals(itemDTO.getUnitPriceWithoutTax(), item.getUnitPriceWithoutTax());
	Assert.assertEquals(itemDTO.getStartDate(), item.getStartDate());
	Assert.assertEquals(itemDTO.getEndDate(), item.getEndDate());
	
    }
    
    @Test
    public void mergeItemDTOTest() {
	
	final Item item = new Item();
	item.setIndex(45);
	FinancialDocumentInstanceMapper.INSTANCE.mergeItemDTO(item, this.defaultItemDTO);
	
	Assert.assertNotEquals(item.getIndex(), this.defaultItemDTO.getIndex());
	Assert.assertEquals(item.getCode(), this.defaultItemDTO.getCode());
	Assert.assertEquals(item.getComment(), this.defaultItemDTO.getComment());
	Assert.assertEquals(item.getCostCenterCode(), this.defaultItemDTO.getCostCenterCode());
	Assert.assertEquals(item.getDescription(), this.defaultItemDTO.getDescription());
	Assert.assertEquals(item.getTaxCode(), this.defaultItemDTO.getTaxCode());
	Assert.assertEquals(item.getAmount(), this.defaultItemDTO.getAmount());
	Assert.assertEquals(item.getItemPurchaseOrderId(), this.defaultItemDTO.getItemPurchaseOrderId());
	Assert.assertEquals(item.getStartDate(), this.defaultItemDTO.getStartDate());
	Assert.assertEquals(item.getEndDate(), this.defaultItemDTO.getEndDate());
	Assert.assertEquals(item.getUnitPriceWithoutTax(), this.defaultItemDTO.getUnitPriceWithoutTax());
	
    }
    
    private <T> OngoingStubbing<LocalDateTime> getLocalDatetimeNowMockitoBehavior() {
	return Mockito.when(LocalDateTime.now());
    }
    
}
