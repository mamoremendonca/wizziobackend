package com.novabase.omni.oney.apps.financialdocument.controller.ri;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentHeader;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.manager.CoreManager;
import com.novabase.omni.oney.apps.financialdocument.manager.FinancialDocumentRepositoryManager;
import com.novabase.omni.oney.apps.financialdocument.manager.WorkflowManager;
import com.novabase.omni.oney.apps.financialdocument.validator.SubmitToCreationValidator;

import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.api.trigger.instance.JourneyInstanceHeader;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.session.JourneySession;

@RunWith(PowerMockRunner.class)
@PrepareForTest(JourneySession.class)
public class FinancialDocumentControllerTest {

	public static final Long DEFAULT_JOURNEY_INSTANCE_ID = 1L;

	@InjectMocks
	private FinancialDocumentController financialDocumentController;

	@Mock
	private WorkflowManager workflowManager;

	@Mock
	private FinancialDocumentRepositoryManager repositoryManager;

	@Mock
	private CoreManager coreManager;

	@Mock
	private SubmitToCreationValidator submitToCreationValidator;

	private FinancialDocumentInstance defaultInstance;

	@Before
	public void setUp() {

		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(JourneySession.class);

		this.defaultInstance = new FinancialDocumentInstance();
		this.defaultInstance.setHeader(new JourneyInstanceHeader());
		this.defaultInstance.getHeader().instanceId = DEFAULT_JOURNEY_INSTANCE_ID;
		this.defaultInstance.setFinancialDocumentHeader(new FinancialDocumentHeader());
		this.defaultInstance.getFinancialDocumentHeader().setDocumentId(123L);

		this.getJourneySessionGetInstanceMockitoBehavior(this.financialDocumentController)
				.thenReturn(this.defaultInstance);

	}

	@Test
	public void createProcessTest() {

//	TODO: Testing createProcess requires Mock of the JourneyContext to be able to call JourneySession.getInstance(this) there. Review with Wizzio Team.

		Assert.assertTrue(Boolean.TRUE);

	}

	@Test
	public void updateProcessHeaderDraftTest() {

//	TODO: Testing createProcess requires Mock of the JourneyContext to be able to call JourneySession.getInstance(this) there. Review with Wizzio Team.

		Assert.assertTrue(Boolean.TRUE);

	}

	@Test
	public void setPurchaseOrderTest() {

//	TODO: Testing createProcess requires Mock of the JourneyContext to be able to call JourneySession.getInstance(this) there. Review with Wizzio Team.

		Assert.assertTrue(Boolean.TRUE);

	}

	@Test
	public void getProcessTest() {

//	final FinancialDocumentDTO resultInstance = this.financialDocumentController.getProcess(DEFAULT_JOURNEY_INSTANCE_ID);
//	
//	Assert.assertEquals(resultInstance.getFinancialDocumentHeader().getDocumentId(), this.defaultInstance.getFinancialDocumentHeader().getDocumentId());

	    Assert.assertTrue(Boolean.TRUE);

	}

	@Test
	public void submitCreationTest() {

//	TODO: Testing createProcess requires Mock of the JourneyContext to be able to call JourneySession.getInstance(this) there. Review with Wizzio Team.

		Assert.assertTrue(Boolean.TRUE);

	}

	@Test
	public void approveFinancialDocumentTest() {

//	TODO: Testing createProcess requires Mock of the JourneyContext to be able to call JourneySession.getInstance(this) there. Review with Wizzio Team.

		Assert.assertTrue(Boolean.TRUE);

	}

	private <T> OngoingStubbing<FinancialDocumentInstance> getJourneySessionGetInstanceMockitoBehavior(final AbstractAppController controller) {
		return Mockito.when(JourneySession.getInstance(controller));
	}

}
