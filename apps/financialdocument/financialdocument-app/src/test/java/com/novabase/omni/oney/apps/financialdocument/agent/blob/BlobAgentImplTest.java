package com.novabase.omni.oney.apps.financialdocument.agent.blob;

import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.ContentDisposition;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;

import io.digitaljourney.platform.plugins.modules.blob.service.api.BlobResource;
import io.digitaljourney.platform.plugins.modules.blob.service.api.dto.BlobDTO;

public class BlobAgentImplTest {

    private static final String DEFAULT_BLOB_NAME = "testFile.txt";
    private static final Long DEFAULT_BLOB_FILE_SIZE = 100L;
    private static final Long DEFAULT_BLOB_ID = 1L;

    @InjectMocks
    private BlobAgentImpl blobAgentImpl;

    @Mock
    private BlobResource blobResource;

    @Mock
    private BlobAgentConfig config;

    private InputStream defaultInputStream;

    private BlobDTO defaultBlobDTO;
    private Attachment defaultAttachment;

    @Before
    public void setUp() {

	MockitoAnnotations.initMocks(this);

	this.defaultInputStream = IOUtils.toInputStream("abc", Charset.defaultCharset());

	this.defaultBlobDTO = new BlobDTO();
	this.defaultBlobDTO.id = DEFAULT_BLOB_ID;

	final ContentDisposition cd = new ContentDisposition("form-data; name=\"data\"; filename=" + DEFAULT_BLOB_NAME);
	this.defaultAttachment = new Attachment("root.message@cxf.apache.org", this.defaultInputStream, cd);

	Mockito.when(config.blobProvider()).thenReturn("FILESYSTEM");

    }

    @Test
    public void saveBlobTest() throws Exception {

	this.getSaveBlobMockitoBehavior(this.defaultAttachment, DEFAULT_BLOB_NAME, "FILESYSTEM", new Float(DEFAULT_BLOB_FILE_SIZE)).thenReturn(this.defaultBlobDTO);

	final Long blobId = this.blobAgentImpl.saveBlob(this.defaultInputStream, DEFAULT_BLOB_NAME, new Float(DEFAULT_BLOB_FILE_SIZE));

	Assert.assertEquals(blobId, this.defaultBlobDTO.id);

    }

    @Test
    public void removeBlobTest() throws Exception {

	this.blobAgentImpl.removeFileFromBlob(DEFAULT_BLOB_ID);

	Mockito.verify(this.blobResource).removeBlob(DEFAULT_BLOB_ID);

    }

    @Test(expected = Exception.class)
    public void saveBlobExceptionTest() throws Exception {

	this.getSaveBlobMockitoBehavior(this.defaultAttachment, DEFAULT_BLOB_NAME, "FILESYSTEM", new Float(DEFAULT_BLOB_FILE_SIZE))
		.thenThrow(new Exception("The BlobResource Service Returned Error When Saving a new Blob..."));

	this.blobAgentImpl.saveBlob(this.defaultInputStream, DEFAULT_BLOB_NAME, new Float(DEFAULT_BLOB_FILE_SIZE));

    }

    @Test(expected = Exception.class)
    public void removeBlobExceptionTest() throws Exception {

	Mockito.doThrow(new Exception("The BlobResource Service Returned Error When Updating the Blob Info...")).when(this.blobResource).removeBlob(ArgumentMatchers.anyLong());

	this.blobAgentImpl.updateBlob(DEFAULT_BLOB_ID, this.defaultInputStream, DEFAULT_BLOB_NAME, new Float(DEFAULT_BLOB_FILE_SIZE));

    }

    private <T> OngoingStubbing<BlobDTO> getSaveBlobMockitoBehavior(final Attachment inputAttachment, final String inputString, final String inputString2, final Float inputFloat) throws Exception {
	return Mockito.when(this.blobResource.saveBlob(inputAttachment, inputString, inputString2, inputFloat));
    }

}
