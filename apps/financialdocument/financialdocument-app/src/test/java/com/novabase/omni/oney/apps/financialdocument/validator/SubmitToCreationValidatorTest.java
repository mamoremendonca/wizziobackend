package com.novabase.omni.oney.apps.financialdocument.validator;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.novabase.omni.oney.apps.financialdocument.agent.referencedata.ReferenceDataAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentType;
import com.novabase.omni.oney.apps.financialdocument.manager.CoreManager;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierGeneralDataMsDTO;

public class SubmitToCreationValidatorTest {

    @InjectMocks
    private SubmitToCreationValidator validator;
    
    @Mock
    private ReferenceDataAgentImpl referenceDataAgentImpl;
    
    @Mock
    private CoreManager coreManager;
    
    @Before
    public void setup() {
	
	MockitoAnnotations.initMocks(this);
	
	SupplierGeneralDataMsDTO supplierGeneralDataMsDTO = new SupplierGeneralDataMsDTO();
	supplierGeneralDataMsDTO.nif = "123123123";
	
	Mockito.when(this.referenceDataAgentImpl.getSupplierMarketInfo("invalidSupplierCode")).thenReturn(null);
	Mockito.when(this.referenceDataAgentImpl.getSupplierMarketInfo("validSupplierCode")).thenReturn(supplierGeneralDataMsDTO);
	Mockito.when(this.coreManager.financialDocumentNumberAlreadyExists("0")).thenReturn(false);
	
    }
    
    @Test
    public void testEveryMandatoryFieldsNull() {
	FinancialDocumentInstance instance = new FinancialDocumentInstance();
	
	instance.getFinancialDocumentHeader().setType(null);
	instance.getFinancialDocumentHeader().setDocumentId(null);
	instance.getFinancialDocumentHeader().setNumber(null);
	instance.getFinancialDocumentHeader().setSupplierCode(null);
	
	List<String> errorFields = this.validator.getErrorFields(instance);
	
	assertEquals(4, errorFields.size());
	assertEquals(true, errorFields.contains("documentId"));
	assertEquals(true, errorFields.contains("type"));
	assertEquals(true, errorFields.contains("number"));
	assertEquals(true, errorFields.contains("supplierCode"));
    }
    
    @Test
    public void testEveryMandatoryFieldsEmpty() {
	FinancialDocumentInstance instance = new FinancialDocumentInstance();
	
	instance.getFinancialDocumentHeader().setType(null);
	instance.getFinancialDocumentHeader().setDocumentId(0L);
	instance.getFinancialDocumentHeader().setNumber(null);
	instance.getFinancialDocumentHeader().setSupplierCode("");
	
	List<String> errorFields = this.validator.getErrorFields(instance);
	
	assertEquals(4, errorFields.size());
	assertEquals(true, errorFields.contains("documentId"));
	assertEquals(true, errorFields.contains("type"));
	assertEquals(true, errorFields.contains("number"));
	assertEquals(true, errorFields.contains("supplierCode"));
    }
    
    @Test
    public void testInvalidSupplierCode() {
	FinancialDocumentInstance instance = new FinancialDocumentInstance();
	
	instance.getFinancialDocumentHeader().setType(FinancialDocumentType.INVOICE);
	instance.getFinancialDocumentHeader().setDocumentId(1L);
	instance.getFinancialDocumentHeader().setNumber("0");
	instance.getFinancialDocumentHeader().setSupplierCode("invalidSupplierCode");
	
	List<String> errorFields = this.validator.getErrorFields(instance);
	
	assertEquals(1, errorFields.size());
	assertEquals(true, errorFields.contains("supplierCode"));
    }
    
    @Test
    public void testValidSupplierCode() {
	FinancialDocumentInstance instance = new FinancialDocumentInstance();
	
	instance.getFinancialDocumentHeader().setType(FinancialDocumentType.INVOICE);
	instance.getFinancialDocumentHeader().setDocumentId(1L);
	instance.getFinancialDocumentHeader().setNumber("0");
	instance.getFinancialDocumentHeader().setSupplierCode("validSupplierCode");
	
	List<String> errorFields = this.validator.getErrorFields(instance);
	
	assertEquals(0, errorFields.size());
	assertEquals(false, errorFields.contains("supplierCode"));
    }
}
