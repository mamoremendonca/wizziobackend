package com.novabase.omni.oney.apps.financialdocument.dto;

import java.time.LocalDateTime;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentStatus;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentType;
import com.novabase.omni.oney.apps.financialdocument.json.serializer.NBLocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class FinancialDocumentListDTO extends DTO {

    @ApiModelProperty
    private FinancialDocumentStatus repositoryStatus;

    @ApiModelProperty
    private FinancialDocumentType type;

    @ApiModelProperty
    private String number;

    @ApiModelProperty
    private Long purchaseOrderNumber;

    @ApiModelProperty
    private String purchaseOrderFriendlyNumber;

    @ApiModelProperty
    private String associatedDocument;

    @ApiModelProperty
    private String supplierCode;

    @ApiModelProperty
    private boolean granting;

    @ApiModelProperty
    private String departmentCode;

    @ApiModelProperty
    private String projectCode;

    @ApiModelProperty
    private String internalOrderCode;

    @ApiModelProperty
    private Double totalWithoutTax;

    @ApiModelProperty(readOnly = true)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime creationDate;

    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime finishDate;

    @ApiModelProperty(readOnly = true)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime emissionDate;

    @ApiModelProperty(readOnly = true)
    private Long repositoryId;

    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime expirationDate;

    @ApiModelProperty
    private String currentUserGroup;

    public FinancialDocumentListDTO() {
	super();
    }

    public FinancialDocumentListDTO(FinancialDocumentStatus repositoryStatus, FinancialDocumentType type, String number, Long purchaseOrderNumber, String purchaseOrderFriendlyNumber,
	    String associatedDocument, String supplierCode, boolean granting, String departmentCode, String projectCode, String internalOrderCode, Double totalWithoutTax, LocalDateTime creationDate,
	    LocalDateTime finishDate, LocalDateTime emissionDate, Long repositoryId, LocalDateTime expirationDate, String currentUserGroup) {
	super();
	this.repositoryStatus = repositoryStatus;
	this.type = type;
	this.number = number;
	this.purchaseOrderNumber = purchaseOrderNumber;
	this.purchaseOrderFriendlyNumber = purchaseOrderFriendlyNumber;
	this.associatedDocument = associatedDocument;
	this.supplierCode = supplierCode;
	this.granting = granting;
	this.departmentCode = departmentCode;
	this.projectCode = projectCode;
	this.internalOrderCode = internalOrderCode;
	this.totalWithoutTax = totalWithoutTax;
	this.creationDate = creationDate;
	this.finishDate = finishDate;
	this.emissionDate = emissionDate;
	this.repositoryId = repositoryId;
	this.expirationDate = expirationDate;
	this.currentUserGroup = currentUserGroup;
    }

    public FinancialDocumentStatus getRepositoryStatus() {
	return repositoryStatus;
    }

    public void setRepositoryStatus(FinancialDocumentStatus repositoryStatus) {
	this.repositoryStatus = repositoryStatus;
    }

    public FinancialDocumentType getType() {
	return type;
    }

    public void setType(FinancialDocumentType type) {
	this.type = type;
    }

    public String getNumber() {
	return number;
    }

    public void setNumber(String number) {
	this.number = number;
    }

    public Long getPurchaseOrderNumber() {
	return purchaseOrderNumber;
    }

    public void setPurchaseOrderNumber(Long purchaseOrderNumber) {
	this.purchaseOrderNumber = purchaseOrderNumber;
    }

    public String getPurchaseOrderFriendlyNumber() {
	return purchaseOrderFriendlyNumber;
    }

    public void setPurchaseOrderFriendlyNumber(String purchaseOrderFriendlyNumber) {
	this.purchaseOrderFriendlyNumber = purchaseOrderFriendlyNumber;
    }

    public String getAssociatedDocument() {
	return associatedDocument;
    }

    public void setAssociatedDocument(String associatedDocument) {
	this.associatedDocument = associatedDocument;
    }

    public String getSupplierCode() {
	return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
	this.supplierCode = supplierCode;
    }

    public boolean isGranting() {
	return granting;
    }

    public void setGranting(boolean granting) {
	this.granting = granting;
    }

    public String getDepartmentCode() {
	return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
	this.departmentCode = departmentCode;
    }

    public String getProjectCode() {
	return projectCode;
    }

    public void setProjectCode(String projectCode) {
	this.projectCode = projectCode;
    }

    public String getInternalOrderCode() {
	return internalOrderCode;
    }

    public void setInternalOrderCode(String internalOrderCode) {
	this.internalOrderCode = internalOrderCode;
    }

    public Double getTotalWithoutTax() {
	return totalWithoutTax;
    }

    public void setTotalWithoutTax(Double totalWithoutTax) {
	this.totalWithoutTax = totalWithoutTax;
    }

    public LocalDateTime getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
	this.creationDate = creationDate;
    }

    public LocalDateTime getFinishDate() {
	return finishDate;
    }

    public void setFinishDate(LocalDateTime finishDate) {
	this.finishDate = finishDate;
    }

    public LocalDateTime getEmissionDate() {
	return emissionDate;
    }

    public void setEmissionDate(LocalDateTime emissionDate) {
	this.emissionDate = emissionDate;
    }

    public Long getRepositoryId() {
	return repositoryId;
    }

    public void setRepositoryId(Long repositoryId) {
	this.repositoryId = repositoryId;
    }

    public LocalDateTime getExpirationDate() {
	return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
	this.expirationDate = expirationDate;
    }

    public String getCurrentUserGroup() {
	return currentUserGroup;
    }

    public void setCurrentUserGroup(String currentUserGroup) {
	this.currentUserGroup = currentUserGroup;
    }

}
