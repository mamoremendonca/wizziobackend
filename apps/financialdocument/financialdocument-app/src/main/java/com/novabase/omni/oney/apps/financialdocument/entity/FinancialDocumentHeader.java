package com.novabase.omni.oney.apps.financialdocument.entity;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentType;
import com.novabase.omni.oney.apps.financialdocument.enums.purchaseorder.PurchaseOrderType;
import com.novabase.omni.oney.apps.financialdocument.json.serializer.NBLocalDateTimeSerializer;

public class FinancialDocumentHeader {

    private String number;
    private FinancialDocumentType type;
    private String supplierCode;
    private Long documentId;
    private String documentMimeType;
    private String documentFileName;
    private String description;
    private String departmentCode;
    private String projectCode;
    private String internalOrderCode;
    private Double totalWithoutTax;
    private Long purchaseOrderId;
    private String purchaseOrderFriendlyNumber;
    private String purchaseCreatedGroup;
    private String firstWorkflowGroup;
    private Double purchaseOrderTotalWithoutTax;
    private PurchaseOrderType purchaseOrderType;
    private String associatedDocument;
    private Long associatedDocumentId;
    private boolean granting;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime expirationDate;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime emissionDate;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime submitToCreationDate;

    private Long repositoryId;

    private String userGroup;
    private Long userId;
    private Long digitalizationUserId;

    public FinancialDocumentHeader() {

	super();

	this.repositoryId = 0L;
	this.documentId = 0L;
//	Cannot init theses dates at start of the Process.
//	this.emissionDate = LocalDateTime.now();
//	this.expirationDate = LocalDateTime.now();
	this.totalWithoutTax = 0D;
	this.userId = 0L;
	this.digitalizationUserId = 0L;
	this.purchaseOrderType = PurchaseOrderType.OPEX;
	this.associatedDocumentId = 0L;
	this.purchaseOrderId = 0L;
	this.totalWithoutTax = 0d;
	this.purchaseOrderTotalWithoutTax = 0d;

    }

    public String getFirstWorkflowGroup() {
        return firstWorkflowGroup;
    }

    public void setFirstWorkflowGroup(String firstWorkflowGroup) {
        this.firstWorkflowGroup = firstWorkflowGroup;
    }

    public Long getPurchaseOrderId() {
	return purchaseOrderId;
    }

    public void setPurchaseOrderId(Long purchaseOrderId) {
	this.purchaseOrderId = purchaseOrderId;
    }

    public boolean isGranting() {
	return granting;
    }

    public void setGranting(boolean granting) {
	this.granting = granting;
    }

    public PurchaseOrderType getPurchaseOrderType() {
	return purchaseOrderType;
    }

    public void setPurchaseOrderType(PurchaseOrderType purchaseOrderType) {
	this.purchaseOrderType = purchaseOrderType;
    }

    public String getPurchaseOrderFriendlyNumber() {
	return purchaseOrderFriendlyNumber;
    }

    public void setPurchaseOrderFriendlyNumber(String purchaseOrderFriendlyNumber) {
	this.purchaseOrderFriendlyNumber = purchaseOrderFriendlyNumber;
    }

    public String getPurchaseCreatedGroup() {
	return purchaseCreatedGroup;
    }

    public void setPurchaseCreatedGroup(String purchaseCreatedGroup) {
	this.purchaseCreatedGroup = purchaseCreatedGroup;
    }

    public String getAssociatedDocument() {
	return associatedDocument;
    }

    public void setAssociatedDocument(String associatedDocument) {
	this.associatedDocument = associatedDocument;
    }

    public Long getDocumentId() {
	return documentId;
    }

    public void setDocumentId(Long documentId) {
	this.documentId = documentId;
    }

    public String getDocumentMimeType() {
	return documentMimeType;
    }

    public void setDocumentMimeType(String documentMimeType) {
	this.documentMimeType = documentMimeType;
    }

    public String getDocumentFileName() {
	return documentFileName;
    }

    public void setDocumentFileName(String documentFileName) {
	this.documentFileName = documentFileName;
    }

    public LocalDateTime getEmissionDate() {
	return emissionDate;
    }

    public void setEmissionDate(LocalDateTime emissionDate) {
	this.emissionDate = emissionDate;
    }

    public Long getRepositoryId() {
	return repositoryId;
    }

    public void setRepositoryId(Long repositoryId) {
	this.repositoryId = repositoryId;
    }

    public String getNumber() {
	return number;
    }

    public void setNumber(String number) {
	this.number = number;
    }

    public FinancialDocumentType getType() {
	return type;
    }

    public void setType(FinancialDocumentType type) {
	this.type = type;
    }

    public String getSupplierCode() {
	return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
	this.supplierCode = supplierCode;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getDepartmentCode() {
	return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
	this.departmentCode = departmentCode;
    }

    public String getProjectCode() {
	return projectCode;
    }

    public void setProjectCode(String projectCode) {
	this.projectCode = projectCode;
    }

    public String getInternalOrderCode() {
	return internalOrderCode;
    }

    public void setInternalOrderCode(String internalOrderCode) {
	this.internalOrderCode = internalOrderCode;
    }

    public LocalDateTime getExpirationDate() {
	return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
	this.expirationDate = expirationDate;
    }

    public Double getTotalWithoutTax() {
	return totalWithoutTax;
    }

    public void setTotalWithoutTax(Double totalWithoutTax) {
	this.totalWithoutTax = totalWithoutTax;
    }

    public String getUserGroup() {
	return userGroup;
    }

    public void setUserGroup(String userGroup) {
	this.userGroup = userGroup;
    }

    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    public Long getDigitalizationUserId() {
	return digitalizationUserId;
    }

    public void setDigitalizationUserId(Long digitalizationUserId) {
	this.digitalizationUserId = digitalizationUserId;
    }

    public LocalDateTime getSubmitToCreationDate() {
	return submitToCreationDate;
    }

    public void setSubmitToCreationDate(LocalDateTime submitToCreationDate) {
	this.submitToCreationDate = submitToCreationDate;
    }

    public Long getAssociatedDocumentId() {
	return associatedDocumentId;
    }

    public void setAssociatedDocumentId(Long associatedDocumentId) {
	this.associatedDocumentId = associatedDocumentId;
    }

    public Double getPurchaseOrderTotalWithoutTax() {
	return purchaseOrderTotalWithoutTax;
    }

    public void setPurchaseOrderTotalWithoutTax(Double purchaseOrderTotalWithoutTax) {
	this.purchaseOrderTotalWithoutTax = purchaseOrderTotalWithoutTax;
    }

}
