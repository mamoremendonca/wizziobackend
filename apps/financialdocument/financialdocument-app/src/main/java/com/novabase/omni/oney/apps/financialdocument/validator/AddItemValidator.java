package com.novabase.omni.oney.apps.financialdocument.validator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.financialdocument.AppContext;
import com.novabase.omni.oney.apps.financialdocument.agent.referencedata.ReferenceDataAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.dto.ItemDTO;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentType;
import com.novabase.omni.oney.apps.financialdocument.enums.purchaseorder.PurchaseOrderType;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.AssetMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.CostCenterMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ItemMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ProjectMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.RetentionCodeMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.TaxMsDTO;

@Component(service = { AddItemValidator.class })
public class AddItemValidator implements Validator<ItemDTO> {

    @Reference
    private AppContext ctx;

    @Reference
    private ReferenceDataAgentImpl referenceDataAgentImpl;

    @Override
    public List<String> getErrorFields(final ItemDTO target) {

	final List<String> errors = new ArrayList<String>();

	if (target == null) {
	    errors.add("item");
	} else {

	    if (this.isInvalid(target.getCode()) 
		    || (this.itemIsInvalid(target.getCode()) && this.assetIsInvalid(target.getCode()))) {
		errors.add("itemCode");
	    }

	    if (this.isInvalid(target.getDescription())) {
		errors.add("description");
	    }

	    if (this.isInvalid(target.getTaxCode()) || this.taxIsInvalid(target.getTaxCode())) {
		errors.add("taxCode");
	    }
	    
	    if(this.isInvalid(target.getInternalOrderCode()) 
		    && this.costCentrerIsInvalid(target.getCostCenterCode()) && !PurchaseOrderType.CAPEX.equals(target.getPurchaseOrderType())) {
		errors.add("costcenter");
	    } 

	    if (this.isInvalid(target.getCostCenterCode()) || this.costCentrerIsInvalid(target.getCostCenterCode())) {
	    }

	    if (target.getAmount() <= 0) {
		errors.add("amount");
	    }

	    if (target.getUnitPriceWithoutTax() == null || target.getUnitPriceWithoutTax() <= 0D) {
		errors.add("unitPriceWithoutTax");
	    }

	    if (FinancialDocumentType.RETENTION_INVOICE.equals(target.getFinancialDocumentType())) {
		if(this.isInvalid(target.getRetentionCode()) || this.retentionCodeIsInvalid(target.getRetentionCode())) {
		    errors.add("retentionCode");
		}
	    }else {
		if(StringUtils.isNotEmpty(target.getRetentionCode())) {
		    errors.add("retentionCode");
		}
	    }

	    // TODO: validar items de acordo com o tipo de PO

	    if (PurchaseOrderType.CONTRACT.equals(target.getPurchaseOrderType())) {

	    }

	    if (PurchaseOrderType.OPEX.equals(target.getPurchaseOrderType())) {

	    }

	    if (PurchaseOrderType.CAPEX.equals(target.getPurchaseOrderType())) {

	    }
	    
	    //TODO: validar quando for para deferimento

	}

	return errors;

    }

    @Override
    public AppContext getCtx() {
	return this.ctx;
    }

    private boolean itemIsInvalid(final String itemCode) {

	final List<ItemMsDTO> items = this.referenceDataAgentImpl.getItems();
	return items.stream().filter(item -> item.code.equals(itemCode)).findAny().orElse(null) == null;

    }
    
    private boolean assetIsInvalid(final String itemCode) {

	 List<AssetMsDTO> assets = this.referenceDataAgentImpl.getAssets();
	return assets.stream().filter(item -> item.code.equals(itemCode)).findAny().orElse(null) == null;

    }

    private boolean taxIsInvalid(final String taxCode) {

	final List<TaxMsDTO> taxes = this.referenceDataAgentImpl.getTaxes();
	return taxes.stream().filter(tax -> tax.code.equals(taxCode)).findAny().orElse(null) == null;

    }

    private boolean retentionCodeIsInvalid(final String retentionCode) {

	final List<RetentionCodeMsDTO> retentionCodes = this.referenceDataAgentImpl.getRetentionCodes();
	return retentionCodes.stream().filter(rt -> rt.code.equals(retentionCode)).findAny().orElse(null) == null;

    }

    private boolean costCentrerIsInvalid(final String costCenterCode) {

	final List<CostCenterMsDTO> costCenters = this.referenceDataAgentImpl.getCostCenters();

	final List<ProjectMsDTO> projects = this.referenceDataAgentImpl.getProjects();

	//@formatter:off
	CostCenterMsDTO costCenter = 
		 costCenters.stream().
		    		filter(cc -> cc.code.equals(costCenterCode)).
		    		findAny().
		    		orElse(null);

	//@formatter:on

	//@formatter:off
	CostCenterMsDTO project = 
		projects.stream().
				filter(p -> p.code.equals(costCenterCode)).
			    	findAny().
		    		orElse(null);
	//@formatter:on

	return costCenter == null && project == null;

    }

}
