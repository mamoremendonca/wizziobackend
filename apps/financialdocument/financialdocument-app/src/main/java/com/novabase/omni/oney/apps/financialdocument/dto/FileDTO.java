package com.novabase.omni.oney.apps.financialdocument.dto;

import java.io.InputStream;

import org.osgi.dto.DTO;

public class FileDTO extends DTO {

    private String mimeType;
    private InputStream inputStream;
    private String fileName;
    
    public FileDTO() {
	super();
    }
    
    public String getMimeType() {
        return mimeType;
    }
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
    public InputStream getInputStream() {
        return inputStream;
    }
    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }
    public String getFileName() {
        return fileName;
    }
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
}
