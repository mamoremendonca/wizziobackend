package com.novabase.omni.oney.apps.financialdocument.exception;

import java.util.List;
import java.util.stream.Collectors;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentFieldValidationException extends FinancialDocumentException {

    private static final long serialVersionUID = 194131178633961805L;

    private List<String> errorFields;

    public FinancialDocumentFieldValidationException(final String errorCode, final Context ctx, List<String> errorFields, final Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
	this.errorFields = errorFields;
    }

    public static FinancialDocumentFieldValidationException of(final Context ctx, final List<String> fields) {

	String concatFields = null;
	if (fields != null && fields.isEmpty() == false) {
	    concatFields = fields.stream().collect(Collectors.joining(", "));
	}
	return new FinancialDocumentFieldValidationException(AppProperties.FIELD_VALIDATION_EXCEPTION, ctx, fields, concatFields);
    }

    public List<String> getErrorFields() {
	return errorFields;
    }

}
