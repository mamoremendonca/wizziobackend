package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentPaidException extends FinancialDocumentException {

    private static final long serialVersionUID = -8125494715683351997L;

    public FinancialDocumentPaidException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
    }
    
    public FinancialDocumentPaidException(String errorCode, Context ctx, String financialDocumentNumber) {
   	super(PlatformCode.NOT_FOUND, errorCode, ctx, financialDocumentNumber);
       }

    public static FinancialDocumentPaidException ofStatus(Context ctx, String financialDocumentNumber, String status) {
	return new FinancialDocumentPaidException(AppProperties.COULD_NOT_PAY_STATUS_EXCEPTION, ctx, financialDocumentNumber, status);
    }
    
    public static FinancialDocumentPaidException ofNotFound(Context ctx, String financialDocumentNumber) {
	return new FinancialDocumentPaidException(AppProperties.COULD_NOT_PAY_NOT_FOUND_EXCEPTION, ctx, financialDocumentNumber);
    }

}
