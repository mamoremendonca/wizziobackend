package com.novabase.omni.oney.apps.financialdocument.manager;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.function.Consumer;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.financialdocument.AppContext;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentHeaderDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentRepositoryDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder.PurchaseOrderDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.CurrentAction;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.entity.Item;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentType;
import com.novabase.omni.oney.apps.financialdocument.enums.WorkFlowActionType;
import com.novabase.omni.oney.apps.financialdocument.enums.purchaseorder.PurchaseOrderType;
import com.novabase.omni.oney.apps.financialdocument.mapper.FinancialDocumentInstanceMapper;
import com.novabase.omni.oney.apps.financialdocument.validator.SubmitToCreationValidator;

@Component(service = { FinancialDocumentManager.class })
public class FinancialDocumentManager {

    @Reference
    private AppContext ctx;

    @Reference
    private PurchaseOrderRepositoryManager purchaseOrderRepositoryManager;
    
    @Reference
    private FinancialDocumentRepositoryManager financialDocumentRepositoryManager;

    @Reference
    private SubmitToCreationValidator submitToCreationValidator;
    
    public FinancialDocumentDTO createProcess(final FinancialDocumentInstance instance, final String userGroup, final Long userId) {

	instance.getFinancialDocumentHeader().setUserGroup(userGroup);
	instance.getFinancialDocumentHeader().setUserId(userId);
	instance.setCurrentAction(new CurrentAction());
	instance.getCurrentAction().setStartDate(LocalDateTime.now());
	instance.getWorkFlowAvailableActions().addAll(Arrays.asList(WorkFlowActionType.CANCEL));

	if (instance.getHeader().targets == null) {
	    instance.getHeader().targets = new HashMap<String, String>();
	}

	instance.refreshTargets();

	if (StringUtils.isBlank(instance.getFinancialDocumentHeader().getUserGroup())) {
	    throw this.ctx.fieldValidationException(Arrays.asList("userGroup"));
	}

	return FinancialDocumentInstanceMapper.INSTANCE.toProcessDTO(instance);

    }

    public FinancialDocumentDTO updateProcessHeaderCreation(final FinancialDocumentInstance instance, final FinancialDocumentHeaderDTO financialDocumentHeaderDTO) {

	return this.updateProcessHeader(instance, financialDocumentHeaderDTO, (final FinancialDocumentInstance funcInstance) -> {

	    if (StringUtils.trimToEmpty(funcInstance.getFinancialDocumentHeader().getPurchaseOrderFriendlyNumber()).isEmpty() == false) {
		FinancialDocumentInstanceMapper.INSTANCE.mergeFinancialDocumentHeaderDTOCreationPO(funcInstance, financialDocumentHeaderDTO);
	    } else {

		// If supplierCode or projectCode or internalOrder or departmentCode changes we must clean the items 
		//@formatter:off
		if (stringChanged(instance.getFinancialDocumentHeader().getSupplierCode(), financialDocumentHeaderDTO.getSupplierCode())
			|| stringChanged(instance.getFinancialDocumentHeader().getProjectCode(), financialDocumentHeaderDTO.getProjectCode())
			|| stringChanged(instance.getFinancialDocumentHeader().getInternalOrderCode(), financialDocumentHeaderDTO.getInternalOrderCode())
			|| stringChanged(instance.getFinancialDocumentHeader().getDepartmentCode(), financialDocumentHeaderDTO.getDepartmentCode())) {
		    funcInstance.setItems(new ArrayList<>());

		    if(stringChanged(instance.getFinancialDocumentHeader().getDepartmentCode(), financialDocumentHeaderDTO.getDepartmentCode())) {
			instance.getFinancialDocumentHeader().setProjectCode(null);
			instance.getFinancialDocumentHeader().setInternalOrderCode(null);
			instance.getFinancialDocumentHeader().setFirstWorkflowGroup(null);
		    }
		    
		}
		
		// If granting check changes and the PO type is not contract we must clean the start and end date on items
		if(instance.getFinancialDocumentHeader().isGranting() != financialDocumentHeaderDTO.isGranting()
			&& PurchaseOrderType.CONTRACT.equals(instance.getFinancialDocumentHeader().getPurchaseOrderType()) == false
			&& instance.getItems() != null
			&& instance.getItems().isEmpty() == false) {
		    
		    instance.getItems()
		    	.forEach(item -> 
		    		{
		    		    item.setStartDate(null); item.setEndDate(null);
		    		});
		}
		//@formatter:on

		FinancialDocumentInstanceMapper.INSTANCE.mergeFinancialDocumentHeaderDTOCreation(funcInstance, financialDocumentHeaderDTO);
	    }

	    funcInstance.refreshTotal();
	    funcInstance.refreshTargets();

	}, true);

    }

    //@formatter:off
    private boolean stringChanged(String str1, String str2) {
	return StringUtils.trimToEmpty(str1).equals(StringUtils.trimToEmpty(str2)) == false;
    }
    //@formatter:on

    public FinancialDocumentDTO updateProcessHeaderDraft(final FinancialDocumentInstance instance, final FinancialDocumentHeaderDTO financialDocumentHeaderDTO) {

	return this.updateProcessHeader(instance, financialDocumentHeaderDTO, (final FinancialDocumentInstance funcInstance) -> {
	    FinancialDocumentInstanceMapper.INSTANCE.mergeFinancialDocumentHeaderDTODraft(funcInstance, financialDocumentHeaderDTO);
	}, false);

    }

    private FinancialDocumentDTO updateProcessHeader(final FinancialDocumentInstance instance, final FinancialDocumentHeaderDTO financialDocumentHeaderDTO,
	    final Consumer<FinancialDocumentInstance> function, boolean refreshTargetIncludeNumber) {

	// Merge header to instance
	function.accept(instance);

	final FinancialDocumentDTO processDTO = FinancialDocumentInstanceMapper.INSTANCE.toProcessDTO(instance);

	instance.refreshTargets(refreshTargetIncludeNumber);

	return processDTO;

    }

    public FinancialDocumentDTO associatePurchaseOrder(final FinancialDocumentInstance instance, final String number) {

	if (FinancialDocumentType.CREDIT_NOTE.equals(instance.getFinancialDocumentHeader().getType())) {
	    this.ctx.fieldValidationException(Arrays.asList("type"));
	}
	
	// If number is null then unassociate Purchase Order
	if (number == null) {

	    instance.getFinancialDocumentHeader().setPurchaseOrderId(0L);
	    instance.getFinancialDocumentHeader().setPurchaseOrderFriendlyNumber(null);
	    instance.getFinancialDocumentHeader().setPurchaseOrderType(PurchaseOrderType.OPEX);
	    instance.getFinancialDocumentHeader().setPurchaseCreatedGroup(null);
	    instance.getFinancialDocumentHeader().setFirstWorkflowGroup(null);
	    instance.setItems(new ArrayList<Item>());
	    
	} else {

	    PurchaseOrderDTO purchaseOrder = null;
	    try {
		purchaseOrder = this.purchaseOrderRepositoryManager.getApprovedPurchaseOrderByNumber(number);
	    } catch (Exception e) {
		throw this.ctx.failedToAssociatePurchaseOrderException(number, instance.getHeader().instanceId);
	    }
	    
	    if (purchaseOrder == null) {
		throw this.ctx.failedToAssociatePurchaseOrderException(number, instance.getHeader().instanceId);
	    }

	    instance.getItems().clear();
	    FinancialDocumentInstanceMapper.INSTANCE.mergeFinancialDocumentInstance(instance, purchaseOrder);

	}

	instance.refreshTotal();
	instance.refreshTargets();

	return FinancialDocumentInstanceMapper.INSTANCE.toProcessDTO(instance);
    }

    public FinancialDocumentDTO associateFinancialDocument(final FinancialDocumentInstance instance, final String number) {

	if (FinancialDocumentType.CREDIT_NOTE.equals(instance.getFinancialDocumentHeader().getType()) == false) {
	    this.ctx.fieldValidationException(Arrays.asList("type"));
	}

	if (number == null) {
	    instance.getFinancialDocumentHeader().setAssociatedDocument(null);
	    instance.getFinancialDocumentHeader().setAssociatedDocumentId(0L);
	} else {

	    FinancialDocumentRepositoryDTO financialDocument = financialDocumentRepositoryManager.getFinancialDocumentNotCanceledNotRejectedByNumber(number);
	    
	    if (financialDocument != null) {
		instance.getFinancialDocumentHeader().setAssociatedDocument(financialDocument.getFinancialDocumentHeader().getNumber());
		instance.getFinancialDocumentHeader().setAssociatedDocumentId(financialDocument.getFinancialDocumentHeader().getRepositoryId());
	    } else  {
		throw this.ctx.failedToAssociateFinancialDocumentException(number, instance.getHeader().instanceId);
	    }
	}

	instance.refreshTotal();
	instance.refreshTargets();

	return FinancialDocumentInstanceMapper.INSTANCE.toProcessDTO(instance);
    }

    public FinancialDocumentHeaderDTO submitToCreation(final FinancialDocumentInstance instance, String creationGroup) {

	this.submitToCreationValidator.validate(instance);

	instance.getFinancialDocumentHeader().setSubmitToCreationDate(LocalDateTime.now());
	
	if(instance.getAttachments() == null) {
	    instance.setAttachments(new ArrayList<>());
	}
	
	instance.getAttachments().addAll(instance.getCurrentAction().getAttachments());
	
	instance.setCurrentAction(new CurrentAction());
	instance.getCurrentAction().setUserGroup(creationGroup);
	instance.getCurrentAction().setStartDate(LocalDateTime.now());
	
	instance.refreshTargets();

	return FinancialDocumentInstanceMapper.INSTANCE.toFinancialDocumentHeaderDTO(instance.getFinancialDocumentHeader());
    }

}
