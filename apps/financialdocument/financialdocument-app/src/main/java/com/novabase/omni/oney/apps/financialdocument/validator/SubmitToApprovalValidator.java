package com.novabase.omni.oney.apps.financialdocument.validator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.financialdocument.AppContext;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentRepositoryDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentType;
import com.novabase.omni.oney.apps.financialdocument.manager.CoreManager;
import com.novabase.omni.oney.apps.financialdocument.manager.FinancialDocumentRepositoryManager;

@Component(service = { SubmitToApprovalValidator.class })
public class SubmitToApprovalValidator implements Validator<FinancialDocumentInstance> {

    @Reference
    private AppContext ctx;

    @Reference
    private FinancialDocumentRepositoryManager financialDocumentRepositoryManager;

    @Reference
    private CoreManager coreManager;

    // TODO: think about use RulesEngine
    //@formatter:off
    @Override
    public List<String> getErrorFields(FinancialDocumentInstance instance) {
	List<String> errors = new ArrayList<String>();
	
	if(isFinancialDocumentNumberUnavailable(instance.getFinancialDocumentHeader().getNumber())) {
	    throw ctx.financialDocumentNumberAlreadyExistsException(instance.getHeader().instanceId, instance.getFinancialDocumentHeader().getNumber());
	}
	
	//@formatter:off
	if(hasPOAssociated(instance) &&  isFinancialDocumentOutOfTheLimitByPurchaseOrderTotal(instance.getFinancialDocumentHeader().getTotalWithoutTax(), instance.getFinancialDocumentHeader().getPurchaseOrderTotalWithoutTax())) {
	    throw  ctx.financialDocumentTotalValuePOException(
		    	instance.getHeader().instanceId, 
		    	instance.getFinancialDocumentHeader().getTotalWithoutTax(), 
		    	instance.getFinancialDocumentHeader().getPurchaseOrderTotalWithoutTax(), 
		    	coreManager.getPercentageLimitToAssociatePO());
	}
	//@formatter:on
	
	if (instance.getItems() == null || instance.getItems().isEmpty()) {
	    errors.add("items");
	}
	
	if( instance.getFinancialDocumentHeader().isGranting()) {
	    instance.getItems().forEach(item -> {
		if(item.getStartDate() == null) {
		    errors.add("item.startDate");
		}
		if(item.getEndDate() == null) {
		    errors.add("item.endDate");
		}
	    });
	}
	
	if(FinancialDocumentType.RETENTION_INVOICE.equals(instance.getFinancialDocumentHeader().getType())) {
	    instance.getItems().forEach(item -> {
		if(StringUtils.isBlank(item.getRetentionCode() )) {
		    errors.add("item.retentionCode");
		}
	    });
	}
	
	if(instance.getFinancialDocumentHeader().getExpirationDate() == null) {
	    errors.add("expirationDate");
	}
	
	if(instance.getFinancialDocumentHeader().getSupplierCode() == null) {
	    errors.add("supplierCode");
	}
	
	
	return errors;
    }
  
    //@formatter:off
    private boolean hasPOAssociated(FinancialDocumentInstance instance) {
	return 
		StringUtils.isNotBlank(instance.getFinancialDocumentHeader().getPurchaseOrderFriendlyNumber()) 
    			&& instance.getFinancialDocumentHeader().getPurchaseOrderId() != null 
    			&&  instance.getFinancialDocumentHeader().getPurchaseOrderId().equals(0L) == false;
    }
    //@formatter:on

    //@formatter:on

    @Override
    public AppContext getCtx() {
	return ctx;
    }

    private boolean isFinancialDocumentNumberUnavailable(String number) {

	try {

	    FinancialDocumentRepositoryDTO financialDocumentNotCanceledByNumber = financialDocumentRepositoryManager.getFinancialDocumentNotCanceledNotRejectedByNumber(number);

	    //@formatter:off
	    if(financialDocumentNotCanceledByNumber != null 
		    && financialDocumentNotCanceledByNumber.getFinancialDocumentHeader().getRepositoryId() != null 
		    && financialDocumentNotCanceledByNumber.getFinancialDocumentHeader().getRepositoryId().equals(0L) == false) {
		return true;
	    }
	    //@formatter:on

	    return false;

	} catch (Exception e) {
	    return false;
	}
    }

    private boolean isFinancialDocumentOutOfTheLimitByPurchaseOrderTotal(Double financialDocumentTotal, Double purchaseOrderTotal) {
	Double percentageLimitToAssociatePO = coreManager.getPercentageLimitToAssociatePO();
	
	if(financialDocumentTotal == null) {
	    financialDocumentTotal = 0d;
	}
	
	if(purchaseOrderTotal == null) {
	    purchaseOrderTotal = 0d;
	}
	
	Double difference = financialDocumentTotal - purchaseOrderTotal;
	
	if(difference <= 0) {
	    return false;
	}
	
	Double percentage = difference / purchaseOrderTotal;
	percentage = percentage *100;
	
	return (percentage <= percentageLimitToAssociatePO) == false;
    }
}
