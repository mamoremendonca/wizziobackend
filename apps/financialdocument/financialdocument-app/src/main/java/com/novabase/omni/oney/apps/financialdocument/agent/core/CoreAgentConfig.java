/*-
 * #%L
 * Apps :: Financial Document App  App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.financialdocument.agent.core;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Financial Document App  Core Agent configuration.
 */
@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/agent/core", icon = @Icon(resource = "OSGI-INF/icon/agent.png", size = 32))
public @interface CoreAgentConfig {
    
    /** Component Persistent Identifier */
    public static final String CPID = "fs.oney.apps.financialdocument.agent.core";

    /**
     * Gets the name of the user used to access core resources.
     * 
     * @return System user name
     */
    @AttributeDefinition(name = "%systemUserName.name", description = "%systemUserName.description")
    public String systemUserName();

    /**
     * Gets the password of the user to access core resources.
     * 
     * @return System user password
     */
    @AttributeDefinition(name = "%systemPassword.name", type = AttributeType.PASSWORD, description = "%systemPassword.description")
    public String systemPassword();
	
    /**
     * Default mail account sender for outgoing e-mails.
     * 
     * @return Email account
     */
    @AttributeDefinition(name = "%defaultFromMail.name", type = AttributeType.STRING, description = "%defaultFromMail.description")
    public String defaultFromMail();
	
    /**
     * Rejection email subject.
     * 
     * @return String
     */
    @AttributeDefinition(name = "%mailSubjectRejection.name", type = AttributeType.STRING, description = "%mailSubjectRejection.description")
    public String mailSubjectRejection();

    /**
     * Rejection email message.
     * 
     * @return String
     */
    @AttributeDefinition(name = "%mailMessageRejection.name", type = AttributeType.STRING, description = "%mailMessageRejection.description")
    public String mailMessageRejection();

    /**
     * Approval email subject.
     * 
     * @return String
     */
    @AttributeDefinition(name = "%mailSubjectApproval.name", type = AttributeType.STRING, description = "%mailSubjectApproval.description")
    public String mailSubjectApproval();

    /**
     * Approval email message.
     * 
     * @return String
     */
    @AttributeDefinition(name = "%mailMessageApproval.name", type = AttributeType.STRING, description = "%mailMessageApproval.description")
    public String mailMessageApproval();
	
    /**
     * Mail notifications switch.
     * 
     * @return Boolean
     */
    @AttributeDefinition(name = "%notificationsOn.name", type = AttributeType.BOOLEAN, description = "%notificationsOn.description")
    public boolean notificationsOn();
    
    /**
     * AD Group attributed to the process when it is created (Digitalization).
     * 
     * @return String
     */
    @AttributeDefinition(name = "%draftGroup.name", type = AttributeType.STRING, description = "%draftGroup.description")
    public String draftGroup();
    
    /**
     * AD Group attributed to the process when it is submitted to creation (Billing).
     * 
     * @return String
     */
    @AttributeDefinition(name = "%creationGroup.name", type = AttributeType.STRING, description = "%creationGroup.description")
    public String creationGroup();
    
    /**
     * Percentage limit to associate a Purchase Order to a Financial Document
     * 
     * @return Double
     */
    @AttributeDefinition(name = "%percentageLimitToAssociatePO.name", type = AttributeType.STRING, description = "%percentageLimitToAssociatePO.description")
    public String percentageLimitToAssociatePO();
    
    
	
}
