package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentDocumentFailedReadStreamException extends FinancialDocumentException {

    private static final long serialVersionUID = -6536364574772186973L;

    public FinancialDocumentDocumentFailedReadStreamException(final String errorCode, final Context ctx, final Object... args) {
	super(PlatformCode.INTERNAL_SERVER_ERROR, errorCode, ctx, args);
    }

    public static FinancialDocumentDocumentFailedReadStreamException of(final Context ctx, final Long instanceId) {
	return new FinancialDocumentDocumentFailedReadStreamException(AppProperties.FAILED_READ_STREAM_EXCEPTION, ctx, instanceId);
    }

}
