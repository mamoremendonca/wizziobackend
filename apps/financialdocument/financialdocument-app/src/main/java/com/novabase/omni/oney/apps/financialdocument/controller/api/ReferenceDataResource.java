package com.novabase.omni.oney.apps.financialdocument.controller.api;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;
import com.novabase.omni.oney.apps.financialdocument.dto.ReferenceDataDTO;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentFieldValidationException;

import io.digitaljourney.platform.modules.commons.type.HttpStatusCode;
import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.ApiKeyAuthDefinition.ApiKeyLocation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.BasicAuthDefinition;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;

//@formatter:off
/**
* Purchase Order Resource, this is only used for documentation purposes.
* *
*/
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path(AppProperties.ADDRESS_REFERENCE)
@SwaggerDefinition(
	securityDefinition = @SecurityDefinition(
		basicAuthDefinitions = {
			@BasicAuthDefinition(key = RSProperties.SWAGGER_BASIC_AUTH),
		},
		apiKeyAuthDefinitions = {
			@ApiKeyAuthDefinition(
				key = RSProperties.SWAGGER_BEARER_AUTH,
				name = RSProperties.HTTP_HEADER_API_KEY,
				in = ApiKeyLocation.HEADER)
			}
	),
	schemes = {
		SwaggerDefinition.Scheme.HTTP,
		SwaggerDefinition.Scheme.HTTPS,
		SwaggerDefinition.Scheme.DEFAULT
	},
	info = @Info(
		title = "Financial Document Reference Data API",
		description = "The Financial Document Reference Data API.",
		version = AppProperties.CURRENT_VERSION,
		license = @License(name = "Digital Journey License", url = "http://www.digitaljourney.io/license")
	),
	basePath = "bin/mvc.do/"+AppProperties.ADDRESS_REFERENCE
)
@Api(
	value = "Purchase Order Reference Data API",
	authorizations = {
		@Authorization(value = RSProperties.SWAGGER_BASIC_AUTH),
		@Authorization(value = RSProperties.SWAGGER_BEARER_AUTH)
	}
)
@ApiResponses(
	value = {
		@ApiResponse(code = HttpStatusCode.UNAUTHORIZED_CODE, message = RSProperties.SWAGGER_UNAUTHORIZED_MESSAGE),
		@ApiResponse(code = HttpStatusCode.FORBIDDEN_CODE, message = RSProperties.SWAGGER_FORBIDDEN_MESSAGE)
	}
)
//@formatter:on
public interface ReferenceDataResource {
    
    @POST
    @Path("/lists")
    @ApiOperation(value = "Get reference data", notes = "Retrieves reference data", response = ReferenceDataDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, responseContainer = "Map", message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public Map<String, List<ReferenceDataDTO>> getReferenceData(@ApiParam(value = "List of reference lists to be returned", required = true) final List<String> referenceList);

}
