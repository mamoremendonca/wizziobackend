package com.novabase.omni.oney.apps.financialdocument.enums;

import java.util.Arrays;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.FinancialDocumentTypeMs;

public enum FinancialDocumentType {

    INVOICE(FinancialDocumentTypeMs.INVOICE), 
    RETENTION_INVOICE(FinancialDocumentTypeMs.RETENTION_INVOICE), 
    CREDIT_NOTE(FinancialDocumentTypeMs.CREDIT_NOTE);
    
    private FinancialDocumentTypeMs msType;

    private FinancialDocumentType(final FinancialDocumentTypeMs msType) {
	this.msType = msType;
    }

    public FinancialDocumentTypeMs getMsType() {
	return msType;
    }
    
    public static FinancialDocumentType valueOf(FinancialDocumentTypeMs msType){
	
	return Arrays.asList(FinancialDocumentType.values()).stream().
								filter(type -> type.getMsType().equals(msType)).
								findFirst().
								orElse(null);
	
    }

}
