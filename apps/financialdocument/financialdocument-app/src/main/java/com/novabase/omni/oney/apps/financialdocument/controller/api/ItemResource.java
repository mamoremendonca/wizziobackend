package com.novabase.omni.oney.apps.financialdocument.controller.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.osgi.annotation.versioning.ProviderType;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.ItemDTO;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentFieldValidationException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentItemNotFoundException;

import io.digitaljourney.platform.modules.commons.type.HttpStatusCode;
import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.ApiKeyAuthDefinition.ApiKeyLocation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.BasicAuthDefinition;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;

//@formatter:off
@ProviderType
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path(AppProperties.ADDRESS_ITEM_CONTROLLER)
@SwaggerDefinition(
	securityDefinition = @SecurityDefinition(
		basicAuthDefinitions = {
			@BasicAuthDefinition(key = RSProperties.SWAGGER_BASIC_AUTH),
		},
		apiKeyAuthDefinitions = {
			@ApiKeyAuthDefinition(
				key = RSProperties.SWAGGER_BEARER_AUTH,
				name = RSProperties.HTTP_HEADER_API_KEY,
				in = ApiKeyLocation.HEADER)
			}
	),
	schemes = {
		SwaggerDefinition.Scheme.HTTP,
		SwaggerDefinition.Scheme.HTTPS,
		SwaggerDefinition.Scheme.DEFAULT
	},
	info = @Info(
		title = "Financial Documents Items API",
		description = "The Financial Documents Items API.",
		version = AppProperties.CURRENT_VERSION,
		license = @License(name = "Digital Journey License", url = "http://www.digitaljourney.io/license")
	),
	basePath = "bin/mvc.do/" + AppProperties.ADDRESS_ITEM_CONTROLLER
)
@Api(
	value = "Purchase Order API",
	authorizations = {
		@Authorization(value = RSProperties.SWAGGER_BASIC_AUTH),
		@Authorization(value = RSProperties.SWAGGER_BEARER_AUTH)
	}
)
@ApiResponses(
	value = {
		@ApiResponse(code = HttpStatusCode.UNAUTHORIZED_CODE, message = RSProperties.SWAGGER_UNAUTHORIZED_MESSAGE),
		@ApiResponse(code = HttpStatusCode.FORBIDDEN_CODE, message = RSProperties.SWAGGER_FORBIDDEN_MESSAGE)
	}
)
//@formatter:on
public interface ItemResource {
    
    @POST
    @Path("/")
    @ApiOperation(value = "Add an item ", notes = "Add a new item to the instance", response = FinancialDocumentDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = FinancialDocumentDTO.class, message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public ItemDTO addItem(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId,
	    		   @ApiParam(value = "The Item to be added to the instance", required = true) final ItemDTO item);
    
    @PUT
    @Path("/{itemIndex}")
    @ApiOperation(value = "Update Item", notes = "Updates an existing item", response = FinancialDocumentDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 404, response = FinancialDocumentItemNotFoundException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public void updateItem(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId,
	    		   @ApiParam(value = "The unique identifier of the item on the process instance that will be updated", required = true, example = "1") @PathParam("itemIndex") final int itemIndex,
	    		   @ApiParam(value = "The Item to be added to the instance", required = true) final ItemDTO item);

    @DELETE
    @Path("/{itemIndex}")
    @ApiOperation(value = "Remove Item", notes = "Removes an existing item", response = FinancialDocumentDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
	    		    @ApiResponse(code = 404, response = FinancialDocumentItemNotFoundException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public void removeItem(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId,
	    		   @ApiParam(value = "The unique identifier of the item on the process instance that will be removed", required = true, example = "1") @PathParam("itemIndex") final int itemIndex);

    @DELETE
    @Path("/all")
    @ApiOperation(value = "Remove all item", notes = "Removes all existing items", response = FinancialDocumentDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public void removeAllItems(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId);

}
