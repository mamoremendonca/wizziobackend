package com.novabase.omni.oney.apps.financialdocument.dto;

import java.time.LocalDateTime;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.financialdocument.enums.DocumentClassType;
import com.novabase.omni.oney.apps.financialdocument.json.serializer.NBLocalDateTimeSerializer;

public class AttachmentRepositoryDTO extends DTO {

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime date;
    private int index;
    private String mimeType;
    private String name;
    private Long blobId;
    private Long ownerId;
    private DocumentClassType documentClass;
    private String externalId;

    public AttachmentRepositoryDTO() {
	super();
    }

    public LocalDateTime getDate() {
	return date;
    }

    public void setDate(LocalDateTime date) {
	this.date = date;
    }

    public int getIndex() {
	return index;
    }

    public void setIndex(int index) {
	this.index = index;
    }

    public String getMimeType() {
	return mimeType;
    }

    public void setMimeType(String mimeType) {
	this.mimeType = mimeType;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public Long getBlobId() {
	return blobId;
    }

    public void setBlobId(Long blobId) {
	this.blobId = blobId;
    }

    public Long getOwnerId() {
	return ownerId;
    }

    public void setOwnerId(Long ownerId) {
	this.ownerId = ownerId;
    }

    public DocumentClassType getDocumentClass() {
	return documentClass;
    }

    public void setDocumentClass(DocumentClassType documentClass) {
	this.documentClass = documentClass;
    }

    public String getExternalId() {
	return externalId;
    }

    public void setExternalId(String externalId) {
	this.externalId = externalId;
    }

}
