/*-
 * #%L
 * Apps :: Purchase Order App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.financialdocument.agent.financialdocumentrepository;

import java.util.List;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.apps.financialdocument.AppContext;
import com.novabase.omni.oney.apps.financialdocument.AppProperties;
import com.novabase.omni.oney.apps.financialdocument.dto.AttachmentRepositoryDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentHeaderDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentRepositoryDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.TimeLineStepDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.WorkflowDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.Attachment;
import com.novabase.omni.oney.apps.financialdocument.entity.CurrentAction;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.mapper.FinancialDocumentRepositoryMapper;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.FinancialDocumentRepositoryResource;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.FinancialDocumentListMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.FinancialDocumentMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.SearchCriteriaMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.WorkflowActionMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.WorkflowMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.WorkFlowActionTypeMs;

import io.digitaljourney.platform.modules.commons.type.tuple.Pair;
import io.digitaljourney.platform.modules.mvc.api.agent.AbstractCoreAgent;

//@formatter:off
@Component(
	service = { Object.class, FinancialDocumentRepositoryAgentImpl.class },
	configurationPid = FinancialDocumentRepositoryAgentConfig.CPID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	reference = {
		@Reference(
			name = AppProperties.REF_CONTEXT,
			service = AppContext.class,
			cardinality = ReferenceCardinality.MANDATORY
		)
	}
)
@Designate(ocd = FinancialDocumentRepositoryAgentConfig.class)
// @formatter:on
public class FinancialDocumentRepositoryAgentImpl extends AbstractCoreAgent<AppContext, FinancialDocumentRepositoryAgentConfig> {

    @Reference
    private FinancialDocumentRepositoryResource financialDocumentRepositoryResource;

    /**
     * Method called whenever the component is activated.
     *
     * @param ctx    Component context
     * @param config Component configuration
     */
    @Activate
    public void activate(ComponentContext ctx, FinancialDocumentRepositoryAgentConfig config) {
	prepare(ctx, config);
    }

    /**
     * Method called whenever the component configuration changes.
     *
     * @param config Component configuration
     */
    @Modified
    public void modified(FinancialDocumentRepositoryAgentConfig config) {
	prepare(config);
    }

    public List<FinancialDocumentListMsDTO> search(final SearchCriteriaMsDTO searchCriteria) {
	return this.financialDocumentRepositoryResource.search(searchCriteria);
    }

    public Pair<FinancialDocumentHeaderDTO, WorkflowDTO> submitFinancialDocumentToRepository(FinancialDocumentInstance instance, WorkflowDTO workflow, String draftGroup) {

	FinancialDocumentMsDTO financialDocumentDTO = FinancialDocumentRepositoryMapper.INSTANCE.toFinancialDocumentMsDTO(instance, workflow, draftGroup);

	financialDocumentDTO = financialDocumentRepositoryResource.addFinancialDocument(financialDocumentDTO);
	// @formatter:off
	return new Pair<FinancialDocumentHeaderDTO, WorkflowDTO>(
			FinancialDocumentRepositoryMapper.INSTANCE.toFinancialDocumentHeaderDTO(financialDocumentDTO),
			FinancialDocumentRepositoryMapper.INSTANCE.toWorkflowDTO(financialDocumentDTO.getWorkflow()));
	// @formatter:on
    }

    public WorkflowDTO approveFinancialDocument(Long financialDocumentRepositoryId, CurrentAction action, Long userId) {
	return updateWorkFlow(financialDocumentRepositoryId, action, userId, WorkFlowActionTypeMs.APPROVE);
    }

    public WorkflowDTO returnFinancialDocument(Long financialDocumentRepositoryId, CurrentAction action, Long userId) {
	return updateWorkFlow(financialDocumentRepositoryId, action, userId, WorkFlowActionTypeMs.RETURN);
    }

    public WorkflowDTO rejectFinancialDocument(Long financialDocumentRepositoryId, CurrentAction action, Long userId) {
	return updateWorkFlow(financialDocumentRepositoryId, action, userId, WorkFlowActionTypeMs.REJECT);
    }

    public WorkflowDTO cancelFinancialDocument(Long financialDocumentRepositoryId, CurrentAction action, Long userId) {
	return updateWorkFlow(financialDocumentRepositoryId, action, userId, WorkFlowActionTypeMs.CANCEL);
    }

    public WorkflowDTO payFinancialDocument(Long financialDocumentRepositoryId, CurrentAction action, Long userId) {
	return updateWorkFlow(financialDocumentRepositoryId, action, userId, WorkFlowActionTypeMs.PAY);
    }

    public List<TimeLineStepDTO> getTimeLine(Long repositoryId) {
	return FinancialDocumentRepositoryMapper.INSTANCE.toTimeLineStepDTOList(financialDocumentRepositoryResource.getTimeLine(repositoryId));
    }

// TODO: when implement the Purchase Order versions we must pass here the
// version in order to on repository return the correct version.
    public FinancialDocumentRepositoryDTO getFinancialDocument(Long financialDocumentId, String userGroup) {

	return FinancialDocumentRepositoryMapper.INSTANCE.toFinancialDocumentRepositoryDTO(financialDocumentRepositoryResource.getFinancialDocument(financialDocumentId, userGroup));
    }

    public WorkflowDTO claimFinancialDocument(Long repositoryId, Long userId, String userGroup, CurrentAction action) {

	WorkflowActionMsDTO workflowAction = null;

	if (action != null && action.getAttachments() != null && action.getAttachments().isEmpty() == false) {
	    workflowAction = FinancialDocumentRepositoryMapper.INSTANCE.toWorkflowActionMsDTO(action);
	    workflowAction.setType(WorkFlowActionTypeMs.SKIP);
	}

	WorkflowMsDTO workFlow = financialDocumentRepositoryResource.claimProcess(repositoryId, userId, userGroup, workflowAction);
	return FinancialDocumentRepositoryMapper.INSTANCE.toWorkflowDTO(workFlow);
    }

    public List<Attachment> getFinancialDocumentAttachments(Long financialDocumentId) {

	return FinancialDocumentRepositoryMapper.INSTANCE.toAttachmentList(financialDocumentRepositoryResource.getFinancialDocumentAttachments(financialDocumentId));
    }

    public AttachmentRepositoryDTO getFinancialDocumentAttachment(Long financialDocumentId, int index) {
	return FinancialDocumentRepositoryMapper.INSTANCE.toAttachmentRepositoryDTO(this.financialDocumentRepositoryResource.getFinancialDocumentAttachment(financialDocumentId, index));
    }

    public FinancialDocumentRepositoryDTO getFinancialDocumentNotCanceledByNumber(final String number) {
	return FinancialDocumentRepositoryMapper.INSTANCE.toFinancialDocumentRepositoryDTO(financialDocumentRepositoryResource.getFinancialDocumentNotCanceledNotRejectedByNumber(number));
    }

    private WorkflowDTO updateWorkFlow(Long financialDocumentRepositoryId, CurrentAction action, Long userId, WorkFlowActionTypeMs actionType) {

	WorkflowActionMsDTO workflowAction = FinancialDocumentRepositoryMapper.INSTANCE.toWorkflowActionMsDTO(action);
	workflowAction.setType(actionType);
	workflowAction.setUserId(userId);

	WorkflowMsDTO workFlow = financialDocumentRepositoryResource.updateWorkflow(financialDocumentRepositoryId, workflowAction);

	return FinancialDocumentRepositoryMapper.INSTANCE.toWorkflowDTO(workFlow);
    }

}
