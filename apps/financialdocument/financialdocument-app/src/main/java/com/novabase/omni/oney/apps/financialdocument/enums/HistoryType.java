package com.novabase.omni.oney.apps.financialdocument.enums;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.TimeLineStepTypeMs;

public enum HistoryType {

    //@formatter:off
    DRAFT(TimeLineStepTypeMs.DRAFT),  
    SUBMITTED(TimeLineStepTypeMs.SUBMITTED), 
    APPROVED(TimeLineStepTypeMs.APPROVED), 
    RETURNED(TimeLineStepTypeMs.RETURNED), 
    REJECTED(TimeLineStepTypeMs.REJECTED), 
    CANCELED(TimeLineStepTypeMs.CANCELED);
    //@formatter:on

    private TimeLineStepTypeMs msType;

    private HistoryType(TimeLineStepTypeMs msType) {
	this.msType = msType;
    }

    public TimeLineStepTypeMs getMsType() {
	return msType;
    }

}
