package com.novabase.omni.oney.apps.financialdocument.validator;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.novabase.omni.oney.apps.financialdocument.AppContext;

/**
 * Validator template
 * 
 * @author NB24879
 *
 * @param <T>
 */
public interface UpdateValidator<T extends Object, U extends Object> {

    /**
     * This method will validate the target invoking the implemented method
     * getErrorFields, if this object have some error one exception will be throw.
     * 
     */
    public default void validate(T target, U source) {

	final List<String> errorFields = this.getErrorFields(target, source);
	if (errorFields != null && 
		errorFields.isEmpty() == false) {
	    throw this.getCtx().fieldValidationException(errorFields);
	}
	
    };

    public default boolean isBlank(final String str) {
	return StringUtils.isBlank(str);
    }
    
    public default boolean isInvalid(final String str) {
	return this.isBlank(str);
    }
    
    public default boolean isInvalid(final Long l) {
	return (l == null) || (l == 0L);
    }

    public List<String> getErrorFields(final T target, final U source);
    
    public AppContext getCtx();
    
}
