package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentNumberAlreadyExistsException extends FinancialDocumentException {

    private static final long serialVersionUID = 55246870938483328L;

    public FinancialDocumentNumberAlreadyExistsException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
    }

    public static FinancialDocumentNumberAlreadyExistsException of(Context ctx, Long instanceId, String number) {
	return new FinancialDocumentNumberAlreadyExistsException(AppProperties.FINANCIAL_DOCUMENT_NUMBER_ALREADY_EXISTS_EXCPETION, ctx, instanceId, number);
    }

}
