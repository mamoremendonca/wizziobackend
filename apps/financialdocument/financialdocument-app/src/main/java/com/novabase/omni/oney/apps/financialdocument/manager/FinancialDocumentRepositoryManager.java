package com.novabase.omni.oney.apps.financialdocument.manager;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.financialdocument.AppContext;
import com.novabase.omni.oney.apps.financialdocument.agent.core.CoreAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.agent.financialdocumentrepository.FinancialDocumentRepositoryAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentListDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentRepositoryDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentSearchCriteriaDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.TimeLineStepDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.CurrentAction;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.enums.FilterRepositoryAttribute;
import com.novabase.omni.oney.apps.financialdocument.enums.TimeLineStepType;
import com.novabase.omni.oney.apps.financialdocument.mapper.FinancialDocumentInstanceMapper;
import com.novabase.omni.oney.apps.financialdocument.mapper.FinancialDocumentRepositoryMapper;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.FinancialDocumentListMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.SearchCriteriaMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.OrderTypeMs;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.OrderType;

import io.digitaljourney.platform.modules.security.api.principal.PrincipalDTO;

@Component(service = { FinancialDocumentRepositoryManager.class })
public class FinancialDocumentRepositoryManager {

    @Reference
    private AppContext ctx;

    @Reference
    private CoreAgentImpl coreAgent;

    @Reference
    private CoreManager coreManager;

    @Reference
    private FinancialDocumentRepositoryAgentImpl financialDocumentRepositoryAgentImpl;

    public List<FinancialDocumentListDTO> searchFinancialDocuments(final FinancialDocumentSearchCriteriaDTO searchDTO, final FilterRepositoryAttribute orderFilter, final OrderType orderType,
	    final PrincipalDTO principal) {

	final SearchCriteriaMsDTO searchCriteriaMs = FinancialDocumentRepositoryMapper.INSTANCE.toSearchCriteriaMsDTO(searchDTO);

	// TODO: Review the FinancialDocument search
	final String currentUserGroup = this.coreManager.getUserGroup(principal.name);
	searchCriteriaMs.getScope().setCurrentUserId(principal.id);
	searchCriteriaMs.getScope().setCurrentUserGroup(currentUserGroup != null ? currentUserGroup : StringUtils.EMPTY);
	searchCriteriaMs.getOrderBy().setField(orderFilter.getFilterAttribute());
	searchCriteriaMs.getOrderBy().setType(OrderTypeMs.valueOf(orderType.getCode()));

	final List<FinancialDocumentListMsDTO> resultList = new ArrayList<>(this.financialDocumentRepositoryAgentImpl.search(searchCriteriaMs));
	return FinancialDocumentRepositoryMapper.INSTANCE.toFinancialDocumentListDTOList(resultList);

    }

    public FinancialDocumentRepositoryDTO getFinancialDocument(final Long financialDocumentId, final String userGroup) {

	final FinancialDocumentRepositoryDTO financialDocument = this.financialDocumentRepositoryAgentImpl.getFinancialDocument(financialDocumentId, userGroup);

	if (financialDocument == null) {
	    throw ctx.finanacialDocumentNotFoundException(financialDocumentId);
	}

	if (financialDocument.isCanClaim()) {
	    financialDocument.setCurrentInstanceId(this.coreAgent.findInstanceIdByRepositoryId(financialDocumentId));
	}

	// Double check
	if (financialDocument.getCurrentInstanceId() == null) {
	    financialDocument.setCanClaim(Boolean.FALSE);
	}

	return financialDocument;

    }

    /**
     * Get timeLine from repository without currentAction
     * 
     * @param repositoryId
     * @param userId
     * @return timeLine
     */
    public List<TimeLineStepDTO> getTimeLine(Long repositoryId, Long userId) {
	return getTimeLine(repositoryId, userId, "", null, null);
    }

    /**
     * Get instance timeLine
     * 
     * @param instance
     * @param userId
     * @return
     */
    public List<TimeLineStepDTO> getTimeLine(final FinancialDocumentInstance instance, final Long userId) {

	final List<TimeLineStepDTO> timeLine = new ArrayList<TimeLineStepDTO>();

	Long repositoryId = instance.getFinancialDocumentHeader().getRepositoryId();

	if (repositoryId == null || repositoryId.equals(0L) == true) {

	    TimeLineStepDTO draftStep = new TimeLineStepDTO();

	    draftStep.setIndex(1);
	    draftStep.setUserId(userId);
	    draftStep.setType(TimeLineStepType.DRAFT);
	    draftStep.setUserGroup(instance.getFinancialDocumentHeader().getUserGroup());
	    draftStep.setDate(instance.getFinancialDocumentHeader().getSubmitToCreationDate());
	    draftStep.setHasNext(true);
	    draftStep.setAttachments(new ArrayList<>());

	    TimeLineStepDTO creationStep = new TimeLineStepDTO();
	    creationStep.setIndex(2);
	    creationStep.setUserId(userId);
	    creationStep.setType(TimeLineStepType.SUBMITTED);
	    creationStep.setUserGroup(instance.getFinancialDocumentHeader().getUserGroup());
	    creationStep.setHasNext(true);
	    creationStep.setAttachments(new ArrayList<>());

	    if (instance.getHeader().status.equals("Draft")) {
		draftStep.setCurrentStep(Boolean.TRUE);
		creationStep.setCurrentStep(Boolean.FALSE);
		draftStep.getAttachments().addAll(FinancialDocumentInstanceMapper.INSTANCE.toAttachmentDTOList(instance.getCurrentAction().getAttachments()));
	    } else {
		draftStep.getAttachments().addAll(FinancialDocumentInstanceMapper.INSTANCE.toAttachmentDTOList(instance.getAttachments()));
		draftStep.setCurrentStep(Boolean.FALSE);
		creationStep.setCurrentStep(Boolean.TRUE);
		creationStep.getAttachments().addAll(FinancialDocumentInstanceMapper.INSTANCE.toAttachmentDTOList(instance.getCurrentAction().getAttachments()));
	    }

	    timeLine.add(draftStep);
	    timeLine.add(creationStep);
	}

	return getTimeLine(repositoryId, userId, instance.getHeader().status, instance.getCurrentAction(), timeLine);

    }

    private List<TimeLineStepDTO> getTimeLine(final Long repositoryId, final Long userId, final String status, final CurrentAction currentAction, List<TimeLineStepDTO> timeLine) {

	if (timeLine == null) {
	    timeLine = new ArrayList<>();
	}

	if (repositoryId != null && repositoryId.equals(0L) == false) {
	    timeLine.addAll(this.financialDocumentRepositoryAgentImpl.getTimeLine(repositoryId));
	}

	this.populateTimeLineCurrentAction(currentAction, timeLine);

	//@formatter:off
	// Verify if the current user is the owner of the attachments and 
	// if this is the current step in order to evaluate if the attachment can be deleted
	timeLine.stream()
		  .filter(step -> 
		  		(step.getType().equals(TimeLineStepType.TO_APPROVE) 
		  			|| (step.getType().equals(TimeLineStepType.DRAFT) && status.equals("Draft"))
		  			|| (step.getType().equals(TimeLineStepType.SUBMITTED) && status.equals("In Creation"))) 
		  		
		  		&& step.getAttachments() != null 
		  		&& step.getAttachments().isEmpty() == false)
		  .flatMap(step -> step.getAttachments().stream())
		  .forEach(attachment -> attachment.setCanBeDeletedByCurrentUser(attachment.getOwnerId().equals(userId)));
	
//	//populate the step user informations
	timeLine.stream()
		  .filter(step -> step.getUserId() != null)
		  .forEach(step -> step.setuserInformation(this.coreAgent.getUserInformations(step.getUserId())));
	
//	//populate the attachment user informations
	timeLine.stream()
		  .filter(step -> step.getAttachments() != null && step.getAttachments().isEmpty() == false)
		  .flatMap(step -> step.getAttachments().stream())
		  .forEach(attachment -> attachment.setuserInformation(this.coreAgent.getUserInformations(attachment.getOwnerId())));
//	//@formatter:on

	return timeLine;

    }

    public FinancialDocumentRepositoryDTO getFinancialDocumentNotCanceledNotRejectedByNumber(String number) {
	return financialDocumentRepositoryAgentImpl.getFinancialDocumentNotCanceledByNumber(number);
    }

    private void populateTimeLineCurrentAction(final CurrentAction currentAction, final List<TimeLineStepDTO> timeLine) {

	//@formatter:off
	// find the current action on timeline, is the first step without userId
	final TimeLineStepDTO currentActionTimeline = timeLine.stream().
								filter(step -> step.getUserId() == null).
								findFirst().
								orElse(null);

	// if the current action timeline is not null, we must set the attachments with
	// the attachments that are on instance currentAction
	if (currentActionTimeline != null && currentAction != null) {
	    
	    currentActionTimeline.setCurrentStep(Boolean.TRUE);
	    currentActionTimeline.setAttachments(FinancialDocumentInstanceMapper.INSTANCE.toAttachmentDTOList(currentAction.getAttachments()));
	    
	}
	//@formatter:on

    }

}
