package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppContext;
import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentUserCannotExecuteActionException extends FinancialDocumentException {

    /**
     * 
     */
    private static final long serialVersionUID = -3628813165104882534L;

    public FinancialDocumentUserCannotExecuteActionException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.AUTHORIZATION_ERROR, errorCode, ctx, args);
    }

    public static FinancialDocumentUserCannotExecuteActionException of(Context ctx, Long instanceId, String userName, String action) {
	return new FinancialDocumentUserCannotExecuteActionException(AppProperties.OWNER_ACTION_USER_CANNOT_EXECUTE_ACTION_EXCEPTION, ctx, instanceId, userName, action);
    }

    public static FinancialDocumentUserCannotExecuteActionException of(AppContext ctx, Long instanceId, String userName, String action, String instanceCurrentUserGroup, String sessionCurrentUserGroup) {
	return new FinancialDocumentUserCannotExecuteActionException(AppProperties.CURRENT_GROUP_ACTION_USER_CANNOT_EXECUTE_ACTION_EXCEPTION, ctx, instanceId, userName, action, instanceCurrentUserGroup, sessionCurrentUserGroup);
    }

}
