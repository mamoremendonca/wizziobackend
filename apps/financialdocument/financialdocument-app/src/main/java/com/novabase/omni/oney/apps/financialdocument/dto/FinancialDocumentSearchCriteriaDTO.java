package com.novabase.omni.oney.apps.financialdocument.dto;

import java.util.Date;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentStatus;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentType;
import com.novabase.omni.oney.apps.financialdocument.enums.SearchScope;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class FinancialDocumentSearchCriteriaDTO extends DTO {

    @ApiModelProperty
    private FinancialDocumentStatus status;

    @ApiModelProperty
    private FinancialDocumentType type;

    @ApiModelProperty
    private String number;

    @ApiModelProperty
    private Long purchaseOrderNumber;

    @ApiModelProperty
    private String entityCode;

    @ApiModelProperty
    private String departmentCode;

    @ApiModelProperty
    private String projectCode;

    @ApiModelProperty
    private String internalOrderCode;

    // TODO: Change back to creationDate and warn FE. 
    @ApiModelProperty(readOnly = true)
    private Date createdDate;

    @ApiModelProperty
    private Date expirationDate;

    @ApiModelProperty
    private Date finishDate;

    @ApiModelProperty
    private SearchScope scope;

    @ApiModelProperty
    private Long minVATValue;

    @ApiModelProperty
    private Long maxVATValue;

    public FinancialDocumentStatus getStatus() {
        return status;
    }

    public void setStatus(FinancialDocumentStatus status) {
        this.status = status;
    }

    public FinancialDocumentType getType() {
	return type;
    }

    public void setType(FinancialDocumentType type) {
	this.type = type;
    }

    public String getNumber() {
	return number;
    }

    public void setNumber(String number) {
	this.number = number;
    }

    public Long getPurchaseOrderNumber() {
	return purchaseOrderNumber;
    }

    public void setPurchaseOrderNumber(Long purchaseOrderNumber) {
	this.purchaseOrderNumber = purchaseOrderNumber;
    }

    public String getEntityCode() {
        return entityCode;
    }

    public void setEntityCode(String entityCode) {
        this.entityCode = entityCode;
    }

    public String getDepartmentCode() {
	return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
	this.departmentCode = departmentCode;
    }

    public String getProjectCode() {
	return projectCode;
    }

    public void setProjectCode(String projectCode) {
	this.projectCode = projectCode;
    }

    public String getInternalOrderCode() {
	return internalOrderCode;
    }

    public void setInternalOrderCode(String internalOrderCode) {
	this.internalOrderCode = internalOrderCode;
    }

    public Date getCreatedDate() {
	return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
	this.createdDate = createdDate;
    }

    public Date getExpirationDate() {
	return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
	this.expirationDate = expirationDate;
    }

    public SearchScope getScope() {
	return scope;
    }

    public void setScope(SearchScope scope) {
	this.scope = scope;
    }

    public Long getMinVATValue() {
	return minVATValue;
    }

    public void setMinVATValue(Long minVATValue) {
	this.minVATValue = minVATValue;
    }

    public Long getMaxVATValue() {
	return maxVATValue;
    }

    public void setMaxVATValue(Long maxVATValue) {
	this.maxVATValue = maxVATValue;
    }

    public Date getFinishDate() {
	return finishDate;
    }

    public void setFinishDate(Date finishDate) {
	this.finishDate = finishDate;
    }

}
