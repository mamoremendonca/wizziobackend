/*-
 * #%L
 * Apps :: Financial Document App  App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.financialdocument.controller.ri;

import java.util.List;

import javax.ws.rs.QueryParam;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.eclipse.gemini.blueprint.extensions.annotation.ServiceReference;
import org.osgi.service.component.annotations.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;
import com.novabase.omni.oney.apps.financialdocument.annotations.CurrentGroupAction;
import com.novabase.omni.oney.apps.financialdocument.annotations.OwnerAction;
import com.novabase.omni.oney.apps.financialdocument.controller.api.FinancialDocumentResource;
import com.novabase.omni.oney.apps.financialdocument.dto.DefaultDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentHeaderDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.TimeLineStepDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.WorkflowDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.manager.CoreManager;
import com.novabase.omni.oney.apps.financialdocument.manager.FinancialDocumentManager;
import com.novabase.omni.oney.apps.financialdocument.manager.FinancialDocumentRepositoryManager;
import com.novabase.omni.oney.apps.financialdocument.manager.WorkflowManager;
import com.novabase.omni.oney.apps.financialdocument.mapper.FinancialDocumentInstanceMapper;

import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.api.trigger.ActionMode;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyMethod;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyReference;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.session.JourneySession;

//@formatter:off
@Component
@RSProvider(value = AppProperties.ADDRESS_FINANCIAL_DOCUMENT_CONTROLLER)
@RestController
@RequestMapping(AppProperties.ADDRESS_FINANCIAL_DOCUMENT_CONTROLLER)
//@formatter:on
public class FinancialDocumentController extends AbstractAppController implements FinancialDocumentResource {

    // TODO criar scheduler para apagar Processos do tipo Financial Document que
    // foram
    // cancelados ou aprovados, isso por que ja temos eles registrados com todo o
    // historico no repositorio de D.Fs

    @ServiceReference
    private WorkflowManager workflowManager;

    @ServiceReference
    private FinancialDocumentManager financialDocumentManager;

    @ServiceReference
    private FinancialDocumentRepositoryManager financialDocumentRepositoryManager;

    @ServiceReference
    private CoreManager coreManager;

    @RequiresPermissions(AppProperties.PERMISSION_EXECUTE)
    @RequestMapping(value = "/init", method = RequestMethod.GET)
    public void init() {
	this.coreManager.createJourney();
    }

    @Override
    @JourneyMethod(value = "Create Process", mode = ActionMode.CREATE)
    @RequiresPermissions(AppProperties.PERMISSION_CREATE)
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public FinancialDocumentDTO createProcess() {

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	return this.financialDocumentManager.createProcess(instance, coreManager.getDraftGroup(), this.currentLoggedUserId());

    }

    // TODO: rever este metodo, não vai ser implementado na primeira fase
    @Override
    @JourneyMethod(value = "Create Process", mode = ActionMode.CREATE)
    @RequiresPermissions(AppProperties.PERMISSION_CREATE)
    @RequestMapping(value = "/copy/{repositoryId}", method = RequestMethod.GET)
    public FinancialDocumentDTO copyFinancialDocument(final Long repositoryId) {

	throw getCtx().exception("Not Implemented");

//	final FinancialDocumentRepositoryDTO financialDocument = this.financialDocumentRepositoryManager.getFinancialDocument(repositoryId, null);
//	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
//
//	FinancialDocumentInstanceMapper.INSTANCE.mergeFinancialDocumentHeaderDTO(instance, financialDocument.getFinancialDocumentHeader());
//	instance.getItems().clear();
//	instance.getItems().addAll(FinancialDocumentInstanceMapper.INSTANCE.toItemList(financialDocument.getItems()));
//
//	instance.getFinancialDocumentHeader().setUserGroup(coreManager.getDraftGroup());
//	instance.getFinancialDocumentHeader().setUserId(this.currentLoggedUserId());
//	instance.setCurrentAction(new CurrentAction());
//	instance.getCurrentAction().setStartDate(LocalDateTime.now());
//	instance.getWorkFlowAvailableActions().addAll(Arrays.asList(WorkFlowActionType.CANCEL)); // When the Financial Document is created the only action available is cancel.
//
//	if (instance.getHeader().targets == null) {
//	    instance.getHeader().targets = new HashMap<String, String>();
//	}
//
//	instance.refreshTargets();
//	instance.refreshTotal();
//
//	instance.getHeader().targets.put(TargetType.COPY_FROM.name(), repositoryId.toString());
//
//	return FinancialDocumentInstanceMapper.INSTANCE.toProcessDTO(instance);

    }

    @Override
    @OwnerAction
    @JourneyMethod(value = "Update Draft Header")
    @RequiresPermissions(AppProperties.PERMISSION_CREATE)
    @RequestMapping(value = "/{instanceId}/draft", method = RequestMethod.PUT)
    public FinancialDocumentDTO updateProcessHeaderDraft(@PathVariable @JourneyReference final long instanceId, @RequestBody final FinancialDocumentHeaderDTO financialDocumentHeaderDTO) {

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	return this.financialDocumentManager.updateProcessHeaderDraft(instance, financialDocumentHeaderDTO);

    }

    @Override
    @CurrentGroupAction // validação de estado vs API é responsabilidade da journey 
    @JourneyMethod(value = "Update Creation Header")
    @RequiresPermissions(AppProperties.PERMISSION_CREATE)
    @RequestMapping(value = "/{instanceId}/creation", method = RequestMethod.PUT)
    public FinancialDocumentDTO updateProcessHeaderCreation(@PathVariable @JourneyReference final long instanceId, @RequestBody final FinancialDocumentHeaderDTO financialDocumentHeaderDTO) {

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	return this.financialDocumentManager.updateProcessHeaderCreation(instance, financialDocumentHeaderDTO);

    }

    @Override
    @CurrentGroupAction // validação de estado vs API é responsabilidade da journey 
    @JourneyMethod(value = "Associate Purchase Order")
    @RequiresPermissions(AppProperties.PERMISSION_CREATE)
    @RequestMapping(value = "/{instanceId}/purchaseorder", method = RequestMethod.PUT)
    public FinancialDocumentDTO associatePurchaseOrder(@PathVariable @JourneyReference final long instanceId, @QueryParam("number") final String number) {

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	return this.financialDocumentManager.associatePurchaseOrder(instance, number);

    }

    @Override
    @CurrentGroupAction // validação de estado vs API é responsabilidade da journey 
    @JourneyMethod(value = "Associate Financial Document")
    @RequiresPermissions(AppProperties.PERMISSION_CREATE)
    @RequestMapping(value = "/{instanceId}/financialdocument", method = RequestMethod.PUT)
    public FinancialDocumentDTO associateFinancialDocument(@PathVariable @JourneyReference final long instanceId, @QueryParam("number") final String number) {
	final FinancialDocumentInstance instance = JourneySession.getInstance(this);

	return this.financialDocumentManager.associateFinancialDocument(instance, number);
    }

    @Override
    @JourneyMethod(value = "Get Process")
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    @RequestMapping(value = "/{instanceId}", method = RequestMethod.GET)
    public FinancialDocumentDTO getProcess(@PathVariable @JourneyReference final long instanceId) {
	return FinancialDocumentInstanceMapper.INSTANCE.toProcessDTO(JourneySession.getInstance(this));
    }

    @Override
    @OwnerAction
    @JourneyMethod(value = "Submit to Creation")
    @RequiresPermissions(AppProperties.PERMISSION_EXECUTE)
    @RequestMapping(value = "/{instanceId}/submit/creation", method = RequestMethod.PUT)
    public FinancialDocumentHeaderDTO submitToCreation(@PathVariable @JourneyReference final long instanceId) {

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	instance.getFinancialDocumentHeader().setDigitalizationUserId(currentLoggedUserId()); // We must keep the digitalization user id that create the instance in order to
											      // generate history on repository when submit this process to repository
	return this.financialDocumentManager.submitToCreation(instance, coreManager.getCreationGroup());
    }

    @Override
    @CurrentGroupAction // validação de estado vs API é responsabilidade da journey 
    @JourneyMethod(value = "Submit to Approval")
    @RequiresPermissions(AppProperties.PERMISSION_EXECUTE)
    @RequestMapping(value = "/{instanceId}/submit/approval", method = RequestMethod.PUT)
    public FinancialDocumentHeaderDTO submitToApproval(@PathVariable @JourneyReference final long instanceId) {

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	instance.getFinancialDocumentHeader().setUserId(currentLoggedUserId()); // The owner of the financial document is the user of the group "Contas a
										// Pagar", we must change the owner before submit to repository
	return this.workflowManager.generateWorkflowAndSubmit(instance);
    }

    @Override
    @CurrentGroupAction
    @JourneyMethod(value = "Approve")
    @RequiresPermissions(AppProperties.PERMISSION_EXECUTE)
    @RequestMapping(value = "/{instanceId}/approve", method = RequestMethod.PUT)
    public WorkflowDTO approveFinancialDocument(@PathVariable @JourneyReference final long instanceId, @RequestBody String comment) {

	// in order to accept null commentaries
	if (StringUtils.isBlank(comment)) {
	    comment = null;
	}

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	instance.getCurrentAction().setComment(comment);

	final WorkflowDTO workflowDTO = this.workflowManager.approveFinancialDocument(instance, this.currentLoggedUserId());

	instance.refreshTargets();

	return workflowDTO;

    }

    @Override
    @CurrentGroupAction
    @JourneyMethod(value = "Return")
    @RequiresPermissions(AppProperties.PERMISSION_EXECUTE)
    @RequestMapping(value = "/{instanceId}/return", method = RequestMethod.PUT)
    public WorkflowDTO returnFinancialDocument(@PathVariable @JourneyReference final long instanceId, @RequestBody final String comment) {

	// TODO: garantir que so rejeita se nao for o primeiro da cadeia de aprovação,
	// quando for feito claim tambem tem que garantir esse cenario
	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	instance.getCurrentAction().setComment(comment);

	final WorkflowDTO workflowDTO = this.workflowManager.returnFinancialDocument(instance, this.currentLoggedUserId());

	instance.refreshTargets();

	return workflowDTO;

    }

    @Override
    @CurrentGroupAction
    @JourneyMethod(value = "Reject")
    @RequiresPermissions(AppProperties.PERMISSION_EXECUTE)
    @RequestMapping(value = "/{instanceId}/reject", method = RequestMethod.PUT)
    public DefaultDTO rejectFinancialDocument(@PathVariable @JourneyReference final long instanceId, @RequestBody final String comment) {

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	instance.getCurrentAction().setComment(comment);

	final WorkflowDTO workflowDTO = this.workflowManager.rejectFinancialDocument(instance, this.currentLoggedUserId());

	instance.refreshTargets();

	return workflowDTO;

    }

    @Override
    @CurrentGroupAction // validação de estado vs API é responsabilidade da journey 
    @JourneyMethod(value = "Cancel API")
    @RequiresPermissions(AppProperties.PERMISSION_UPDATE)
    @RequestMapping(value = "/{instanceId}/cancel", method = RequestMethod.PUT)
    public void cancelFinancialDocument(@PathVariable @JourneyReference final long instanceId) {

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	if (instance.getFinancialDocumentHeader().getRepositoryId() != null && instance.getFinancialDocumentHeader().getRepositoryId().equals(0L) == false) {
	    this.workflowManager.cancelFinancialDocument(instance, currentLoggedUserId());
	}

	instance.refreshTargets();

    }

    @Override
    @JourneyMethod(value = "Get Timeline")
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    @RequestMapping(value = "/{instanceId}/timeline", method = RequestMethod.GET)
    public List<TimeLineStepDTO> getTimeLine(@PathVariable @JourneyReference final long instanceId) {

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	return this.financialDocumentRepositoryManager.getTimeLine(instance, this.currentLoggedUserId());

    }

    @Override
    @JourneyMethod(value = "Claim")
    @RequiresPermissions(AppProperties.PERMISSION_EXECUTE)
    @RequestMapping(value = "/{instanceId}/claim", method = RequestMethod.PUT)
    public void claimProcess(@PathVariable @JourneyReference final long instanceId) {

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	this.workflowManager.claimProcess(instance, this.currentLoggedUserId(), this.currentLoggedUserGroup());

	instance.refreshTargets();

    }

}
