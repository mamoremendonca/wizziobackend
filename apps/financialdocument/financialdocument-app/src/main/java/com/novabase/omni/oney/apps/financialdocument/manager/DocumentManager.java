package com.novabase.omni.oney.apps.financialdocument.manager;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartFile;

import com.novabase.omni.oney.apps.financialdocument.AppContext;
import com.novabase.omni.oney.apps.financialdocument.agent.blob.BlobAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.agent.core.CoreAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.dto.FileDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.Attachment;
import com.novabase.omni.oney.apps.financialdocument.entity.CurrentAction;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.enums.DocumentClassType;
import com.novabase.omni.oney.apps.financialdocument.mapper.FinancialDocumentInstanceMapper;
import com.novabase.omni.oney.apps.financialdocument.validator.UpdateDocumentValidator;

@Component(service = { DocumentManager.class })
public class DocumentManager {

    @Reference
    private AppContext ctx;

    @Reference
    private BlobAgentImpl blobAgentImpl;

    @Reference
    private CoreAgentImpl coreAgentImpl;
    
    @Reference
    private UpdateDocumentValidator updateDocumentValidator;
    
    public FinancialDocumentDTO saveDocument(final FinancialDocumentInstance instance, final MultipartFile document) {
	
	this.updateDocumentValidator.validate(document);

	InputStream inputStream = null;
	try {
	    inputStream = document.getInputStream();
	} catch (IOException e) {
	    throw this.ctx.attachmentFailedReadStreamException(instance.getHeader().instanceId);
	}
	
	instance.getFinancialDocumentHeader().setDocumentFileName(document.getOriginalFilename());
	instance.getFinancialDocumentHeader().setDocumentMimeType(document.getContentType());
	instance.getFinancialDocumentHeader().setDocumentId(this.saveDocument(instance, inputStream, DocumentClassType.FINANCIAL_DOCUMENT, document.getSize()));
	
	
	Attachment draftAttachment = new Attachment();
	draftAttachment.setBlobId(instance.getFinancialDocumentHeader().getDocumentId());
	draftAttachment.setMimeType(instance.getFinancialDocumentHeader().getDocumentMimeType());
	draftAttachment.setName(instance.getFinancialDocumentHeader().getDocumentFileName());
	draftAttachment.setDocumentClass(DocumentClassType.FINANCIAL_DOCUMENT);
	draftAttachment.setDate(LocalDateTime.now());
	draftAttachment.setIndex(1);
	draftAttachment.setOwnerId(instance.getFinancialDocumentHeader().getUserId());
	
	instance.getCurrentAction().getAttachments().add(draftAttachment);
	
	return FinancialDocumentInstanceMapper.INSTANCE.toProcessDTO(instance);
	
    }
    
    public Long saveDocument(final FinancialDocumentInstance instance, final InputStream documentStream, final DocumentClassType type, final long size) {

	if (instance.getCurrentAction() == null) {
	    instance.setCurrentAction(new CurrentAction());
	}

	Long documentId = 0L;
	try {
	    
	    // if blob already exists remove it
	    if (instance.getFinancialDocumentHeader().getDocumentId() > 0) {
		this.blobAgentImpl.removeFileFromBlob(instance.getFinancialDocumentHeader().getDocumentId());
	    }
	    
	    documentId = this.blobAgentImpl.saveBlob(documentStream, instance.getFinancialDocumentHeader().getDocumentFileName(), new Float(size));
	    
	} catch (Exception e) {
	    throw this.ctx.failedToUploadBlobException(instance.getHeader().instanceId, e.getMessage());
	}
	
	return documentId;
	
    }
    
    public byte[] getDocument(final FinancialDocumentInstance instance, final HttpServletResponse response) {
	
    	final FileDTO fileDTO = this.getDocument(instance);
        
    	response.addHeader("Content-Disposition", "attachment; filename=" + fileDTO.getFileName());
    	response.setContentType(fileDTO.getMimeType());
    
    	byte[] byteArray = null;
    	try {
    	    byteArray = StreamUtils.copyToByteArray(fileDTO.getInputStream());
    	} catch (IOException e) {
    	    throw this.ctx.attachmentFailedReadStreamException(instance.getHeader().instanceId);
    	}
    
    	return byteArray;
    	
    }
    
    public FileDTO getDocument(final FinancialDocumentInstance instance) {
	
	InputStream blobInputStream = null;
	try {
	    blobInputStream = this.blobAgentImpl.readFileFromBlob(instance.getFinancialDocumentHeader().getDocumentId());
	} catch (Exception e) {
	    throw ctx.failedToRetrieveBlobException(instance.getFinancialDocumentHeader().getDocumentId(), instance.getHeader().instanceId, e.getMessage());
	}

	final FileDTO fileDTO = new FileDTO();
	fileDTO.setInputStream(blobInputStream);
	fileDTO.setMimeType(instance.getFinancialDocumentHeader().getDocumentMimeType());
	fileDTO.setFileName(instance.getFinancialDocumentHeader().getDocumentFileName());

	return fileDTO;
	
    }
    
    
}
