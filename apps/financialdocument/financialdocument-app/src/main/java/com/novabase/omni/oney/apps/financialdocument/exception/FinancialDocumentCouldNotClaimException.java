package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentCouldNotClaimException extends FinancialDocumentException {

    private static final long serialVersionUID = 6242435970547510027L;

    public FinancialDocumentCouldNotClaimException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.AUTHORIZATION_ERROR, errorCode, ctx, args);
    }

    public static FinancialDocumentCouldNotClaimException of(Context ctx, Long instanceId, String userGroup) {
	return new FinancialDocumentCouldNotClaimException(AppProperties.COULD_NOT_CLAIM_EXCEPTION, ctx, instanceId, userGroup);
    }

}
