package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentFailedToRetrieveBlobException extends FinancialDocumentException {

    private static final long serialVersionUID = -244264750240314709L;

    public FinancialDocumentFailedToRetrieveBlobException(final String errorCode, final Context ctx, final Object... args) {
	super(PlatformCode.INTERNAL_SERVER_ERROR, errorCode, ctx, args);
    }

    public static FinancialDocumentFailedToRetrieveBlobException of(final Context ctx, final int identifier, final Long instanceId, final String message) {
	return new FinancialDocumentFailedToRetrieveBlobException(AppProperties.FAILED_TO_RETRIEVE_BLOB_EXCEPTION, ctx, identifier, instanceId, message);
    }

    public static FinancialDocumentFailedToRetrieveBlobException of(final Context ctx, final Long identifier, final Long instanceId, final String message) {
	return new FinancialDocumentFailedToRetrieveBlobException(AppProperties.FAILED_TO_RETRIEVE_BLOB_EXCEPTION, ctx, identifier, instanceId, message);
    }

}
