package com.novabase.omni.oney.apps.financialdocument.dto;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ReferenceDataDTO extends DTO {

    @ApiModelProperty
    private String code;

    @ApiModelProperty
    private String description;

    @ApiModelProperty
    private Boolean active;

    public ReferenceDataDTO() {
	
	super();
	
	this.active = Boolean.FALSE;
	
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
}
