package com.novabase.omni.oney.apps.financialdocument.dto;

import java.time.LocalDateTime;
import java.util.List;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.financialdocument.enums.TimeLineStepType;
import com.novabase.omni.oney.apps.financialdocument.json.serializer.NBLocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("TimeLineStep")
public class TimeLineStepDTO extends DTO {

    @ApiModelProperty(readOnly = true)
    private int index;

    @ApiModelProperty(readOnly = true)
    private TimeLineStepType type;

    @ApiModelProperty(readOnly = true)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime date;

    @ApiModelProperty(readOnly = true)
    private String comment;

    private transient Long userId;
    
    @ApiModelProperty(readOnly = true)
    private String ownerName;

    @ApiModelProperty(readOnly = true)
    private String userGroup;

    @ApiModelProperty(readOnly = true)
    private boolean hasNext;
    
    @ApiModelProperty(readOnly = true)
    private boolean isCurrentStep;

    @ApiModelProperty(readOnly = true)
    private List<AttachmentDTO> attachments;
    
    public TimeLineStepDTO() {
	super();
    }

    public void setuserInformation(UserInformationDTO userInformationDTO) {
	this.ownerName = userInformationDTO.getCompleteName();
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public TimeLineStepType getType() {
        return type;
    }

    public void setType(TimeLineStepType type) {
        this.type = type;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public boolean isHasNext() {
        return hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }

    public boolean isCurrentStep() {
        return isCurrentStep;
    }

    public void setCurrentStep(boolean isCurrentStep) {
        this.isCurrentStep = isCurrentStep;
    }

    public List<AttachmentDTO> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentDTO> attachments) {
        this.attachments = attachments;
    }

}
