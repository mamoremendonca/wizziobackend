package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentFailedToUploadBlobException extends FinancialDocumentException {

    private static final long serialVersionUID = -9106812614274743213L;

    public FinancialDocumentFailedToUploadBlobException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.INTERNAL_SERVER_ERROR, errorCode, ctx, args);
    }

    public static FinancialDocumentFailedToUploadBlobException of(Context ctx,Long instanceId, String message) {
	return new FinancialDocumentFailedToUploadBlobException(AppProperties.FAILED_TO_UPLOAD_BLOB_EXCEPTION, ctx, instanceId, message);
    }

}
