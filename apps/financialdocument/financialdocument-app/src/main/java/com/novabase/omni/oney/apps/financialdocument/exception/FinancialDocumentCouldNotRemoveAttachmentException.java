package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentCouldNotRemoveAttachmentException extends FinancialDocumentException {

    private static final long serialVersionUID = -7101687158595359865L;

    public FinancialDocumentCouldNotRemoveAttachmentException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.AUTHORIZATION_ERROR, errorCode, ctx, args);
    }

    public static FinancialDocumentCouldNotRemoveAttachmentException of(Context ctx, String userName, int attachmentIndex, Long instanceId) {
	return new FinancialDocumentCouldNotRemoveAttachmentException(AppProperties.USER_COULD_NOT_REMOVE_ATTACHMENT_EXCEPTION, ctx, userName, attachmentIndex, instanceId);
    }
    
    public static FinancialDocumentCouldNotRemoveAttachmentException ofStep(Context ctx, String userName, int attachmentIndex, Long instanceId) {
	return new FinancialDocumentCouldNotRemoveAttachmentException(AppProperties.STEP_COULD_NOT_REMOVE_ATTACHMENT_EXCEPTION, ctx, userName, attachmentIndex, instanceId);
    }

}
