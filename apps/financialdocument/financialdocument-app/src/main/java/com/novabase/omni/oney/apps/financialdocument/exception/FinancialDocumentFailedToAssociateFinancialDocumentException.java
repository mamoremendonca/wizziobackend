package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentFailedToAssociateFinancialDocumentException extends FinancialDocumentException {

    private static final long serialVersionUID = -5983077997753786514L;

    public FinancialDocumentFailedToAssociateFinancialDocumentException(final String errorCode, final Context ctx, final Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
    }

    public static FinancialDocumentFailedToAssociateFinancialDocumentException of(final Context ctx, final String identifier, final Long instanceId) {
	return new FinancialDocumentFailedToAssociateFinancialDocumentException(AppProperties.FAILED_TO_ASSOCIATE_FINANCIAL_DOCUMENT_EXCEPTION, ctx, identifier, instanceId);
    }

}
