package com.novabase.omni.oney.apps.financialdocument.enums;

import java.util.Arrays;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.DocumentClassTypeMs;

public enum DocumentClassType {

    //@formatter:off
    FINANCIAL_DOCUMENT(DocumentClassTypeMs.FINANCIAL_DOCUMENT), 
    FINANCIAL_DOCUMENT_ATTACHMENT(DocumentClassTypeMs.FINANCIAL_DOCUMENT_ATTACHMENT); 
    //@formatter:on

    private DocumentClassTypeMs msDocumentClass;

    private DocumentClassType(DocumentClassTypeMs msDocumentClass) {
	this.msDocumentClass = msDocumentClass;
    }

    public DocumentClassTypeMs getMsDocumentClass() {
	return msDocumentClass;
    }

    //@formatter:off
    public static DocumentClassType valueOf(DocumentClassTypeMs msDocumentClass){
	return Arrays.asList(DocumentClassType.values())
		.stream()
		.filter(type -> type.getMsDocumentClass().equals(msDocumentClass))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on

}
