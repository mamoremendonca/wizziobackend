package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentTotalValuePOException extends FinancialDocumentException {

    private static final long serialVersionUID = -3515617314437992853L;

    public FinancialDocumentTotalValuePOException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
    }

    public static FinancialDocumentTotalValuePOException of(Context ctx, Long instanceId, Double financialDocumentTotalWithoutTax, Double purchaseOrderTotalWithoutTax, Double percentage) {
	return new FinancialDocumentTotalValuePOException(AppProperties.TOTAL_VALUE_ASSOCIATED_PO_EXCEPTION, ctx, instanceId, financialDocumentTotalWithoutTax, purchaseOrderTotalWithoutTax,
		percentage);
    }

}
