package com.novabase.omni.oney.apps.financialdocument.enums;

public enum WorkFlowActionType {

    // TODO: Revise Workflow Action Types
    //@formatter:off
    APPROVE, 
    RETURN, 
    REJECT, 
    CANCEL;
    //@formatter:on

}
