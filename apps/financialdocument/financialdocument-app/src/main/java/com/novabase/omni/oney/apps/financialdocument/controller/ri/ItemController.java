package com.novabase.omni.oney.apps.financialdocument.controller.ri;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.eclipse.gemini.blueprint.extensions.annotation.ServiceReference;
import org.osgi.service.component.annotations.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;
import com.novabase.omni.oney.apps.financialdocument.annotations.CurrentGroupAction;
import com.novabase.omni.oney.apps.financialdocument.annotations.OwnerAction;
import com.novabase.omni.oney.apps.financialdocument.controller.api.ItemResource;
import com.novabase.omni.oney.apps.financialdocument.dto.ItemDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.manager.ItemManager;

import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyMethod;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyReference;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.session.JourneySession;

@Component
@RSProvider(value=AppProperties.SERVICE_ADDRESS_ITEM_CONTROLLER) 
@RestController
@RequestMapping(AppProperties.ADDRESS_ITEM_CONTROLLER)
public class ItemController extends AbstractAppController implements ItemResource {

    @ServiceReference
    private ItemManager itemManager;
    
    @CurrentGroupAction // validação de estado vs API é responsabilidade da journey 
    @RequestMapping(path = "/", method = RequestMethod.POST)
    @JourneyMethod(value = "Add Item")
    @RequiresPermissions(AppProperties.PERMISSION_UPDATE)
    public ItemDTO addItem(@PathVariable @JourneyReference final long instanceId, @RequestBody final ItemDTO item) {

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	return this.itemManager.addItem(instance, item);
	
    }

    @CurrentGroupAction // validação de estado vs API é responsabilidade da journey 
    @RequestMapping(path = "/{itemIndex}", method = RequestMethod.PUT)
    @JourneyMethod(value = "Update Item")
    @RequiresPermissions(AppProperties.PERMISSION_UPDATE)
    public void updateItem(@PathVariable @JourneyReference final long instanceId, @PathVariable final int itemIndex, @RequestBody final ItemDTO item) {

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	this.itemManager.updateItem(instance, itemIndex, item);
	
    }

    @CurrentGroupAction // validação de estado vs API é responsabilidade da journey 
    @RequestMapping(path = "/{itemIndex}", method = RequestMethod.DELETE)
    @JourneyMethod(value = "Remove Item")
    @RequiresPermissions(AppProperties.PERMISSION_UPDATE)
    public void removeItem(@PathVariable @JourneyReference final long instanceId, @PathVariable final int itemIndex) {

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	this.itemManager.removeItem(instance, itemIndex);
	
    }

    @CurrentGroupAction // validação de estado vs API é responsabilidade da journey 
    @RequestMapping(path = "/all", method = RequestMethod.DELETE)
    @JourneyMethod(value = "Remove Item")
    @RequiresPermissions(AppProperties.PERMISSION_UPDATE)
    public void removeAllItems(@PathVariable @JourneyReference final long instanceId) {
	
	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	this.itemManager.removeAllItem(instance);
	
    }

}
