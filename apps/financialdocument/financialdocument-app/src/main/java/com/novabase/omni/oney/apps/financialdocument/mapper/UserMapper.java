package com.novabase.omni.oney.apps.financialdocument.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.apps.financialdocument.dto.UserInformationDTO;

import io.digitaljourney.platform.plugins.modules.uam.service.api.dto.UserDTO;

@Mapper
public interface UserMapper {

    public static final UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
    
    @Mapping(source = "name", target = "userName")
    public UserInformationDTO toUserInformationDTO(UserDTO userDTO);
    
}
