package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentAttachmentNotFoundException extends FinancialDocumentException {

    private static final long serialVersionUID = -4455977178519122903L;

    public FinancialDocumentAttachmentNotFoundException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.NOT_FOUND, errorCode, ctx, args);
    }

    public static FinancialDocumentAttachmentNotFoundException of(Context ctx, int attachmentIndex, Long instanceId) {
	return new FinancialDocumentAttachmentNotFoundException(AppProperties.ATTACHMENT_NOT_FOUND_EXCEPTION, ctx, attachmentIndex, instanceId);
    }

}
