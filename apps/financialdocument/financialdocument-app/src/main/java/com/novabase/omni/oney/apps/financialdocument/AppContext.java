/*-
 * #%L
 * Apps :: Financial Document App  App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.financialdocument;

import java.util.Date;
import java.util.List;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.apps.financialdocument.agent.core.CoreAgentConfig;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentAttachmentFailedReadStreamException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentAttachmentNotFoundException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentCouldNotClaimException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentCouldNotRemoveAttachmentException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentFailedToAssociateFinancialDocumentException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentFailedToAssociatePurchaseOrderException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentFailedToRemoveBlobException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentFailedToRetrieveBlobException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentFailedToRetrieveFileECMServiceException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentFailedToUploadBlobException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentFieldValidationException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentInvalidMediaTypeException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentItemNotFoundException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentNotFoundException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentNumberAlreadyExistsException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentPaidException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentTotalValuePOException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentUserCannotExecuteActionException;

import io.digitaljourney.platform.modules.async.api.PlatformAsyncManager;
import io.digitaljourney.platform.modules.cache.api.PlatformCacheManager;
import io.digitaljourney.platform.modules.commons.annotation.DocumentationResource;
import io.digitaljourney.platform.modules.invocation.api.PlatformInvocationManager;
import io.digitaljourney.platform.modules.invocation.api.model.EventLog;
import io.digitaljourney.platform.modules.invocation.api.model.type.StatusType;
import io.digitaljourney.platform.modules.security.api.PlatformSecurityManager;
import io.digitaljourney.platform.modules.security.api.SystemSecurityManager;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.api.JourneyWorkflowEngineResource;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.context.AbstractJourneyContext;

// @formatter:off
/**
 * Financial Document App App implementation of an {@link AbstractJourneyContext
 * Journey Context}.
 * 
 */
@Component(service = { Object.class, AppContext.class }, reference = {
		@Reference(name = AppProperties.REF_JOURNEY_ENGINE, service = JourneyWorkflowEngineResource.class, cardinality = ReferenceCardinality.MANDATORY),
		@Reference(name = AppProperties.REF_PLATFORM_SECURITY_MANAGER, service = PlatformSecurityManager.class, policy = ReferencePolicy.DYNAMIC, cardinality = ReferenceCardinality.OPTIONAL),
		@Reference(name = AppProperties.REF_SYSTEM_SECURITY_MANAGER, service = SystemSecurityManager.class, cardinality = ReferenceCardinality.MANDATORY),
		@Reference(name = AppProperties.REF_PLATFORM_INVOCATION_MANAGER, service = PlatformInvocationManager.class, cardinality = ReferenceCardinality.MANDATORY),
		@Reference(name = AppProperties.REF_ASYNC_MANAGER, service = PlatformAsyncManager.class, cardinality = ReferenceCardinality.MANDATORY),
		@Reference(name = AppProperties.REF_CACHE_MANAGER, service = PlatformCacheManager.class, cardinality = ReferenceCardinality.MANDATORY) })
@DocumentationResource(AppProperties.DOCS_ADDRESS)
@Designate(ocd = CoreAgentConfig.class)
// @formatter:on
public class AppContext extends AbstractJourneyContext {

    /**
     * Method called whenever the component is activated.
     *
     * @param ctx Component context
     */
    @Activate
    public void activate(ComponentContext ctx) {
	prepare(ctx);
    }

    public void logError(String message) {

	final EventLog event = new EventLog();
	event.setEvent_message(message);
	event.setStart_event_time(new Date());

	this.getPlatformInvocationManager().logNewEvent(event, null, StatusType.ERROR);
	this.getLogger().error(message);

    }

    public void logInfo(String message) {

	final EventLog event = new EventLog();
	event.setEvent_message(message);
	event.setStart_event_time(new Date());

	this.getPlatformInvocationManager().logNewEvent(event, null, StatusType.SUCCESS);
	this.getLogger().info(message);

    }

    /**
     * Creates a new Financial Document App Exception (500 - Internal Server Error)
     * with the given error message.
     *
     * @param message Error message
     * @return Created exception
     */
    public FinancialDocumentException exception(String message) {
	return FinancialDocumentException.of(this, message);
    }

    /**
     * Creates a new Financial Document App Exception (500 - Internal Server Error)
     * with the given error cause.
     *
     * @param cause Error cause
     * @return Created exception
     */
    public FinancialDocumentException exception(Throwable cause) {
	return FinancialDocumentException.of(this, cause);
    }

    public FinancialDocumentAttachmentNotFoundException attachmentNotFoundException(int attachmentIndex, Long instanceId) {
	return FinancialDocumentAttachmentNotFoundException.of(this, attachmentIndex, instanceId);
    }

    public FinancialDocumentFailedToRemoveBlobException failedToRemoveBlobException(int attachmentIndex, Long instanceId, String message) {
	return FinancialDocumentFailedToRemoveBlobException.of(this, attachmentIndex, instanceId, message);
    }

    public FinancialDocumentFailedToUploadBlobException failedToUploadBlobException(final Long instanceId, final String message) {
	return FinancialDocumentFailedToUploadBlobException.of(this, instanceId, message);
    }

    public FinancialDocumentFailedToRetrieveBlobException failedToRetrieveBlobException(final int attachmentIndex, final Long instanceId, final String message) {
	return FinancialDocumentFailedToRetrieveBlobException.of(this, attachmentIndex, instanceId, message);
    }

    public FinancialDocumentFailedToRetrieveBlobException failedToRetrieveBlobException(final Long identifier, final Long instanceId, final String message) {
	return FinancialDocumentFailedToRetrieveBlobException.of(this, identifier, instanceId, message);
    }

    public FinancialDocumentCouldNotRemoveAttachmentException user_couldNotRemoveAttachmentException(final String userName, final int attachmentIndex, final Long instanceId) {
	return FinancialDocumentCouldNotRemoveAttachmentException.of(this, userName, attachmentIndex, instanceId);
    }

    public FinancialDocumentFailedToAssociatePurchaseOrderException failedToAssociatePurchaseOrderException(final String identifier, final Long instanceId) {
	return FinancialDocumentFailedToAssociatePurchaseOrderException.of(this, identifier, instanceId);
    }

    public FinancialDocumentFailedToAssociateFinancialDocumentException failedToAssociateFinancialDocumentException(final String identifier, final Long instanceId) {
	return FinancialDocumentFailedToAssociateFinancialDocumentException.of(this, identifier, instanceId);
    }

    public FinancialDocumentInvalidMediaTypeException invalidDocumentTypeException(final String documentName, final String sent, final String expected) {
	return FinancialDocumentInvalidMediaTypeException.of(this, documentName, sent, expected);
    }

    public FinancialDocumentCouldNotRemoveAttachmentException step_couldNotRemoveAttachmentException(final String userName, final int attachmentIndex, final Long instanceId) {
	return FinancialDocumentCouldNotRemoveAttachmentException.ofStep(this, userName, attachmentIndex, instanceId);
    }

    public FinancialDocumentAttachmentFailedReadStreamException attachmentFailedReadStreamException(Long instanceId) {
	return FinancialDocumentAttachmentFailedReadStreamException.of(this, instanceId);
    }

    public FinancialDocumentItemNotFoundException itemNotFoundException(int index, Long instanceId) {
	return FinancialDocumentItemNotFoundException.of(this, index, instanceId);
    }

    public FinancialDocumentFieldValidationException fieldValidationException(List<String> errorFields) {
	return FinancialDocumentFieldValidationException.of(this, errorFields);
    }

    public FinancialDocumentPaidException paidNotFoundException(String financialDocumentNumber) {
	return FinancialDocumentPaidException.ofNotFound(this, financialDocumentNumber);
    }

    public FinancialDocumentPaidException paidStatusException(String financialDocumentNumber, String status) {
	return FinancialDocumentPaidException.ofStatus(this, financialDocumentNumber, status);
    }

    public FinancialDocumentNotFoundException finanacialDocumentNotFoundException(Long financialDocumentId) {
	return FinancialDocumentNotFoundException.of(this, financialDocumentId);
    }

    public FinancialDocumentNumberAlreadyExistsException financialDocumentNumberAlreadyExistsException(Long instanceId, String number) {
	return FinancialDocumentNumberAlreadyExistsException.of(this, instanceId, number);
    }

    public FinancialDocumentTotalValuePOException financialDocumentTotalValuePOException(Long instanceId, Double financialDocumentTotalWithoutTax, Double purchaseOrderTotalWithoutTax, Double percentage) {
   	return FinancialDocumentTotalValuePOException.of(this, instanceId, financialDocumentTotalWithoutTax, purchaseOrderTotalWithoutTax, percentage);
       }

    public FinancialDocumentFailedToRetrieveFileECMServiceException failedToRetrieveFileECMException(int attachmentIndex, Long financialDocumentId, String message) {
	return FinancialDocumentFailedToRetrieveFileECMServiceException.of(this, attachmentIndex, financialDocumentId, message);
    }

    public FinancialDocumentCouldNotClaimException couldNotClaimException(Long instanceId, String userGroup) {
	return FinancialDocumentCouldNotClaimException.of(this, instanceId, userGroup);
    }
    
    public FinancialDocumentUserCannotExecuteActionException userCannotExecuteActionException(Long instanceId, String userName, String action) {
	return FinancialDocumentUserCannotExecuteActionException.of(this, instanceId, userName, action);
    }

    public FinancialDocumentUserCannotExecuteActionException userCannotExecuteActionException(Long instanceId, String userName, String action, String instanceCurrentUserGroup,
	    String sessionCurrentUserGroup) {
	return FinancialDocumentUserCannotExecuteActionException.of(this, instanceId, userName, action, instanceCurrentUserGroup, sessionCurrentUserGroup);
    }
    
}
