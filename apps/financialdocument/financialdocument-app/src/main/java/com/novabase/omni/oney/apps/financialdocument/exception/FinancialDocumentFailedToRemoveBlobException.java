package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentFailedToRemoveBlobException extends FinancialDocumentException {

    private static final long serialVersionUID = -3328724774019201311L;

    public FinancialDocumentFailedToRemoveBlobException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.INTERNAL_SERVER_ERROR, errorCode, ctx, args);
    }

    public static FinancialDocumentFailedToRemoveBlobException of(Context ctx, int attachmentIndex, Long instanceId, String message) {
	return new FinancialDocumentFailedToRemoveBlobException(AppProperties.FAILED_TO_REMOVE_BLOB_EXCEPTION, ctx, attachmentIndex, instanceId, message);
    }

}
