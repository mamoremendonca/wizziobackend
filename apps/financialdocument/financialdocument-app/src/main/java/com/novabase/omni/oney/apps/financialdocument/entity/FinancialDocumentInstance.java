package com.novabase.omni.oney.apps.financialdocument.entity;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.novabase.omni.oney.apps.financialdocument.enums.WorkFlowActionType;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.TargetType;

import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.session.JourneyInstance;

public class FinancialDocumentInstance extends JourneyInstance {

    private FinancialDocumentHeader financialDocumentHeader; // o nome é financialDocumentHeader porque temos na classe extendida

    // "JourneyInstance" um atributo chamado header
    private List<Item> items;

    /**
     * The current action on instance, when some action that are persisted on
     * repository is done, the data on this object is send to the repository and
     * this object became new again to the next action.
     */
    private CurrentAction currentAction;

    /**
     * Attachments that already are on PurchaseOrderRepository
     */
    private List<Attachment> attachments;

    /**
     * List of available actions on current status and workflow position.
     */
    private List<WorkFlowActionType> workFlowAvailableActions;

    public FinancialDocumentInstance() {

	super();

	this.items = new ArrayList<Item>();
	this.currentAction = new CurrentAction();
	this.attachments = new ArrayList<Attachment>();
	this.workFlowAvailableActions = new ArrayList<WorkFlowActionType>();
	this.financialDocumentHeader = new FinancialDocumentHeader();

    }

    //@formatter:off
    public void refreshTotal() {
	
	if(this.financialDocumentHeader == null) {
	    this.financialDocumentHeader = new FinancialDocumentHeader();
	}
	
	if(this.items == null) {
	    this.items = new ArrayList<>();
	}
	
	this.financialDocumentHeader.setTotalWithoutTax(
		this.items.stream()
        		.filter(item -> item != null)
        		.map(item -> item.getAmount() * item.getUnitPriceWithoutTax())
        		.reduce(0d, Double::sum));
	
    }
    //@formatter:on

    /**
     * We must refresh the targets on the process instance in order to obtain this
     * fields on worklist filter, the payload is not accessible on search methods of
     * processcontinuity. All fields that are on the instance and does not is a
     * default field on JourneyInstance must be on target to be accessible to get on
     * search methods.
     */
    public void refreshTargets() {
	refreshTargets(true);
    }

    /**
     * We must refresh the targets on the process instance in order to obtain this
     * fields on worklist filter, the payload is not accessible on search methods of
     * processcontinuity. All fields that are on the instance and does not is a
     * default field on JourneyInstance must be on target to be accessible to get on
     * search methods.
     */
    public void refreshTargets(boolean includeFriendlyNumber) {

	final Map<String, String> headerTargets = this.getHeader().targets;

//	CREATION_USER_ID
	final String creationUserId = this.financialDocumentHeader != null && financialDocumentHeader.getUserId() != null ? this.financialDocumentHeader.getUserId().toString() : StringUtils.EMPTY;
	headerTargets.put(TargetType.CREATION_USER_ID.name(), creationUserId);

//      CURRENT_GROUP
	final String currentGroup = this.currentAction != null && currentAction.getUserGroup() != null ? this.currentAction.getUserGroup() : StringUtils.EMPTY;
	final String createByGroup = this.financialDocumentHeader != null && financialDocumentHeader.getUserGroup() != null ? this.financialDocumentHeader.getUserGroup() : StringUtils.EMPTY;

	if (currentGroup.isEmpty() == false) {

	    headerTargets.put(TargetType.CURRENT_GROUP.name(), currentGroup);
	} else {

	    // The CURRENT_GROUP target is set when the FinancialDocument is on the
	    // Approval workflow with the currentGroup, if the FinancialDocument status is
	    // Draft or Rejected
	    // the CURRENT_GROUP must be set with the createByGroup
	    headerTargets.put(TargetType.CURRENT_GROUP.name(), createByGroup);
	}

//	NUMBER
	final String number = this.financialDocumentHeader.getNumber() != null ? this.financialDocumentHeader.getNumber() : StringUtils.EMPTY;
	headerTargets.put(TargetType.NUMBER.name(), number);

	if (includeFriendlyNumber) {
	    headerTargets.put(TargetType.FRIENDLY_NUMBER.name(), number);
	}

// 	ENTITY_CODE
	final String supplierCode = this.financialDocumentHeader.getSupplierCode() != null ? this.financialDocumentHeader.getSupplierCode() : StringUtils.EMPTY;
	headerTargets.put(TargetType.ENTITY_CODE.name(), supplierCode);

//	VALUE
	final Double totalWithoutTax = this.financialDocumentHeader.getTotalWithoutTax() != null ? this.financialDocumentHeader.getTotalWithoutTax() : 0;
	headerTargets.put(TargetType.VALUE.name(), TargetType.formatValue(totalWithoutTax));

//	DEPARTMENT_CODE
	final String departmentCode = this.financialDocumentHeader.getDepartmentCode() != null ? this.financialDocumentHeader.getDepartmentCode() : StringUtils.EMPTY;
	headerTargets.put(TargetType.DEPARTMENT_CODE.name(), departmentCode);

	//@formatter:off

	final String repositoryId = this.financialDocumentHeader.getRepositoryId() != null 
			? this.financialDocumentHeader.getRepositoryId().toString()
			: "";

	headerTargets.put(TargetType.REPOSITORY_ID.name(), repositoryId);

	final String expirationDate = this.financialDocumentHeader.getExpirationDate() != null 
        		? DateTimeFormatter
               			.ofPattern("yyyy-MM-dd")
               			.format(this.financialDocumentHeader.getExpirationDate()) 
        		: "";
        headerTargets.put(TargetType.EXPIRATION_DATE.name(), expirationDate);
	//@formatter:on

    }

    public FinancialDocumentHeader getFinancialDocumentHeader() {
	return financialDocumentHeader;
    }

    public void setFinancialDocumentHeader(FinancialDocumentHeader financialDocumentHeader) {
	this.financialDocumentHeader = financialDocumentHeader;
    }

    public List<Item> getItems() {
	return items;
    }

    public void setItems(List<Item> items) {
	this.items = items;
    }

    public CurrentAction getCurrentAction() {
	return currentAction;
    }

    public void setCurrentAction(CurrentAction currentAction) {
	this.currentAction = currentAction;
    }

    public List<Attachment> getAttachments() {
	return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
	this.attachments = attachments;
    }

    public List<WorkFlowActionType> getWorkFlowAvailableActions() {
	return workFlowAvailableActions;
    }

    public void setWorkFlowAvailableActions(List<WorkFlowActionType> workFlowAvailableActions) {
	this.workFlowAvailableActions = workFlowAvailableActions;
    }

}
