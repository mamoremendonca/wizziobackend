package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentInvalidMediaTypeException extends FinancialDocumentException {

    private static final long serialVersionUID = 3920727057728868962L;

    public FinancialDocumentInvalidMediaTypeException(final String errorCode, final Context ctx, final Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
    }

    public static FinancialDocumentInvalidMediaTypeException of(final Context ctx, final String documentName, final String sent, final String expected) {
	return new FinancialDocumentInvalidMediaTypeException(AppProperties.INVALID_MEDIA_TYPE, ctx, documentName, sent, expected);
    }
    
}
