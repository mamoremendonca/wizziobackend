package com.novabase.omni.oney.apps.financialdocument.enums;

import com.novabase.omni.oney.modules.oneycommon.service.api.enums.ReferenceDataEnum;

public enum FilterRepositoryAttribute implements ReferenceDataEnum {

    //@formatter:off
    STATUS("status"),
    TYPE("type"),
    SUPPLIER("supplierCode"),
    INTERNAL_ORDER("internalOrderCode"),
    DEPARTMENT("departmentCode"),
    PROJECT("projectCode"),
    NUMBER("number"),
    CREATION_DATE("creationDate"), 
    APPROVAL_DATE("approvalDate"),
    EXPIRATION_DATE("expirationDate"),
    USER_GROUP("workflow.currentUserGroup"),
    PO_NUMBER("purchaseOrderFriendlyNumber"),
    TOTAL("totalWithoutTax");
    //@formatter:on

    private String filterAttribute;

    private FilterRepositoryAttribute(String filterAttribute) {
	this.filterAttribute = filterAttribute;
    }

    public String getFilterAttribute() {
        return filterAttribute;
    }

    @Override
    public String getDescription() {
	return this.name();
    }
}
