package com.novabase.omni.oney.apps.financialdocument.controller.api;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentListDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentRepositoryDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentSearchCriteriaDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.TimeLineStepDTO;
import com.novabase.omni.oney.apps.financialdocument.enums.FilterRepositoryAttribute;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentAttachmentNotFoundException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentNotFoundException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentPaidException;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.OrderType;

import io.digitaljourney.platform.modules.commons.type.HttpStatusCode;
import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.ApiKeyAuthDefinition.ApiKeyLocation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.BasicAuthDefinition;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;

//@formatter:off
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path(AppProperties.ADDRESS_FINANCIAL_DOCUMENT_REPOSITORY_CONTROLLER)
@SwaggerDefinition(
	securityDefinition = @SecurityDefinition(
		basicAuthDefinitions = {
			@BasicAuthDefinition(key = RSProperties.SWAGGER_BASIC_AUTH),
		},
		apiKeyAuthDefinitions = {
			@ApiKeyAuthDefinition(
				key = RSProperties.SWAGGER_BEARER_AUTH,
				name = RSProperties.HTTP_HEADER_API_KEY,
				in = ApiKeyLocation.HEADER)
			}
	),
	schemes = {
		SwaggerDefinition.Scheme.HTTP,
		SwaggerDefinition.Scheme.HTTPS,
		SwaggerDefinition.Scheme.DEFAULT
	},
	info = @Info(
		title = "Financial Document Repository API",
		description = "The Financial Document Repository API.",
		version = AppProperties.CURRENT_VERSION,
		license = @License(name = "Digital Journey License", url = "http://www.digitaljourney.io/license")
	),
	basePath = "bin/mvc.do/"+AppProperties.ADDRESS_FINANCIAL_DOCUMENT_REPOSITORY_CONTROLLER
)
@Api(
	value = "Financial Document Repository API",
	authorizations = {
		@Authorization(value = RSProperties.SWAGGER_BASIC_AUTH),
		@Authorization(value = RSProperties.SWAGGER_BEARER_AUTH)
	}
)
@ApiResponses(
	value = {
		@ApiResponse(code = HttpStatusCode.UNAUTHORIZED_CODE, message = RSProperties.SWAGGER_UNAUTHORIZED_MESSAGE),
		@ApiResponse(code = HttpStatusCode.FORBIDDEN_CODE, message = RSProperties.SWAGGER_FORBIDDEN_MESSAGE)
	}
)
public interface FinancialDocumentRepositoryResource {

    @POST
    @Path("/list")
    @ApiOperation(value = "Searches Financial Document Repository", notes = "Retrieves a list of Financial Documents", response = FinancialDocumentDTO.class, responseContainer = "List")
    @ApiResponses(value = { @ApiResponse(code = 200, response = FinancialDocumentListDTO.class, responseContainer = "List", message = "OK")})
    public List<FinancialDocumentListDTO> list(@ApiParam(value = "Search Criteria", required = false) FinancialDocumentSearchCriteriaDTO SearchDTO,
	    @QueryParam(value = "Order by field") FilterRepositoryAttribute orderField,
	    @QueryParam(value = "Order by") OrderType orderType);
    
    @GET
    @Path("/{repositoryId}")
    @ApiOperation(value = "Get Financial Document from Repository", notes = "Get Financial Document from Repository", response = FinancialDocumentRepositoryDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = FinancialDocumentRepositoryDTO.class, message = "OK"), 
	    		    @ApiResponse(code = 400, response = FinancialDocumentNotFoundException.class, message = "BadRequest")})
    
    public FinancialDocumentRepositoryDTO get(@PathParam("repositoryId") long repositoryId);
    

    @GET
    @Path("/{repositoryId}/timeline")
    @ApiOperation(value = "Get Process Timeline", notes = "Retrieves the Timeline", responseContainer = "List", response = TimeLineStepDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, responseContainer = "List", response = TimeLineStepDTO.class, message = "OK")})
    public List<TimeLineStepDTO> getTimeLine(
	    @ApiParam(value = "The unique identifier of the Financial Document repository record", required = true, example = "1")@PathParam("repositoryId") long repositoryId);
    
    @GET
    @Path("/{repositoryId}/attachment/{attachmentIndex}")
    @ApiOperation(value = "Get Attachment ", notes = "Get (attachment) file byte[] from blob")
    @ApiResponses(value = { 
	    @ApiResponse(code = 200, message = "OK", response = byte.class, responseContainer = "Array"),
	    @ApiResponse(code = 400, response = FinancialDocumentAttachmentNotFoundException.class, message = "Bad Request"),
	    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public byte[] getAttachment(  
	    @ApiParam(value = "The unique identifier of the Financial Document repository record", required = true, example = "1") @PathParam("repositoryId") long repositoryId,
	    @ApiParam(value = "The attachment index", required = true) @PathParam("attachmentIndex") int attachmentIndex,
	    HttpServletResponse response);
    
    
  @PUT
  @Path("/paid")
  @ApiOperation(value = "Update Financial Document to Paid", notes = "Update Financial Document to Paid")
  @ApiResponses(value = {
	  @ApiResponse(code = 200, message = "OK"),
	  @ApiResponse(code = 400, response = FinancialDocumentPaidException.class, message = "NOT_OK"),
	  @ApiResponse(code = 404, response = FinancialDocumentPaidException.class, message = "NOT_FOUND"),
	  @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "INTERNAL_ERROR")})
  public void paid(@QueryParam("financialDocumentNumber") String financialDocumentNumber);

}
