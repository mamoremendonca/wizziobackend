package com.novabase.omni.oney.apps.financialdocument.enums;

public enum SearchScope {
    
    MINE, 
    TEAM, 
    ALL
    
}
