package com.novabase.omni.oney.apps.financialdocument.dto;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import io.digitaljourney.platform.plugins.modules.uam.service.api.type.UserStatusType;

public class UserInformationDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2665403969596227769L;
    
    private Long id;
    private UserStatusType status;
    private String firstName;
    private String lastName;
    private String email;
    private String userName;
    private String domain;

    public UserInformationDTO() {
	super();
    }

    public String getCompleteName() {
	
	String completeName = "";

	if (StringUtils.isNotBlank(firstName)) {
	    completeName += firstName;
	}

	if (StringUtils.isNotBlank(lastName)) {
	    completeName += (" " + lastName);
	}
	
	//@formatter:off
	return StringUtils.isNotBlank(completeName) 
		? completeName 
		: userName;
	//@formatter:on
	
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserStatusType getStatus() {
        return status;
    }

    public void setStatus(UserStatusType status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

}
