package com.novabase.omni.oney.apps.financialdocument.mapper;

import java.time.LocalDateTime;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.apps.financialdocument.dto.AttachmentDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentHeaderDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.ItemDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder.PurchaseOrderDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.Attachment;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentHeader;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.entity.Item;

@Mapper
public abstract class FinancialDocumentInstanceMapper {

    public static final FinancialDocumentInstanceMapper INSTANCE = Mappers.getMapper(FinancialDocumentInstanceMapper.class);

    public FinancialDocumentDTO toProcessDTO(final FinancialDocumentInstance instance) {
	
	if (instance == null) {
	    return null;
	}

	final FinancialDocumentDTO financialDocumentDTO = new FinancialDocumentDTO();
	financialDocumentDTO.setWorkFlowAvailableActions(instance.getWorkFlowAvailableActions());
	financialDocumentDTO.setFinancialDocumentHeader(this.toFinancialDocumentHeaderDTO(instance.getFinancialDocumentHeader()));
	financialDocumentDTO.getItems().addAll(this.toItemDTOList(instance.getItems()));
	financialDocumentDTO.getFinancialDocumentHeader().setGranting(instance.getFinancialDocumentHeader().isGranting());

	financialDocumentDTO.getFinancialDocumentHeader().setCreationDate(instance.getHeader() != null && instance.getHeader().createdOn != null ? instance.getHeader().createdOn
                                                            											 : LocalDateTime.now()); // When the DF is created we must set the LocalDateTime.now() 
																					 // because the instance.header does not exists yet.

	return financialDocumentDTO;
	
    }

//    @Mapping(source = "granting", target = "java(new Boolean(granting).booleanValue()")
    public abstract FinancialDocumentHeaderDTO toFinancialDocumentHeaderDTO(final FinancialDocumentHeader financialDocumentHeader);

    public abstract Item toItem(final ItemDTO itemDTO);
    
    @IterableMapping(elementTargetType = Item.class)
    public abstract List<Item> toItemList(final List<ItemDTO> items);

    public abstract ItemDTO toItemDTO(final Item item);
    
    @Mapping(source = "id", target = "itemPurchaseOrderId")
    public abstract Item toItem(com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder.ItemDTO itemDTO);
    
    @IterableMapping(elementTargetType = ItemDTO.class)
    public abstract List<ItemDTO> toItemDTOList(final List<Item> items);

    public abstract AttachmentDTO toAttachmentDTO(final Attachment attachment);
    
    @IterableMapping(elementTargetType = AttachmentDTO.class)
    public abstract List<AttachmentDTO> toAttachmentDTOList(final List<Attachment> attachments);

    @Mapping(source = "number", target = "financialDocumentHeader.number")
    @Mapping(source = "type", target = "financialDocumentHeader.type")
    @Mapping(source = "supplierCode", target = "financialDocumentHeader.supplierCode")
    @Mapping(source = "emissionDate", target = "financialDocumentHeader.emissionDate")
    @Mapping(ignore = true, source = "documentId", target = "financialDocumentHeader.documentId")
    @Mapping(ignore = true, source = "documentMimeType", target = "financialDocumentHeader.documentMimeType")
    @Mapping(ignore = true, source = "documentFileName", target = "financialDocumentHeader.documentFileName")
    @Mapping(ignore = true, source = "firstWorkflowGroup", target = "financialDocumentHeader.firstWorkflowGroup")
    public abstract void mergeFinancialDocumentHeaderDTODraft(@MappingTarget final FinancialDocumentInstance instance, final FinancialDocumentHeaderDTO financialDocumentHeaderDTO);
    
    @Mapping(source = "granting", target = "financialDocumentHeader.granting")
    @Mapping(source = "supplierCode", target = "financialDocumentHeader.supplierCode")
    @Mapping(source = "description", target = "financialDocumentHeader.description")
    @Mapping(source = "departmentCode", target = "financialDocumentHeader.departmentCode")
    @Mapping(source = "projectCode", target = "financialDocumentHeader.projectCode")
    @Mapping(source = "internalOrderCode", target = "financialDocumentHeader.internalOrderCode")
    @Mapping(source = "expirationDate", target = "financialDocumentHeader.expirationDate")
    @Mapping(source = "firstWorkflowGroup", target = "financialDocumentHeader.firstWorkflowGroup")
    @Mapping(ignore = true, source = "purchaseOrderId", target = "financialDocumentHeader.purchaseOrderId")
    @Mapping(ignore = true, source = "purchaseOrderFriendlyNumber", target = "financialDocumentHeader.purchaseOrderFriendlyNumber")
    @Mapping(ignore = true, source = "purchaseOrderType", target = "financialDocumentHeader.purchaseOrderType")
    @Mapping(ignore = true, source = "documentId", target = "financialDocumentHeader.documentId")
    @Mapping(ignore = true, source = "documentMimeType", target = "financialDocumentHeader.documentMimeType")
    @Mapping(ignore = true, source = "documentFileName", target = "financialDocumentHeader.documentFileName")
    public abstract void mergeFinancialDocumentHeaderDTOCreation(@MappingTarget final FinancialDocumentInstance instance, final FinancialDocumentHeaderDTO financialDocumentHeaderDTO);
    
    @Mapping(source = "granting", target = "financialDocumentHeader.granting")
    @Mapping(source = "expirationDate", target = "financialDocumentHeader.expirationDate")
    @Mapping(source = "description", target = "financialDocumentHeader.description")
    @Mapping(ignore = true, source = "firstWorkflowGroup", target = "financialDocumentHeader.firstWorkflowGroup")
    @Mapping(ignore = true, source = "associatedDocument", target = "financialDocumentHeader.associatedDocument")
    @Mapping(ignore = true, source = "supplierCode", target = "financialDocumentHeader.supplierCode")
    @Mapping(ignore = true, source = "departmentCode", target = "financialDocumentHeader.departmentCode")
    @Mapping(ignore = true, source = "projectCode", target = "financialDocumentHeader.projectCode")
    @Mapping(ignore = true, source = "internalOrderCode", target = "financialDocumentHeader.internalOrderCode")
    @Mapping(ignore = true, source = "purchaseOrderId", target = "financialDocumentHeader.purchaseOrderId")
    @Mapping(ignore = true, source = "purchaseOrderFriendlyNumber", target = "financialDocumentHeader.purchaseOrderFriendlyNumber")
    @Mapping(ignore = true, source = "purchaseOrderType", target = "financialDocumentHeader.purchaseOrderType")
    @Mapping(ignore = true, source = "documentId", target = "financialDocumentHeader.documentId")
    @Mapping(ignore = true, source = "documentMimeType", target = "financialDocumentHeader.documentMimeType")
    @Mapping(ignore = true, source = "documentFileName", target = "financialDocumentHeader.documentFileName")
    public abstract void mergeFinancialDocumentHeaderDTOCreationPO(@MappingTarget final FinancialDocumentInstance instance, final FinancialDocumentHeaderDTO financialDocumentHeaderDTO);

    @Mapping(source = "type", target = "financialDocumentHeader.type")
    @Mapping(source = "supplierCode", target = "financialDocumentHeader.supplierCode")
    @Mapping(source = "description", target = "financialDocumentHeader.description")
    @Mapping(source = "departmentCode", target = "financialDocumentHeader.departmentCode")
    @Mapping(source = "projectCode", target = "financialDocumentHeader.projectCode")
    @Mapping(source = "internalOrderCode", target = "financialDocumentHeader.internalOrderCode")
    @Mapping(source = "expirationDate", target = "financialDocumentHeader.expirationDate")    
    @Mapping(source = "firstWorkflowGroup", target = "financialDocumentHeader.firstWorkflowGroup")
    @Mapping(ignore = true, source = "number", target = "financialDocumentHeader.number")
    @Mapping(ignore = true, source = "documentId", target = "financialDocumentHeader.documentId")
    @Mapping(ignore = true, source = "documentMimeType", target = "financialDocumentHeader.documentMimeType")
    @Mapping(ignore = true, source = "documentFileName", target = "financialDocumentHeader.documentFileName")
    public abstract void mergeFinancialDocumentHeaderDTO(@MappingTarget final FinancialDocumentInstance instance, final FinancialDocumentHeaderDTO financialDocumentHeaderDTO);

    @Mapping(source = "id", target = "financialDocumentHeader.purchaseOrderId")
    @Mapping(source = "friendlyNumber", target = "financialDocumentHeader.purchaseOrderFriendlyNumber")
    @Mapping(source = "userGroup", target = "financialDocumentHeader.purchaseCreatedGroup")
    @Mapping(source = "userGroup", target = "financialDocumentHeader.firstWorkflowGroup")
    @Mapping(source = "description", target = "financialDocumentHeader.description")
    @Mapping(source = "departmentCode", target = "financialDocumentHeader.departmentCode")
    @Mapping(source = "projectCode", target = "financialDocumentHeader.projectCode")
    @Mapping(source = "internalOrderCode", target = "financialDocumentHeader.internalOrderCode")
    @Mapping(source = "totalWithoutTax", target = "financialDocumentHeader.totalWithoutTax")
    @Mapping(source = "type", target = "financialDocumentHeader.purchaseOrderType")
    @Mapping(source = "supplierCode", target = "financialDocumentHeader.supplierCode")
    @Mapping(source = "totalWithoutTax", target = "financialDocumentHeader.purchaseOrderTotalWithoutTax")
    public abstract void mergeFinancialDocumentInstance(@MappingTarget final FinancialDocumentInstance instance, final PurchaseOrderDTO purchaseOrder);

    @Mapping(ignore = true, source = "index", target = "index")
    public abstract void mergeItemDTO(@MappingTarget Item item, ItemDTO itemDTO);
    
}
