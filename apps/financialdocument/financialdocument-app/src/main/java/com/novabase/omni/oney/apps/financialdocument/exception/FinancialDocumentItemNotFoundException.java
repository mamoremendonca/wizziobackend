package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentItemNotFoundException extends FinancialDocumentException {

    private static final long serialVersionUID = 9009094786557873837L;

    public FinancialDocumentItemNotFoundException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.NOT_FOUND, errorCode, ctx, args);
    }

    public static FinancialDocumentItemNotFoundException of(Context ctx, int index, Long instanceId) {
	return new FinancialDocumentItemNotFoundException(AppProperties.ITEM_NOT_FOUND_EXCEPTION, ctx, index, instanceId);
    }

}
