package com.novabase.omni.oney.apps.financialdocument.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class WorkflowStepDTO {

    @ApiModelProperty
    private int index;
    
    @ApiModelProperty
    private String userGroup;
    
    @ApiModelProperty
    private boolean hasNextStep;
    
    @ApiModelProperty
    private boolean active;

    public WorkflowStepDTO() {
	
	super();
	
	this.index = 0;
	this.hasNextStep = Boolean.FALSE;
	this.active = Boolean.TRUE;
	
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public boolean isHasNextStep() {
        return hasNextStep;
    }

    public void setHasNextStep(boolean hasNextStep) {
        this.hasNextStep = hasNextStep;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}
