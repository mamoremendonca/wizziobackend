package com.novabase.omni.oney.apps.financialdocument.manager;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.financialdocument.AppContext;
import com.novabase.omni.oney.apps.financialdocument.agent.core.CoreAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.agent.financialdocumentrepository.FinancialDocumentRepositoryAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.agent.workflow.WorkflowAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentHeaderDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentRepositoryDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.UserInformationDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.WorkflowDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.CurrentAction;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentStatus;
import com.novabase.omni.oney.apps.financialdocument.enums.WorkFlowActionType;
import com.novabase.omni.oney.apps.financialdocument.validator.SubmitToApprovalValidator;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception.FDRepositoryNotFoundException;

import io.digitaljourney.platform.modules.commons.type.tuple.Pair;

@Component(service = { WorkflowManager.class })
public class WorkflowManager {

    @Reference
    private AppContext ctx;

    @Reference
    private CoreAgentImpl coreAgentImpl;

    @Reference
    private WorkflowAgentImpl workflowAgentImpl;

    @Reference
    private SubmitToApprovalValidator submitToApprovalValidator;

    @Reference
    private FinancialDocumentRepositoryAgentImpl financialDocumentRepositoryAgentImpl;

    public FinancialDocumentHeaderDTO generateWorkflowAndSubmit(final FinancialDocumentInstance instance) {

	submitToApprovalValidator.validate(instance);

	// TODO: can be removed after all running instances (created before 16/07/2020) are finished.
	final boolean hasAsssociatedPO = StringUtils.isEmpty(instance.getFinancialDocumentHeader().getPurchaseOrderFriendlyNumber()) == false;
	final String creationStartGroup = !hasAsssociatedPO ? instance.getFinancialDocumentHeader().getFirstWorkflowGroup() 
			: instance.getFinancialDocumentHeader().getPurchaseCreatedGroup();
	
	//@formatter:off
	final WorkflowDTO workflow = 
		this.workflowAgentImpl
			.generateWorkFlowByPosition(creationStartGroup
				,instance.getFinancialDocumentHeader().getDepartmentCode()
				,instance.getFinancialDocumentHeader().getTotalWithoutTax()
				,instance.getFinancialDocumentHeader().getUserGroup()
				,instance.getCurrentAction().getUserGroup()
				, hasAsssociatedPO);
	//@formatter:on

	instance.getFinancialDocumentHeader().setUserGroup(instance.getCurrentAction().getUserGroup()); // The owner of the Financial Document is the "Contas a Pagar" Group. After
													// generate the workflow we must change the owner group to "Contas a Pagar" in
													// order to persist the owner group properly on repository

	final Pair<FinancialDocumentHeaderDTO, WorkflowDTO> submitFinancialDocumentToRepository = this.financialDocumentRepositoryAgentImpl.submitFinancialDocumentToRepository(instance, workflow,
		coreAgentImpl.getDraftGroup());

	final FinancialDocumentHeaderDTO financialDocumentHeader = submitFinancialDocumentToRepository.getValue0();
	final WorkflowDTO workflowRepository = submitFinancialDocumentToRepository.getValue1();

	instance.getFinancialDocumentHeader().setRepositoryId(financialDocumentHeader.getRepositoryId());
	instance.getFinancialDocumentHeader().setNumber(financialDocumentHeader.getNumber());

	this.updateInstace(instance, workflowRepository);
	
	instance.refreshTargets();
	return financialDocumentHeader;

    }

    public WorkflowDTO approveFinancialDocument(final FinancialDocumentInstance instance, final Long userId) {

	// TODO: Review the approveFinancialDocument method
	final WorkflowDTO workflowDTO = this.financialDocumentRepositoryAgentImpl.approveFinancialDocument(instance.getFinancialDocumentHeader().getRepositoryId(), instance.getCurrentAction(),
		userId);
	this.updateInstace(instance, workflowDTO);

	if (workflowDTO.getCurrentIndex() == 0) {

	    // TODO: IMPORTANTE: AS REGRAS NA JOURNEY SOMENTE PODEM VALIDAR A INSTANCIA, O
	    // RETORNO DO METODO É IRRELEVANTE,
	    // QUANDO FOR CORRIGIDO TEMOS DE MUDAR A REGRA NA JORUNEY PARA AVALIAR O
	    // WORKFLOWDTO QUE É DEVOLVIDO NA APROVAÇÃO, REJEIÇÃO, RETORNO OU CANCELAMENTO
	    instance.setCurrentAction(null); // if the DF is approved and don't have more steps, we need to set
					     // currentAction = null in order to validate on post condition that the approval
					     // process is finished

	    // Send Mail to owner
	    this.sendNotification(instance.getFinancialDocumentHeader().getUserId(), instance.getHeader().instanceId, instance.getFinancialDocumentHeader().getNumber(), WorkFlowActionType.APPROVE);

	}

	return workflowDTO;
    }

    public WorkflowDTO returnFinancialDocument(final FinancialDocumentInstance instance, final Long userId) {

	final WorkflowDTO workflowDTO = this.financialDocumentRepositoryAgentImpl.returnFinancialDocument(instance.getFinancialDocumentHeader().getRepositoryId(), instance.getCurrentAction(), userId);

	this.updateInstace(instance, workflowDTO);

	return workflowDTO;
    }

    public WorkflowDTO rejectFinancialDocument(final FinancialDocumentInstance instance, final Long userId) {

	final WorkflowDTO workflowDTO = this.financialDocumentRepositoryAgentImpl.rejectFinancialDocument(instance.getFinancialDocumentHeader().getRepositoryId(), instance.getCurrentAction(), userId);
	this.updateInstace(instance, workflowDTO);

	// Send Mail to owner
	this.sendNotification(instance.getFinancialDocumentHeader().getUserId(), instance.getHeader().instanceId, instance.getFinancialDocumentHeader().getNumber(), WorkFlowActionType.REJECT);

	return workflowDTO;
    }

    private void sendNotification(final Long userId, final Long instanceId, final String financialDocumentNumber, final WorkFlowActionType actionType) {

	try {

	    final UserInformationDTO currentUserInformation = this.coreAgentImpl.getUserInformations(userId);
	    this.coreAgentImpl.sendMail(Arrays.asList(currentUserInformation.getEmail()), actionType, null, instanceId, financialDocumentNumber);

	} catch (Exception e) {
	    this.ctx.logError("ERROR trying to send notification [" + actionType + "] for instanceId [" + instanceId + "]: " + e.getMessage());
	}

    }

    public WorkflowDTO cancelFinancialDocument(final FinancialDocumentInstance instance, final Long userId) {

	final WorkflowDTO workflowDTO = this.financialDocumentRepositoryAgentImpl.cancelFinancialDocument(instance.getFinancialDocumentHeader().getRepositoryId(), instance.getCurrentAction(), userId);
	this.updateInstace(instance, workflowDTO);

	return workflowDTO;
    }
    
    public WorkflowDTO payFinancialDocument(String financialDocumentNumber, final Long userId) {
	
	FinancialDocumentRepositoryDTO financialDocument = null;
	
	try {
	    financialDocument = this.financialDocumentRepositoryAgentImpl.getFinancialDocumentNotCanceledByNumber(financialDocumentNumber);
	} catch (Exception e) {
	    this.ctx.logError("[financialdocument] Error trying to find financialDocument from Repository with number" + financialDocumentNumber + " exception: " + e);
	    if(e.getCause().getClass().equals(FDRepositoryNotFoundException.class)) {
		throw ctx.paidNotFoundException(financialDocumentNumber);
	    }
	    throw ctx.exception("Unexpected exception trying to update status of the Financial Document: " + financialDocumentNumber );
	}
	
	if(financialDocument.getFinancialDocumentHeader().getRepositoryStatus() == null) {
	    throw ctx.exception("Unexpected exception trying to update status of the Financial Document: " + financialDocumentNumber);
	}
	
	if (FinancialDocumentStatus.SAP_SUBMITTED.equals(financialDocument.getFinancialDocumentHeader().getRepositoryStatus()) == false) {
	    throw ctx.paidStatusException(financialDocumentNumber, financialDocument.getFinancialDocumentHeader().getRepositoryStatus().name());
	}
	
	final WorkflowDTO workflowDTO = this.financialDocumentRepositoryAgentImpl.payFinancialDocument(financialDocument.getFinancialDocumentHeader().getRepositoryId(), new CurrentAction(), userId);

	return workflowDTO;
    }

    public void claimProcess(final FinancialDocumentInstance instance, final Long userId, final String userGroup) {

	WorkflowDTO workflow;
	try {
	    workflow = this.financialDocumentRepositoryAgentImpl.claimFinancialDocument(instance.getFinancialDocumentHeader().getRepositoryId(), userId, userGroup, instance.getCurrentAction());
	} catch (Exception e) {
	    throw ctx.couldNotClaimException(instance.getHeader().instanceId, userGroup);
	}

	this.updateInstace(instance, workflow);

    }

    public List<String> getUserDepartmentsByUserGroup(final String userGroup) {
	return this.workflowAgentImpl.getDepartments(userGroup);
    }

    private void updateInstace(final FinancialDocumentInstance instance, final WorkflowDTO workflowDTO) {

	this.updateCurrentAction(instance, workflowDTO);
	this.updateAvailableActions(instance, workflowDTO);

    }

    private void updateCurrentAction(final FinancialDocumentInstance instance, final WorkflowDTO workflowDTO) {

	if (instance.getAttachments() == null) {
	    instance.setAttachments(new ArrayList<>());
	}

	if (instance.getCurrentAction().getAttachments() != null && instance.getCurrentAction().getAttachments().isEmpty() == false) {
	    instance.getAttachments().addAll(instance.getCurrentAction().getAttachments());
	}

	instance.setCurrentAction(new CurrentAction());
	instance.getCurrentAction().setStartDate(LocalDateTime.now());
	instance.getCurrentAction().setUserGroup(workflowDTO.getCurrentUserGroup());
	instance.getCurrentAction().setLimitDateToNextStep(workflowDTO.getLimitDateToNextStep());

    }

    private void updateAvailableActions(final FinancialDocumentInstance instance, final WorkflowDTO workflowDTO) {

	//@formatter:off
	final int previousStepIndex = workflowDTO.getSteps().stream().
								sorted((step1, step2) -> step2.getIndex() - step1.getIndex()).
								filter(step -> step.isActive() && step.getIndex() < workflowDTO.getCurrentIndex()).
								map(step -> step.getIndex()).
								findFirst().
								orElse(0);
	//@formatter:on

	//@formatter:off
	if(FinancialDocumentStatus.REJECTED.equals(workflowDTO.getFinancialDocumentStatus())) {
	    
	    //When a Financial Document is REJECTED the only possible action is CANCEL
	    instance.getWorkFlowAvailableActions().clear();
	    instance.getWorkFlowAvailableActions().addAll(Arrays.asList(WorkFlowActionType.CANCEL));
	
	} else if(FinancialDocumentStatus.IN_APPROVAL.equals(workflowDTO.getFinancialDocumentStatus()) && previousStepIndex <= 2) {
	    
	    // When a Financial Document is IN_APPROVAL and the current workflow index is 2 is only possible APPROVE or REJECT,
	    // because index 2 is the first index of the approval workflow the index 1 is the submission to workflow.
	    instance.getWorkFlowAvailableActions().clear();
	    instance.getWorkFlowAvailableActions().addAll(Arrays.asList(WorkFlowActionType.APPROVE, WorkFlowActionType.REJECT));
	
	} else if(FinancialDocumentStatus.IN_APPROVAL.equals(workflowDTO.getFinancialDocumentStatus()) && previousStepIndex >= 3) {
	   
	    // When a Financial Document is IN_APPROVAL and the current workflow index is greater than 2 is possible APPROVE, RETURN or REJECT..
	    instance.getWorkFlowAvailableActions().clear();
	    instance.getWorkFlowAvailableActions().addAll(Arrays.asList(WorkFlowActionType.APPROVE, WorkFlowActionType.RETURN, WorkFlowActionType.REJECT));
	
	} else {
	    
	    // Otherwise the Financial Document is APPROVED or CANCELED, is not possible perform any action.
	    instance.setWorkFlowAvailableActions(null);
	    
	}
	//@formatter:on
    }

}
