package com.novabase.omni.oney.apps.financialdocument.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.financialdocument.json.serializer.NBLocalDateTimeSerializer;

public class CurrentAction {

    private String comment;
    
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime startDate;
    
    private String userGroup;
	
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime limitDateToNextStep;
	
    private List<Attachment> attachments;

    public CurrentAction() {
	
	super();
	
	this.startDate = LocalDateTime.now();
	this.limitDateToNextStep = LocalDateTime.now();
	this.attachments = new ArrayList<Attachment>();
	
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public LocalDateTime getLimitDateToNextStep() {
        return limitDateToNextStep;
    }

    public void setLimitDateToNextStep(LocalDateTime limitDateToNextStep) {
        this.limitDateToNextStep = limitDateToNextStep;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

}
