package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentAttachmentFailedReadStreamException extends FinancialDocumentException {

    private static final long serialVersionUID = -6536364574772186973L;

    public FinancialDocumentAttachmentFailedReadStreamException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.INTERNAL_SERVER_ERROR, errorCode, ctx, args);
    }

    public static FinancialDocumentAttachmentFailedReadStreamException of(Context ctx, Long instanceId) {
	return new FinancialDocumentAttachmentFailedReadStreamException(AppProperties.FAILED_READ_STREAM_EXCEPTION, ctx, instanceId);
    }

}
