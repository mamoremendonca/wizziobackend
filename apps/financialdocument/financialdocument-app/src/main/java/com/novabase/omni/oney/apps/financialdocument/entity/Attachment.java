package com.novabase.omni.oney.apps.financialdocument.entity;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.financialdocument.enums.DocumentClassType;
import com.novabase.omni.oney.apps.financialdocument.json.serializer.NBLocalDateTimeSerializer;

public class Attachment {

    private int index;
    
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime date;
    
    private String mimeType;
    private String name;
    private Long blobId;
    private Long ownerId;
    private DocumentClassType documentClass;
    
    public Attachment() {
	
	super();
	
	this.index = 0;
	this.date = LocalDateTime.now();
	this.blobId = 0L;
	this.ownerId = 0L;
	this.documentClass = DocumentClassType.FINANCIAL_DOCUMENT;
	
    }
    
    public int getIndex() {
        return index;
    }
    
    public void setIndex(int index) {
        this.index = index;
    }
    
    public LocalDateTime getDate() {
        return date;
    }
    
    public void setDate(LocalDateTime date) {
        this.date = date;
    }
    
    public String getMimeType() {
        return mimeType;
    }
    
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Long getBlobId() {
        return blobId;
    }
    
    public void setBlobId(Long blobId) {
        this.blobId = blobId;
    }
    
    public Long getOwnerId() {
        return ownerId;
    }
    
    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }
    
    public DocumentClassType getDocumentClass() {
        return documentClass;
    }
    
    public void setDocumentClass(DocumentClassType documentClass) {
        this.documentClass = documentClass;
    }
	
}
