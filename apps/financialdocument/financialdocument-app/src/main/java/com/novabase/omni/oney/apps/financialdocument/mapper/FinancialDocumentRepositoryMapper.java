package com.novabase.omni.oney.apps.financialdocument.mapper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.apps.financialdocument.dto.AttachmentDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.AttachmentRepositoryDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentHeaderDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentListDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentRepositoryDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentSearchCriteriaDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.ItemDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.TimeLineStepDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.WorkflowDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.Attachment;
import com.novabase.omni.oney.apps.financialdocument.entity.CurrentAction;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.entity.Item;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.AttachmentMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.FinancialDocumentListMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.FinancialDocumentMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.HistoryMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.ItemMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.SearchCriteriaMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.TimeLineStepMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.WorkflowActionMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.WorkflowMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.TimeLineStepTypeMs;

@Mapper
public abstract class FinancialDocumentRepositoryMapper {

    public static final FinancialDocumentRepositoryMapper INSTANCE = Mappers.getMapper(FinancialDocumentRepositoryMapper.class);

    @Mapping(source = "entityCode", target = "supplierCode")
    @Mapping(source = "createdDate", target = "creationDate")
    @Mapping(expression = "java(com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.ScopeTypeMs.valueOf(searchScope.name()))", target = "scope.scopeType")
    public abstract SearchCriteriaMsDTO toSearchCriteriaMsDTO(FinancialDocumentSearchCriteriaDTO searchCriteira);

    @Mapping(source = "id", target = "repositoryId")
    @Mapping(source = "status", target = "repositoryStatus")
    public abstract FinancialDocumentListDTO toFinancialDocumentListDTO(final FinancialDocumentListMsDTO financialDocumentListMs);

    @IterableMapping(elementTargetType = FinancialDocumentListDTO.class)
    public abstract List<FinancialDocumentListDTO> toFinancialDocumentListDTOList(final List<FinancialDocumentListMsDTO> financialDocumentListMsList);

    public FinancialDocumentMsDTO toFinancialDocumentMsDTO(FinancialDocumentInstance instance, WorkflowDTO workflow, String draftGroup) {

	FinancialDocumentMsDTO financialDocument = new FinancialDocumentMsDTO();
	financialDocument.setInstanceId(instance.getHeader().instanceId);
	financialDocument.setNumber(instance.getFinancialDocumentHeader().getNumber());
	financialDocument.setType(instance.getFinancialDocumentHeader().getType().getMsType());
	financialDocument.setSupplierCode(instance.getFinancialDocumentHeader().getSupplierCode());
	financialDocument.setDescription(instance.getFinancialDocumentHeader().getDescription());
	financialDocument.setDepartmentCode(instance.getFinancialDocumentHeader().getDepartmentCode());
	financialDocument.setProjectCode(instance.getFinancialDocumentHeader().getProjectCode());
	financialDocument.setInternalOrderCode(instance.getFinancialDocumentHeader().getInternalOrderCode());
	financialDocument.setCreationDate(instance.getHeader().createdOn);
	financialDocument.setExpirationDate(instance.getFinancialDocumentHeader().getExpirationDate());
	financialDocument.setEmissionDate(instance.getFinancialDocumentHeader().getEmissionDate());

	financialDocument.setGranting(instance.getFinancialDocumentHeader().isGranting());

	
	Long associatedDocumentId = instance.getFinancialDocumentHeader().getAssociatedDocumentId();
	
	if(new Long(0).equals(associatedDocumentId)) {
	    associatedDocumentId = null;
	}
	
	financialDocument.setAssociatedDocument(instance.getFinancialDocumentHeader().getAssociatedDocument());
	financialDocument.setAssociatedDocumentId(associatedDocumentId);
	
	Long purchaseOrderId = instance.getFinancialDocumentHeader().getPurchaseOrderId();
	if(new Long(0).equals(purchaseOrderId)) {
	    purchaseOrderId = null;
	}

	financialDocument.setPurchaseOrderFriendlyNumber(instance.getFinancialDocumentHeader().getPurchaseOrderFriendlyNumber());
	financialDocument.setPurchaseOrderId(purchaseOrderId);
	financialDocument.setPurchaseOrderType(instance.getFinancialDocumentHeader().getPurchaseOrderType().getMsType());

	financialDocument.setDocumentFileName(instance.getFinancialDocumentHeader().getDocumentFileName());
	financialDocument.setDocumentId(instance.getFinancialDocumentHeader().getDocumentId());
	financialDocument.setDocumentMimeType(instance.getFinancialDocumentHeader().getDocumentMimeType());

	financialDocument.setUserGroup(instance.getFinancialDocumentHeader().getUserGroup());
	financialDocument.setUserId(instance.getFinancialDocumentHeader().getUserId());
	financialDocument.setTotalWithoutTax(instance.getFinancialDocumentHeader().getTotalWithoutTax());

	financialDocument.setHistories(new ArrayList<>());

	HistoryMsDTO draftHistory = new HistoryMsDTO();
	HistoryMsDTO creationHistory = new HistoryMsDTO();
	
	draftHistory.setDate(instance.getFinancialDocumentHeader().getSubmitToCreationDate());
	draftHistory.setUserGroup(draftGroup);
	draftHistory.setUserId(instance.getFinancialDocumentHeader().getDigitalizationUserId());
	draftHistory.setType(TimeLineStepTypeMs.DRAFT);
	draftHistory.setAttachments(new HashSet<AttachmentMsDTO>());
	draftHistory.getAttachments().addAll(toAttachmentMsDTOSet(instance.getAttachments()));
	
	creationHistory.setDate(LocalDateTime.now());
	creationHistory.setUserGroup(financialDocument.getUserGroup());
	creationHistory.setUserId(financialDocument.getUserId());
	creationHistory.setType(TimeLineStepTypeMs.SUBMITTED);
	creationHistory.setAttachments(toAttachmentMsDTOSet(instance.getCurrentAction().getAttachments()));

	financialDocument.getHistories().add(draftHistory);
	financialDocument.getHistories().add(creationHistory);

	financialDocument.setItems(toItemMsDTOList(instance.getItems()));

	financialDocument.setWorkflow(toWorkflowMsDTO(workflow));

	return financialDocument;
    }

    public abstract ItemMsDTO toItemMsDTO(Item item);

    public abstract List<ItemMsDTO> toItemMsDTOList(List<Item> items);

    @Mapping(source = "ownerId", target = "userId")
    @Mapping(target = "documentClass", expression = "java(attachment.getDocumentClass().getMsDocumentClass())")
    public abstract AttachmentMsDTO toAttachmentMsDTO(Attachment attachment);

    public abstract List<AttachmentMsDTO> toAttachmentMsDTOList(List<Attachment> attachments);
    
    public abstract Set<AttachmentMsDTO> toAttachmentMsDTOSet(List<Attachment> attachments);

    public abstract WorkflowMsDTO toWorkflowMsDTO(WorkflowDTO workflow);

    public abstract HistoryMsDTO toHistoryMsDTO(CurrentAction currentAction);

    @Mapping(source = "id", target = "repositoryId")
    @Mapping(target = "type", expression = "java(com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentType.valueOf(financialDocumentMsDTO.getType()))")
    @Mapping(target = "repositoryStatus", expression = "java(com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentStatus.valueOf(financialDocumentMsDTO.getStatus()))")
    @Mapping(target = "currentUserGroup", source = "workflow.currentUserGroup")
    public abstract FinancialDocumentHeaderDTO toFinancialDocumentHeaderDTO(FinancialDocumentMsDTO financialDocumentMsDTO);

    public abstract WorkflowActionMsDTO toWorkflowActionMsDTO(CurrentAction action);

    @Mapping(target = "financialDocumentStatus", expression = "java(com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentStatus.valueOf(workFlow.getFinancialDocumentStatus()))")
    public abstract WorkflowDTO toWorkflowDTO(WorkflowMsDTO workFlow);

    @Mapping(source = "id", target = "financialDocumentHeader.repositoryId")
    @Mapping(source = "associatedDocumentId", target = "financialDocumentHeader.associatedDocumentId")
    @Mapping(source = "associatedDocument", target = "financialDocumentHeader.associatedDocument")
    @Mapping(source = "purchaseOrderFriendlyNumber", target = "financialDocumentHeader.purchaseOrderFriendlyNumber")
    @Mapping(source = "purchaseOrderId", target = "financialDocumentHeader.purchaseOrderId")
    @Mapping(target = "financialDocumentHeader.purchaseOrderType", expression = "java(com.novabase.omni.oney.apps.financialdocument.enums.purchaseorder.PurchaseOrderType.valueOf(financialDocumentMsDTO.getPurchaseOrderType()))")
    @Mapping(source = "granting", target = "financialDocumentHeader.granting")
    @Mapping(source = "emissionDate", target = "financialDocumentHeader.emissionDate")
    @Mapping(source = "number", target = "financialDocumentHeader.number")
    @Mapping(source = "type", target = "financialDocumentHeader.type")
    @Mapping(source = "supplierCode", target = "financialDocumentHeader.supplierCode")
    @Mapping(source = "description", target = "financialDocumentHeader.description")
    @Mapping(source = "departmentCode", target = "financialDocumentHeader.departmentCode")
    @Mapping(source = "projectCode", target = "financialDocumentHeader.projectCode")
    @Mapping(source = "internalOrderCode", target = "financialDocumentHeader.internalOrderCode")
    @Mapping(source = "creationDate", target = "financialDocumentHeader.creationDate")
    @Mapping(source = "expirationDate", target = "financialDocumentHeader.expirationDate")
    @Mapping(source = "finishDate", target = "financialDocumentHeader.finishDate")
    @Mapping(source = "totalWithoutTax", target = "financialDocumentHeader.totalWithoutTax")
    @Mapping(source = "canClaim", target = "canClaim")
    @Mapping(source = "items", target = "items")
    @Mapping(target = "financialDocumentHeader.repositoryStatus", expression = "java(com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentStatus.valueOf(financialDocumentMsDTO.getStatus()))")
    public abstract FinancialDocumentRepositoryDTO toFinancialDocumentRepositoryDTO(FinancialDocumentMsDTO financialDocument);

    public abstract List<ItemDTO> toItemListDTO(List<ItemMsDTO> item);

    public abstract ItemDTO toItemDTO(ItemMsDTO item);

    @Mapping(target = "type", expression = "java(com.novabase.omni.oney.apps.financialdocument.enums.TimeLineStepType.valueOf(timelineStep.getType()))")
    public abstract TimeLineStepDTO toTimeLineStepDTO(TimeLineStepMsDTO timelineStep);

    public abstract List<TimeLineStepDTO> toTimeLineStepDTOList(List<TimeLineStepMsDTO> timelineSteps);

    @Mapping(target = "ownerId", source = "userId")
    public abstract AttachmentDTO toAttachmentMsDTO(AttachmentMsDTO attachment);

    @Mapping(target = "ownerId", source = "userId")
    @Mapping(target = "documentClass", expression = "java(com.novabase.omni.oney.apps.financialdocument.enums.DocumentClassType.valueOf(financialDocumentAttachment.getDocumentClass()))")
    public abstract Attachment toAttachment(AttachmentMsDTO financialDocumentAttachment);

    public abstract List<Attachment> toAttachmentList(List<AttachmentMsDTO> financialDocumentAttachment);

    @Mapping(target = "ownerId", source = "userId")
    @Mapping(target = "documentClass", expression = "java(com.novabase.omni.oney.apps.financialdocument.enums.DocumentClassType.valueOf(attachment.getDocumentClass()))")
    public abstract AttachmentRepositoryDTO toAttachmentRepositoryDTO(final AttachmentMsDTO attachment);

}
