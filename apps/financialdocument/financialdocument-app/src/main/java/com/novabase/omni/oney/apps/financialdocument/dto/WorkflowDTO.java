package com.novabase.omni.oney.apps.financialdocument.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentStatus;
import com.novabase.omni.oney.apps.financialdocument.json.serializer.NBLocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class WorkflowDTO extends DefaultDTO {

    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime creationDate;
    
    @ApiModelProperty
    private int currentIndex;
    
    @ApiModelProperty
    private String currentUserGroup;
    
    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime limitDateToNextStep;
    
    @ApiModelProperty
    private boolean hasNextStep;
    
    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private FinancialDocumentStatus financialDocumentStatus;

    @ApiModelProperty
    private List<WorkflowStepDTO> steps;

    public WorkflowDTO() {
	
	super();
	this.creationDate = LocalDateTime.now();
	this.currentIndex = 0;
	this.limitDateToNextStep = LocalDateTime.now();
	this.hasNextStep = Boolean.FALSE;
	this.steps = new ArrayList<WorkflowStepDTO>();
	
    }

    //@formatter:off
    public WorkflowStepDTO getStepByIndex(final int index) {
	
	return this.steps != null ? this.steps.stream().
						filter(step -> step.getIndex() == index).
						findFirst().
						orElse(null) : null;
			
    }
    //@formatter:on

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
    }

    public String getCurrentUserGroup() {
        return currentUserGroup;
    }

    public void setCurrentUserGroup(String currentUserGroup) {
        this.currentUserGroup = currentUserGroup;
    }

    public LocalDateTime getLimitDateToNextStep() {
        return limitDateToNextStep;
    }

    public void setLimitDateToNextStep(LocalDateTime limitDateToNextStep) {
        this.limitDateToNextStep = limitDateToNextStep;
    }

    public boolean isHasNextStep() {
        return hasNextStep;
    }

    public void setHasNextStep(boolean hasNextStep) {
        this.hasNextStep = hasNextStep;
    }


    public FinancialDocumentStatus getFinancialDocumentStatus() {
        return financialDocumentStatus;
    }

    public void setFinancialDocumentStatus(FinancialDocumentStatus financialDocumentStatus) {
        this.financialDocumentStatus = financialDocumentStatus;
    }

    public List<WorkflowStepDTO> getSteps() {
        return steps;
    }

    public void setSteps(List<WorkflowStepDTO> steps) {
        this.steps = steps;
    }
    
}
