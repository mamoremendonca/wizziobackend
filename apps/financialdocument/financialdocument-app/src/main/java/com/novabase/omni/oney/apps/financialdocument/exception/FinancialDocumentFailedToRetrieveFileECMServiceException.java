package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentFailedToRetrieveFileECMServiceException extends FinancialDocumentException {

    private static final long serialVersionUID = 8903174019305471216L;

    public FinancialDocumentFailedToRetrieveFileECMServiceException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.INTERNAL_SERVER_ERROR, errorCode, ctx, args);
    }

    public static FinancialDocumentFailedToRetrieveFileECMServiceException of(Context ctx, int attachmentIndex, Long financialDocumentId, String message) {
	return new FinancialDocumentFailedToRetrieveFileECMServiceException(AppProperties.FAILED_TO_RETRIEVE_FILE_ECM_EXCEPTION, ctx, attachmentIndex, financialDocumentId, message);
    }

}
