package com.novabase.omni.oney.apps.financialdocument.dto;

import java.time.LocalDateTime;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentStatus;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentType;
import com.novabase.omni.oney.apps.financialdocument.enums.purchaseorder.PurchaseOrderType;
import com.novabase.omni.oney.apps.financialdocument.json.serializer.NBLocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class FinancialDocumentHeaderDTO extends DTO {

    @ApiModelProperty
    private FinancialDocumentType type;

    @ApiModelProperty
    private String number;

    @ApiModelProperty
    private Long purchaseOrderId;

    @ApiModelProperty
    private String purchaseOrderFriendlyNumber;

    @ApiModelProperty
    private PurchaseOrderType purchaseOrderType;

    @ApiModelProperty
    private String associatedDocument;

    @ApiModelProperty
    private Long associatedDocumentId;

    @ApiModelProperty
    private String supplierCode;

    @ApiModelProperty(readOnly = true)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime emissionDate;

    @ApiModelProperty
    private Long documentId;

    @ApiModelProperty
    private String documentMimeType;

    @ApiModelProperty
    private String documentFileName;

    @ApiModelProperty
    private boolean granting;

    @ApiModelProperty
    private String description;

    @ApiModelProperty
    private String departmentCode;

    @ApiModelProperty
    private String projectCode;

    @ApiModelProperty
    private String internalOrderCode;

    @ApiModelProperty
    private Double totalWithoutTax;

    @ApiModelProperty(readOnly = true)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime creationDate;

    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime finishDate;

    // TODO: Revise Financial Document Header Structure.
    @ApiModelProperty(readOnly = true)
    private Long repositoryId;

    @ApiModelProperty
    private FinancialDocumentStatus repositoryStatus;

    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime expirationDate;

    @ApiModelProperty
    private String userId;

    @ApiModelProperty
    private String userGroup;

    @ApiModelProperty
    private String currentUserGroup;

    @ApiModelProperty
    private String firstWorkflowGroup;
    
    public FinancialDocumentHeaderDTO() {

	super();

	this.granting = Boolean.FALSE;
	this.repositoryId = 0L;
	this.documentId = 0L;
//	Cannot init theses Dates at the start of the process.
//	this.expirationDate = LocalDateTime.now();
//	this.approvalDate = LocalDateTime.now();
	this.creationDate = LocalDateTime.now();
	this.totalWithoutTax = 0D;
	this.purchaseOrderType = PurchaseOrderType.OPEX;
	this.purchaseOrderId = 0L;

    }

    public String getFirstWorkflowGroup() {
        return firstWorkflowGroup;
    }

    public void setFirstWorkflowGroup(String firstWorkflowGroup) {
        this.firstWorkflowGroup = firstWorkflowGroup;
    }

    public Long getPurchaseOrderId() {
	return purchaseOrderId;
    }

    public void setPurchaseOrderId(Long purchaseOrderId) {
	this.purchaseOrderId = purchaseOrderId;
    }

    public PurchaseOrderType getPurchaseOrderType() {
	return purchaseOrderType;
    }

    public void setPurchaseOrderType(PurchaseOrderType purchaseOrderType) {
	this.purchaseOrderType = purchaseOrderType;
    }

    public String getPurchaseOrderFriendlyNumber() {
	return purchaseOrderFriendlyNumber;
    }

    public void setPurchaseOrderFriendlyNumber(String purchaseOrderFriendlyNumber) {
	this.purchaseOrderFriendlyNumber = purchaseOrderFriendlyNumber;
    }

    public String getAssociatedDocument() {
	return associatedDocument;
    }

    public void setAssociatedDocument(String associatedDocument) {
	this.associatedDocument = associatedDocument;
    }

    public boolean isGranting() {
	return granting;
    }

    public void setGranting(boolean granting) {
	this.granting = granting;
    }

    public String getDocumentMimeType() {
	return documentMimeType;
    }

    public void setDocumentMimeType(String documentMimeType) {
	this.documentMimeType = documentMimeType;
    }

    public String getDocumentFileName() {
	return documentFileName;
    }

    public void setDocumentFileName(String documentFileName) {
	this.documentFileName = documentFileName;
    }

    public Long getDocumentId() {
	return documentId;
    }

    public void setDocumentId(Long documentId) {
	this.documentId = documentId;
    }

    public LocalDateTime getEmissionDate() {
	return emissionDate;
    }

    public void setEmissionDate(LocalDateTime emissionDate) {
	this.emissionDate = emissionDate;
    }

    public Long getRepositoryId() {
	return repositoryId;
    }

    public void setRepositoryId(Long repositoryId) {
	this.repositoryId = repositoryId;
    }

    public FinancialDocumentStatus getRepositoryStatus() {
	return repositoryStatus;
    }

    public void setRepositoryStatus(FinancialDocumentStatus repositoryStatus) {
	this.repositoryStatus = repositoryStatus;
    }

    public String getNumber() {
	return number;
    }

    public void setNumber(String number) {
	this.number = number;
    }

    public FinancialDocumentType getType() {
	return type;
    }

    public void setType(FinancialDocumentType type) {
	this.type = type;
    }

    public String getSupplierCode() {
	return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
	this.supplierCode = supplierCode;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getDepartmentCode() {
	return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
	this.departmentCode = departmentCode;
    }

    public String getProjectCode() {
	return projectCode;
    }

    public void setProjectCode(String projectCode) {
	this.projectCode = projectCode;
    }

    public String getInternalOrderCode() {
	return internalOrderCode;
    }

    public void setInternalOrderCode(String internalOrderCode) {
	this.internalOrderCode = internalOrderCode;
    }

    public Long getAssociatedDocumentId() {
	return associatedDocumentId;
    }

    public void setAssociatedDocumentId(Long associatedDocumentId) {
	this.associatedDocumentId = associatedDocumentId;
    }

    public LocalDateTime getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
	this.creationDate = creationDate;
    }

    public LocalDateTime getExpirationDate() {
	return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
	this.expirationDate = expirationDate;
    }

    public LocalDateTime getFinishDate() {
	return finishDate;
    }

    public void setFinishDate(LocalDateTime finishDate) {
	this.finishDate = finishDate;
    }

    public Double getTotalWithoutTax() {
	return totalWithoutTax;
    }

    public void setTotalWithoutTax(Double totalWithoutTax) {
	this.totalWithoutTax = totalWithoutTax;
    }

    public String getUserId() {
	return userId;
    }

    public void setUserId(String userId) {
	this.userId = userId;
    }

    public String getUserGroup() {
	return userGroup;
    }

    public void setUserGroup(String userGroup) {
	this.userGroup = userGroup;
    }

    public String getCurrentUserGroup() {
	return currentUserGroup;
    }

    public void setCurrentUserGroup(String currentUserGroup) {
	this.currentUserGroup = currentUserGroup;
    }

}
