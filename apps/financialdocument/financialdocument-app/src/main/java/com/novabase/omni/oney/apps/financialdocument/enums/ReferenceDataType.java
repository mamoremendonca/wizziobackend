package com.novabase.omni.oney.apps.financialdocument.enums;

import java.util.ArrayList;
import java.util.List;

import com.novabase.omni.oney.apps.financialdocument.agent.referencedata.ReferenceDataAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.dto.ReferenceDataDTO;
import com.novabase.omni.oney.apps.financialdocument.provider.ReferenceDataProvider;

public enum ReferenceDataType {

    FINANCIAL_DOCUMENT_TYPE() {

	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getPurchaseOrderType();
	}

    },

    FINANCIAL_DOCUMENT_STATUS() {

	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getPurchaseOrderListStatus();
	}
	
    },

    DEPARTMENT() {

	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ArrayList<>();
	}

    };

    public abstract <T> List<T> getData(final ReferenceDataAgentImpl agent);

}
