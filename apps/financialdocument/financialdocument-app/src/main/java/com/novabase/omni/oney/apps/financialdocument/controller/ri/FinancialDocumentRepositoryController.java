/*-
 * #%L
 * Apps :: Financial Document App  App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.financialdocument.controller.ri;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.eclipse.gemini.blueprint.extensions.annotation.ServiceReference;
import org.osgi.service.component.annotations.Component;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;
import com.novabase.omni.oney.apps.financialdocument.controller.api.FinancialDocumentRepositoryResource;
import com.novabase.omni.oney.apps.financialdocument.dto.FileDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentListDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentRepositoryDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentSearchCriteriaDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.TimeLineStepDTO;
import com.novabase.omni.oney.apps.financialdocument.enums.FilterRepositoryAttribute;
import com.novabase.omni.oney.apps.financialdocument.manager.AttachmentManager;
import com.novabase.omni.oney.apps.financialdocument.manager.CoreManager;
import com.novabase.omni.oney.apps.financialdocument.manager.FinancialDocumentRepositoryManager;
import com.novabase.omni.oney.apps.financialdocument.manager.WorkflowManager;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.OrderType;

import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;

//@formatter:off
@Component
@RSProvider(value = AppProperties.ADDRESS_FINANCIAL_DOCUMENT_REPOSITORY_CONTROLLER)
@RestController
@RequestMapping(AppProperties.ADDRESS_FINANCIAL_DOCUMENT_REPOSITORY_CONTROLLER)
//@formatter:on
public class FinancialDocumentRepositoryController extends AbstractAppController implements FinancialDocumentRepositoryResource {

    @ServiceReference
    private AttachmentManager attachmentManager;

    @ServiceReference
    private CoreManager coreManager;

    @ServiceReference
    private FinancialDocumentRepositoryManager financialDocumentRepositoryManager;

    @ServiceReference
    private WorkflowManager workflowManager;

    /**
     * Retrieves all Purchase Orders from Repository, based on specific search
     * criterias
     */
    @Override
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public List<FinancialDocumentListDTO> list(
    //@formatter:off
        	    @RequestBody FinancialDocumentSearchCriteriaDTO searchDTO, 
        	    @RequestParam(required = false) FilterRepositoryAttribute orderField, 
        	    @RequestParam(required = false) OrderType orderType) {
		//@formatter:on) {
	return this.financialDocumentRepositoryManager.searchFinancialDocuments(searchDTO, orderField, orderType, currentPrincipal());
    }

    @Override
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    @RequestMapping(value = "/{repositoryId}", method = RequestMethod.GET)
    public FinancialDocumentRepositoryDTO get(@PathVariable long repositoryId) {
	return this.financialDocumentRepositoryManager.getFinancialDocument(repositoryId, currentLoggedUserGroup());
    }

    @Override
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    @RequestMapping(value = "/{repositoryId}/timeline", method = RequestMethod.GET)
    public List<TimeLineStepDTO> getTimeLine(@PathVariable long repositoryId) {

	return financialDocumentRepositoryManager.getTimeLine(repositoryId, currentLoggedUserId());
    }

    @RequestMapping(path = "/{repositoryId}/attachment/{attachmentIndex}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM)
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    public byte[] getAttachment(@PathVariable long repositoryId, @PathVariable int attachmentIndex, HttpServletResponse response) {

	// PurchaseOrderInstance instance = JourneySession.getInstance(this);
	// retrieve instanceId from repository
	FileDTO fileDTO = attachmentManager.getAttachmentByFinancialDocumentId(repositoryId, attachmentIndex);

	response.addHeader("Content-Disposition", "attachment; filename=" + fileDTO.getFileName());
	response.setContentType(fileDTO.getMimeType());

	byte[] byteArray = null;
	try {
	    byteArray = StreamUtils.copyToByteArray(fileDTO.getInputStream());
	} catch (IOException e) {
	    throw getCtx().attachmentFailedReadStreamException(repositoryId);
	}

	return byteArray;
    }

    @RequiresPermissions(AppProperties.PERMISSION_INTEGRATION)
    @RequestMapping(value = "/paid", method = RequestMethod.PUT)
    public void paid(@QueryParam("financialDocumentNumber") String financialDocumentNumber) {

	this.getCtx().logInfo("[financialdocument] going to update status of financialdocument with number: " + financialDocumentNumber);

	if (StringUtils.isBlank(financialDocumentNumber)) {
	    throw this.getCtx().fieldValidationException(Arrays.asList("financialDocumentNumber"));
	}

	workflowManager.payFinancialDocument(financialDocumentNumber, currentLoggedUserId());
    }

}
