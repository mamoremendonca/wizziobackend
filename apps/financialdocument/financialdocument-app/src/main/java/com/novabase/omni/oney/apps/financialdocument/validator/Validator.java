package com.novabase.omni.oney.apps.financialdocument.validator;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.novabase.omni.oney.apps.financialdocument.AppContext;

/**
 * Validator template
 * 
 * @author NB24879
 *
 * @param <T>
 */
public interface Validator<T extends Object> {

    /**
     * This method will validate the target invoking the implemented method
     * getErrorFields, if this object have some error one exception will be throw.
     * 
     */
    public default void validate(T target) {

	final List<String> errorFields = this.getErrorFields(target);
	if (errorFields != null && errorFields.isEmpty() == false) {
	    throw this.getCtx().fieldValidationException(errorFields);
	}
	
    };

    public default boolean isInvalid(String str) {
	return StringUtils.isBlank(str);
    }
    
    public default boolean isInvalid(Long l) {
	return l == null || l == 0L;
    }

    public List<String> getErrorFields(T target);
    
    public AppContext getCtx();
    

}
