package com.novabase.omni.oney.apps.financialdocument.controller.ri;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.eclipse.gemini.blueprint.extensions.annotation.ServiceReference;
import org.osgi.service.component.annotations.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;
import com.novabase.omni.oney.apps.financialdocument.annotations.CurrentGroupAction;
import com.novabase.omni.oney.apps.financialdocument.controller.api.AttachmentResource;
import com.novabase.omni.oney.apps.financialdocument.dto.AttachmentDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.enums.DocumentClassType;
import com.novabase.omni.oney.apps.financialdocument.manager.AttachmentManager;

import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyMethod;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyReference;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.session.JourneySession;

@Component
@RSProvider(value = AppProperties.SERVICE_ADDRESS_ATTACHMENT_CONTROLLER)
@RestController
@RequestMapping(AppProperties.ADDRESS_ATTACHMENT_CONTROLLER)
public class AttachmentController extends AbstractAppController implements AttachmentResource {

    @ServiceReference
    private AttachmentManager attachmentManager;

    @CurrentGroupAction // validação de estado vs API é responsabilidade da journey 
    @RequestMapping(path = "/", method = RequestMethod.POST)
    @JourneyMethod(value = "Add Attachment")
    @RequiresPermissions(AppProperties.PERMISSION_UPDATE)
    public AttachmentDTO uploadAttachment(@PathVariable @JourneyReference final long instanceId, @QueryParam("type") final DocumentClassType type, final MultipartFile attachment) {
	
	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	return this.attachmentManager.saveAttachment(instance, attachment, type, this.currentLoggedUserId());
	
    }

    @RequestMapping(path = "/{attachmentIndex}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM)
    @JourneyMethod(value = "Get Attachment")
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    public byte[] getAttachment(@PathVariable @JourneyReference final long instanceId, @PathVariable final int attachmentIndex, final HttpServletResponse response) {

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	return this.attachmentManager.getAttachment(instance, attachmentIndex, response);
	
    }

    @CurrentGroupAction // validação de estado vs API é responsabilidade da journey 
    @RequestMapping(path = "/{attachmentIndex}", method = RequestMethod.DELETE)
    @JourneyMethod(value = "Add Attachment")
    @RequiresPermissions(AppProperties.PERMISSION_UPDATE)
    public void removeAttachment(@PathVariable @JourneyReference final long instanceId, @PathVariable final int attachmentIndex) {

	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	this.attachmentManager.removeAttachment(instance, attachmentIndex, this.currentLoggedUserId());
	
    }

}
