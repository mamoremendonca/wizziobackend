package com.novabase.omni.oney.apps.financialdocument.entity;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.financialdocument.json.serializer.NBLocalDateTimeSerializer;

public class Item {

    private int index;
    private Long itemPurchaseOrderId;
    private String code;
    private String description;
    private String costCenterCode;
    private Double amount;
    private Double unitPriceWithoutTax;
    private String taxCode;
    private String retentionCode;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime startDate;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime endDate;

    private String comment;

    public Item() {

	super();

	this.index = 0;
	this.amount = 0D;
	this.unitPriceWithoutTax = 0D;
	this.startDate = LocalDateTime.now();
	this.endDate = LocalDateTime.now();

    }

    public Long getItemPurchaseOrderId() {
	return itemPurchaseOrderId;
    }

    public void setItemPurchaseOrderId(Long itemPurchaseOrderId) {
	this.itemPurchaseOrderId = itemPurchaseOrderId;
    }

    public Double getUnitPriceWithoutTax() {
	return unitPriceWithoutTax;
    }

    public void setUnitPriceWithoutTax(Double unitPriceWithoutTax) {
	this.unitPriceWithoutTax = unitPriceWithoutTax;
    }

    public int getIndex() {
	return index;
    }

    public void setIndex(int index) {
	this.index = index;
    }

    public String getCode() {
	return code;
    }

    public void setCode(String code) {
	this.code = code;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getCostCenterCode() {
	return costCenterCode;
    }

    public void setCostCenterCode(String costCenterCode) {
	this.costCenterCode = costCenterCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTaxCode() {
	return taxCode;
    }

    public void setTaxCode(String taxCode) {
	this.taxCode = taxCode;
    }

    public LocalDateTime getStartDate() {
	return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
	this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
	return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
	this.endDate = endDate;
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public String getRetentionCode() {
	return retentionCode;
    }

    public void setRetentionCode(String retentionCode) {
	this.retentionCode = retentionCode;
    }

}
