package com.novabase.omni.oney.apps.financialdocument.provider;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.novabase.omni.oney.apps.financialdocument.agent.referencedata.ReferenceDataAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.agent.workflow.WorkflowAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.dto.ReferenceDataDTO;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentStatus;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentType;
import com.novabase.omni.oney.apps.financialdocument.mapper.ReferenceDataMapper;

public class ReferenceDataProvider {

    private ReferenceDataAgentImpl referenceDataAgent;

    private WorkflowAgentImpl workflowAgentImpl;

    public ReferenceDataProvider(final ReferenceDataAgentImpl referenceAgent) {
	this.referenceDataAgent = referenceAgent;
    }

    public ReferenceDataProvider(final WorkflowAgentImpl workflowAgent) {
	this.workflowAgentImpl = workflowAgent;
    }

    public List<ReferenceDataDTO> getPurchaseOrderType() {

	return ReferenceDataMapper.INSTANCE.toReferenceDTOList(Arrays.asList(FinancialDocumentType.values()).parallelStream().map(enumParam -> enumParam.name()).collect(Collectors.toList()));

    }

    public List<ReferenceDataDTO> getPurchaseOrderListStatus() {

	return ReferenceDataMapper.INSTANCE.toReferenceDTOList(Arrays.asList(FinancialDocumentStatus.values()).parallelStream().map(enumParam -> enumParam.name()).collect(Collectors.toList()));

    }
    
}
