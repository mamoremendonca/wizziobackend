package com.novabase.omni.oney.apps.financialdocument.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyMethod;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyReference;

/**
 * Annotation that specifies that a given method is only executable by the users
 * that are associated with currentGroup on
 * {@link PurchaseOrderInstance}.currentAction.userGroup of the process. <br>
 * Only usable when the method is already annotated with {@link JourneyMethod}
 * and the annotation {@link JourneyReference} is present in the method
 * signature. <br>
 * 
 * <i>The methods with this annotation will be intercepted by
 * ValidateUserAgainstCurrentActionUserGroupAspect</i>
 *
 * @author Wilian Moraes (wilian.moraes@novabase.pt)
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface CurrentGroupAction {

}
