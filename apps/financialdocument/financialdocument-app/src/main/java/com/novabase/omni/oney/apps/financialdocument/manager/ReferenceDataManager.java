package com.novabase.omni.oney.apps.financialdocument.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.util.CollectionUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.financialdocument.agent.referencedata.ReferenceDataAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.agent.workflow.WorkflowAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.dto.reference.DepartmentDTO;
import com.novabase.omni.oney.apps.financialdocument.enums.ReferenceDataType;
import com.novabase.omni.oney.apps.financialdocument.mapper.ReferenceDataMapper;

@Component(service = { ReferenceDataManager.class })
public class ReferenceDataManager {

    @Reference
    private CoreManager coreManager;

    @Reference
    private WorkflowAgentImpl workflowAgent;

    @Reference
    private ReferenceDataAgentImpl referenceAgent;

    public <T> Map<String, List<T>> getReferenceData(final List<String> referenceList) {

	final Map<String, List<T>> resultMap = new HashMap<>();
	if (!CollectionUtils.isEmpty(referenceList)) {

	    referenceList.stream().forEach(referenceName -> {

		if (ReferenceDataType.valueOf(referenceName) == ReferenceDataType.DEPARTMENT) {
		    resultMap.put(referenceName, this.getDepartments());
		} else {
		    resultMap.put(referenceName, ReferenceDataType.valueOf(referenceName).getData(this.referenceAgent));
		}

	    });

	}

	return resultMap;

    }

    @SuppressWarnings("unchecked")
    private <T> List<T> getDepartments() {

	final List<DepartmentDTO> departmentsFromService = ReferenceDataMapper.INSTANCE.departmnetMsDTOtoDepartmentDTOList(this.referenceAgent.getDepartments());
	final List<DepartmentDTO> userDepartments = ReferenceDataMapper.INSTANCE.stringToDepartmentDTOList(this.workflowAgent.getDepartments(referenceAgent.getHigherGroup()));

	if (userDepartments == null || userDepartments.isEmpty()) {
	    return (List<T>) new ArrayList<>();
	} else if (departmentsFromService == null || departmentsFromService.isEmpty()) {
	    return (List<T>) userDepartments;
	} else {

	    final List<DepartmentDTO> departments = new ArrayList<>();
	    userDepartments.stream().forEach(department -> {

		final int departmentIndex = departmentsFromService.indexOf(department);
		departments.add(departmentIndex >= 0 ? departmentsFromService.get(departmentIndex) : department);

	    });
	    return (List<T>) departments;

	}

    }

}
