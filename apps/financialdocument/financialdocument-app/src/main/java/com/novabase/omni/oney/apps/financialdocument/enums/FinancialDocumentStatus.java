package com.novabase.omni.oney.apps.financialdocument.enums;

import java.util.Arrays;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.FinancialDocumentStatusMs;

public enum FinancialDocumentStatus {
    
    IN_APPROVAL(FinancialDocumentStatusMs.IN_APPROVAL), 
    APPROVED(FinancialDocumentStatusMs.APPROVED), 
    REJECTED(FinancialDocumentStatusMs.REJECTED), 
    CANCELED(FinancialDocumentStatusMs.CANCELED),
    SAP_SUBMITTED(FinancialDocumentStatusMs.SAP_SUBMITTED),
    PAID(FinancialDocumentStatusMs.PAID);
    //@formatter:on

    private FinancialDocumentStatusMs msType;

    private FinancialDocumentStatus(final FinancialDocumentStatusMs msType) {
	this.msType = msType;
    }

    public FinancialDocumentStatusMs getMsType() {
	return msType;
    }
    
    public static FinancialDocumentStatus valueOf(final FinancialDocumentStatusMs msType){
	
	return Arrays.asList(FinancialDocumentStatus.values()).stream().
								filter(type -> type.getMsType().equals(msType)).
								findFirst().
								orElse(null);
	
    }
    
}
