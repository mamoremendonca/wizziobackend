package com.novabase.omni.oney.apps.financialdocument.controller.ri;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.eclipse.gemini.blueprint.extensions.annotation.ServiceReference;
import org.osgi.service.component.annotations.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;
import com.novabase.omni.oney.apps.financialdocument.annotations.OwnerAction;
import com.novabase.omni.oney.apps.financialdocument.controller.api.DocumentResource;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.manager.DocumentManager;

import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyMethod;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.annotation.JourneyReference;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.session.JourneySession;

@Component
@RSProvider(value = AppProperties.SERVICE_ADDRESS_DOCUMENT_CONTROLLER)
@RestController
@RequestMapping(AppProperties.ADDRESS_DOCUMENT_CONTROLLER)
public class DocumentController extends AbstractAppController  implements DocumentResource {

//    private static final String DEFAULT_ALLOWED_DOCUMENT_TYPE = "application/pdf";
    
    @ServiceReference
    private DocumentManager documentManager;
    
    @Override
    @OwnerAction
    @RequestMapping(path = "/", method = RequestMethod.POST)
    @JourneyMethod(value = "Upload Financial Document")
    @RequiresPermissions(AppProperties.PERMISSION_UPDATE)
    public FinancialDocumentDTO uploadDocument(@PathVariable @JourneyReference long instanceId, final MultipartFile document) {
	
	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
	return this.documentManager.saveDocument(instance, document);
	
    }
    
    @Override
    @RequestMapping(path = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM)
    @JourneyMethod(value = "Get Financial Document")
    @RequiresPermissions(AppProperties.PERMISSION_READ)
    public byte[] getDocument(@PathVariable @JourneyReference final long instanceId, final HttpServletResponse response) {
    
    	final FinancialDocumentInstance instance = JourneySession.getInstance(this);
    	return this.documentManager.getDocument(instance, response);
    	
    }
    
}
