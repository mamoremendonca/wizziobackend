package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentNotFoundException extends FinancialDocumentException {

    private static final long serialVersionUID = -408567019268003284L;

    public FinancialDocumentNotFoundException(final String errorCode, final Context ctx, final Object... args) {
	super(PlatformCode.NOT_FOUND, errorCode, ctx, args);
    }

    public static FinancialDocumentNotFoundException of(final Context ctx, final Long id) {
	return new FinancialDocumentNotFoundException(AppProperties.NOT_FOUND_EXCEPTION, ctx, id);
    }
    
}
