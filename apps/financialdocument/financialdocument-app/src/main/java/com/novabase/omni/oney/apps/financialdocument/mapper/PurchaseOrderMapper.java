package com.novabase.omni.oney.apps.financialdocument.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder.PurchaseOrderDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.PurchaseOrderMsDTO;

@Mapper
public interface PurchaseOrderMapper {

    public static final PurchaseOrderMapper INSTANCE = Mappers.getMapper(PurchaseOrderMapper.class);
    
    public PurchaseOrderDTO ToPurchaseOrderDTO(final PurchaseOrderMsDTO purchaseOrder);
    
}
