package com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder;

import java.time.LocalDateTime;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModelProperty;

public class ItemDTO extends DTO {
    
    @ApiModelProperty
    private int index;
    
    @ApiModelProperty
    private Long id;

    @ApiModelProperty
    private String code;

    @ApiModelProperty
    private String description;

    @ApiModelProperty
    private String costCenterCode;

    @ApiModelProperty
    private Double amount;

    @ApiModelProperty
    private Double unitPriceWithoutTax;

    @ApiModelProperty
    private String taxCode;

    @ApiModelProperty
    private LocalDateTime startDate;

    @ApiModelProperty
    private LocalDateTime endDate;

    @ApiModelProperty
    private String comment;

    public ItemDTO() {
	
	super();
	
	this.id = 0L;
	this.index = 0;
	this.amount = 0D;
	this.unitPriceWithoutTax = 0D;
	
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCostCenterCode() {
        return costCenterCode;
    }

    public void setCostCenterCode(String costCenterCode) {
        this.costCenterCode = costCenterCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getUnitPriceWithoutTax() {
        return unitPriceWithoutTax;
    }

    public void setUnitPriceWithoutTax(Double unitPriceWithoutTax) {
        this.unitPriceWithoutTax = unitPriceWithoutTax;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
    
}
