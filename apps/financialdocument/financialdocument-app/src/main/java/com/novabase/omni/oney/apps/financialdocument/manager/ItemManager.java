package com.novabase.omni.oney.apps.financialdocument.manager;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.financialdocument.AppContext;
import com.novabase.omni.oney.apps.financialdocument.dto.ItemDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.entity.Item;
import com.novabase.omni.oney.apps.financialdocument.mapper.FinancialDocumentInstanceMapper;
import com.novabase.omni.oney.apps.financialdocument.validator.AddItemValidator;

@Component(service = { ItemManager.class })
public class ItemManager {

    @Reference
    private AppContext ctx;

    @Reference
    private AddItemValidator addItemValidator;
    
    public ItemDTO addItem(final FinancialDocumentInstance instance, final ItemDTO itemDTO) {
	
	itemDTO.setPurchaseOrderType(instance.getFinancialDocumentHeader().getPurchaseOrderType());
	itemDTO.setFinancialDocumentType(instance.getFinancialDocumentHeader().getType());
	itemDTO.setInternalOrderCode(instance.getFinancialDocumentHeader().getInternalOrderCode());
	
	this.addItemValidator.validate(itemDTO);
	
	final Item newItem = FinancialDocumentInstanceMapper.INSTANCE.toItem(itemDTO);
	final Item lastItemById = instance.getItems().stream().
							max((it1, it2) -> it1.getIndex() - it2.getIndex()).
							orElse(null);
	
	newItem.setIndex(lastItemById != null ? lastItemById.getIndex() + 1 
					      : 1);
	
	instance.getItems().add(newItem);
	//@formatter:on
	instance.refreshTotal();
	instance.refreshTargets();
	
	return FinancialDocumentInstanceMapper.INSTANCE.toItemDTO(newItem);
	
    }

    public void updateItem(final FinancialDocumentInstance instance, final int index, final ItemDTO itemDTO) {
	
	itemDTO.setPurchaseOrderType(instance.getFinancialDocumentHeader().getPurchaseOrderType());
	itemDTO.setFinancialDocumentType(instance.getFinancialDocumentHeader().getType());
	itemDTO.setInternalOrderCode(instance.getFinancialDocumentHeader().getInternalOrderCode());
	
	this.addItemValidator.validate(itemDTO);
	
	final Item instanceItem = this.getItemByIndex(instance, index);
	
	FinancialDocumentInstanceMapper.INSTANCE.mergeItemDTO(instanceItem, itemDTO);

	instance.refreshTotal();
	instance.refreshTargets();
	
    }

    public void removeItem(final FinancialDocumentInstance instance, final int index) {

	final Item instanceItem = this.getItemByIndex(instance, index);
	
	instance.getItems().remove(instanceItem);
	instance.refreshTotal();
	instance.refreshTargets();
	
    }
    
    public void removeAllItem(final FinancialDocumentInstance instance) {
   	
	instance.getItems().clear();

	instance.refreshTotal();
	instance.refreshTargets();
   	
    }


    private Item getItemByIndex(final FinancialDocumentInstance instance, final int index) {

	if (instance.getItems() == null) {
	    throw ctx.itemNotFoundException(index, instance.getHeader().instanceId);
	}
	
	//@formatter:off
	final Item instanceItem =instance.getItems().stream().
							filter(item -> item.getIndex() == index).
							findFirst().
							orElse(null);
	//@formatter:on
	if (instanceItem == null) {
	    throw ctx.itemNotFoundException(index, instance.getHeader().instanceId);
	}

	return instanceItem;
    }

}
