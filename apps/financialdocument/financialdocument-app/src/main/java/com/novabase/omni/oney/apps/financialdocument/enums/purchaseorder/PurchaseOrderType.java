package com.novabase.omni.oney.apps.financialdocument.enums.purchaseorder;

import java.util.Arrays;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.PurchaseOrderTypeMs;


public enum PurchaseOrderType {
    
    CAPEX(PurchaseOrderTypeMs.CAPEX), 
    OPEX(PurchaseOrderTypeMs.OPEX), 
    CONTRACT(PurchaseOrderTypeMs.CONTRACT);
    
    private PurchaseOrderTypeMs msType;
    
    private PurchaseOrderType(final PurchaseOrderTypeMs msType) {
	this.msType = msType;
    }

    public PurchaseOrderTypeMs getMsType() {
	return msType;
    }
    
    public static PurchaseOrderType valueOf(PurchaseOrderTypeMs msType){
	
	return Arrays.asList(PurchaseOrderType.values()).stream().
								filter(type -> type.getMsType().equals(msType)).
								findFirst().
								orElse(null);
	
    }
}
