/*-
 * #%L
 * Archetype :: APP Vault
 * %%
 * Copyright (C) 2016 - 2018 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.financialdocument.controller.ri;

import java.util.ArrayList;

import org.eclipse.gemini.blueprint.extensions.annotation.ServiceReference;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.novabase.omni.oney.apps.financialdocument.AppContext;
import com.novabase.omni.oney.apps.financialdocument.AppProperties;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentFieldValidationException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentUserCannotExecuteActionException;
import com.novabase.omni.oney.apps.financialdocument.manager.CoreManager;

import io.digitaljourney.platform.modules.commons.Platform;
import io.digitaljourney.platform.modules.commons.domain.PlatformError;
import io.digitaljourney.platform.modules.commons.domain.PlatformErrorDetailBuilder;
import io.digitaljourney.platform.modules.security.api.principal.PrincipalDTO;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.api.JourneyWorkflowEngineResource;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.JourneyProcess;
import io.digitaljourney.platform.plugins.modules.journeyworkflowengine.gateway.aspect.controller.AbstractJourneyController;

/**
 * Abstract application controller which extends an
 * {@link AbstractJourneyController Abstract Journey Controller}.
 */
public abstract class AbstractAppController extends AbstractJourneyController<AppContext> implements JourneyProcess<FinancialDocumentInstance> {

    @ServiceReference
    private AppContext ctx;

    @ServiceReference
    private CoreManager coreManager;

    /**
     * Gets the application context.
     *
     * @return Context
     */
    @Override
    protected AppContext getCtx() {
	return this.ctx;
    }

    @Override
    public String journeyName() {
	return AppProperties.JOURNEY_NAME;
    }

    @Override
    public int majorVersion() {
	return AppProperties.JOURNEY_VERSION;
    }

    @Override
    public Class<FinancialDocumentInstance> getInstanceClass() {
	return FinancialDocumentInstance.class;
    }

    @Override
    public JourneyWorkflowEngineResource getJourneyEngine() {
	return journeyEngine();
    }

    protected Long currentLoggedUserId() {

	final PrincipalDTO principal = currentPrincipal();
	return new Long(principal.id);

    }

    protected String currentLoggedUsername() {

	final PrincipalDTO principal = currentPrincipal();
	return principal.name;

    }

    protected String currentLoggedUserGroup() {
	return this.coreManager.getUserGroup(currentLoggedUsername());
    }

    public boolean isCurrentUserTheInstanceOwner(FinancialDocumentInstance instance) {
	return true;
//	instance.getHeader().createdBy.equals(currentUser()); // TODO: create a configuration in order to bypass
	
	
								     // @OwnerAction
	// and @CurrentGroupAction aspects
    }

    public boolean isCurrentAllowedToModifyInstace(FinancialDocumentInstance instance) {
	return true; // TODO: uncomment and create a configuration in order to bypass @OwnerAction
	// and @CurrentGroupAction aspects
//	 return instance.currentAction.userGroup.equals(currentLoggedUserGroup());
    }

    // @formatter:off
	@ExceptionHandler(FinancialDocumentFieldValidationException.class)
	public ResponseEntity<PlatformError> handleException(FinancialDocumentFieldValidationException e) {

		this.getCtx().getPlatformInvocationManager().logError(e);
		final PlatformError error = Platform.buildError(e);
		error.details = new ArrayList<>();

		for (final String field : e.getErrorFields()) {
			error.details.add(new PlatformErrorDetailBuilder().withReference("field").withMessage(field).build());
		}

		return new ResponseEntity<PlatformError>(error, HttpStatus.valueOf(e.getStatusCode().getStatus()));
	}
	// @formatter:on

//	// @formatter:off
//	@ExceptionHandler(FinancialDocumentInvalidMediaTypeException.class)
//	public ResponseEntity<PlatformError> handleException(FinancialDocumentInvalidMediaTypeException e) {
//
//		this.getCtx().getPlatformInvocationManager().logError(e);
//		final PlatformError error = Platform.buildError(e);
//		error.details = new ArrayList<>();
//
//		for (final String field : e.getErrorFields()) {
//			error.details.add(new PlatformErrorDetailBuilder().withReference("field").withMessage(field).build());
//		}
//
//		return new ResponseEntity<PlatformError>(error, HttpStatus.valueOf(e.getStatusCode().getStatus()));
//	}
//	// @formatter:on

    public FinancialDocumentUserCannotExecuteActionException ownerActionUserCannotExecuteActionException(Long instanceId, String action) {
	return ctx.userCannotExecuteActionException(instanceId, currentUser(), action);
    }

    public FinancialDocumentUserCannotExecuteActionException currentGroupActionUserCannotExecuteActionException(Long instanceId, String action, String instanceCurrentUserGroup) {
	return ctx.userCannotExecuteActionException(instanceId, currentUser(), action, instanceCurrentUserGroup, currentLoggedUserGroup());
    }

}
