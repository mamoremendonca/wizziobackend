package com.novabase.omni.oney.apps.financialdocument.controller.api;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.web.multipart.MultipartFile;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentDTO;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentAttachmentNotFoundException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentFieldValidationException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentInvalidMediaTypeException;

import io.digitaljourney.platform.modules.commons.type.HttpStatusCode;
import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.ApiKeyAuthDefinition.ApiKeyLocation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.BasicAuthDefinition;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;

//@formatter:off
@Path(AppProperties.ADDRESS_DOCUMENT_CONTROLLER)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@SwaggerDefinition(
	securityDefinition = @SecurityDefinition(
		basicAuthDefinitions = {
			@BasicAuthDefinition(key = RSProperties.SWAGGER_BASIC_AUTH),
		},
		apiKeyAuthDefinitions = {
			@ApiKeyAuthDefinition(
				key = RSProperties.SWAGGER_BEARER_AUTH,
				name = RSProperties.HTTP_HEADER_API_KEY,
				in = ApiKeyLocation.HEADER)
			}
	),
	schemes = {
		SwaggerDefinition.Scheme.HTTP,
		SwaggerDefinition.Scheme.HTTPS,
		SwaggerDefinition.Scheme.DEFAULT
	},
	info = @Info(
		title = "Financial Documents Document API",
		description = "The Financial Documents Document API.",
		version = AppProperties.CURRENT_VERSION,
		license = @License(name = "Digital Journey License", url = "http://www.digitaljourney.io/license")
	),
	basePath = "bin/mvc.do/" + AppProperties.SERVICE_ADDRESS_DOCUMENT_CONTROLLER
)
@Api(
	value = "Financial Documents Document API",
	authorizations = {
		@Authorization(value = RSProperties.SWAGGER_BASIC_AUTH),
		@Authorization(value = RSProperties.SWAGGER_BEARER_AUTH)
	}
)
@ApiResponses(
	value = {
		@ApiResponse(code = HttpStatusCode.UNAUTHORIZED_CODE, message = RSProperties.SWAGGER_UNAUTHORIZED_MESSAGE),
		@ApiResponse(code = HttpStatusCode.FORBIDDEN_CODE, message = RSProperties.SWAGGER_FORBIDDEN_MESSAGE)
	}
)
public interface DocumentResource {

    @POST 
    @Path("/")
    @ApiOperation(value = "Upload a Document", notes = "Upload a Document and save the reference on the process", response = FinancialDocumentDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = FinancialDocumentDTO.class, message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentInvalidMediaTypeException.class, message = "Unsupported Media Type"), // TODO: validate how to return unsupported media type 415.
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public FinancialDocumentDTO uploadDocument(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId,
	    				       @ApiParam(value = "The Document", required = true) final MultipartFile document);
    
    @GET
    @Path("/")
    @ApiOperation(value = "Get Document", notes = "Get Document byte[] from blob")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK", response = byte.class, responseContainer = "Array"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentAttachmentNotFoundException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public byte[] getDocument(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId, final HttpServletResponse response);
    
}
