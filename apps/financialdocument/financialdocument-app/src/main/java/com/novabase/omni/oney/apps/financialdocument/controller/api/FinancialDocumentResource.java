package com.novabase.omni.oney.apps.financialdocument.controller.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;
import com.novabase.omni.oney.apps.financialdocument.dto.DefaultDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentHeaderDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.TimeLineStepDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.WorkflowDTO;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentCouldNotClaimException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentException;
import com.novabase.omni.oney.apps.financialdocument.exception.FinancialDocumentFieldValidationException;

import io.digitaljourney.platform.modules.commons.type.HttpStatusCode;
import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.ApiKeyAuthDefinition.ApiKeyLocation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.BasicAuthDefinition;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;

//@formatter:off
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path(AppProperties.ADDRESS_FINANCIAL_DOCUMENT_CONTROLLER)
@SwaggerDefinition(
	securityDefinition = @SecurityDefinition(
		basicAuthDefinitions = {
			@BasicAuthDefinition(key = RSProperties.SWAGGER_BASIC_AUTH),
		},
		apiKeyAuthDefinitions = {
			@ApiKeyAuthDefinition(
				key = RSProperties.SWAGGER_BEARER_AUTH,
				name = RSProperties.HTTP_HEADER_API_KEY,
				in = ApiKeyLocation.HEADER)
			}
	),
	schemes = {
		SwaggerDefinition.Scheme.HTTP,
		SwaggerDefinition.Scheme.HTTPS,
		SwaggerDefinition.Scheme.DEFAULT
	},
	info = @Info(
		title = "Financial Document API",
		description = "The Financial Document API.",
		version = AppProperties.CURRENT_VERSION,
		license = @License(name = "Digital Journey License", url = "http://www.digitaljourney.io/license")
	),
	basePath = "bin/mvc.do/" + AppProperties.ADDRESS_FINANCIAL_DOCUMENT_CONTROLLER
)
@Api(
	value = "Financial Document App  API",
	authorizations = {
		@Authorization(value = RSProperties.SWAGGER_BASIC_AUTH),
		@Authorization(value = RSProperties.SWAGGER_BEARER_AUTH)
	}
)
@ApiResponses(
	value = {
		@ApiResponse(code = HttpStatusCode.UNAUTHORIZED_CODE, message = RSProperties.SWAGGER_UNAUTHORIZED_MESSAGE),
		@ApiResponse(code = HttpStatusCode.FORBIDDEN_CODE, message = RSProperties.SWAGGER_FORBIDDEN_MESSAGE)
	}
)
//@formatter:on
public interface FinancialDocumentResource {

    @GET
    @Path("/init")
    @ApiOperation(value = "Init Journey", notes = "Initializes Financial Document Journey")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
    public void init();
    
    @POST
    @Path("/")
    @ApiOperation(value = "Creates a process", notes = "Creates a new process instance", response = FinancialDocumentDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = FinancialDocumentDTO.class, message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public FinancialDocumentDTO createProcess();
    
    @GET
    @Path("/copy/{repositoryId}")
    @ApiOperation(value = "Copy a process", notes = "Creates a new process instance based on existing Financial Document", response = FinancialDocumentDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = FinancialDocumentDTO.class, message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public FinancialDocumentDTO copyFinancialDocument(@ApiParam(value = "The FinancialDocument repository Id to be copy", required = true) @PathParam("repositoryId") final Long repositoryId);
    
    @PUT
    @Path("/{instanceId}/draft")
    @ApiOperation(value = "Update Process Header Draf", notes = "Updates an existing process header in Draft", response = FinancialDocumentDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = FinancialDocumentDTO.class, message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public FinancialDocumentDTO updateProcessHeaderDraft(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId,
	    						 @ApiParam(value = "The FinancialDocumentHeader to be updated", required = true) final FinancialDocumentHeaderDTO financialDocumentHeaderDTO);

    @PUT
    @Path("/{instanceId}/creation")
    @ApiOperation(value = "Update Process Header Creation", notes = "Updates an existing process header in Creation", response = FinancialDocumentDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = FinancialDocumentDTO.class, message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public FinancialDocumentDTO updateProcessHeaderCreation(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId,
	    					    	    @ApiParam(value = "The FinancialDocumentHeader to be updated", required = true) final FinancialDocumentHeaderDTO financialDocumentHeaderDTO);
   
    @PUT
    @Path("/{instanceId}/purchaseorder")
    @ApiOperation(value = "Associate Purchase Order to Financial Document", notes = "Associate an aproved Purchase Order to Financial Document", response = FinancialDocumentDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = FinancialDocumentDTO.class, message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public FinancialDocumentDTO associatePurchaseOrder(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId,
	    					       @ApiParam(value = "The Purchase Order number to be associate") @QueryParam("number") final String number);
    
    @PUT
    @Path("/{instanceId}/financialdocument")
    @ApiOperation(value = "Associate Financial Document to another Financial Document", notes = "Associate an aproved Financial Document to another Financial Document", response = FinancialDocumentDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = FinancialDocumentDTO.class, message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public FinancialDocumentDTO associateFinancialDocument(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId,
	    					       @ApiParam(value = "The FinancialDocument Number to be associate") @QueryParam("number") final String number);
 
    @GET
    @Path("/{instanceId}")
    @ApiOperation(value = "Get Process Data", notes = "Retrieves the process data", response = FinancialDocumentDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = FinancialDocumentDTO.class, message = "OK")})
    public FinancialDocumentDTO getProcess(@PathParam("instanceId") final long instanceId);
    
    @PUT
    @Path("/{instanceId}/submit/creation")
    @ApiOperation(value = "Submit process to creation", notes = "Submit an existing process to creation", response = WorkflowDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = FinancialDocumentHeaderDTO.class ,message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public FinancialDocumentHeaderDTO submitToCreation(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId);
    
    @PUT
    @Path("/{instanceId}/submit/approval")
    @ApiOperation(value = "Submit process to approval", notes = "Submit an existing process to approval workflow", response = WorkflowDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = FinancialDocumentHeaderDTO.class ,message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public FinancialDocumentHeaderDTO submitToApproval(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId);
    
    @PUT
    @Path("/{instanceId}/approve")
    @ApiOperation(value = "Approve the process to the next step", notes = "Approve the process to the next step in the workflow", response = DefaultDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = WorkflowDTO.class, message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public WorkflowDTO approveFinancialDocument(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId,
	    				    	@ApiParam(value = "Comments of the evaluation", required = true) final String comment);
    
    @PUT
    @Path("/{instanceId}/return")
    @ApiOperation(value = "Return the process to the previous step", notes = "Approve the process to the previous step in the workflow", response = DefaultDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = WorkflowDTO.class, message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public WorkflowDTO returnFinancialDocument(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId,
	    				       @ApiParam(value = "Comments of the evaluation", required = true) final String comment);
    
    @PUT
    @Path("/{instanceId}/reject")
    @ApiOperation(value = "Reject the process", notes = "Reject the process and finalize approval workflow", response = DefaultDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, response = DefaultDTO.class, message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public DefaultDTO rejectFinancialDocument(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId,
	    				      @ApiParam(value = "Comments of the evaluation", required = true) final String comment);
    
    @PUT
    @Path("/{instanceId}/cancel")
    @ApiOperation(value = "Cancel the process", notes = "Cancel the process", response = DefaultDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "OK"),
	    		    @ApiResponse(code = 400, response = FinancialDocumentFieldValidationException.class, message = "Bad Request"),
	    		    @ApiResponse(code = 500, response = FinancialDocumentException.class, message = "Internal Server Error") })
    public void cancelFinancialDocument(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1") @PathParam("instanceId") final long instanceId);
    
    @GET
    @Path("/{instanceId}/timeline")
    @ApiOperation(value = "Get Process Timeline", notes = "Retrieves the Timeline", responseContainer = "List", response = TimeLineStepDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, responseContainer = "List", response = TimeLineStepDTO.class, message = "OK")})
    public List<TimeLineStepDTO> getTimeLine(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1")@PathParam("instanceId") final long instanceId);
    
    @PUT
    @Path("/{instanceId}/claim")
    @ApiOperation(value = "Claim Process to the Group of current user", notes = "Claim Process to the Group of current user")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK"),
	    		   @ApiResponse(code = 400, response = FinancialDocumentCouldNotClaimException.class, message = "NOTOK")})
    public void claimProcess(@ApiParam(value = "The unique identifier of the process instance", required = true, example = "1")@PathParam("instanceId") final long instanceId);

}
