package com.novabase.omni.oney.apps.financialdocument.manager;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.financialdocument.AppContext;
import com.novabase.omni.oney.apps.financialdocument.agent.purchaseorderrepository.PurchaseOrderRepositoryAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder.PurchaseOrderDTO;

@Component(service = { PurchaseOrderRepositoryManager.class })
public class PurchaseOrderRepositoryManager {

    @Reference
    private AppContext ctx;

    @Reference
    private PurchaseOrderRepositoryAgentImpl purchaseOrderRepositoryAgentImpl;

    public PurchaseOrderDTO getApprovedPurchaseOrderByNumber(final String number) {
	return this.purchaseOrderRepositoryAgentImpl.getApprovedPurchaseOrderByNumber(number);
    }
    
    public PurchaseOrderDTO getPurchaseOrderById(final Long purchaseOrderId) {
   	return this.purchaseOrderRepositoryAgentImpl.getPurchaseOrderById(purchaseOrderId);
       }
    
}
