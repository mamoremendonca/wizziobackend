package com.novabase.omni.oney.apps.financialdocument.dto.purchaseorder;

import java.util.ArrayList;
import java.util.List;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.apps.financialdocument.enums.purchaseorder.PurchaseOrderType;

import io.swagger.annotations.ApiModelProperty;

public class PurchaseOrderDTO extends DTO {

    @ApiModelProperty
    private Long id;
    
    @ApiModelProperty
    public Long instanceId;
    
    @ApiModelProperty
    private Long number;

    @ApiModelProperty(readOnly = true)
    private String friendlyNumber;

    @ApiModelProperty
    private PurchaseOrderType type;

    @ApiModelProperty
    private String supplierCode;

    @ApiModelProperty
    private String description;

    @ApiModelProperty
    private String departmentCode;

    @ApiModelProperty
    private String projectCode;

    @ApiModelProperty
    private String internalOrderCode;

    @ApiModelProperty
    private Double totalWithoutTax;

    @ApiModelProperty
    private String userGroup;

    @ApiModelProperty
    private List<ItemDTO> items;

    public PurchaseOrderDTO() {

	super();

	this.id = 0L;
	this.number = 0L;
	this.totalWithoutTax = 0D;
	this.items = new ArrayList<ItemDTO>();

    }

    public PurchaseOrderType getType() {
	return type;
    }

    public void setType(PurchaseOrderType type) {
	this.type = type;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Long getNumber() {
	return number;
    }

    public void setNumber(Long number) {
	this.number = number;
    }

    public String getFriendlyNumber() {
	return friendlyNumber;
    }

    public void setFriendlyNumber(String friendlyNumber) {
	this.friendlyNumber = friendlyNumber;
    }

    public String getSupplierCode() {
	return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
	this.supplierCode = supplierCode;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getDepartmentCode() {
	return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
	this.departmentCode = departmentCode;
    }

    public String getProjectCode() {
	return projectCode;
    }

    public void setProjectCode(String projectCode) {
	this.projectCode = projectCode;
    }

    public String getInternalOrderCode() {
	return internalOrderCode;
    }

    public void setInternalOrderCode(String internalOrderCode) {
	this.internalOrderCode = internalOrderCode;
    }

    public Double getTotalWithoutTax() {
	return totalWithoutTax;
    }

    public void setTotalWithoutTax(Double totalWithoutTax) {
	this.totalWithoutTax = totalWithoutTax;
    }

    public List<ItemDTO> getItems() {
	return items;
    }

    public void setItems(List<ItemDTO> items) {
	this.items = items;
    }

    public String getUserGroup() {
	return userGroup;
    }

    public void setUserGroup(String userGroup) {
	this.userGroup = userGroup;
    }

}
