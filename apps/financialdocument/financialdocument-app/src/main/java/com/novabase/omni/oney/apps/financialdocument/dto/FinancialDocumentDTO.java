package com.novabase.omni.oney.apps.financialdocument.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.novabase.omni.oney.apps.financialdocument.enums.WorkFlowActionType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class FinancialDocumentDTO extends DefaultDTO implements Serializable {

    private static final long serialVersionUID = 5500972246182866605L;

    @ApiModelProperty
    private FinancialDocumentHeaderDTO financialDocumentHeader;
    
    @ApiModelProperty
    private List<ItemDTO> items;
    
    private List<WorkFlowActionType> workFlowAvailableActions;

    public FinancialDocumentDTO() {
	
	super();
	
	this.financialDocumentHeader = new FinancialDocumentHeaderDTO();
	this.items = new ArrayList<ItemDTO>();
	this.workFlowAvailableActions = new ArrayList<WorkFlowActionType>();
	
    }

    public FinancialDocumentHeaderDTO getFinancialDocumentHeader() {
        return financialDocumentHeader;
    }

    public void setFinancialDocumentHeader(FinancialDocumentHeaderDTO financialDocumentHeader) {
        this.financialDocumentHeader = financialDocumentHeader;
    }

    public List<ItemDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemDTO> items) {
        this.items = items;
    }

    public List<WorkFlowActionType> getWorkFlowAvailableActions() {
        return workFlowAvailableActions;
    }

    public void setWorkFlowAvailableActions(List<WorkFlowActionType> workFlowAvailableActions) {
        this.workFlowAvailableActions = workFlowAvailableActions;
    }
    
}
