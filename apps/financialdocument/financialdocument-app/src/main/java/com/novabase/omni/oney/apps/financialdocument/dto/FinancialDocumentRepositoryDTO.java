package com.novabase.omni.oney.apps.financialdocument.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.novabase.omni.oney.modules.workflow.service.api.dto.WorkflowDTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class FinancialDocumentRepositoryDTO extends DTO implements Serializable {

    private static final long serialVersionUID = 2710847434963660265L;

    @ApiModelProperty
    private FinancialDocumentHeaderDTO financialDocumentHeader;
    
    @ApiModelProperty
    private boolean canClaim;
    
    @ApiModelProperty
    private Long currentInstanceId;

    @ApiModelProperty
    private List<ItemDTO> items;
    
    @JsonIgnore
    private WorkflowDTO workflow;

    public FinancialDocumentRepositoryDTO() {
	
	super();
	
	this.financialDocumentHeader = new FinancialDocumentHeaderDTO();
	this.canClaim = Boolean.FALSE;
	this.currentInstanceId = 0L;
	this.items = new ArrayList<ItemDTO>();
	this.workflow = new WorkflowDTO();
	
    }

    public FinancialDocumentHeaderDTO getFinancialDocumentHeader() {
        return financialDocumentHeader;
    }

    public void setFinancialDocumentHeader(FinancialDocumentHeaderDTO financialDocumentHeader) {
        this.financialDocumentHeader = financialDocumentHeader;
    }

    public boolean isCanClaim() {
        return canClaim;
    }

    public void setCanClaim(boolean canClaim) {
        this.canClaim = canClaim;
    }

    public Long getCurrentInstanceId() {
        return currentInstanceId;
    }

    public void setCurrentInstanceId(Long currentInstanceId) {
        this.currentInstanceId = currentInstanceId;
    }

    public List<ItemDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemDTO> items) {
        this.items = items;
    }

    public WorkflowDTO getWorkflow() {
        return workflow;
    }

    public void setWorkflow(WorkflowDTO workflow) {
        this.workflow = workflow;
    }
    
}
