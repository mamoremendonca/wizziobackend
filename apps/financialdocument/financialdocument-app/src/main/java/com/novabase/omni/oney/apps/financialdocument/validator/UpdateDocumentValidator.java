package com.novabase.omni.oney.apps.financialdocument.validator;

import java.util.ArrayList;
import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.springframework.web.multipart.MultipartFile;

import com.novabase.omni.oney.apps.financialdocument.AppContext;

@Component(service = { UpdateDocumentValidator.class })
public class UpdateDocumentValidator implements Validator<MultipartFile> {

    public static final String DEFAULT_SUPPORTED_DOCUMENT_MEDIA_TYPE = "application/pdf"; 
    
    @Reference
    private AppContext ctx;
    
    @Override
    public List<String> getErrorFields(final MultipartFile target) {

	final List<String> errors = new ArrayList<String>();
	if (target.getContentType().equals(DEFAULT_SUPPORTED_DOCUMENT_MEDIA_TYPE) == Boolean.FALSE) {
	    throw this.ctx.invalidDocumentTypeException(target.getOriginalFilename(), target.getContentType(), DEFAULT_SUPPORTED_DOCUMENT_MEDIA_TYPE);
	}
	
	return errors;
	
    }

    @Override
    public AppContext getCtx() {
	return this.ctx;
    }

}
