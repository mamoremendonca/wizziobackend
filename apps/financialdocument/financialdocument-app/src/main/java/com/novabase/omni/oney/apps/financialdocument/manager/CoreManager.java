package com.novabase.omni.oney.apps.financialdocument.manager;

import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;
import com.novabase.omni.oney.apps.financialdocument.agent.core.CoreAgentImpl;

import io.digitaljourney.platform.modules.cache.api.Cache;

@Component(service = { CoreManager.class })
public class CoreManager {

    private static final String REFRESH_TOKEN_CACHE_REF_NAME = "refreshTokenCache"; // TODO; validar se o nome tem que ser refreshtoken

    @Reference(name = REFRESH_TOKEN_CACHE_REF_NAME, target = "(cacheName=" + AppProperties.CACHE_USER_PROPERTIES + ")")
    private volatile Cache cache;

    @Reference
    private CoreAgentImpl coreAgent;

    public void createJourney() {
	this.coreAgent.createBlueprint(AppProperties.JOURNEY_NAME, AppProperties.JOURNEY_VERSION, "/financialdocument.json");
    }

    public String getUserGroup(final String username) {

	if (StringUtils.isEmpty(username)) {
	    return StringUtils.EMPTY;
	}

	// Retrieve (if exists) User Properties
	final Properties props = this.getProperties(username);
	// Returns Group if found
	return props != null && !props.isEmpty() && props.containsKey(AppProperties.CACHE_KEY_GROUP) ? (String) props.get(AppProperties.CACHE_KEY_GROUP) : null;
	// (GroupDTO)props.get(AppProperties.CACHE_KEY_GROUP) : null);
    }

    public String getDraftGroup() {
	return coreAgent.getDraftGroup();
    }

    public String getCreationGroup() {
	return coreAgent.getCreationGroup();
    }

    public Double getPercentageLimitToAssociatePO() {
	return coreAgent.getPercentageLimitToAssociatePO();
    }

    private Properties getProperties(String username) {

	final Properties props = cache.get(username, Properties.class);
	return props != null ? props : new Properties();

    }
    
    public boolean financialDocumentNumberAlreadyExists(String number) {
	return this.coreAgent.financialDocumentNumberAlreadyExists(number);
    }

}
