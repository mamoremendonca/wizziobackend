package com.novabase.omni.oney.apps.financialdocument.validator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.apps.financialdocument.AppContext;
import com.novabase.omni.oney.apps.financialdocument.agent.referencedata.ReferenceDataAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.dto.FinancialDocumentRepositoryDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.manager.CoreManager;
import com.novabase.omni.oney.apps.financialdocument.manager.FinancialDocumentRepositoryManager;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierGeneralDataMsDTO;

@Component(service = { SubmitToCreationValidator.class })
public class SubmitToCreationValidator implements Validator<FinancialDocumentInstance> {

    public static final int FINANCIAL_DOCUMENT_NUMBER_LENGTH_LIMIT = 25;
    
    @Reference
    private AppContext ctx;

    @Reference
    private ReferenceDataAgentImpl referenceDataAgentImpl;

    @Reference
    private FinancialDocumentRepositoryManager financialDocumentRepositoryManager;

    @Reference
    private CoreManager coreManager;

    // TODO: think about use RulesEngine
    //@formatter:off
    @Override
    public List<String> getErrorFields(FinancialDocumentInstance instance) {
	List<String> errors = new ArrayList<String>();
	
	if(isFinancialDocumentNumberUnavailable(instance.getFinancialDocumentHeader().getNumber())) {
	    // this exception is different of the others because the FE must know why this field is invalid
	   throw ctx.financialDocumentNumberAlreadyExistsException(instance.getHeader().instanceId, instance.getFinancialDocumentHeader().getNumber());
	}

	if (instance.getFinancialDocumentHeader() == null) {
	    errors.add("financialDocumentHeader");
	}
	
	if(instance.getFinancialDocumentHeader() != null 
		&& isInvalid(instance.getFinancialDocumentHeader().getDocumentId())) {
	    errors.add("documentId");
	}

	if (instance.getFinancialDocumentHeader() != null 
		&& instance.getFinancialDocumentHeader().getType() == null) {
	    errors.add("type");
	}

	if (instance.getFinancialDocumentHeader() != null 
		&& (isInvalid(instance.getFinancialDocumentHeader().getNumber()) 
			|| instance.getFinancialDocumentHeader().getNumber().length() > FINANCIAL_DOCUMENT_NUMBER_LENGTH_LIMIT)) {
	    errors.add("number");
	}

	if (instance.getFinancialDocumentHeader() != null 
		&& (isInvalid(instance.getFinancialDocumentHeader().getSupplierCode())
			|| supplierIsValid(instance.getFinancialDocumentHeader().getSupplierCode()) == false)) {
	    errors.add("supplierCode");
	}

	return errors;
    }
    //@formatter:on

    @Override
    public AppContext getCtx() {
	return ctx;
    }

    private boolean supplierIsValid(String supplierCode) {
	try {
	    SupplierGeneralDataMsDTO supplierMarketInfo = referenceDataAgentImpl.getSupplierMarketInfo(supplierCode);

	    if (supplierMarketInfo == null || StringUtils.isBlank(supplierMarketInfo.nif)) {
		return false;
	    }

	} catch (Exception e) {
	    return false;
	}

	return true;
    }

    private boolean isFinancialDocumentNumberUnavailable(String number) {

	// Validate on processContinuity process that are not canceled or rejected or in
	// state draft
	if (StringUtils.isNotBlank(number) && coreManager.financialDocumentNumberAlreadyExists(number)) {
	    return true;
	}

	// Validate on repository process that are not canceled or rejected
	try {
	    FinancialDocumentRepositoryDTO financialDocumentNotCanceledByNumber = financialDocumentRepositoryManager.getFinancialDocumentNotCanceledNotRejectedByNumber(number);

	    //@formatter:off
	    if(financialDocumentNotCanceledByNumber != null 
		    && financialDocumentNotCanceledByNumber.getFinancialDocumentHeader().getRepositoryId() != null 
		    && financialDocumentNotCanceledByNumber.getFinancialDocumentHeader().getRepositoryId().equals(0L) == false) {
		return true;
	    }
	    //@formatter:on

	} catch (Exception e) {
	}

	return false;
    }
}
