package com.novabase.omni.oney.apps.financialdocument.manager;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartFile;

import com.novabase.omni.oney.apps.financialdocument.AppContext;
import com.novabase.omni.oney.apps.financialdocument.agent.blob.BlobAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.agent.core.CoreAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.agent.financialdocumentrepository.FinancialDocumentRepositoryAgentImpl;
import com.novabase.omni.oney.apps.financialdocument.dto.AttachmentDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.AttachmentRepositoryDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.FileDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.UserInformationDTO;
import com.novabase.omni.oney.apps.financialdocument.entity.Attachment;
import com.novabase.omni.oney.apps.financialdocument.entity.CurrentAction;
import com.novabase.omni.oney.apps.financialdocument.entity.FinancialDocumentInstance;
import com.novabase.omni.oney.apps.financialdocument.enums.DocumentClassType;
import com.novabase.omni.oney.apps.financialdocument.mapper.FinancialDocumentInstanceMapper;

import io.digitaljourney.platform.plugins.modules.blob.service.api.exception.BlobNotFoundException;

@Component(service = { AttachmentManager.class })
public class AttachmentManager {

    @Reference
    private AppContext ctx;

    @Reference
    private BlobAgentImpl blobAgentImpl;

    @Reference
    private CoreAgentImpl coreAgentImpl;
    
    @Reference
    private FinancialDocumentRepositoryAgentImpl financialDocumentRepositoryAgentImpl;

    public AttachmentDTO saveAttachment(final FinancialDocumentInstance instance, final MultipartFile attachmentMultipartFile, final DocumentClassType type, final Long userId) {
	
	InputStream attachmentStream = null;
	try {
	    attachmentStream = attachmentMultipartFile.getInputStream();
	} catch (IOException e) {
	    throw this.ctx.attachmentFailedReadStreamException(instance.getHeader().instanceId);
	}
	
	return this.saveAttachment(instance, attachmentStream, type, userId, attachmentMultipartFile.getContentType(), attachmentMultipartFile.getOriginalFilename(), attachmentMultipartFile.getSize());
	
    }
    
    public AttachmentDTO saveAttachment(final FinancialDocumentInstance instance, final InputStream attachmentInputStream, DocumentClassType type, final Long userId, final String contentType, final String originalFileName, final Long size) {

	type = DocumentClassType.FINANCIAL_DOCUMENT_ATTACHMENT;

	if (instance.getCurrentAction() == null) {
	    instance.setCurrentAction(new CurrentAction());
	}

	final Attachment attachment = new Attachment();
	attachment.setMimeType(contentType);
	attachment.setOwnerId(userId);
	attachment.setName(originalFileName);
	attachment.setDate(LocalDateTime.now());
	attachment.setDocumentClass(type);

	try {
	    attachment.setBlobId(this.blobAgentImpl.saveBlob(attachmentInputStream, originalFileName, new Float(size)));
	} catch (Exception e) {
	    throw this.ctx.failedToUploadBlobException(instance.getHeader().instanceId, e.getMessage());
	}

	attachment.setIndex(this.getNextAttachmentIndex(instance));
	instance.getCurrentAction().getAttachments().add(attachment);

	final UserInformationDTO userInformation = this.coreAgentImpl.getUserInformations(attachment.getOwnerId());
	
	final AttachmentDTO attachmentDTO = FinancialDocumentInstanceMapper.INSTANCE.toAttachmentDTO(attachment);
	attachmentDTO.setOwnerName(userInformation.getCompleteName());
	attachmentDTO.setCanBeDeletedByCurrentUser(Boolean.TRUE);

	return attachmentDTO;
	
    }

    public byte[] getAttachment(final FinancialDocumentInstance instance, final int attachmentIndex, final HttpServletResponse response) {
	
	final FileDTO fileDTO = this.getAttachment(instance, attachmentIndex);

	response.addHeader("Content-Disposition", "attachment; filename=" + fileDTO.getFileName());
	response.setContentType(fileDTO.getMimeType());

	byte[] byteArray = null;
	try {
	    byteArray = StreamUtils.copyToByteArray(fileDTO.getInputStream());
	} catch (IOException e) {
	    throw this.ctx.attachmentFailedReadStreamException(instance.getHeader().instanceId);
	}

	return byteArray;
	
    }
    
    public FileDTO getAttachment(final FinancialDocumentInstance instance, final int attachmentIndex) {
	
	final Attachment attachment = this.getAttachmentByIndex(instance, attachmentIndex);
	
	InputStream blobInputStream = null;
	try {
	    blobInputStream = this.blobAgentImpl.readFileFromBlob(attachment.getBlobId());
	} catch (Exception e) {
	    throw this.ctx.failedToRetrieveBlobException(attachmentIndex, instance.getHeader().instanceId, e.getMessage());
	}

	final FileDTO fileDTO = new FileDTO();
	fileDTO.setInputStream(blobInputStream);
	fileDTO.setMimeType(attachment.getMimeType());
	fileDTO.setFileName(attachment.getName());

	return fileDTO;
	
    }

    public void removeAttachment(final FinancialDocumentInstance instance, final int attachmentIndex, final Long loggedUserId) {
	
	final Attachment attachment = this.getAttachmentByIndex(instance, attachmentIndex);
	if (!attachment.getOwnerId().equals(loggedUserId)) {

	    final UserInformationDTO currentUserInformation = this.coreAgentImpl.getUserInformations(loggedUserId);
	    throw this.ctx.user_couldNotRemoveAttachmentException(currentUserInformation.getUserName(), attachmentIndex, instance.getHeader().instanceId);
	    
	}

	if (instance.getCurrentAction().getAttachments().contains(attachment)) {
	    instance.getCurrentAction().getAttachments().remove(attachment);
	} else {

	    // Is not possible remove attachments that are already submitted to the
	    // repository, if this became possible, we have to update this information on
	    // repository to
	    final UserInformationDTO currentUserInformation = this.coreAgentImpl.getUserInformations(loggedUserId);
	    throw this.ctx.step_couldNotRemoveAttachmentException(currentUserInformation.getUserName(), attachmentIndex, instance.getHeader().instanceId);
	    
	}

	try {
	    this.blobAgentImpl.removeFileFromBlob(attachment.getBlobId()); 
	} catch (BlobNotFoundException e) {
	    this.ctx.logError(e.getMessage());
	} catch (Exception e) {
	    throw ctx.failedToRemoveBlobException(attachmentIndex, instance.getHeader().instanceId, e.getMessage());
	}

    }

    public FileDTO getAttachmentByFinancialDocumentId(final Long financialDocumentId, final int attachmentIndex) {
	
	AttachmentRepositoryDTO attachment = this.financialDocumentRepositoryAgentImpl.getFinancialDocumentAttachment(financialDocumentId, attachmentIndex);

	InputStream fileInputStream = null;
	if (StringUtils.isBlank(attachment.getExternalId())) {
	
	    try {
		fileInputStream = this.blobAgentImpl.readFileFromBlob(attachment.getBlobId());
	    } catch (Exception e) {
		throw ctx.failedToRetrieveBlobException(attachmentIndex, financialDocumentId, e.getMessage());
	    }
	    
	} else {
	    
	    try {
		fileInputStream = this.coreAgentImpl.getDocumentByIdFromECM(attachment.getExternalId());
	    } catch (Exception e) {
		throw ctx.failedToRetrieveFileECMException(attachmentIndex, financialDocumentId, e.getMessage());
	    }
	    
	}
	
	FileDTO fileDTO = new FileDTO();
	fileDTO.setInputStream(fileInputStream);
	fileDTO.setMimeType(attachment.getMimeType());
	fileDTO.setFileName(attachment.getName());

	return fileDTO;
	
    }

    /**
     * Finds and return the attachment on instance
     * 
     * @param instance
     * @param attachmentIndex
     * @return
     */
    private Attachment getAttachmentByIndex(final FinancialDocumentInstance instance, final int attachmentIndex) {

	Attachment resultAttachment = null;
	
	// verify if the attachment is on currentAction
	if (instance.getCurrentAction() != null) {
	    
	    resultAttachment = instance.getCurrentAction().getAttachments().stream().
		    								filter(attachment -> attachment.getIndex() == attachmentIndex).
		    								findFirst().
		    								orElse(null);
	    
	}

	if (resultAttachment == null) {
	    
	    // if the attachment is not on current action, we verify on the attachment list
	    // that already were submitted to the repository
	    if (instance.getAttachments() != null) {
        	
		resultAttachment = instance.getAttachments().stream().
                                				filter(attachment -> attachment.getIndex() == attachmentIndex).
                                				findFirst().
                                				orElse(null);
        	    
	    }
        	
	}
	
	if (resultAttachment == null) {
	    
	    // throw exception if the attachment does not exists on the current instance
	    throw ctx.attachmentNotFoundException(attachmentIndex, instance.getHeader().instanceId);
	    
	}
	
	return resultAttachment;
		
    }

    private int getNextAttachmentIndex(final FinancialDocumentInstance instance) {

	int nextIndex = 1;

	// verify the max value of attachmentIndex on the currentAction
	if (instance.getCurrentAction() != null && 
		!instance.getCurrentAction().getAttachments().isEmpty()) {
	    
	    final Attachment attachment = instance.getCurrentAction().getAttachments().stream().
		    									sorted((a1, a2) -> a2.getIndex() - a1.getIndex()).
		    									findFirst().
		    									get();
	    
	    if (attachment.getIndex() >= nextIndex) {
		nextIndex = attachment.getIndex() + 1;
	    }
	    
	}

	// verify the max value of attachmentIndex on the attachment list that already were
	// submitted to the repository
	if (!instance.getAttachments().isEmpty()) {
	    
	    final Attachment attachment = instance.getAttachments().stream().
		    							sorted((a1, a2) -> a2.getIndex() - a1.getIndex()).
		    							findFirst().
		    							get();
	    
	    if (attachment.getIndex() >= nextIndex) {
		nextIndex = attachment.getIndex() + 1;
	    }
	    
	}

	return nextIndex;
	
    }

}
