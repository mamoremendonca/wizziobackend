package com.novabase.omni.oney.apps.financialdocument.dto;

import java.time.LocalDateTime;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.financialdocument.enums.DocumentClassType;
import com.novabase.omni.oney.apps.financialdocument.json.serializer.NBLocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class AttachmentDTO extends DTO {

    @ApiModelProperty(readOnly = true)
    private int index;
    
    @ApiModelProperty(readOnly = true)
    private String name;
    
    @ApiModelProperty(readOnly = true)
    private String ownerName;
    
    @ApiModelProperty(readOnly = true)
    private DocumentClassType documentClass;
    
    @ApiModelProperty(readOnly = true)
    private boolean canBeDeletedByCurrentUser;
    
    @ApiModelProperty(readOnly = true)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime date;

    /**
     * Used to find the ownerName, this attribute is not visible on the response
     */
    @ApiModelProperty(hidden = true)
    private transient Long ownerId;
    
    public AttachmentDTO() {
	
	super();
	 
	this.documentClass = DocumentClassType.FINANCIAL_DOCUMENT;
	this.canBeDeletedByCurrentUser = Boolean.FALSE;
	this.date = LocalDateTime.now();
	
    }

    public void setuserInformation(UserInformationDTO userInformationDTO) {
	this.ownerName = userInformationDTO.getCompleteName();
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public DocumentClassType getDocumentClass() {
        return documentClass;
    }

    public void setDocumentClass(DocumentClassType documentClass) {
        this.documentClass = documentClass;
    }

    public boolean isCanBeDeletedByCurrentUser() {
        return canBeDeletedByCurrentUser;
    }

    public void setCanBeDeletedByCurrentUser(boolean canBeDeletedByCurrentUser) {
        this.canBeDeletedByCurrentUser = canBeDeletedByCurrentUser;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }
    
    
}
