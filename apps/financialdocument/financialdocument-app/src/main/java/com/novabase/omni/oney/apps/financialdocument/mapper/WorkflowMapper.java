package com.novabase.omni.oney.apps.financialdocument.mapper;

import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.apps.financialdocument.dto.WorkflowDTO;
import com.novabase.omni.oney.apps.financialdocument.dto.WorkflowStepDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.WorkflowMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.WorkflowStepMsDTO;

@Mapper
public interface WorkflowMapper {

    public static final WorkflowMapper INSTANCE = Mappers.getMapper(WorkflowMapper.class);

    public WorkflowDTO toWorkflowDTO(WorkflowMsDTO msDTO);
    public WorkflowStepDTO toWorkflowStepDTO(WorkflowStepMsDTO msDTO);

    @IterableMapping(elementTargetType = WorkflowStepDTO.class)
    public List<WorkflowStepDTO> toWorkflowStepListDTO(List<WorkflowStepMsDTO> msDTO);
    
}