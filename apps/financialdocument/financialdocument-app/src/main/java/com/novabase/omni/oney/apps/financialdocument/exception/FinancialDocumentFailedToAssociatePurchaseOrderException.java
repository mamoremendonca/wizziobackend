package com.novabase.omni.oney.apps.financialdocument.exception;

import com.novabase.omni.oney.apps.financialdocument.AppProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FinancialDocumentFailedToAssociatePurchaseOrderException extends FinancialDocumentException {

    private static final long serialVersionUID = -244264750240314709L;

    public FinancialDocumentFailedToAssociatePurchaseOrderException(final String errorCode, final Context ctx, final Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
    }

    public static FinancialDocumentFailedToAssociatePurchaseOrderException of(final Context ctx, final String identifier, final Long instanceId) {
	return new FinancialDocumentFailedToAssociatePurchaseOrderException(AppProperties.FAILED_TO_ASSOCIATE_PURCHASE_ORDER_EXCEPTION, ctx, identifier, instanceId);
    }

}
