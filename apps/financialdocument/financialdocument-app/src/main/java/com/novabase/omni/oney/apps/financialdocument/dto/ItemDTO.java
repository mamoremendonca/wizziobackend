package com.novabase.omni.oney.apps.financialdocument.dto;

import java.time.LocalDateTime;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.apps.financialdocument.enums.FinancialDocumentType;
import com.novabase.omni.oney.apps.financialdocument.enums.purchaseorder.PurchaseOrderType;
import com.novabase.omni.oney.apps.financialdocument.json.serializer.NBLocalDateTimeSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ItemDTO extends DTO {

    @ApiModelProperty(readOnly = true)
    private int index;

    @JsonIgnore
    @ApiModelProperty
    private Long itemPurchaseOrderId;

    @ApiModelProperty
    private String code;

    @ApiModelProperty
    private String description;

    @ApiModelProperty
    private String costCenterCode;

    @ApiModelProperty
    private String retentionCode;

    @ApiModelProperty
    private Double amount;

    @ApiModelProperty
    private Double unitPriceWithoutTax;

    @ApiModelProperty
    private String taxCode;

    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime startDate;

    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime endDate;

    @ApiModelProperty
    private String comment;

    @JsonIgnore
    private transient FinancialDocumentType financialDocumentType;

    @JsonIgnore
    private transient PurchaseOrderType purchaseOrderType;

    @JsonIgnore
    private transient String internalOrderCode;

    public ItemDTO() {

	super();

	this.index = 0;
	this.amount = 0D;
	this.unitPriceWithoutTax = 0D;
	this.itemPurchaseOrderId = 0L;
//	this.startDate = LocalDateTime.now();
//	this.endDate = LocalDateTime.now();

    }

    public int getIndex() {
	return index;
    }

    public void setIndex(int index) {
	this.index = index;
    }

    public String getCode() {
	return code;
    }

    public void setCode(String code) {
	this.code = code;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getCostCenterCode() {
	return costCenterCode;
    }

    public void setCostCenterCode(String costCenterCode) {
	this.costCenterCode = costCenterCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getItemPurchaseOrderId() {
	return itemPurchaseOrderId;
    }

    public void setItemPurchaseOrderId(Long itemPurchaseOrderId) {
	this.itemPurchaseOrderId = itemPurchaseOrderId;
    }

    public Double getUnitPriceWithoutTax() {
	return unitPriceWithoutTax;
    }

    public void setUnitPriceWithoutTax(Double unitPriceWithoutTax) {
	this.unitPriceWithoutTax = unitPriceWithoutTax;
    }

    public String getTaxCode() {
	return taxCode;
    }

    public void setTaxCode(String taxCode) {
	this.taxCode = taxCode;
    }

    public LocalDateTime getStartDate() {
	return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
	this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
	return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
	this.endDate = endDate;
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public String getRetentionCode() {
	return retentionCode;
    }

    public void setRetentionCode(String retentionCode) {
	this.retentionCode = retentionCode;
    }

    public FinancialDocumentType getFinancialDocumentType() {
	return financialDocumentType;
    }

    public void setFinancialDocumentType(FinancialDocumentType financialDocumentType) {
	this.financialDocumentType = financialDocumentType;
    }

    public PurchaseOrderType getPurchaseOrderType() {
	return purchaseOrderType;
    }

    public void setPurchaseOrderType(PurchaseOrderType purchaseOrderType) {
	this.purchaseOrderType = purchaseOrderType;
    }

    public String getInternalOrderCode() {
	return internalOrderCode;
    }

    public void setInternalOrderCode(String internalOrderCode) {
	this.internalOrderCode = internalOrderCode;
    }

}
