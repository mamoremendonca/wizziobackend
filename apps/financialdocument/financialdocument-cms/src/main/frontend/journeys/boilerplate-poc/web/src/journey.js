import React from "react";
import { connect } from "react-redux";
import { withRootHoc } from "omni-journey";
import MainContainer from "./containers/MainContainer";

const mapStateToProps = ({}) => ({});

const mapDispatchToProps = (dispatch) => ({});

class Journey extends React.Component {
  render() {
    return (
      <div>
        <MainContainer />
      </div>
    );
  }
}

export default withRootHoc(
  connect(mapStateToProps, mapDispatchToProps)(Journey)
);
