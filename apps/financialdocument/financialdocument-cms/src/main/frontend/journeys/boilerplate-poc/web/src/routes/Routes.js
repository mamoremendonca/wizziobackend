import { matchPath } from "react-router-dom";

const AppRoutes = {
  HOME: {
    path: "/",
    defaultTitle: "",
    exact: true,
  },
  // CREATE: {
  //   path: "/create",
  //   basePath: "/create",
  //   defaultTitle: "route.subtitle_create_financial_document",
  //   exact: true,
  // },
};

export const getPathTitle = (aPath) => {
  let title = aPath;
  const pathKeysArray = Object.keys(AppRoutes);

  for (let i = 0; i < pathKeysArray.length; i++) {
    const pathKey = pathKeysArray[i];
    const pathInfo = AppRoutes[pathKey];

    const matchResult = matchPath(aPath, {
      path: pathInfo.path,
      exact: pathInfo.exact,
      strict: false,
    });

    if (matchResult) {
      title = pathInfo.defaultTitle;
    }
  }
  return title;
};

export const updateJourneySubtitle = (
  path,
  JourneyActions,
  JourneySettings,
  claimsData,
  i18nProvider
) => {
  let pathTitle = getPathTitle(path);
  let subtitle = "";
  if (pathTitle) {
    // TODO: REFACTOR TO CLAIMS
    //Empty path title means that you are in the home.
    // if (financialDocumentData && financialDocumentData.jwcontext) {
    //   const {
    //     financialDocumentHeader: { number },
    //     jwcontext: { status, id },
    //   } = financialDocumentData;
    //   if (status !== FINANCIAL_DOCUMENT_PROCESS_STATUS.DRAFT) {
    //     subtitle = `${i18nProvider.Texti18n(pathTitle)} ${number}`;
    //   } else {
    //     subtitle = `${i18nProvider.Texti18n(
    //       pathTitle
    //     )} TDF${id.toString().padStart(4, "0")}`;
    //   }
    // } else if (financialDocumentData) {
    //   subtitle = `${i18nProvider.Texti18n(pathTitle)} ${
    //     financialDocumentData.financialDocumentHeader.number
    //   }`;
    // }
  }

  let journey_instance_id = JourneySettings.instance_id;
  JourneyActions.setJourneySubtitle(journey_instance_id, subtitle);
};

export default AppRoutes;
