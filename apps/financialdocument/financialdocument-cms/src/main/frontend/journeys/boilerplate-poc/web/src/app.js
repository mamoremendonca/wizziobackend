import React from "react";
import Journey from "./journey";
import { Provider } from "react-redux";
import { name as packageName, parent, contentVersion } from "./../package.json";
import { createStoreSingleton } from "omni-journey";
import { FinalJourney } from "omni-journey";

import { reducer } from "./reducers/reducer";

let reducersToMerge = { reducer };
let Store = createStoreSingleton(reducersToMerge);

export default class App extends React.Component {
  render = () => {
    return (
      <Provider store={Store}>
        <div>
          <RenderJourney handleReceiveMessage={this.handleReceiveMessage} />
        </div>
      </Provider>
    );
  };
}

let RenderJourney = FinalJourney(
  Journey,
  Store,
  packageName,
  parent,
  contentVersion
);
