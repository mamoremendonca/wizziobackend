import * as constants from "../constants";

const initialState = {
  data: [],
};

export const reducer = (state, action) => {
  if (state === undefined) return initialState;

  switch (action.type) {
    case constants.GET_ITEMS:
      return {
        ...state,
        data: action.payload,
      };
    default:
      return state;
  }
};
