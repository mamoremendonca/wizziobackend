//#region  timeline
import { dateFormatter } from "./formUtils";
const getTimelineStepIcon = (status) => {
  switch (status) {
    case "SUBMITTED":
      return "view_headline";
    case "APPROVED":
      return "check";
    case "REJECTED":
      return "close";
    case "RETURNED":
      return "keyboard_return";
    case "DRAFT":
      return "lens";
    case "TO_APPROVE":
    case "PREVISION":
      return "access_time";
    case "JUMP":
      return "not_interested";
    default:
  }
};

const getTimelineItems = (stepData, I18nProvider) => {
  const items = [];

  if (stepData.comment && stepData.comment.trim().length > 0) {
    items.push({
      header: I18nProvider.Labeli18n("timeline.reason.label"),
      content: stepData.comment,
    });
  }
  if (stepData.userGroup) {
    items.push({
      header: I18nProvider.Labeli18n("timeline.owner_group.label"),
      content: stepData.userGroup,
    });
  }
  return items;
};
export const activitiesTimelineDataMapper = (timelineData, I18nProvider) => {
  return {
    iconSize: "16",
    content: timelineData.map((stepData, index) => {
      return {
        title: I18nProvider.Labeli18n(`timeline.type.${stepData.type}.label`),
        date: stepData.date,
        selected: stepData.currentStep,
        user: stepData.ownerName || "",
        icon: stepData.date
          ? getTimelineStepIcon(stepData.type)
          : getTimelineStepIcon("PREVISION"),
        borderStyle:
          stepData.date || stepData.isCurrentStep ? "solid" : "dashed",
        borderColor: "#0f0",
        items: getTimelineItems(stepData, I18nProvider),
        annexes:
          stepData.attachments && stepData.attachments.length > 0
            ? stepData.attachments.map((attachmentRaw) => {
                return {
                  id: attachmentRaw.index,
                  fileName: attachmentRaw.name,
                  uploader: attachmentRaw.ownerName,
                  date: dateFormatter(attachmentRaw.date),
                  editMode: false,
                };
              })
            : [],
      };
    }),
  };
};

export const getAttachmentListMapped = (timelineData, I18nProvider) => {
  return timelineData.map((stepData, index) => {
    return {
      status: I18nProvider.Labeli18n(`timeline.type.${stepData.type}.label`),
      user: stepData.ownerName,
      attachments:
        stepData.attachments && stepData.attachments.length > 0
          ? stepData.attachments.map((attachmentRaw) => {
              return {
                id: attachmentRaw.index,
                name: attachmentRaw.name,
                date: dateFormatter(attachmentRaw.date),
                isAbleToDelete: attachmentRaw.canBeDeletedByCurrentUser,
                isLoading: attachmentRaw.isLoading,
              };
            })
          : [],
    };
  });
};

//#endregion

//For CreateFDScreen

//
