export const componentName = "FooterStyles";

export const styles = (theme) => ({
  root: { display: "flex", flexDirection: "column" },
  formFieldSet: { display: "flex" },
  choiceField: { display: "flex", flexDirection: "column" },
  buttonGroupContainer: {
    display: "flex",
    flexDirection: "row",
  },
  radioButton: {
    "& .MuiIconButton-label": {
      color: theme.palette.error.main,
    },
  },
  radioButtonErrorText: {
    color: theme.palette.error.main,
  },
  justificationField: {
    marginLeft: "60px",
    width: "320px",
  },
  formActions: {
    display: "flex",
    justifyContent: "flex-end",
    marginTop: "35px",
  },
  container: {
    padding: `${theme.spacing.unit * 2}px 0px ${theme.spacing.unit * 3}px 0px`,
  },
  disabledInfoField: {
    paddingBottom: theme.spacing.unit * 2,
    "& .MuiInputBase-root.MuiInputBase-disabled ": {
      color: theme.palette.text.primary,
    },
    "& .MuiFormLabel-root.MuiFormLabel-disabled": {
      color: theme.palette.text.secondary,
    },
    "& .MuiInput-underline.MuiInput-disabled:before": {
      borderBottomStyle: "none",
    },
  },
  footerButtonsContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
  },
});
