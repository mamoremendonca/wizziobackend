import React, { Fragment } from "react";
import { withStyles } from "@ui-lib/core/styles";
import Grid from "@ui-lib/core/Grid";
import * as Constants from "../../constants";
import styles from "./ItemFormStyles";
import { Field } from "react-final-form";
import { TextField, Select, BasicDateField } from "../FFComponentWrappers";
import { OnChange } from "react-final-form-listeners";
import NumberInput from "../FFComponentWrappers/NumberInput";
import { CurrencyType } from "../../utils";
import PropTypes from "prop-types";

const required = (I18nProvider) => (value) =>
  value
    ? undefined
    : I18nProvider.Texti18n("create_fd.item.dialog.form.error.required");
const mustBeNumber = (I18nProvider) => (value) =>
  isNaN(value)
    ? I18nProvider.Texti18n("create_fd.item.dialog.form.error.not_a_number")
    : undefined;
const minValue = (min, I18nProvider) => (value) =>
  isNaN(value) || value >= min
    ? undefined
    : `${I18nProvider.Texti18n(
        "create_fd.item.dialog.form.error.value_below_limit"
      )} ${min}`;

const composeValidators = (...validators) => (value) =>
  validators.reduce((error, validator) => error || validator(value), undefined);

/**
 * Return the default cost center of a project or of a internal order.
 *
 * @param {*} costCenterValues filtered costCenters of department
 * @param {*} financialDocumentData the current financial document
 * @param {*} internalOrders the list of all internal orders to get the cost center code
 * @param {*} projects the list of all projects to get the cost center code
 */
const getCostCenterDefaultValue = (costCenterValues, values) => {
  if (costCenterValues.length === 1) {
    return costCenterValues[0];
  }

  if (values && values.item) {
    for (const costCenter of costCenterValues) {
      if (costCenter.code === values.item.defaultCostCenterCode) {
        return costCenter;
      }
    }
  }
  return undefined;
};

const filterSelectInputValues = (
  options,
  financialDocumentHeader,
  filterName,
  internalOrders,
  projects
) => {
  let optionsFiltered = [];

  if (filterName === Constants.DEPARTMENT) {
    options.forEach((option) => {
      if (
        !option.departmentCode ||
        option.departmentCode === financialDocumentHeader.departmentCode
      ) {
        optionsFiltered.push(option);
      }
    });
  }

  if (filterName === Constants.COST_CENTER) {
    // get cost center from internal order
    if (financialDocumentHeader.internalOrderCode) {
      let costCenterCode = "";

      for (const internalOrder of internalOrders) {
        if (internalOrder.code === financialDocumentHeader.internalOrderCode) {
          costCenterCode = internalOrder.costCenterCode;
          break; //* just to not take too long
        }
      }

      if (costCenterCode) {
        for (const option of options) {
          if (option.code === costCenterCode) {
            optionsFiltered.push(option);
            break; //* just to not take too long
          }
        }
      } else {
        optionsFiltered = [...options];
      }
    }
    // get cost center from project
    else if (financialDocumentHeader.projectCode) {
      for (const project of projects) {
        if (project.code === financialDocumentHeader.projectCode) {
          optionsFiltered.push(project);
          break; //* just to not take too long
        }
      }
    } else {
      options.forEach((option) => {
        if (
          !option.departmentCode ||
          option.departmentCode === financialDocumentHeader.departmentCode
        ) {
          optionsFiltered.push(option);
        }
      });
    }
  }
  return optionsFiltered;
};

//#region //* Aux Functions
const getVariableFields = (
  classes,
  financialDocumentHeader,
  refData,
  values,
  I18nProvider
) => {
  let costCenterField = undefined;
  let retentionFields = undefined;
  let dateFields = undefined;
  let costCenterOptions = undefined;
  let defaultValue = undefined;

  const today = new Date().toISOString().slice(0, 10);

  const minGrantingDate = () => {
    return values[Constants.START_DATE] ? values[Constants.START_DATE] : today;
  };

  const maxGrantingDate = () => {
    return values[Constants.END_DATE] ? values[Constants.END_DATE] : undefined;
  };

  // map cost center
  if (financialDocumentHeader.purchaseOrderType !== Constants.FD_CAPEX) {
    if (!financialDocumentHeader.internalOrderCode) {
      costCenterOptions = filterSelectInputValues(
        refData.costCenters,
        financialDocumentHeader,
        Constants.COST_CENTER,
        refData.internalOrders,
        refData.projects
      );
      defaultValue = getCostCenterDefaultValue(costCenterOptions, values);
      costCenterField = (
        <Grid item className={classes.Grid} xs={4}>
          <Field
            name={Constants.COST_CENTER}
            validate={required(I18nProvider)}
            label={I18nProvider.Labeli18n(
              "create_fd.item.dialog.form.label.cost_center"
            )}
            component={Select}
            defaultValue={defaultValue}
            fullWidth
            formControlProps={{ fullWidth: true }}
            InputLabelProps={{ shrink: true }}
            options={costCenterOptions}
            required
            disabled={costCenterOptions.length === 1}
          />
        </Grid>
      );
    }
  }

  //map retention data if needed
  retentionFields =
    financialDocumentHeader.type ===
    Constants.FINANCIAL_DOCUMENT_TYPE_VALUES.RETENTION_INVOICE ? (
      <>
        <Grid item className={classes.Grid} xs={4}>
          <Field
            name={Constants.RETENTION}
            validate={required(I18nProvider)}
            label={I18nProvider.Labeli18n(
              "create_fd.item.dialog.form.label.retentionPercentage"
            )}
            component={Select}
            options={refData.retentionCodes}
            fullWidth
            formControlProps={{ fullWidth: true }}
            InputLabelProps={{ shrink: true }}
            required
          />
        </Grid>
        <Grid item className={classes.Grid} xs={4}>
          <Field
            name={Constants.RETENTION_VALUE}
            label={I18nProvider.Labeli18n(
              "create_fd.item.dialog.form.label.retentionValue"
            )}
            component={NumberInput}
            fullWidth
            disabled
            currency={CurrencyType.EUR}
            InputLabelProps={{ shrink: true }}
          />
        </Grid>
        <Grid item className={classes.Grid} xs={4}>
          <Field
            name={Constants.FINAL_AMOUNT}
            label={I18nProvider.Labeli18n(
              "create_fd.item.dialog.form.label.finalAmount"
            )}
            component={NumberInput}
            fullWidth
            disabled
            currency={CurrencyType.EUR}
            InputLabelProps={{ shrink: true }}
          />
        </Grid>
      </>
    ) : undefined;

  //map dates if needed
  dateFields =
    financialDocumentHeader.purchaseOrderType ===
      Constants.PURCHASE_ORDER_TYPES.PO_CONTRACT ||
    financialDocumentHeader.granting ? (
      <>
        <Grid item className={classes.Grid} xs={4}>
          <Field
            name={Constants.START_DATE}
            validate={required(I18nProvider)}
            label={I18nProvider.Labeli18n(
              "create_fd.item.dialog.form.label.initial_date"
            )}
            min={today}
            max={maxGrantingDate()}
            component={BasicDateField}
            fullWidth
            InputLabelProps={{ shrink: true }}
            required
          />
        </Grid>
        <Grid item className={classes.Grid} xs={4}>
          <Field
            name={Constants.END_DATE}
            validate={required(I18nProvider)}
            label={I18nProvider.Labeli18n(
              "create_fd.item.dialog.form.label.final_date"
            )}
            min={minGrantingDate()}
            component={BasicDateField}
            fullWidth
            InputLabelProps={{ shrink: true }}
            required
          />
        </Grid>
      </>
    ) : undefined;
  return (
    <Fragment>
      {costCenterField}
      {retentionFields}
      {dateFields}
    </Fragment>
  );
};

//#endregion

const getItemType = (
  classes,
  refData,
  itemTypes,
  financialDocumentHeader,
  values,
  I18nProvider
) => {
  const isFixedAsset =
    financialDocumentHeader.purchaseOrderType === Constants.FD_CAPEX
      ? true
      : false;

  if (isFixedAsset) {
    const options = filterSelectInputValues(
      refData.assets,
      financialDocumentHeader,
      Constants.DEPARTMENT
    );
    return (
      <Grid item className={classes.Grid} xs={4}>
        <Field
          name={Constants.ITEM}
          validate={required(I18nProvider)}
          label={I18nProvider.Labeli18n(
            "create_fd.item.dialog.form.label.asset"
          )}
          component={Select}
          fullWidth
          options={options}
          InputLabelProps={{ shrink: true }}
          formControlProps={{ fullWidth: true }}
          required
        />
      </Grid>
    );
  } else {
    const itemOptions = [];
    if (values.itemType) {
      refData.items.forEach((item) => {
        if (
          (!item.itemTypeCode || values.itemType.code === item.itemTypeCode) &&
          (!item.departmentCode ||
            financialDocumentHeader.departmentCode === item.departmentCode)
        ) {
          itemOptions.push(item);
        }
      });
    }
    return (
      <>
        <Grid item className={classes.Grid} xs={4}>
          <Field
            name={Constants.ITEM_TYPE}
            validate={required(I18nProvider)}
            label={I18nProvider.Labeli18n(
              "create_fd.item.dialog.form.label.item_type"
            )}
            component={Select}
            options={itemTypes}
            fullWidth
            formControlProps={{ fullWidth: true }}
            InputLabelProps={{ shrink: true }}
            required
          />
        </Grid>
        <Grid item className={classes.Grid} xs={4}>
          <Field
            name={Constants.ITEM}
            validate={required(I18nProvider)}
            label={I18nProvider.Labeli18n(
              "create_fd.item.dialog.form.label.item"
            )}
            component={Select}
            fullWidth
            options={itemOptions}
            formControlProps={{ fullWidth: true }}
            InputLabelProps={{ shrink: true }}
            disabled={!values[Constants.ITEM_TYPE] ? true : false}
            required
          />
        </Grid>
      </>
    );
  }
};

const WhenFieldChanges = ({ field, becomes, set, to }) => (
  <Field name={set}>
    {({ input: { onChange } }) => (
      <OnChange name={field}>
        {() => {
          if (becomes) {
            const toModified = mapChangedValues(set, to);
            onChange(toModified);
          }
        }}
      </OnChange>
    )}
  </Field>
);

WhenFieldChanges.propTypes = {
  field: PropTypes.string,
  becomes: PropTypes.any,
  set: PropTypes.any,
  to: PropTypes.any,
};

const shouldChange = (fieldName, values) => {
  switch (fieldName) {
    case Constants.TOTAL_AMOUNT_WO_VAT:
      return values[Constants.QUANTITY] >= 0 && values[Constants.UNIT_VALUE];
    case Constants.TOTAL_AMOUNT_W_VAT:
      return (
        values[Constants.QUANTITY] >= 0 &&
        values[Constants.UNIT_VALUE] &&
        values[Constants.TAX] &&
        values[Constants.TAX].value
      );
    case Constants.RETENTION_VALUE:
      return (
        values[Constants.TOTAL_AMOUNT_WO_VAT] >= 0 &&
        values[Constants.RETENTION] &&
        values[Constants.RETENTION].value
      );
    case Constants.FINAL_AMOUNT:
      return (
        values[Constants.QUANTITY] >= 0 &&
        values[Constants.UNIT_VALUE] &&
        values[Constants.TAX] &&
        values[Constants.TAX].value &&
        values[Constants.RETENTION] &&
        values[Constants.RETENTION].value
      );
    case Constants.DESCRIPTION:
      return (
        (values[Constants.ITEM] && values[Constants.ITEM].description) ||
        (values[Constants.FIXEDASSET] && values[Constants.FIXEDASSET].name)
      );
    default:
      return false;
  }
};

const shouldChangeCostCenter = (refData, financialDocumentHeader) => {
  const costCenters = filterSelectInputValues(
    refData.costCenters,
    financialDocumentHeader,
    Constants.COST_CENTER,
    refData.internalOrders,
    refData.projects
  );
  return costCenters.length > 1;
};

const mapChangedValues = (fieldName, values) => {
  switch (fieldName) {
    case Constants.TOTAL_AMOUNT_WO_VAT: {
      return values.toFixed(2);
    }
    case Constants.TOTAL_AMOUNT_W_VAT: {
      return values.toFixed(2);
    }
    case Constants.RETENTION_VALUE: {
      return values.toFixed(2);
    }
    case Constants.FINAL_AMOUNT: {
      return values.toFixed(2);
    }
    default: {
      return values;
    }
  }
};
const parsePercentage = (value) => (value ? parseFloat(value) / 100 : 0);
const setItemValueWOVAT = (quantity, value) =>
  parseFloat(quantity) >= 0 && parseFloat(value) >= 0
    ? parseFloat(quantity) * parseFloat(value)
    : 0;
const setItemValueWVAT = (quantity, value, vat) =>
  parseFloat(quantity) >= 0 && parseFloat(value) >= 0 && parseFloat(vat) >= 0
    ? parseFloat(quantity) * parseFloat(value) * parseFloat(1 + vat)
    : 0;
const setItemRetentionValue = (quantity, value, retention) => {
  return parseFloat(quantity) >= 0 &&
    parseFloat(value) >= 0 &&
    parseFloat(retention) >= 0
    ? parseFloat(quantity) * parseFloat(value) * parseFloat(retention)
    : 0;
};
const setItemFinalValue = (quantity, value, vat, retention) =>
  parseFloat(quantity) >= 0 &&
  parseFloat(value) >= 0 &&
  parseFloat(vat) >= 0 &&
  parseFloat(retention) >= 0
    ? parseFloat(quantity) * parseFloat(value) * parseFloat(1 + vat) -
      parseFloat(quantity) * parseFloat(value) * parseFloat(retention)
    : 0;

const setTax = (item, taxesRefData) => {
  if (item) {
    const tax = taxesRefData.find((tax) => tax.code === item.defaultTaxCode);
    if (tax) return tax;
  }
  return null;
};

const ItemForm = (props) => {
  const {
    classes,
    refData,
    values,
    financialDocumentData,
    I18nProvider,
    marketType,
  } = props;
  const itemTypes = refData.itemTypes;
  const itemType = getItemType(
    classes,
    refData,
    itemTypes,
    financialDocumentData.financialDocumentHeader,
    values,
    I18nProvider
  );
  const variableFields = getVariableFields(
    classes,
    financialDocumentData.financialDocumentHeader,
    refData,
    values,
    I18nProvider
  );
  const filteredTaxes = refData.tax.filter(
    (t) => !t.marketCode || !marketType || t.marketCode === marketType
  );
  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        <WhenFieldChanges
          field={Constants.UNIT_VALUE}
          becomes={shouldChange(Constants.TOTAL_AMOUNT_WO_VAT, values)}
          set={Constants.TOTAL_AMOUNT_WO_VAT}
          to={setItemValueWOVAT(
            values[Constants.QUANTITY],
            values[Constants.UNIT_VALUE]
          )}
        />
        <WhenFieldChanges
          field={Constants.QUANTITY}
          becomes={shouldChange(Constants.TOTAL_AMOUNT_WO_VAT, values)}
          set={Constants.TOTAL_AMOUNT_WO_VAT}
          to={setItemValueWOVAT(
            values[Constants.QUANTITY],
            values[Constants.UNIT_VALUE]
          )}
        />
        <WhenFieldChanges
          field={Constants.UNIT_VALUE}
          becomes={shouldChange(Constants.TOTAL_AMOUNT_W_VAT, values)}
          set={Constants.TOTAL_AMOUNT_W_VAT}
          to={setItemValueWVAT(
            values[Constants.QUANTITY],
            values[Constants.UNIT_VALUE],
            values[Constants.TAX]
              ? parsePercentage(values[Constants.TAX].taxPercentage)
              : 0
          )}
        />
        <WhenFieldChanges
          field={Constants.QUANTITY}
          becomes={shouldChange(Constants.TOTAL_AMOUNT_W_VAT, values)}
          set={Constants.TOTAL_AMOUNT_W_VAT}
          to={setItemValueWVAT(
            values[Constants.QUANTITY],
            values[Constants.UNIT_VALUE],
            values[Constants.TAX]
              ? parsePercentage(values[Constants.TAX].taxPercentage)
              : 0
          )}
        />
        <WhenFieldChanges
          field={Constants.TAX}
          becomes={shouldChange(Constants.TOTAL_AMOUNT_W_VAT, values)}
          set={Constants.TOTAL_AMOUNT_W_VAT}
          to={setItemValueWVAT(
            values[Constants.QUANTITY],
            values[Constants.UNIT_VALUE],
            values[Constants.TAX]
              ? parsePercentage(values[Constants.TAX].taxPercentage)
              : 0
          )}
        />
        <WhenFieldChanges
          field={Constants.RETENTION}
          becomes={shouldChange(Constants.RETENTION_VALUE, values)}
          set={Constants.RETENTION_VALUE}
          to={setItemRetentionValue(
            values[Constants.QUANTITY],
            values[Constants.UNIT_VALUE],
            values[Constants.RETENTION]
              ? parsePercentage(values[Constants.RETENTION].taxPercentage)
              : 0
          )}
        />
        <WhenFieldChanges
          field={Constants.UNIT_VALUE}
          becomes={shouldChange(Constants.FINAL_AMOUNT, values)}
          set={Constants.FINAL_AMOUNT}
          to={setItemFinalValue(
            values[Constants.QUANTITY],
            values[Constants.UNIT_VALUE],
            values[Constants.TAX]
              ? parsePercentage(values[Constants.TAX].taxPercentage)
              : 0,
            values[Constants.RETENTION]
              ? parsePercentage(values[Constants.RETENTION].taxPercentage)
              : 0
          )}
        />
        <WhenFieldChanges
          field={Constants.QUANTITY}
          becomes={shouldChange(Constants.FINAL_AMOUNT, values)}
          set={Constants.FINAL_AMOUNT}
          to={setItemFinalValue(
            values[Constants.QUANTITY],
            values[Constants.UNIT_VALUE],
            values[Constants.TAX]
              ? parsePercentage(values[Constants.TAX].taxPercentage)
              : 0,
            values[Constants.RETENTION]
              ? parsePercentage(values[Constants.RETENTION].taxPercentage)
              : 0
          )}
        />
        <WhenFieldChanges
          field={Constants.TAX}
          becomes={shouldChange(Constants.FINAL_AMOUNT, values)}
          set={Constants.FINAL_AMOUNT}
          to={setItemFinalValue(
            values[Constants.QUANTITY],
            values[Constants.UNIT_VALUE],
            values[Constants.TAX]
              ? parsePercentage(values[Constants.TAX].taxPercentage)
              : 0,
            values[Constants.RETENTION]
              ? parsePercentage(values[Constants.RETENTION].taxPercentage)
              : 0
          )}
        />
        <WhenFieldChanges
          field={Constants.RETENTION}
          becomes={shouldChange(Constants.FINAL_AMOUNT, values)}
          set={Constants.FINAL_AMOUNT}
          to={setItemFinalValue(
            values[Constants.QUANTITY],
            values[Constants.UNIT_VALUE],
            values[Constants.TAX]
              ? parsePercentage(values[Constants.TAX].taxPercentage)
              : 0,
            values[Constants.RETENTION]
              ? parsePercentage(values[Constants.RETENTION].taxPercentage)
              : 0
          )}
        />
        <WhenFieldChanges
          field={Constants.ITEM}
          becomes={shouldChange(Constants.DESCRIPTION, values)}
          set={Constants.DESCRIPTION}
          to={
            values[Constants.ITEM]
              ? values[Constants.ITEM].description
              : values[Constants.DESCRIPTION]
          }
        />
        <WhenFieldChanges
          field={Constants.FIXEDASSET}
          becomes={shouldChange(Constants.DESCRIPTION, values)}
          set={Constants.DESCRIPTION}
          to={
            values[Constants.FIXEDASSET]
              ? values[Constants.FIXEDASSET].name
              : values[Constants.DESCRIPTION]
          }
        />
        <WhenFieldChanges
          field={Constants.ITEM_TYPE}
          becomes={true}
          set={Constants.ITEM}
          to={undefined}
        />
        <WhenFieldChanges
          field={Constants.ITEM_TYPE}
          becomes={true}
          set={Constants.DESCRIPTION}
          to={undefined}
        />
        <WhenFieldChanges
          field={Constants.ITEM}
          becomes={shouldChangeCostCenter(
            refData,
            financialDocumentData.financialDocumentHeader
          )}
          set={Constants.COST_CENTER}
          to={
            values[Constants.ITEM]
              ? values[Constants.ITEM].costCenter
              : values[Constants.COST_CENTER]
          }
        />
        <WhenFieldChanges
          field={Constants.ITEM}
          becomes={true}
          set={Constants.TAX}
          to={setTax(values[Constants.ITEM], filteredTaxes)}
        />
      </Grid>
      <Grid container spacing={24}>
        {itemType}
        <Grid item className={classes.Grid} xs={4}>
          <Field
            name={Constants.DESCRIPTION}
            validate={required(I18nProvider)}
            label={I18nProvider.Labeli18n(
              "create_fd.item.dialog.form.label.description"
            )}
            component={TextField}
            fullWidth
            InputLabelProps={{ shrink: true }}
            required
            disabled={false}
          />
        </Grid>
        <Grid item className={classes.Grid} xs={4}>
          <Field
            name={Constants.QUANTITY}
            validate={composeValidators(
              required(I18nProvider),
              mustBeNumber(I18nProvider),
              minValue(0, I18nProvider)
            )}
            label={I18nProvider.Labeli18n(
              "create_fd.item.dialog.form.label.qty"
            )}
            component={NumberInput}
            fullWidth
            InputLabelProps={{ shrink: true }}
            required
          />
        </Grid>
        <Grid item className={classes.Grid} xs={4}>
          <Field
            name={Constants.UNIT_VALUE}
            validate={composeValidators(
              required(I18nProvider),
              mustBeNumber(I18nProvider),
              minValue(0, I18nProvider)
            )}
            label={I18nProvider.Labeli18n(
              "create_fd.item.dialog.form.label.unit_value"
            )}
            component={NumberInput}
            currency={CurrencyType.EUR}
            fullWidth
            InputLabelProps={{ shrink: true }}
            required
          />
        </Grid>
        <Grid item className={classes.Grid} xs={4}>
          <Field
            name={Constants.TOTAL_AMOUNT_WO_VAT}
            label={I18nProvider.Labeli18n(
              "create_fd.item.dialog.form.label.total_amount_w_vat"
            )}
            component={NumberInput}
            fullWidth
            currency={CurrencyType.EUR}
            disabled={true}
            InputLabelProps={{ shrink: true }}
          />
        </Grid>
        <Grid item className={classes.Grid} xs={4}>
          <Field
            name={Constants.TAX}
            validate={required(I18nProvider)}
            label={I18nProvider.Labeli18n(
              "create_fd.item.dialog.form.label.tax"
            )}
            component={Select}
            fullWidth
            formControlProps={{ fullWidth: true }}
            InputLabelProps={{ shrink: true }}
            options={filteredTaxes}
            required
          />
        </Grid>
        <Grid item className={classes.Grid} xs={4}>
          <Field
            name={Constants.TOTAL_AMOUNT_W_VAT}
            label={I18nProvider.Labeli18n(
              "create_fd.item.dialog.form.label.total_amount_value"
            )}
            component={NumberInput}
            fullWidth
            currency={CurrencyType.EUR}
            disabled={true}
            InputLabelProps={{ shrink: true }}
          />
        </Grid>
        {variableFields}
        <Grid item className={classes.Grid} xs={4}>
          <Field
            name={Constants.COMMENT}
            label={I18nProvider.Labeli18n(
              "create_fd.item.dialog.form.label.comment"
            )}
            component={TextField}
            fullWidth
            InputLabelProps={{ shrink: true }}
          />
        </Grid>
      </Grid>
    </div>
  );
};

ItemForm.propTypes = {
  I18nProvider: PropTypes.object,
  classes: PropTypes.object,
  refData: PropTypes.object,
  financialDocumentData: PropTypes.object,
  values: PropTypes.object,
  marketType: PropTypes.string,
};

export default withStyles(styles)(ItemForm);
