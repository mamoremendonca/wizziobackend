import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import withContainerWrapper from "../../components/ContainerWrapper";
import FDDetailScreenRender from "./FDDetailScreenRender";
import {
  getFinancialDocumentDataFromRepository,
  getTimelineData,
  clearFinancialDocument,
} from "../../actions";
import { calculateTotalItemsValue } from "../../utils/infoFDUtils";
import { Config } from "../../config";
import { handleManageDfRequest } from "../../actions/services/financialDocumentServiceActions";
import PropTypes from "prop-types";

/**
 * Template Screen to the Detail Process
 * Here he just update the GlobalState setting the FinancialDocument Reducer and mount the screen.
 * This screen is always used by the Financial Document Process Continuity
 */
class FDDetailScreen extends Component {
  state = {
    allDependenciesResolved: false,
    externalLoadingDependencies: false,
    canSubmit: true,
    confirmationDialogOptions: {
      title: "",
      isOpen: false,
      closeHandler: undefined,
    },
  };

  componentDidMount() {
    const {
      match,
      HttpClient,
      Notifications,
      getFinancialDocumentDataFromRepository,
      Loading,
      I18nProvider,
      getTimelineData,
    } = this.props;
    Loading.request("setLoading", "JOURNEY").then(() => {
      if (match && match.params && match.params.id) {
        getFinancialDocumentDataFromRepository(
          HttpClient,
          Notifications,
          I18nProvider.Texti18n(
            "services.get.financial_document.error.message"
          ),
          match.params.id.replace(":id", "")
        );
        getTimelineData(
          HttpClient,
          Notifications,
          match.params.id.replace(":id", ""),
          I18nProvider.Texti18n("service.get_timeline.error.message"),
          Config.FINANCIAL_DOCUMENT_REPOSITORY
        );
      }
    });
  }

  componentDidUpdate() {
    const { financialDocumentData, timelineData, Loading } = this.props;
    const { allDependenciesResolved } = this.state;
    if (financialDocumentData && timelineData && !allDependenciesResolved) {
      //remove loading show screen
      this.setState({ allDependenciesResolved: true }, () =>
        Loading.request("stopLoading")
      );
    }
  }

  handleBackClick = () => {
    const {
      history,
      JourneyActions,
      JourneySettings,
      clearFinancialDocument,
    } = this.props;
    const { isFromWorkList } = this.state;

    if (isFromWorkList) {
      JourneyActions.closeJourney(JourneySettings.instance_id);
    } else {
      history.goBack();
      clearFinancialDocument();
    }
  };

  closeDialog = () => {
    this.setState({
      ...this.state,
      confirmationDialogOptions: {
        title: "",
        isOpen: false,
        closeHandler: undefined,
      },
    });
  };

  handleManageDfEvent = async (decision) => {
    const {
      HttpClient,
      Notifications,
      financialDocumentData,
      history,
      Loading,
      I18nProvider,
      clearFinancialDocument,
    } = this.props;
    if (decision) {
      try {
        await Loading.request("setLoading", "JOURNEY");
        await handleManageDfRequest(
          HttpClient,
          financialDocumentData.currentInstanceId
        );
        this.closeDialog();
        history.push(`/approve/${financialDocumentData.currentInstanceId}`);
        clearFinancialDocument();
      } catch (error) {
        Notifications.notify(
          I18nProvider.Texti18n(
            "service.claim.financial_document.error.message"
          ),
          "error",
          "Process",
          5000
        );
      } finally {
        Loading.request("stopLoading", "JOURNEY");
      }
    } else {
      this.closeDialog();
    }
  };

  handleManageDf = () => {
    const { I18nProvider } = this.props;
    this.setState({
      ...this.state,
      confirmationDialogOptions: {
        title: I18nProvider.Texti18n(
          "fdDetailsScreen.footer.confirmation_dialog.manage_df.title"
        ),
        isOpen: true,
        closeHandler: this.handleManageDfEvent,
      },
    });
  };

  render() {
    const { allDependenciesResolved } = this.state;
    const { financialDocumentData, I18nProvider, refData } = this.props;
    if (allDependenciesResolved) {
      return (
        <FDDetailScreenRender
          confirmationDialogOptions={this.state.confirmationDialogOptions}
          I18nProvider={I18nProvider}
          financialDocumentData={financialDocumentData}
          refData={refData}
          totalItemsValue={calculateTotalItemsValue(
            refData,
            financialDocumentData
          )}
          handleBackClick={this.handleBackClick}
          handleManageDf={this.handleManageDf}
        />
      );
    }
    return null;
  }
}

FDDetailScreen.propTypes = {
  I18nProvider: PropTypes.object,
  match: PropTypes.object,
  financialDocumentData: PropTypes.object,
  Loading: PropTypes.object,
  JourneyActions: PropTypes.object,
  refData: PropTypes.object,
  history: PropTypes.object,
  Notifications: PropTypes.object,
  JourneySettings: PropTypes.object,
  HttpClient: PropTypes.object,
  clearFinancialDocument: PropTypes.func,
  getTimelineData: PropTypes.func,
  getFinancialDocumentDataFromRepository: PropTypes.func,
  timelineData: PropTypes.any,
};

const mapStateToProps = ({
  journey: {
    services: { I18nProvider, Loading, Notifications, HttpClient },
  },
  financialDocumentReducer: { financialDocumentData, timelineData },
  refDataReducer: { refData },
}) => ({
  I18nProvider,
  Loading,
  Notifications,
  HttpClient,
  financialDocumentData,
  refData,
  timelineData,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getFinancialDocumentDataFromRepository,
      getTimelineData,
      clearFinancialDocument,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withContainerWrapper(FDDetailScreen));
