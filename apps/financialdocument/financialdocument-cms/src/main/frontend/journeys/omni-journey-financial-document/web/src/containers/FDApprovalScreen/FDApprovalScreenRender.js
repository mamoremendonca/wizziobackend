import React from "react";
import { componentName, styles } from "./FDApprovalScreenStyles";
import { withStyles } from "@ui-lib/core/styles";
import FDTableItems from "../../components/FDTableItems";
import DetailsFields from "../../components/DetailsFields/DetailsFields";
import Separator from "../../components/Separator";
import FDTotalTableItems from "../../components/FDTableItems/FDTotalTableItems";
import Timeline from "../../components/Timeline";
import { Config } from "../../config";
import Footer from "./Footer";
import PropTypes from "prop-types";

const FDApprovalScreenRender = ({
  classes,
  financialDocumentData,
  I18nProvider,
  refData,
  totalItemsValue,
  setLoadingDependencies,
  history,
}) => {
  return (
    <div className={classes.root}>
      <div className={classes.approvalDetails}>
        <div className={classes.info}>
          <DetailsFields
            I18nProvider={I18nProvider}
            refData={refData}
            financialDocumentHeader={
              financialDocumentData.financialDocumentHeader
            }
          />
        </div>
        <div className={classes.tableItems}>
          <Separator
            text={I18nProvider.Labeli18n(
              "fdApprovalScreenRender.separator.items.label"
            )}
          />
          <FDTableItems
            I18nProvider={I18nProvider}
            itemsList={financialDocumentData.items}
            financialDocumentHeader={
              financialDocumentData.financialDocumentHeader
            }
            refData={refData}
          />
          <FDTotalTableItems
            I18nProvider={I18nProvider}
            total={totalItemsValue}
          />
        </div>
        <div className={classes.footer}>
          <Footer history={history} />
        </div>
      </div>
      <div className={classes.timeline}>
        <Timeline
          dataSource={Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}
          setParentLoadingDependencies={setLoadingDependencies}
          allowAddAttachments
        />
      </div>
    </div>
  );
};

FDApprovalScreenRender.propTypes = {
  classes: PropTypes.object,
  I18nProvider: PropTypes.object,
  refData: PropTypes.object,
  totalItemsValue: PropTypes.number,
  financialDocumentData: PropTypes.object,
  setLoadingDependencies: PropTypes.func,
  history: PropTypes.object,
};

export default withStyles(styles, { name: componentName })(
  FDApprovalScreenRender
);
