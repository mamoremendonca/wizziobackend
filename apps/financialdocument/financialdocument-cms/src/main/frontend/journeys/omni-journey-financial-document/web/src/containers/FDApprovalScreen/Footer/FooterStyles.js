export const componentName = "FooterStyles";

export const styles = (theme) => ({
  root: { display: "flex", flexDirection: "column" },
  formFieldSet: { display: "flex" },
  choiceField: { display: "flex", flexDirection: "column" },
  buttonGroupContainer: {
    display: "flex",
    flexDirection: "row",
  },
  radioButton: {
    "& .MuiIconButton-label": {
      color: theme.palette.error.main,
    },
  },
  radioButtonErrorText: {
    color: theme.palette.error.main,
  },
  justificationField: {
    marginLeft: "60px",
    width: "320px",
  },
  formActions: {
    display: "flex",
    justifyContent: "flex-end",
    marginTop: "35px",
  },
  cancelButton: {
    marginRight: "10px",
  },
});
