import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { styles, ComponentName } from "./FDListScreenStyle";
import Button from "@ui-lib/core/Button";
import Tooltip from "@ui-lib/core/Tooltip";
//import Tooltip from '@material-ui/core/Tooltip';
import Icon from "@ui-lib/core/Icon";
import IconButton from "@ui-lib/core/IconButton";
import Grid from "@ui-lib/core/Grid";
import HeaderBar from "../../components/HeaderBar";
import FilterList from "../../components/FilterList";
import classNames from "@ui-lib/core/classnames";
import CustomTable from "@ui-lib/custom-components/CustomTable";
import Collapse from "@ui-lib/core/Collapse";
import Typography from "@ui-lib/core/Typography";
import { FINANCIAL_DOCUMENT_STATUS } from "../../constants";
import Countdown from "./Countdown";
import { getFormattedNumber } from "../../utils";
import PropTypes from "prop-types";
/**
 * Return a jsx component for the status(Estado) column to add in the row of FD list table
 */

const getStatusVisualComponent = (classes, status) => {
  switch (status) {
    case FINANCIAL_DOCUMENT_STATUS.IN_APPROVAL:
      return (
        <div className={classes.statsAlign}>
          <div className={classes.waitingApprovalStatus} />
        </div>
      );
    case FINANCIAL_DOCUMENT_STATUS.SAP_SUBMITTED:
      return (
        <div className={classes.statsAlign}>
          <div className={classes.sap_submittedStatus} />
        </div>
      );
    case FINANCIAL_DOCUMENT_STATUS.APPROVED:
      return (
        <div className={classes.statsAlign}>
          <div className={classes.approvedStatus} />
        </div>
      );
    case FINANCIAL_DOCUMENT_STATUS.CANCELED:
      return (
        <div className={classes.statsAlign}>
          <div className={classes.canceledStatus} />
        </div>
      );
    case FINANCIAL_DOCUMENT_STATUS.REJECTED:
      return (
        <div className={classes.statsAlign}>
          <div className={classes.rejectedStatus} />
        </div>
      );
    case FINANCIAL_DOCUMENT_STATUS.PAID:
      return (
        <div className={classes.statsAlign}>
          <div className={classes.paidStatus} />
        </div>
      );
    default:
      return "-";
  }
};

const toLocalDate = (date) => `${new Date(date).toLocaleDateString()}`;

/**
 * Will return a i18n string for the description or for the code.
 *
 * @param {*} i18nProvider
 * @param {*} code
 * @param {*} ref
 */
const getDescriptionByCode = (i18nProvider, code, ref) => {
  const elements = ref.filter((entry) => entry.code === code);
  if (elements.length > 0) {
    return i18nProvider.Texti18n(elements[0].description);
  } else {
    return i18nProvider.Texti18n(code);
  }
};

/**
 * Gets the value that came from the response and format it.
 * @param {*} fieldValue
 * @param {*} format
 * @param {*} ref
 */
const parseCellValues = (i18nProvider, fieldValue, format, ref) => {
  switch (format) {
    case "date":
      return fieldValue ? toLocalDate(fieldValue) : "-";
    case "countdown":
      return fieldValue ? (
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            position: "relative",
          }}
        >
          {toLocalDate(fieldValue)}
          <Countdown
            i18nProvider={i18nProvider}
            dateToCompare={fieldValue}
            status={ref}
          />
        </div>
      ) : (
        "-"
      );
    case "referenceData":
      return fieldValue
        ? getDescriptionByCode(i18nProvider, fieldValue, ref)
        : "-";
    case "euro":
      return fieldValue ? getFormattedNumber(fieldValue, true, "EUR") : "-";
    default:
      return fieldValue ? i18nProvider.Texti18n(fieldValue) : "-";
  }
};

/**
 * Create a parsed row
 * Has the logic to transform data in visual content.
 */
const createTableRow = (classes, dataRow, i18nProvider, refData) => {
  const row = {};
  row.id = dataRow.repositoryId;
  row.repositoryStatus = getStatusVisualComponent(
    classes,
    dataRow.repositoryStatus
  );
  row.purchaseOrderFriendlyNumber = parseCellValues(
    i18nProvider,
    dataRow.purchaseOrderFriendlyNumber
  );
  row.number = parseCellValues(i18nProvider, dataRow.number);
  row.type = parseCellValues(i18nProvider, dataRow.type);
  row.supplierCode = parseCellValues(
    i18nProvider,
    dataRow.supplierCode,
    "referenceData",
    refData.referenceData.suppliers
  );
  row.totalWithoutTax = parseCellValues(
    i18nProvider,
    dataRow.totalWithoutTax,
    "euro"
  );
  row.creationDate = parseCellValues(
    i18nProvider,
    dataRow.creationDate,
    "date"
  );
  row.expirationDate = parseCellValues(
    i18nProvider,
    dataRow.expirationDate,
    "countdown",
    dataRow.repositoryStatus
  );
  row.departmentCode = parseCellValues(
    i18nProvider,
    dataRow.departmentCode,
    "referenceData",
    refData.referenceData.allDepartments
  );
  row.projectCode = parseCellValues(
    i18nProvider,
    dataRow.projectCode,
    "referenceData",
    refData.referenceData.projects
  );
  row.internalOrderCode = parseCellValues(
    i18nProvider,
    dataRow.internalOrderCode,
    "referenceData",
    refData.referenceData.internalOrders
  );
  row.userGroup = dataRow.currentUserGroup;
  return row;
};
/**
 * Used to parse the data that cames from the backend
 */
const tableParseData = (classes, data, i18nProvider, refData) => {
  let parseData = [];
  data.forEach((element) => {
    parseData.push(createTableRow(classes, element, i18nProvider, refData));
  });
  return parseData;
};

const FDListScreenRender = ({
  classes,
  I18nProvider,
  tableHeaders,
  financialDocumentsList,
  totalFinancialDocuments,
  numberOfRecords,
  expandFilter,
  navigateToPage,
  onExpandFilterClick,
  requestFilterData,
  onRowClick,
  sortHandler,
  refData,
  tableApi,
  setTableApi,
  allowCreateFinancialDocument,
}) => {
  return (
    <div className={classes.root}>
      {
        <HeaderBar title={I18nProvider.Texti18n("fd_list")} hideBackButton>
          <Grid container spacing={0}>
            <Grid container item xs={6}>
              <Grid container item xs={12}>
                {allowCreateFinancialDocument && (
                  <Button
                    id="CreateNewFD"
                    style={{ height: "40px", paddingBottom: "6px" }}
                    variant="contained"
                    color="primary"
                    onClick={() => navigateToPage("createFD")}
                  >
                    {I18nProvider.Texti18n("create_fd")}
                    <Icon className={classNames(classes.icon, "icon-add")} />
                  </Button>
                )}
              </Grid>
            </Grid>
            <Grid container item xs={6}>
              <Grid container justify="flex-end" item xs={12}>
                <IconButton
                  id="filterIconButton"
                  className={classes.filterIcon}
                  onClick={onExpandFilterClick}
                >
                  <Icon className={"icon-filter"}></Icon>
                </IconButton>
              </Grid>
            </Grid>
          </Grid>
        </HeaderBar>
      }
      <div className={classes.mainContent}>
        <Grid container spacing={0}>
          <Grid container item xs={12} spacing={0}>
            <Collapse
              id="collapseFilterFields"
              className={classes.collapseFilter}
              in={expandFilter}
              timeout="auto"
            >
              <FilterList onFilterClick={requestFilterData} />
            </Collapse>
          </Grid>
          <Grid container item xs={12} spacing={0} justify="flex-end">
            <div className={classes.tableNumbers}>
              {/* TODO: Uncomment when the new ui-lib/core 2.2.0 is used*/}
              {/* <Tooltip  classes={{ tooltip: classes.increaseTooltip }}
								title={I18nProvider.Texti18n('listFD.table.header.tooltip.record_numbers')}>
								<Icon className="icon-information" style = {{fontSize: 16}} />
							</Tooltip> */}
              <Tooltip
                title={
                  <Typography>
                    {I18nProvider.Labeli18n(
                      "listFD.table.header.tooltip.record_numbers"
                    )}
                  </Typography>
                }
              >
                <Icon className="icon-information" style={{ fontSize: 16 }} />
              </Tooltip>
              <Typography
                variant="body2 Heading"
                color="textPrimary"
                style={{ marginLeft: "10px" }}
              >
                {I18nProvider.Texti18n("showing")} {numberOfRecords}{" "}
                {I18nProvider.Texti18n("of")} {totalFinancialDocuments}{" "}
                {I18nProvider.Texti18n("results")}.
              </Typography>
            </div>
          </Grid>
          <Grid container item xs={12} spacing={0}>
            <CustomTable
              onRowClick={onRowClick}
              id="TableListFD"
              singleSort={true}
              headers={tableHeaders}
              apiCallback={(api) => {
                if (!tableApi) setTableApi(api);
              }}
              noDataLabel={I18nProvider.Texti18n("no_results_found")}
              data={tableParseData(
                classes,
                financialDocumentsList,
                I18nProvider,
                refData
              )}
              onSort={(headerKey, directionResponseOk) => {
                return new Promise((resolve, error) => {
                  sortHandler(headerKey, directionResponseOk, resolve, error);
                });
              }}
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

FDListScreenRender.propTypes = {
  classes: PropTypes.object,
  I18nProvider: PropTypes.object,
  refData: PropTypes.object,
  tableHeaders: PropTypes.object,
  financialDocumentsList: PropTypes.array,
  totalFinancialDocuments: PropTypes.number,
  numberOfRecords: PropTypes.number,
  expandFilter: PropTypes.bool,
  navigateToPage: PropTypes.func,
  onExpandFilterClick: PropTypes.func,
  requestFilterData: PropTypes.func,
  onRowClick: PropTypes.func,
  sortHandler: PropTypes.func,
  tableApi: PropTypes.func,
  setTableApi: PropTypes.func,
  allowCreateFinancialDocument: PropTypes.bool,
};

export default withStyles(styles, { name: ComponentName })(FDListScreenRender);
