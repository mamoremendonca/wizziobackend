import * as React from "react";
import Select from "react-select";
import FormControl from "@ui-lib/core/FormControl";
import InputLabel from "@ui-lib/core/InputLabel";
import FormHelperText from "@ui-lib/core/FormHelperText";
import { withTheme } from "@ui-lib/core/styles";
import { connect } from "react-redux";
import PropTypes from "prop-types";

const customStyles = (theme) => ({
  option: (provided, state) => ({
    ...provided,
    fontFamily: theme.typography.fontFamily,
    backgroundColor: state.isSelected ? theme.palette.grey[400] : "transparent",
    "&:hover": {
      backgroundColor: theme.palette.grey[400],
      cursor: "pointer",
      color: theme.palette.common.white,
    },
  }),
  control: (provided, state) => {
    return state.isDisabled
      ? {
          ...provided,
          marginTop: 12,
          borderTop: 0,
          borderLeft: 0,
          borderRight: 0,
          borderBottom: "none",
          boxShadow: 0,
          backgroundColor: "transparent",
          borderRadius: 0,
          padding: "-2px -8px",
        }
      : {
          ...provided,
          marginTop: 12,
          borderTop: state.isSelected || state.isFocused ? 0 : 0,
          borderLeft: 0,
          borderRight: 0,
          borderBottom: "1px solid #8e8e8e;",
          boxShadow: state.isSelected || state.isFocused ? 0 : 0,
          backgroundColor: "transparent",
          borderRadius: 0,
          height: "40px",
          "&:hover": {
            borderBottom: "2px solid black",
            cursor: "pointer",
            height: "40px",
          },
          padding: "-2px -8px",
        };
  },
  dropdownIndicator: (provided, state) => ({
    ...provided,
    marginBottom: 4,
    color: state.isDisabled ? "transparent" : theme.palette.primary.main,
  }),
  singleValue: (provided) => ({
    ...provided,
    backgroundColor: "transparent",
    color: theme.palette.text.primary,
    fontFamily: theme.typography.fontFamily,
  }),
  root: (provided) => ({ ...provided, width: "100%" }),
  placeholder: (provided) => ({
    ...provided,
    fontFamily: theme.typography.fontFamily,
  }),
  valueContainer: (provided) => ({ ...provided, padding: "-2px -8px" }),
  menu: (provided) => ({
    ...provided,
    backgroundColor: theme.palette.grey[100],
  }),
});

const IndicatorSeparator = ({ innerProps }) => {
  return <span {...innerProps} />;
};

IndicatorSeparator.propTypes = { innerProps: PropTypes.object };

/**
 * input : {
 *name === key
 *value - number/string
 *onChange - func
 * }
 *meta { submitError-string, dirtySinceLastSubmit- bool ?, error - string, touched - bool  }
 *label - string
 * formControlProps ?
 * inputLabelProps ?
 *disabled - bool
 * reqired - bool
 * @param {*} param0
 */

const FormHelperTextWrapper = ({
  input: { name, value, onChange, ...restInput },
  meta,
  label,
  formControlProps,
  inputLabelProps,
  disabled,
  theme,
  required,
  I18nProvider,
  ...rest
}) => {
  const showError =
    (((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) &&
      meta.touched) ||
    (meta.modified && meta.error);
  return (
    <FormControl
      {...formControlProps}
      error={showError}
      className={customStyles(theme).root}
    >
      <InputLabel htmlFor={name} {...inputLabelProps} shrink={true}>
        {label} {required && !disabled ? "*" : ""}
      </InputLabel>
      <Select
        {...rest}
        name={name}
        onChange={onChange}
        inputProps={restInput}
        close
        value={value}
        styles={customStyles(theme)}
        components={{ IndicatorSeparator }}
        placeholder={
          disabled
            ? "-"
            : I18nProvider.Labeli18n("select_component.placeholder.label")
        }
        isDisabled={disabled}
        isClearable={true}
        // noOptionsMessage= {(args) => {console.log(args.inputValue); return 'Sem resultados'  }}
        noOptionsMessage={() =>
          I18nProvider.Labeli18n("select_component.no_matches.label")
        }
        menuIsOpen={disabled ? false : undefined}
      />
      {showError && (
        <FormHelperText>{meta.error || meta.submitError}</FormHelperText>
      )}
    </FormControl>
  );
};

const mapStateToProps = ({
  journey: {
    services: { I18nProvider },
  },
  i18n,
}) => ({
  I18nProvider,
  i18n,
});

FormHelperTextWrapper.propTypes = {
  input: PropTypes.object,
  meta: PropTypes.object,
  I18nProvider: PropTypes.object,
  formControlProps: PropTypes.object,
  inputLabelProps: PropTypes.object,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
  theme: PropTypes.object,
};

export default connect(mapStateToProps)(withTheme()(FormHelperTextWrapper));
