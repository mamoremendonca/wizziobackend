const ComponentName = "StatusToolTip";
const styles = (theme) => ({
  statusIndicator: {
    width: 10,
    height: 10,
    borderRadius: "50%",
    marginTop: 3,
    marginRight: 10,
    float: "left",
    backgroundColor: theme.palette.primary.main,
  },
  container: {
    width: 130,
    height: 158,
    display: "flex",
    flexDirection: "column",
  },
  title: {
    margin: "0px 0px 5px 0px",
  },
  item: {
    margin: "4px 0px 4px 0px",
  },
});

export { ComponentName };
export { styles };
