import { SERVER_ERROR_EXCEPTIONS } from "../../constants/FDConstants";

const extractErrorFields = (
  details,
  errorCode,
  I18nProvider,
  Notifications
) => {
  const errorFields = {};
  const itemTableErrors = [];
  details.forEach((detail) => {
    switch (detail.message) {
      case "item.endDate":
      case "item.startDate":
        if (itemTableErrors.length === 0) {
          itemTableErrors.push(
            I18nProvider.Texti18n(
              `server_error_exception.${errorCode}.item.date.message`
            )
          );
        }
        break;
      case "item.retentionCode":
        if (itemTableErrors.length === 0) {
          itemTableErrors.push(
            I18nProvider.Texti18n(
              `server_error_exception.${errorCode}.${detail.message}.message`
            )
          );
        }
        break;
      default:
        errorFields[detail.message] = I18nProvider.Texti18n(
          `server_error_exception.${errorCode}.message`
        );
        break;
    }
  });

  if (itemTableErrors.length > 0) {
    Notifications.notify(itemTableErrors[0], "error", "Process", 5000);
  }

  return errorFields;
};

export const handleSubmitServerErrorResponse = (
  serverError,
  Notifications,
  I18nProvider
) => {
  let errorFields = {};
  if (serverError.status === 400) {
    const { details, errorCode } = serverError.data;
    switch (errorCode) {
      case SERVER_ERROR_EXCEPTIONS.FIELD_VALIDATION_EXCEPTION:
        errorFields = extractErrorFields(
          details,
          errorCode,
          I18nProvider,
          Notifications
        );
        break;
      case SERVER_ERROR_EXCEPTIONS.TOTAL_VALUE_ASSOCIATED_PO_EXCEPTION:
      case SERVER_ERROR_EXCEPTIONS.FINANCIAL_DOCUMENT_NUMBER_ALREADY_EXISTS_EXCEPTION:
        Notifications.notify(
          I18nProvider.Texti18n(`server_error_exception.${errorCode}.message`),
          "error",
          "Process",
          5000
        );
        break;
      default:
        Notifications.notify(
          I18nProvider.Texti18n("createFDScreen.submit.generic.error.message"),
          "error",
          "Process",
          5000
        );
    }
  } else {
    Notifications.notify(
      I18nProvider.Texti18n("createFDScreen.submit.generic.error.message"),
      "error",
      "Process",
      5000
    );
  }
  return errorFields;
};
