// @flow
import * as React from "react";
import { withStyles } from "@ui-lib/core/styles";
import Typography from "@ui-lib/core/Typography";
import PropTypes from "prop-types";

const componentName = "FDTableItemStyles";
const styles = (theme) => ({
  root: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    marginTop: "16px",
  },
});

const FDTotalTableItems = ({ classes, I18nProvider, total }) => {
  return (
    <div className={classes.root}>
      <Typography variant="subtitle1" color="primary">
        {I18nProvider.Labeli18n("fdTableItems.total_value.label")}
      </Typography>
      <Typography variant="subtitle2" color="textPrimary">
        {total}
      </Typography>
    </div>
  );
};

FDTotalTableItems.propTypes = {
  classes: PropTypes.object,
  I18nProvider: PropTypes.object,
  total: PropTypes.number,
};

export default withStyles(styles, { name: componentName })(FDTotalTableItems);
