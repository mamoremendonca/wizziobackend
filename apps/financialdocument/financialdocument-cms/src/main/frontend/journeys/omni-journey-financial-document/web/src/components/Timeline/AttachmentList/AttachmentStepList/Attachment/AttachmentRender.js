import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { styles, componentName } from "./AttachmentStyles";
import Typography from "@ui-lib/core/Typography";
import Icon from "@ui-lib/core/Icon";
import IconButton from "@ui-lib/core/IconButton";
import CircularProgress from "@ui-lib/core/CircularProgress";
import PropTypes from "prop-types";

const AttachmentRender = ({
  classes,
  attachmentData,
  downloadAttachment,
  deleteAttachment,
}) => {
  return (
    <div className={classes.root}>
      {(attachmentData.isAbleToDelete || attachmentData.isLoading) && (
        <div className={classes.deleteAttachment}>
          {attachmentData.isLoading ? (
            <CircularProgress
              value={true}
              color="primary"
              className={classes.circularProgress}
            />
          ) : (
            <IconButton
              onClick={deleteAttachment}
              className={classes.closeIcon}
            >
              <Icon>clear</Icon>
            </IconButton>
          )}
        </div>
      )}
      <div className={classes.attachmentContainer}>
        <div className={classes.attachmentHeader}>
          <Icon className={classes.attachmentIcon}>attach_file</Icon>
          <div onClick={downloadAttachment}>
            <Typography variant="h5" className={classes.attachmentTitle}>
              {attachmentData.name}
            </Typography>
          </div>
        </div>
        <Typography>{attachmentData.date}</Typography>
      </div>
    </div>
  );
};

AttachmentRender.propTypes = {
  classes: PropTypes.object,
  attachmentData: PropTypes.object,
  downloadAttachment: PropTypes.func,
  deleteAttachment: PropTypes.func,
};

export default withStyles(styles, { name: componentName })(AttachmentRender);
