// @flow
import * as React from "react";
import { withStyles } from "@ui-lib/core/styles";
import Viewer, { Worker, defaultLayout } from "@phuocng/react-pdf-viewer";
import "@phuocng/react-pdf-viewer/cjs/react-pdf-viewer.css";
import PDFToolbarRender from "./PDFToolbar/PDFToolbarRender";
import { ComponentName, styles } from "./PDFViewerStyles";
import "./overrideCSS.css";
import { Config } from "../../../config";
import PropTypes from "prop-types";

const layout = (isSidebarOpened, container, main, toolbar, sidebar) => {
  return defaultLayout(
    isSidebarOpened,
    container,
    main,
    toolbar((toolbarSlot) => <PDFToolbarRender toolbarSlot={toolbarSlot} />),
    sidebar
  );
};

const PDFViewerRender = ({ classes, pdfURL, hostPort, contentUrl }) => {
  return (
    <Worker workerUrl={hostPort + contentUrl + Config.WORKER_URL}>
      <div className={classes.workerRoot}>
        <div className={classes.workerViewer}>
          <Viewer fileUrl={pdfURL} layout={layout} />
        </div>
      </div>
    </Worker>
  );
};

PDFViewerRender.propTypes = {
  classes: PropTypes.object,
  pdfURL: PropTypes.string,
  hostPort: PropTypes.string,
  contentUrl: PropTypes.string,
};

export default withStyles(styles, { name: ComponentName })(PDFViewerRender);
