import * as constants from "../constants";
import {
  getTimelineRequest,
  addAttachmentRequest,
  removeAttachmentRequest,
} from "./services/financialDocumentServiceActions";

export const setTimeline = (timelineData) => {
  return {
    type: constants.SET_TIMELINE,
    payload: timelineData,
  };
};

export const getTimelineData = (
  HttpClient,
  Notifications,
  instanceId,
  errorMessage,
  dataSource
) => (dispatch) =>
  getTimelineRequest(HttpClient, instanceId, dataSource)
    .then((res) => {
      dispatch(setTimeline(res.data));
    })
    .catch(() => {
      Notifications.notify(errorMessage, "error", "Process", 5000);
    });

const addAttachmentToTimelineStep = (stepIndex, attachmentInfo) => {
  return {
    type: constants.ADD_ATTACHMENT_TO_TIMELINE_STEP,
    payload: { stepIndex, attachmentInfo },
  };
};

const removeAttachmentFromTimelineStep = (stepIndex, attachmentIndex) => {
  return {
    type: constants.REMOVE_ATTACHMENT_FROM_TIMELINE_STEP,
    payload: { stepIndex, attachmentIndex },
  };
};

const setAttachmentTimelineLoading = (
  attachmentIndex,
  stepIndex,
  attachmentInfo,
  isLoading
) => {
  return {
    type: constants.SET_ATTACHMENT_TIMELINE_LOADING,
    payload: { attachmentIndex, stepIndex, attachmentInfo, isLoading },
  };
};

export const addTimelineAttachment = (
  HttpClient,
  Notifications,
  errorMessage,
  stepIndex,
  attachment,
  instanceId,
  documentClass
) => (dispatch) => {
  dispatch(addAttachmentToTimelineStep(stepIndex, attachment.attachmentInfo));
  addAttachmentRequest(
    HttpClient,
    instanceId,
    attachment.attachmentFile,
    documentClass
  )
    .then((res) => {
      dispatch(
        setAttachmentTimelineLoading(
          res.data.index,
          stepIndex,
          attachment.attachmentInfo,
          false
        )
      );
    })
    .catch(() => {
      dispatch(
        removeAttachmentFromTimelineStep(
          stepIndex,
          attachment.attachmentInfo.index
        )
      );
      Notifications.notify(errorMessage, "error", "Process", 5000);
    });
};

export const removeTimelineAttachment = (
  HttpClient,
  stepIndex,
  attachmentInfo,
  instanceId,
  Notifications,
  errorMessage
) => (dispatch) => {
  dispatch(
    setAttachmentTimelineLoading(
      attachmentInfo.index,
      stepIndex,
      attachmentInfo,
      true
    )
  );
  removeAttachmentRequest(HttpClient, attachmentInfo.index, instanceId)
    .then((res) => {
      dispatch(
        removeAttachmentFromTimelineStep(stepIndex, attachmentInfo.index)
      );
    })
    .catch(() => {
      Notifications.notify(errorMessage, "error", "Process", 5000);
      dispatch(
        setAttachmentTimelineLoading(
          attachmentInfo.index,
          stepIndex,
          attachmentInfo,
          false
        )
      );
    });
};
