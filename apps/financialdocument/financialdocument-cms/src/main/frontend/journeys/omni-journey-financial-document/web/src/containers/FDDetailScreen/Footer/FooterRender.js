import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { styles, componentName } from "./FooterStyles";
import Grid from "@ui-lib/core/Grid";
import Button from "@ui-lib/core/Button";
import TextField from "@ui-lib/core/TextField";
import { dateFormatter } from "../../../utils/infoFDUtils";
import PropTypes from "prop-types";

const footerFDFields = (FDHeader) => {
  return [
    { label: "Data de Início", value: dateFormatter(FDHeader.creationDate) },
    { label: "Data de Conclusão", value: dateFormatter(FDHeader.finishDate) },
  ];
};

const renderFooterField = (field, classes) => {
  return (
    <Grid item key={field.label}>
      <TextField
        name={field.label}
        label={field.label}
        value={field.value}
        disabled
        className={classes.disabledInfoField}
      />
    </Grid>
  );
};

const FooterRender = ({
  classes,
  financialDocumentHeader,
  handleBackClick,
}) => {
  return (
    <div className={classes.root}>
      <Grid container className={classes.container}>
        {footerFDFields(financialDocumentHeader).map((field) =>
          renderFooterField(field, classes)
        )}
      </Grid>
      <div className={classes.footerButtonsContainer}>
        <Button
          variant="contained"
          color="primary"
          onClick={() => handleBackClick()}
        >
          Voltar
        </Button>
      </div>
    </div>
  );
};

FooterRender.propTypes = {
  classes: PropTypes.object,
  financialDocumentHeader: PropTypes.object,
  handleBackClick: PropTypes.func,
};

export default withStyles(styles, { componentName })(FooterRender);
