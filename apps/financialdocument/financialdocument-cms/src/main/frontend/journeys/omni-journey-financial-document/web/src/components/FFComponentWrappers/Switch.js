import * as React from "react";
import Switch from "@ui-lib/core/Switch";
import FormControlLabel from "@ui-lib/core/FormControlLabel";
import PropTypes from "prop-types";

const SwitchWrapper = ({
  input: { name, onChange, value },
  label,
  meta,
  ...rest
}) => {
  return (
    <FormControlLabel
      control={
        <Switch
          {...rest}
          checked={value}
          onChange={onChange}
          name={name}
          color="primary"
        />
      }
      label={label}
    />
  );
};

SwitchWrapper.propTypes = {
  input: PropTypes.object,
  meta: PropTypes.object,
  label: PropTypes.object,
};

export default SwitchWrapper;
