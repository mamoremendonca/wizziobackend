import * as React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { componentName, styles } from "./DetailsFieldsStyle";
import Grid from "@ui-lib/core/Grid";
import Separator from "../../components/Separator";
import TextField from "@ui-lib/core/TextField";
import { detailsFieldsMapper } from "../../utils";
import { FINANCIAL_DOCUMENT_TYPE_VALUES } from "../../constants";
import Countdown from "../Countdown";
import PropTypes from "prop-types";

const DetailsFields = ({
  classes,
  I18nProvider,
  refData,
  financialDocumentHeader,
}) => {
  const mappedValues = detailsFieldsMapper(financialDocumentHeader, refData);
  return (
    <Grid className={classes.root}>
      <Grid item xs={12}>
        <Separator
          text={I18nProvider.Labeli18n(
            "createFDScreen.form.financial_document.header.title"
          )}
        />
      </Grid>
      <Grid item xs={12} className={classes.gridRow}>
        <Grid item xs={3}>
          <TextField
            fullWidth
            name={I18nProvider.Labeli18n(
              "createFDScreen.scanning_team_form.type_field.label"
            )}
            label={I18nProvider.Labeli18n(
              "createFDScreen.scanning_team_form.type_field.label"
            )}
            value={mappedValues.type}
            disabled
            className={classes.disabledInfoField}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            fullWidth
            name={I18nProvider.Labeli18n(
              "createFDScreen.scanning_team_form.supplierName_field.label"
            )}
            label={I18nProvider.Labeli18n(
              "createFDScreen.scanning_team_form.supplierName_field.label"
            )}
            value={mappedValues.supplierName}
            disabled
            className={classes.disabledInfoField}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            fullWidth
            name={I18nProvider.Labeli18n(
              "createFDScreen.scanning_team_form.supplierCode_field.label"
            )}
            label={I18nProvider.Labeli18n(
              "createFDScreen.scanning_team_form.supplierCode_field.label"
            )}
            value={mappedValues.supplierCode}
            disabled
            className={classes.disabledInfoField}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            fullWidth
            name={I18nProvider.Labeli18n(
              "createFDScreen.scanning_team_form.supplierTaxNumber_field.label"
            )}
            label={I18nProvider.Labeli18n(
              "createFDScreen.scanning_team_form.supplierTaxNumber_field.label"
            )}
            value={mappedValues.supplierTaxNumber}
            disabled
            className={classes.disabledInfoField}
          />
        </Grid>
      </Grid>
      <Grid item xs={12} className={classes.gridRow}>
        <Grid item xs={3}>
          {financialDocumentHeader.type ===
          FINANCIAL_DOCUMENT_TYPE_VALUES.CREDIT_NOTE ? (
            <TextField
              fullWidth
              name={I18nProvider.Labeli18n(
                "createFDScreen.scanning_team_form.associated_document_field.label"
              )}
              label={I18nProvider.Labeli18n(
                "createFDScreen.scanning_team_form.associated_document_field.label"
              )}
              value={mappedValues.associatedDocument}
              disabled
              className={classes.disabledInfoField}
            />
          ) : (
            <TextField
              fullWidth
              name={I18nProvider.Labeli18n(
                "createFDScreen.scanning_team_form.purchaseOrder_field.label"
              )}
              label={I18nProvider.Labeli18n(
                "createFDScreen.scanning_team_form.purchaseOrder_field.label"
              )}
              value={mappedValues.purchaseOrderFriendlyNumber}
              disabled
              className={classes.disabledInfoField}
            />
          )}
        </Grid>
        <Grid item xs={3}>
          <TextField
            fullWidth
            name={I18nProvider.Labeli18n("vat_exclusive")}
            label={I18nProvider.Labeli18n("vat_exclusive")}
            value={`${parseFloat(mappedValues.totalWithoutTax).toFixed(2)} €`}
            disabled
            className={classes.disabledInfoField}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            fullWidth
            name={I18nProvider.Labeli18n(
              "createFDScreen.scanning_team_form.description_field.label"
            )}
            label={I18nProvider.Labeli18n(
              "createFDScreen.scanning_team_form.description_field.label"
            )}
            value={mappedValues.description}
            disabled
            className={classes.disabledInfoField}
          />
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <Separator text="ONEY" />
      </Grid>
      <Grid item xs={12} className={classes.gridRow}>
        <Grid item xs={3}>
          <TextField
            fullWidth
            name={I18nProvider.Labeli18n("department")}
            label={I18nProvider.Labeli18n("department")}
            value={mappedValues.department}
            disabled
            className={classes.disabledInfoField}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            fullWidth
            name={I18nProvider.Labeli18n("project")}
            label={I18nProvider.Labeli18n("project")}
            value={mappedValues.project}
            disabled
            className={classes.disabledInfoField}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            fullWidth
            name={I18nProvider.Labeli18n(
              "createFDScreen.scanning_team_form.emissionDate_field.label"
            )}
            label={I18nProvider.Labeli18n(
              "createFDScreen.scanning_team_form.emissionDate_field.label"
            )}
            type={"date"}
            value={mappedValues.emissionDate}
            disabled
            className={classes.disabledInfoField}
          />
        </Grid>
        <Grid item xs={3} className={classes.fieldContainerGrid}>
          <TextField
            fullWidth
            type={"date"}
            name={I18nProvider.Labeli18n("approval_date_limit")}
            label={I18nProvider.Labeli18n("approval_date_limit")}
            value={mappedValues.expirationDate}
            disabled
            className={classes.disabledInfoField}
          />
          <Countdown
            dateToCompare={mappedValues.expirationDate}
            i18nProvider={I18nProvider}
          />
        </Grid>
      </Grid>
      <Grid item xs={12} className={classes.gridRow}>
        <Grid item xs={3}>
          <TextField
            fullWidth
            name={I18nProvider.Labeli18n("internal_order")}
            label={I18nProvider.Labeli18n("internal_order")}
            value={mappedValues.internalOrderCode}
            disabled
            className={classes.disabledInfoField}
          />
        </Grid>
        <Grid item xs={3}>
          <TextField
            fullWidth
            name={I18nProvider.Labeli18n(
              "createFDScreen.scanning_team_form.granting_field.label"
            )}
            label={I18nProvider.Labeli18n(
              "createFDScreen.scanning_team_form.granting_field.label"
            )}
            value={I18nProvider.Texti18n(
              `details.granting_field.value.${mappedValues.granting}.description`
            )}
            disabled
            className={classes.disabledInfoField}
          />
        </Grid>
      </Grid>
    </Grid>
  );
};

DetailsFields.propTypes = {
  classes: PropTypes.object,
  I18nProvider: PropTypes.object,
  refData: PropTypes.object,
  financialDocumentHeader: PropTypes.object,
};

export default withStyles(styles, { name: componentName })(DetailsFields);
