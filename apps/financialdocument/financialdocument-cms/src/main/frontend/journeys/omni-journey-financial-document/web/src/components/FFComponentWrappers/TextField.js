import * as React from "react";

import TextField from "@ui-lib/core/TextField";

import { withStyles } from "@ui-lib/core/styles";
import styles from "./styles";
import PropTypes from "prop-types";

const TextFieldWrapper = ({
  input: { name, onChange, value, ...restInput },
  meta,
  disabled,
  isMoney,
  classes,
  ...rest
}) => {
  const showError =
    ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) &&
    meta.touched;

  const className = disabled ? classes.textFieldDisabled : undefined;
  let defaultValue = disabled && !value ? "-" : undefined;
  let formattedValue = disabled ? undefined : ""; // hack to clear dash when the field is enabled.

  if (isMoney) {
    if (value) {
      formattedValue = `${value} €`;
    }
    defaultValue = disabled && !value ? "0 €" : undefined;
  } else if (value) {
    formattedValue = value;
  }

  return (
    <React.Fragment>
      <TextField
        {...rest}
        multiline
        rowsMax="4"
        name={name}
        helperText={showError ? meta.error || meta.submitError : undefined}
        error={showError}
        inputProps={restInput}
        onChange={onChange}
        className={className}
        value={formattedValue}
        defaultValue={defaultValue} //hack to show a dash when the value is empty and disabled
        disabled={disabled}
        style={{ paddingTop: 5 }}
      />
    </React.Fragment>
  );
};

TextFieldWrapper.propTypes = {
  input: PropTypes.object,
  meta: PropTypes.object,
  disabled: PropTypes.bool,
  classes: PropTypes.object,
  isMoney: PropTypes.bool,
};

export default withStyles(styles)(TextFieldWrapper);
