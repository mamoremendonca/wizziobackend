import React from "react";
import { componentName, styles } from "./FDDetailScreenStyles";
import { withStyles } from "@ui-lib/core/styles";
import FDTableItems from "../../components/FDTableItems";
import DetailsFields from "../../components/DetailsFields/DetailsFields";
import Separator from "../../components/Separator";
import FDTotalTableItems from "../../components/FDTableItems/FDTotalTableItems";
import Timeline from "../../components/Timeline";
import { Config } from "../../config";
import Footer from "./Footer";
import Button from "@ui-lib/core/Button";
import ConfirmationDialog from "../../components/Dialog/ConfirmationDialog";
import PropTypes from "prop-types";

const FDDetailScreenRender = ({
  classes,
  financialDocumentData,
  I18nProvider,
  refData,
  totalItemsValue,
  handleBackClick,
  handleManageDf,
  confirmationDialogOptions,
}) => {
  return (
    <div className={classes.root}>
      <div className={classes.approvalDetails}>
        <div className={classes.footerButtonsContainer}>
          {financialDocumentData.canClaim && (
            <Button
              color="primary"
              variant="contained"
              onClick={handleManageDf}
            >
              {I18nProvider.Labeli18n(
                "fdDetailsScreen.claim.financial_document.button.label"
              )}
            </Button>
          )}
        </div>
        <div className={classes.info}>
          <DetailsFields
            I18nProvider={I18nProvider}
            refData={refData}
            financialDocumentHeader={
              financialDocumentData.financialDocumentHeader
            }
          />
        </div>
        <div className={classes.tableItems}>
          <Separator
            text={I18nProvider.Labeli18n(
              "fdDetailScreenRender.separator.items.label"
            )}
          />
          <FDTableItems
            I18nProvider={I18nProvider}
            itemsList={financialDocumentData.items}
            financialDocumentHeader={
              financialDocumentData.financialDocumentHeader
            }
            refData={refData}
          />
          <FDTotalTableItems
            I18nProvider={I18nProvider}
            total={totalItemsValue}
          />
        </div>
        <div className={classes.footer}>
          <Footer
            financialDocumentHeader={
              financialDocumentData.financialDocumentHeader
            }
            handleBackClick={handleBackClick}
          />
        </div>
      </div>
      <div className={classes.timeline}>
        <Timeline dataSource={Config.FINANCIAL_DOCUMENT_REPOSITORY} />
      </div>
      <ConfirmationDialog
        open={confirmationDialogOptions.isOpen}
        title={confirmationDialogOptions.title}
        onClose={confirmationDialogOptions.closeHandler}
      />
    </div>
  );
};

FDDetailScreenRender.propTypes = {
  classes: PropTypes.object,
  I18nProvider: PropTypes.object,
  refData: PropTypes.object,
  totalItemsValue: PropTypes.number,
  financialDocumentData: PropTypes.object,
  handleBackClick: PropTypes.func,
  handleManageDf: PropTypes.func,
  confirmationDialogOptions: PropTypes.object,
};

export default withStyles(styles, { name: componentName })(
  FDDetailScreenRender
);
