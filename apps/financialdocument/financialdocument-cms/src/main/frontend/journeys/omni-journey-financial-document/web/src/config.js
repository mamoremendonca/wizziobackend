export const Config = {
  FINANCIAL_DOCUMENT_BASE_URL: "/bin/mvc.do/financialdocument/v1",
  FINANCIAL_DOCUMENT_PROCESS_CONTINUITY: "/instance/",
  FINANCIAL_DOCUMENT_REPOSITORY: "/repository/",

  ATTACHMENT_URL_PART: "/attachment/",
  DEFAULT_FINANCIAL_DOCUMENT_ATTACHMENT_TYPE: "FINANCIAL_DOCUMENT",
  ITEM_URL: "/item/",
  ITEM_ALL_URL: "/item/all",
  TIMELINE_URL: "/timeline",
  PURCHASE_ORDER_URL_PART: "/purchaseorder",
  FINANCIAL_DOCUMENT_URL_PART: "/financialdocument",

  //#region FINANCIAL DOCUMENT ACTION METHODS
  SUBMIT_CREATION_URL_PART: "/submit/creation",
  APPROVAL_ACTIONS_URL_PART: {
    APPROVE: "/approve",
    RETURN: "/return",
    REJECT: "/reject",
  },
  CANCEL_DF_URL_PART: "/cancel",
  CLAIM_DF_URL_PART: "/claim",
  DRAFT_URL_PART: "/draft",
  CREATION_URL_PART: "/creation",
  SUBMIT_APPROVAL_URL_PART: "/submit/approval",
  //#endregion

  //#region REST METHODS
  METHOD_POST: "post",
  METHOD_GET: "get",
  METHOD_PUT: "put",
  METHOD_DELETE: "delete",
  //#endregion

  //LIST METHODS
  POST_SEARCH_REPOSITORY_URL:
    "/bin/mvc.do/financialdocument/v1/repository/list",

  //#region Reference Data Request
  ONEY_REFERENCE_LISTS_URL: "/bin/mvc.do/oneyapp/v1/reference/lists",
  SUPPLIER_URL: "/bin/mvc.do/oneyapp/v1/reference/supplier",
  FINANCIAL_DOCUMENT_REFERENCE_LISTS_URL:
    "/bin/mvc.do/financialdocument/v1/reference/lists",
  //#endregion

  //#PDF Viewer
  WORKER_URL:
    "/foundation/v3/deliverables/pdfjs-dist/2.3.200/pdf.worker.min.js",
  //#endregion

  //#Upload PDF
  INVOICE_DOCUMENT_URL_PART: "/document/",
};
