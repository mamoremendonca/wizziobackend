import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { ComponentName, styles } from "./AccountsPayableTeamStyles";
import Grid from "@ui-lib/core/Grid";
import { Field } from "react-final-form";
import * as constants from "../../../../constants";
import {
  Select,
  TextField,
  BasicDateField,
  Switch,
  AsyncSearchField,
} from "../../../../components/FFComponentWrappers";
import PropTypes from "prop-types";

import { isRequired, currentDate } from "../../validator";
import {
  Condition,
  FilterOptionsBy,
  WhenFieldChanges,
} from "../../react-final-form-helpers";
import NumberInput from "../../../../components/FFComponentWrappers/NumberInput";
import { CurrencyType } from "../../../../utils";

const filterGroups = (formValues, allData, isFDCombinedWithPO) => {
  let groups = allData;

  if (formValues.departmentCode)
    groups = allData.filter(
      (group) =>
        group.departmentCode === formValues.departmentCode.code ||
        (isFDCombinedWithPO && group.departmentCode === "")
    );

  if (formValues[constants.GROUP_CODE] && !groups.length) {
    groups.push({
      value: formValues[constants.GROUP_CODE],
      label: formValues[constants.GROUP_CODE],
      code: formValues[constants.GROUP_CODE],
      description: formValues[constants.GROUP_CODE],
      active: true,
      departmentCode: "",
    });
  }

  return groups;
};

const AccountsPayableTeam = ({
  showAccountsPayableTeam,
  refData,
  I18nProvider,
  classes,
  isFDCombinedWithPO,
  formValues,
  associatePurchaseOrder,
  associateFinancialDocument,
  hasDepartmentChanged,
}) => {
  let groups = filterGroups(formValues, refData.groups, isFDCombinedWithPO);

  return (
    <Grid container>
      {showAccountsPayableTeam && (
        <Grid className={classes.gridRow} container spacing={32} item xs={12}>
          <Condition
            when={constants.FINANCIAL_DOCUMENT_TYPE}
            isNot={constants.FINANCIAL_DOCUMENT_TYPE_VALUES.CREDIT_NOTE}
          >
            <Grid item xs={4}>
              <Field
                name={constants.PURCHASE_ORDER_FRIENDLY_NUMBER}
                component={AsyncSearchField}
                label={I18nProvider.Labeli18n(
                  "createFDScreen.scanning_team_form.purchaseOrder_field.label"
                )}
                placeholder={I18nProvider.Texti18n(
                  "createFDScreen.accounts_payable_team_form.search.placeholder.purchaseOrder_field.label"
                )}
                InputLabelProps={{ shrink: true }}
                fullWidth
                promiseFn={associatePurchaseOrder}
              />
            </Grid>
          </Condition>
          <Condition
            when={constants.FINANCIAL_DOCUMENT_TYPE}
            is={constants.FINANCIAL_DOCUMENT_TYPE_VALUES.CREDIT_NOTE}
          >
            <Grid item xs={4}>
              <Field
                name={constants.ASSOCIATED_DOCUMENT_FRIENDLY_NUMBER}
                component={AsyncSearchField}
                label={I18nProvider.Labeli18n(
                  "createFDScreen.scanning_team_form.associated_document_field.label"
                )}
                placeholder={I18nProvider.Texti18n(
                  "createFDScreen.accounts_payable_team_form.search.placeholder.associated_document_field.label"
                )}
                InputLabelProps={{ shrink: true }}
                fullWidth
                promiseFn={associateFinancialDocument}
              />
            </Grid>
          </Condition>
          <Grid item xs={4}>
            <Field
              name={constants.TOTAL_WITHOUT_TAX}
              component={NumberInput}
              label={I18nProvider.Labeli18n("priceWithoutTax")}
              fullWidth
              currency={CurrencyType.EUR}
              InputLabelProps={{ shrink: true }}
              disabled
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name={constants.DESCRIPTION_FIELD}
              component={TextField}
              label={I18nProvider.Labeli18n(
                "createFDScreen.scanning_team_form.description_field.label"
              )}
              InputLabelProps={{ shrink: true }}
              fullWidth
            />
          </Grid>
        </Grid>
      )}
      {showAccountsPayableTeam && (
        <Grid className={classes.gridRow} container spacing={32} item xs={12}>
          <WhenFieldChanges
            field={constants.INTERNAL_ORDER_CODE}
            becomes={!!formValues[constants.INTERNAL_ORDER_CODE]}
            set={constants.PROJECT_CODE}
            to={null}
          />
          <WhenFieldChanges
            field={constants.PROJECT_CODE}
            becomes={!!formValues[constants.PROJECT_CODE]}
            set={constants.INTERNAL_ORDER_CODE}
            to={null}
          />

          <Grid item xs={4}>
            <Field
              name={constants.DEPARTMENT_CODE}
              component={Select}
              label={I18nProvider.Labeli18n("department")}
              formControlProps={{ fullWidth: true }}
              fullWidth
              InputLabelProps={{ shrink: true }}
              options={refData.departments}
              validate={isRequired(
                I18nProvider.Texti18n("validator.required.field.error.message")
              )}
              required
              disabled={isFDCombinedWithPO}
            />
          </Grid>
          <Grid item xs={4}>
            <FilterOptionsBy
              fieldName={constants.DEPARTMENT_CODE}
              options={groups}
              optionPropertyName="departmentCode"
            >
              <Field
                name={constants.GROUP_CODE}
                component={Select}
                label={I18nProvider.Labeli18n("group")}
                validate={isRequired(
                  I18nProvider.Texti18n(
                    "validator.required.field.error.message"
                  )
                )}
                formControlProps={{
                  fullWidth: true,
                }}
                fullWidth
                InputLabelProps={{ shrink: true }}
                required
                disabled={
                  isFDCombinedWithPO || !formValues[constants.DEPARTMENT_CODE]
                }
                // menuIsOpen={hasDepartmentChanged || undefined}
              />
            </FilterOptionsBy>
          </Grid>
          <Grid item xs={4}>
            <FilterOptionsBy
              fieldName={constants.DEPARTMENT_CODE}
              options={refData.projects}
              optionPropertyName="departmentCode"
            >
              <Field
                name={constants.PROJECT_CODE}
                component={Select}
                label={I18nProvider.Labeli18n("project")}
                formControlProps={{ fullWidth: true }}
                fullWidth
                InputLabelProps={{ shrink: true }}
                disabled={
                  isFDCombinedWithPO ||
                  !!formValues[constants.INTERNAL_ORDER_CODE] ||
                  !formValues[constants.DEPARTMENT_CODE]
                }
              />
            </FilterOptionsBy>
          </Grid>
        </Grid>
      )}
      <Grid className={classes.gridRow} container spacing={32} item xs={12}>
        <Grid item xs={4}>
          <FilterOptionsBy
            fieldName={constants.DEPARTMENT_CODE}
            options={refData.internalOrders}
            optionPropertyName="departmentCode"
          >
            <Field
              name={constants.INTERNAL_ORDER_CODE}
              component={Select}
              label={I18nProvider.Labeli18n("internal_order")}
              formControlProps={{ fullWidth: true }}
              fullWidth
              InputLabelProps={{ shrink: true }}
              disabled={
                isFDCombinedWithPO ||
                !!formValues[constants.PROJECT_CODE] ||
                !formValues[constants.DEPARTMENT_CODE]
              }
            />
          </FilterOptionsBy>
        </Grid>
        <Grid item xs={4}>
          <Field
            name={constants.CREATION_DATE}
            label={I18nProvider.Labeli18n(
              "createFDScreen.scanning_team_form.creationDate_field.label"
            )}
            component={BasicDateField}
            fullWidth
            InputLabelProps={{ shrink: true }}
            required
            disabled
          />
        </Grid>
        {showAccountsPayableTeam && (
          <Grid item xs={4}>
            <Field
              min={currentDate}
              name={constants.EXPIRATION_DATE}
              label={I18nProvider.Labeli18n("approval_date_limit")}
              component={BasicDateField}
              fullWidth
              InputLabelProps={{ shrink: true }}
              required
              validate={isRequired(
                I18nProvider.Texti18n("validator.required.field.error.message")
              )}
            />
          </Grid>
        )}
      </Grid>
      <Grid className={classes.gridRow} container spacing={32} item xs={12}>
        {showAccountsPayableTeam && (
          <Grid item xs={4}>
            <Field
              name={constants.GRANTING}
              label={I18nProvider.Labeli18n(
                "createFDScreen.scanning_team_form.granting_field.label"
              )}
              component={Switch}
            />
          </Grid>
        )}
      </Grid>
    </Grid>
  );
};

AccountsPayableTeam.propTypes = {
  I18nProvider: PropTypes.object,
  classes: PropTypes.object,
  refData: PropTypes.object,
  services: PropTypes.object,
  Loading: PropTypes.object,
  formValues: PropTypes.object,
  showAccountsPayableTeam: PropTypes.bool,
  associateFinancialDocument: PropTypes.func,
  associatePurchaseOrder: PropTypes.func,
  isFDCombinedWithPO: PropTypes.bool,
};

export default withStyles(styles, { name: ComponentName })(AccountsPayableTeam);
