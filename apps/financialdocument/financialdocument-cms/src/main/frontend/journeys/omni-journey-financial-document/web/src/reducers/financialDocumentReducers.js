import * as constants from "../constants";
import { Map } from "immutable";

const initialState = {
  financialDocumentId: undefined,
  financialDocumentData: undefined,
  financialDocumentAttachments: undefined,
  //Manager Purchase Order Resoruces
  dependencies: Map({ refData: false }),
  ///FD List area
  financialDocumentsList: [],
  totalFinancialDocuments: 0,
  numberOfRecords: 0,
  // Filter area
  searchCriteria: {
    filterCriteria: { ...constants.INITIAL_FILTER_CRITERIA },
    sortCriteria: {
      orderField: constants.DEFAULT_SORTED_FIELD["orderField"],
      orderType: constants.DEFAULT_SORTED_FIELD["orderType"],
    },
  },
  timelineData: undefined,
};

export const financialDocumentReducer = (state, action) => {
  if (state === undefined) return initialState;

  switch (action.type) {
    case constants.SET_FINANCIAL_DOCUMENT: {
      const financialDocumentData = action.payload;
      const financialDocumentId = financialDocumentData.jwcontext
        ? financialDocumentData.jwcontext.id
        : financialDocumentData.financialDocumentHeader.repositoryId;
      return { ...state, financialDocumentId, financialDocumentData };
    }
    case constants.CLEAR_FINANCIAL_DOCUMENT: {
      return {
        ...state,
        financialDocumentId: undefined,
        financialDocumentData: undefined,
        financialDocumentAttachments: undefined,
        timelineData: undefined,
      };
    }
    case constants.CREATE_FINANCIAL_DOCUMENT: {
      const message = action.payload;
      return { ...state, message };
    }
    case constants.CREATE_FINANCIAL_DOCUMENT_SUCESS: {
      const financialDocumentId = action.payload.id;
      const financialDocumentCreationDate = action.payload.creationDate;
      return { ...state, financialDocumentId, financialDocumentCreationDate };
    }
    case constants.CREATE_FINANCIAL_DOCUMENT_ERROR: {
      const message = action.payload;
      return { ...state, message };
    }
    case constants.DELETE_FINANCIAL_DOCUMENT: {
      const refDatas = action.payload;
      return { ...state, refDatas };
    }
    case constants.UPDATE_FINANCIAL_DOCUMENT: {
      const message = action.payload;
      return { ...state, message };
    }
    case constants.SET_FD_LIST_PROCESSES:
      return {
        ...state,
        financialDocumentsList: action.payload.processes,
        totalFinancialDocuments: action.payload.totals,
        numberOfRecords: action.payload.numberOfRecords,
      };
    case constants.UPDATE_FILTER_CRITERIA: {
      return {
        ...state,
        searchCriteria: {
          ...state.searchCriteria,
          filterCriteria: action.payload,
        },
      };
    }
    case constants.UPDATE_SORTING_CRITERIA: {
      return {
        ...state,
        searchCriteria: {
          ...state.searchCriteria,
          sortCriteria: action.payload,
        },
      };
    }
    case constants.RESET_FILTER_CRITERIA: {
      return {
        ...state,
        searchCriteria: {
          ...state.searchCriteria,
          filterCriteria: { ...constants.INITIAL_FILTER_CRITERIA },
        },
      };
    }
    case constants.SET_FINANCIAL_DOCUMENT_DEPENDENCY: {
      return {
        ...state,
        dependencies: state.dependencies.set(action.payload, true),
      };
    }
    case constants.ADD_FINANCIAL_DOCUMENT_ITEM_SUCCESS: {
      let newItem = action.payload.item;
      let itemIndex = action.payload.response.index;
      let newItems = [...state.financialDocumentData.items];
      newItems.push({ ...newItem, index: itemIndex });
      return {
        ...state,
        financialDocumentData: {
          ...state.financialDocumentData,
          items: newItems,
        },
      };
    }
    case constants.EDIT_FINANCIAL_DOCUMENT_ITEM_SUCCESS: {
      const newItem = action.payload.item;
      let newItems = [];
      const oldItems = [...state.financialDocumentData.items];
      oldItems.map((item) =>
        item.index === newItem.index
          ? newItems.push({ ...newItem })
          : newItems.push({ ...item })
      );
      return {
        ...state,
        financialDocumentData: {
          ...state.financialDocumentData,
          items: newItems,
        },
      };
    }
    case constants.REMOVE_FINANCIAL_DOCUMENT_ITEM_SUCCESS: {
      const index = action.payload.index;
      const oldItems = [...state.financialDocumentData.items];
      const newItems = oldItems.filter((item) => {
        return item.index !== index;
      });
      return {
        ...state,
        financialDocumentData: {
          ...state.financialDocumentData,
          items: newItems,
        },
      };
    }
    case constants.SET_TIMELINE: {
      return { ...state, timelineData: action.payload };
    }
    case constants.ADD_ATTACHMENT_TO_TIMELINE_STEP: {
      const { stepIndex, attachmentInfo } = action.payload;
      let timeline = [...state.timelineData];
      if (
        timeline[stepIndex].attachments &&
        timeline[stepIndex].attachments.length > 0
      ) {
        timeline[stepIndex].attachments.push(attachmentInfo);
      } else {
        timeline[stepIndex].attachments = [attachmentInfo];
      }
      return {
        ...state,
        timelineData: timeline,
      };
    }
    case constants.REMOVE_ATTACHMENT_FROM_TIMELINE_STEP: {
      const { stepIndex, attachmentIndex } = action.payload;
      let timeline = [...state.timelineData];
      if (
        timeline[stepIndex].attachments &&
        timeline[stepIndex].attachments.length > 0
      ) {
        timeline[stepIndex].attachments = timeline[
          stepIndex
        ].attachments.filter(
          (attachment) => attachment.index !== attachmentIndex
        );
      }
      return {
        ...state,
        timelineData: timeline,
      };
    }
    case constants.SET_ATTACHMENT_TIMELINE_LOADING: {
      const {
        attachmentIndex,
        stepIndex,
        attachmentInfo,
        isLoading,
      } = action.payload;
      let timeline = [...state.timelineData];
      timeline[stepIndex].attachments = timeline[stepIndex].attachments.map(
        (attachment) => {
          if (attachment.index === attachmentInfo.index) {
            return {
              ...attachment,
              index: attachmentIndex,
              isLoading: isLoading,
            };
          } else {
            return attachment;
          }
        }
      );
      return {
        ...state,
        timelineData: timeline,
      };
    }
    default:
      return state;
  }
};
