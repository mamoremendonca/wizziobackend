import { SORT_FIELDS_MAPPER, SORTING_ORDER_MAPPER } from "../constants";
import qs from "qs";

export function prepareSearchCriteria(searchCriteria) {
  const serverSearchCriteria = {};
  if (searchCriteria.filterCriteria) {
    let filterParams = Object.keys(searchCriteria.filterCriteria);
    filterParams.forEach((param) => {
      serverSearchCriteria[param] =
        searchCriteria.filterCriteria[param] &&
        searchCriteria.filterCriteria[param].length !== 0
          ? searchCriteria.filterCriteria[param]
          : null;
    });
  }
  return serverSearchCriteria;
}

export const prepareSortCriteria = (sortCriteria) => {
  const sort = {
    orderField: SORT_FIELDS_MAPPER[sortCriteria.orderField],
    orderType: SORTING_ORDER_MAPPER[sortCriteria.orderType],
  };
  return "?" + qs.stringify(sort, { filter: ["orderField", "orderType"] });
};
