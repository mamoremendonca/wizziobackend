import React, { Component } from "react";
import { withRouter } from "react-router";
import HeaderRender from "./HeaderBarRender";
import PropTypes from "prop-types";

class HeaderBar extends Component {
  localBackClickHandler = (ev) => {
    const { backClickHandler, history } = this.props;
    if (backClickHandler) {
      backClickHandler();
    } else {
      history.goBack();
    }
  };

  render() {
    const { children, title, hideBackButton } = this.props;
    return (
      <HeaderRender
        hideBackButton={hideBackButton}
        title={title}
        localBackClickHandler={this.localBackClickHandler}
        children={children}
      />
    );
  }
}

HeaderBar.propTypes = {
  children: PropTypes.object,
  title: PropTypes.string,
  hideBackButton: PropTypes.bool,
  history: PropTypes.object,
  backClickHandler: PropTypes.func,
};

export default withRouter(HeaderBar);
