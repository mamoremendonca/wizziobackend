import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { styles, ComponentName } from "./HeaderBarStyles";
import IconButton from "@ui-lib/core/IconButton";
import Icon from "@ui-lib/core/Icon";
import Typography from "@ui-lib/core/Typography";
import PropTypes from "prop-types";

function HeaderBarRender({
  classes,
  localBackClickHandler,
  hideBackButton,
  title,
  children,
}) {
  return (
    <div className={classes.root}>
      {!hideBackButton && (
        <IconButton onClick={localBackClickHandler}>
          <Icon>arrow_back</Icon>
        </IconButton>
      )}
      <Typography
        variant="h3"
        color="textPrimary"
        className={classes.titleContainer}
      >
        {title}
      </Typography>
      <div className={classes.childrenContainer}>{children}</div>
    </div>
  );
}

HeaderBarRender.propTypes = {
  classes: PropTypes.object,
  localBackClickHandler: PropTypes.func,
  hideBackButton: PropTypes.bool,
  title: PropTypes.string,
  children: PropTypes.array,
};

export default withStyles(styles, { name: ComponentName })(HeaderBarRender);
