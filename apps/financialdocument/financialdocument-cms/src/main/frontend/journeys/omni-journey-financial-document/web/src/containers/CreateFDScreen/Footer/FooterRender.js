// @flow
import * as React from "react";
import Button from "@ui-lib/core/Button";
import { withStyles } from "@ui-lib/core/styles";
import { ComponentName, styles } from "./FooterStyles";
import { DELETE_FD_BUTTON_ID } from "../../../constants";
import PropTypes from "prop-types";

const FooterRender = ({
  classes,
  I18nProvider,
  handleOnSubmit,
  handleOnDelete,
  isSubmitButtonDisabled,
}) => {
  return (
    <div className={classes.root}>
      <Button id={DELETE_FD_BUTTON_ID} type="submit" onClick={handleOnDelete}>
        {I18nProvider.Labeli18n(
          "createFDScreen.form.delete_button.description"
        )}
      </Button>
      <Button
        type="submit"
        disabled={isSubmitButtonDisabled}
        variant="contained"
        color="primary"
        className={classes.marginButton}
        onClick={handleOnSubmit}
      >
        {I18nProvider.Labeli18n(
          "createFDScreen.form.submit_button.description"
        )}
      </Button>
    </div>
  );
};

FooterRender.propTypes = {
  classes: PropTypes.object,
  I18nProvider: PropTypes.object,
  handleOnDelete: PropTypes.func,
  handleOnSubmit: PropTypes.func,
  isSubmitButtonDisabled: PropTypes.bool,
};

export default withStyles(styles, { name: ComponentName })(FooterRender);
