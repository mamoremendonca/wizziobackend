import React, { Component } from "react";
import { connect } from "react-redux";
import FooterRender from "./FooterRender";
import { deleteFinancialDocumentRequest } from "../../../actions/services/financialDocumentServiceActions";
import PropTypes from "prop-types";

class Footer extends Component {
  state = {
    confirmationDialogOptions: {
      open: false,
      title: undefined,
      onclose: undefined,
    },
  };

  handleOnCloseCancelProcess = async (decision) => {
    if (decision) {
      const {
        HttpClient,
        I18nProvider: { Texti18n },
        Notifications,
        Loading,
        financialDocumentData: {
          jwcontext: { id },
        },
        JourneySettings: { instance_id },
        JourneyActions,
      } = this.props;

      try {
        await Loading.request("setLoading", "JOURNEY");
        await deleteFinancialDocumentRequest(HttpClient, id);
        JourneyActions.closeJourney(instance_id);
      } catch {
        Notifications.notify(
          Texti18n("service.delete.financial_document.error.message"),
          "error",
          "Process",
          5000
        );
      } finally {
        await Loading.request("stopLoading");
      }
    }
    this.setState({
      confirmationDialogOptions: {
        ...this.state.confirmationDialogOptions,
        open: false,
      },
    });
  };

  handleCancel = () => {
    const {
      I18nProvider: { Labeli18n },
    } = this.props;
    this.setState({
      confirmationDialogOptions: {
        open: true,
        title: Labeli18n(
          "fdRejectionScreen.footer.confirmation_dialog.cancel.title"
        ),
        onClose: this.handleOnCloseCancelProcess,
      },
    });
  };

  render() {
    const { I18nProvider } = this.props;
    const { confirmationDialogOptions } = this.state;
    return (
      <FooterRender
        onCancel={this.handleCancel}
        I18nProvider={I18nProvider}
        confirmationDialogOptions={confirmationDialogOptions}
      />
    );
  }
}

Footer.propTypes = {
  classes: PropTypes.object,
  I18nProvider: PropTypes.object,
  HttpClient: PropTypes.object,
  Loading: PropTypes.object,
  financialDocumentData: PropTypes.object,
  JourneyActions: PropTypes.object,
  JourneySettings: PropTypes.object,
  Notifications: PropTypes.object,
};

const mapStateToProps = ({
  journey: {
    services: {
      HttpClient,
      I18nProvider,
      Notifications,
      Loading,
      JourneyActions,
    },
    settings,
  },
  financialDocumentReducer: { financialDocumentData },
}) => ({
  HttpClient,
  I18nProvider,
  Notifications,
  Loading,
  financialDocumentData,
  JourneySettings: settings,
  JourneyActions,
});

export default connect(mapStateToProps)(Footer);
