const ComponentName = "HeaderBar";

const styles = (theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    height: "28px",
    marginLeft: "20px",
    paddingRight: "5px",
  },
  titleContainer: {
    marginRight: "12px",
    minWidth: "99px",
  },
  childrenContainer: {
    marginLeft: theme.spacing.unit,
    width: "100%",
  },
});

export { styles, ComponentName };
