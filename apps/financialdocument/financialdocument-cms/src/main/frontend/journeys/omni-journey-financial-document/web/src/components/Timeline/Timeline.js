import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import TimelineRender from "./TimelineRender";
import * as constants from "../../constants/timeline";
import {
  activitiesTimelineDataMapper,
  getAttachmentListMapped,
} from "../../utils/mappers";
import { generateId } from "../../utils/createFDUtils";
import { downloadAttachmentRequest } from "../../actions/services/financialDocumentServiceActions";
import { Config } from "../../config";
import { getTimelineData, addTimelineAttachment } from "../../actions";
import PropTypes from "prop-types";

class Timeline extends Component {
  state = {
    currentTab: constants.TABS.ACTIVITIES,
    isLoading: false,
  };

  selectTab = ({ currentTarget: { id } }) => {
    this.setState({ currentTab: id });
  };

  refreshTimelineData = async () => {
    const {
      dataSource,
      getTimelineData,
      I18nProvider,
      Notifications,
      HttpClient,
      Loading,
      financialDocumentData,
    } = this.props;
    let instanceId = undefined;
    if (dataSource === Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY) {
      instanceId = financialDocumentData.jwcontext.id;
    } else {
      instanceId = financialDocumentData.financialDocumentHeader.repositoryId;
    }
    await Loading.request("setLoading", "JOURNEY");
    await getTimelineData(
      HttpClient,
      Notifications,
      instanceId,
      I18nProvider.Texti18n("service.get_timeline.error.message"),
      dataSource
    );
    Loading.request("stopLoading");
  };

  uploadAttachment = (files) => {
    const {
      financialDocumentData: {
        jwcontext: { id },
      },
      HttpClient,
      Notifications,
      addTimelineAttachment,
      I18nProvider,
      timelineData,
    } = this.props;
    const attachmentFile = files[0];
    if (attachmentFile) {
      const stepIndex = timelineData.findIndex(
        (item) => item.type === "TO_APPROVE"
      );
      const attachmentInfo = {
        index: generateId(true),
        name: attachmentFile.name,
        date: new Date().toISOString(),
        canBeDeletedByCurrentUser: true,
        isLoading: true,
      };
      addTimelineAttachment(
        HttpClient,
        Notifications,
        I18nProvider.Texti18n("service.addTimelineAttachment.error.message"),
        stepIndex,
        { attachmentFile: attachmentFile, attachmentInfo: attachmentInfo },
        id,
        "FINANCIAL_DOCUMENT"
      );
    }
  };

  downloadAttachment = async ({ id }) => {
    const {
      dataSource,
      I18nProvider,
      Notifications,
      HttpClient,
      FileSystem,
      Loading,
      financialDocumentData,
    } = this.props;
    let instanceId = undefined;
    if (dataSource === Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY) {
      instanceId = financialDocumentData.jwcontext.id;
    } else {
      instanceId = financialDocumentData.financialDocumentHeader.repositoryId;
    }
    downloadAttachmentRequest(
      HttpClient,
      Notifications,
      FileSystem,
      Loading,
      instanceId,
      id,
      I18nProvider.Texti18n("services.download_attachment.error.message"),
      dataSource
    );
  };

  render() {
    const {
      allowAddAttachments,
      setContainerLoadingDependencies,
      isLoading,
      dataSource,
      I18nProvider,
      timelineData,
    } = this.props;
    const { currentTab } = this.state;

    return (
      <TimelineRender
        I18nProvider={I18nProvider}
        currentTab={currentTab}
        allowAddAttachments={allowAddAttachments}
        uploadAttachment={this.uploadAttachment}
        downloadAttachment={this.downloadAttachment}
        selectTab={this.selectTab}
        setContainerLoadingDependencies={setContainerLoadingDependencies}
        isLoading={isLoading}
        dataSource={dataSource}
        refreshTimelineData={this.refreshTimelineData}
        activitiesTimeline={activitiesTimelineDataMapper(
          timelineData,
          I18nProvider
        )}
        timelineAttachments={getAttachmentListMapped(
          timelineData,
          I18nProvider
        )}
      />
    );
  }
}

Timeline.propTypes = {
  I18nProvider: PropTypes.object,
  getTimelineData: PropTypes.func,
  addTimelineAttachment: PropTypes.func,
  financialDocumentData: PropTypes.object,
  Loading: PropTypes.object,
  allowAddAttachments: PropTypes.bool,
  dataSource: PropTypes.string,
  Notifications: PropTypes.object,
  HttpClient: PropTypes.object,
  timelineData: PropTypes.any,
  FileSystem: PropTypes.object,
  setContainerLoadingDependencies: PropTypes.object,
  isLoading: PropTypes.bool,
};

const mapStateToProps = ({
  journey: {
    services: { I18nProvider, Notifications, FileSystem, HttpClient, Loading },
  },
  financialDocumentReducer: { financialDocumentData, timelineData },
}) => ({
  I18nProvider,
  Notifications,
  FileSystem,
  HttpClient,
  Loading,
  financialDocumentData,
  timelineData,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ getTimelineData, addTimelineAttachment }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Timeline);
