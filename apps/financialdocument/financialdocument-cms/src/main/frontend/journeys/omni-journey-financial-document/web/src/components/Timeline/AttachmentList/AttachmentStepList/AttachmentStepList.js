import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { styles, componentName } from "./AttachmentStepListStyles";
import Typography from "@ui-lib/core/Typography";
import Attachment from "./Attachment";
import PropTypes from "prop-types";

const AttachmentStepList = ({
  classes,
  dataSource,
  stepAttachments,
  setContainerLoadingDependencies,
}) => {
  return (
    <div className={classes.root}>
      <div className={classes.header}>
        <Typography variant="h5">{stepAttachments.status}</Typography>
        <div className={classes.divider} />
        <Typography>{stepAttachments.user} </Typography>
      </div>
      {stepAttachments.attachments.map((attachmentData, index) => {
        return (
          <Attachment
            key={`attachment-${index}`}
            dataSource={dataSource}
            attachmentData={attachmentData}
            setContainerLoadingDependencies={setContainerLoadingDependencies}
          />
        );
      })}
    </div>
  );
};

AttachmentStepList.propTypes = {
  classes: PropTypes.object,
  dataSource: PropTypes.string,
  stepAttachments: PropTypes.object,
  setContainerLoadingDependencies: PropTypes.func,
};

export default withStyles(styles, { name: componentName })(AttachmentStepList);
