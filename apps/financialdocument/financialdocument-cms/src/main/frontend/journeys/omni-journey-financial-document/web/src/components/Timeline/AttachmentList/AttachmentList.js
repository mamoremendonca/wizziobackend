import React from "react";
import { styles, componentName } from "./AttachmentListStyles";
import { withStyles } from "@ui-lib/core/styles";
import AttachmentStepList from "./AttachmentStepList";
import PropTypes from "prop-types";

const AttachmentList = (props) => {
  const {
    classes,
    dataSource,
    timelineAttachments,
    setContainerLoadingDependencies,
  } = props;

  return (
    <div className={classes.root}>
      {timelineAttachments.map(
        (stepAttachments, index) =>
          stepAttachments.attachments.length > 0 && (
            <AttachmentStepList
              key={`attachment-step-list-${index}`}
              dataSource={dataSource}
              stepAttachments={stepAttachments}
              setContainerLoadingDependencies={setContainerLoadingDependencies}
            />
          )
      )}
    </div>
  );
};

AttachmentList.propTypes = {
  classes: PropTypes.object,
  dataSource: PropTypes.string,
  timelineAttachments: PropTypes.array,
  setContainerLoadingDependencies: PropTypes.func,
};

export default withStyles(styles, { name: componentName })(AttachmentList);
