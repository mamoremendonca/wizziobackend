import React from "react";
import Journey from "./journey";
import { Provider } from "react-redux";
import { name as packageName, parent, contentVersion } from "./../package.json";
import { createStoreSingleton, FinalJourney } from "omni-journey";

/**
 * REDUCERS TO IMPORT
 */
import { refDataReducer } from "./reducers/refDataReducers";
import { financialDocumentReducer } from "./reducers/financialDocumentReducers";

let reducersToMerge = { financialDocumentReducer, refDataReducer };

let Store = createStoreSingleton(reducersToMerge);

export default class App extends React.Component {
  render = () => {
    return (
      <Provider store={Store}>
        <RenderJourney handleReceiveMessage={this.handleReceiveMessage} />
      </Provider>
    );
  };
}

let RenderJourney = FinalJourney(
  Journey,
  Store,
  packageName,
  parent,
  contentVersion
);
