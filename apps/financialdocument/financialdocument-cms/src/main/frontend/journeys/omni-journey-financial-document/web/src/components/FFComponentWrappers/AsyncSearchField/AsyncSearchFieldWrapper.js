import * as React from "react";
import AsyncSearchField from "./AsyncSearchField";
import PropTypes from "prop-types";

const AsyncSearchFieldWrapper = ({
  input: { name, onChange, value },
  meta,
  ...rest
}) => {
  return (
    <AsyncSearchField {...rest} name={name} onChange={onChange} value={value} />
  );
};

AsyncSearchFieldWrapper.propTypes = {
  input: PropTypes.object,
  meta: PropTypes.object,
};

export default AsyncSearchFieldWrapper;
