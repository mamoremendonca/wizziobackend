import React from "react";
import Typography from "@ui-lib/core/Typography";
import { styles, ComponentName } from "./StatusToolTipStyles";
import { withStyles } from "@ui-lib/core/styles";
import { FINANCIAL_DOCUMENT_STATUS } from "../../../constants";
import PropTypes from "prop-types";

const stallColor = (stall) => {
  switch (stall) {
    case FINANCIAL_DOCUMENT_STATUS.IN_APPROVAL:
      return "#E79208";
    case FINANCIAL_DOCUMENT_STATUS.APPROVED:
      return "#81BC00";
    case FINANCIAL_DOCUMENT_STATUS.SAP_SUBMITTED:
      return "#6F1D46";
    case FINANCIAL_DOCUMENT_STATUS.REJECTED:
      return "#26AEE5";
    case FINANCIAL_DOCUMENT_STATUS.CANCELED:
      return "#002B49";
    case FINANCIAL_DOCUMENT_STATUS.PAID:
      return "#000000";
    default:
  }
};

const StatusTooltip = ({ i18nProvider, classes }) => {
  return (
    <div key="status_description" className={classes.container}>
      <Typography variant="body2" className={classes.title}>
        {i18nProvider.Texti18n(
          "listFD.column.status.header.tool_tip.status_description"
        )}
      </Typography>
      <div key={FINANCIAL_DOCUMENT_STATUS.IN_APPROVAL} className={classes.item}>
        <div
          style={{
            backgroundColor: stallColor(FINANCIAL_DOCUMENT_STATUS.IN_APPROVAL),
          }}
          className={classes.statusIndicator}
        />
        <Typography variant="caption">
          {i18nProvider.Texti18n(
            "listFD.column.status.header.tool_tip.status_in_approval"
          )}
        </Typography>
      </div>
      <div key={FINANCIAL_DOCUMENT_STATUS.APPROVED} className={classes.item}>
        <div
          style={{
            backgroundColor: stallColor(FINANCIAL_DOCUMENT_STATUS.APPROVED),
          }}
          className={classes.statusIndicator}
        />
        <Typography variant="caption">
          {i18nProvider.Texti18n(
            "listFD.column.status.header.tool_tip.status_approved"
          )}
        </Typography>
      </div>
      <div
        key={FINANCIAL_DOCUMENT_STATUS.SAP_SUBMITTED}
        className={classes.item}
      >
        <div
          style={{
            backgroundColor: stallColor(
              FINANCIAL_DOCUMENT_STATUS.SAP_SUBMITTED
            ),
          }}
          className={classes.statusIndicator}
        />
        <Typography variant="caption">
          {i18nProvider.Texti18n(
            "listFD.column.status.header.tool_tip.status_sap_submitted"
          )}
        </Typography>
      </div>
      <div key={FINANCIAL_DOCUMENT_STATUS.REJECTED} className={classes.item}>
        <div
          style={{
            backgroundColor: stallColor(FINANCIAL_DOCUMENT_STATUS.REJECTED),
          }}
          className={classes.statusIndicator}
        />
        <Typography variant="caption">
          {i18nProvider.Texti18n(
            "listFD.column.status.header.tool_tip.status_rejected"
          )}
        </Typography>
      </div>
      <div key={FINANCIAL_DOCUMENT_STATUS.CANCELED} className={classes.item}>
        <div
          style={{
            backgroundColor: stallColor(FINANCIAL_DOCUMENT_STATUS.CANCELED),
          }}
          className={classes.statusIndicator}
        />
        <Typography variant="caption">
          {i18nProvider.Texti18n(
            "listFD.column.status.header.tool_tip.status_canceled"
          )}
        </Typography>
      </div>
      <div key={FINANCIAL_DOCUMENT_STATUS.PAID} className={classes.item}>
        <div
          style={{
            backgroundColor: stallColor(FINANCIAL_DOCUMENT_STATUS.PAID),
          }}
          className={classes.statusIndicator}
        />
        <Typography variant="caption">
          {i18nProvider.Texti18n(
            "listFD.column.status.header.tool_tip.status_paid"
          )}
        </Typography>
      </div>
    </div>
  );
};

StatusTooltip.propTypes = {
  i18nProvider: PropTypes.object,
  classes: PropTypes.object,
};

export default withStyles(styles, { name: ComponentName })(StatusTooltip);
