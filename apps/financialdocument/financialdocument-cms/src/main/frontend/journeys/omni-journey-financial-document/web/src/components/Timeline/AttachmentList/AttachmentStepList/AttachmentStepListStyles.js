export const componentName = "AttachmentStepListStyles";
export const styles = (theme) => ({
  root: {
    padding: theme.spacing.unit * 2,
  },
  header: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  divider: {
    width: theme.spacing.unit / 2,
    height: theme.spacing.unit / 2,
    backgroundColor: theme.palette.grey[500],
    borderRadius: "50%",
    margin: `0px ${theme.spacing.unit * 3}px`,
  },
});
