import * as React from "react";

import TextField from "@ui-lib/core/TextField";

import { withStyles } from "@ui-lib/core/styles";
import styles from "./styles";
import PropTypes from "prop-types";

const BasicDateFieldWrapper = ({
  input: { name, onChange, value, ...restInput },
  meta,
  disabled,
  classes,
  required,
  min,
  max,
  ...rest
}) => {
  const showError =
    ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) &&
    meta.touched;
  return (
    <React.Fragment>
      <TextField
        {...rest}
        name={name}
        helperText={showError ? meta.error || meta.submitError : undefined}
        error={showError}
        inputProps={{ ...restInput, min: min, max: max }}
        onChange={onChange}
        type={"date"}
        required={disabled ? false : required}
        disabled={disabled}
        style={{ paddingTop: 5 }}
        value={value ? value : ""}
        className={disabled && classes.textFieldDisabled}
      />
    </React.Fragment>
  );
};

BasicDateFieldWrapper.propTypes = {
  input: PropTypes.object,
  meta: PropTypes.object,
  classes: PropTypes.object,
  min: PropTypes.string,
  max: PropTypes.string,
  disabled: PropTypes.bool,
  required: PropTypes.bool,
};

export default withStyles(styles)(BasicDateFieldWrapper);
