import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
// Components
import { withStyles } from "@ui-lib/core/styles";
import { ComponentName, styles } from "./CreateFDScreenStyles";
import { Form } from "react-final-form";
import withContainerWrapper from "../../components/ContainerWrapper";
import Grid from "@ui-lib/core/Grid";
import IconButton from "@ui-lib/core/IconButton";
import Icon from "@ui-lib/core/Icon";
import Typography from "@ui-lib/core/Typography";
import Paper from "@ui-lib/core/Paper";
import PDFViewerRender from "./PDFViewer/PDFViewerRender";
import FooterRender from "./Footer/FooterRender";
import {
  createFinancialDocument,
  getFinancialDocumentData,
  clearFinancialDocument,
  addFinancialDocumentItem,
  editFinancialDocumentItem,
  removeFinancialDocumentItem,
  associatePurchaseOrderRequest,
  associateFinancialDocumentRequest,
  updateProcessHeaderDraftRequest,
  updateProcessHeaderCreationRequest,
} from "../../actions";
import {
  mapFinancialDocumentDataToCreateDTO,
  financialDocumentHeaderDraftBodyFrom,
  financialDocumentHeaderCreationBodyFrom,
  mapBackendItemToFormValues,
} from "../../utils";
import {
  FINANCIAL_DOCUMENT_PROCESS_STATUS,
  UPLOAD_INVOICE_BUTTON_ID,
  PURCHASE_ORDER_FRIENDLY_NUMBER,
  RESET_VALUE,
} from "../../constants";
import * as fdConstants from "../../constants/FDConstants";
import {
  uploadInvoiceDocumentRequest,
  downloadInvoiceDocumentRequest,
  deleteFinancialDocumentRequest,
  submitToCreationRequest,
  addAttachmentRequest,
  getTimelineRequest,
  removeAttachmentRequest,
  submitToApprovalRequest,
  downloadAttachmentRequest,
} from "../../actions/services/financialDocumentServiceActions";
import ConfirmationDialog from "../../components/Dialog/ConfirmationDialog";
import * as dialogConstants from "../../constants/dialogConstants";
import {
  isTargetAllowedToUpdate,
  submitToCreationPreValidations,
  submitToApprovalPreValidations,
} from "./validator";
import FormDetails from "./FormDetails";
import Separator from "../../components/Separator";
import FileInput from "../../components/Shared/FileInput";
import FileChip from "../../components/Shared/FileChip";
import FDTableItems from "../../components/FDTableItems";
import ItemForm from "../../components/ItemForm";
import ItemDialog from "../../components/Dialog/ItemDialog";
import { ChangesEngine } from "./react-final-form-helpers";
import { handleSubmitServerErrorResponse } from "./server-error-handler";
import { calculateTotalItemsValue } from "../../utils/infoFDUtils";
import PropTypes from "prop-types";

const compareGrantingDates = (item, I18nProvider) => {
  const startDate = new Date(item.startDate);
  const endDate = new Date(item.endDate);
  const startDateUTC = Date.UTC(
    startDate.getFullYear(),
    startDate.getMonth(),
    startDate.getDate()
  );
  const endDateUTC = Date.UTC(
    endDate.getFullYear(),
    endDate.getMonth(),
    endDate.getDate()
  );
  return Math.ceil((endDateUTC - startDateUTC) / (1000 * 60 * 60 * 24));
};

class CreateFDScreen extends Component {
  state = {
    isAllDependenciesResolved: false,
    isFromWorkList: false,
    formInitialState: undefined,
    isDeleteConfirmationDialogOpen: false,
    fileDialogOpen: false,
    files: [],
    isLoadingAttachment: { index: undefined, isLoading: false },
    pdfURL: undefined,
    selectedItem: undefined,
    itemDialogTitle: "",
    itemDialogAction: "",
    showModalWarningChanges: false,
    hasDepartmentChanged: false,
    values: {},
  };

  componentDidMount() {
    const {
      match,
      HttpClient,
      Notifications,
      createFinancialDocument,
      getFinancialDocumentData,
      Loading,
      I18nProvider,
    } = this.props;
    Loading.request("setLoading", "JOURNEY").then(() => {
      if (match && match.params && match.params.id) {
        this.setState({ isFromWorkList: true }, () =>
          getFinancialDocumentData(
            HttpClient,
            Notifications,
            I18nProvider.Texti18n(
              "services.get.financial_document.error.message"
            ),
            match.params.id
          )
        );
      } else {
        //it came from create button click
        createFinancialDocument(
          HttpClient,
          Notifications,
          I18nProvider.Texti18n(
            "services.create.financial_document.error.message"
          )
        );
      }
    });
  }

  handleLoadInvoiceDocument = async () => {
    const { Loading, financialDocumentData, refData } = this.props;
    try {
      let pdfURL = undefined;
      if (financialDocumentData.financialDocumentHeader.documentId) {
        pdfURL = await this.handleDownloadInvoiceDocumentRequest();
      }

      if (
        financialDocumentData.jwcontext.status ===
          FINANCIAL_DOCUMENT_PROCESS_STATUS.IN_CREATION &&
        this.state.isFromWorkList
      ) {
        await this.handleGetAttachmentsRequest();
      }

      this.setState({
        pdfURL,
        isAllDependenciesResolved: true,
        formInitialState: mapFinancialDocumentDataToCreateDTO(
          refData,
          financialDocumentData
        ),
      });
      Loading.request("stopLoading");
    } catch {
      Loading.request("stopLoading");
    }
  };

  componentDidUpdate(prevProps) {
    const { financialDocumentData, Loading, refData } = this.props;
    const { isAllDependenciesResolved, isFromWorkList } = this.state;

    if (
      financialDocumentData &&
      prevProps.financialDocumentData !== financialDocumentData &&
      !isAllDependenciesResolved
    ) {
      if (isFromWorkList) {
        this.handleLoadInvoiceDocument();
      } else {
        Loading.request("stopLoading").then(() => {
          //all dependencies are satisfied and now the component will render data.
          this.setState({
            isAllDependenciesResolved: true,
            formInitialState: mapFinancialDocumentDataToCreateDTO(
              refData,
              financialDocumentData
            ),
          });
        });
      }
    } else if (
      financialDocumentData &&
      prevProps.financialDocumentData !== financialDocumentData &&
      isAllDependenciesResolved
    ) {
      let financialData = this.getFinancialData(
        financialDocumentData,
        prevProps.financialDocumentData.financialDocumentHeader
      );

      this.setState({
        formInitialState: mapFinancialDocumentDataToCreateDTO(
          refData,
          financialData
        ),
      });
    }

    /* const height = document.body.scrollHeight 
    alert(height) */
  }

  saveFormData = async (formValues) => {
    const {
      updateProcessHeaderDraftRequest,
      HttpClient,
      Notifications,
      financialDocumentData,
      I18nProvider,
      updateProcessHeaderCreationRequest,
    } = this.props;

    if (
      financialDocumentData.jwcontext.status ===
      FINANCIAL_DOCUMENT_PROCESS_STATUS.DRAFT
    ) {
      await updateProcessHeaderDraftRequest(
        HttpClient,
        financialDocumentData.jwcontext.id,
        financialDocumentHeaderDraftBodyFrom(formValues),
        Notifications,
        I18nProvider.Texti18n("update.process_header_draft.error.message")
      );
    } else if (
      financialDocumentData.jwcontext.status ===
      FINANCIAL_DOCUMENT_PROCESS_STATUS.IN_CREATION
    ) {
      await updateProcessHeaderCreationRequest(
        HttpClient,
        financialDocumentData.jwcontext.id,
        financialDocumentHeaderCreationBodyFrom(formValues),
        Notifications,
        I18nProvider.Texti18n("update.process_header_draft.error.message")
      );
    }
  };

  handleOnBlur = (formValues, ev) => {
    const { showModalWarningChanges, hasDepartmentChanged } = this.state;
    if (
      (!ev.relatedTarget || isTargetAllowedToUpdate(ev.relatedTarget.id)) &&
      !showModalWarningChanges &&
      !hasDepartmentChanged
    ) {
      //Hack when the onblur event is fired and the user clicked on a button
      this.saveFormData(formValues);
    }
  };

  handleOnSaveDataBeforeSubmit = async (formValues) => {
    try {
      await this.saveFormData(formValues); //to guarantee that all data is saved before call the submit.
    } catch (error) {
      throw Object.assign(new Error(error.message), { errorHandled: true }); // to avoid eslint error.
    }
  };

  handleOnSubmit = async (formValues) => {
    //TODO: refactor: function with repetition code
    const {
      financialDocumentData,
      Loading,
      HttpClient,
      Notifications,
      I18nProvider,
    } = this.props;
    try {
      await Loading.request("setLoading", "JOURNEY");
      await this.handleOnSaveDataBeforeSubmit(formValues); //to guarantee that all data is saved before call the submit.
      if (
        financialDocumentData.jwcontext.status ===
        FINANCIAL_DOCUMENT_PROCESS_STATUS.DRAFT
      ) {
        const invalidFields = submitToCreationPreValidations(
          formValues,
          I18nProvider
        );
        if (!Object.entries(invalidFields).length) {
          // without invalid fields we can proceed
          await submitToCreationRequest(
            HttpClient,
            financialDocumentData.jwcontext.id
          );
          Notifications.notify(
            I18nProvider.Texti18n(
              "create_df_form_submit_to_creation.success.message"
            ),
            "success",
            "Process",
            5000
          );
          this.handleBackClick();
        } else {
          // show invalid fiels
          return invalidFields;
        }
      } else if (
        financialDocumentData.jwcontext.status ===
        FINANCIAL_DOCUMENT_PROCESS_STATUS.IN_CREATION
      ) {
        const invalidFields = submitToApprovalPreValidations(
          formValues,
          I18nProvider
        );

        if (!Object.entries(invalidFields).length) {
          await submitToApprovalRequest(
            HttpClient,
            financialDocumentData.jwcontext.id
          );
          Notifications.notify(
            I18nProvider.Texti18n(
              "createFDScreen.submit_to_approval.success.message"
            ),
            "success",
            "Process",
            5000
          );
          this.handleBackClick();
        } else {
          // show invalid fiels
          return invalidFields;
        }
      }
    } catch (error) {
      if (error.response) {
        return handleSubmitServerErrorResponse(
          error.response,
          Notifications,
          I18nProvider
        );
      } else if (!error.errorHandled) {
        Notifications.notify(
          I18nProvider.Texti18n("createFDScreen.submit.generic.error.message"),
          "error",
          "Process",
          5000
        );
      }
    } finally {
      Loading.request("stopLoading");
    }
  };

  handleUploadInvoiceDocumentRequest = async (file) => {
    const {
      Notifications,
      I18nProvider,
      HttpClient,
      financialDocumentData,
    } = this.props;
    try {
      await uploadInvoiceDocumentRequest(
        HttpClient,
        financialDocumentData.jwcontext.id,
        file
      );
    } catch (error) {
      Notifications.notify(
        I18nProvider.Texti18n(
          "create_df_form_upload_invoice_failed.error.message"
        ),
        "error",
        "Process",
        5000
      );
      throw error;
    }
  };

  handleGetAttachmentsRequest = async () => {
    const {
      Notifications,
      I18nProvider,
      HttpClient,
      financialDocumentData,
    } = this.props;
    try {
      const timelineResponse = await getTimelineRequest(
        HttpClient,
        financialDocumentData.jwcontext.id
      );

      this.setState({
        files: timelineResponse.data.flatMap((steps) =>
          steps.attachments.map((attachment) => ({
            content: attachment,
            index: attachment.index,
          }))
        ),
      });
    } catch (error) {
      Notifications.notify(
        I18nProvider.Texti18n(
          "create_df_form_get_attachments_from_timeline_failed.error.message"
        ),
        "error",
        "Process",
        5000
      );
      throw error;
    }
  };

  handleDownloadInvoiceDocumentRequest = async () => {
    const {
      Notifications,
      I18nProvider,
      HttpClient,
      financialDocumentData,
    } = this.props;
    try {
      const invoiceDocumentResponse = await downloadInvoiceDocumentRequest(
        HttpClient,
        financialDocumentData.jwcontext.id
      );
      return window.URL.createObjectURL(invoiceDocumentResponse.data);
    } catch (error) {
      Notifications.notify(
        I18nProvider.Texti18n(
          "create_df_form_download_invoice_failed.error.message"
        ),
        "error",
        "Process",
        5000
      );
      throw error;
    }
  };

  handleSyncUploadInvoice = async (file) => {
    const { Loading } = this.props;
    try {
      await Loading.request("setLoading", "JOURNEY");
      await this.handleUploadInvoiceDocumentRequest(file);
      const fileURL = await this.handleDownloadInvoiceDocumentRequest();
      this.setState({ pdfURL: fileURL }, () => Loading.request("stopLoading"));
    } catch {
      Loading.request("stopLoading");
    }
  };

  uploadInvoiceDocument = (files) => {
    if (files.length) {
      const { Notifications, I18nProvider } = this.props;
      if (files[0].type === "application/pdf") {
        this.handleSyncUploadInvoice(files[0]);
      } else {
        Notifications.notify(
          I18nProvider.Texti18n(
            "create_df_form_invalid_file_type.warning.message"
          ),
          "warning",
          "Process",
          5000
        );
      }
    }
  };

  handleBackClick = () => {
    const {
      history,
      JourneyActions,
      JourneySettings,
      clearFinancialDocument,
    } = this.props;
    //const { isFromWorkList, isFromCopy } = this.state;
    const { isFromWorkList } = this.state;
    if (isFromWorkList) {
      JourneyActions.closeJourney(JourneySettings.instance_id);
      // } else if(isFromCopy){
      //   history.push("/");
    } else {
      clearFinancialDocument();
      history.goBack();
    }
  };

  handleDeleteConfirmDialogClose = async (decision) => {
    if (decision) {
      const {
        Loading,
        HttpClient,
        financialDocumentData,
        Notifications,
        I18nProvider,
      } = this.props;
      try {
        await Loading.request("setLoading", "JOURNEY");
        await deleteFinancialDocumentRequest(
          HttpClient,
          financialDocumentData.jwcontext.id
        );
        this.setState({ isDeleteConfirmationDialogOpen: false });
        Loading.request("stopLoading");
        this.handleBackClick();
      } catch {
        Notifications.notify(
          I18nProvider.Texti18n("create_df_form_delete_failed.error.message"),
          "error",
          "Process",
          5000
        );
        Loading.request("stopLoading");
      }
    } else {
      this.setState({ isDeleteConfirmationDialogOpen: false });
    }
  };

  handleCloseFileDialog = (ev, documentClass) => {
    if (ev && documentClass) {
      const filesArray = [...this.state.files];
      const fileToSubmit = this.state.fileDialogItems;
      fileToSubmit.canBeDeletedByCurrentUser = true; //to be equal to get attachments body data.
      //ADD NEW FILE TO ARRAY AND SET IT LOADING
      filesArray.push({
        content: fileToSubmit,
        index: undefined,
        documentClass: documentClass,
      });
      this.setState(
        {
          fileDialogOpen: false,
          fileDialogItems: undefined,
          files: filesArray,
          isLoadingAttachment: {
            index: filesArray.length - 1,
            isLoadingAttachment: true,
          },
        },
        () => this.addPurchaseOrderAttachment(fileToSubmit, documentClass)
      );
    } else if (!ev) {
      this.setState({ fileDialogOpen: false, fileDialogItems: undefined });
    }
  };

  //TODO: Refactor Solution made only for demo.
  handleUploadAttachment = async (files) => {
    if (files[0]) {
      const {
        financialDocumentData: {
          jwcontext: { id },
        },
        HttpClient,
        Notifications,
        I18nProvider,
      } = this.props;
      const filesArray = [...this.state.files];
      const fileToSubmit = files[0];
      fileToSubmit.canBeDeletedByCurrentUser = true; //to be equal to get attachments body data.
      filesArray.push({ content: fileToSubmit, index: undefined });
      this.setState(
        {
          files: filesArray,
          isLoadingAttachment: {
            index: filesArray.length - 1,
            isLoadingAttachment: true,
          },
        },
        async () => {
          try {
            const response = await addAttachmentRequest(
              HttpClient,
              id,
              fileToSubmit
            );
            filesArray[filesArray.length - 1].index = response.data.index;
            this.setState({
              files: filesArray,
              isLoadingAttachment: {
                index: undefined,
                isLoadingAttachment: false,
              },
            });
          } catch (error) {
            filesArray.pop();
            this.setState({
              files: filesArray,
              isLoadingAttachment: {
                index: undefined,
                isLoadingAttachment: false,
              },
            });
            Notifications.notify(
              I18nProvider.Texti18n(
                "create_df_form_upload_attachment.failed.error.message"
              ),
              "error",
              "Process",
              5000
            );
          }
        }
      );
    }
  };

  downloadAttachment = (index) => {
    const {
      HttpClient,
      Notifications,
      I18nProvider,
      FileSystem,
      Loading,
      financialDocumentData: {
        jwcontext: { id },
      },
    } = this.props;
    downloadAttachmentRequest(
      HttpClient,
      Notifications,
      FileSystem,
      Loading,
      id,
      index,
      I18nProvider.Texti18n("services.download_attachment.error.message")
    );
  };

  removePurchaseOrderAttachment = async (index) => {
    const {
      HttpClient,
      Notifications,
      I18nProvider,
      financialDocumentData: {
        jwcontext: { id },
      },
    } = this.props;
    const filesArray = [...this.state.files].filter(
      (file) => file.index !== index
    );
    try {
      await removeAttachmentRequest(HttpClient, index, id);
      this.setState({ files: filesArray });
    } catch {
      Notifications.notify(
        I18nProvider.Texti18n(
          "create_df_form_delete_attachment.failed.error.message"
        ),
        "error",
        "Process",
        5000
      );
    }
  };

  isToLockScanningTeamFormFields = () => {
    const { pdfURL } = this.state;
    return !pdfURL;
  };

  isSubmitButtonDisabled = (hasFormValidationErrors) => {
    const { financialDocumentData } = this.props;
    if (financialDocumentData) {
      if (
        financialDocumentData.jwcontext.status ===
        FINANCIAL_DOCUMENT_PROCESS_STATUS.DRAFT
      ) {
        return this.isToLockScanningTeamFormFields() || hasFormValidationErrors;
      } else if (
        financialDocumentData.jwcontext.status ===
        FINANCIAL_DOCUMENT_PROCESS_STATUS.IN_CREATION
      ) {
        return (
          this.isToLockScanningTeamFormFields() ||
          hasFormValidationErrors ||
          !this.existItems()
        );
      }
    }
    return true;
  };

  editItemHandler = (index, itemsList) => {
    const {
      services: { I18nProvider },
      refData,
    } = this.props;

    let preSelectedItem = itemsList.find((item) => item.index === index);

    const selectedItem = mapBackendItemToFormValues(preSelectedItem, refData);

    this.setState({
      isItemDialogOpen: true,
      itemDialogTitle: I18nProvider.Labeli18n(
        "create_fd.dialog.edit_item.title"
      ),
      itemDialogAction: dialogConstants.ITEM_DIALOG_ACTIONS.EDIT_ITEM,
      selectedItem,
    });
  };

  copyItemHandler = async (index, itemsList) => {
    const {
      Loading,
      services,
      financialDocumentData,
      addFinancialDocumentItem,
      Notifications,
      I18nProvider,
      refData,
    } = this.props;

    let preSelectedItem = itemsList.find((item) => item.index === index);

    const selectedItem = mapBackendItemToFormValues(preSelectedItem, refData);

    try {
      await Loading.request("setLoading", "JOURNEY");
      await addFinancialDocumentItem(
        services.HttpClient,
        Notifications,
        I18nProvider,
        financialDocumentData.jwcontext.id,
        selectedItem
      );
    } catch {
    } finally {
      Loading.request("stopLoading");
    }
  };

  closeItemDialog = (form) => {
    form.reset();
    this.setState({
      isItemDialogOpen: false,
      selectedItem: {},
      selectedItemIndex: undefined,
    });
  };

  openItemDialog = () => {
    const {
      services: { I18nProvider },
      refData,
    } = this.props;

    this.setState({
      isItemDialogOpen: true,
      selectedItem: mapBackendItemToFormValues({}, refData),
      selectedItemIndex: undefined,
      itemDialogTitle: I18nProvider.Labeli18n(
        "create_fd.dialog.add_item.title"
      ),
      itemDialogAction: dialogConstants.ITEM_DIALOG_ACTIONS.ADD_ITEM,
    });
  };

  emulateSaveItemClick = (form) => {
    document
      .getElementById("item-form")
      .dispatchEvent(new Event("submit", { cancelable: true }));
  };

  saveItem = async (item, form) => {
    const {
      Loading,
      services,
      financialDocumentData,
      addFinancialDocumentItem,
      editFinancialDocumentItem,
      Notifications,
      I18nProvider,
    } = this.props;

    let invalidFields = {};

    if (item.startDate) {
      if (compareGrantingDates(item, I18nProvider) < 0) {
        const invalidFields = {};
        invalidFields["startDate"] = I18nProvider.Texti18n(
          "validator.start_date_higher_than_end_date.error.message"
        );
        return invalidFields;
      }
    }

    if (!Object.entries(invalidFields).length) {
      try {
        await Loading.request("setLoading", "JOURNEY");
        if (this.state.selectedItem.index === undefined) {
          await addFinancialDocumentItem(
            services.HttpClient,
            Notifications,
            I18nProvider,
            financialDocumentData.jwcontext.id,
            item
          );
        } else {
          await editFinancialDocumentItem(
            services.HttpClient,
            Notifications,
            I18nProvider,
            financialDocumentData.jwcontext.id,
            item
          );
        }
        this.setState({
          isItemDialogOpen: false,
        });
      } catch {
      } finally {
        Loading.request("stopLoading");
      }
    } else {
      return invalidFields;
    }
  };

  handleDeleteItemConfirmation = () => {
    this.setState({ isItemDeleteDialogOpen: true });
  };

  deleteItemHandler = (shallDelete) => {
    const {
      services,
      financialDocumentData,
      removeFinancialDocumentItem,
      Notifications,
      I18nProvider,
    } = this.props;
    if (shallDelete) {
      removeFinancialDocumentItem(
        services.HttpClient,
        Notifications,
        I18nProvider,
        financialDocumentData.jwcontext.id,
        this.state.selectedItem.index
      );
      this.setState({
        isItemDialogOpen: false,
        selectedItem: {},
        selectedItemIndex: undefined,
        isItemDeleteDialogOpen: false,
      });
    } else {
      this.setState({
        isItemDeleteDialogOpen: false,
      });
    }
  };

  showAccountsPayableTeam = () => {
    const { financialDocumentData } = this.props;
    return (
      financialDocumentData &&
      [FINANCIAL_DOCUMENT_PROCESS_STATUS.IN_CREATION].some(
        (status) => financialDocumentData.jwcontext.status === status
      )
    );
  };

  existItems = () => {
    const { financialDocumentData } = this.props;
    return !!(
      financialDocumentData &&
      financialDocumentData.items &&
      financialDocumentData.items.length
    );
  };

  showUploadInvoiceDocument = () => {
    const { financialDocumentData } = this.props;
    return (
      financialDocumentData &&
      financialDocumentData.jwcontext &&
      financialDocumentData.jwcontext.status ===
        FINANCIAL_DOCUMENT_PROCESS_STATUS.DRAFT
    );
  };

  handleClearPurchaseOrderAssociation = async (form) => {
    const {
      Loading,
      associatePurchaseOrderRequest,
      HttpClient,
      financialDocumentData: {
        jwcontext: { id },
      },
      Notifications,
      I18nProvider,
    } = this.props;
    try {
      await Loading.request("setLoading", "JOURNEY");
      await associatePurchaseOrderRequest(
        HttpClient,
        id,
        Notifications,
        I18nProvider.Texti18n(
          "createFDScreen.clear_purchase_order_unexpected_error_message"
        )
      );
    } catch {
      form.reset();
    } finally {
      Loading.request("stopLoading");
    }
  };

  handleClearFinancialDocumentAssociation = async (form) => {
    const {
      Loading,
      associateFinancialDocumentRequest,
      HttpClient,
      financialDocumentData: {
        jwcontext: { id },
      },
      Notifications,
      I18nProvider,
    } = this.props;
    try {
      await Loading.request("setLoading", "JOURNEY");
      await associateFinancialDocumentRequest(
        HttpClient,
        id,
        Notifications,
        I18nProvider.Texti18n(
          "createFDScreen.clear_financial_document_unexpected_error_message"
        )
      );
    } catch {
      form.reset();
    } finally {
      Loading.request("stopLoading");
    }
  };

  handleFormChanges = (currentValues, form) => {
    const { formInitialState, showModalWarningChanges } = this.state;
    const { financialDocumentData, refData } = this.props;

    let isToChange = true;
    if (!showModalWarningChanges) {
      for (const key in formInitialState) {
        if (currentValues.hasOwnProperty(key)) {
          if (
            key === fdConstants.PURCHASE_ORDER_FRIENDLY_NUMBER ||
            key === fdConstants.ASSOCIATED_DOCUMENT_FRIENDLY_NUMBER
          ) {
            if (key === fdConstants.PURCHASE_ORDER_FRIENDLY_NUMBER) {
              if (financialDocumentData && financialDocumentData.items.length) {
                if (
                  !currentValues[key] &&
                  formInitialState[key] !== currentValues[key]
                ) {
                  // notify user that items will be lost.
                  isToChange = false;
                  this.setState({ showModalWarningChanges: true });
                }
              } else if (
                !currentValues[key] &&
                formInitialState[key] !== currentValues[key]
              ) {
                //doesn't exist items so just clear.
                //call backend to clear PO association
                this.handleClearPurchaseOrderAssociation(form);
              }
            } else if (
              !currentValues[key] &&
              formInitialState[key] !== currentValues[key]
            ) {
              this.handleClearFinancialDocumentAssociation(form);
            }
          } else if (
            financialDocumentData &&
            financialDocumentData.items.length &&
            fdConstants.TABLE_ITEMS_CHANGE_FIELDS.some((field) => field === key)
          ) {
            // check if is different
            if (formInitialState[key] !== currentValues[key]) {
              isToChange = false;
              this.setState({ showModalWarningChanges: true });
              break;
            }
          }
        }
      }
    }

    this.handleFormDepartmentChange(
      formInitialState,
      currentValues,
      isToChange,
      financialDocumentData
    );
  };

  handleClearPurchaseOrder = async () => {
    const {
      financialDocumentData: {
        jwcontext: { id },
      },
      associatePurchaseOrderRequest,
      HttpClient,
      Notifications,
      I18nProvider,
    } = this.props;
    try {
      await associatePurchaseOrderRequest(
        HttpClient,
        id,
        Notifications,
        //Losing connection cases
        I18nProvider.Texti18n(
          "createFDScreen.clear_purchase_order_unexpected_error_message"
        )
      );
    } catch (error) {
      if (error.response && error.response.data) {
        //for cases when a bad request is sent.
        Notifications.notify(
          I18nProvider.Texti18n(
            "createFDScreen.clear_purchase_order_unexpected_error_message"
          ),
          "error",
          "Process",
          5000
        );
      }
      throw error;
    }
  };

  onCloseModalWarningChanges = async (decision, currentValues, form) => {
    const {
      Loading,
      financialDocumentData: { financialDocumentHeader },
    } = this.props;
    if (decision) {
      try {
        Loading.request("setLoading", "JOURNEY");
        //exception for po association
        if (
          financialDocumentHeader[fdConstants.PURCHASE_ORDER_FRIENDLY_NUMBER] &&
          !currentValues[fdConstants.PURCHASE_ORDER_FRIENDLY_NUMBER]
        ) {
          await this.handleClearPurchaseOrder();
        } else {
          await this.saveFormData(currentValues);
        }
      } catch {
        form.reset();
      } finally {
        Loading.request("stopLoading", "JOURNEY");
      }
    } else {
      form.reset();
    }
    this.setState({ showModalWarningChanges: false });
  };

  handleAssociatePurchaseOrderRequest = async (poNumber) => {
    const {
      financialDocumentData: {
        jwcontext: { id },
      },
      associatePurchaseOrderRequest,
      HttpClient,
      Notifications,
      I18nProvider,
    } = this.props;
    return new Promise(async (resolve, reject) => {
      try {
        await associatePurchaseOrderRequest(
          HttpClient,
          id,
          Notifications,
          I18nProvider.Texti18n(
            "createFDScreen.associate_purchase_order_unexpected_error_message"
          ),
          poNumber
        );
        resolve(poNumber);
      } catch {
        reject(
          `${poNumber} ${I18nProvider.Texti18n(
            "createFDScreen.associate_purchase_order_not_found_error_message"
          )}.`
        );
      }
    });
  };

  handleAssociateFinancialDocumentRequest = async (fdNumber) => {
    const {
      financialDocumentData: {
        jwcontext: { id },
      },
      associateFinancialDocumentRequest,
      HttpClient,
      Notifications,
      I18nProvider,
    } = this.props;
    return new Promise(async (resolve, reject) => {
      try {
        await associateFinancialDocumentRequest(
          HttpClient,
          id,
          Notifications,
          I18nProvider.Texti18n(
            "createFDScreen.set_financial_document_unexpected_error_message"
          ),
          fdNumber
        );
        resolve(fdNumber);
      } catch {
        reject(
          `${fdNumber} ${I18nProvider.Texti18n(
            "createFDScreen.associate_financial_document_not_found_error_message"
          )}.`
        );
      }
    });
  };

  // Specific departmentCode
  // For the specific onChange of departmentCode, we want it to change immediately, instead of onBlur.
  // Also, we have to make sure it does NOT change immediately if an Item is associated with the Financial Document.
  // Only when the user accepts to discard the changes.
  handleFormDepartmentChange = (init, curr, toChange, financialData) => {
    if (
      init[fdConstants.DEPARTMENT_CODE] !== curr[fdConstants.DEPARTMENT_CODE] &&
      toChange
    ) {
      if (!financialData.items.length) {
        this.setState({ hasDepartmentChanged: true });
        let newValues = {
          ...curr,
          firstWorkflowGroup: RESET_VALUE,
          internalOrderCode: RESET_VALUE,
          projectCode: RESET_VALUE,
        };
        this.saveFormData(newValues);
      }
    } else {
      this.setState({ hasDepartmentChanged: false });
    }
  };

  /* Executed on didUpdate, this method validates if the department code has been changed
     If it also has no PO associated, then he resets the values of fields associated with the department code
  */
  getFinancialData = (newFData, prevFData) => {
    let newFinancialData = newFData;
    if (
      prevFData[fdConstants.DEPARTMENT_CODE] !==
        newFData.financialDocumentHeader[fdConstants.DEPARTMENT_CODE] &&
      !newFData.financialDocumentHeader[PURCHASE_ORDER_FRIENDLY_NUMBER]
    ) {
      newFinancialData = this.handleCheckDepartmentChange(
        newFData,
        newFData.financialDocumentHeader[fdConstants.DEPARTMENT_CODE]
      );
      this.setState({ hasDepartmentChanged: true });
    } else {
      this.setState({ hasDepartmentChanged: false });
    }

    return newFinancialData;
  };

  /*  Resets the values of fields associated witht the department code
   */
  handleCheckDepartmentChange = (currentFD, departmentCode) => {
    let newFinancialData = currentFD;

    newFinancialData = {
      ...currentFD,
      financialDocumentHeader: {
        ...currentFD.financialDocumentHeader,
        departmentCode,
        projectCode: RESET_VALUE,
        internalOrderCode: RESET_VALUE,
        firstWorkflowGroup: RESET_VALUE,
      },
    };

    return newFinancialData;
  };

  render() {
    const {
      hostPort,
      contentUrl,
      I18nProvider,
      classes,
      refData,
      financialDocumentData,
    } = this.props;

    const {
      isAllDependenciesResolved,
      formInitialState,
      pdfURL,
      isDeleteConfirmationDialogOpen,
      showModalWarningChanges,
      hasDepartmentChanged,
    } = this.state;

    const supplierCodes = refData.suppliers.map((element) => ({
      ...element,
      value: element.id,
      label: element.code,
    }));
    const supplierNames = refData.suppliers.map((element) => ({
      ...element,
      value: element.id,
      label: element.name,
    }));
    const supplierTaxes = refData.suppliers.map((element) => ({
      ...element,
      value: element.id,
      label: element.tax,
    }));

    return (
      <>
        {isAllDependenciesResolved && (
          <Grid container>
            <Grid
              item
              xs={pdfURL ? 7 : 12}
              id={"grid-form-df"}
              className={classes.gridForm}
            >
              <Form
                initialValues={formInitialState}
                onSubmit={this.handleOnSubmit}
                render={({
                  handleSubmit,
                  hasValidationErrors,
                  values,
                  form,
                }) => (
                  <Paper elevation={0} square className={classes.rootPaper}>
                    <Grid className={classes.root}>
                      <Grid item xs={12}>
                        <div className={classes.fdHeaderData}>
                          <Typography variant="h4" color="textSecondary">
                            {I18nProvider.Labeli18n(
                              "createFDScreen.form.financial_document.header.title"
                            )}
                          </Typography>
                          {this.showUploadInvoiceDocument() && (
                            <label htmlFor="input-upload-pdf-file">
                              <IconButton
                                id={UPLOAD_INVOICE_BUTTON_ID}
                                htmlFor="icon-upload-pdf-file"
                                component="span"
                              >
                                <input
                                  accept=".pdf"
                                  className={classes.InputFile}
                                  type="file"
                                  id="input-upload-pdf-file"
                                  onChange={(e) =>
                                    this.uploadInvoiceDocument(e.target.files)
                                  }
                                />
                                <Icon>publish</Icon>
                              </IconButton>
                            </label>
                          )}
                        </div>
                      </Grid>
                      <Grid item xs={12}>
                        {/* Form Fields */}
                        <FormDetails
                          lockForm={this.isToLockScanningTeamFormFields()}
                          refData={refData}
                          supplierCodes={supplierCodes}
                          supplierNames={supplierNames}
                          supplierTaxes={supplierTaxes}
                          I18nProvider={I18nProvider}
                          handleOnBlur={this.handleOnBlur}
                          hasDepartmentChanged={hasDepartmentChanged}
                          formValues={values}
                          showAccountsPayableTeam={this.showAccountsPayableTeam()}
                          isFDCombinedWithPO={
                            !!values[PURCHASE_ORDER_FRIENDLY_NUMBER]
                          }
                          associatePurchaseOrder={
                            this.handleAssociatePurchaseOrderRequest
                          }
                          associateFinancialDocument={
                            this.handleAssociateFinancialDocumentRequest
                          }
                        />
                      </Grid>
                      {/* TODO: i18n */}
                      {this.showAccountsPayableTeam() && (
                        <Grid item xs={12} className={classes.formSection}>
                          <Separator
                            text={I18nProvider.Labeli18n(
                              "createFDScreen.form.financial_document.header.items_section.title"
                            )}
                          >
                            <IconButton
                              id="addItem"
                              disabled={hasValidationErrors}
                              onClick={this.openItemDialog}
                            >
                              <Icon className="icon-add"></Icon>
                            </IconButton>
                          </Separator>
                          {this.existItems() && (
                            <>
                              <FDTableItems
                                I18nProvider={I18nProvider}
                                itemsList={financialDocumentData.items}
                                financialDocumentHeader={
                                  financialDocumentData.financialDocumentHeader
                                }
                                refData={refData}
                                handleEdit={(index) =>
                                  this.editItemHandler(
                                    index,
                                    financialDocumentData.items
                                  )
                                }
                                handleCopy={(index) =>
                                  this.copyItemHandler(
                                    index,
                                    financialDocumentData.items
                                  )
                                }
                              />
                              <Grid
                                container
                                className={classes.priceSubsection}
                              >
                                <Grid item xs={6}>
                                  <Typography className={classes.priceLabel}>
                                    {I18nProvider.Labeli18n(
                                      "create_fd.item.dialog.form.label.finalAmount"
                                    )}
                                  </Typography>
                                </Grid>
                                <Grid
                                  item
                                  xs={6}
                                  className={classes.priceValue}
                                  justify="flex-end"
                                >
                                  {calculateTotalItemsValue(
                                    refData,
                                    financialDocumentData
                                  )}
                                </Grid>
                              </Grid>
                              <ConfirmationDialog //Just to delete the financial document
                                open={showModalWarningChanges}
                                title={I18nProvider.Labeli18n(
                                  "createFDScreen.confirmation_dialog.accept_current_changes.title"
                                )}
                                onClose={(decision) =>
                                  this.onCloseModalWarningChanges(
                                    decision,
                                    values,
                                    form
                                  )
                                }
                              />
                            </>
                          )}
                        </Grid>
                      )}
                      {this.showAccountsPayableTeam() && (
                        <Grid
                          item
                          xs={12}
                          className={classes.formSectionAttachments}
                        >
                          <Separator
                            text={I18nProvider.Labeli18n(
                              "createFDScreen.form.financial_document.header.attachment_section.title"
                            )}
                          >
                            <FileInput
                              disabled={hasValidationErrors}
                              handleFileSelection={this.handleUploadAttachment}
                            />
                          </Separator>
                        </Grid>
                      )}
                      {this.showAccountsPayableTeam() && (
                        <Grid item xs={12} className={classes.formSection}>
                          {this.state.files.map((file, index) => (
                            <FileChip
                              I18nProvider={I18nProvider}
                              indexLoading={index}
                              isLoading={this.state.isLoadingAttachment}
                              name={file.content.name}
                              key={file.index}
                              documentClass={file.documentClass}
                              onDelete={() =>
                                this.removePurchaseOrderAttachment(file.index)
                              }
                              onDownload={() =>
                                this.downloadAttachment(file.index)
                              }
                              canDelete={file.content.canBeDeletedByCurrentUser}
                            />
                          ))}
                        </Grid>
                      )}
                      <Grid item xs={12}>
                        <FooterRender
                          isSubmitButtonDisabled={this.isSubmitButtonDisabled(
                            hasValidationErrors
                          )}
                          I18nProvider={I18nProvider}
                          handleOnDelete={() =>
                            this.setState({
                              isDeleteConfirmationDialogOpen: true,
                            })
                          }
                          handleOnSubmit={handleSubmit}
                        />
                        <ConfirmationDialog //Just to delete the financial document
                          open={isDeleteConfirmationDialogOpen}
                          title={I18nProvider.Labeli18n(
                            "createFDScreen.confirmation_dialog.delete.title"
                          )}
                          onClose={this.handleDeleteConfirmDialogClose}
                        />
                        <ChangesEngine
                          initialValues={this.state.formInitialState}
                          handleFormChanges={(currentValues) =>
                            this.handleFormChanges(currentValues, form)
                          }
                        />
                      </Grid>
                    </Grid>
                  </Paper>
                )}
              />
              {/* end of the create FD form*/}
              <Form
                onSubmit={this.saveItem}
                initialValues={this.state.selectedItem}
                render={({
                  handleSubmit,
                  form,
                  values,
                  errors,
                  pristine,
                  touched,
                  valid,
                  hasValidationErrors,
                  ...renderProps
                }) => (
                  <form
                    onSubmit={(ev, form) => {
                      handleSubmit(ev, form);
                    }}
                    id="item-form"
                  >
                    <ConfirmationDialog
                      open={this.state.isItemDeleteDialogOpen}
                      title={I18nProvider.Labeli18n(
                        "createFDScreen.confirmation_dialog.accept_delete_item.title"
                      )}
                      onClose={(decision) => this.deleteItemHandler(decision)}
                    />
                    <ItemDialog
                      open={this.state.isItemDialogOpen}
                      close={() => this.closeItemDialog(form)}
                      handleDelete={this.handleDeleteItemConfirmation}
                      save={() => this.emulateSaveItemClick(form)}
                      title={this.state.itemDialogTitle}
                      errors={errors}
                      valid={!hasValidationErrors}
                      action={this.state.itemDialogAction}
                      itemIndex={this.state.selectedItemIndex}
                      I18nProvider={I18nProvider}
                    >
                      <ItemForm
                        title={"Adicionar items"}
                        values={values || this.state.values}
                        errors={errors}
                        refData={refData}
                        I18nProvider={I18nProvider}
                        financialDocumentData={financialDocumentData}
                      ></ItemForm>
                    </ItemDialog>
                  </form>
                )}
              />
            </Grid>
            {pdfURL && (
              <Grid item xs={5}>
                <PDFViewerRender
                  hostPort={hostPort}
                  contentUrl={contentUrl}
                  pdfURL={this.state.pdfURL}
                />
              </Grid>
            )}
          </Grid>
        )}
      </>
    );
  }
}

CreateFDScreen.propTypes = {
  I18nProvider: PropTypes.object,
  classes: PropTypes.object,
  refData: PropTypes.object,
  services: PropTypes.object,
  Loading: PropTypes.object,
  hostPort: PropTypes.string,
  financialDocumentData: PropTypes.object,
  isError: PropTypes.bool,
  FileSystem: PropTypes.object,
  HttpClient: PropTypes.object,
  Notifications: PropTypes.object,
  match: PropTypes.object,
  history: PropTypes.object,
  JourneyActions: PropTypes.object,
  JourneySettings: PropTypes.object,
  contentUrl: PropTypes.string,
  createFinancialDocument: PropTypes.func,
  getFinancialDocumentData: PropTypes.func,
  updateProcessHeaderDraftRequest: PropTypes.func,
  clearFinancialDocument: PropTypes.func,
  updateProcessHeaderCreationRequest: PropTypes.func,
  addFinancialDocumentItem: PropTypes.func,
  editFinancialDocumentItem: PropTypes.func,
  removeFinancialDocumentItem: PropTypes.func,
  associatePurchaseOrderRequest: PropTypes.func,
  associateFinancialDocumentRequest: PropTypes.func,
};

const mapStateToProps = ({
  journey: {
    hostPort,
    contentUrl,
    services,
    services: { I18nProvider, Loading, Notifications, HttpClient, FileSystem },
  },
  refDataReducer: { refData },
  financialDocumentReducer: { financialDocumentData },
  isError,
}) => ({
  hostPort,
  contentUrl,
  services,
  I18nProvider,
  Loading,
  Notifications,
  HttpClient,
  refData,
  financialDocumentData,
  isError,
  FileSystem,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      createFinancialDocument,
      getFinancialDocumentData,
      updateProcessHeaderDraftRequest,
      clearFinancialDocument,
      updateProcessHeaderCreationRequest,
      addFinancialDocumentItem,
      editFinancialDocumentItem,
      removeFinancialDocumentItem,
      associatePurchaseOrderRequest,
      associateFinancialDocumentRequest,
    },
    dispatch
  );

export default withStyles(styles, { name: ComponentName })(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withContainerWrapper(CreateFDScreen))
);
