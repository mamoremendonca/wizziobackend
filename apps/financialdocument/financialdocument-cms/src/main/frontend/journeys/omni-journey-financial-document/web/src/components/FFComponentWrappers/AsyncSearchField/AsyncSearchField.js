import React, { Component } from "react";
import TextField from "@ui-lib/core/TextField";
import { withStyles } from "@ui-lib/core/styles";
import { ComponentName, styles } from "./AsyncSearchFieldStyles";

import InputAdornment from "@ui-lib/core/InputAdornment";
import Icon from "@ui-lib/core/Icon";
import CircularProgress from "@ui-lib/core/CircularProgress";
import PropTypes from "prop-types";

const InfoAction = ({ classes, currentAction, disabled }) => {
  if (disabled) {
    return null;
  }
  switch (currentAction) {
    case "loading":
      return <CircularProgress className={classes.loadingIcon} size="20px" />;
    case "found":
      return <Icon className="icon-close" fontSize="small" />;
    default:
      return <Icon className="icon-search" fontSize="small" />;
  }
};

InfoAction.propTypes = {
  classes: PropTypes.object,
  currentAction: PropTypes.string,
  disabled: PropTypes.bool,
};

class AsyncSearchField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTextField: props.value,
      currentAction: !!props.value ? "found" : "ready",
      errorMessage: undefined,
      mounted: false,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.value !== this.props.value) {
      this.setState({
        searchTextField: this.props.value,
        currentAction: !!this.props.value ? "found" : "ready",
        errorMessage: !!this.props.value ? undefined : this.state.errorMessage,
      });
    }
  }

  doSearch = () => {
    const { promiseFn, onChange } = this.props;
    const { currentAction, searchTextField } = this.state;
    if (promiseFn && currentAction === "ready") {
      this.setState({ currentAction: "loading" }, () => {
        promiseFn(searchTextField)
          .then((result) => {
            onChange(result);
          })
          .catch((error) => {
            this.setState({
              currentAction: "ready",
              errorMessage: error,
            });
          });
      });
    }
  };

  clearValue = () => {
    const { onChange } = this.props;
    onChange(null);
  };

  executeCurrentAction = () => {
    const { currentAction, searchTextField } = this.state;
    if (
      currentAction === "ready" &&
      searchTextField.length &&
      searchTextField.trim()
    ) {
      this.doSearch();
    } else if (currentAction === "found") {
      this.clearValue();
    }
  };

  render() {
    const {
      classes,
      name,
      disabled,
      label,
      InputLabelProps,
      fullWidth,
      placeholder,
    } = this.props;
    const { searchTextField, currentAction, errorMessage } = this.state;
    return (
      <TextField
        placeholder={placeholder}
        fullWidth={fullWidth}
        InputLabelProps={InputLabelProps}
        error={!!errorMessage}
        label={label}
        value={searchTextField}
        helperText={errorMessage}
        onChange={(e) =>
          this.setState({
            searchTextField: e.target.value,
            errorMessage: undefined,
          })
        }
        InputProps={{
          readOnly: currentAction !== "ready",
          endAdornment: (
            <InputAdornment key={`${name}-async-search-value`}>
              <span
                className={classes.buttonIcon}
                onClick={this.executeCurrentAction}
              >
                <InfoAction
                  classes={classes}
                  currentAction={currentAction}
                  disabled={disabled}
                />
              </span>
            </InputAdornment>
          ),
        }}
      />
    );
  }
}

AsyncSearchField.propTypes = {
  classes: PropTypes.object,
  value: PropTypes.string,
  promiseFn: PropTypes.func,
  onChange: PropTypes.func,
  name: PropTypes.string,
  disabled: PropTypes.bool,
  label: PropTypes.string,
  InputLabelProps: PropTypes.object,
  fullWidth: PropTypes.bool,
  placeholder: PropTypes.string,
};

export default withStyles(styles, { name: ComponentName })(AsyncSearchField);
