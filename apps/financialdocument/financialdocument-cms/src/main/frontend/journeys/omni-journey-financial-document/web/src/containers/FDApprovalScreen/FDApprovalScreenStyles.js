export const componentName = "FDApprovalScreenStyle";
export const styles = (theme) => ({
  root: {
    display: "flex",
    width: "100%",
    height: "100%",
  },
  approvalDetails: {
    display: "flex",
    flexDirection: "column",
    width: "75%",
    height: "100%",
    padding: "0 40px 40px",
    borderRight: `2px solid ${theme.palette.primary.main}`,
  },
  timeline: {
    width: "25%",
    height: "100%",
  },
  info: {
    order: 1,
    flexGrow: 8,
    width: "100%",
    //height: "54%",
  },
  tableItems: {
    order: 2,
    flexGrow: 2,
    width: "100%",
    //height: "22%",
  },
  footer: {
    order: 3,
    flexGrow: 2,
    width: "100%",
    //height: "24%",
  },
});
