export const ITEM = "item";
export const ITEM_TYPE = "itemType";
export const DESCRIPTION = "description";
export const QUANTITY = "quantity";
export const UNIT_VALUE = "unitValue";
export const TOTAL_AMOUNT_WO_VAT = "totalAmountWoVAT";
export const TOTAL_AMOUNT_W_VAT = "totalAmountWithVAT";
export const FINAL_AMOUNT = "finalAmount";
export const TAX = "tax";
export const COST_CENTER = "costCenter";
export const RETENTION = "retention";
export const RETENTION_VALUE = "retentionValue";
export const START_DATE = "startDate";
export const END_DATE = "endDate";
export const COMMENT = "comment";
export const FIXEDASSET = "fixedAsset";
export const INDEX = "index";
export const VERSION = "version";
export const DEPARTMENT = "department";
