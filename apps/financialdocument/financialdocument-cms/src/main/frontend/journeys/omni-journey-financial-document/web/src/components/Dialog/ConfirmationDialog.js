import React from "react";
import Dialog from "@ui-lib/core/Dialog";

import DialogTitle from "@ui-lib/core/DialogTitle";
import DialogContent from "@ui-lib/core/DialogContent";
import DialogActions from "@ui-lib/core/DialogActions";
import Button from "@ui-lib/core/Button";
import IconButton from "@ui-lib/core/IconButton";
import Icon from "@ui-lib/core/Icon";
import classNames from "@ui-lib/core/classnames";
import { withStyles } from "@ui-lib/core/styles";
import { styles, ComponentName } from "./DialogStyles";
import PropTypes from "prop-types";

const ConfirmationDialog = (props) => {
  const {
    open,
    onClose,
    children,
    title,
    disabled,
    disableBackdropClick,
    classes,
    showTitleCloseButton,
    cancelButtonLabel,
    acceptButtonLabel,
  } = props;
  return (
    <Dialog
      open={open}
      onClose={() => onClose(false)}
      fullWidth={true}
      maxWidth={"md"}
      disableBackdropClick={disableBackdropClick}
    >
      <DialogTitle>
        {title}
        {showTitleCloseButton && (
          <IconButton
            aria-label="cancel"
            className={classes.buttonCancel}
            onClick={() => onClose(false)}
          >
            <Icon className={classNames("icon-close")} />
          </IconButton>
        )}
      </DialogTitle>
      <DialogContent style={{ overflowY: "visible" }}>{children}</DialogContent>
      <DialogActions>
        <Button type="submit" onClick={() => onClose(false)}>
          {cancelButtonLabel || "Cancelar"}
        </Button>
        <Button
          type="submit"
          variant="contained"
          color="primary"
          disabled={disabled}
          onClick={() => onClose(true)}
        >
          {acceptButtonLabel || "Continuar"}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

ConfirmationDialog.propTypes = {
  children: PropTypes.array,
  title: PropTypes.string,
  classes: PropTypes.object,
  disableBackdropClick: PropTypes.func,
  open: PropTypes.func,
  onClose: PropTypes.func,
  showTitleCloseButton: PropTypes.string,
  cancelButtonLabel: PropTypes.string,
  acceptButtonLabel: PropTypes.string,
  disabled: PropTypes.bool,
};

export default withStyles(styles, { name: ComponentName })(ConfirmationDialog);
