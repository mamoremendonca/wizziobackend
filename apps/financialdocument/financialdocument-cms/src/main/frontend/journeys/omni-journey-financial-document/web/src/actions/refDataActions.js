import * as constants from "../constants";
/**
 * Used to load the common reference data that came from the service
 * @param {*} refData
 */
export const setRefData = (refData) => {
  return {
    type: constants.SET_REFDATA,
    payload: refData,
  };
};
