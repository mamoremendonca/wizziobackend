import { Config } from "../../config";

/**
 * Gets the attachment
 * @param {*} instanceId
 * @param {*} index
 * @param {*} services
 * @param {*} dataSource
 * @param {*} repositoryId
 */
export const downloadAttachmentRequest = async (
  HttpClient,
  Notifications,
  FileSystem,
  Loading,
  instanceId,
  index,
  errorMessage,
  dataSource = Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY
) => {
  const config = {
    method: Config.METHOD_GET,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${dataSource}${instanceId}${Config.ATTACHMENT_URL_PART}${index}`,
    responseType: "blob",
    headers: {
      accept: "*/*",
    },
  };
  try {
    await Loading.request("setLoading", "JOURNEY");
    const response = await HttpClient.request(config);
    FileSystem.downloadFile(
      response.data,
      response.headers["content-disposition"].replace(
        "attachment; filename=",
        ""
      )
    );
  } catch (error) {
    Notifications.notify(errorMessage, "error", "Process", 5000);
  } finally {
    Loading.request("stopLoading");
  }
};

/**
 * To delete all items added in the financial document
 * In case of success no response body is sent.
 * @param {int} instanceId
 * @param {object} services
 */
export const clearAllItemsRequest = async (instanceId, HttpClient) => {
  const config = {
    method: Config.METHOD_DELETE,
    url: `${Config.URL_PURCHASE_ORDER}${instanceId}${Config.ITEM_ALL_URL}`,
  };

  try {
    return await HttpClient.request(config);
  } catch (error) {
    return error;
  }
};

/**
 * To cancel the financial document.
 * @param {*} HttpClient
 * @param {*} id
 */
export const deleteFinancialDocumentRequest = async (
  HttpClient,
  instanceId
) => {
  const config = {
    method: Config.METHOD_PUT,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${instanceId}${Config.CANCEL_DF_URL_PART}`,
  };
  return HttpClient.request(config);
};
/**
 *
 * @param {*} HttpClient
 * @param {*} supplierCode
 */
export const getSupplierDetailsRequest = (HttpClient, supplierCode) => {
  const config = {
    method: Config.METHOD_GET,
    url: `${Config.SUPPLIER_URL}/${supplierCode}`,
  };
  return HttpClient.request(config);
};

export const uploadInvoiceDocumentRequest = (HttpClient, instanceId, file) => {
  const data = new FormData();
  data.append("document", file);
  const config = {
    method: Config.METHOD_POST,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${instanceId}${Config.INVOICE_DOCUMENT_URL_PART}`,
    data: data,
  };
  return HttpClient.request(config);
};

export const downloadInvoiceDocumentRequest = (HttpClient, instanceId) => {
  const config = {
    method: Config.METHOD_GET,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${instanceId}${Config.INVOICE_DOCUMENT_URL_PART}`,
    responseType: "blob",
    headers: {
      accept: "*/*",
    },
  };
  return HttpClient.request(config);
};

export const submitToCreationRequest = (HttpClient, instanceId) => {
  const config = {
    method: Config.METHOD_PUT,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${instanceId}${Config.SUBMIT_CREATION_URL_PART}`,
  };
  return HttpClient.request(config);
};

export const addAttachmentRequest = (
  HttpClient,
  instanceId,
  file,
  type = Config.DEFAULT_FINANCIAL_DOCUMENT_ATTACHMENT_TYPE
) => {
  const data = new FormData();
  data.append("attachment", file);
  const config = {
    method: Config.METHOD_POST,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${instanceId}${Config.ATTACHMENT_URL_PART}?type=${type}`,
    data: data,
  };
  return HttpClient.request(config);
};

/**
 * returns all the attachments added in the draft.
 * data[0] correspond to the first step of the process
 * @param {*} HttpClient
 * @param {*} instanceId
 */
export const getTimelineRequest = (
  HttpClient,
  instanceId,
  dataSource = Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY
) => {
  const config = {
    method: Config.METHOD_GET,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${dataSource}${instanceId}${Config.TIMELINE_URL}`,
  };
  return HttpClient.request(config);
};

export const removeAttachmentRequest = (HttpClient, index, instanceId) => {
  let config = {
    method: Config.METHOD_DELETE,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${instanceId}${Config.ATTACHMENT_URL_PART}${index}`,
  };
  return HttpClient.request(config);
};

export const submitToApprovalRequest = (HttpClient, instanceId) => {
  const config = {
    method: Config.METHOD_PUT,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${instanceId}${Config.SUBMIT_APPROVAL_URL_PART}`,
  };
  return HttpClient.request(config);
};

export const submitApprovalDecisionRequest = (
  HttpClient,
  instanceId,
  justification,
  decision
) => {
  const config = {
    method: Config.METHOD_PUT,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${instanceId}${decision}`,
    data: justification ? justification : " ",
    headers: { "Content-Type": "text/html; charset=utf-8" },
  };
  return HttpClient.request(config);
};

export const handleManageDfRequest = (HttpClient, instanceId) => {
  let config = {
    method: Config.METHOD_PUT,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${instanceId}${Config.CLAIM_DF_URL_PART}`,
  };

  return HttpClient.request(config);
};
