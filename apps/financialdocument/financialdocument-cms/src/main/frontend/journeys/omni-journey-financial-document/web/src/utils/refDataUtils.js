export const getRefDataLabelFromOption = (refDataFields, fieldValue) => {
  const field = refDataFields.find((field) => {
    if (field.value) {
      return field.value.toString() === fieldValue.toString();
    } else if (field.code) {
      return field.code.toString() === fieldValue.toString();
    }
    return undefined;
  });
  return field ? field.label : "-";
};

export const getRefDataOptionFromValue = (refDataFields, fieldValue) => {
  return (
    refDataFields.find((field) => {
      if (field.id) {
        return field.id.toString() === fieldValue.toString();
      }
      if (field.value) {
        return field.value.toString() === fieldValue.toString();
      } else if (field.code) {
        return field.code.toString() === fieldValue.toString();
      }
      return undefined;
    }) || undefined
  );
};

/**
 * Gets the data has been came from Backend and add the property value and label to be directly used by the
 * AutocompleteSelect.
 * And apllies the i18nProvider in the label
 * @param {*} data
 * @param {*} I18nProvider
 */
export const mapBackendDTO = (data, I18nProvider) =>
  data.map((element) => ({
    ...element,
    value: element.code,
    label: I18nProvider.Texti18n(element.description),
  }));


export const mapSupplierDTO = data => data.map(element => ({
  id: element.code,
  code: element.code,
  name: element.name,
  description: element.name,
  tax: element.nif,
  marketCode: element.marketCode
}));