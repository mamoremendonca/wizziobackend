import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { ComponentName, styles } from "./PDFToolbarStyles";
import Icon from "@ui-lib/core/Icon";
import PropTypes from "prop-types";

const PDFToolbarRender = ({ classes, toolbarSlot, titleSpan }) => {
  return (
    <div className={classes.flexContainer}>
      <div className={classes.pdfNavIcon}>
        {/* <img src={icon} alt="Icone" style={{ maxHeight: "1.5em" }} /> */}
        <Icon style={{ maxHeight: "1.5em" }}>subject</Icon>
      </div>

      <div className={classes.pdfNavTitle}>
        <div style={{ padding: "0 2px" }}>
          <span>Visualização do Documento</span>
        </div>
      </div>

      <div className={classes.pdfNavTools}>
        {/* <div style={{ padding: '0 2px' }}>
                    {toolbarSlot.previousPageButton}
                </div>
                <div style={{ padding: '0 2px' }}>
                    {toolbarSlot.currentPage + 1} / {toolbarSlot.numPages}
                </div>
                <div style={{ padding: '0 2px' }}>
                    {toolbarSlot.nextPageButton}
                </div> */}
        <div style={{ padding: "0 2px" }}>{toolbarSlot.zoomOutButton}</div>
        <div style={{ padding: "0 2px" }}>{toolbarSlot.zoomPopover}</div>
        <div style={{ padding: "0 2px" }}>{toolbarSlot.zoomInButton}</div>
      </div>
    </div>
  );
};

PDFToolbarRender.propTypes = {
  classes: PropTypes.object,
  toolbarSlot: PropTypes.object,
  titleSpan: PropTypes.string,
};

export default withStyles(styles, { name: ComponentName })(PDFToolbarRender);
