import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { componentName, styles } from "./FooterStyles";
import Button from "@ui-lib/core/Button";
import ConfirmationDialog from "../../../components/Dialog/ConfirmationDialog";
import PropTypes from "prop-types";

const FooterRender = ({
  classes,
  I18nProvider: { Labeli18n },
  confirmationDialogOptions: { open, title, onClose },
  onCancel,
}) => {
  return (
    <div className={classes.root}>
      <Button variant="contained" color="primary" onClick={onCancel}>
        {Labeli18n("fdRejectionScreen.footer.cancel.button.label")}
      </Button>
      <ConfirmationDialog
        open={open}
        title={title}
        onClose={onClose}
        cancelButtonLabel={Labeli18n(
          "fdRejectionScreen.footer.confirmation_dialog.cancel.button.label"
        )}
        acceptButtonLabel={Labeli18n(
          "fdRejectionScreen.footer.confirmation_dialog.accept.button.label"
        )}
      />
    </div>
  );
};

FooterRender.propTypes = {
  classes: PropTypes.object,
  I18nProvider: PropTypes.object,
  confirmationDialogOptions: PropTypes.object,
  onCancel: PropTypes.func,
};

export default withStyles(styles, { name: componentName })(FooterRender);
