import React, { Component } from "react";
import AttachmentRender from "./AttachmentRender";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { downloadAttachmentRequest } from "../../../../../actions/services/financialDocumentServiceActions";
import { Config } from "../../../../../config";
import { removeTimelineAttachment } from "../../../../../actions";
import PropTypes from "prop-types";

class Attachment extends Component {
  downloadAttachment = () => {
    const {
      attachmentData,
      dataSource,
      I18nProvider,
      Notifications,
      HttpClient,
      FileSystem,
      Loading,
      financialDocumentData,
    } = this.props;
    let instanceId = undefined;
    if (dataSource === Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY) {
      instanceId = financialDocumentData.jwcontext.id;
    } else {
      instanceId = financialDocumentData.financialDocumentHeader.repositoryId;
    }
    downloadAttachmentRequest(
      HttpClient,
      Notifications,
      FileSystem,
      Loading,
      instanceId,
      attachmentData.id,
      I18nProvider.Texti18n("services.download_attachment.error.message"),
      dataSource
    );
  };
  deleteAttachment = () => {
    const {
      financialDocumentData: {
        jwcontext: { id },
      },
      HttpClient,
      Notifications,
      removeTimelineAttachment,
      timelineData,
      I18nProvider,
      attachmentData,
    } = this.props;

    const attachmentToRemove = {
      ...attachmentData,
      index: attachmentData.id,
    };
    const stepIndex = timelineData.findIndex(
      (item) => item.type === "TO_APPROVE"
    );
    removeTimelineAttachment(
      HttpClient,
      stepIndex,
      attachmentToRemove,
      id,
      Notifications,
      I18nProvider.Texti18n("service.removeTimelineAttachment.error.message")
    );
  };
  render() {
    const { attachmentData } = this.props;
    return (
      <AttachmentRender
        attachmentData={attachmentData}
        downloadAttachment={this.downloadAttachment}
        deleteAttachment={this.deleteAttachment}
      />
    );
  }
}

Attachment.propTypes = {
  I18nProvider: PropTypes.object,
  Notifications: PropTypes.object,
  FileSystem: PropTypes.object,
  HttpClient: PropTypes.object,
  Loading: PropTypes.object,
  removeTimelineAttachment: PropTypes.func,
  financialDocumentData: PropTypes.object,
  timelineData: PropTypes.any,
  attachmentData: PropTypes.object,
  dataSource: PropTypes.string,
};

const mapStateToProps = ({
  journey: {
    services: { I18nProvider, Notifications, FileSystem, HttpClient, Loading },
  },
  financialDocumentReducer: { financialDocumentData, timelineData },
}) => ({
  I18nProvider,
  Notifications,
  FileSystem,
  HttpClient,
  Loading,
  financialDocumentData,
  timelineData,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ removeTimelineAttachment }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Attachment);
