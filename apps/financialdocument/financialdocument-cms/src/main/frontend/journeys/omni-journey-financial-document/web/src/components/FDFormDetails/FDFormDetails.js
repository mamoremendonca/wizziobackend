import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { styles, ComponentName } from "./FDFormDetailsStyles";

const FDFormDetails = () => {
  return <h3>FD Form Details</h3>;
};

export default withStyles(styles, { name: ComponentName })(FDFormDetails);
