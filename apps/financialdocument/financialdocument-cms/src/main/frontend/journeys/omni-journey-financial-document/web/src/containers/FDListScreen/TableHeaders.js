import React from "react";
import Tooltip from "@ui-lib/core/Tooltip";
import Icon from "@ui-lib/core/Icon";
import StatusTooltip from "./StatusToolTip";
//todo import of that adds the tooltip for the states
const FDListHeaders = (i18nProvider) => {
  return [
    {
      label: (
        <Tooltip
          key="status_tooltip"
          title={
            <StatusTooltip
              key={"status-tooltip-details"}
              i18nProvider={i18nProvider}
            />
          }
        >
          <div style={{ marginLeft: "24px", marginTop: "8px" }}>
            <Icon style={{ fontSize: "21px" }}>info_outlined</Icon>
          </div>
        </Tooltip>
      ),
      key: "repositoryStatus",
      sortable: true,
    },
    {
      label: i18nProvider.Labeli18n("fd_number"),
      key: "number",
      sortable: true,
    },
    { label: i18nProvider.Labeli18n("fd_type"), key: "type", sortable: true },
    {
      label: i18nProvider.Labeli18n("supplier"),
      key: "supplierCode",
      sortable: false,
    },
    {
      label: i18nProvider.Labeli18n("po_number"),
      key: "purchaseOrderFriendlyNumber",
      sortable: true,
    },
    {
      label: i18nProvider.Labeli18n("vat_exclusive"),
      key: "totalWithoutTax",
      sortable: true,
    },
    {
      label: i18nProvider.Labeli18n("created_at"),
      key: "creationDate",
      sortable: true,
    },
    {
      label: i18nProvider.Labeli18n("approval_date_limit"),
      key: "expirationDate",
      sortable: true,
    },
    {
      label: i18nProvider.Labeli18n("department"),
      key: "departmentCode",
      sortable: false,
    },
    {
      label: i18nProvider.Labeli18n("project"),
      key: "projectCode",
      sortable: false,
    },
    {
      label: i18nProvider.Labeli18n("internal_order"),
      key: "internalOrderCode",
      sortable: false,
    },
    {
      label: i18nProvider.Labeli18n("owner_responsible_position"),
      key: "userGroup",
      sortable: true,
    },
  ];
};

export { FDListHeaders };
