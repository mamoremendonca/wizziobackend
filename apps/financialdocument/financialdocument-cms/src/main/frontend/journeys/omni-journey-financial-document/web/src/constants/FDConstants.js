// HANDLE INITIAL CREATE OF FINANCIAL DOCUMENT
export const CREATE_FINANCIAL_DOCUMENT = "CREATE_FINANCIAL_DOCUMENT";
export const CREATE_FINANCIAL_DOCUMENT_SUCESS =
  "CREATE_FINANCIAL_DOCUMENT_SUCESS";
export const CREATE_FINANCIAL_DOCUMENT_ERROR =
  "CREATE_FINANCIAL_DOCUMENT_ERROR";
// HANDLE SUBMISSION OF FINANCIAL DOCUMENT TO UPDATE
export const UPDATE_FINANCIAL_DOCUMENT = "UPDATE_FINANCIAL_DOCUMENT";
export const UPDATE_FINANCIAL_DOCUMENT_SUCCESS =
  "UPDATE_FINANCIAL_DOCUMENT_SUCCESS";
export const UPDATE_FINANCIAL_DOCUMENT_ERROR =
  "UPDATE_FINANCIAL_DOCUMENT_ERROR";
// DELETE FINANCIAL DOCUMENT
export const DELETE_FINANCIAL_DOCUMENT = "DELETE_FINANCIAL_DOCUMENT";
export const DELETE_FINANCIAL_DOCUMENT_SUCCESS =
  "DELETE_FINANCIAL_DOCUMENT_SUCCESS";
export const DELETE_FINANCIAL_DOCUMENT_ERROR =
  "DELETE_FINANCIAL_DOCUMENT_ERROR";
// HANDLE UPDATE OF FINANCIAL DOCUMENT STATE LOCAL
export const SET_FINANCIAL_DOCUMENT = "SET_FINANCIAL_DOCUMENT";
export const CLEAR_FINANCIAL_DOCUMENT = "CLEAR_FINANCIAL_DOCUMENT";
//ADD ATTACHMENTS OF FINANCIAL DOCUMENT
export const ADD_FINANCIAL_DOCUMENT_ATTACHMENT =
  "ADD_FINANCIAL_DOCUMENT_ATTACHMENT";
export const ADD_FINANCIAL_DOCUMENT_ATTACHMENT_SUCCESS =
  "ADD_FINANCIAL_DOCUMENT_ATTACHMENT_SUCCESS";
export const ADD_FINANCIAL_DOCUMENT_ATTACHMENT_ERROR =
  "ADD_FINANCIAL_DOCUMENT_ATTACHMENT_ERROR";
//REMOVE ATTACHMENTS OF FINANCIAL DOCUMENT
export const REMOVE_FINANCIAL_DOCUMENT_ATTACHMENT =
  "REMOVE_FINANCIAL_DOCUMENT_ATTACHMENT";
export const REMOVE_FINANCIAL_DOCUMENT_ATTACHMENT_SUCCESS =
  "REMOVE_FINANCIAL_DOCUMENT_ATTACHMENT_SUCCESS";
export const REMOVE_FINANCIAL_DOCUMENT_ATTACHMENT_ERROR =
  "REMOVE_FINANCIAL_DOCUMENT_ATTACHMENT_ERROR";
//ADD ITEMS OF FINANCIAL DOCUMENT
export const ADD_FINANCIAL_DOCUMENT_ITEM = "ADD_FINANCIAL_DOCUMENT_ITEM";
export const ADD_FINANCIAL_DOCUMENT_ITEM_SUCCESS =
  "ADD_FINANCIAL_DOCUMENT_ITEM_SUCCESS";
export const ADD_FINANCIAL_DOCUMENT_ITEM_ERROR =
  "ADD_FINANCIAL_DOCUMENT_ITEM_ERROR";
//EDIT ITEMS OF FINANCIAL DOCUMENT
export const EDIT_FINANCIAL_DOCUMENT_ITEM = "EDIT_FINANCIAL_DOCUMENT_ITEM";
export const EDIT_FINANCIAL_DOCUMENT_ITEM_SUCCESS =
  "EDIT_FINANCIAL_DOCUMENT_ITEM_SUCCESS";
export const EDIT_FINANCIAL_DOCUMENT_ITEM_ERROR =
  "EDIT_FINANCIAL_DOCUMENT_ITEM_ERROR";
//REMOVE ITEMS OF FINANCIAL DOCUMENT
export const REMOVE_FINANCIAL_DOCUMENT_ITEM = "REMOVE_FINANCIAL_DOCUMENT_ITEM";
export const REMOVE_FINANCIAL_DOCUMENT_ITEM_SUCCESS =
  "REMOVE_FINANCIAL_DOCUMENT_ITEM_SUCCESS";
export const REMOVE_FINANCIAL_DOCUMENT_ITEM_ERROR =
  "REMOVE_FINANCIAL_DOCUMENT_ITEM_ERROR";
// SUBMIT TO APPROVAL
export const SUBMIT_FINANCIAL_DOCUMENT = "SUBMIT_FINANCIAL_DOCUMENT";
export const SUBMIT_FINANCIAL_DOCUMENT_SUCCESS =
  "SUBMIT_FINANCIAL_DOCUMENT_SUCCESS";
export const SUBMIT_FINANCIAL_DOCUMENT_ERROR =
  "SUBMIT_FINANCIAL_DOCUMENT_ERROR";
//TIMELINE ATTACHMENTS
export const ADD_ATTACHMENT_TO_TIMELINE_STEP =
  "ADD_ATTACHMENT_TO_TIMELINE_STEP";
export const REMOVE_ATTACHMENT_FROM_TIMELINE_STEP =
  "REMOVE_ATTACHMENT_FROM_TIMELINE_STEP";
export const SET_ATTACHMENT_TIMELINE_LOADING =
  "SET_ATTACHMENT_TIMELINE_LOADING";
///LIST FD
export const SET_FD_LIST_PROCESSES = "SET_FD_LIST_PROCESSES";
//FILTER DATA

export const FINANCIAL_DOCUMENT_STATUS = {
  IN_APPROVAL: "IN_APPROVAL",
  APPROVED: "APPROVED",
  SAP_SUBMITTED: "SAP_SUBMITTED",
  REJECTED: "REJECTED",
  CANCELED: "CANCELED",
  PAID: "PAID",
};

export const SCOPE_VALUES = {
  MINE: "MINE",
  TEAM: "TEAM",
  ALL: "ALL",
};

export const SORTING_ORDER_MAPPER = {
  asc: "ASC",
  desc: "DESC",
};

export const INITIAL_FILTER_CRITERIA = {
  status: "",
  number: "",
  purchaseOrderNumber: "",
  type: "",
  entityCode: "",
  internalOrderCode: "",
  minVATValue: "",
  maxVATValue: "",
  createdDate: "",
  expirationDate: "",
  departmentCode: "",
  projectCode: "",
  scope: SCOPE_VALUES.MINE,
};

export const SORT_FIELDS_MAPPER = {
  repositoryStatus: "STATUS",
  friendlyNumber: "NUMBER",
  number: "NUMBER",
  purchaseOrderFriendlyNumber: "PO_NUMBER",
  type: "TYPE",
  supplierCode: "SUPPLIER",
  internalOrderCode: "INTERNAL_ORDER",
  totalWithoutTax: "TOTAL",
  creationDate: "CREATION_DATE",
  expirationDate: "EXPIRATION_DATE",
  departmentCode: "DEPARTMENT",
  projectCode: "PROJECT",
  userGroup: "USER_GROUP",
};

export const RESET_FILTER_CRITERIA = "RESET_FILTER_CRITERIA";
export const UPDATE_FILTER_CRITERIA = "UPDATE_FILTER_CRITERIA";
export const UPDATE_SORTING_CRITERIA = "UPDATE_SORTING_CRITERIA";
export const DEFAULT_SORTED_FIELD = {
  orderField: "creationDate",
  orderType: "asc",
};

export const SET_FINANCIAL_DOCUMENT_DEPENDENCY =
  "SET_FINANCIAL_DOCUMENT_DEPENDENCY";
export const APPROVAL_DECISIONS = {
  APPROVE: "APPROVE",
  RETURN: "RETURN",
  REJECT: "REJECT",
};

export const FINANCIAL_DOCUMENT_TYPE = "type";
export const FINANCIAL_DOCUMENT_FINAL_TYPE = "finaType";

export const DESCRIPTION = "description";
export const DEPARTMENT = "department";
export const PROJECT = "project";
export const INTERNAL_ORDER = "internalOrder";
export const EMITION_DATE = "emissionDate";
export const LIMIT_DATE = "limitDate";
export const NO_ITEM = "noItem";
export const FD_CAPEX = "CAPEX";
export const FD_CONTRACT = "CONTRACT";
export const FD_OPEX = "OPEX";
export const FINANCIAL_DOCUMENT_TYPE_VALUES = {
  CREDIT_NOTE: "CREDIT_NOTE",
  RETENTION_INVOICE: "RETENTION_INVOICE",
  INVOICE: "INVOICE",
};

//#region Fields for ScanningForm
export const FINANCIAL_DOCUMENT_NUMBER = "number";
export const EMISSION_DATE = "emissionDate";
export const SUPPLIER_CODE = "supplierCode";
export const SUPPLIER_NAME = "supplierName";
export const SUPPLIER_TAX_NUMBER = "supplierTaxNumber";
export const CREATION_DATE = "creationDate";
export const DELETE_FD_BUTTON_ID = "deleteFD";
export const UPLOAD_INVOICE_BUTTON_ID = "uploadInvoice";
export const ADD_ITEM_ID = "addItem"; //TODO in future delete this

//#endregion

//#region Fields for AccountsPayableTeamForm
export const GRANTING = "granting";
export const ASSOCIATED_DOCUMENT_FRIENDLY_NUMBER = "associatedDocument";
export const DESCRIPTION_FIELD = "description";
export const DEPARTMENT_CODE = "departmentCode";
export const GROUP_CODE = "firstWorkflowGroup";
export const PROJECT_CODE = "projectCode";
export const INTERNAL_ORDER_CODE = "internalOrderCode";
export const EXPIRATION_DATE = "expirationDate";
export const PURCHASE_ORDER_FRIENDLY_NUMBER = "purchaseOrderFriendlyNumber";
export const TOTAL_WITHOUT_TAX = "totalWithoutTax";

// the fields that change the table items structure when they were changed
export const TABLE_ITEMS_CHANGE_FIELDS = [
  DEPARTMENT_CODE,
  PROJECT_CODE,
  INTERNAL_ORDER_CODE,
  SUPPLIER_CODE,
];

//#endregion

//#region journey.js
export const FINANCIAL_DOCUMENT_PROCESS_STATUS = {
  DRAFT: "Draft",
  IN_CREATION: "In Creation",
  IN_APPROVAL: "In Approval",
  REJECTED: "Rejected",
};

//#endregion

//#region Timeline actions constants
export const SET_TIMELINE = "SET_TIMELINE";
//#endregion

//#region server error exceptions
export const SERVER_ERROR_EXCEPTIONS = {
  FIELD_VALIDATION_EXCEPTION: "FIELD_VALIDATION_EXCEPTION",
  TOTAL_VALUE_ASSOCIATED_PO_EXCEPTION: "TOTAL_VALUE_ASSOCIATED_PO_EXCEPTION",
  FINANCIAL_DOCUMENT_NUMBER_ALREADY_EXISTS_EXCEPTION:
    "FINANCIAL_DOCUMENT_NUMBER_ALREADY_EXISTS_EXCPETION",
};
//#endregion

//#region Default Form Values
//reset value of values when
export const RESET_VALUE = null;

//#endregion
