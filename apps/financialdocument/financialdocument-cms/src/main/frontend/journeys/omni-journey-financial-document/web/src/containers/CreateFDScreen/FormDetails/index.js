import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { ComponentName, styles } from "./FormDetailsStyles";
import Grid from "@ui-lib/core/Grid";
import ScanningTeam from "./ScanningTeam";
import AccountsPayableTeam from "./AccountsPayableTeam";
import PropTypes from "prop-types";

const FormDetails = ({
  refData,
  I18nProvider,
  handleOnBlur,
  classes,
  supplierCodes,
  supplierNames,
  supplierTaxes,
  lockForm,
  formValues,
  showAccountsPayableTeam,
  isFDCombinedWithPO,
  associatePurchaseOrder,
  associateFinancialDocument,
  hasDepartmentChanged
}) => {

  return (
    <div className={classes.root} onBlur={(ev) => handleOnBlur(formValues, ev)}>
      <Grid container>
        <Grid item xs={12}>
          <ScanningTeam
            I18nProvider={I18nProvider}
            refData={refData}
            supplierCodes={supplierCodes}
            supplierNames={supplierNames}
            supplierTaxes={supplierTaxes}
            lockForm={lockForm}
            lockFormForToAccountsPayableTeam={showAccountsPayableTeam}
            isFDCombinedWithPO={isFDCombinedWithPO}
          />
        </Grid>
        <Grid item xs={12}>
          <AccountsPayableTeam
            formValues={formValues}
            showAccountsPayableTeam={showAccountsPayableTeam}
            I18nProvider={I18nProvider}
            hasDepartmentChanged={hasDepartmentChanged}
            refData={refData}
            isFDCombinedWithPO={isFDCombinedWithPO}
            associatePurchaseOrder={associatePurchaseOrder}
            associateFinancialDocument={associateFinancialDocument}
          />
        </Grid>
      </Grid>
    </div>
  );
};

FormDetails.propTypes = {
  refData: PropTypes.object,
  I18nProvider: PropTypes.object,
  handleOnBlur: PropTypes.func,
  classes: PropTypes.object,
  supplierCodes: PropTypes.array,
  supplierNames: PropTypes.array,
  supplierTaxes: PropTypes.array,
  lockForm: PropTypes.bool,
  formValues: PropTypes.object,
  showAccountsPayableTeam: PropTypes.bool,
  isFDCombinedWithPO: PropTypes.bool,
  associatePurchaseOrder: PropTypes.func,
  associateFinancialDocument: PropTypes.func,
};

export default withStyles(styles, { name: ComponentName })(FormDetails);
