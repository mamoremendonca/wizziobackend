export const componentName = "AttachmentStyles";
export const styles = (theme) => ({
  root: {
    backgroundColor: theme.palette.common.white,
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px`,
    margin: `${theme.spacing.unit * 2}px 0px`,
  },
  deleteAttachment: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  circularProgress: {
    marginBottom: `${theme.spacing.unit * 2}px`,
    width: `${theme.spacing.unit * 3}px !important`,
    height: `${theme.spacing.unit * 3}px !important`,
  },
  closeIcon: {
    fontSize: 24,
    cursor: "pointer",
    padding: 5,
    bottom: 5,
    "&:hover": {
      color: theme.palette.primary.main,
    },
  },
  attachmentContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingBottom: theme.spacing.unit * 2,
  },
  attachmentHeader: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  attachmentTitle: {
    paddingLeft: theme.spacing.unit,
    "&:hover": {
      color: theme.palette.primary.main,
      textDecorationLine: "underline",
    },
    cursor: "pointer",
  },
  attachmentIcon: {
    fontSize: 16,
    transform: "rotate(45deg)",
  },
});
