const ComponentName = 'DialogStyle';
const styles = theme => ({
  dialogContainer:{
    position: "absolute"
  },
  dialogBackdrop:{
    position: "absolute"
  },
  headerContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  Icon: {
    cursor: "pointer",
    margin: `0 ${theme.spacing.unit}px`,
  },
  buttonCancel: {
    position: "absolute",
    right: "5px",
    top: "0px"
  },
  iconCancel: {
    color: "blue"
  },
  
});

export { ComponentName, styles };
