import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import withContainerWrapper from "../../components/ContainerWrapper";
import FDRejectionScreenRender from "./FDRejectionScreenRender";
import { getFinancialDocumentData, getTimelineData } from "../../actions";
import { calculateTotalItemsValue } from "../../utils/infoFDUtils";
import PropTypes from "prop-types";

class FDRejectionScreen extends Component {
  //TODO: refactor put this function inside FDTableItems create a component
  state = {
    allDependenciesResolved: false,
  };

  componentDidMount() {
    const {
      match,
      HttpClient,
      Notifications,
      I18nProvider,
      Loading,
      getFinancialDocumentData,
      getTimelineData,
    } = this.props;
    Loading.request("setLoading", "JOURNEY").then(() => {
      if (match && match.params && match.params.id) {
        getFinancialDocumentData(
          HttpClient,
          Notifications,
          I18nProvider.Texti18n(
            "services.get.financial_document.error.message"
          ),
          match.params.id
        );
        getTimelineData(
          HttpClient,
          Notifications,
          match.params.id,
          I18nProvider.Texti18n("service.get_timeline.error.message")
        );
      }
    });
  }

  componentDidUpdate() {
    const { financialDocumentData, timelineData, Loading } = this.props;
    const { allDependenciesResolved } = this.state;
    if (!allDependenciesResolved && financialDocumentData && timelineData) {
      this.setState({ allDependenciesResolved: true });
      Loading.request("stopLoading");
    }
  }

  render() {
    const { allDependenciesResolved } = this.state;
    const { financialDocumentData, I18nProvider, refData } = this.props;
    if (allDependenciesResolved)
      return (
        <FDRejectionScreenRender
          I18nProvider={I18nProvider}
          financialDocumentData={financialDocumentData}
          refData={refData}
          totalItemsValue={calculateTotalItemsValue(
            refData,
            financialDocumentData
          )}
        />
      );
    return null;
  }
}

FDRejectionScreen.propTypes = {
  I18nProvider: PropTypes.object,
  Loading: PropTypes.object,
  Notifications: PropTypes.object,
  HttpClient: PropTypes.object,
  financialDocumentData: PropTypes.object,
  refData: PropTypes.object,
  timelineData: PropTypes.any,
  match: PropTypes.object,
  getFinancialDocumentData: PropTypes.func,
  getTimelineData: PropTypes.func,
};

const mapStateToProps = ({
  journey: {
    services: { I18nProvider, Loading, Notifications, HttpClient },
  },
  financialDocumentReducer: { financialDocumentData, timelineData },
  refDataReducer: { refData },
}) => ({
  I18nProvider,
  Loading,
  Notifications,
  HttpClient,
  financialDocumentData,
  refData,
  timelineData,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ getFinancialDocumentData, getTimelineData }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withContainerWrapper(FDRejectionScreen));
