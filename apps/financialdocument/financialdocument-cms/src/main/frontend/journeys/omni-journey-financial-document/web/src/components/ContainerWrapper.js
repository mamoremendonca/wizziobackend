import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { updateJourneySubtitle } from "../routes/Routes";
import PropTypes from "prop-types";

export default (ComposedComponent) => {
  class ReduxContainer extends React.PureComponent {
    //To update the subtitle as soon as the component is mount.
    componentDidMount() {
      const { location, JourneyActions, JourneySettings } = this.props;
      updateJourneySubtitle(location.pathname, JourneyActions, JourneySettings);
    }

    //To update the the subtitle after the purchase order is loaded.
    componentDidUpdate(prevProps) {
      const {
        location,
        JourneyActions,
        JourneySettings,
        financialDocumentData,
        I18nProvider,
      } = this.props;
      if (financialDocumentData !== prevProps.financialDocumentData) {
        updateJourneySubtitle(
          location.pathname,
          JourneyActions,
          JourneySettings,
          financialDocumentData,
          I18nProvider
        );
      }
    }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }

  ReduxContainer.propTypes = {
    location: PropTypes.object,
    JourneyActions: PropTypes.object,
    I18nProvider: PropTypes.object,
    JourneySettings: PropTypes.object,
    financialDocumentData: PropTypes.object,
  };

  const mapStateToProps = ({
    journey: {
      services: { I18nProvider, JourneyActions },
      settings,
      i18n,
    },
    financialDocumentReducer: { financialDocumentData },
  }) => ({
    i18n, //to update textI18n when the current language changes.
    I18nProvider,
    JourneyActions,
    JourneySettings: settings,
    financialDocumentData,
  });
  return connect(mapStateToProps)(withRouter(ReduxContainer));
};
