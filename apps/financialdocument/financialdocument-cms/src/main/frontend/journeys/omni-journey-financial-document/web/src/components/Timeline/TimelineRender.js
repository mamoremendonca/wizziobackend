import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import Icon from "@ui-lib/core/Icon";
import IconButton from "@ui-lib/core/IconButton";
import Typography from "@ui-lib/core/Typography";
import { componentName, styles } from "./TimelineStyles";
import * as constants from "../../constants/timeline";
import ActivityTimeline from "@ui-lib/oney/ActivityTimeline";
import AttachmentList from "./AttachmentList";
import FileInput from "../Shared/FileInput";
import PropTypes from "prop-types";

const TimelineRender = ({
  classes,
  I18nProvider,
  currentTab,
  allowAddAttachments,
  uploadAttachment,
  downloadAttachment,
  selectTab,
  setContainerLoadingDependencies,
  isLoading,
  dataSource,
  refreshTimelineData,
  activitiesTimeline,
  timelineAttachments,
}) => {
  return (
    <div className={classes.root}>
      <div className={classes.header}>
        <div className={classes.headerTile}>
          <div className={classes.iconContainer}>
            <Icon className={classes.icon}>list</Icon>
          </div>
          <div className={classes.headerDescription}>
            <Typography variant="h3" color="textPrimary">
              {I18nProvider.Labeli18n("timeline.header.description")}
            </Typography>
          </div>
        </div>
        <div className={classes.flexRowContainer}>
          <div
            id={constants.TABS.ACTIVITIES}
            className={
              currentTab === constants.TABS.ACTIVITIES
                ? classes.selectedHeaderTab
                : classes.headerTab
            }
            onClick={selectTab}
          >
            {I18nProvider.Labeli18n(
              `timeline.header.tab.${constants.TABS.ACTIVITIES}.label`
            )}
          </div>
          <div
            id={constants.TABS.ATTACHMENTS}
            className={
              currentTab === constants.TABS.ATTACHMENTS
                ? classes.selectedHeaderTab
                : classes.headerTab
            }
            onClick={selectTab}
          >
            {I18nProvider.Labeli18n(
              `timeline.header.tab.${constants.TABS.ATTACHMENTS}.label`
            )}
          </div>
        </div>
      </div>
      <div className={classes.timelineActionsContainer}>
        <IconButton
          disabled={isLoading}
          onClick={refreshTimelineData}
          className={classes.refreshIconButton}
        >
          <Icon>autorenew</Icon>
        </IconButton>
        {currentTab === constants.TABS.ATTACHMENTS && allowAddAttachments && (
          <FileInput handleFileSelection={uploadAttachment} />
        )}
      </div>
      <div className={classes.contentComponentContainer}>
        {currentTab === constants.TABS.ACTIVITIES && activitiesTimeline && (
          <ActivityTimeline
            downloadClick={downloadAttachment}
            data={activitiesTimeline}
          ></ActivityTimeline>
        )}
        {currentTab === constants.TABS.ATTACHMENTS &&
          timelineAttachments.length > 0 && (
            <AttachmentList
              dataSource={dataSource}
              setContainerLoadingDependencies={setContainerLoadingDependencies}
              timelineAttachments={timelineAttachments}
            />
          )}
      </div>
    </div>
  );
};

TimelineRender.propTypes = {
  I18nProvider: PropTypes.object,
  classes: PropTypes.object,
  currentTab: PropTypes.string,
  dataSource: PropTypes.string,
  refreshTimelineData: PropTypes.func,
  allowAddAttachments: PropTypes.bool,
  downloadAttachment: PropTypes.func,
  setContainerLoadingDependencies: PropTypes.func,
  timelineAttachments: PropTypes.object,
  uploadAttachment: PropTypes.func,
  isLoading: PropTypes.bool,
  selectTab: PropTypes.func,
  activitiesTimeline: PropTypes.object,
};

export default withStyles(styles, { name: componentName })(TimelineRender);
