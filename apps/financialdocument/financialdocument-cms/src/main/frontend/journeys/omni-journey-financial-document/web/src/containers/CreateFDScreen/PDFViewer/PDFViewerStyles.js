const ComponentName = "PDFViewerStyle";

/* setTimeout(function(){ 
    const height = document.body.scrollHeight 
    alert(height)
}, 2000); */

const styles = (theme) => ({
    workerRoot: {
        display: "flex"
    },
    workerViewer: {
        height: "1125px",
        width: "100% !important"
    }

});

export { styles, ComponentName };