import * as React from "react";
import Radio from "@ui-lib/core/Radio";
import PropTypes from "prop-types";

const RadioWrapper = ({
  input: { checked, value, name, onChange, ...restInput },
  meta,
  ...rest
}) => (
  <Radio
    {...rest}
    name={name}
    inputProps={restInput}
    onChange={onChange}
    checked={checked}
    value={value}
  />
);

RadioWrapper.propTypes = { input: PropTypes.object, meta: PropTypes.object };

export default RadioWrapper;
