import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { styles, ComponentName } from "./FDTableItemsStyle";
import CustomTable from "@ui-lib/custom-components/CustomTable";
import FDTableItemsHeaders from "./FDTableItemsHeaders";
import Icon from "@ui-lib/core/Icon";
import { PURCHASE_ORDER_TYPES } from "../../constants";
import { dateFormatter, getFormattedNumber } from "../../utils";
import PropTypes from "prop-types";

const auxNumberCheck = (value) => {
  return value !== undefined && value !== null && !isNaN(value);
};

const createTableRow = (
  dataRow,
  i18nProvider,
  refData,
  financialDocumentHeader,
  handleCopy,
  handleEdit
) => {
  const tableRow = {};
  tableRow.version = 1;
  if (
    financialDocumentHeader.purchaseOrderType !== PURCHASE_ORDER_TYPES.PO_CAPEX
  ) {
    tableRow.articleType = refData.itemTypes.find(
      (itemType) =>
        itemType.code ===
        refData.items.find((item) => item.code === dataRow.code).itemTypeCode
    ).description;
  }
  tableRow.article = dataRow.code;
  tableRow.description = dataRow.description;
  if (
    financialDocumentHeader.purchaseOrderType !== PURCHASE_ORDER_TYPES.PO_CAPEX
  ) {
    let costCenterToDisplay =
      refData.projects.find(
        (project) => project.code === dataRow.costCenterCode
      ) ||
      refData.costCenters.find(
        (costCenter) => costCenter.code === dataRow.costCenterCode
      );
    tableRow.projectCode = costCenterToDisplay
      ? costCenterToDisplay.description
      : " - ";
  }
  tableRow.quantity = getFormattedNumber(dataRow.amount, false);

  //Check if unitPriceWithoutTax has a value
  let numberCheck = auxNumberCheck(dataRow.unitPriceWithoutTax);
  tableRow.unitPriceWithoutTax = numberCheck
    ? getFormattedNumber(dataRow.unitPriceWithoutTax, true, "EUR")
    : " - ";

  //Check if floatItemPriceWithoutTax has a value
  const floatItemPriceWithoutTax = numberCheck
    ? dataRow.unitPriceWithoutTax * dataRow.amount
    : null;
  tableRow.itemPriceWithoutTax = auxNumberCheck(floatItemPriceWithoutTax)
    ? getFormattedNumber(floatItemPriceWithoutTax, true, "EUR")
    : " - ";

  //VAT value
  const floatVAT =
    dataRow.taxCode !== undefined && dataRow.taxCode !== null
      ? parseFloat(
          refData.tax.find((taxObject) => taxObject.code === dataRow.taxCode)
            .taxPercentage
        )
      : null;
  tableRow.VAT = floatVAT ? `${floatVAT.toFixed(0)} %` : " - ";

  //Check if floatItemPriceWithTax has a value
  const floatItemPriceWithTax = floatItemPriceWithoutTax * (1 + floatVAT / 100);
  tableRow.itemPriceWithTax = numberCheck
    ? getFormattedNumber(floatItemPriceWithTax, true, "EUR")
    : " - ";

  if (dataRow.retentionCode) {
    const floatRetentionPercentage = parseFloat(
      refData.retentionCodes.find(
        (retentionObject) => retentionObject.code === dataRow.retentionCode
      ).taxPercentage
    );
    tableRow.retentionPercentage = `${floatRetentionPercentage.toFixed(0)} %`;
    const floatRetention =
      (floatRetentionPercentage * floatItemPriceWithoutTax) / 100;
    tableRow.retentionValue = getFormattedNumber(floatRetention, true, "EUR");
    tableRow.finalAmount = getFormattedNumber(
      floatItemPriceWithTax - floatRetention,
      true,
      "EUR"
    );
  }

  tableRow.startDate = dateFormatter(dataRow.startDate);
  tableRow.endDate = dateFormatter(dataRow.endDate);
  tableRow.copyButton = (
    <Icon onClick={() => handleCopy(dataRow.index)}>filter_none</Icon>
  );
  tableRow.editButton = (
    <Icon onClick={() => handleEdit(dataRow.index)}>edit_outlined</Icon>
  );
  return tableRow;
};

const tableParseData = (
  data,
  i18nProvider,
  refData,
  financialDocumentHeader,
  handleCopy,
  handleEdit
) => {
  let parseData = [];
  data.forEach((element) => {
    parseData.push(
      createTableRow(
        element,
        i18nProvider,
        refData,
        financialDocumentHeader,
        handleCopy,
        handleEdit
      )
    );
  });
  return parseData;
};

const FDTableItems = ({
  I18nProvider,
  itemsList,
  financialDocumentHeader,
  refData,
  handleCopy,
  handleEdit,
  classes,
}) => {
  return (
    <div className={classes.tableParent}>
      <CustomTable
        id="TableCreateFD"
        singleSort={true}
        headers={FDTableItemsHeaders(
          I18nProvider,
          financialDocumentHeader,
          handleCopy,
          handleEdit
        )}
        noDataLabel={I18nProvider.Texti18n("no_results_found")}
        data={tableParseData(
          itemsList,
          I18nProvider,
          refData,
          financialDocumentHeader,
          handleCopy,
          handleEdit
        )}
      />
    </div>
  );
};

FDTableItems.propTypes = {
  I18nProvider: PropTypes.object,
  itemsList: PropTypes.array,
  financialDocumentHeader: PropTypes.object,
  refData: PropTypes.object,
  handleCopy: PropTypes.func,
  handleEdit: PropTypes.func,
  classes: PropTypes.object,
};

export default withStyles(styles, { name: ComponentName })(FDTableItems);
