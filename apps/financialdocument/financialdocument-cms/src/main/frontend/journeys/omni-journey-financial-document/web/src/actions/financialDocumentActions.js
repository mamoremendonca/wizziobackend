import * as constants from "../constants";
import { Config } from "../config";
import { mapFormValuesToBackendItem } from "../utils";
export const setFinancialDocument = (financialDocument) => {
  return {
    type: constants.SET_FINANCIAL_DOCUMENT,
    payload: financialDocument,
  };
};

export const clearFinancialDocument = () => {
  return {
    type: constants.CLEAR_FINANCIAL_DOCUMENT,
  };
};

export const createFinancialDocument = (
  HttpClient,
  Notifications,
  errorMessage
) => {
  let config = {
    method: Config.METHOD_POST,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}`,
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        dispatch(setFinancialDocument(res.data));
      })
      .catch(() => {
        Notifications.notify(errorMessage, "error", "Process", 5000);
      });
  };
};

// When we want to add attachments to the FD we have to call this method
export const addFinancialDocumentAttachment = (
  HttpClient,
  financialDocumentNumber,
  attachment
) => {
  const data = new FormData();
  data.append("attachment", attachment);
  let config = {
    method: Config.METHOD_FDST,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${financialDocumentNumber}${Config.ATTACHMENT_URL}`,
    data,
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        dispatch(addFinancialDocumentAttachmentSucess(res.data));
      })
      .catch((error) => {
        dispatch(addFinancialDocumentAttachmentError(error));
      });
  };
};

export const addFinancialDocumentAttachmentSucess = (response) => {
  return {
    type: constants.ADD_FINANCIAL_DOCUMENT_ATTACHMENT_SUCCESS,
    payload: response,
  };
};

export const addFinancialDocumentAttachmentError = (error) => {
  return {
    type: constants.ADD_FINANCIAL_DOCUMENT_ATTACHMENT_ERROR,
    payload: error,
  };
};

// When we want delete attachments from the FD we have to call this method
export const removeFinancialDocumentAttachment = (
  HttpClient,
  financialDocumentNumber,
  attachmentId
) => {
  let config = {
    method: Config.METHOD_DELETE,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${financialDocumentNumber}${Config.ATTACHMENT_URL}${attachmentId}`,
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        dispatch(removeFinancialDocumentAttachmentSucess(res.data));
      })
      .catch((error) => {
        dispatch(removeFinancialDocumentAttachmentError(error));
      });
  };
};

export const removeFinancialDocumentAttachmentSucess = (response) => {
  return {
    type: constants.REMOVE_FINANCIAL_DOCUMENT_ATTACHMENT_SUCCESS,
    payload: response,
  };
};

export const removeFinancialDocumentAttachmentError = (error) => {
  return {
    type: constants.REMOVE_FINANCIAL_DOCUMENT_ATTACHMENT_ERROR,
    payload: error,
  };
};

export const addFinancialDocumentItem = (
  HttpClient,
  Notifications,
  I18nProvider,
  financialDocumentNumber,
  item
) => {
  const config = {
    method: Config.METHOD_POST,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${financialDocumentNumber}${Config.ITEM_URL}`,
    data: mapFormValuesToBackendItem(item),
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        dispatch(addFinancialDocumentItemSucess(res.data, config.data));
      })
      .catch((error) => {
        Notifications.notify(
          I18nProvider.Texti18n(
            "createFDScreen.submit.generic.error.itemMessage"
          ),
          "error",
          "Process",
          15000
        );
        dispatch(addFinancialDocumentItemError(error));
        throw error;
      });
  };
};

export const editFinancialDocumentItem = (
  HttpClient,
  Notifications,
  I18nProvider,
  financialDocumentNumber,
  item
) => {
  const updatedItem = mapFormValuesToBackendItem(item);
  const index = item.index;
  const config = {
    method: Config.METHOD_PUT,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${financialDocumentNumber}${Config.ITEM_URL}${index}`,
    data: updatedItem,
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        dispatch(
          editFinancialDocumentItemSucess(res.data, { ...updatedItem, index })
        );
      })
      .catch((error) => {
        Notifications.notify(
          I18nProvider.Texti18n(
            "createFDScreen.submit.generic.error.itemMessage"
          ),
          "error",
          "Process",
          15000
        );
        dispatch(editFinancialDocumentItemError(error));
        throw error;
      });
  };
};

export const removeFinancialDocumentItem = (
  HttpClient,
  Notifications,
  I18nProvider,
  financialDocumentNumber,
  index
) => {
  const config = {
    method: Config.METHOD_DELETE,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${financialDocumentNumber}${Config.ITEM_URL}${index}`,
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        dispatch(removeFinancialDocumentItemSucess(res.data, index));
      })
      .catch((error) => {
        Notifications.notify(
          I18nProvider.Texti18n(
            "createFDScreen.submit.generic.error.itemMessage"
          ),
          "error",
          "Process",
          15000
        );
        dispatch(removeFinancialDocumentItemError(error));
      });
  };
};

export const addFinancialDocumentItemSucess = (response, item) => {
  return {
    type: constants.ADD_FINANCIAL_DOCUMENT_ITEM_SUCCESS,
    payload: { response, item },
  };
};

export const addFinancialDocumentItemError = (error) => {
  return {
    type: constants.ADD_FINANCIAL_DOCUMENT_ITEM_ERROR,
    payload: error,
  };
};

export const editFinancialDocumentItemSucess = (response, item) => {
  return {
    type: constants.EDIT_FINANCIAL_DOCUMENT_ITEM_SUCCESS,
    payload: { response, item },
  };
};

export const editFinancialDocumentItemError = (error) => {
  return {
    type: constants.EDIT_FINANCIAL_DOCUMENT_ITEM_ERROR,
    payload: error,
  };
};

export const removeFinancialDocumentItemSucess = (response, index) => {
  return {
    type: constants.REMOVE_FINANCIAL_DOCUMENT_ITEM_SUCCESS,
    payload: { response, index },
  };
};

export const removeFinancialDocumentItemError = (error) => {
  return {
    type: constants.REMOVE_FINANCIAL_DOCUMENT_ITEM_ERROR,
    payload: error,
  };
};

// GET FD INFO
export const getFinancialDocumentData = (
  HttpClient,
  Notifications,
  errorMessage,
  financialDocumentNumber,
  dataSource = Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY
) => {
  let config = {
    method: Config.METHOD_GET,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${dataSource}${financialDocumentNumber}`,
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        dispatch(setFinancialDocument(res.data));
      })
      .catch((error) => {
        Notifications.notify(errorMessage, "error", "Process", 5000);
      });
  };
};

// GET FD FROM REPOSITORY
export const getFinancialDocumentDataFromRepository = (
  HttpClient,
  Notifications,
  errorMessage,
  financialDocumentNumber
) => {
  let config = {
    method: Config.METHOD_GET,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_REPOSITORY}${financialDocumentNumber}`,
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        dispatch(setFinancialDocument(res.data));
      })
      .catch((error) => {
        Notifications.notify(errorMessage, "error", "Process", 5000);
      });
  };
};

//GET COPY FD INFO
export const getCopyFinancialDocumentData = (
  HttpClient,
  Notifications,
  repositoryId,
  history
) => {
  let config = {
    method: Config.METHOD_GET,
    url: `${Config.URL_COPY_FINANCIAL_DOCUMENT}/${repositoryId}/1`,
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        dispatch(setFinancialDocument(res.data));
        history.push(`/create/${res.data.jwcontext.id}`);
      })
      .catch((error) => {
        // TODO Notifications.notify
      });
  };
};

export const setFinancialDocumentsList = (newFinancialDocumentList) => {
  return {
    type: constants.SET_FD_LIST_PROCESSES,
    payload: newFinancialDocumentList,
  };
};

export const setFinancialDocumentsDependencies = (dependency) => {
  return {
    type: constants.SET_FINANCIAL_DOCUMENT_DEPENDENCY,
    payload: dependency,
  };
};

export const updateFilter = (newFilter) => ({
  type: constants.UPDATE_FILTER_CRITERIA,
  payload: newFilter,
});

export const updateSorting = (fieldToSort) => ({
  type: constants.UPDATE_SORTING_CRITERIA,
  payload: fieldToSort,
});

export const updateProcessHeaderDraftRequest = (
  httpClient,
  instance,
  data,
  notifications,
  errorMessage
) => {
  const config = {
    method: Config.METHOD_PUT,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${instance}${Config.DRAFT_URL_PART}`,
    data: data,
  };

  return (dispatch) => {
    return httpClient
      .request(config)
      .then((res) => {
        dispatch(setFinancialDocument(res.data));
      })
      .catch((e) => {
        notifications.notify(errorMessage, "error", "Process", 5000);
        throw e;
      });
  };
};

export const updateProcessHeaderCreationRequest = (
  httpClient,
  instance,
  data,
  notifications,
  errorMessage
) => {
  const config = {
    method: Config.METHOD_PUT,
    url: `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${instance}${Config.CREATION_URL_PART}`,
    data: data,
  };

  return (dispatch) => {
    return httpClient
      .request(config)
      .then((res) => {
        dispatch(setFinancialDocument(res.data));
      })
      .catch((e) => {
        notifications.notify(errorMessage, "error", "Process", 5000);
        throw e;
      });
  };
};

export const associatePurchaseOrderRequest = (
  HttpClient,
  instanceId,
  notifications,
  unexpectedErrorMessage,
  poNumber = null
) => {
  const config = {
    method: Config.METHOD_PUT,
    url: poNumber
      ? `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${instanceId}${Config.PURCHASE_ORDER_URL_PART}?number=${poNumber}`
      : `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${instanceId}${Config.PURCHASE_ORDER_URL_PART}`,
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        dispatch(setFinancialDocument(res.data));
      })
      .catch((e) => {
        if (!(e.response && e.response.data)) {
          notifications.notify(
            unexpectedErrorMessage,
            "error",
            "Process",
            5000
          );
        }
        throw e;
      });
  };
};

export const associateFinancialDocumentRequest = (
  HttpClient,
  instanceId,
  notifications,
  unexpectedErrorMessage,
  fdNumber = null
) => {
  const config = {
    method: Config.METHOD_PUT,
    url: fdNumber
      ? `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${instanceId}${Config.FINANCIAL_DOCUMENT_URL_PART}?number=${fdNumber}`
      : `${Config.FINANCIAL_DOCUMENT_BASE_URL}${Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY}${instanceId}${Config.FINANCIAL_DOCUMENT_URL_PART}`,
  };
  return (dispatch) => {
    return HttpClient.request(config)
      .then((res) => {
        dispatch(setFinancialDocument(res.data));
      })
      .catch((e) => {
        if (!(e.response && e.response.data)) {
          notifications.notify(
            unexpectedErrorMessage,
            "error",
            "Process",
            5000
          );
        }
        throw e;
      });
  };
};
