import React, { Component } from "react";
import withContainerWrapper from "../../components/ContainerWrapper";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import FDListScreenRender from "./FDListScreenRender";
import { FDListHeaders } from "./TableHeaders";
import AppRoutes from "../../routes/Routes";
import { getFDListProcesses, updateSorting } from "../../actions";
import PropTypes from "prop-types";

class FDListScreen extends Component {
  state = {
    expandFilter: false,
    tableApi: undefined,
    allDependenciesResolved: false,
    allowCreateFinancialDocument: false,
  };

  loadAllDependencies = async () => {
    const { EntitlementsVerifier, Loading } = this.props;

    Loading.request("setLoading", "JOURNEY");
    const allowCreateFinancialDocument = await EntitlementsVerifier.verify(
      "ONEY_FD:CREATE_FD"
    );
    this.setState({
      allDependenciesResolved: true,
      allowCreateFinancialDocument,
    });
    Loading.request("stopLoading");
  };

  componentDidMount() {
    const { searchCriteria } = this.props;
    this.getFinancialDocumentsFromServer(searchCriteria);
    this.loadAllDependencies();
  }

  componentDidUpdate(prevProps, prevState) {
    const { searchCriteria } = this.props;
    if (this.state.tableApi && prevState.tableApi !== this.state.tableApi) {
      this.state.tableApi.setSortedFields([
        {
          key: searchCriteria.sortCriteria.orderField,
          direction: searchCriteria.sortCriteria.orderType,
        },
      ]);
    }
  }

  /**
   * Logic to navigate between containers.
   */

  navigateToPage = (page, payload) => {
    const { history } = this.props;
    switch (page) {
      case "createFD":
        history.push(AppRoutes.CREATE.path);
        break;
      case "detailsFD":
        history.push(AppRoutes.DETAILS.path + payload);
        break;
      default:
        break;
    }
  };

  /**
   * toggle the expand filter
   */
  handleExpandFilterClick = () => {
    this.setState({ expandFilter: !this.state.expandFilter });
  };

  /**
   * To navigate to the PO details screen
   */
  handleRowClick = (row) => {
    this.navigateToPage("detailsFD", row.id);
  };

  /**
   * Makes a request to the server to fetch the PO data
   * the service must return the total existent records.
   */
  getFinancialDocumentsFromServer = (searchCriteria) => {
    const { getFDListProcesses, HttpClient } = this.props;
    getFDListProcesses(HttpClient, searchCriteria);
  };

  /**
   * the onSort method returned by the custom table as
   * has the follow arguments headerkey, directionResponseOk, directionResponseError
   * direction can be asc or desc
   */
  sortHandler = (headerKey, directionResponseOk, resolve, error) => {
    const { updateSorting, searchCriteria } = this.props;
    const sorting = { ...searchCriteria.sortCriteria };
    sorting.orderField = headerKey;
    // hack to solve problem with arrow sort.
    if (
      searchCriteria.sortCriteria.orderType === "desc" &&
      directionResponseOk === "desc"
    ) {
      sorting.orderType = "asc";
      error();
    } else {
      sorting.orderType = directionResponseOk;
      resolve();
    }
    updateSorting({ ...sorting });
    resolve();
    this.getFinancialDocumentsFromServer({
      ...searchCriteria,
      sortCriteria: { ...sorting },
    });
  };

  setTableApi = (api) => {
    this.setState({ tableApi: api });
  };
  render() {
    const {
      I18nProvider,
      financialDocumentsList,
      totalFinancialDocuments,
      numberOfRecords,
      refData,
      suppliers,
    } = this.props;
    const {
      allDependenciesResolved,
      allowCreateFinancialDocument,
    } = this.state;

    if (!allDependenciesResolved) {
      return null;
    }
    return (
      <FDListScreenRender
        I18nProvider={I18nProvider}
        expandFilter={this.state.expandFilter}
        navigateToPage={this.navigateToPage}
        onRowClick={this.handleRowClick}
        onExpandFilterClick={this.handleExpandFilterClick}
        tableHeaders={FDListHeaders(I18nProvider)}
        onFilterClick={this.getFinancialDocumentsFromServer}
        sortHandler={this.sortHandler}
        financialDocumentsList={financialDocumentsList}
        totalFinancialDocuments={totalFinancialDocuments}
        numberOfRecords={numberOfRecords}
        refData={{ referenceData: refData, suppliers }}
        requestFilterData={this.getFinancialDocumentsFromServer}
        setTableApi={this.setTableApi}
        tableApi={this.state.tableApi}
        allowCreateFinancialDocument={allowCreateFinancialDocument}
      />
    );
  }
}

FDListScreen.propTypes = {
  I18nProvider: PropTypes.object,
  match: PropTypes.object,
  financialDocumentData: PropTypes.object,
  Loading: PropTypes.object,
  refData: PropTypes.object,
  history: PropTypes.object,
  Notifications: PropTypes.object,
  searchCriteria: PropTypes.object,
  HttpClient: PropTypes.object,
  EntitlementsVerifier: PropTypes.object,
  getFDListProcesses: PropTypes.func,
  updateSorting: PropTypes.func,
  timelineData: PropTypes.any,
  financialDocumentsList: PropTypes.array,
  totalFinancialDocuments: PropTypes.number,
  numberOfRecords: PropTypes.number,
  suppliers: PropTypes.array,
};

const mapStateToProps = ({
  journey: {
    services: { I18nProvider, HttpClient, Loading, EntitlementsVerifier },
  },
  i18n,
  financialDocumentReducer: {
    financialDocumentsList,
    searchCriteria,
    totalFinancialDocuments,
    numberOfRecords,
  },
  refDataReducer: { refData, suppliers },
}) => ({
  I18nProvider,
  i18n,
  HttpClient,
  financialDocumentsList,
  searchCriteria,
  totalFinancialDocuments,
  numberOfRecords,
  refData,
  suppliers,
  Loading,
  EntitlementsVerifier,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getFDListProcesses,
      updateSorting,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withContainerWrapper(FDListScreen));
