import { getRefDataOptionFromValue } from "./refDataUtils";
import { dateFormatter } from "./infoFDUtils";

const parsePercentage = (value) => (value ? parseFloat(value) / 100 : 0);

export const formatToDatePickerString = (dateString) => {
  if (dateString) {
    const dateToTransform = new Date(dateString);
    return dateToTransform.toISOString().slice(0, 10);
  }
};

export const mapValuesToFinancialDocument = (values, financialDocumentId) => ({
  financialDocumentNumber: financialDocumentId,
  version: 0,
  type: values.type ? values.type.value : null,
  supplierCode: values.supplierCode ? values.supplierCode.value : null,
  description: values.description ? values.description : null,
  departmentCode: values.department ? values.department.value : null,
  projectCode: values.project ? values.project.value : null,
  internalOrderCode: values.internalOrder ? values.internalOrder.value : null,
  creationDate: new Date(values.creationDate) || null,
  expirationDate: new Date(values.limitDate) || undefined,
});

export const mapFinancialDocumentToValues = (
  financialDocumentData,
  lovs,
  suppliers
) => ({
  type: financialDocumentData.type
    ? getRefDataOptionFromValue(
        lovs.financialDocumentTypes,
        financialDocumentData.type || "-"
      )
    : undefined,
  supplierCode: getSupplierField(
    suppliers,
    "code",
    financialDocumentData.supplierCode
  ),
  supplierTaxNumber: getSupplierField(
    suppliers,
    "tax",
    financialDocumentData.supplierCode
  ),
  supplierName: getSupplierField(
    suppliers,
    "name",
    financialDocumentData.supplierCode
  ),
  valueWOVat: financialDocumentData.totalWithoutTax,
  description: financialDocumentData.description,
  department: getRefDataOptionFromValue(
    lovs.departments,
    financialDocumentData.departmentCode || "-"
  ),
  project: financialDocumentData.projectCode
    ? getRefDataOptionFromValue(
        lovs.projects,
        financialDocumentData.projectCode || "-"
      )
    : undefined,
  internalOrder: financialDocumentData.internalOrderCode
    ? getRefDataOptionFromValue(
        lovs.internalOrders,
        financialDocumentData.internalOrderCode || "-"
      )
    : undefined,
  creationDate: dateFormatter(financialDocumentData.creationDate),
  limitDate: financialDocumentData.expirationDate
    ? formatToDatePickerString(financialDocumentData.expirationDate)
    : undefined,
});

export const mapItemsToValues = (financialDocumentData, lovs) => {
  return financialDocumentData.items.length > 0
    ? financialDocumentData.items.map((financialDocumentItem) => {
        let item;
        let itemType;
        let costCenter;
        let fixedAsset = false;

        switch (financialDocumentData.financialDocumentHeader.type) {
          case "CAPEX":
            item = getRefDataOptionFromValue(
              loadFixedAssets(lovs.assets, financialDocumentItem.code),
              financialDocumentItem.code || "-"
            );
            itemType = "-";
            fixedAsset = true;
            break;
          case "CONTRACT":
          case "OPEX":
            item = getRefDataOptionFromValue(
              loadItems(lovs.items, financialDocumentItem.code),
              financialDocumentItem.code || "-"
            );
            if (item !== "-") {
              itemType =
                getRefDataOptionFromValue(lovs.itemTypes, item.itemTypeCode) ||
                "-";
            } else {
              itemType = "-";
            }
            costCenter = lovs.costCenters.find(
              (c) => c.code === financialDocumentItem.costCenterCode
            );
            if (!costCenter) {
              costCenter = lovs.projects.find(
                (c) => c.code === financialDocumentItem.costCenterCode
              );
            }
            break;
          default:
            return {};
        }

        return {
          fixedAsset: fixedAsset,
          itemType: itemType,
          item: item,
          index: financialDocumentItem.index,
          comment: financialDocumentItem.comment,
          startDate:
            new Date(financialDocumentItem.startDate)
              .toISOString()
              .slice(0, 10) || undefined,
          endDate:
            new Date(financialDocumentItem.endDate)
              .toISOString()
              .slice(0, 10) || undefined,
          description: financialDocumentItem.description,
          tax:
            getRefDataOptionFromValue(
              lovs.tax,
              financialDocumentItem.taxCode
            ) || "-",
          quantity: financialDocumentItem.amount,
          unitValue: financialDocumentItem.unitPriceWithoutTax,
          costCenter: costCenter || "-",
          unitValueWoVAT:
            financialDocumentItem.amount *
            financialDocumentItem.unitPriceWithoutTax,
          totalAmountWithVAT:
            financialDocumentItem.amount *
            financialDocumentItem.unitPriceWithoutTax *
            (1 +
              parsePercentage(
                getRefDataOptionFromValue(
                  lovs.tax,
                  financialDocumentItem.taxCode
                ).taxPercentage || 0
              ) /
                100),
        };
      })
    : [];
};

//TODO Refactor remove this method soon as possible
const loadFixedAssets = (assets, inputValue) => {
  if (assets) {
    const newAssets = inputValue
      ? assets.filter((asset) => asset.code.includes(inputValue))
      : assets;
    return newAssets.map((asset) => ({
      value: asset.value,
      description: asset.description,
      name: asset.label,
      label: `${asset.value} - ${asset.label}`,
    }));
  }
};

//TODO Refactor remove this method soon as possible
const loadItems = (items, inputValue) => {
  if (items) {
    const newItems = inputValue
      ? items.filter((item) => item.code.includes(inputValue))
      : items;
    return newItems.map((item) => ({
      value: item.code,
      description: item.description,
      itemTypeCode: item.itemTypeCode,
      departmentCode: item.departmentCode,
      defaultTaxCode: item.defaultTaxCode,
      defaultCostCenterCode: item.defaultCostCenterCode,
      label: item.code,
      code: item.code,
    }));
  }
};

//TODO Refactor remove this method soon as possible
const loadSupplierCodeOptions = (suppliers, inputValue) => {
  if (suppliers) {
    const newSuppliers = inputValue
      ? suppliers.filter((supplier) => supplier.id.includes(inputValue))
      : suppliers;
    return newSuppliers.map((supplier) => ({
      value: supplier.id,
      label: supplier.id,
      code: supplier.id,
      name: supplier.name,
      tax: supplier.tax,
    }));
  }
};

//TODO Refactor remove this method soon as possible
const loadSupplierNameOptions = (suppliers, inputValue) => {
  if (suppliers) {
    const newSuppliers = inputValue
      ? suppliers.filter((supplier) => supplier.name.includes(inputValue))
      : suppliers;
    return newSuppliers.map((supplier) => ({
      value: supplier.name,
      label: supplier.name,
      code: supplier.id,
      name: supplier.name,
      tax: supplier.tax,
    }));
  }
};

//TODO Refactor remove this method soon as possible
const loadSupplierTaxOptions = (suppliers, inputValue) => {
  if (suppliers) {
    const newSuppliers = inputValue
      ? suppliers.filter((supplier) => supplier.tax.includes(inputValue))
      : suppliers;

    return newSuppliers.map((supplier) => ({
      value: supplier.tax,
      label: supplier.tax,
      code: supplier.id,
      name: supplier.name,
      tax: supplier.tax,
    }));
  }
};

const getSupplierField = (suppliers, fieldName, supplierCode) => {
  return (
    suppliers.length > 0 &&
    suppliers.find((supplier) => {
      return (
        supplier.code === supplierCode && {
          value: supplier[fieldName],
          label: supplier[fieldName],
        }
      );
    })
  );
};

export const generateId = (isTemporary) => {
  const randomNumber = Math.random().toString().substr(2, 9);
  return isTemporary ? "temp_" + randomNumber : randomNumber;
};

export const loadOptionsUtil = {
  loadFixedAssets,
  loadItems,
  loadSupplierCodeOptions,
  loadSupplierNameOptions,
  loadSupplierTaxOptions,
};
