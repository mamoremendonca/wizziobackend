import React from "react";
import Dialog from "@ui-lib/core/Dialog";

import DialogTitle from "@ui-lib/core/DialogTitle";
import DialogContent from "@ui-lib/core/DialogContent";
import DialogActions from "@ui-lib/core/DialogActions";
import Button from "@ui-lib/core/Button";
import Icon from "@ui-lib/core/Icon";

import { withStyles } from "@ui-lib/core/styles";
import { styles, ComponentName } from "./DialogStyles";

import * as constants from "../../constants";
import PropTypes from "prop-types";

const ItemDialog = (props) => {
  const {
    open,
    close,
    children,
    title,
    save,
    valid,
    handleDelete,
    itemIndex,
    classes,
    action,
    I18nProvider,
  } = props;
  return (
    <Dialog
      open={open}
      onClose={close}
      fullWidth={true}
      maxWidth={"l"}
      container={() => document.getElementById("grid-form-df")}
      className={classes.dialogContainer}
      BackdropProps={{ className: classes.dialogBackdrop }}
    >
      <DialogTitle>
        <div className={classes.headerContainer}>
          <div>{title}</div>
          <Icon className={classes.Icon} onClick={close}>
            close
          </Icon>
        </div>
      </DialogTitle>
      <DialogContent style={{ overflowY: "visible" }}>{children}</DialogContent>
      <DialogActions>
        {action === constants.ITEM_DIALOG_ACTIONS.EDIT_ITEM && (
          <Button
            type="submit"
            onClick={() => {
              handleDelete(itemIndex);
              //close();
            }}
          >
            {I18nProvider.Labeli18n(
              "createFDScreen.form.delete_button.description"
            )}
          </Button>
        )}
        <Button
          type="submit"
          variant="contained"
          color="primary"
          onClick={save}
          disabled={!valid}
        >
          {I18nProvider.Labeli18n(
            "createFDScreen.form.save_button.description"
          )}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

ItemDialog.propTypes = {
  I18nProvider: PropTypes.object,
  classes: PropTypes.object,
  children: PropTypes.array,
  save: PropTypes.func,
  open: PropTypes.func,
  close: PropTypes.func,
  title: PropTypes.string,
  valid: PropTypes.bool,
  handleDelete: PropTypes.func,
  itemIndex: PropTypes.number,
  action: PropTypes.string,
};

export default withStyles(styles, { name: ComponentName })(ItemDialog);
