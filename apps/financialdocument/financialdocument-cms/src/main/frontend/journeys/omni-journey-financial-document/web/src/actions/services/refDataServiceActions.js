import * as constants from "../../constants";
import { Config } from "../../config";
import { setRefData } from "../../actions";
import { mapBackendDTO, mapSupplierDTO } from "../../utils";

export const getAllRefData = (services) => {
  const { Notifications, HttpClient, I18nProvider } = services;

  const oneySupplierReferenceDataConfig = {
    method: Config.METHOD_POST,
    url: Config.SUPPLIER_URL,
    data: {},
  };

  const oneyReferenceDataConfig = {
    method: Config.METHOD_POST,
    url: Config.ONEY_REFERENCE_LISTS_URL,
    data: constants.ONEY_REFERENCE_DATA_BODY,
  };

  const financialDocumentReferenceDataConfig = {
    method: Config.METHOD_POST,
    url: Config.FINANCIAL_DOCUMENT_REFERENCE_LISTS_URL,
    data: constants.FINANCIAL_DOCUMENT_REFERENCE_DATA_BODY,
  };
  return (dispatch) => {
    return Promise.all([
      HttpClient.request(oneySupplierReferenceDataConfig),
      HttpClient.request(oneyReferenceDataConfig),
      HttpClient.request(financialDocumentReferenceDataConfig),
    ])
      .then((res) => {
        const suppliersReferenceData = res[0].data;
        const oneyReferenceData = res[1].data;
        const financialDocumentReferenceData = res[2].data;
        const refData = {};

        refData.items = mapBackendDTO(oneyReferenceData.ITEM, I18nProvider);
        refData.internalOrders = mapBackendDTO(
          oneyReferenceData.INTERNAL_ORDER,
          I18nProvider
        );
        refData.projects = mapBackendDTO(
          oneyReferenceData.PROJECT,
          I18nProvider
        );
        refData.assets = mapBackendDTO(oneyReferenceData.ASSET, I18nProvider);
        refData.costCenters = mapBackendDTO(
          oneyReferenceData.COST_CENTER,
          I18nProvider
        );
        refData.tax = mapBackendDTO(oneyReferenceData.TAX, I18nProvider);
        refData.documentType = mapBackendDTO(
          oneyReferenceData.DOCUMENT_TYPE,
          I18nProvider
        );
        refData.itemTypes = mapBackendDTO(
          oneyReferenceData.ITEM_TYPE,
          I18nProvider
        );
        refData.retentionCodes = mapBackendDTO(
          oneyReferenceData.RETENTION_CODE,
          I18nProvider
        );
        refData.allDepartments = mapBackendDTO(
          oneyReferenceData.DEPARTMENT,
          I18nProvider
        );
        refData.groups = mapBackendDTO(oneyReferenceData.GROUP, I18nProvider);

        refData.financialDocumentTypes = mapBackendDTO(
          financialDocumentReferenceData.FINANCIAL_DOCUMENT_TYPE,
          I18nProvider
        );
        refData.departments = mapBackendDTO(
          financialDocumentReferenceData.DEPARTMENT,
          I18nProvider
        );
        refData.status = mapBackendDTO(
          financialDocumentReferenceData.FINANCIAL_DOCUMENT_STATUS,
          I18nProvider
        );
        refData.suppliers = mapSupplierDTO(suppliersReferenceData);

        dispatch(setRefData(refData));
      })
      .catch((error) => {
        Notifications.notify(
          I18nProvider.Texti18n(
            "services.get.ref_data.error.message",
            "error",
            "Process",
            5000
          )
        );
      });
  };
};
