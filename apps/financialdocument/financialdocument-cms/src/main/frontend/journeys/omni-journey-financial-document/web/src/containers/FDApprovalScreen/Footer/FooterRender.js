import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { styles, componentName } from "./FooterStyles";
import ConfirmationDialog from "../../../components/Dialog/ConfirmationDialog";
import Button from "@ui-lib/core/Button";
import TextField from "@ui-lib/core/TextField";
import Radio from "@ui-lib/core/Radio";
import RadioGroup from "@ui-lib/core/RadioGroup";
import FormControlLabel from "@ui-lib/core/FormControlLabel";
import FormHelperText from "@ui-lib/core/FormHelperText";
import PropTypes from "prop-types";

const FooterRender = ({
  classes,
  I18nProvider,
  availableActions,
  confirmationDialogOptions,
  formValues,
  formErrors,
  formSetters,
  disableSubmit,
  onSubmit,
  onCancel,
  isJustificationRequired,
}) => {
  return (
    <div className={classes.root}>
      {availableActions && availableActions.length > 0 && (
        <div className={classes.formFieldSet}>
          <div className={classes.choiceField}>
            <RadioGroup
              required
              onChange={formSetters.setChoice}
              value={formValues.choice}
              className={classes.buttonGroupContainer}
              name={"handleChangeChoice"}
              aria-label="choice"
            >
              {availableActions.map((action) => (
                <FormControlLabel
                  key={action}
                  value={action}
                  control={
                    <Radio
                      className={
                        (formErrors.choice && classes.radioButton) || undefined
                      }
                      color="primary"
                    />
                  }
                  label={I18nProvider.Labeli18n(
                    `fdApprovalScreen.footer.${action}.radio.option.label`
                  )}
                />
              ))}
            </RadioGroup>
            {formErrors.choice && (
              <FormHelperText className={classes.radioButtonErrorText}>
                {formErrors.choice}
              </FormHelperText>
            )}
          </div>
          <div className={classes.justificationField}>
            <TextField
              label={I18nProvider.Labeli18n(
                "fdApprovalScreen.footer.justification.field.label"
              )}
              required={formValues.choice && isJustificationRequired()}
              inputProps={{
                onBlur: formSetters.setJustificationOnBlur,
              }}
              value={formValues.justification}
              onChange={formSetters.setJustification}
              fullWidth
              InputLabelProps={{ shrink: true }}
              helperText={
                formErrors.justification ? formErrors.justification : undefined
              }
              error={!!formErrors.justification}
            />
          </div>
        </div>
      )}
      <div className={classes.formActions}>
        <Button onClick={onCancel} className={classes.cancelButton}>
          {I18nProvider.Labeli18n(
            "fdApprovalScreen.footer.cancel.button.label"
          )}
        </Button>
        <Button
          disabled={disableSubmit}
          color="primary"
          variant="contained"
          onClick={onSubmit}
        >
          {I18nProvider.Labeli18n(
            "fdApprovalScreen.footer.submit.button.label"
          )}
        </Button>
      </div>
      <ConfirmationDialog
        open={confirmationDialogOptions.open}
        title={confirmationDialogOptions.title}
        onClose={confirmationDialogOptions.onClose}
        cancelButtonLabel={I18nProvider.Labeli18n(
          "fdApprovalScreen.footer.confirmation_dialog.cancel.button.label"
        )}
        acceptButtonLabel={I18nProvider.Labeli18n(
          "fdApprovalScreen.footer.confirmation_dialog.accept.button.label"
        )}
      />
    </div>
  );
};

FooterRender.propTypes = {
  classes: PropTypes.object,
  I18nProvider: PropTypes.object,
  availableActions: PropTypes.object,
  formValues: PropTypes.object,
  formErrors: PropTypes.object,
  confirmationDialogOptions: PropTypes.object,
  formSetters: PropTypes.object,
  disableSubmit: PropTypes.func,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  isJustificationRequired: PropTypes.bool,
};

export default withStyles(styles, { componentName })(FooterRender);
