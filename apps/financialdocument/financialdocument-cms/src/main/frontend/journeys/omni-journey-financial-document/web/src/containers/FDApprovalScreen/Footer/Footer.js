import React, { Component } from "react";
import { connect } from "react-redux";
import FooterRender from "./FooterRender";
import { submitApprovalDecisionRequest } from "../../../actions/services/financialDocumentServiceActions";
import { Config } from "../../../config";
import { APPROVAL_DECISIONS } from "../../../constants/FDConstants";
import { clearFinancialDocument } from "../../../actions";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";

class Footer extends Component {
  state = {
    formValues: {
      choice: undefined,
      justification: undefined,
    },
    formErrors: { choice: undefined, justification: undefined },
    confirmationDialogOptions: {
      open: false,
      title: undefined,
      onclose: undefined,
    },
  };

  componentDidUpdate(_, prevState) {
    if (prevState.formValues !== this.state.formValues) {
      this.validate();
    }
  }

  setChoice = ({ target: { value } }) => {
    this.setState({ formValues: { ...this.state.formValues, choice: value } });
  };

  setJustification = ({ target: { value } }) => {
    this.setState({
      formValues: { ...this.state.formValues, justification: value },
    });
  };

  setJustificationOnBlur = ({ target: { value } }) => {
    if (this.state.justification === undefined) {
      this.setState({
        formValues: { ...this.state.formValues, justification: value },
      });
    }
  };

  isJustificationRequired = () => {
    const {
      formValues: { choice },
    } = this.state;
    return choice && choice !== APPROVAL_DECISIONS.APPROVE;
  };

  validate = () => {
    const errors = {};
    const { I18nProvider } = this.props;
    const {
      formValues: { justification, choice },
    } = this.state;
    if (!justification && this.isJustificationRequired()) {
      if (justification === undefined) {
        errors.justification = undefined;
      } else {
        errors.justification = I18nProvider.Texti18n(
          "validator.required.field.error.message"
        );
      }
    }

    if (!choice) {
      errors.choice = I18nProvider.Labeli18n(
        "fdApprovalScreen.footer.required.choice.error.message"
      );
    }
    this.setState({ formErrors: errors });
  };

  /**
   * Check if there is some attachment that is being uploaded or removed
   * Get the first TO_APPROVE" step and check if exists some loading attachment
   * by default returns false.
   * @returns false if there is no attachment loading or no "TO_APPROVE" step
   */
  isSomeAttachmentLoading = () => {
    const { timelineData } = this.props;
    //get first step "TO_APPROVE"
    const toApproveStep = timelineData.find(
      (step) => step.type === "TO_APPROVE"
    );
    if (toApproveStep) {
      return toApproveStep.attachments.some(
        (attachment) => attachment.isLoading
      );
    }
    return false;
  };

  disableSubmitButton = () => {
    return (
      this.props.isTimelineDataLoading ||
      this.isSomeAttachmentLoading() ||
      Object.keys(this.state.formErrors).length > 0
    );
  };

  handleOnCloseCancelChanges = (decision) => {
    if (decision) {
      this.navigateToPreviousScreen();
    }
    this.setState({
      confirmationDialogOptions: {
        ...this.state.confirmationDialogOptions,
        open: false,
      },
    });
  };

  handleOnCloseSubmitChoice = async (decision) => {
    const {
      formValues: { choice, justification },
    } = this.state;
    const {
      I18nProvider,
      Notifications,
      HttpClient,
      Loading,
      financialDocumentData: {
        jwcontext: { id },
      },
    } = this.props;
    if (decision) {
      if (
        Object.keys(Config.APPROVAL_ACTIONS_URL_PART).some(
          (action) => action === choice
        )
      ) {
        try {
          await Loading.request("setLoading", "JOURNEY");
          await submitApprovalDecisionRequest(
            HttpClient,
            id,
            justification,
            Config.APPROVAL_ACTIONS_URL_PART[choice]
          );
          this.navigateToPreviousScreen();
        } catch {
          Notifications.notify(
            I18nProvider.Texti18n(
              "services.submit.approval.choice.error.message"
            ),
            "error",
            "Process",
            5000
          );
        } finally {
          Loading.request("stopLoading");
        }
      } else {
        Notifications.notify(
          "Approval action not implemented",
          "warning",
          "Process",
          5000
        );
      }
    }

    this.setState({
      confirmationDialogOptions: {
        ...this.state.confirmationDialogOptions,
        open: false,
      },
    });
  };

  navigateToPreviousScreen = () => {
    const { history, JourneyActions, JourneySettings } = this.props;

    if (history.length === 1) {
      JourneyActions.closeJourney(JourneySettings.instance_id);
    } else {
      history.push("/");
      clearFinancialDocument();
    }
  };

  handleCancel = () => {
    const { I18nProvider } = this.props;
    this.setState({
      confirmationDialogOptions: {
        open: true,
        title: I18nProvider.Labeli18n(
          "fdApprovalScreen.footer.confirmation_dialog.cancel.title"
        ),
        onClose: this.handleOnCloseCancelChanges,
      },
    });
  };

  handleSubmit = () => {
    const { I18nProvider } = this.props;
    this.setState({
      confirmationDialogOptions: {
        open: true,
        title: I18nProvider.Labeli18n(
          "fdApprovalScreen.footer.confirmation_dialog.submit.title"
        ),
        onClose: this.handleOnCloseSubmitChoice,
      },
    });
  };

  render() {
    const { financialDocumentData, I18nProvider } = this.props;
    const { formValues, formErrors, confirmationDialogOptions } = this.state;
    return (
      <FooterRender
        I18nProvider={I18nProvider}
        availableActions={financialDocumentData.workFlowAvailableActions}
        formValues={formValues}
        formErrors={formErrors}
        formSetters={{
          setChoice: this.setChoice,
          setJustification: this.setJustification,
          setJustificationOnBlur: this.setJustificationOnBlur,
        }}
        disableSubmit={this.disableSubmitButton()}
        confirmationDialogOptions={confirmationDialogOptions}
        onSubmit={this.handleSubmit}
        onCancel={this.handleCancel}
        isJustificationRequired={this.isJustificationRequired}
      />
    );
  }
}

Footer.propTypes = {
  classes: PropTypes.object,
  I18nProvider: PropTypes.object,
  timelineData: PropTypes.any,
  isTimelineDataLoading: PropTypes.bool,
  HttpClient: PropTypes.object,
  Notifications: PropTypes.object,
  Loading: PropTypes.object,
  financialDocumentData: PropTypes.object,
  JourneyActions: PropTypes.object,
  JourneySettings: PropTypes.object,
  clearFinancialDocument: PropTypes.func,
  history: PropTypes.object,
};

const mapStateToProps = ({
  journey: {
    services: {
      HttpClient,
      I18nProvider,
      Notifications,
      Loading,
      JourneyActions,
    },
    settings,
  },
  financialDocumentReducer: { financialDocumentData, timelineData },
}) => ({
  HttpClient,
  I18nProvider,
  Notifications,
  Loading,
  financialDocumentData,
  timelineData,
  JourneySettings: settings,
  JourneyActions,
});

const mapDispatchToProps = (dispatch) => {
  bindActionCreators(
    {
      clearFinancialDocument,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
