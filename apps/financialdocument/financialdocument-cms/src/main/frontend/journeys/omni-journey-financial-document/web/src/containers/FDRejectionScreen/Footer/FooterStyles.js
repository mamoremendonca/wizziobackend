export const componentName = "FooterStyles";

export const styles = (theme) => ({
  root: { display: "flex", justifyContent: "flex-end" },
});
