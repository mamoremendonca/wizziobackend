import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import styles from "./styles";

import Icon from "@ui-lib/core/Icon";
import Typography from "@ui-lib/core/Typography";
import CircularProgress from "@ui-lib/core/CircularProgress";
import Popover from "@ui-lib/core/Popover";
import PropTypes from "prop-types";

const FileDeleteIcon = (props) => {
  const { classes, itemClick } = props;
  return (
    <Icon className={classes.DeleteIcon} onClick={itemClick}>
      clear
    </Icon>
  );
};

FileDeleteIcon.propTypes = {
  classes: PropTypes.object,
  itemClick: PropTypes.func,
};

const FileChip = (props) => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  const {
    classes,
    name,
    key,
    onDelete,
    documentClass,
    I18nProvider,
    isLoading,
    indexLoading,
    onDownload,
    canDelete,
  } = props;
  return (
    <div className={classes.root} tabIndex={key} key={key}>
      <div
        style={{ display: "flex", flexDirection: "row", alignItems: "center" }}
        onMouseEnter={handlePopoverOpen}
        onMouseLeave={handlePopoverClose}
      >
        <div className={classes.chipParent}>
          <div className={classes.Chip}>
            <img src="./Fill-1.svg" alt="File" />
            <Typography className={classes.chipText} onClick={onDownload}>
              {name}
            </Typography>
          </div>
          {documentClass && (
            <div>
              <Typography className={classes.documentClassText}>
                {I18nProvider.Texti18n(`${documentClass}_ATTACHMENT`)}
              </Typography>
            </div>
          )}
          {isLoading.index === indexLoading && isLoading.isLoadingAttachment ? (
            <CircularProgress
              value={true}
              color="primary"
              className={classes.progress}
            />
          ) : (
            onDelete &&
            canDelete && (
              <div className={classes.deleteIconContainer}>
                <FileDeleteIcon itemClick={onDelete} classes={classes} />
              </div>
            )
          )}
        </div>
      </div>
      {name.length > 20 && (
        <Popover
          id="mouse-over-popover"
          className={classes.popover}
          classes={{
            paper: classes.paper,
          }}
          open={open}
          anchorEl={anchorEl}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "left",
          }}
          onClose={handlePopoverClose}
          disableRestoreFocus
        >
          <Typography>{name}</Typography>
        </Popover>
      )}
    </div>
  );
};

FileChip.propTypes = {
  classes: PropTypes.object,
  I18nProvider: PropTypes.object,
  name: PropTypes.string,
  isLoading: PropTypes.bool,
  onDownload: PropTypes.func,
  onDelete: PropTypes.func,
  key: PropTypes.string,
  documentClass: PropTypes.string,
  indexLoading: PropTypes.bool,
  canDelete: PropTypes.bool,
};

export default withStyles(styles)(FileChip);
