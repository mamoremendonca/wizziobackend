import React, { Component } from "react";
import FooterRender from "./FooterRender";
import PropTypes from "prop-types";

class Footer extends Component {
  render() {
    const {
      financialDocumentHeader,
      I18nProvider,
      handleBackClick,
    } = this.props;
    return (
      <FooterRender
        I18nProvider={I18nProvider}
        financialDocumentHeader={financialDocumentHeader}
        handleBackClick={handleBackClick}
      />
    );
  }
}

Footer.propTypes = {
  financialDocumentHeader: PropTypes.object,
  I18nProvider: PropTypes.object,
  handleBackClick: PropTypes.func,
};

export default Footer;
