import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import withContainerWrapper from "../../components/ContainerWrapper";
import FDApprovalScreenRender from "./FDApprovalScreenRender";
import { getFinancialDocumentData, getTimelineData } from "../../actions";
import { calculateTotalItemsValue } from "../../utils/infoFDUtils";
import PropTypes from "prop-types";

/**
 * Template Screen to the Approval Process
 * Here he just update the GlobalState setting the FinancialDocument Reducer and mount the screen.
 * This screen is always used by the Financial Document Process Continuity
 */
class FDApprovalScreen extends Component {
  state = {
    allDependenciesResolved: false,
    externalLoadingDependencies: false,
    canSubmit: true,
  };

  componentDidMount() {
    const {
      match,
      HttpClient,
      Notifications,
      getFinancialDocumentData,
      Loading,
      I18nProvider,
      getTimelineData,
    } = this.props;
    Loading.request("setLoading", "JOURNEY").then(() => {
      if (match && match.params && match.params.id) {
        getFinancialDocumentData(
          HttpClient,
          Notifications,
          I18nProvider.Texti18n(
            "services.get.financial_document.error.message"
          ),
          match.params.id
        );
        getTimelineData(
          HttpClient,
          Notifications,
          match.params.id,
          I18nProvider.Texti18n("service.get_timeline.error.message")
        );
      }
    });
  }

  componentDidUpdate() {
    const { financialDocumentData, timelineData, Loading } = this.props;
    const { allDependenciesResolved } = this.state;
    if (financialDocumentData && timelineData && !allDependenciesResolved) {
      //remove loading show screen
      this.setState({ allDependenciesResolved: true }, () =>
        Loading.request("stopLoading")
      );
    }
  }

  setLoadingDependencies = (dependency) => {
    // this.setState({
    //   externalLoadingDependencies: {
    //     ...this.state.externalLoadingDependencies,
    //   },
    // });
    //TODO set loading dependencies
    //TODO set canSubmit depending of the resolution status of all dependencies.
  };

  render() {
    const { allDependenciesResolved } = this.state;
    const {
      financialDocumentData,
      I18nProvider,
      refData,
      history,
    } = this.props;
    if (allDependenciesResolved) {
      return (
        <FDApprovalScreenRender
          I18nProvider={I18nProvider}
          financialDocumentData={financialDocumentData}
          history={history}
          refData={refData}
          totalItemsValue={calculateTotalItemsValue(
            refData,
            financialDocumentData
          )}
          setLoadingDependencies={this.setLoadingDependencies}
        />
      );
    }
    return null;
  }
}

FDApprovalScreen.propTypes = {
  match: PropTypes.object,
  refData: PropTypes.object,
  Loading: PropTypes.object,
  I18nProvider: PropTypes.object,
  timelineData: PropTypes.any,
  HttpClient: PropTypes.object,
  financialDocumentData: PropTypes.object,
  Notifications: PropTypes.object,
  getFinancialDocumentData: PropTypes.func,
  getTimelineData: PropTypes.func,
  history: PropTypes.object,
};

const mapStateToProps = ({
  journey: {
    services: { I18nProvider, Loading, Notifications, HttpClient },
  },
  financialDocumentReducer: { financialDocumentData, timelineData },
  refDataReducer: { refData },
}) => ({
  I18nProvider,
  Loading,
  Notifications,
  HttpClient,
  financialDocumentData,
  refData,
  timelineData,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ getFinancialDocumentData, getTimelineData }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withContainerWrapper(FDApprovalScreen));
