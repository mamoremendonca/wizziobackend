import React from "react";
import { Field, FormSpy } from "react-final-form";
import { OnChange } from "react-final-form-listeners";
import * as fdConstants from "../../constants/FDConstants";
import PropTypes from "prop-types";

export const Condition = ({ when, is, isNot, children }) => (
  <Field name={when} subscription={{ value: true }}>
    {({ input: { value } }) => {
      const currentValue = value && value.code ? value.code : value;
      if (is) {
        if (currentValue === is) {
          return children;
        }
      } else if (isNot) {
        if (currentValue !== isNot) {
          return children;
        }
      }
      return null;
    }}
  </Field>
);

Condition.propTypes = {
  when: PropTypes.string,
  is: PropTypes.string,
  isNot: PropTypes.string,
  children: PropTypes.object,
};

export const FilterOptionsBy = ({
  fieldName,
  options,
  optionPropertyName,
  children,
}) => (
  <Field name={fieldName} subscription={{ value: true }}>
    {({ input: { value } }) => {
      const currentValue = value && value.value ? value.value : value;
      let newOptions = [...options];
      if (currentValue) {
        newOptions = options.filter(
          (option) =>
            !option[optionPropertyName] ||
            option[optionPropertyName] === currentValue
        );
      }
      return React.cloneElement(children, { options: newOptions });
    }}
  </Field>
);

FilterOptionsBy.propTypes = {
  fieldName: PropTypes.string,
  options: PropTypes.array,
  optionPropertyName: PropTypes.string,
  children: PropTypes.object,
};

export const WhenFieldChanges = ({ field, becomes, set, to }) => (
  <Field name={set}>
    {({ input: { onChange } }) => (
      <OnChange name={field}>
        {() => {
          if (becomes) {
            onChange(to);
          }
        }}
      </OnChange>
    )}
  </Field>
);

WhenFieldChanges.propTypes = {
  field: PropTypes.string,
  becomes: PropTypes.bool,
  set: PropTypes.string,
  to: PropTypes.string,
};

//Detects any change in the form
export const ChangesEngine = ({ initialValues, handleFormChanges }) => (
  <FormSpy
    subscription={{ values: true }}
    onChange={({ values }) => {
      if (JSON.stringify(values) !== JSON.stringify(initialValues)) {
        handleFormChanges(values);
      }
    }}
  />
);

ChangesEngine.propTypes = {
  initialValues: PropTypes.object,
  handleFormChanges: PropTypes.func,
};

const updateRelatedSupplierField = (
  newValue,
  childFieldName,
  childValue,
  onchangeChildValue
) => {
  if (
    (!newValue && childValue) ||
    (newValue && !childValue) ||
    (newValue && childValue && newValue.code !== childValue.code)
  ) {
    if (newValue) {
      switch (childFieldName) {
        case fdConstants.SUPPLIER_TAX_NUMBER: {
          const childNewValue = {
            ...newValue,
            label: newValue.tax,
          };
          onchangeChildValue(childNewValue);
          break;
        }
        case fdConstants.SUPPLIER_CODE: {
          const childNewValue = {
            ...newValue,
            label: newValue.code,
          };
          onchangeChildValue(childNewValue);
          break;
        }
        case fdConstants.SUPPLIER_NAME: {
          const childNewValue = {
            ...newValue,
            label: newValue.description,
          };
          onchangeChildValue(childNewValue);
          break;
        }
        default:
          break;
      }
    } else {
      onchangeChildValue(newValue);
    }
  }
};

//supplierCode [supplierName, supplierTax]
export const WhenSupplierCodeChanges = () => {
  return (
    <>
      <Field name={fdConstants.SUPPLIER_NAME}>
        {({ input: { onChange, ...rest } }) => {
          return (
            <OnChange name={fdConstants.SUPPLIER_CODE}>
              {(value) => {
                const newValue = value;
                const childValue = rest.value;
                updateRelatedSupplierField(
                  newValue,
                  fdConstants.SUPPLIER_NAME,
                  childValue,
                  onChange
                );
              }}
            </OnChange>
          );
        }}
      </Field>
      <Field name={fdConstants.SUPPLIER_TAX_NUMBER}>
        {({ input: { onChange, ...rest } }) => {
          return (
            <OnChange name={fdConstants.SUPPLIER_CODE}>
              {(value) => {
                const newValue = value;
                const childValue = rest.value;
                updateRelatedSupplierField(
                  newValue,
                  fdConstants.SUPPLIER_TAX_NUMBER,
                  childValue,
                  onChange
                );
              }}
            </OnChange>
          );
        }}
      </Field>
    </>
  );
};

export const WhenSupplierNameChanges = () => {
  return (
    <>
      <Field name={fdConstants.SUPPLIER_CODE}>
        {({ input: { onChange, ...rest } }) => {
          return (
            <OnChange name={fdConstants.SUPPLIER_NAME}>
              {(value) => {
                const newValue = value;
                const childValue = rest.value;
                updateRelatedSupplierField(
                  newValue,
                  fdConstants.SUPPLIER_CODE,
                  childValue,
                  onChange
                );
              }}
            </OnChange>
          );
        }}
      </Field>
      <Field name={fdConstants.SUPPLIER_TAX_NUMBER}>
        {({ input: { onChange, ...rest } }) => {
          return (
            <OnChange name={fdConstants.SUPPLIER_NAME}>
              {(value) => {
                const newValue = value;
                const childValue = rest.value;
                updateRelatedSupplierField(
                  newValue,
                  fdConstants.SUPPLIER_TAX_NUMBER,
                  childValue,
                  onChange
                );
              }}
            </OnChange>
          );
        }}
      </Field>
    </>
  );
};

export const WhenSupplierTaxChanges = () => {
  return (
    <>
      <Field name={fdConstants.SUPPLIER_CODE}>
        {({ input: { onChange, ...rest } }) => {
          return (
            <OnChange name={fdConstants.SUPPLIER_TAX_NUMBER}>
              {(value) => {
                const newValue = value;
                const childValue = rest.value;
                updateRelatedSupplierField(
                  newValue,
                  fdConstants.SUPPLIER_CODE,
                  childValue,
                  onChange
                );
              }}
            </OnChange>
          );
        }}
      </Field>
      <Field name={fdConstants.SUPPLIER_NAME}>
        {({ input: { onChange, ...rest } }) => {
          return (
            <OnChange name={fdConstants.SUPPLIER_TAX_NUMBER}>
              {(value) => {
                const newValue = value;
                const childValue = rest.value;
                updateRelatedSupplierField(
                  newValue,
                  fdConstants.SUPPLIER_NAME,
                  childValue,
                  onChange
                );
              }}
            </OnChange>
          );
        }}
      </Field>
    </>
  );
};
