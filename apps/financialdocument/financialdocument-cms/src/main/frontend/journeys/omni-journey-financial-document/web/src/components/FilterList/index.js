import React, { Component } from "react";
import FilterListRender from "./FilterListRender";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  comboTranslateRefDataToDataSources,
  isDecimal,
  numberEndsWithDot,
  isNumeric,
  isDecimalWithComma,
  allowDecimalWithComma,
} from "../../utils";
import { Map } from "immutable";
import { updateFilter } from "../../actions";
import { INITIAL_FILTER_CRITERIA } from "../../constants";
import PropTypes from "prop-types";

/**
 * HOC to render a component with custom fields that is used to collect the filters used in the server request to get the filtered data.
 */
class FilterList extends Component {
  state = {
    filterFieldsValue: { ...INITIAL_FILTER_CRITERIA },
    invalidFields: Map({ minVATValue: false, maxVATValue: false }),
    errorMessages: Map({ minValueErrorMessage: "", maxValueErrorMessage: "" }),
  };

  // componentDidUpdate() {
  //     console.log('filtros de pesquisa: ' + this.state.filterFieldsValue)
  // }

  /**
   * helper function used in the AutocompleteInput to get the i18n resources.
   */
  getLabelFn = (key, defaultLabel) => {
    const { I18nProvider } = this.props;
    return I18nProvider.Texti18n(key);
  };

  /**
   * To change the filter fields in the component state.
   * For numeric values is not allowed other characters.
   */
  handleChange = (newValue, fieldName) => {
    if (this.isValidInput(newValue, fieldName)) {
      const newState = this.state.filterFieldsValue;
      newState[fieldName] = newValue;
      this.setState({ filterFieldsValue: newState });
    }
  };

  isValidInput = (value, fieldName) => {
    switch (fieldName) {
      case "minVATValue":
      case "maxVATValue":
        return (
          !value ||
          isDecimal.test(value) ||
          numberEndsWithDot.test(value) ||
          allowDecimalWithComma.test(value)
        );
      case "purchaseOrderNumber":
        return !value || isNumeric.test(value);
      default:
        return true;
    }
  };

  isValidMinAndMaxFields = () => {
    const { I18nProvider } = this.props;
    const { filterFieldsValue } = this.state;

    const maxValueState = {
      name: "maxVATValue",
      isInvalid: false,
      errorMessage: "",
    };
    const minValueState = {
      name: "minVATValue",
      isInvalid: false,
      errorMessage: "",
    };
    //Data Input validations
    if (numberEndsWithDot.test(filterFieldsValue.maxVATValue)) {
      maxValueState.isInvalid = true;
      maxValueState.errorMessage = "listFD.filter.missing_decimal_digit";
    } else if (isDecimalWithComma.test(filterFieldsValue.maxVATValue)) {
      maxValueState.isInvalid = true;
      maxValueState.errorMessage = "listFD.filter.invalid_decimal_separator";
    }

    if (numberEndsWithDot.test(filterFieldsValue.minVATValue)) {
      minValueState.isInvalid = true;
      minValueState.errorMessage = "listFD.filter.missing_decimal_digit";
    } else if (isDecimalWithComma.test(filterFieldsValue.minVATValue)) {
      minValueState.isInvalid = true;
      minValueState.errorMessage = "listFD.filter.invalid_decimal_separator";
    }

    //Business validations
    if (
      !(minValueState.isInvalid || maxValueState.isInvalid) &&
      filterFieldsValue.minVATValue &&
      filterFieldsValue.maxVATValue
    ) {
      if (
        parseFloat(filterFieldsValue.minVATValue) >
        parseFloat(filterFieldsValue.maxVATValue)
      ) {
        maxValueState.isInvalid = true;
        minValueState.isInvalid = true;
        minValueState.errorMessage =
          I18nProvider.Texti18n("listFD.filter.total.field.invalid_range") +
          filterFieldsValue.maxVATValue;
      }
    }
    this.updateErrorFieldStates([minValueState, maxValueState]);
    return !(minValueState.isInvalid || maxValueState.isInvalid);
  };

  updateErrorFieldStates = (fields) => {
    const { invalidFields, errorMessages } = this.state;
    if (fields.length > 0) {
      const field = fields.pop();
      this.setState(
        {
          invalidFields: invalidFields.set(field.name, field.isInvalid),
          errorMessages: errorMessages.set(field.name, field.errorMessage),
        },
        () => {
          this.updateErrorFieldStates(fields);
        }
      );
    }
  };

  /**
   * To call the parent method that receives a filter before call the service that gets the new data.
   */
  handleFilterClick = () => {
    const { updateFilter, onFilterClick, searchCriteria } = this.props;
    if (this.isValidMinAndMaxFields()) {
      updateFilter({ ...this.state.filterFieldsValue });
      if (onFilterClick) {
        onFilterClick({
          ...searchCriteria,
          filterCriteria: { ...this.state.filterFieldsValue },
        });
      }
    }
  };

  /**
   * Clear the filter fields value and make a new request to get data from server without the filters.
   */
  handleClearFilterClick = () => {
    this.setState(
      { filterFieldsValue: { ...INITIAL_FILTER_CRITERIA } },
      this.handleFilterClick
    );
  };

  getDataSourceList = (refData, suppliers, I18nProvider) => {
    let dataSourceList = {};
    dataSourceList.status = comboTranslateRefDataToDataSources(
      refData.status,
      I18nProvider
    );
    dataSourceList.projects = comboTranslateRefDataToDataSources(
      refData.projects,
      I18nProvider
    );
    dataSourceList.departments = comboTranslateRefDataToDataSources(
      refData.departments,
      I18nProvider
    );
    dataSourceList.internalOrders = comboTranslateRefDataToDataSources(
      refData.internalOrders,
      I18nProvider
    );
    dataSourceList.financialDocumentTypes = comboTranslateRefDataToDataSources(
      refData.financialDocumentTypes,
      I18nProvider
    );
    dataSourceList.suppliers = comboTranslateRefDataToDataSources(
      refData.suppliers,
      I18nProvider
    );
    return dataSourceList;
  };

  render() {
    const { I18nProvider, refData, suppliers } = this.props;
    return (
      <FilterListRender
        filterFieldsValue={this.state.filterFieldsValue}
        dataSourceList={this.getDataSourceList(
          refData,
          suppliers,
          I18nProvider
        )}
        onFilterClick={this.handleFilterClick}
        onClearFilterClick={this.handleClearFilterClick}
        onChangeFilter={this.handleChange}
        I18nProvider={I18nProvider}
        invalidFields={this.state.invalidFields}
        errorMessages={this.state.errorMessages}
        getLabelFn={this.getLabelFn}
      />
    );
  }
}

FilterList.propTypes = {
  I18nProvider: PropTypes.object,
  updateFilter: PropTypes.func,
  refData: PropTypes.object,
  suppliers: PropTypes.array,
  searchCriteria: PropTypes.object,
  onFilterClick: PropTypes.func,
};

const mapStateToProps = ({
  journey: {
    services: { I18nProvider },
  },
  i18n,
  refDataReducer: { refData, suppliers },
  financialDocumentReducer: { searchCriteria },
}) => ({
  I18nProvider,
  i18n,
  refData,
  suppliers,
  searchCriteria,
});
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      updateFilter,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(FilterList);
