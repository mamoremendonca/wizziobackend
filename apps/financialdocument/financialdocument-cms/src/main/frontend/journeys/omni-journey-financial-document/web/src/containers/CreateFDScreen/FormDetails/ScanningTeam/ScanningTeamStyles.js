const ComponentName = 'ScanningTeamStyle';

const styles = theme => ({
    root:{
        display: "flex"
    },
    gridRow:{
        marginTop: "30px"
    }
});

export {ComponentName, styles};