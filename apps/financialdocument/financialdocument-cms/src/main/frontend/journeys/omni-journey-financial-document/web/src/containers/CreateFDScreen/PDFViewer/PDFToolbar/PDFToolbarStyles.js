const ComponentName = "PDFToolbarStyle";
const styles = (theme) => ({
    "flexContainer": {
        "alignItems": "center",
        "display": "flex",
        "width": "100%"
    },
    "pdfNavIcon": {
        "height": "44px",
        "width": "44px",
        "backgroundColor": "#81bc00",
        "alignItems": "center",
        "display": "flex",
        "justifyContent": "center"
    },
    "pdfNavTitle": {
        "alignItems": "center",
        "display": "flex",
        "flexGrow": "1",
        "flexShrink": "1",
        "justifyContent": "left",
        "paddingLeft": "1em"
    },
    "pdfNavTools": {
        "alignItems": "center",
        "display": "flex",
        "flexGrow": "1",
        "flexShrink": "1",
        "justifyContent": "center"
    }
});

export { styles, ComponentName };
