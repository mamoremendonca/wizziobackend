import * as React from "react";
import Input from "@ui-lib/core/Input";
import FormHelperText from "@ui-lib/core/FormHelperText";
import PropTypes from "prop-types";

const InputWrapper = ({
  input: { name, onChange, value, ...restInput },
  meta,
  ...rest
}) => {
  const showError =
    ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) &&
    meta.touched;

  return (
    <>
      <Input
        {...rest}
        name={name}
        error={showError}
        inputProps={restInput}
        onChange={onChange}
        value={value}
      />

      {showError && (
        <FormHelperText>{meta.error || meta.submitError}</FormHelperText>
      )}
    </>
  );
};

InputWrapper.propTypes = { input: PropTypes.object, meta: PropTypes.object };

export default InputWrapper;
