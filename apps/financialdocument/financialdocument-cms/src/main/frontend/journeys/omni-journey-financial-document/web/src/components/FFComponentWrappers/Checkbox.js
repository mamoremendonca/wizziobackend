import * as React from "react";
import Checkbox from "@ui-lib/core/Checkbox";
import PropTypes from "prop-types";

const CheckboxWrapper = ({
  input: { checked, name, onChange, ...restInput },
  meta,
  ...rest
}) => (
  <Checkbox
    {...rest}
    name={name}
    inputProps={restInput}
    onChange={onChange}
    checked={checked}
  />
);

CheckboxWrapper.propTypes = {
  input: PropTypes.object,
  meta: PropTypes.object,
};

export default CheckboxWrapper;
