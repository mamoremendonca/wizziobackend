export const SET_VALUE = "SET_VALUE";
export const SET_REFDATA = "SET_REFDATA";
export const SET_SUPPLIERS = "SET_SUPPLIERS";
export const SET_SUPPLIER_DETAILS = "SET_SUPPLIER_DETAILS";
export const FD_CAPEX = "CAPEX";
export const FD_CONTRACT = "CONTRACT";
export const FD_OPEX = "OPEX";

export const ONEY_REFERENCE_DATA_BODY = [
  "ASSET", // items used for capex purchase order type.
  "COST_CENTER",
  "DOCUMENT_TYPE", //used to attachment document class.
  "INTERNAL_ORDER",
  "ITEM", // used for Contract and OPEX purchase order types.
  "ITEM_TYPE", // used for Contract and OPEX purchase order types.
  "PROJECT",
  "TAX",
  "RETENTION_CODE",
  "DEPARTMENT",
  "GROUP"
];

export const FINANCIAL_DOCUMENT_REFERENCE_DATA_BODY = [
  "DEPARTMENT",
  "FINANCIAL_DOCUMENT_STATUS",
  "FINANCIAL_DOCUMENT_TYPE",
];
