export const componentName = "AttachmentListStyles";

export const styles = (theme) => ({
  root: {
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.grey[100],
    height: "100%",
  },
});
