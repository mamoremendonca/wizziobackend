import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import { ComponentName, styles } from "./ScanningTeamStyles";
import Grid from "@ui-lib/core/Grid";
import { Field } from "react-final-form";
import * as constants from "../../../../constants";
import {
  Select,
  TextField,
  BasicDateField,
} from "../../../../components/FFComponentWrappers";
import { isRequired, currentDate } from "../../validator";
import {
  WhenSupplierCodeChanges,
  WhenSupplierNameChanges,
  WhenSupplierTaxChanges,
} from "../../react-final-form-helpers";
import PropTypes from "prop-types";

const ScanningTeam = ({
  refData,
  I18nProvider,
  classes,
  supplierCodes,
  supplierNames,
  supplierTaxes,
  lockForm,
  lockFormForToAccountsPayableTeam,
  isFDCombinedWithPO,
}) => {
  return (
    <Grid container>
      <Grid container>
        {/*First Row*/}
        <Grid className={classes.gridRow} container spacing={32} item xs={12}>
          <Grid item xs={4}>
            <Field
              name={constants.FINANCIAL_DOCUMENT_TYPE}
              component={Select}
              label={I18nProvider.Labeli18n(
                "createFDScreen.scanning_team_form.type_field.label"
              )}
              formControlProps={{ fullWidth: true }}
              InputLabelProps={{ shrink: true }}
              options={refData.financialDocumentTypes}
              validate={isRequired(
                I18nProvider.Texti18n("validator.required.field.error.message")
              )}
              required
              disabled={lockForm || lockFormForToAccountsPayableTeam}
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name={constants.FINANCIAL_DOCUMENT_NUMBER}
              label={I18nProvider.Labeli18n(
                "createFDScreen.scanning_team_form.number_field.label"
              )}
              component={TextField}
              fullWidth
              InputLabelProps={{ shrink: true }}
              validate={isRequired(
                I18nProvider.Texti18n("validator.required.field.error.message")
              )}
              required={!(lockForm || lockFormForToAccountsPayableTeam)} //Hack to hide info required when disabled
              disabled={lockForm || lockFormForToAccountsPayableTeam}
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name={constants.EMISSION_DATE}
              label={I18nProvider.Labeli18n(
                "createFDScreen.scanning_team_form.emissionDate_field.label"
              )}
              component={BasicDateField}
              fullWidth
              InputLabelProps={{ shrink: true }}
              validate={isRequired(
                I18nProvider.Texti18n("validator.required.field.error.message")
              )}
              max={currentDate}
              required
              disabled={lockForm || lockFormForToAccountsPayableTeam}
            />
          </Grid>
        </Grid>
        {/*Second Row  */}
        <Grid className={classes.gridRow} container spacing={32} item xs={12}>
          <WhenSupplierCodeChanges />
          <WhenSupplierNameChanges />
          <WhenSupplierTaxChanges />
          <Grid item xs={4}>
            <Field
              name={constants.SUPPLIER_CODE}
              component={Select}
              label={I18nProvider.Labeli18n(
                "createFDScreen.scanning_team_form.supplierCode_field.label"
              )}
              formControlProps={{ fullWidth: true }}
              InputLabelProps={{ shrink: true }}
              options={supplierCodes}
              validate={isRequired(
                I18nProvider.Texti18n("validator.required.field.error.message")
              )}
              required
              disabled={
                lockForm ||
                (lockFormForToAccountsPayableTeam && isFDCombinedWithPO)
              }
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name={constants.SUPPLIER_NAME}
              component={Select}
              label={I18nProvider.Labeli18n(
                "createFDScreen.scanning_team_form.supplierName_field.label"
              )}
              formControlProps={{ fullWidth: true }}
              InputLabelProps={{ shrink: true }}
              options={supplierNames}
              validate={isRequired(
                I18nProvider.Texti18n("validator.required.field.error.message")
              )}
              required
              disabled={
                lockForm ||
                (lockFormForToAccountsPayableTeam && isFDCombinedWithPO)
              }
            />
          </Grid>
          <Grid item xs={4}>
            <Field
              name={constants.SUPPLIER_TAX_NUMBER}
              component={Select}
              label={I18nProvider.Labeli18n(
                "createFDScreen.scanning_team_form.supplierTaxNumber_field.label"
              )}
              formControlProps={{ fullWidth: true }}
              InputLabelProps={{ shrink: true }}
              options={supplierTaxes}
              validate={isRequired(
                I18nProvider.Texti18n("validator.required.field.error.message")
              )}
              required
              disabled={
                lockForm ||
                (lockFormForToAccountsPayableTeam && isFDCombinedWithPO)
              }
            />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

ScanningTeam.propTypes = {
  I18nProvider: PropTypes.object,
  classes: PropTypes.object,
  refData: PropTypes.object,
  supplierCodes: PropTypes.array,
  supplierNames: PropTypes.array,
  supplierTaxes: PropTypes.array,
  lockForm: PropTypes.bool,
  lockFormForToAccountsPayableTeam: PropTypes.bool,
  isFDCombinedWithPO: PropTypes.bool,
};

export default withStyles(styles, { name: ComponentName })(ScanningTeam);
