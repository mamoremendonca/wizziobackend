const ComponentName = "AsyncSearchFieldStyles";
const styles = (theme) => ({
  buttonIcon: {
    cursor: "pointer",
    /* maxWidth: "4px", */
    fontWidth: "4px",
    "&:hover": {
      color: theme.palette.primary.main,
    },
  },
  notchedOutline: {
    borderColor: "red !important",
  },
  loadingIcon: {},
});

export { ComponentName, styles };
