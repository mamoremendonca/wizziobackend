import * as constants from "../../constants";
import { isAlphaNumeric } from "../../utils/regexpatterns";
//#region Helper functions

//only use this for BasicDateField
export const currentDate = new Date().toISOString().slice(0, 10);

export const composeValidators = (...validators) => (value) =>
  validators.reduce((error, validator) => error || validator(value), undefined);

/**
 * To handle the respose error
 * TODO: maybe put this function in utils.js
 * @param {*} fields
 * @param {*} I18nProvider
 */
export const extractInvalidFields = (fields, I18nProvider) => {
  const invalidFields = {};
  fields.forEach((field) => {
    invalidFields[field.message] = I18nProvider.Texti18n(
      field.reference === "field"
        ? "validator.required.field.error.message"
        : field.reference
    );
  });
  return invalidFields;
};

//#endregion

//#region  Validations to use in the react final form fields
/**
 * curry function
 * @param {*} errorMessage could be the i18n key or the message *
 * @return the last function returns a error message if the value fails the test.
 */
export const isRequired = (errorMessage) => (value) =>
  !value ? errorMessage : undefined;

export const mustBeAlphaNumeric = (errorMessage) => (value) =>
  !isAlphaNumeric.test(value) ? errorMessage : undefined;

export const lengthNoHigherThanTwentyFive = (errorMessage) => (value) =>
  value && value.length && value.length > 25 ? errorMessage : undefined;

const daysDiff = (checkDate) => {
  const currentDate = new Date();
  const compareDate = new Date(checkDate);

  const currentDateUTC = Date.UTC(
    currentDate.getFullYear(),
    currentDate.getMonth(),
    currentDate.getDate()
  );
  const compareDateUTC = Date.UTC(
    compareDate.getFullYear(),
    compareDate.getMonth(),
    compareDate.getDate()
  );

  const diffTime = compareDateUTC - currentDateUTC;
  return Math.ceil(diffTime / (1000 * 60 * 60 * 24));
};

export const isDateHigherThenCurrentDate = (errorMessage) => (value) => {
  if (daysDiff(value) > 0) {
    return errorMessage;
  }
  return undefined;
};

export const isDateLowerThenCurrentDate = (errorMessage) => (value) => {
  if (daysDiff(value) < 0) {
    return errorMessage;
  }
  return undefined;
};

//#endregion

//#region validate form before submit the process to creation

export const submitToCreationPreValidations = (formValues, I18nProvider) => {
  const invalidFields = {};
  for (const fieldName in formValues) {
    if (fieldName === constants.FINANCIAL_DOCUMENT_NUMBER) {
      const fieldErrorMessage = composeValidators(
        lengthNoHigherThanTwentyFive(
          I18nProvider.Texti18n(
            "validator.length_higher_than_ten.error.message"
          )
        )
      )(formValues[fieldName]);
      if (fieldErrorMessage) {
        invalidFields[fieldName] = fieldErrorMessage;
      }
    } else if (fieldName === constants.EMISSION_DATE) {
      const fieldErrorMessage = isDateHigherThenCurrentDate(
        I18nProvider.Texti18n(
          "validator.date_higher_than_current_date.error.message"
        )
      )(formValues[fieldName]);
      if (fieldErrorMessage) {
        invalidFields[fieldName] = fieldErrorMessage;
      }
    }
  }
  return invalidFields;
};

//#endregion

//#region validate form before submit the process to approval
export const submitToApprovalPreValidations = (formValues, I18nProvider) => {
  const invalidFields = {};
  for (const fieldName in formValues) {
    if (fieldName === constants.EXPIRATION_DATE) {
      const fieldErrorMessage = isDateLowerThenCurrentDate(
        I18nProvider.Texti18n(
          "validator.date_lower_than_current_date.error.message"
        )
      )(formValues[fieldName]);
      if (fieldErrorMessage) {
        invalidFields[fieldName] = fieldErrorMessage;
      }
    }
  }
  return invalidFields;
};
//#endregion

//#region validations to allow auto save.
export const isTargetAllowedToUpdate = (targetName) => {
  const targetsAllowed = [
    constants.DELETE_FD_BUTTON_ID,
    constants.UPLOAD_INVOICE_BUTTON_ID,
    constants.ADD_ITEM_ID, //TODO: in future delete this
  ];
  return targetsAllowed.some((element) => element === targetName);
};
//#endregion
