import React from "react";
import { withStyles } from "@ui-lib/core/styles";
import styles from "./styles";

import Icon from "@ui-lib/core/Icon";
import PropTypes from "prop-types";

const FileInput = (props) => {
  const { classes, disabled, handleFileSelection } = props;
  return (
    <span
      className={!disabled ? classes.buttonIcon : classes.buttonIconDisabled}
    >
      <input
        value={""}
        type="file"
        className={classes.InputFile}
        id="icon-button-file"
        onChange={(e) => handleFileSelection(e.target.files)}
      />
      <label htmlFor="icon-button-file">
        <Icon className="icon-add" style={{ cursor: "pointer" }}></Icon>
      </label>
    </span>
  );
};

FileInput.propTypes = {
  classes: PropTypes.object,
  disabled: PropTypes.bool,
  handleFileSelection: PropTypes.func,
};

export default withStyles(styles)(FileInput);
