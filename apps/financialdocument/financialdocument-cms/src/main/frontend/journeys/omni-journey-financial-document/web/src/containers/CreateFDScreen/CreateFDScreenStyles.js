const ComponentName = "CreateDFScreenStyle";
const styles = (theme) => ({
  root: {
    minHeight: "100%",
    display: "grid",
    gridTemplateRows: "auto auto auto auto",
    gridTemplateColumns: "100%",
    padding: "10px 40px 40px 40px",
  },
  attachmentButton: {
    position: "absolute",
    right: "5px",
  },
  fdHeaderData: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottom: `1px solid ${theme.palette.text.secondary}`,
    width: "100%",
  },
  InputFile: { display: "none", cursor: "pointer" },
  gridForm: { position: "relative", paddingRight: "0 !important" },
  formSection: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 6,
    display: "flex",
    flexWrap: "wrap",
  },
  formSectionAttachments: {
    marginTop: theme.spacing.unit * 3,
  },
  rootPaper: {
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.common.white,
  },
  priceSubsection: {
    margin: `${theme.spacing.unit * 2}px 0`,
  },
  priceLabel: {
    ...theme.typography.subtitle1,
    color: theme.palette.primary.main,
  },
  priceValue: {
    ...theme.typography.subtitle2,
    display: "flex",
  },
  buttonIcon: {
    cursor: "pointer",
    "&:hover": {
      color: theme.palette.primary.main,
    },
  },
});

export { ComponentName, styles };
