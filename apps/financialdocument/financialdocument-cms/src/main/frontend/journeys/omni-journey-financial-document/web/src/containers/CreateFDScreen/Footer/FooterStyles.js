const ComponentName = 'CreateDFFooterStyle';
const styles = theme =>({
    root:{
        display:'flex',
        justifyContent:'flex-end',
        width: "100%"
    },
    marginButton: {
        marginLeft: '15px'
    }
});

export {ComponentName, styles};