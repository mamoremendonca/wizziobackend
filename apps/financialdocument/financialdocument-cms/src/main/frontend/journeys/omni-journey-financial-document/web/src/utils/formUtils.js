import {
  ITEM,
  DESCRIPTION,
  QUANTITY,
  UNIT_VALUE,
  TOTAL_AMOUNT_WO_VAT,
  TOTAL_AMOUNT_W_VAT,
  TAX,
  COST_CENTER,
  COMMENT,
  START_DATE,
  END_DATE,
  ITEM_TYPE,
  FIXEDASSET,
  INDEX,
} from "../constants/itemFormConstants";
import { Config } from "../config";

import * as fdConstants from "../constants";

export const mapBackendItemToFormValues = (backendItem, refData) => {
  let item = refData.items.find((item) => item.code === backendItem.code);
  let itemType = undefined;
  if (item) {
    itemType = refData.itemTypes.find(
      (type) => type.code === item.itemTypeCode
    );
  } else {
    item = refData.assets.find((asset) => asset.code === backendItem.code);
  }
  let costCenter = refData.costCenters.find(
    (costCenter) => costCenter.code === backendItem.costCenterCode
  );
  if (!costCenter) {
    costCenter = refData.projects.find(
      (costCenter) => costCenter.code === backendItem.costCenterCode
    );
  }
  const tax = refData.tax.find((tax) => tax.code === backendItem.taxCode);
  const retention = refData.retentionCodes.find(
    (retention) => retention.code === backendItem.retentionCode
  );
  const unitValue = backendItem.unitPriceWithoutTax;
  const quantity = backendItem.amount;
  const totalAmountWoVAT = quantity * unitValue;
  let retentionValue = 0;
  if (retention) {
    retentionValue =
      (totalAmountWoVAT * parseFloat(retention.taxPercentage)) / 100;
  }
  const totalAmountWithVAT = tax
    ? quantity * unitValue * (1 + parseFloat(tax.taxPercentage) / 100)
    : 0;
  const finalAmount = totalAmountWithVAT - retentionValue;
  const startDate = dateFormatter(backendItem.startDate);
  const endDate = dateFormatter(backendItem.endDate);

  return {
    ...backendItem,
    item,
    itemType,
    costCenter,
    tax,
    unitValue,
    quantity,
    retention,
    retentionValue,
    totalAmountWoVAT,
    totalAmountWithVAT,
    finalAmount,
    startDate,
    endDate,
  };
};

// Mapping item values to be sent to the Database
export const mapFormValuesToBackendItem = (financialDocumentItem) => ({
  code: financialDocumentItem.item
    ? financialDocumentItem.item.value
    : financialDocumentItem.fixedAsset
    ? financialDocumentItem.fixedAsset.value
    : undefined,
  description: financialDocumentItem.description
    ? financialDocumentItem.description
    : undefined,
  costCenterCode: financialDocumentItem.costCenter
    ? financialDocumentItem.costCenter.value
    : undefined,
  amount: parseFloat(financialDocumentItem.quantity),
  unitPriceWithoutTax: parseFloat(financialDocumentItem.unitValue),
  taxCode: financialDocumentItem.tax
    ? financialDocumentItem.tax.value
    : undefined,
  startDate: financialDocumentItem.startDate
    ? new Date(financialDocumentItem.startDate)
    : undefined,
  endDate: financialDocumentItem.endDate
    ? new Date(financialDocumentItem.endDate)
    : undefined,
  comment: financialDocumentItem.comment
    ? financialDocumentItem.comment
    : undefined,
  retentionCode: financialDocumentItem.retention
    ? financialDocumentItem.retention.code
    : undefined,
});

export const addItemToFields = (values, index, fields) => {
  fields.push({
    [FIXEDASSET]: values[FIXEDASSET],
    [ITEM_TYPE]: values[ITEM_TYPE],
    [ITEM]: values[ITEM],
    [DESCRIPTION]: values[DESCRIPTION],
    [QUANTITY]: values[QUANTITY],
    [UNIT_VALUE]: values[UNIT_VALUE],
    [TOTAL_AMOUNT_WO_VAT]: values[TOTAL_AMOUNT_WO_VAT],
    [TOTAL_AMOUNT_W_VAT]: values[TOTAL_AMOUNT_W_VAT],
    [TAX]: values[TAX],
    [COST_CENTER]: values[COST_CENTER],
    [COMMENT]: values[COMMENT],
    [START_DATE]: values[START_DATE],
    [END_DATE]: values[END_DATE],
    [INDEX]: index,
  });
  return fields;
};

export const editItemField = (values, selectedItemIndex, fields) => {
  const updatedField = {
    [FIXEDASSET]: values[FIXEDASSET],
    [ITEM_TYPE]: values[ITEM_TYPE],
    [ITEM]: values[ITEM],
    [DESCRIPTION]: values[DESCRIPTION],
    [QUANTITY]: values[QUANTITY],
    [UNIT_VALUE]: values[UNIT_VALUE],
    [TOTAL_AMOUNT_WO_VAT]: values[TOTAL_AMOUNT_WO_VAT],
    [TOTAL_AMOUNT_W_VAT]: values[TOTAL_AMOUNT_W_VAT],
    [TAX]: values[TAX],
    [COST_CENTER]: values[COST_CENTER],
    [COMMENT]: values[COMMENT],
    [START_DATE]: values[START_DATE],
    [END_DATE]: values[END_DATE],
    [INDEX]: values[INDEX],
  };
  fields[selectedItemIndex] = updatedField;
  return fields;
};

// TODO MOVE SERVICE REQUESTS
/*export const addFinancialDocumentItemRequest = (
  HttpClient,
  item,
  financialDocumentId
) => {
  let config = {
    method: Config.METHOD_POST,
    url: `${Config.URL_PURCHASE_ORDER}${financialDocumentId}${Config.ITEM_URL}`,
    data: mapValuesToItem(item),
  };
  return HttpClient.request(config);
};

export const updateFinancialDocumentItemRequest = (
  HttpClient,
  item,
  financialDocumentId
) => {
  let config = {
    method: Config.METHOD_PUT,
    url: `${Config.URL_PURCHASE_ORDER}${financialDocumentId}${Config.ITEM_URL}${item.index}`,
    data: mapValuesToItem(item),
  };
  return HttpClient.request(config);
};*/

export const removeFinancialDocumentItemRequest = (
  HttpClient,
  index,
  financialDocumentId
) => {
  let config = {
    method: Config.METHOD_DELETE,
    url: `${Config.URL_PURCHASE_ORDER}${financialDocumentId}${Config.ITEM_URL}${index}`,
  };
  return HttpClient.request(config);
};

export const addFinancialDocumentAttachmentRequest = (
  HttpClient,
  attachment,
  financialDocumentId,
  documentClass
) => {
  const data = new FormData();
  data.append("attachment", attachment);

  let config = {
    method: Config.METHOD_POST,
    url: `${Config.URL_PURCHASE_ORDER}${financialDocumentId}${Config.ATTACHMENT_URL}?type=${documentClass}`,
    data,
  };
  return HttpClient.request(config);
};

export const dateFormatter = (dateAsString) => {
  if (dateAsString) {
    return new Date(dateAsString).toISOString().slice(0, 10);
  }
  return null;
};

const getRefDataOptionFromValue = (lovFields, fieldValue) => {
  let option = null;

  if (fieldValue) {
    option = lovFields.find((field) => {
      if (field.id) {
        return field.id.toString() === fieldValue.toString();
      }
      if (field.value) {
        return field.value.toString() === fieldValue.toString();
      } else if (field.code) {
        return field.code.toString() === fieldValue.toString();
      }
      return false;
    });
  }
  return option;
};

const getSupplierFieldsOption = (suppliers, supplierCode) => {
  const supplierField = {
    code: null,
    name: null,
    taxNumber: null,
  };
  if (supplierCode) {
    const supplierOption = getRefDataOptionFromValue(suppliers, supplierCode);
    if (supplierOption) {
      supplierField.code = {
        ...supplierOption,
        value: supplierOption.code,
        label: supplierOption.code,
      };
      supplierField.name = {
        ...supplierOption,
        value: supplierOption.code,
        label: supplierOption.name,
      };
      supplierField.taxNumber = {
        ...supplierOption,
        value: supplierOption.code,
        label: supplierOption.tax,
      };
    }
  }
  return supplierField;
};

const auxFirstWorkflowGroup = (allGroups, valueBE) => {
  let firstWorkflowGroup = getRefDataOptionFromValue(allGroups, valueBE);

  // When associated PO doesn't have a current valid firstWorkflowGroup value.
  // The value returned by the BE is then used to create the object
  if (!firstWorkflowGroup && valueBE) {
    firstWorkflowGroup = [];
    firstWorkflowGroup.push({
      value: valueBE,
      label: valueBE,
      code: valueBE,
      description: valueBE,
      active: true,
      departmentCode: "",
    });
  }

  return firstWorkflowGroup;
};

export const mapFinancialDocumentDataToCreateDTO = (
  refData,
  financialDocumentData
) => {
  const supplierField = getSupplierFieldsOption(
    refData.suppliers,
    financialDocumentData.financialDocumentHeader.supplierCode
  );

  return {
    ...financialDocumentData.financialDocumentHeader,
    creationDate: dateFormatter(
      financialDocumentData.financialDocumentHeader.creationDate
    ),
    type: getRefDataOptionFromValue(
      refData.financialDocumentTypes,
      financialDocumentData.financialDocumentHeader.type
    ),
    number: financialDocumentData.financialDocumentHeader.number || null,
    supplierCode: supplierField.code,
    supplierName: supplierField.name,
    supplierTaxNumber: supplierField.taxNumber,
    emissionDate: dateFormatter(
      financialDocumentData.financialDocumentHeader.emissionDate
    ),
    departmentCode: getRefDataOptionFromValue(
      refData.departments,
      financialDocumentData.financialDocumentHeader[fdConstants.DEPARTMENT_CODE]
    ),
    firstWorkflowGroup: auxFirstWorkflowGroup(
      refData.groups,
      financialDocumentData.financialDocumentHeader[fdConstants.GROUP_CODE]
    ),
    projectCode: getRefDataOptionFromValue(
      refData.projects,
      financialDocumentData.financialDocumentHeader[fdConstants.PROJECT_CODE]
    ),
    internalOrderCode: getRefDataOptionFromValue(
      refData.internalOrders,
      financialDocumentData.financialDocumentHeader[
        fdConstants.INTERNAL_ORDER_CODE
      ]
    ),
    expirationDate: dateFormatter(
      financialDocumentData.financialDocumentHeader[fdConstants.EXPIRATION_DATE]
    ),
  };
};

export const financialDocumentHeaderDraftBodyFrom = (scanningFormData) => ({
  number: scanningFormData.number,
  type: scanningFormData.type ? scanningFormData.type.code : null,
  supplierCode: scanningFormData.supplierCode
    ? scanningFormData.supplierCode.code
    : null,
  emissionDate: scanningFormData.emissionDate
    ? new Date(scanningFormData.emissionDate)
    : null,
});

export const financialDocumentHeaderCreationBodyFrom = (
  accountsPayableFormData
) => ({
  granting: accountsPayableFormData[fdConstants.GRANTING],
  description: accountsPayableFormData[fdConstants.DESCRIPTION_FIELD],
  departmentCode: accountsPayableFormData[fdConstants.DEPARTMENT_CODE]
    ? accountsPayableFormData[fdConstants.DEPARTMENT_CODE].code
    : null,
  firstWorkflowGroup: accountsPayableFormData[fdConstants.GROUP_CODE]
    ? accountsPayableFormData[fdConstants.GROUP_CODE].code
    : null,
  projectCode: accountsPayableFormData[fdConstants.PROJECT_CODE]
    ? accountsPayableFormData[fdConstants.PROJECT_CODE].code
    : null,
  internalOrderCode: accountsPayableFormData[fdConstants.INTERNAL_ORDER_CODE]
    ? accountsPayableFormData[fdConstants.INTERNAL_ORDER_CODE].code
    : null,
  expirationDate: accountsPayableFormData[fdConstants.EXPIRATION_DATE]
    ? new Date(accountsPayableFormData[fdConstants.EXPIRATION_DATE])
    : null,
  supplierCode: accountsPayableFormData[fdConstants.SUPPLIER_CODE]
    ? accountsPayableFormData[fdConstants.SUPPLIER_CODE].code
    : null,
});

// fields where the descritpion is in the refData
const getDescription = (field, refData) => {
  if (!field) return "-";
  if (!refData) return field;
  //with refData
  const element = getRefDataOptionFromValue(refData, field);
  if (!element) return "-";
  return element.label;
};

export const detailsFieldsMapper = (financialDocumentHeader, refData) => {
  const supplierField = getSupplierFieldsOption(
    refData.suppliers,
    financialDocumentHeader.supplierCode
  );
  const values = {};
  values.type = getDescription(
    financialDocumentHeader.type,
    refData.financialDocumentTypes
  );
  values.purchaseOrder = financialDocumentHeader.purchaseOrderId;
  values.supplierCode = supplierField.code.code;
  values.supplierName = supplierField.code.name;
  values.supplierTaxNumber = supplierField.code.tax;
  values.purchaseOrder = getDescription(
    financialDocumentHeader.purchaseOrderId
  );
  values.totalWithoutTax = getDescription(
    financialDocumentHeader.totalWithoutTax
  );
  values.associatedDocument = getDescription(
    financialDocumentHeader.associatedDocument
  );
  values.department = getDescription(
    financialDocumentHeader.departmentCode,
    refData.departments
  );
  values.project = getDescription(
    financialDocumentHeader.projectCode,
    refData.projects.description
  );
  values.internalOrderCode = getDescription(
    financialDocumentHeader.internalOrderCode
  );
  values.emissionDate = dateFormatter(financialDocumentHeader.emissionDate);
  values.expirationDate = dateFormatter(financialDocumentHeader.expirationDate);
  values.description = getDescription(financialDocumentHeader.description);
  values.granting = financialDocumentHeader.granting;
  values.purchaseOrderFriendlyNumber = getDescription(
    financialDocumentHeader.purchaseOrderFriendlyNumber
  );
  return values;
};
