import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRootHoc } from "omni-journey";
import { withStyles } from "@ui-lib/core/styles";

import { MemoryRouter as Router, Switch, Route } from "react-router-dom";
import AppRoutes from "./routes/Routes";
import { getAllRefData, setFinancialDocumentsDependencies } from "./actions";
import FDListScreen from "./containers/FDListScreen";
import FDDetailScreen from "./containers/FDDetailScreen";
import FDApprovalScreen from "./containers/FDApprovalScreen";
import FDRejectionScreen from "./containers/FDRejectionScreen";
import CreateFDScreen from "./containers/CreateFDScreen";
import styles from "./styles";
import * as constants from "./constants";
import PropTypes from "prop-types";

class Journey extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      allDependenciesResolved: false,
    };
  }

  componentDidUpdate(prevProps) {
    //update current dependencies
    const {
      refData,
      dependencies,
      services: { Loading },
      setFinancialDocumentsDependencies,
    } = this.props;
    if (refData !== prevProps.refData && !dependencies.get("refData")) {
      setFinancialDocumentsDependencies("refData");
    } else if (
      !this.state.allDependenciesResolved &&
      dependencies.every((v) => v)
    ) {
      Loading.request("stopLoading").then(() => {
        //all dependencies are load so you can do the render.
        this.setState({ allDependenciesResolved: true });
      });
    }
  }

  componentDidMount() {
    const {
      services,
      services: { Loading },
      getAllRefData,
    } = this.props;
    Loading.request("setLoading", "JOURNEY").then(() => {
      getAllRefData(services);
    });
  }

  routerInitialEntries = (initialArgs) => {
    //TODO: refactor fo status
    if (initialArgs && initialArgs.journey) {
      //here the purchase order came from the work list so is process continuity
      switch (initialArgs.journey.status) {
        case constants.FINANCIAL_DOCUMENT_PROCESS_STATUS.DRAFT:
        case constants.FINANCIAL_DOCUMENT_PROCESS_STATUS.IN_CREATION:
          return [
            AppRoutes.DRAFT.basePath + "/" + initialArgs.journey.instanceId,
          ];
        case constants.FINANCIAL_DOCUMENT_PROCESS_STATUS.IN_APPROVAL:
          return [
            AppRoutes.APPROVE.basePath + "/" + initialArgs.journey.instanceId,
          ];
        case constants.FINANCIAL_DOCUMENT_PROCESS_STATUS.REJECTED:
          return [
            AppRoutes.CANCEL.basePath + "/" + initialArgs.journey.instanceId,
          ];
        default:
          return ["/"];
      }
    }
    return ["/"];
  };

  render() {
    const { initialArgs } = this.props;
    return (
      <React.Fragment>
        {this.state.allDependenciesResolved && (
          <Router
            initialEntries={this.routerInitialEntries(initialArgs)}
            initialIndex={0}
          >
            <Switch>
              <Route exact={AppRoutes.HOME.exact} path={AppRoutes.HOME.path}>
                <FDListScreen className={this.props.classes.root} />
              </Route>
              <Route
                exact={AppRoutes.CREATE.exact}
                path={AppRoutes.CREATE.path}
              >
                <CreateFDScreen className={this.props.classes.root} />
              </Route>
              <Route exact={AppRoutes.DRAFT.exact} path={AppRoutes.DRAFT.path}>
                <CreateFDScreen className={this.props.classes.root} />
              </Route>
              <Route
                exact={AppRoutes.DETAILS.exact}
                path={AppRoutes.DETAILS.path}
              >
                <FDDetailScreen className={this.props.classes.root} />
              </Route>
              <Route
                exact={AppRoutes.APPROVE.exact}
                path={AppRoutes.APPROVE.path}
              >
                <FDApprovalScreen className={this.props.classes.root} />
              </Route>
              <Route
                exact={AppRoutes.CANCEL.exact}
                path={AppRoutes.CANCEL.path}
              >
                <FDRejectionScreen className={this.props.classes.root} />
              </Route>
            </Switch>
          </Router>
        )}
      </React.Fragment>
    );
  }
}

Journey.propTypes = {
  refData: PropTypes.object,
  classes: PropTypes.object,
  services: PropTypes.object,
  getAllRefData: PropTypes.func,
  setFinancialDocumentsDependencies: PropTypes.func,
  dependencies: PropTypes.object,
  initialArgs: PropTypes.object,
};

const mapStateToProps = ({
  refDataReducer: { refData },
  financialDocumentReducer: { dependencies },
  journey: { services, initialArgs },
}) => ({
  refData,
  dependencies,
  services,
  initialArgs,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getAllRefData,
      setFinancialDocumentsDependencies,
    },
    dispatch
  );

export default withStyles(styles)(
  withRootHoc(connect(mapStateToProps, mapDispatchToProps)(Journey))
);
