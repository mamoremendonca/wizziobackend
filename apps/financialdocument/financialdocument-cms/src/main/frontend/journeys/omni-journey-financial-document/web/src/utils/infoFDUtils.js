import React from "react";
import Typography from "@ui-lib/core/Typography";
import * as fdConstants from "../constants/FDConstants";
import { getFormattedNumber } from "../utils";

export const getDateDiff = (dateToCompare, classes) => {
  const actualDate = new Date();
  const compareDate = new Date(dateToCompare);

  //Convert to UTC
  const actualDateUTC = Date.UTC(
    actualDate.getFullYear(),
    actualDate.getMonth(),
    actualDate.getDate()
  );
  const compareDateUTC = Date.UTC(
    compareDate.getFullYear(),
    compareDate.getMonth(),
    compareDate.getDate()
  );

  const diffTime = compareDateUTC - actualDateUTC;
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

  if (diffDays > 7) {
    return undefined;
  } else if (diffDays >= 0) {
    return (
      <Typography
        variant="h6"
        className={classes.dateDiffAbove}
      >{`${"+"} ${Math.abs(diffDays)} dias`}</Typography>
    );
  } else if (diffDays < 0) {
    return (
      <Typography
        variant="h6"
        className={classes.dateDiffBelow}
      >{`${"-"} ${Math.abs(diffDays)} dias`}</Typography>
    );
  }
};

export const dateFormatter = (dateAsString) => {
  if (dateAsString) {
    const fdDate = new Date(dateAsString);
    return `${fdDate.getDate()}/${
      fdDate.getMonth() + 1
    }/${fdDate.getFullYear()}`;
  }
  return "-";
};

export const calculateTotalItemsValue = (refData, financialDocumentData) => {
  let total = 0;
  let retentionTotal = 0;
  financialDocumentData.items.forEach((item) => {
    const tax = refData.tax.find(
      (taxObject) => taxObject.code === item.taxCode
    );
    if (tax) {
      total +=
        item.unitPriceWithoutTax *
        item.amount *
        (1 + parseFloat(tax.taxPercentage) / 100);
    }
  });
  if (
    financialDocumentData.financialDocumentHeader.type ===
    fdConstants.FINANCIAL_DOCUMENT_TYPE_VALUES.RETENTION_INVOICE
  ) {
    financialDocumentData.items.forEach((item) => {
      const retention = refData.retentionCodes.find(
        (retentionObject) => retentionObject.code === item.retentionCode
      );

      if (retention) {
        retentionTotal +=
          (item.unitPriceWithoutTax *
            item.amount *
            parseFloat(retention.taxPercentage)) /
          100;
      }
    });
  }
  return getFormattedNumber(total - retentionTotal, true, "EUR");
};
