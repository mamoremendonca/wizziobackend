export const ITEM_DIALOG_ACTIONS = {
  ADD_ITEM: "addItem",
  EDIT_ITEM: "editItem",
};
