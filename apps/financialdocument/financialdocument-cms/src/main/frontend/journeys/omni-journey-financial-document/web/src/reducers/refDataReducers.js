import * as constants from "../constants";

const initialState = {
  refData: {},
  message: "",
};

export const refDataReducer = (state, action) => {
  if (state === undefined) return initialState;

  switch (action.type) {
    case constants.SET_REFDATA: {
      const refData = action.payload;
      return { ...state, refData };
    }
    default:
      return state;
  }
};
