export const ComponentName = "Countdown";
export const styles = (theme) => ({
  dateDiffAbove: {
    display: "flex",
    alignSelf: "center",
    color: theme.palette.common.white,
    backgroundColor: "#E79208",
    padding: `0px ${theme.spacing.unit}px`,
    borderRadius: theme.spacing.unit * 3,
    position: "absolute",
    left: theme.spacing.unit * 10 + 10,
    top: theme.spacing.unit * 3,
  },
  dateDiffBelow: {
    display: "flex",
    alignSelf: "center",
    color: theme.palette.common.white,
    backgroundColor: "#D92727",
    padding: `0px ${theme.spacing.unit}px`,
    borderRadius: theme.spacing.unit * 3,
    position: "absolute",
    left: theme.spacing.unit * 10 + 10,
    top: theme.spacing.unit * 3,
  },
});
