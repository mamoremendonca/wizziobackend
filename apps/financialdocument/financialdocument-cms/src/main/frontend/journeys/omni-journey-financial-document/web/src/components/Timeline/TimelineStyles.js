export const componentName = "TimelineStyles";
export const styles = (theme) => ({
  root: { height: "100%", backgroundColor: theme.palette.grey["100"] },
  header: { backgroundColor: theme.palette.common.white },
  flexRowContainer: { display: "flex", flexDirection: "row" },
  iconContainer: { padding: 20, backgroundColor: theme.palette.primary.main },
  icon: {
    color: theme.palette.common.white,
  },
  headerTile: {
    display: "flex",
    backgroundColor: theme.palette.primary["50"],
  },
  headerDescription: { padding: "20px" },
  headerTab: {
    display: "flex",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    padding: theme.spacing.unit * 2,
    cursor: "pointer",
  },
  selectedHeaderTab: {
    display: "flex",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    padding: theme.spacing.unit * 2,
    color: theme.palette.primary.main,
    borderBottom: `2px solid ${theme.palette.primary.main}`,
    cursor: "pointer",
  },
  timelineActionsContainer: {
    position: "absolute",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit}px 0px 0px`,
    right: 0,
    display: "flex",
    flexDirection: "row",
  },
  refreshIconButton: {
    fontSize: 24,
    cursor: "pointer",
    padding: 5,
    bottom: 5,
    transitionDuration: "0.8s",
    transitionProperty: "transform",
    "&:hover": {
      transform: "rotate(90deg)",
      color: theme.palette.primary.main,
    },
  },
  contentComponentContainer: {
    paddingTop: theme.spacing.unit * 2,
  },
});
