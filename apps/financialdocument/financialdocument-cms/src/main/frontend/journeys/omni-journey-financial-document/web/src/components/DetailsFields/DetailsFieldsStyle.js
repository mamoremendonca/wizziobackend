export const componentName = "DetailsFieldsStyle";
export const styles = (theme) => ({
  root: {
    minHeight: "100%",
    display: "grid",
    gridTemplateRows: "auto auto auto auto",
    gridTemplateColumns: "100%",
    padding: "10px 0px",
  },
  gridRow: {
    display: "flex",
  },
  disabledInfoField: {
    paddingBottom: theme.spacing.unit * 2,
    "& .MuiInputBase-root.MuiInputBase-disabled ": {
      color: theme.palette.text.primary,
    },
    "& .MuiFormLabel-root.MuiFormLabel-disabled": {
      color: theme.palette.text.secondary,
    },
    "& .MuiInput-underline.MuiInput-disabled:before": {
      borderBottomStyle: "none",
    },
  },
  fieldContainerGrid: {
    display: "flex",
    flexDirection: "row",
    position: "relative",
  },
});
