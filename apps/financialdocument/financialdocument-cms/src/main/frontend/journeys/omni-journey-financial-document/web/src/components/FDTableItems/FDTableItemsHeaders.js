import {
  PURCHASE_ORDER_TYPES,
  FINANCIAL_DOCUMENT_TYPE_VALUES,
} from "../../constants";

//todo import of that adds the tooltip for the states
const FDTableItemsHeaders = (
  i18nProvider,
  financialDocumentHeader,
  handleCopy,
  handleEdit
) => {
  let headers = [];
  headers.push({
    label: i18nProvider.Labeli18n("version"),
    key: "version",
    align: "center",
  });
  if (
    financialDocumentHeader.purchaseOrderType === PURCHASE_ORDER_TYPES.PO_CAPEX
  ) {
    headers.push({
      label: i18nProvider.Labeli18n("asset"),
      key: "article",
      align: "center",
    });
  } else {
    headers.push({
      label: i18nProvider.Labeli18n("articleType"),
      key: "articleType",
      align: "center",
    });
    headers.push({
      label: i18nProvider.Labeli18n("article"),
      key: "article",
      align: "center",
    });
  }
  headers.push({
    label: i18nProvider.Labeli18n("description"),
    key: "description",
    align: "center",
  });
  financialDocumentHeader.purchaseOrderType !== PURCHASE_ORDER_TYPES.PO_CAPEX &&
    headers.push({
      label: i18nProvider.Labeli18n("costCenter"),
      key: "projectCode",
      align: "center",
    });
  headers.push({
    label: i18nProvider.Labeli18n("quantity"),
    key: "quantity",
    align: "center",
  });
  headers.push({
    label: i18nProvider.Labeli18n("unitPriceWithoutTax"),
    key: "unitPriceWithoutTax",
    align: "center",
  });
  headers.push({
    label: i18nProvider.Labeli18n("itemPriceWithoutTax"),
    key: "itemPriceWithoutTax",
    align: "center",
  });
  headers.push({
    label: i18nProvider.Labeli18n("VAT"),
    key: "VAT",
    align: "center",
  });
  headers.push({
    label: i18nProvider.Labeli18n("itemPriceWithTax"),
    key: "itemPriceWithTax",
    align: "center",
  });
  if (
    financialDocumentHeader.type ===
    FINANCIAL_DOCUMENT_TYPE_VALUES.RETENTION_INVOICE
  ) {
    headers.push({
      label: i18nProvider.Labeli18n("retentionPercentage"),
      key: "retentionPercentage",
      align: "center",
    });
    headers.push({
      label: i18nProvider.Labeli18n("retentionValue"),
      key: "retentionValue",
      align: "center",
    });
    headers.push({
      label: i18nProvider.Labeli18n("finalAmount"),
      key: "finalAmount",
      align: "center",
    });
  }
  if (financialDocumentHeader.granting) {
    headers.push({
      label: i18nProvider.Labeli18n("startDate"),
      key: "startDate",
      align: "center",
    });
    headers.push({
      label: i18nProvider.Labeli18n("endDate"),
      key: "endDate",
      align: "center",
    });
  }
  if (handleCopy) {
    headers.push({
      label: "",
      key: "copyButton",
      align: "center",
    });
  }
  if (handleEdit) {
    headers.push({
      label: "",
      key: "editButton",
      align: "center",
    });
  }
  return headers;
};

export default FDTableItemsHeaders;
