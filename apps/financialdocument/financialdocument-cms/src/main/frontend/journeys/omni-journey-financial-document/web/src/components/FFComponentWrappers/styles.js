const styles = (theme) => ({
  root: {
    height: "100%",
    display: "flex",
    paddingBottom: 3,
  },
  textFieldDisabled: {
    paddingBottom: theme.spacing.unit * 2,
    "& .MuiInputBase-root.MuiInputBase-disabled ": {
      color: theme.palette.text.primary,
    },
    "& .MuiFormLabel-root.MuiFormLabel-disabled": {
      color: theme.palette.text.secondary,
    },
    "& .MuiInput-underline.MuiInput-disabled:before": {
      borderBottomStyle: "none",
    },
  },
});

export default styles;
