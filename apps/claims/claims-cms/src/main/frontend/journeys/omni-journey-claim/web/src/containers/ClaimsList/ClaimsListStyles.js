const ComponentName = 'ClaimsListStyle'
const styles = (theme) => ({
    root: {
        marginTop: '22px'
    },
    mainContent: {
        margin: '0px 20px',
        width: '100%'
    },
    icon: {
        marginLeft: '10px'
    },
    tableLabel: {
        marginRight: '10px'
    },
    tableNumbers: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flexStart',
        alignItems: 'center',
        margin: '20px 0px 10px'
    },
    waitingApprovalStatus: {
        display: 'block',
        height: 10,
        width: 10,
        borderRadius: '50%',
        backgroundColor: '#E79208'
    },
    sap_submittedStatus: {
        display: 'block',
        height: 10,
        width: 10,
        borderRadius: '50%',
        backgroundColor: '#6F1D46'
    },
    approvedStatus: {
        display: 'block',
        height: 10,
        width: 10,
        borderRadius: '50%',
        backgroundColor: '#81BC00'
    },
    canceledStatus: {
        display: 'block',
        height: 10,
        width: 10,
        borderRadius: '50%',
        backgroundColor: '#002B49'
    },
    rejectedStatus: {
        display: 'block',
        height: 10,
        width: 10,
        borderRadius: '50%',
        backgroundColor: '#26AEE5'
    },
    paidStatus: {
        display: 'block',
        height: 10,
        width: 10,
        borderRadius: '50%',
        backgroundColor: '#000000'
    },
    statsAlign: {
        marginLeft: '27px'
    },
    increaseTooltip: {
        fontSize: 13
    },
    collapseFilter: {
        width: '100%'
    },
    filterIcon: {
        float: 'right',
        marginRight: 8.24,
        color: '#81BC00'
    },
    claimStatusCircleArea: {
        marginLeft: '24px'
    }
})

export { ComponentName, styles }
