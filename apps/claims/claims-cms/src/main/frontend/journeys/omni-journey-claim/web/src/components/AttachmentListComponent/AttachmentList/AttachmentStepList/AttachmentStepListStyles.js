const ComponentName = 'AttachmentStepListStyles'
const styles = (theme) => ({
    root: {},
    header: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    divider: {
        width: theme.spacing.unit / 2,
        height: theme.spacing.unit / 2,
        backgroundColor: theme.palette.grey[500],
        borderRadius: '50%',
        margin: `0px ${theme.spacing.unit * 3}px`
    }
})

export { ComponentName, styles }
