// State of Claim Object
const ClaimState = {
    NEW: 0,
    ASSIGNED: 1,
    IN_PROGRESS: 2,
    FORWARD: 3,
    WAITING_PARTNER: 4,
    SOLVED: 5,
    WITHOUT_TRACE: 6
}

/**
 * Receives The Status of The Claim and Returns a Color
 * @param {*} status
 */
const claimStatusColor = (status) => {
    switch (status) {
        case ClaimState.NEW:
            return '#E79208'
        case ClaimState.ASSIGNED:
            return '#E1AD01'
        case ClaimState.IN_PROGRESS:
            return '#72c437'
        case ClaimState.FORWARD:
            return '#26aee5'
        case ClaimState.WAITING_PARTNER:
            return '#4365e0'
        case ClaimState.SOLVED:
            return '#4237c4'
        case ClaimState.WITHOUT_TRACE:
            return '#c437af'
        default:
            return '#FFF'
    }
}

/**
 * Receives The Status of The Claim and Returns an Icon for The Timeline
 * @param {*} status
 */
const getTimelineStepIcon = (status) => {
    switch (status) {
        case 'SUBMITTED':
            return 'view_headline'
        case 'APPROVED':
            return 'check'
        case 'REJECTED':
            return 'close'
        case 'RETURNED':
            return 'keyboard_return'
        case 'DRAFT':
            return 'lens'
        case 'TO_APPROVE':
        case 'PREVISION':
            return 'access_time'
        case 'JUMP':
            return 'not_interested'
        default:
    }
}

export { ClaimState, claimStatusColor, getTimelineStepIcon }
