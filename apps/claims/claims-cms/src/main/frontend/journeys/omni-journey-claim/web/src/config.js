//Configuration
const Config = {
    //#region REST METHODS
    METHOD_POST: 'post',
    METHOD_GET: 'get',
    METHOD_PUT: 'put',
    METHOD_DELETE: 'delete',
    //#endregion
    CLAIM_BASE_URL: '/bin/mvc.do/claim/v1',
    CLAIM_REPOSITORY: '/repository',
    ONEY_BASE_URL: '/bin/mvc.do/oneyapp/v1',
    CLAIM_LIST_URL_PART: '/list'
}

export { Config }
