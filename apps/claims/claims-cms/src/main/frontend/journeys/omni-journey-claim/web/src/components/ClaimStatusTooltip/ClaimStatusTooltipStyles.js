const ComponentName = 'ClaimStateTooltip'

const styles = (theme) => ({
    header: {
        marginBottom: '5px'
    },
    statusIcon: {
        fontSize: '16px'
    },
    statusText: {
        marginLeft: '10px'
    }
})

export { ComponentName, styles }
