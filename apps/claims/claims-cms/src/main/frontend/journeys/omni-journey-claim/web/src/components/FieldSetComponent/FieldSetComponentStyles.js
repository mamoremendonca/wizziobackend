const ComponentName = 'FieldSetComponent'

const styles = () => ({
    root: {
        width: '100%',
        marginTop: '40px'
    },
    title: {
        paddingBottom: '10px'
    },
    content: {}
})

export { ComponentName, styles }
