//Regex Patterns
const isDecimal = /^\d+(\.\d+)?$/
const allowDecimalWithComma = /^\d+(,\d+)?$/ //Allow introduce, but will be banned at submit the filter
const isDecimalWithComma = /[0-9]+[,]\d+/
const numberEndsWithDot = /[0-9]+[.,]$/
const isNumeric = /[0-9]+$/
const isAlphaNumeric = /^[a-zA-Z0-9_]*$/

export {
    isDecimal,
    allowDecimalWithComma,
    isDecimalWithComma,
    numberEndsWithDot,
    isNumeric,
    isAlphaNumeric
}
