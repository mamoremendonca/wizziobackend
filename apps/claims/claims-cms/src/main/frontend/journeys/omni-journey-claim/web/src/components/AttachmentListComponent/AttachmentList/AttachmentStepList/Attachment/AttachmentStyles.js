const ComponentName = 'AttachmentStyles'
const styles = (theme) => ({
    root: {
        display: 'flex',
        backgroundColor: theme.palette.grey[100],
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px`,
        margin: `${theme.spacing.unit * 2}px 0px`
    },
    deleteAttachment: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    circularProgress: {
        marginBottom: `${theme.spacing.unit * 2}px`,
        width: `${theme.spacing.unit * 3}px !important`,
        height: `${theme.spacing.unit * 3}px !important`
    },
    closeIcon: {
        fontSize: 24,
        cursor: 'pointer',
        padding: 0,
        '&:hover': {
            color: theme.palette.primary.main
        }
    },
    attachmentContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    attachmentHeader: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    attachmentTitle: {
        paddingLeft: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        '&:hover': {
            color: theme.palette.primary.main,
            textDecorationLine: 'underline'
        },
        cursor: 'pointer'
    },
    attachmentIcon: {
        fontSize: 16,
        transform: 'rotate(45deg)'
    },
    attachmentComment: {
        margin: theme.spacing.unit * 2
    },
    commentContent: {
        display: '-webkit-box',
        '-webkit-line-clamp': 2,
        '-webkit-box-orient': 'vertical',
        overflow: 'hidden',
        textOverflow: 'ellipsis'
    }
})

export { ComponentName, styles }
