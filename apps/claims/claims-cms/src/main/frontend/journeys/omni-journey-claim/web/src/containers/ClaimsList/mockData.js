const claimsListData = [
    {
        id: 1,
        repositoryStatus: 1,
        number: 1234567890,
        origin: 'Centro de contactos',
        channel: 'Presencial',
        motive: 'Cobrança',
        subMotive: 'Cobrança indevida',
        client: 'A. Silva',
        ammount: 2000,
        creationDate: '2020-07-10T12:00:00.000Z',
        expirationDate: '2020-07-23T12:00:00.000Z',
        userGroup: null
    },
    {
        id: 2,
        repositoryStatus: 2,
        number: 99933336661,
        origin: 'Centro de contactos',
        channel: 'Presencial',
        motive: 'Cobrança',
        subMotive: 'Cobrança indevida',
        client: 'J. Almeida',
        ammount: 4000,
        creationDate: '2020-07-08T12:00:00.000Z',
        expirationDate: '2020-07-16T12:00:00.000Z',
        userGroup: null
    }
]

export { claimsListData }
