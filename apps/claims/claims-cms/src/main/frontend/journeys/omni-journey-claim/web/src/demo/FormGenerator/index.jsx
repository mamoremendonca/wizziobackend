import React from 'react'
import FormGenerator, { FormProvider } from 'build-react-form'
import TestFormContext from '../../forms/TestFormContext'
import {
    FormInputs as ClaimsListDataFormInputs,
    sections as ClaimsListDataSections
} from '../../forms/ClaimListData'
import { FormInputs as ClaimListFilterFormInputs } from '../../forms/ClaimsListFilter'

const FormGeneratorDemo = () => (
    <FormProvider>
        <FormGenerator
            formId="claimsListFilter"
            inputOptions={ClaimListFilterFormInputs}
        />
        <FormGenerator
            formId="claimsListData"
            sections={ClaimsListDataSections}
            inputOptions={ClaimsListDataFormInputs}
        />
        <TestFormContext />
    </FormProvider>
)

export default FormGeneratorDemo
