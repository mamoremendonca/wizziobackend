import React from 'react'
import { connect } from 'react-redux'
// import { Config } from "./config";
import { withRootHoc } from 'omni-journey'
import ClaimAnalysis from './containers/ClaimAnalysis'
import ClaimsList from './containers/ClaimsList'
import ClaimHistory from './containers/ClaimHistory'
import FormGeneratorDemo from './demo/FormGenerator'
import LineChartPanelDemo from './demo/LineChartPanel'
import DoughnutStatusDemo from './demo/DoughnutStatus'
import FileUploaderDemo from './demo/FileUploader'
import CameraBrowserDemo from './demo/CameraBrowser'

const Journey = () => {
    return (
        <div>
            {/*TODO Necessario dar permissões de acesso à webcam
                Para isto é encessario incluir ' allow="camera;microfone" ', no iframe  */}
            <CameraBrowserDemo />
            <FileUploaderDemo />
            {/* TODO Not Working! */}
            {/* <DoughnutStatusDemo /> */}
            <LineChartPanelDemo />
            <FormGeneratorDemo />
            {/*TODO TO TEST */}
            <ClaimAnalysis />
            <ClaimHistory />
            <ClaimsList />
        </div>
    )
}

//Redux
const mapStateToProps = ({}) => ({})

const mapDispatchToProps = (dispatch) => ({})

export default withRootHoc(
    connect(mapStateToProps, mapDispatchToProps)(Journey)
)
