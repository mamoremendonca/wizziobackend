import React from 'react'
import Divider from '@ui-lib/core/Divider'
import Typography from '@ui-lib/core/Typography'
import { withStyles } from '@ui-lib/core/styles'
import { styles, ComponentName } from './FieldSetComponentStyles'
import Grid from '@ui-lib/core/Grid'

const FieldSetComponent = ({ title, actions, content, classes }) => {
    return (
        <div className={classes.root}>
            <Grid container justify={'space-between'}>
                <Typography
                    className={classes.title}
                    color={'primary'}
                    variant={'h4'}>
                    {title}
                </Typography>
                {actions}
            </Grid>
            <Divider />
            <div className={classes.content}>{content}</div>
        </div>
    )
}

export default withStyles(styles, { name: ComponentName })(FieldSetComponent)
