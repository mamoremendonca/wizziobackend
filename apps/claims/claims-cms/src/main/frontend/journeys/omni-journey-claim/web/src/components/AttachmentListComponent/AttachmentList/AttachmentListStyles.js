const ComponentName = 'AttachmentListStyles'

const styles = (theme) => ({
    root: {
        paddingTop: '20px',
        paddingBottom: '20px',
        height: '100%'
    }
})

export { ComponentName, styles }
