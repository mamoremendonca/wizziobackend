import * as constants from '../constants'
import { prepareSearchCriteria, prepareSortCriteria } from '../utils'
import { Config } from '../config'

const updateFilter = (newFilter) => ({
    type: constants.UPDATE_FILTER_CRITERIA,
    payload: newFilter
})

const updateSorting = (fieldToSort) => ({
    type: constants.UPDATE_SORTING_CRITERIA,
    payload: fieldToSort
})

const setClaimsList = (newClaimsList) => {
    return {
        type: constants.SET_CLAIMS_LIST_PROCESSES,
        payload: newClaimsList
    }
}

const getClaimsListProcesses = (httpClient, criteria) => {
    const config = {
        method: Config.METHOD_POST,
        url: `${Config.CLAIM_BASE_URL}${Config.CLAIM_REPOSITORY}${
            Config.CLAIM_LIST_URL_PART
        }${prepareSortCriteria(criteria.sortCriteria)}`,
        data: prepareSearchCriteria(criteria)
    }
    return (dispatch) => {
        return httpClient
            .request(config)
            .then((res) => {
                //Todo Logic of Mapping Processes and Totals for Each Process
                const payLoad = {
                    claimsList: res.data,
                    numberOfRecords: res.data.length,
                    totals: res.data.length
                }
                dispatch(setClaimsList(payLoad))
            })
            .catch((err) => {
                //TODO: httpErrorHandler(widget, err, false);
            })
    }
}

export { updateFilter, updateSorting, setClaimsList, getClaimsListProcesses }
