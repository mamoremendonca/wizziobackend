import React, { useState, useEffect } from 'react'
import FilterListRender from './FilterListRender'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
    comboTranslateRefDataToDataSources,
    isDecimal,
    numberEndsWithDot,
    isNumeric,
    isDecimalWithComma,
    allowDecimalWithComma
} from '../../utils'
import { Map } from 'immutable'
import { updateFilter } from '../../actions'
import { INITIAL_FILTER_CRITERIA } from '../../constants'

/**
 * HOC to render a component with custom fields that is used to collect the filters used in the server request to get the filtered data.
 */

const FilterList = ({
    I18nProvider,
    updateFilter,
    onFilterClick,
    searchCriteria,
    refData,
    suppliers
}) => {
    const [filterFieldsValue, setFilterFieldsValue] = useState({
        ...INITIAL_FILTER_CRITERIA
    })
    const [invalidFields, setInvalidFields] = useState(
        Map({ minVATValue: false, maxVATValue: false })
    )
    const [errorMessages, setErrorMessages] = useState(
        Map({
            minValueErrorMessage: '',
            maxValueErrorMessage: ''
        })
    )

    useEffect(() => {
        //console.log('filtros de pesquisa: ' + filterFieldsValue)
    }, filterFieldsValue)

    /**
     * Helper function used in the AutocompleteInput to get the i18n resources.
     * @param {*} key
     * @param {*} defaultLabel
     */
    const getLabelFn = (key, defaultLabel) => {
        return I18nProvider.Texti18n(key)
    }

    /**
     * To change the filter fields in the component state.
     * For numeric values is not allowed other characters.
     * @param {*} newValue
     * @param {*} fieldName
     */
    const handleChange = (newValue, fieldName) => {
        if (isValidInput(newValue, fieldName)) {
            const newState = filterFieldsValue
            newState[fieldName] = newValue
            setFilterFieldsValue(newState)
        }
    }

    const isValidInput = (value, fieldName) => {
        switch (fieldName) {
            case 'minVATValue':
            case 'maxVATValue':
                return (
                    !value ||
                    isDecimal.test(value) ||
                    numberEndsWithDot.test(value) ||
                    allowDecimalWithComma.test(value)
                )
            case 'purchaseOrderNumber':
                return !value || isNumeric.test(value)
            default:
                return true
        }
    }

    const isValidMinAndMaxFields = () => {
        const maxValueState = {
            name: 'maxVATValue',
            isInvalid: false,
            errorMessage: ''
        }
        const minValueState = {
            name: 'minVATValue',
            isInvalid: false,
            errorMessage: ''
        }
        //Data Input validations
        if (numberEndsWithDot.test(filterFieldsValue.maxVATValue)) {
            maxValueState.isInvalid = true
            maxValueState.errorMessage = 'listFD.filter.missing_decimal_digit'
        } else if (isDecimalWithComma.test(filterFieldsValue.maxVATValue)) {
            maxValueState.isInvalid = true
            maxValueState.errorMessage =
                'listFD.filter.invalid_decimal_separator'
        }

        if (numberEndsWithDot.test(filterFieldsValue.minVATValue)) {
            minValueState.isInvalid = true
            minValueState.errorMessage = 'listFD.filter.missing_decimal_digit'
        } else if (isDecimalWithComma.test(filterFieldsValue.minVATValue)) {
            minValueState.isInvalid = true
            minValueState.errorMessage =
                'listFD.filter.invalid_decimal_separator'
        }

        //Business Validations
        if (
            !(minValueState.isInvalid || maxValueState.isInvalid) &&
            filterFieldsValue.minVATValue &&
            filterFieldsValue.maxVATValue
        ) {
            if (
                parseFloat(filterFieldsValue.minVATValue) >
                parseFloat(filterFieldsValue.maxVATValue)
            ) {
                maxValueState.isInvalid = true
                minValueState.isInvalid = true
                minValueState.errorMessage =
                    I18nProvider.Texti18n(
                        'listFD.filter.total.field.invalid_range'
                    ) + filterFieldsValue.maxVATValue
            }
        }
        updateErrorFieldStates([minValueState, maxValueState])
        return !(minValueState.isInvalid || maxValueState.isInvalid)
    }

    const updateErrorFieldStates = (fields) => {
        if (fields.length > 0) {
            const field = fields.pop()
            setInvalidFields(invalidFields.set(field.name, field.isInvalid))
            setErrorMessages(errorMessages.set(field.name, field.errorMessage))
        }
    }

    /**
     * To call the parent method that receives a filter before call the service * that gets the new data.
     */
    const handleFilterClick = () => {
        if (isValidMinAndMaxFields()) {
            updateFilter({ ...filterFieldsValue })
            if (onFilterClick) {
                onFilterClick({
                    ...searchCriteria,
                    filterCriteria: { ...filterFieldsValue }
                })
            }
        }
    }

    /**
     * Clear the filter fields value and make a new request to get data from * * server without the filters.
     */
    const handleClearFilterClick = () => {
        setFilterFieldsValue({ ...INITIAL_FILTER_CRITERIA })
        handleFilterClick()
    }

    const getDataSourceList = (refData, suppliers, I18nProvider) => {
        let dataSourceList = {}
        // dataSourceList.status = comboTranslateRefDataToDataSources(
        //   refData.status,
        //   I18nProvider
        // );
        // dataSourceList.projects = comboTranslateRefDataToDataSources(
        //   refData.projects,
        //   I18nProvider
        // );
        // dataSourceList.departments = comboTranslateRefDataToDataSources(
        //   refData.departments,
        //   I18nProvider
        // );
        // dataSourceList.internalOrders = comboTranslateRefDataToDataSources(
        //   refData.internalOrders,
        //   I18nProvider
        // );
        // dataSourceList.financialDocumentTypes = comboTranslateRefDataToDataSources(
        //   refData.financialDocumentTypes,
        //   I18nProvider
        // );
        // dataSourceList.suppliers = comboTranslateRefDataToDataSources(
        //   refData.suppliers,
        //   I18nProvider
        // );
        return dataSourceList
    }

    return (
        <FilterListRender
            filterFieldsValue={filterFieldsValue}
            dataSourceList={getDataSourceList(refData, suppliers, I18nProvider)}
            onFilterClick={handleFilterClick}
            onClearFilterClick={handleClearFilterClick}
            onChangeFilter={handleChange}
            I18nProvider={I18nProvider}
            invalidFields={invalidFields}
            errorMessages={errorMessages}
            getLabelFn={getLabelFn}
        />
    )
}

//Redux
const mapStateToProps = ({
    journey: {
        services: { I18nProvider }
    },
    i18n
    // refDataReducer: { refData, suppliers },
    // financialDocumentReducer: { searchCriteria },
}) => ({
    I18nProvider,
    i18n
    // refData,
    // suppliers,
    // searchCriteria,
})
const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            updateFilter
        },
        dispatch
    )

export default connect(mapStateToProps, mapDispatchToProps)(FilterList)
