import React from 'react'
import { withStyles } from '@ui-lib/core/styles'
import { styles, ComponentName } from './ClaimStatusCircleStyles'
import { claimStatusColor } from '../../utils/claim'

const ClaimStatusCircle = ({ classes, status }) => {
    return (
        <div
            className={classes.statusCircle}
            style={{ backgroundColor: claimStatusColor(status) }}
        />
    )
}

export default withStyles(styles, { name: ComponentName })(ClaimStatusCircle)
