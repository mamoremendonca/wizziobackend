import * as React from 'react'
import { withStyles } from '@ui-lib/core/styles'
import styles from './styles'
import Grid from '@ui-lib/core/Grid'
import Button from '@ui-lib/core/Button'
import Icon from '@ui-lib/core/Icon'

const HeaderActionButtons = ({ classes }) => {
    return (
        <Grid container item>
            <Grid container justify="flex-end" item>
                <Button className={classes.filterIconButton}>
                    <div className={classes.filterIconButtonContent}>
                        <Icon className={'icon-filter'}></Icon>
                        <span>Cliente</span>
                    </div>
                </Button>
                <Button className={classes.filterIconButton}>
                    <div className={classes.filterIconButtonContent}>
                        <Icon className={'icon-filter'}></Icon>
                        <span>Encaminhar</span>
                    </div>
                </Button>
                <Button className={classes.filterIconButton}>
                    <div className={classes.filterIconButtonContent}>
                        <Icon className={'icon-filter'}></Icon>
                        <span>Pausar</span>
                    </div>
                </Button>
                <Button className={classes.filterIconButton}>
                    <div className={classes.filterIconButtonContent}>
                        <Icon className={'icon-filter'}></Icon>
                        <span>Adiar SLA</span>
                    </div>
                </Button>
                <Button className={classes.filterIconButton}>
                    <div className={classes.filterIconButtonContent}>
                        <Icon className={'icon-filter'}></Icon>
                        <span>Sem seguimento</span>
                    </div>
                </Button>
                <Button className={classes.filterIconButton}>
                    <div className={classes.filterIconButtonContent}>
                        <Icon className={'icon-filter'}></Icon>
                        <span>Concluir</span>
                    </div>
                </Button>
            </Grid>
        </Grid>
    )
}

export default withStyles(styles)(HeaderActionButtons)
