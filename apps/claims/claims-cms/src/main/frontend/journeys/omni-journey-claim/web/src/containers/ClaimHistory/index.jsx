import React, { Fragment } from 'react'
import Grid from '@ui-lib/core/Grid'
import HeaderBar from '../../components/HeaderBar'
import { withStyles } from '@ui-lib/core/styles'
import { styles, ComponentName } from './ClaimAnalysisStyles'
import NavigationBar from './NavigationBar'

const ClaimHistory = ({ classes }) => {
    return (
        <Fragment>
            <Grid className={classes.root}>
                <HeaderBar title={'ClaimHistory'}></HeaderBar>
                <NavigationBar />
            </Grid>
        </Fragment>
    )
}

export default withStyles(styles, { name: ComponentName })(ClaimHistory)
