import React from 'react'
import FieldSetComponent from '../../../components/FieldSetComponent'

const DetailsSection = () => {
    return (
        <div
            className={'claim-analysis-client-details'}
            style={{ height: '300px', background: '#ffa7a7' }}>
            <FieldSetComponent
                title={'Dados da Reclamação'}
                content={'Dados'}
            />
        </div>
    )
}

export default DetailsSection
