import React from 'react'
import FieldSetComponent from '../FieldSetComponent'
import ActivityTimelineStep from './ActivityTimelineStep'
import IconButton from '@ui-lib/core/IconButton'
import Icon from '@ui-lib/core/Icon'
import Grid from '@ui-lib/core/Grid'
import { withStyles } from '@ui-lib/core/styles'
import { styles, ComponentName } from './ActivityTimelineStyles'

const ActivityTimeline = ({ classes }) => {
    return (
        <div className={classes.root}>
            <FieldSetComponent
                title={'Atividades'}
                content={<ActivityTimelineStep />}
                actions={
                    <Grid
                        container
                        wrap={'nowrap'}
                        className={classes.actionArea}>
                        <IconButton
                            className={classes.actionButton}
                            disabled={false}
                            onClick={() => {}}>
                            <Icon>AUTORENEW</Icon>
                        </IconButton>
                    </Grid>
                }
            />
        </div>
    )
}

export default withStyles(styles, { name: ComponentName })(ActivityTimeline)
