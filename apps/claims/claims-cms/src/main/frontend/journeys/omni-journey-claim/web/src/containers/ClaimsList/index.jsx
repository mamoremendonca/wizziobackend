import React, { useState } from 'react'
import ClaimsListHeader from './ClaimsListHeader'
import ClaimsListFilter from './ClaimsListFilter'
import ClaimsListTable from './ClaimsListTable'
import withContainerWrapper from '../../components/ContainerWrapper'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getClaimsListProcesses, updateSorting } from '../../actions'
import Grid from '@ui-lib/core/Grid'
import { claimsListData } from './mockData'

/**
 * Toggle the Expand Filter
 */
const ClaimsList = ({
    classes,
    I18nProvider,
    i18n,
    HttpClient,
    claimsList,
    searchCriteria,
    totalClaims,
    numberOfRecords,
    refData,
    suppliers,
    Loading,
    EntitlementsVerifier
}) => {
    const [expandFilter, setExpandFilter] = useState(true)
    const [api, setTableApi] = useState(null)

    /**
     * Navigates to the PO Details Screen
     * @param {*} row
     */
    const handleRowClick = (row) => {
        // this.navigateToPage("detailsClaim", row.id);
    }

    /**
     * Makes a request to the server to fetch the PO data
     * the service must return the total existent records.
     * @param {*} searchCriteria
     */
    const getClaimsFromServer = (searchCriteria) => {
        getClaimsListProcesses(HttpClient, searchCriteria)
    }

    /**
     * Direction Can Be Asc or Desc
     * @param {*} headerKey
     * @param {*} directionResponseOk
     * @param {*} resolve
     * @param {*} error
     */
    const sortHandler = (headerKey, directionResponseOk, resolve, error) => {
        const sorting = { ...searchCriteria.sortCriteria }
        sorting.orderField = headerKey
        // Hack to solve problem with arrow sort
        if (
            searchCriteria.sortCriteria.orderType === 'desc' &&
            directionResponseOk === 'desc'
        ) {
            sorting.orderType = 'asc'
            error()
        } else {
            sorting.orderType = directionResponseOk
            resolve()
        }
        updateSorting({ ...sorting })
        resolve()
        getClaimsFromServer({
            ...searchCriteria,
            sortCriteria: { ...sorting }
        })
    }

    return (
        <div>
            <ClaimsListHeader
                I18nProvider={I18nProvider}
                onExpandFilterClick={() => setExpandFilter(!expandFilter)}
            />

            <div>
                <Grid container spacing={0}>
                    <ClaimsListFilter
                        I18nProvider={I18nProvider}
                        expandFilter={expandFilter}
                    />
                    <ClaimsListTable
                        I18nProvider={I18nProvider}
                        claimsList={
                            claimsList.length ? claimsList : claimsListData
                        }
                        totalClaims={totalClaims}
                        numberOfRecords={numberOfRecords}
                        setTableApi={setTableApi}
                        handleRowClick={() => handleRowClick}
                        sortHandler={() => sortHandler}
                    />
                </Grid>
            </div>
        </div>
    )
}

//Redux
const mapStateToProps = ({
    journey: {
        services: { I18nProvider, HttpClient, Loading, EntitlementsVerifier }
    },
    i18n,
    claimsReducer: { claimsList, searchCriteria, totalClaims, numberOfRecords }
    // refDataReducer: { refData, suppliers },
}) => ({
    I18nProvider,
    i18n,
    HttpClient,
    claimsList,
    searchCriteria,
    totalClaims,
    numberOfRecords,
    // refData,
    // suppliers,
    Loading,
    EntitlementsVerifier
})

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            getClaimsListProcesses,
            updateSorting
        },
        dispatch
    )

export default connect(mapStateToProps, mapDispatchToProps)(ClaimsList)
