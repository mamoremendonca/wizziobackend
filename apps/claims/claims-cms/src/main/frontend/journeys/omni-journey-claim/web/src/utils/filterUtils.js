import { SORT_FIELDS_MAPPER, SORTING_ORDER_MAPPER } from '../constants'
import qs from 'qs'

/**
 * Receives Search Criteria and Then Return It
 * @param {*} searchCriteria
 */
const prepareSearchCriteria = (searchCriteria) => {
    const serverSearchCriteria = {}
    if (searchCriteria.filterCriteria) {
        let filterParams = Object.keys(searchCriteria.filterCriteria)
        filterParams.forEach((param) => {
            serverSearchCriteria[param] =
                searchCriteria.filterCriteria[param] &&
                searchCriteria.filterCriteria[param].length !== 0
                    ? searchCriteria.filterCriteria[param]
                    : null
        })
    }
    return serverSearchCriteria
}

/**
 * Receives Sort Criteria and Then Return It
 * @param {*} sortCriteria
 */
const prepareSortCriteria = (sortCriteria) => {
    const sort = {
        orderField: SORT_FIELDS_MAPPER[sortCriteria.orderField],
        orderType: SORTING_ORDER_MAPPER[sortCriteria.orderType]
    }
    return '?' + qs.stringify(sort, { filter: ['orderField', 'orderType'] })
}

export { prepareSearchCriteria, prepareSortCriteria }
