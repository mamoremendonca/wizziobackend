/**
 * Only use for @ui-lib/oney/Autocomplete
 * To translate the description using i18nProvider.
 * In the autocomplete component the values are put in the keys and the descriptions are put in the value.
 * @param {*} refData
 */
const autoCompleteTranslateRefDataToDataSources = (refData, i18nProvider) => {
    const dataSource = []
    if (refData) {
        refData.forEach((element) => {
            dataSource.push({
                key: element.code,
                value: i18nProvider.Texti18n(element.description)
            })
        })
    }
    return dataSource
}

/**
 * For other combos
 * To translate the description using i18nProvider
 * @param {*} refData
 */
const comboTranslateRefDataToDataSources = (refData, i18nProvider) => {
    const dataSource = []
    if (refData) {
        refData.forEach((element) => {
            dataSource.push({
                value: element.code,
                label: i18nProvider.Texti18n(element.description)
            })
        })
    }
    return dataSource
}

export {
    autoCompleteTranslateRefDataToDataSources,
    comboTranslateRefDataToDataSources
}
