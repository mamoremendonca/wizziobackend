const CurrencyType = {
    EUR: '€',
    USD: '$'
}

/**
 * Returns Formatted Number With Currency
 * @param {float} num
 * @param {*} decimal
 * @param {string} currency
 */
const getFormattedNumber = (num, decimal, currency) => {
    num = !decimal ? num : parseFloat(num).toFixed(2)
    return new Intl.NumberFormat(
        'es-ES',
        currency && {
            style: 'currency',
            currency: currency
        }
    ).format(num)
}

export { CurrencyType, getFormattedNumber }
