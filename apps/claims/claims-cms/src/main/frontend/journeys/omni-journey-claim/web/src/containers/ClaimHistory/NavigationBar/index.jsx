import React from 'react'
import Tabs from '@ui-lib/core/Tabs'
import Tab from '@ui-lib/core/Tab'
import Button from '@ui-lib/core/Button'
import Icon from '@ui-lib/core/Icon'
import Typography from '@ui-lib/core/Typography'

const NavigationBar = () => {
    const [selectedTab, setSelectedTab] = React.useState(0)

    const handleChangeTab = (event, newTab) => {
        setSelectedTab(newTab)
    }

    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'space-between',
                // borderBottom: "1px solid #bdbdbd",
                alignItems: 'flex-end'
            }}>
            <div style={{ width: '100%' }}>
                <Tabs
                    value={selectedTab}
                    onChange={handleChangeTab}
                    indicatorColor="primary"
                    textColor="primary"
                    scrollButtons="auto">
                    <Tab label="Todas (3)" />
                    <Tab label="A decorrer (0)" />
                    <Tab label="Concluídas (3)" />
                </Tabs>
            </div>
            <div style={{ borderBottom: '1px solid #bdbdbd' }}>
                <Button
                    variant="contained"
                    color="primary"
                    style={{
                        padding: '5px',
                        minWidth: '145px',
                        marginBottom: '15px'
                    }}>
                    <Icon
                        style={{
                            fontSize: '16px',
                            marginRight: '5px'
                        }}>
                        add_circle_outline
                    </Icon>
                    <Typography variant="h5">Nova Reclamação</Typography>
                </Button>
            </div>
        </div>
    )
}

export default NavigationBar
