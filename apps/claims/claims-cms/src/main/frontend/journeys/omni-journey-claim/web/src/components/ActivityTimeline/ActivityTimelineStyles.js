const ComponentName = 'ActivityTimeline'

const styles = (theme) => ({
    root: {
        paddingRight: '10px',
        width: '100%'
    },
    actionArea: {
        width: '30px',
        height: '30px',
        marginRight: '90px'
    },
    actionButton: {
        width: '100%'
    }
})

export { ComponentName, styles }
