import React from 'react'
import ActivityTimeline from '@ui-lib/oney/ActivityTimeline'
import Typography from '@ui-lib/core/Typography'
import { getTimelineStepIcon } from '../../../utils'

const getTimelineContent = (timelineData) => ({
    iconSize: '16',
    content: [
        {
            title: <Typography variant="h5">Análise de Reclamação</Typography>,
            date: '2020-06-22T15:00:41.076Z',
            selected: false,
            user: 'Miguel Gonçalves',
            icon: getTimelineStepIcon('PREVISION'), // stepData.date ? getTimelineStepIcon(stepData.type):getTimelineStepIcon("PREVISION")
            borderStyle: 'solid', // or dashed
            borderColor: '#0f0',
            items: [
                {
                    header: 'Atribuído a',
                    content: '-'
                },
                {
                    header: 'Data limite',
                    content: '13/09/20 às 18:00'
                }
            ],
            annexes: [
                {
                    id: 1,
                    fileName: 'Fatura.template.pdf',
                    uploader: 'Miguel Gonçalves',
                    date: '2020-06-22',
                    editMode: false
                }
            ]
        }
    ]
})

const downloadAttachment = async ({ id }) => {}

const ActivityTimelineStep = ({ timelineData }) => {
    return (
        <ActivityTimeline
            downloadClick={downloadAttachment}
            data={getTimelineContent(timelineData)}></ActivityTimeline>
    )
}

export default ActivityTimelineStep
