const translate_month = (month) => {
    let result = month

    switch (month) {
        case 'Feb':
            result = 'Fev'
            break
        case 'Apr':
            result = 'Abr'
            break
        case 'May':
            result = 'Mai'
            break
        case 'Aug':
            result = 'Ago'
            break
        case 'Sep':
            result = 'Set'
            break
        case 'Dec':
            result = 'Dez'
            break
    }

    return result
}

const translate_this_label = (label) => {
    const month = label.match(/Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Nov|Dec/g)

    if (!month) return label

    const translation = translate_month(month[0])
    return label.replace(month, translation, 'g')
}

const componentToHex = (c) => {
    const hex = c.toString(16)
    return hex.length == 1 ? '0' + hex : hex
}

const hexToRgb = (hex) => {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
    if (result) {
        const rgb = {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        }
        const rbgString = 'rgb(' + rgb.r + ',' + rgb.g + ',' + rgb.b + ')'
        return rbgString
    } else {
        return null
    }
}

export { translate_month, translate_this_label, componentToHex, hexToRgb }
