import React from 'react'
import { withStyles } from '@ui-lib/core/styles'
import { styles, ComponentName } from './FilterListStyles'
import Grid from '@ui-lib/core/Grid'
import Button from '@ui-lib/core/Button'
import TextField from '@ui-lib/core/TextField'
import Typography from '@ui-lib/core/Typography'
import RadioGroup from '@ui-lib/core/RadioGroup'
import Radio from '@ui-lib/core/Radio'
import FormControlLabel from '@ui-lib/core/FormControlLabel'
import { SCOPE_VALUES } from '../../constants'
import AutocompleteSelect from '@ui-lib/oney/AutocompleteSelect'

const FilterListRender = ({
    classes,
    onFilterClick,
    onClearFilterClick,
    I18nProvider,
    filterFieldsValue,
    dataSourceList,
    onChangeFilter,
    invalidFields,
    errorMessages
}) => {
    return (
        <div className={classes.root}>
            <div className={classes.filterFieldsArea}>
                <Grid container spacing={24}>
                    <Grid
                        id="firstRow"
                        container
                        alignItems="flex-start"
                        spacing={16}
                        item
                        xs={12}>
                        <Grid item xs={2}>
                            <div style={{ height: '40px' }}>
                                {' '}
                                {/* Hammer Fall refactor onHover push all items */}
                                <AutocompleteSelect
                                    style={{ height: '40px' }}
                                    formControlProps={{ fullWidth: true }}
                                    inputKey="statusSelect"
                                    label={I18nProvider.Texti18n('status')}
                                    placeholder={I18nProvider.Labeli18n(
                                        'select_component.placeholder.label'
                                    )}
                                    value={
                                        filterFieldsValue.status
                                            ? dataSourceList.status.find(
                                                  (data) =>
                                                      data.value ===
                                                      filterFieldsValue.status
                                              )
                                            : null
                                    } //TODO Refactor
                                    options={dataSourceList.status}
                                    onChange={(value) =>
                                        onChangeFilter(
                                            value && value.value
                                                ? value.value
                                                : undefined,
                                            'status'
                                        )
                                    }
                                    noOptionsMessage={() =>
                                        I18nProvider.Labeli18n(
                                            'select_component.no_matches.label'
                                        )
                                    }
                                />
                            </div>
                        </Grid>
                        <Grid item xs={2}>
                            <div className={classes.filterTextField}>
                                <TextField
                                    fullWidth
                                    InputLabelProps={{ shrink: true }}
                                    label={I18nProvider.Texti18n('fd_number')}
                                    value={filterFieldsValue.number}
                                    onChange={(e) =>
                                        onChangeFilter(e.target.value, 'number')
                                    }
                                />
                            </div>
                        </Grid>
                        <Grid item xs={2}>
                            <div style={{ height: '40px' }}>
                                <AutocompleteSelect
                                    formControlProps={{ fullWidth: true }}
                                    inputKey="typeSelect"
                                    label={I18nProvider.Texti18n('fd_type')}
                                    placeholder={I18nProvider.Labeli18n(
                                        'select_component.placeholder.label'
                                    )}
                                    value={
                                        filterFieldsValue.type
                                            ? dataSourceList.financialDocumentTypes.find(
                                                  (data) =>
                                                      data.value ===
                                                      filterFieldsValue.type
                                              )
                                            : null
                                    } //TODO Refactor
                                    options={
                                        dataSourceList.financialDocumentTypes
                                    }
                                    onChange={(value) =>
                                        onChangeFilter(
                                            value && value.value
                                                ? value.value
                                                : undefined,
                                            'type'
                                        )
                                    }
                                    noOptionsMessage={() =>
                                        I18nProvider.Labeli18n(
                                            'select_component.no_matches.label'
                                        )
                                    }
                                />
                            </div>
                        </Grid>
                        <Grid item xs={2}>
                            <div style={{ height: '40px' }}>
                                <AutocompleteSelect
                                    formControlProps={{ fullWidth: true }}
                                    inputKey="supplierSelect"
                                    label={I18nProvider.Texti18n('supplier')}
                                    placeholder={I18nProvider.Labeli18n(
                                        'select_component.placeholder.label'
                                    )}
                                    value={
                                        filterFieldsValue.entityCode
                                            ? dataSourceList.suppliers.find(
                                                  (data) =>
                                                      data.value ===
                                                      filterFieldsValue.entityCode
                                              )
                                            : null
                                    } //TODO Refactor
                                    options={dataSourceList.suppliers}
                                    onChange={(value) =>
                                        onChangeFilter(
                                            value && value.value
                                                ? value.value
                                                : undefined,
                                            'entityCode'
                                        )
                                    }
                                    noOptionsMessage={() =>
                                        I18nProvider.Labeli18n(
                                            'select_component.no_matches.label'
                                        )
                                    }
                                />
                            </div>
                        </Grid>
                        <Grid item xs={4}>
                            <Grid
                                id="secondRow"
                                container
                                alignItems="flex-start"
                                spacing={0}
                                item
                                xs={12}>
                                <Grid item xs={5}>
                                    <div className={classes.filterTextField}>
                                        <TextField
                                            fullWidth
                                            InputLabelProps={{ shrink: true }}
                                            label={I18nProvider.Texti18n(
                                                'vat_exclusive'
                                            )}
                                            placeholder={I18nProvider.Texti18n(
                                                'capitalized_of'
                                            )}
                                            value={
                                                filterFieldsValue.minVATValue
                                            }
                                            // helperText={ invalidFields.get('minVATValue') ? errorMessages.get('minVATValue') === "filter_value_out_of_bounds"? I18nProvider.Texti18n(errorMessages.get('minVATValue')) +  filterFieldsValue.maxValue : I18nProvider.Texti18n(errorMessages.get('minVATValue')): undefined}
                                            helperText={
                                                invalidFields.get('minVATValue')
                                                    ? I18nProvider.Texti18n(
                                                          errorMessages.get(
                                                              'minVATValue'
                                                          )
                                                      )
                                                    : undefined
                                            }
                                            error={invalidFields.get(
                                                'minVATValue'
                                            )}
                                            onChange={(e) =>
                                                onChangeFilter(
                                                    e.target.value,
                                                    'minVATValue'
                                                )
                                            }
                                        />
                                    </div>
                                </Grid>
                                <Grid item xs={2}>
                                    <div className={classes.filterDashSplitter}>
                                        <Typography
                                            color="textPrimary"
                                            variant="subtitle2">
                                            -
                                        </Typography>
                                    </div>
                                </Grid>
                                <Grid item xs={5}>
                                    <div
                                        className={
                                            classes.filterLimitTextField
                                        }>
                                        <TextField
                                            fullWidth
                                            InputLabelProps={{ shrink: true }}
                                            placeholder={I18nProvider.Texti18n(
                                                'capitalized_until'
                                            )}
                                            value={
                                                filterFieldsValue.maxVATValue
                                            }
                                            helperText={
                                                invalidFields.get('maxVATValue')
                                                    ? I18nProvider.Texti18n(
                                                          errorMessages.get(
                                                              'maxVATValue'
                                                          )
                                                      )
                                                    : undefined
                                            }
                                            error={invalidFields.get(
                                                'maxVATValue'
                                            )}
                                            onChange={(e) =>
                                                onChangeFilter(
                                                    e.target.value,
                                                    'maxVATValue'
                                                )
                                            }
                                        />
                                    </div>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid
                        id="secondRow"
                        container
                        alignItems="flex-start"
                        spacing={16}
                        item
                        xs={12}>
                        <Grid item xs={2}>
                            <div className={classes.filterTextField}>
                                <TextField
                                    fullWidth
                                    InputLabelProps={{ shrink: true }}
                                    label={I18nProvider.Texti18n('po_number')}
                                    value={
                                        filterFieldsValue.purchaseOrderNumber
                                    }
                                    onChange={(e) =>
                                        onChangeFilter(
                                            e.target.value,
                                            'purchaseOrderNumber'
                                        )
                                    }
                                />
                            </div>
                        </Grid>
                        <Grid item xs={2}>
                            <div className={classes.filterTextField}>
                                <TextField
                                    fullWidth
                                    id="date"
                                    label={I18nProvider.Texti18n('created_at')}
                                    type="date"
                                    value={filterFieldsValue.createdDate}
                                    onChange={(e) =>
                                        onChangeFilter(
                                            e.target.value,
                                            'createdDate'
                                        )
                                    }
                                    inputProps={{
                                        max: new Date()
                                            .toISOString()
                                            .slice(0, 10)
                                    }}
                                    InputLabelProps={{
                                        shrink: true
                                    }}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={2}>
                            <div className={classes.filterTextField}>
                                <TextField
                                    fullWidth
                                    id="date"
                                    label={I18nProvider.Texti18n(
                                        'approval_date_limit'
                                    )}
                                    type="date"
                                    value={filterFieldsValue.expirationDate}
                                    onChange={(e) =>
                                        onChangeFilter(
                                            e.target.value,
                                            'expirationDate'
                                        )
                                    }
                                    InputLabelProps={{
                                        shrink: true
                                    }}
                                />
                            </div>
                        </Grid>
                        <Grid item xs={2}>
                            <div style={{ height: '40px' }}>
                                <AutocompleteSelect
                                    formControlProps={{ fullWidth: true }}
                                    inputKey="departmentSelect"
                                    placeholder={I18nProvider.Labeli18n(
                                        'select_component.placeholder.label'
                                    )}
                                    label={I18nProvider.Texti18n('department')}
                                    value={
                                        filterFieldsValue.departmentCode
                                            ? dataSourceList.departments.find(
                                                  (data) =>
                                                      data.value ===
                                                      filterFieldsValue.departmentCode
                                              )
                                            : null
                                    } //TODO Refactor
                                    options={dataSourceList.departments}
                                    onChange={(value) =>
                                        onChangeFilter(
                                            value && value.value
                                                ? value.value
                                                : undefined,
                                            'departmentCode'
                                        )
                                    }
                                    noOptionsMessage={() =>
                                        I18nProvider.Labeli18n(
                                            'select_component.no_matches.label'
                                        )
                                    }
                                />
                            </div>
                        </Grid>
                        <Grid item xs={2}>
                            <div style={{ height: '40px' }}>
                                <AutocompleteSelect
                                    formControlProps={{ fullWidth: true }}
                                    inputKey="projectSelect"
                                    placeholder={I18nProvider.Labeli18n(
                                        'select_component.placeholder.label'
                                    )}
                                    label={I18nProvider.Texti18n('project')}
                                    value={
                                        filterFieldsValue.projectCode
                                            ? dataSourceList.projects.find(
                                                  (data) =>
                                                      data.value ===
                                                      filterFieldsValue.projectCode
                                              )
                                            : null
                                    } //TODO Refactor
                                    options={dataSourceList.projects}
                                    onChange={(value) =>
                                        onChangeFilter(
                                            value && value.value
                                                ? value.value
                                                : undefined,
                                            'projectCode'
                                        )
                                    }
                                    noOptionsMessage={() =>
                                        I18nProvider.Labeli18n(
                                            'select_component.no_matches.label'
                                        )
                                    }
                                />
                            </div>
                        </Grid>
                        <Grid item xs={2}>
                            <div style={{ height: '40px' }}>
                                {' '}
                                {/* Hammer Fall refactor onHover push all items */}
                                <AutocompleteSelect
                                    formControlProps={{ fullWidth: true }}
                                    inputKey="internalOrderSelect"
                                    placeholder={I18nProvider.Labeli18n(
                                        'select_component.placeholder.label'
                                    )}
                                    label={I18nProvider.Texti18n(
                                        'internal_order'
                                    )}
                                    value={
                                        filterFieldsValue.internalOrderCode
                                            ? dataSourceList.internalOrders.find(
                                                  (data) =>
                                                      data.value ===
                                                      filterFieldsValue.internalOrderCode
                                              )
                                            : null
                                    } //TODO Refactor
                                    options={dataSourceList.internalOrders}
                                    onChange={(value) =>
                                        onChangeFilter(
                                            value && value.value
                                                ? value.value
                                                : undefined,
                                            'internalOrderCode'
                                        )
                                    }
                                    noOptionsMessage={() =>
                                        I18nProvider.Labeli18n(
                                            'select_component.no_matches.label'
                                        )
                                    }
                                />
                            </div>
                        </Grid>
                    </Grid>
                    <Grid
                        id="thirdRow"
                        container
                        alignItems="flex-start"
                        spacing={16}
                        item
                        xs={12}>
                        <Grid item xs={3}>
                            <RadioGroup row={true}>
                                <FormControlLabel
                                    control={
                                        <Radio
                                            color="primary"
                                            checked={
                                                filterFieldsValue.scope ===
                                                SCOPE_VALUES.MINE
                                            }
                                            onChange={(e) =>
                                                onChangeFilter(
                                                    e.target.value,
                                                    'scope'
                                                )
                                            }
                                            value={SCOPE_VALUES.MINE}
                                            name="radio-button-mine"
                                            aria-label="Mine"
                                        />
                                    }
                                    label={I18nProvider.Texti18n('mine')}
                                />
                                <FormControlLabel
                                    control={
                                        <Radio
                                            color="primary"
                                            checked={
                                                filterFieldsValue.scope ===
                                                SCOPE_VALUES.TEAM
                                            }
                                            onChange={(e) =>
                                                onChangeFilter(
                                                    e.target.value,
                                                    'scope'
                                                )
                                            }
                                            value={SCOPE_VALUES.TEAM}
                                            name="radio-button-my-team"
                                            aria-label="My Team"
                                        />
                                    }
                                    label={I18nProvider.Texti18n('my_team')}
                                />
                                <FormControlLabel
                                    control={
                                        <Radio
                                            color="primary"
                                            checked={
                                                filterFieldsValue.scope ===
                                                SCOPE_VALUES.ALL
                                            }
                                            onChange={(e) =>
                                                onChangeFilter(
                                                    e.target.value,
                                                    'scope'
                                                )
                                            }
                                            value={SCOPE_VALUES.ALL}
                                            name="radio-button-all"
                                            aria-label="All"
                                        />
                                    }
                                    label={I18nProvider.Texti18n('all')}
                                />
                            </RadioGroup>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
            <div className={classes.filterButtonsArea}>
                <Button
                    id="ClearButton"
                    className={classes.cancelButton}
                    variant="text"
                    onClick={onClearFilterClick}>
                    {I18nProvider.Texti18n('claims.filter.clear.button')}
                </Button>
                <Button
                    id="FilterButton"
                    variant="contained"
                    color="primary"
                    onClick={onFilterClick}>
                    {I18nProvider.Texti18n(
                        'claims.filter.apply_filters.button'
                    )}
                </Button>
            </div>
        </div>
    )
}

export default withStyles(styles, { name: ComponentName })(FilterListRender)
