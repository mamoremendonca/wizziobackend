import { Doughnut } from 'react-chartjs-2'
import React from 'react'
import { extendDefaultDoughnut } from './utils'

const Donut = ({
    data,
    options,
    doughnutWidth = 300,
    doughnutHeight = 300
}) => {
    extendDefaultDoughnut()
    return (
        <Doughnut
            data={data}
            width={doughnutWidth}
            height={doughnutHeight}
            options={options}
        />
    )
}

export default Donut
