import React from 'react'
import FieldSetComponent from '../FieldSetComponent'
import { withStyles } from '@ui-lib/core/styles'
import { styles, ComponentName } from './AttachmentListComponentStyles'
import AttachmentList from './AttachmentList'

const timelineAttachments = [
    {
        attachments: [
            {
                id: 1,
                name: 'Fatura.template.pdf',
                date: '2020-06-25',
                isAbleToDelete: true,
                comment:
                    'Factura template da reclamaração associada. Factura template da reclamaração associada. Factura template da reclamaração associada. Factura template da reclamaração associada. Factura template da reclamaração associada.'
            }
        ]
    },
    {
        attachments: [
            {
                id: 1,
                name: 'Fatura.template.pdf',
                date: '2020-06-25',
                isAbleToDelete: false
            }
        ]
    }
]

const AttachmentsListComponent = ({ classes }) => {
    return (
        <div className={classes.root}>
            <FieldSetComponent
                //title={}
                content={
                    <AttachmentList
                        dataSource={'/instance/'}
                        setContainerLoadingDependencies={undefined}
                        timelineAttachments={timelineAttachments}
                    />
                }
            />
        </div>
    )
}

export default withStyles(styles, { name: ComponentName })(
    AttachmentsListComponent
)
