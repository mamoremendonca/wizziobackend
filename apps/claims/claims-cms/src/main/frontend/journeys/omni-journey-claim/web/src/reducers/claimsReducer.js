import * as constants from '../constants'

//App Initial State
const initialState = {
    claimsData: undefined,
    ///Claims List area
    claimsList: [],
    totalClaims: 0,
    numberOfRecords: 0,
    // Filter Area
    searchCriteria: {
        filterCriteria: { ...constants.INITIAL_FILTER_CRITERIA },
        sortCriteria: {
            orderField: constants.DEFAULT_SORTED_FIELD['orderField'],
            orderType: constants.DEFAULT_SORTED_FIELD['orderType']
        }
    },
    timelineData: undefined
}

/**
 * Receives The State of The App and With the Action Changes It
 * @param {*} state
 * @param {*} action
 */
const claimsReducer = (state, action) => {
    if (state === undefined) return initialState

    switch (action.type) {
        case constants.SET_CLAIMS_LIST_PROCESSES:
            return {
                ...state,
                claimsList: action.payload.claimsList,
                totalClaims: action.payload.totals,
                numberOfRecords: action.payload.numberOfRecords
            }
        //   case constants.UPDATE_FILTER_CRITERIA: {
        //     return {
        //       ...state,
        //       searchCriteria: {
        //         ...state.searchCriteria,
        //         filterCriteria: action.payload,
        //       },
        //     };
        //   }
        //   case constants.UPDATE_SORTING_CRITERIA: {
        //     return {
        //       ...state,
        //       searchCriteria: {
        //         ...state.searchCriteria,
        //         sortCriteria: action.payload,
        //       },
        //     };
        //   }
        //   case constants.RESET_FILTER_CRITERIA: {
        //     return {
        //       ...state,
        //       searchCriteria: {
        //         ...state.searchCriteria,
        //         filterCriteria: { ...constants.INITIAL_FILTER_CRITERIA },
        //       },
        //     };
        //   }
        default:
            return state
    }
}

export { claimsReducer }
