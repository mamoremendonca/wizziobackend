import React, { Fragment } from 'react'
import Grid from '@ui-lib/core/Grid'
import HeaderBar from '../../components/HeaderBar'
import DetailsSection from './DetailsSection'
import ActivityTimeline from '../../components/ActivityTimeline'
import DescriptionNotesComponent from '../../components/DescriptionNotesComponent'
import AttachmentListComponent from '../../components/AttachmentListComponent'
import RelatedClaimsComponent from '../../components/RelatedClaimsComponent'
import { withStyles } from '@ui-lib/core/styles'
import { styles, ComponentName } from './ClaimAnalysisStyles'
import { descriptionNotesData, relatedClaimsData } from './mockData'
//import HeaderActionButtons from './HeaderActionButtons'
import { connect } from 'react-redux'
//import { bindActionCreators } from 'redux'

const ClaimAnalysis = ({ classes, I18nProvider }) => {
    return (
        <Fragment>
            <Grid className={classes.root}>
                <HeaderBar title={'Análise de Reclamação'}>
                    {/* <HeaderActionButtons /> */}
                </HeaderBar>
                <DetailsSection />
                <DescriptionNotesComponent
                    descriptionNotesData={descriptionNotesData}
                />
                <Grid
                    container
                    wrap="nowrap"
                    className="claim-analysis-client-activities-attachments">
                    <ActivityTimeline />
                    <AttachmentListComponent />
                </Grid>
                <RelatedClaimsComponent
                    relatedClaimsData={relatedClaimsData}
                    i18nProvider={I18nProvider}
                />
            </Grid>
        </Fragment>
    )
}

//Redux
const mapStateToProps = ({
    journey: {
        services: { I18nProvider, HttpClient, Loading, EntitlementsVerifier }
    },
    i18n
}) => ({
    I18nProvider,
    i18n,
    HttpClient,
    Loading,
    EntitlementsVerifier
})

export default connect(mapStateToProps)(
    withStyles(styles, { name: ComponentName })(ClaimAnalysis)
)
