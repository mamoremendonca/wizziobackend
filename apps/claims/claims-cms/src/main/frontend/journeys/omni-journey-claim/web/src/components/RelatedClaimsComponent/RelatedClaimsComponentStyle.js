const ComponentName = 'RelatedClaimsComponentStyle'

const styles = (theme) => ({
    claimStatusIconArea: {
        marginLeft: '8px'
    },
    claimStatusCircleArea: {
        marginLeft: '10px'
    }
})

export { ComponentName, styles }
