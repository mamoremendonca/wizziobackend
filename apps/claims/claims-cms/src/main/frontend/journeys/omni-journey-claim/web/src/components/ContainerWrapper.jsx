import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { updateJourneySubtitle } from '../routes/Routes'

export default (ComposedComponent) => {
    const ReduxContainer = ({
        location,
        JourneyActions,
        JourneySettings,
        claimsData,
        I18nProvider
    }) => {
        useEffect(() => {
            updateJourneySubtitle(
                location.pathname,
                JourneyActions,
                JourneySettings,
                claimsData,
                I18nProvider
            )
        }, [])

        return <ComposedComponent {...props} />
    }

    //Redux
    const mapStateToProps = ({
        journey: {
            services: { I18nProvider, JourneyActions },
            settings,
            i18n
        },
        claimsReducer: { claimsData }
    }) => ({
        i18n, //To Update TextI18n When The Current Language Changes.
        I18nProvider,
        JourneyActions,
        JourneySettings: settings,
        claimsData
    })

    return connect(mapStateToProps)(withRouter(ReduxContainer))
}
