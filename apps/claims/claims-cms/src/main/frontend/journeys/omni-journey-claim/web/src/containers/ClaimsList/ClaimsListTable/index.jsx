import React, { useState } from 'react'
import CustomTable from '@ui-lib/custom-components/CustomTable'
import Grid from '@ui-lib/core/Grid'
import Countdown from './Countdown'
import { CLAIMS_STATUS } from '../../../constants'
import { getFormattedNumber } from '../../../utils'
import { ClaimsListHeaders } from './TableHeaders'
import ResultTooltip from '../../../components/ResultTooltip'
import Typography from '@ui-lib/core/Typography'
import { withStyles } from '@ui-lib/core/styles'
import { styles, ComponentName } from '../ClaimsListStyles'
import ClaimStatusCircle from '../../../components/ClaimStatusCircle'

/**
 * Returns a JSX Component for The Status Column to Add in The Row of FD List  * Table
 * @param {*} classes
 * @param {*} status
 */
const getStatusVisualComponent = (classes, status) => {
    switch (status) {
        case CLAIMS_STATUS.IN_APPROVAL:
            return (
                <div className={classes.statsAlign}>
                    <div className={classes.waitingApprovalStatus} />
                </div>
            )
        case CLAIMS_STATUS.SAP_SUBMITTED:
            return (
                <div className={classes.statsAlign}>
                    <div className={classes.sap_submittedStatus} />
                </div>
            )
        case CLAIMS_STATUS.APPROVED:
            return (
                <div className={classes.statsAlign}>
                    <div className={classes.approvedStatus} />
                </div>
            )
        case CLAIMS_STATUS.CANCELED:
            return (
                <div className={classes.statsAlign}>
                    <div className={classes.canceledStatus} />
                </div>
            )
        case CLAIMS_STATUS.REJECTED:
            return (
                <div className={classes.statsAlign}>
                    <div className={classes.rejectedStatus} />
                </div>
            )
        case CLAIMS_STATUS.PAID:
            return (
                <div className={classes.statsAlign}>
                    <div className={classes.paidStatus} />
                </div>
            )
        default:
            return '-'
    }
}

const toLocalDate = (date) => `${new Date(date).toLocaleDateString()}`

/**
 * Will Return a i18n String for The Description or For The Code.
 *
 * @param {*} i18nProvider
 * @param {*} code
 * @param {*} ref
 */
const getDescriptionByCode = (i18nProvider, code, ref) => {
    const elements = ref.filter((entry) => entry.code === code)
    if (elements.length > 0) {
        return i18nProvider.Texti18n(elements[0].description)
    } else {
        return i18nProvider.Texti18n(code)
    }
}

/**
 * Gets The Value That Came From The Response and Format It.
 * @param {*} fieldValue
 * @param {*} format
 * @param {*} ref
 */
const parseCellValues = (i18nProvider, fieldValue, format, ref) => {
    switch (format) {
        case 'date':
            return fieldValue ? toLocalDate(fieldValue) : '-'
        case 'countdown':
            return fieldValue ? (
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'row',
                        position: 'relative'
                    }}>
                    {toLocalDate(fieldValue)}
                    <Countdown
                        i18nProvider={i18nProvider}
                        dateToCompare={fieldValue}
                        status={ref}
                    />
                </div>
            ) : (
                '-'
            )
        case 'referenceData':
            return fieldValue
                ? getDescriptionByCode(i18nProvider, fieldValue, ref)
                : '-'
        case 'euro':
            return fieldValue
                ? getFormattedNumber(fieldValue, true, 'EUR')
                : '-'
        default:
            return fieldValue ? i18nProvider.Texti18n(fieldValue) : '-'
    }
}

/**
 * Create a Parsed Row
 * Has The Logic to Transform Data in Visual Content.
 * @param {*} classes
 * @param {*} dataRow
 * @param {*} i18nProvider
 * @param {*} refData
 */
const createTableRow = (classes, dataRow, i18nProvider, refData) => {
    const row = {}
    row.id = dataRow.repositoryId
    row.repositoryStatus = (
        <div className={classes.claimStatusCircleArea}>
            <ClaimStatusCircle status={dataRow.repositoryStatus} />
        </div>
    )
    row.number = parseCellValues(i18nProvider, dataRow.number)
    row.origin = parseCellValues(i18nProvider, dataRow.origin)
    row.channel = parseCellValues(i18nProvider, dataRow.channel, 'channel')
    row.motive = parseCellValues(i18nProvider, dataRow.motive, 'motive')
    row.subMotive = parseCellValues(i18nProvider, dataRow.motive, 'subMotive')
    row.client = parseCellValues(i18nProvider, dataRow.client, 'client')
    row.ammount = parseCellValues(i18nProvider, dataRow.ammount, 'euro')
    row.creationDate = parseCellValues(
        i18nProvider,
        dataRow.creationDate,
        'date'
    )
    row.expirationDate = parseCellValues(
        i18nProvider,
        dataRow.expirationDate,
        'countdown'
    )

    row.userGroup = dataRow.currentUserGroup || ' - '
    return row
}

/**
 * Used to Parse The Data That Comes From The Backend
 * @param {*} classes
 * @param {*} data
 * @param {*} i18nProvider
 * @param {*} refData
 */
const tableParseData = (classes, data, i18nProvider, refData) => {
    let parseData = []
    data.forEach((element) => {
        parseData.push(createTableRow(classes, element, i18nProvider, refData))
    })
    return parseData
}

// const renderData = (data, classes) => {
//   if (relatedClaimsData?.length > 0) {
//     return relatedClaimsData.map((relatedClaim) => ({
//       ...relatedClaim,
//       status: (
//         <div className={classes.claimStatusCircleArea}>
//           <ClaimStatusCircle status={relatedClaim.status} />
//         </div>
//       ),
//     }));
//   }
//   return undefined;
// };

const ClaimsListTable = ({
    classes,
    I18nProvider,
    totalClaims,
    numberOfRecords,
    claimsList,
    onRowClick,
    sortHandler,
    refData,
    tableApi,
    setTableApi
}) => {
    console.log(totalClaims, numberOfRecords, claimsList)
    return (
        <div className={classes.mainContent}>
            <Grid container spacing={0}>
                <Grid container item xs={12} spacing={0} justify="flex-end">
                    <div className={classes.tableNumbers}>
                        <ResultTooltip I18nProvider={I18nProvider} />
                        <Typography
                            variant="body2 Heading"
                            color="textPrimary"
                            style={{ marginLeft: '10px' }}>
                            {I18nProvider.Texti18n('showing')}{' '}
                            {numberOfRecords ? numberOfRecords : 0}{' '}
                            {I18nProvider.Texti18n('of')}{' '}
                            {totalClaims ? totalClaims : 0}{' '}
                            {I18nProvider.Texti18n('results')}.
                        </Typography>
                    </div>
                </Grid>
                <Grid container item xs={12} spacing={0}>
                    <CustomTable
                        onRowClick={onRowClick}
                        id="TableClaimsList"
                        singleSort={true}
                        headers={ClaimsListHeaders(I18nProvider)}
                        apiCallback={(api) => {
                            if (!tableApi) setTableApi(api)
                        }}
                        noDataLabel={I18nProvider.Texti18n('no_results_found')}
                        data={tableParseData(
                            classes,
                            claimsList,
                            I18nProvider,
                            refData
                        )}
                        onSort={(headerKey, directionResponseOk) => {
                            return new Promise((resolve, error) => {
                                sortHandler(
                                    headerKey,
                                    directionResponseOk,
                                    resolve,
                                    error
                                )
                            })
                        }}
                    />
                </Grid>
            </Grid>
        </div>
    )
}

export default withStyles(styles, { name: ComponentName })(ClaimsListTable)
