import React from 'react'
import Journey from './journey'
import { Provider } from 'react-redux'
import { name as packageName, parent, contentVersion } from '../package.json'
import { createStoreSingleton } from 'omni-journey'
import { FinalJourney } from 'omni-journey'

import { claimsReducer } from './reducers/claimsReducer'

let reducersToMerge = { claimsReducer }
let Store = createStoreSingleton(reducersToMerge)

const App = () => {
    return (
        <Provider store={Store}>
            <div>
                <RenderJourney />
            </div>
        </Provider>
    )
}

//Journey Configuration
let RenderJourney = FinalJourney(
    Journey,
    Store,
    packageName,
    parent,
    contentVersion
)

export default App
