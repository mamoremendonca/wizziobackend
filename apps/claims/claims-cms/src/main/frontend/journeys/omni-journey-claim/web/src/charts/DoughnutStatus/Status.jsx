import React from 'react'
import StatusEntry from './StatusEntry'
import { Grid } from '@material-ui/core'

const Status = ({ data }) => {
    return (
        <Grid container spacing={3}>
            {data.map((dataEntry, index) => {
                return <StatusEntry key={index} data={dataEntry} />
            })}
        </Grid>
    )
}

export default Status
