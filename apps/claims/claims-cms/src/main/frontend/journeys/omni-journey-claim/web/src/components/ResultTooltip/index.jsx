import React, { useState } from 'react'
import Tooltip from '@ui-lib/core/Tooltip'
import Icon from '@ui-lib/core/Icon'
import Typography from '@ui-lib/core/Typography'
import { withStyles } from '@ui-lib/core/styles'
import { styles, ComponentName } from './ResultTooltipStyles'

const ResultsTooltip = ({ text, I18nProvider }) => {
    return (
        <div>
            {/* TODO: Uncomment when the new ui-lib/core 2.2.0 is used*/}
            {/* <Tooltip
                classes={{ tooltip: classes.increaseTooltip }}
                title={I18nProvider.Texti18n(
                    'listFD.table.header.tooltip.record_numbers'
                )}>
                <Icon className="icon-information" style={{ fontSize: 16 }} />
            </Tooltip> */}
            <Tooltip
                title={
                    <Typography>
                        {text
                            ? text
                            : I18nProvider.Texti18n(
                                  'claims.table.header.tooltip.record_numbers'
                              )}
                    </Typography>
                }>
                <Icon className="icon-information" style={{ fontSize: 16 }} />
            </Tooltip>
        </div>
    )
}

export default withStyles(styles, { name: ComponentName })(ResultsTooltip)
