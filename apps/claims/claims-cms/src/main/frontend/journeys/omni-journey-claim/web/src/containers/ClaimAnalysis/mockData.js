const descriptionNotesData = [
    {
        id: 1,
        notes: 'Numero de telemovel do cliente não está atribuido',
        date: '13/02/2020',
        user: 'Vera Fernandes'
    },
    {
        id: 2,
        notes: 'O cliente não está contactavel',
        date: '14/02/2020',
        user: 'Vera Fernandes'
    }
]

const relatedClaimsData = [
    {
        id: 1,
        status: 1,
        chanel: 'Presencial',
        origin: 'Centro de contactos',
        reason: 'Cobrança',
        subReason: 'Cobrança indevida',
        date: '14/02/2020'
    },
    {
        id: 2,
        status: 2,
        chanel: 'Presencial',
        origin: 'Centro de contactos',
        reason: 'Cobrança',
        subReason: 'Cobrança indevida',
        date: '15/02/2020'
    }
]

export { descriptionNotesData, relatedClaimsData }
