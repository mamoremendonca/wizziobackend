import React, { Fragment, useEffect } from 'react'

const OneyItemsTableInput = ({ inputData, setInputData, disabled }) => {
    // addItemHandler
    // removeItemHandler
    // copyItemHandler
    // editItemHandler
    // ...

    return (
        <Fragment>
            <button
                onClick={(evt) => {
                    evt.preventDefault()
                    setInputData('Goodbye')
                }}>
                ADD DATA TO INPUT
            </button>
            <button
                onClick={(evt) => {
                    evt.preventDefault()
                    setInputData(undefined)
                }}>
                RESET DATA
            </button>
        </Fragment>
    )
}

export default OneyItemsTableInput
