import React from 'react'
import DonutStatus from '../../charts/DoughnutStatus/DonutStatus'
import {
    parseDonutData,
    donutOptions
} from '../../charts/DoughnutStatus/parser'
import mockData from '../../charts/DoughnutStatus/mockData'

const DoughnutStatusDemo = () => {
    return (
        <DonutStatus
            rawData={mockData}
            data={parseDonutData(mockData)}
            options={donutOptions}
        />
    )
}

export default DoughnutStatusDemo
