const ComponentName = 'HeaderActionButtonsStyle'

const styles = (theme) => ({
    filterIconButton: {
        border: '1px solid #000',
        height: '50px',
        width: '100px',
        marginRight: '10px'
    },
    filterIconButtonContent: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }
})

export { ComponentName, styles }

export default styles
