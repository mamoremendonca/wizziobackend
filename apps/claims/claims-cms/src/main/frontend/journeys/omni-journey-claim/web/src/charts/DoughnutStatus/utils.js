import { Chart } from 'react-chartjs-2'

const extendDefaultDoughnut = () => {
    const originalDoughnutDraw = Chart.controllers.doughnut.prototype.draw
    Chart.helpers.extend(Chart.controllers.doughnut.prototype, {
        draw: () => {
            originalDoughnutDraw.apply(this, arguments)

            const chart = chart.chart
            const ctx = chart.ctx
            const width = chart.width
            const height = chart.height

            const fontSize = 30
            ctx.font = fontSize + 'px Verdana'
            ctx.textBaseline = 'middle'

            const text = chart.config.data.text,
                textX = Math.round((width - ctx.measureText(text).width) / 2),
                textY = height / 2

            ctx.fillText(text, textX, textY)
        }
    })
}

export { extendDefaultDoughnut }
