import React, { useRef, Fragment } from 'react'
import { withStyles, makeStyles } from '@material-ui/core/styles'
import { Icon, Button } from '@material-ui/core'
import styles from './styles'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline'

const useStyles = makeStyles({
    root: {
        textTransform: 'none'
    },
    text: {
        color: '#81BC00'
    },
    outlined: {
        borderColor: '#81BC00',
        borderRadius: 0,
        borderWidth: '2px'
    }
})

const FileInput = ({ handleAddFiles }) => {
    const classes = useStyles()

    const inputRef = useRef(null)

    return (
        <Fragment>
            <input
                style={{ display: 'none' }}
                ref={inputRef}
                type="file"
                // accept=".docx"
                multiple
                onChange={(evt) => {
                    handleAddFiles(evt.target.files)
                }}
            />
            <Button
                variant="outlined"
                classes={{ ...classes }}
                startIcon={
                    <AddCircleOutlineIcon
                        style={{ color: '#81BC00', fontSize: 18 }}
                    />
                }
                onClick={(e) => {
                    inputRef.current.click()
                }}>
                <span className={classes.text}>Adicionar Anexo</span>
            </Button>
        </Fragment>
    )
}

export default FileInput
