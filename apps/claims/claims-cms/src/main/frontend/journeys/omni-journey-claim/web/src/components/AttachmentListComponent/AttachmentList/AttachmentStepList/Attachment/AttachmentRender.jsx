import React from 'react'
import { withStyles } from '@ui-lib/core/styles'
import { styles, ComponentName } from './AttachmentStyles'
import Typography from '@ui-lib/core/Typography'
import Icon from '@ui-lib/core/Icon'
import IconButton from '@ui-lib/core/IconButton'
import CircularProgress from '@ui-lib/core/CircularProgress'
import Grid from '@ui-lib/core/Grid'
import Tooltip from '@ui-lib/core/Tooltip'

const AttachmentRender = ({
    classes,
    attachmentData,
    downloadAttachment,
    deleteAttachment
}) => {
    return (
        <Grid container wrap={'nowrap'}>
            <div className={classes.root}>
                <div className={classes.attachmentContainer}>
                    <div className={classes.attachmentHeader}>
                        <Icon className={classes.attachmentIcon}>
                            attach_file
                        </Icon>
                        <div onClick={downloadAttachment}>
                            <Typography
                                variant="h5"
                                className={classes.attachmentTitle}>
                                {attachmentData.name}
                            </Typography>
                        </div>
                    </div>
                </div>
                {(attachmentData.isAbleToDelete ||
                    attachmentData.isLoading) && (
                    <div className={classes.deleteAttachment}>
                        {attachmentData.isLoading ? (
                            <CircularProgress
                                value={true}
                                color="primary"
                                className={classes.circularProgress}
                            />
                        ) : (
                            <IconButton
                                onClick={deleteAttachment}
                                className={classes.closeIcon}>
                                <Icon>clear</Icon>
                            </IconButton>
                        )}
                    </div>
                )}
            </div>
            {attachmentData.comment && (
                <div className={classes.attachmentComment}>
                    <Tooltip title={attachmentData.comment}>
                        <Typography
                            variant="caption"
                            color="textSecondary"
                            className={classes.commentContent}>
                            <div>
                                <b>Comentário</b>
                            </div>
                            {attachmentData.comment}
                        </Typography>
                    </Tooltip>
                </div>
            )}
        </Grid>
    )
}

export default withStyles(styles, { name: ComponentName })(AttachmentRender)
