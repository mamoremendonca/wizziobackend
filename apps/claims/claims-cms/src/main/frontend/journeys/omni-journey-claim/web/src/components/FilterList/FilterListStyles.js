const ComponentName = 'POFilterList'
const styles = (theme) => ({
    root: {
        marginTop: '20px'
    },
    filterFieldsArea: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: '46px 130px 0px'
    },

    filterButtonsArea: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginTop: '20px',
        marginRight: '20px'
    },
    formControl: {
        width: '100%'
    },
    filterTextField: {
        marginTop: '5px'
    },
    filterLimitTextField: {
        marginTop: '22px'
    },
    filterCalendarField: {
        marginTop: '30px'
    },
    filterDashSplitter: {
        marginTop: '36px',
        textAlign: 'center'
    },
    filterRadioGroupField: {
        marginTop: '28px'
    },
    cancelButton: {
        marginRight: '30px'
    }
})

export { ComponentName, styles }
