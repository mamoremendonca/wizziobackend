import 'core-js/stable'
import 'react-app-polyfill/ie11'
import 'regenerator-runtime/runtime'
import React from 'react'
import ReactDOM from 'react-dom'
import App from './app'

//App Entry Point
ReactDOM.render(<App />, document.getElementById('root'))
