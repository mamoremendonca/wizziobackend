import React from 'react'
import Donut from './Donut'
import Status from './Status'
import { Flex } from './Grid'

const DonutStatus = ({ data, options, rawData }) => {
    return (
        <Flex flexDirection="column">
            <Flex>
                <Donut data={data} options={options} />
            </Flex>
            <Flex style={{ marginTop: '10px' }}>
                <Status data={rawData} />
            </Flex>
        </Flex>
    )
}

export default DonutStatus
