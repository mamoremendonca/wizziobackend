import React from 'react'
import ClaimStatusTooltip from '../../../components/ClaimStatusTooltip'

const ClaimsListHeaders = (i18nProvider) => {
    return [
        {
            label: (
                <div style={{ marginLeft: '22px', marginTop: '8px' }}>
                    <ClaimStatusTooltip i18nProvider={i18nProvider} />
                </div>
            ),
            key: 'repositoryStatus',
            sortable: true
        },
        {
            label: i18nProvider.Labeli18n('claims.table.header.claim_number'),
            key: 'number',
            sortable: true
        },
        {
            label: i18nProvider.Labeli18n('claims.table.header.origin'),
            key: 'origin',
            sortable: true
        },
        {
            label: i18nProvider.Labeli18n('claims.table.header.channel'),
            key: 'channel',
            sortable: false
        },
        {
            label: i18nProvider.Labeli18n('claims.table.header.motive'),
            key: 'motive',
            sortable: true
        },
        {
            label: i18nProvider.Labeli18n('claims.table.header.sub_motive'),
            key: 'subMotive',
            sortable: true
        },
        {
            label: i18nProvider.Labeli18n('claims.table.header.client'),
            key: 'client',
            sortable: true
        },
        {
            label: i18nProvider.Labeli18n('claims.table.header.ammount'),
            key: 'ammount',
            sortable: true
        },
        {
            label: i18nProvider.Labeli18n('claims.table.header.created_at'),
            key: 'creationDate',
            sortable: true
        },
        {
            label: i18nProvider.Labeli18n(
                'claims.table.header.approval_date_limit'
            ),
            key: 'expirationDate',
            sortable: true
        },
        {
            label: i18nProvider.Labeli18n(
                'claims.table.header.owner_responsible_position'
            ),
            key: 'userGroup',
            sortable: true
        }
    ]
}

export { ClaimsListHeaders }
