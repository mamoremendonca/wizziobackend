const ComponentName = 'ClaimStatusCircle'

const styles = (theme) => ({
    statusCircle: {
        width: '10px',
        height: '10px',
        borderRadius: '5px'
    }
})

export { ComponentName, styles }
