import React from 'react'
import { withStyles } from '@ui-lib/core/styles'
import { styles, ComponentName } from './ClaimStatusTooltipStyles'
import Icon from '@ui-lib/core/Icon'
import Grid from '@ui-lib/core/Grid'
import Tooltip from '@ui-lib/core/Tooltip'
import Typography from '@ui-lib/core/Typography'
import ClaimStatusCircle from '../ClaimStatusCircle'
import { ClaimState } from '../../utils/claim'

const ClaimStatusTooltip = ({ classes, i18nProvider }) => {
    return (
        <Tooltip
            title={
                <Grid>
                    <div className={classes.header}>
                        <Typography variant="body2">
                            {i18nProvider.Labeli18n('claims.table.state.title')}
                        </Typography>
                    </div>
                    <Grid container item alignItems={'center'}>
                        <ClaimStatusCircle status={ClaimState.NEW} />
                        <Typography
                            variant="caption"
                            className={classes.statusText}>
                            {i18nProvider.Labeli18n('claims.table.state.new')}
                        </Typography>
                    </Grid>
                    <Grid container item alignItems={'center'}>
                        <ClaimStatusCircle status={ClaimState.ASSIGNED} />
                        <Typography
                            variant="caption"
                            className={classes.statusText}>
                            {i18nProvider.Labeli18n(
                                'claims.table.state.assigned'
                            )}
                        </Typography>
                    </Grid>
                    <Grid container item alignItems={'center'}>
                        <ClaimStatusCircle status={ClaimState.IN_PROGRESS} />
                        <Typography
                            variant="caption"
                            className={classes.statusText}>
                            {i18nProvider.Labeli18n(
                                'claims.table.state.in_progress'
                            )}
                        </Typography>
                    </Grid>
                    <Grid container item alignItems={'center'}>
                        <ClaimStatusCircle status={ClaimState.FORWARD} />
                        <Typography
                            variant="caption"
                            className={classes.statusText}>
                            {i18nProvider.Labeli18n(
                                'claims.table.state.forward'
                            )}
                        </Typography>
                    </Grid>
                    <Grid container item alignItems={'center'}>
                        <ClaimStatusCircle
                            status={ClaimState.WAITING_PARTNER}
                        />
                        <Typography
                            variant="caption"
                            className={classes.statusText}>
                            {i18nProvider.Labeli18n(
                                'claims.table.state.waiting_partner'
                            )}
                        </Typography>
                    </Grid>
                    <Grid container item alignItems={'center'}>
                        <ClaimStatusCircle status={ClaimState.SOLVED} />
                        <Typography
                            variant="caption"
                            className={classes.statusText}>
                            {i18nProvider.Labeli18n(
                                'claims.table.state.solved'
                            )}
                        </Typography>
                    </Grid>
                    <Grid container item alignItems={'center'}>
                        <ClaimStatusCircle status={ClaimState.WITHOUT_TRACE} />
                        <Typography
                            variant="caption"
                            className={classes.statusText}>
                            {i18nProvider.Labeli18n(
                                'claims.table.state.without_trace'
                            )}
                        </Typography>
                    </Grid>
                </Grid>
            }
            placement="bottom-start">
            <Grid container>
                <Icon color="primary" className={classes.statusIcon}>
                    info_outlined
                </Icon>
            </Grid>
        </Tooltip>
    )
}

export default withStyles(styles, { name: ComponentName })(ClaimStatusTooltip)
