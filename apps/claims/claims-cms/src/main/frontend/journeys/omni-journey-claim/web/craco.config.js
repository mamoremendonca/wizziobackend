//CRACO (Create React App Configuration Override) Configuration
const {
    addAfterLoader,
    removeLoaders,
    loaderByName,
    getLoaders,
    throwUnexpectedConfigError,
    ESLINT_MODES
} = require('@craco/craco')

const throwError = (message) =>
    throwUnexpectedConfigError({
        packageName: 'craco',
        githubRepo: 'gsoft-inc/craco',
        message,
        githubIssueQuery: 'webpack'
    })

module.exports = {
    eslint: {
        mode: ESLINT_MODES.file
    },
    webpack: {
        configure: (webpackConfig, { paths }) => {
            webpackConfig.externals = [
                (context, request, callback) => {
                    if (/@ui-lib\/core\/.*/.test(request)) {
                        if (request.replace('@ui-lib/core', '') === '') {
                            return callback(null, 'var ' + 'UiLibCore')
                        } else {
                            if (/classnames/.test(request)) {
                                return callback(
                                    null,
                                    'var ' + 'UiLibCore.classNames'
                                )
                            }
                            return callback(
                                null,
                                'var ' +
                                    'UiLibCore.' +
                                    request
                                        .replace('@ui-lib/core/', '')
                                        .replace('/', '.')
                            )
                        }
                    }
                    if (/@ui-lib\/custom-components\/.*/.test(request)) {
                        if (
                            request.replace('@ui-lib/custom-components', '') ===
                            ''
                        ) {
                            return callback(
                                null,
                                'var ' + 'UiLibCustomComponents'
                            )
                        } else {
                            return callback(
                                null,
                                'var ' +
                                    'UiLibCustomComponents.' +
                                    request
                                        .replace(
                                            '@ui-lib/custom-components/',
                                            ''
                                        )
                                        .replace('/', '.')
                            )
                        }
                    }
                    if (/@ui-lib\/oney\/.*/.test(request)) {
                        if (request.replace('@ui-lib/oney', '') === '') {
                            return callback(null, 'var ' + 'Oney')
                        } else {
                            return callback(
                                null,
                                'var ' +
                                    'Oney.' +
                                    request
                                        .replace('@ui-lib/oney/', '')
                                        .replace('/', '.')
                            )
                        }
                    }
                    callback()
                },
                { 'omni-journey': 'OmniJourney' },
                { 'react-redux': 'ReactRedux' },
                { redux: 'Redux' },
                { react: 'React' },
                { 'react-dom': 'ReactDOM' }
            ]
            const { hasFoundAny, matches } = getLoaders(
                webpackConfig,
                loaderByName('babel-loader')
            )
            if (!hasFoundAny) throwError('failed to find babel-loader')

            console.log('removing babel-loader')
            const { hasRemovedAny, removedCount } = removeLoaders(
                webpackConfig,
                loaderByName('babel-loader')
            )
            if (!hasRemovedAny) throwError('no babel-loader to remove')
            if (removedCount !== 2)
                throwError('had expected to remove 2 babel loader instances')

            console.log('adding ts-loader')

            const tsLoader = {
                test: /\.(js|mjs|jsx|ts|tsx)$/,
                include: paths.appSrc,
                loader: require.resolve('ts-loader'),
                options: { transpileOnly: true }
            }

            const { isAdded: tsLoaderIsAdded } = addAfterLoader(
                webpackConfig,
                loaderByName('url-loader'),
                tsLoader
            )
            if (!tsLoaderIsAdded) throwError('failed to add ts-loader')
            console.log('added ts-loader')

            console.log('adding non-application JS babel-loader back')
            const { isAdded: babelLoaderIsAdded } = addAfterLoader(
                webpackConfig,
                loaderByName('ts-loader'),
                matches[1].loader // babel-loader
            )
            if (!babelLoaderIsAdded)
                throwError(
                    'failed to add back babel-loader for non-application JS'
                )
            console.log('added non-application JS babel-loader back')
            return webpackConfig
        }
    }
}
