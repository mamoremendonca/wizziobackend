const SET_VALUE = 'SET_VALUE'

const CLAIMS_STATUS = {
    IN_APPROVAL: 'IN_APPROVAL',
    APPROVED: 'APPROVED',
    SAP_SUBMITTED: 'SAP_SUBMITTED',
    REJECTED: 'REJECTED',
    CANCELED: 'CANCELED',
    PAID: 'PAID'
}

const SCOPE_VALUES = {
    MINE: 'MINE',
    TEAM: 'TEAM',
    ALL: 'ALL'
}

const INITIAL_FILTER_CRITERIA = {
    status: '',
    number: '',
    purchaseOrderNumber: '',
    type: '',
    entityCode: '',
    internalOrderCode: '',
    minVATValue: '',
    maxVATValue: '',
    createdDate: '',
    expirationDate: '',
    departmentCode: '',
    projectCode: '',
    scope: SCOPE_VALUES.MINE
}

const DEFAULT_SORTED_FIELD = {
    orderField: 'creationDate',
    orderType: 'asc'
}

const UPDATE_FILTER_CRITERIA = 'UPDATE_FILTER_CRITERIA'
const UPDATE_SORTING_CRITERIA = 'UPDATE_SORTING_CRITERIA'

const SORTING_ORDER_MAPPER = {
    asc: 'ASC',
    desc: 'DESC'
}
const SORT_FIELDS_MAPPER = {
    repositoryStatus: 'STATUS',
    friendlyNumber: 'NUMBER',
    number: 'NUMBER',
    purchaseOrderFriendlyNumber: 'PO_NUMBER',
    type: 'TYPE',
    supplierCode: 'SUPPLIER',
    internalOrderCode: 'INTERNAL_ORDER',
    totalWithoutTax: 'TOTAL',
    creationDate: 'CREATION_DATE',
    expirationDate: 'EXPIRATION_DATE',
    departmentCode: 'DEPARTMENT',
    projectCode: 'PROJECT',
    userGroup: 'USER_GROUP'
}

const SET_CLAIMS_LIST_PROCESSES = 'SET_CLAIMS_LIST_PROCESSES'

export {
    SET_VALUE,
    CLAIMS_STATUS,
    SCOPE_VALUES,
    INITIAL_FILTER_CRITERIA,
    DEFAULT_SORTED_FIELD,
    UPDATE_FILTER_CRITERIA,
    UPDATE_SORTING_CRITERIA,
    SORTING_ORDER_MAPPER,
    SORT_FIELDS_MAPPER,
    SET_CLAIMS_LIST_PROCESSES
}
