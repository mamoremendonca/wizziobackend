import React from 'react'
//import Tooltip from '@material-ui/core/Tooltip';
import Grid from '@ui-lib/core/Grid'
import FilterList from '../../../components/FilterList'
import Collapse from '@ui-lib/core/Collapse'
import { withStyles } from '@ui-lib/core/styles'
import { styles, ComponentName } from '../ClaimsListStyles'
import { claimListFilterOptions } from './filterOptions'
// import FormGenerator from "";

const ClaimsListFilter = ({ classes, expandFilter, requestFilterData }) => {
    return (
        <div>
            <Grid container item xs={12} spacing={0}>
                <Collapse
                    id="collapseFilterFields"
                    className={classes.collapseFilter}
                    in={expandFilter}
                    timeout="auto">
                    <FilterList onFilterClick={requestFilterData} />
                    {/* <FormGenerator
                        rowNum={2}
                        colNum={5}
                        colSize={204}
                        formOptions={claimListFilterOptions}
                        margin={20}
                    /> */}
                </Collapse>
            </Grid>
        </div>
    )
}

export default withStyles(styles, { name: ComponentName })(ClaimsListFilter)
