import React from 'react'
import LineChartPanel from '../../charts/LineChart/LineChartPanel'
import mockData from '../../charts/LineChart/mockData'
import { lineChartOptions, parseChartData } from '../../charts/LineChart/parser'
import moment from 'moment'

const LineChartPanelDemo = () => {
    return (
        <div>
            <LineChartPanel
                lineChartOptions={lineChartOptions}
                data={parseChartData(mockData)}
                title="Eficiência de Aprovação"
            />
        </div>
    )
}

export default LineChartPanelDemo
