const ComponentName = 'AttachmentListComponentStyles'

const styles = (theme) => ({
    root: {
        width: '100%',
        paddingLeft: '10px'
    }
})

export { ComponentName, styles }
