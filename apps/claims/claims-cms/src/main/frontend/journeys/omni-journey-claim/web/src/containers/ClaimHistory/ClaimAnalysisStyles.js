const ComponentName = 'ClaimHistoryStyles'

const styles = (theme) => ({
    root: {
        padding: '20px'
    }
})

export { ComponentName, styles }
