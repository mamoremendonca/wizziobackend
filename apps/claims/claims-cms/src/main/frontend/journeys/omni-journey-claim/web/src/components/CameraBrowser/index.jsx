import React from 'react'
import ReactWebCam from './ReactWebCam'

const CameraBrowser = () => {
    return (
        <div className="App">
            <ReactWebCam />
        </div>
    )
}

export default CameraBrowser
