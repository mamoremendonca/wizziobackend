import React from 'react'
import { withStyles } from '@ui-lib/core/styles'
import { styles, ComponentName } from './AttachmentStepListStyles'
import Attachment from './Attachment'

const AttachmentStepList = ({
    classes,
    dataSource,
    stepAttachments,
    setContainerLoadingDependencies
}) => {
    return (
        <div className={classes.root}>
            {stepAttachments.attachments.map((attachmentData, index) => {
                return (
                    <Attachment
                        key={`attachment-${index}`}
                        dataSource={dataSource}
                        attachmentData={attachmentData}
                        setContainerLoadingDependencies={
                            setContainerLoadingDependencies
                        }
                    />
                )
            })}
        </div>
    )
}

export default withStyles(styles, { name: ComponentName })(AttachmentStepList)
