import React from 'react'
import AttachmentRender from './AttachmentRender'

const Attachment = ({ attachmentData }) => {
    const downloadAttachment = () => {
        // const {
        //   attachmentData,
        //   dataSource,
        //   I18nProvider,
        //   Notifications,
        //   HttpClient,
        //   FileSystem,
        //   Loading,
        //   financialDocumentData,
        // } = this.props;
        // let instanceId = undefined;
        // if (dataSource === Config.FINANCIAL_DOCUMENT_PROCESS_CONTINUITY) {
        //   instanceId = financialDocumentData.jwcontext.id;
        // } else {
        //   instanceId = financialDocumentData.financialDocumentHeader.repositoryId;
        // }
        // downloadAttachmentRequest(
        //   HttpClient,
        //   Notifications,
        //   FileSystem,
        //   Loading,
        //   instanceId,
        //   attachmentData.id,
        //   I18nProvider.Texti18n("services.download_attachment.error.message"),
        //   dataSource
        // );
    }

    const deleteAttachment = () => {
        // const {
        //   financialDocumentData: {
        //     jwcontext: { id },
        //   },
        //   HttpClient,
        //   Notifications,
        //   removeTimelineAttachment,
        //   timelineData,
        //   I18nProvider,
        //   attachmentData,
        // } = this.props;
        // const attachmentToRemove = {
        //   ...attachmentData,
        //   index: attachmentData.id,
        // };
        // const stepIndex = timelineData.findIndex(
        //   (item) => item.type === "TO_APPROVE"
        // );
        // removeTimelineAttachment(
        //   HttpClient,
        //   stepIndex,
        //   attachmentToRemove,
        //   id,
        //   Notifications,
        //   I18nProvider.Texti18n("service.removeTimelineAttachment.error.message")
        // );
    }
    return (
        <AttachmentRender
            attachmentData={attachmentData}
            downloadAttachment={downloadAttachment}
            deleteAttachment={deleteAttachment}
        />
    )
}

export default Attachment
