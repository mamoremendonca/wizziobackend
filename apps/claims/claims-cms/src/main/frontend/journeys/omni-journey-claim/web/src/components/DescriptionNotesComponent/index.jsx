import React from 'react'
import FieldSetComponent from '../FieldSetComponent'
import CustomTable from '@ui-lib/custom-components/CustomTable'
import Typography from '@ui-lib/core/Typography'

const tableHeader = [
    {
        label: (
            <Typography variant="caption" color="textSecondary">
                Notas
            </Typography>
        ),
        key: 'notes'
    },
    {
        label: (
            <Typography variant="caption" color="textSecondary">
                Data
            </Typography>
        ),
        key: 'date'
    },
    {
        label: (
            <Typography variant="caption" color="textSecondary">
                Utilizador
            </Typography>
        ),
        key: 'user'
    }
]

const DescriptionNotesComponent = ({ descriptionNotesData }) => {
    return (
        <FieldSetComponent
            title={'Descrição e Notas'}
            content={
                <CustomTable
                    headers={tableHeader}
                    data={descriptionNotesData}
                />
            }
        />
    )
}

export default DescriptionNotesComponent
