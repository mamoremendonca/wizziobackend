import React, { useState } from 'react'
import Grid from '@ui-lib/core/Grid'
import IconButton from '@ui-lib/core/IconButton'
import Icon from '@ui-lib/core/Icon'
import HeaderBar from '../../../components/HeaderBar'
import Button from '@ui-lib/core/Button'
import styled from 'styled-components'
import classNames from '@ui-lib/core/classnames'
import { withStyles } from '@ui-lib/core/styles'
import { styles, ComponentName } from '../ClaimsListStyles'

// const Button = styled.button`
//   font-size: 1em;
//   margin: 1em;
//   padding: 0.25em 1em;
//   border-radius: 3px;
// `;

const ClaimsListHeader = ({ classes, I18nProvider, onExpandFilterClick }) => {
    return (
        <div className={classes.root}>
            <HeaderBar hideBackButton>
                <Grid container spacing={0}>
                    <Grid container item xs={6}>
                        <Grid container item xs={12}>
                            <Button
                                id="CreateNewClaim"
                                style={{
                                    height: '40px',
                                    paddingBottom: '6px',
                                    marginRight: '20px'
                                }}
                                variant="contained"
                                color="primary"
                                // onClick={() => navigateToPage("createClaim")}
                            >
                                {I18nProvider.Texti18n('create_claims')}
                                <Icon
                                    className={classNames(
                                        classes.icon,
                                        'icon-add'
                                    )}
                                />
                            </Button>
                            {/* <Button
                                id="ForceDistribution"
                                style={{ height: '40px', paddingBottom: '6px' }}
                                variant="contained"
                                color="primary">
                                {I18nProvider.Texti18n('force_distribution')}
                            </Button> */}
                        </Grid>
                    </Grid>

                    <Grid container item xs={6}>
                        <Grid container justify="flex-end" item xs={12}>
                            <IconButton
                                id="filterIconButton"
                                className={classes.filterIcon}
                                onClick={onExpandFilterClick}>
                                <Icon className={'icon-filter'}></Icon>
                            </IconButton>
                        </Grid>
                    </Grid>
                </Grid>
            </HeaderBar>
        </div>
    )
}

export default withStyles(styles, { name: ComponentName })(ClaimsListHeader)
