import React from 'react'

import LineChartPanel from './LineChartPanel'
import mockData from './mockData'
import { lineChartOptions, parseChartData } from './parser'
import moment from 'moment'

const LineChart = () => {
    return (
        <LineChartPanel
            lineChartOptions={lineChartOptions}
            data={parseChartData(mockData)}
            title="Eficiência de Aprovação"
        />
    )
}

export default LineChart
