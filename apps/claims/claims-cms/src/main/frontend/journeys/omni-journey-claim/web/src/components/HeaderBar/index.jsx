import React from 'react'
import HeaderRender from './HeaderBarRender'

const HeaderBar = ({ children, title }) => {
    return <HeaderRender title={title}>{children}</HeaderRender>
}

export default HeaderBar
