import React from 'react'
import FieldSetComponent from '../FieldSetComponent'
import CustomTable from '@ui-lib/custom-components/CustomTable'
import ClaimStatusTooltip from '../ClaimStatusTooltip'
import ClaimStatusCircle from '../ClaimStatusCircle'
import { withStyles } from '@ui-lib/core/styles'
import { styles, ComponentName } from './RelatedClaimsComponentStyle'

const renderHeader = (classes, i18nProvider) => [
    {
        label: (
            <div className={classes.claimStatusIconArea}>
                <ClaimStatusTooltip i18nProvider={i18nProvider} />
            </div>
        ),
        key: 'status'
    },
    { label: 'ID', key: 'id' },
    { label: 'Canal', key: 'chanel' },
    { label: 'Origem', key: 'origin' },
    { label: 'Motivo', key: 'reason' },
    { label: 'Sub-Motivo', key: 'subReason' },
    { label: 'Data Criação', key: 'date' }
]

const renderData = (relatedClaimsData, classes) => {
    if (relatedClaimsData?.length > 0) {
        return relatedClaimsData.map((relatedClaim) => ({
            ...relatedClaim,
            status: (
                <div className={classes.claimStatusCircleArea}>
                    <ClaimStatusCircle status={relatedClaim.status} />
                </div>
            )
        }))
    }
    return undefined
}

const RelatedClaimsComponent = ({
    relatedClaimsData,
    classes,
    i18nProvider
}) => {
    return (
        <FieldSetComponent
            title={'Reclamação Relacionada'}
            content={
                <CustomTable
                    headers={renderHeader(classes, i18nProvider)}
                    data={renderData(relatedClaimsData, classes)}
                />
            }
        />
    )
}

export default withStyles(styles, { name: ComponentName })(
    RelatedClaimsComponent
)
