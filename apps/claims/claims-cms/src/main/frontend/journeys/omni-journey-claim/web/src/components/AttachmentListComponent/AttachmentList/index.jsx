import React from 'react'
import { styles, ComponentName } from './AttachmentListStyles'
import { withStyles } from '@ui-lib/core/styles'
import AttachmentStepList from './AttachmentStepList'

const AttachmentList = ({
    classes,
    dataSource,
    timelineAttachments,
    setContainerLoadingDependencies
}) => {
    return (
        <div className={classes.root}>
            {timelineAttachments.map(
                (stepAttachments, index) =>
                    stepAttachments.attachments.length > 0 && (
                        <AttachmentStepList
                            key={`attachment-step-list-${index}`}
                            dataSource={dataSource}
                            stepAttachments={stepAttachments}
                            setContainerLoadingDependencies={
                                setContainerLoadingDependencies
                            }
                        />
                    )
            )}
        </div>
    )
}

export default withStyles(styles, { name: ComponentName })(AttachmentList)
