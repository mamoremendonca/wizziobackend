/*-
 * #%L
 * Apps :: Claim APP App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.claims;

import io.digitaljourney.platform.modules.mvc.api.MVCProperties;

/**
 * Claim APP App properties
 */
public final class AppProperties extends MVCProperties {
	private AppProperties() {}
	
	public static final String APP_NAME = "claims";
	public static final String CURRENT_VERSION = "1";
	public static final String APP_VERSION = "/v" + CURRENT_VERSION;
	public static final String ADDRESS = "/" + APP_NAME + APP_VERSION;

	public static final String SUBSERVICE_NAME = SUBSERVICE_PREFIX + APP_NAME;

	public static final String DOCS_ADDRESS = CMS_RESOURCE_PATTERN + ADDRESS + DOCS_PATH;
	
    public static final String ADDRESS_CLAIM_CONTROLLER = ADDRESS + "/instance";
    public static final String ADDRESS_CLAIM_REPOSITORY_CONTROLLER = ADDRESS + "/repository";
	
    public static final String JOURNEY_NAME = "CLAIMS";
    public static final int JOURNEY_VERSION = 1;

	public static final String PERMISSION_ALL = APP_NAME + ACTION_ALL;
	public static final String PERMISSION_CREATE = APP_NAME + ACTION_CREATE;
	public static final String PERMISSION_READ = APP_NAME + ACTION_READ;
	public static final String PERMISSION_UPDATE = APP_NAME + ACTION_UPDATE;
	public static final String PERMISSION_DELETE = APP_NAME + ACTION_DELETE;
	public static final String PERMISSION_EXECUTE = APP_NAME + ACTION_EXECUTE;

	/** General Exception error code */
	public static final String CLAIMS000 = "CLAIMS000";
	
    public static final String FIELD_VALIDATION_EXCEPTION = "FIELD_VALIDATION_EXCEPTION";
    public static final String NOT_FOUND_EXCEPTION = "NOT_FOUND_EXCEPTION";
    public static final String ITEM_NOT_FOUND_EXCEPTION = "ITEM_NOT_FOUND_EXCEPTION";
    public static final String ATTACHMENT_NOT_FOUND_EXCEPTION = "ATTACHMENT_NOT_FOUND_EXCEPTION";
    public static final String FAILED_READ_STREAM_EXCEPTION = "FAILED_READ_STREAM_EXCEPTION";
    public static final String FAILED_TO_RETRIEVE_BLOB_EXCEPTION = "FAILED_TO_RETRIEVE_BLOB_EXCEPTION";
    public static final String FAILED_TO_RETRIEVE_FILE_ECM_EXCEPTION = "FAILED_TO_RETRIEVE_FILE_ECM_EXCEPTION";
    public static final String FAILED_TO_REMOVE_BLOB_EXCEPTION = "FAILED_TO_REMOVE_BLOB_EXCEPTION";
    public static final String FAILED_TO_UPLOAD_BLOB_EXCEPTION = "FAILED_TO_UPLOAD_BLOB_EXCEPTION";
    public static final String USER_COULD_NOT_REMOVE_ATTACHMENT_EXCEPTION = "USER_COULD_NOT_REMOVE_ATTACHMENT_EXCEPTION";
    public static final String STEP_COULD_NOT_REMOVE_ATTACHMENT_EXCEPTION = "STEP_COULD_NOT_REMOVE_ATTACHMENT_EXCEPTION";
    public static final String OWNER_ACTION_USER_CANNOT_EXECUTE_ACTION_EXCEPTION = "OWNER_ACTION_USER_CANNOT_EXECUTE_ACTION_EXCEPTION";
    public static final String CURRENT_GROUP_ACTION_USER_CANNOT_EXECUTE_ACTION_EXCEPTION = "CURRENT_GROUP_ACTION_USER_CANNOT_EXECUTE_ACTION_EXCEPTION";
    public static final String COULD_NOT_CLAIM_EXCEPTION = "COULD_NOT_CLAIM_EXCEPTION";
     
    /** Cache names */
    // TODO: criar os respectivos ficheiros de configuração para Cache
    public static final String CACHE_REFERENCE_DEPARTMENTS = "Cache_Departments";
    public static final String CACHE_USER_INFORMATIONS = "Cache_UserInformations";
    public static final String INTERNAL_SERVER_ERROR_MSG = "Internal Server Error";
    
    // Cache Buckets
    public static final String CACHE_USER_PROPERTIES = "user_properties";
    // Cache keys
    public static final Object CACHE_KEY_GROUP = "group";
}
