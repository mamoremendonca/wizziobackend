package com.novabase.omni.oney.apps.claims.dto.generic;

import java.io.Serializable;

//TODO: Move to oneycommon module after prove the concept
/**
 * Entities that can be paginated and will be returned in a {@link Pageable}
 * must implement this interface
 * 
 * @author NB24879
 *
 */
public interface ListEntity extends Serializable {

}
