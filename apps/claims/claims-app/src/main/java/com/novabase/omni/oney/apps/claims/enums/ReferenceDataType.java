package com.novabase.omni.oney.apps.claims.enums;

import java.util.List;

import com.novabase.omni.oney.apps.claims.agent.referencedata.ReferenceDataAgentImpl;
import com.novabase.omni.oney.apps.claims.dto.generic.ReferenceDataDTO;
import com.novabase.omni.oney.apps.claims.provider.ReferenceDataProvider;

public enum ReferenceDataType {

    STATUS() {

	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getStatus();
	}

    },
    CHANNEL() {

	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getChannels();
	}
    },
    ORIGIN() {

	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getOrigins();
	}

    },
    REASON() {

	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getReasons();
	}

    },
    SUBREASON() {

	@SuppressWarnings("unchecked")
	@Override
	public List<ReferenceDataDTO> getData(final ReferenceDataAgentImpl agent) {
	    return new ReferenceDataProvider(agent).getSubReasons();
	}

    }

    ;

    public abstract <T> List<T> getData(final ReferenceDataAgentImpl agent);

}
