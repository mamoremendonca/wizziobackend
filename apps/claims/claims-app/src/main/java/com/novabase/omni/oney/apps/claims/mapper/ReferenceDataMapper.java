package com.novabase.omni.oney.apps.claims.mapper;

import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.apps.claims.dto.generic.ReferenceDataDTO;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.ReferenceDataEnum;

@Mapper
public interface ReferenceDataMapper {

    public static final ReferenceDataMapper INSTANCE = Mappers.getMapper(ReferenceDataMapper.class);

    @Mapping(expression = "java(stringParam)", target = "code")
    @Mapping(expression = "java(stringParam)", target = "description")
    @Mapping(target = "active", expression = "java(true)")
    public ReferenceDataDTO toReferenceDTO(final String stringParam);

    @IterableMapping(elementTargetType = ReferenceDataDTO.class)
    public List<ReferenceDataDTO> toReferenceDTOList(final List<String> stringParamList);

    @Mapping(target = "active", expression = "java(true)")
    ReferenceDataDTO toReferenceDTOFromEnum(ReferenceDataEnum referenceDataEnum);

    public List<ReferenceDataDTO> toReferenceDTOListFromEnum(final List<ReferenceDataEnum> referenceDataEnum);

}
