package com.novabase.omni.oney.apps.claims.enums;

public enum Reason {
	SIGN_UP,
	ATTENDANCE,
	BLOCKING,
	COLLECT,
	COMMUNICATION,
	GENERAL_CONDITIONS,
	COSTS,
	STATEMENT,
	FRAUD,
	FUNCTION,
	CRC_COMMUNICATION,
	SITE_APP,
	GDPR,
	INSURANCES,
	SERVICES,
	MORATORIUM
}


//	Adesão
//	Atendimento
//	Bloqueio
//	Cobrança
//	Comunicação
//	Condições Gerais
//	Custos
//	Extrato
//	Fraude
//	Funcionamento
//	Comunicação CRC
//	Site e APP
//	RGPD
//	Seguros
//	Serviços
//	Moratória
