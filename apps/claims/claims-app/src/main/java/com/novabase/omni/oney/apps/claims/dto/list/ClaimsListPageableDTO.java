package com.novabase.omni.oney.apps.claims.dto.list;

import com.novabase.omni.oney.apps.claims.dto.generic.Pageable;

public class ClaimsListPageableDTO extends Pageable<ClaimsListFilterDTO, ClaimsListEntityDTO>{

    private static final long serialVersionUID = 6516846724803845917L;

}
