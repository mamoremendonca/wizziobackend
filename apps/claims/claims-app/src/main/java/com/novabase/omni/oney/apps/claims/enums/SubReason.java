package com.novabase.omni.oney.apps.claims.enums;

public enum SubReason {
	NOT_SIGNED_UP,
	REFUSED_PROPOSAL,
	DEFERRED_PROPOSAL,
	SOLICITED_DOCUMENTS,
	SECOND_HOLDER_ANNEXATION,
	ID_DOCUMENT_PHOTO_COLLECT,
	RESPONSE_LENGTH
}

//Não adesão
//Proposta Recusada
//Proposta Diferida
//Documentos solicitados 
//Anexação 2º titular
//Recolha Foto Doc. Identificação
//Demora na Resposta
