package com.novabase.omni.oney.apps.claims.dto.list;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.novabase.omni.oney.apps.claims.dto.generic.SearchFilter;
import com.novabase.omni.oney.apps.claims.enums.ClaimsStatus;
import com.novabase.omni.oney.apps.claims.json.serializer.NBLocalDateTimeSerializer;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.OrderType;

public class ClaimsListFilterDTO extends SearchFilter {

    private static final long serialVersionUID = 4509301582473002005L;

    private ClaimsStatus status;
    private String nif;
    private Long claimId;
    private String origin;
    private String channel;
    private String reason;
    private String subReason;
    private String clientName;
    @JsonSerialize(using =  NBLocalDateTimeSerializer.class)
    private LocalDateTime creationDate;
    @JsonSerialize(using =  NBLocalDateTimeSerializer.class)
    private LocalDateTime limitDate;
    private Integer workingDaysToLimitDate;
    private String assignedTo;

    public ClaimsListFilterDTO() {
	super();
    }

    //@formatter:off
    public ClaimsListFilterDTO(
	    Integer pageNumber, 
	    Integer pageSize, 
	    String sortParam, 
	    OrderType orderType) {
	super(pageNumber, pageSize, sortParam, orderType);
    }
    //@formatter:on

    //@formatter:off
    public ClaimsListFilterDTO(
	    Integer pageNumber, 
	    Integer pageSize, 
	    String sortParam, 
	    OrderType orderType, 
	    ClaimsStatus status, 
	    String nif, 
	    Long claimId, 
	    String origin, 
	    String channel, 
	    String reason,
	    String subReason, 
	    String clientName, 
	    LocalDateTime creationDate, 
	    LocalDateTime limitDate, 
	    Integer workingDaysToLimitDate, 
	    String assignedTo) {
	super(pageNumber, pageSize, sortParam, orderType);
	this.status = status;
	this.nif = nif;
	this.claimId = claimId;
	this.origin = origin;
	this.channel = channel;
	this.reason = reason;
	this.subReason = subReason;
	this.clientName = clientName;
	this.creationDate = creationDate;
	this.limitDate = limitDate;
	this.workingDaysToLimitDate = workingDaysToLimitDate;
	this.assignedTo = assignedTo;
    }
    //@formatter:on

    public ClaimsStatus getStatus() {
	return status;
    }

    public void setStatus(ClaimsStatus status) {
	this.status = status;
    }

    public String getNif() {
	return nif;
    }

    public void setNif(String nif) {
	this.nif = nif;
    }

    public Long getClaimId() {
	return claimId;
    }

    public void setClaimId(Long claimId) {
	this.claimId = claimId;
    }

    public String getOrigin() {
	return origin;
    }

    public void setOrigin(String origin) {
	this.origin = origin;
    }

    public String getChannel() {
	return channel;
    }

    public void setChannel(String channel) {
	this.channel = channel;
    }

    public String getReason() {
	return reason;
    }

    public void setReason(String reason) {
	this.reason = reason;
    }

    public String getSubReason() {
	return subReason;
    }

    public void setSubReason(String subReason) {
	this.subReason = subReason;
    }

    public String getClientName() {
	return clientName;
    }

    public void setClientName(String clientName) {
	this.clientName = clientName;
    }

    public LocalDateTime getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
	this.creationDate = creationDate;
    }

    public LocalDateTime getLimitDate() {
	return limitDate;
    }

    public void setLimitDate(LocalDateTime limitDate) {
	this.limitDate = limitDate;
    }

    public Integer getWorkingDaysToLimitDate() {
	return workingDaysToLimitDate;
    }

    public void setWorkingDaysToLimitDate(Integer workingDaysToLimitDate) {
	this.workingDaysToLimitDate = workingDaysToLimitDate;
    }

    public String getAssignedTo() {
	return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
	this.assignedTo = assignedTo;
    }

}

//Estado (dropdown com os estados possíveis) com possibilidade de excolha multipla)
//NIF
//Contrato
//Origem (dropdown com as origens possíveis)
//Canal (dropdown com os canais possíveis)
//Motivo (dropdown com os motivos possíveis)
//Submotivo (campo de texto livre)
//Nome Cliente (campo de texto livre)
//Data criação (datetime picker)
//Data limite (datetime picker)
//Nº de dias uteis até data limite [em análise]
//Atribuído A.