package com.novabase.omni.oney.apps.claims.enums;

public enum Origin {

	PARTNER,
	EFOP,
	ONEY_HEADQUARTERS,
	PORTUGAL_BANK,
	ASF,
	GAC,
	CLAIMS_PORTAL,
	DECO
}
