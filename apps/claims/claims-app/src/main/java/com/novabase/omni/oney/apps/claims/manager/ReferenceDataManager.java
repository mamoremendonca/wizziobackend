package com.novabase.omni.oney.apps.claims.manager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.util.CollectionUtils;
import org.osgi.service.component.annotations.Component;

import com.novabase.omni.oney.apps.claims.enums.ReferenceDataType;

@Component(service = { ReferenceDataManager.class })
public class ReferenceDataManager {

////    @Reference
//    private ReferenceDataAgentImpl referenceAgent;

    public <T> Map<String, List<T>> getReferenceData(final List<ReferenceDataType> referenceList) {
	
	final Map<String, List<T>> resultMap = new HashMap<>();
	
	if (CollectionUtils.isEmpty(referenceList) == false) {
	    //@formatter:off
	    referenceList.stream()
	    		.forEach(referenceName -> 
	    			resultMap.put(referenceName.name(), referenceName.getData(null))
	    			);
	    //@formatter:on
	}
	
	return resultMap;
	
    }

    
}
