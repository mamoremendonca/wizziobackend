package com.novabase.omni.oney.apps.claims.dto.generic;

import java.io.Serializable;

import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.OrderType;

//TODO: Move to oneycommon module after prove the concept
/**
 * Filters applied to a search that can be paginated that will be returned in a
 * {@link Pageable} must extends this abstract class in other to implement the
 * contract of this functionality
 * 
 * @author NB24879
 *
 */
@SuppressWarnings("serial")
public abstract class SearchFilter implements Serializable {

    private Integer pageNumber;
    private Integer pageSize;
    private String sortParam;
    private OrderType orderType;

    public SearchFilter() {
	super();
    }

    public SearchFilter(Integer pageNumber, Integer pageSize, String sortParam, OrderType orderType) {
	super();
	this.pageNumber = pageNumber;
	this.pageSize = pageSize;
	this.sortParam = sortParam;
	this.orderType = orderType;
    }

    public Integer getPageNumber() {
	return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
	this.pageNumber = pageNumber;
    }

    public Integer getPageSize() {
	return pageSize;
    }

    public void setPageSize(Integer pageSize) {
	this.pageSize = pageSize;
    }

    public String getSortParam() {
	return sortParam;
    }

    public void setSortParam(String sortParam) {
	this.sortParam = sortParam;
    }

    public OrderType getOrderType() {
	return orderType;
    }

    public void setOrderType(OrderType orderType) {
	this.orderType = orderType;
    }

}
