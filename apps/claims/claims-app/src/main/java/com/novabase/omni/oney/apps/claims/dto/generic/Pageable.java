package com.novabase.omni.oney.apps.claims.dto.generic;

import java.io.Serializable;
import java.util.List;

//TODO: Move to oneycommon module after prove the concept
/**
 * This class represents the contract to "pageable" a list of entities using a
 * filter.
 * 
 * @author NB24879
 *
 * @param <F>
 * @param <E>
 */
@SuppressWarnings("serial")
public abstract class Pageable<F extends SearchFilter, E extends ListEntity> implements Serializable {

    private Integer totalPages;
    private Integer totalElements;
    private Integer totalPageElements;
    private F filter;
    private List<E> entities;

    public Integer getTotalPages() {
	return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
	this.totalPages = totalPages;
    }

    public Integer getTotalElements() {
	return totalElements;
    }

    public void setTotalElements(Integer totalElements) {
	this.totalElements = totalElements;
    }

    public Integer getTotalPageElements() {
	return totalPageElements;
    }

    public void setTotalPageElements(Integer totalPageElements) {
	this.totalPageElements = totalPageElements;
    }

    public F getFilter() {
	return filter;
    }

    public void setFilter(F filter) {
	this.filter = filter;
    }

    public List<E> getEntities() {
	return entities;
    }

    public void setEntities(List<E> entities) {
	this.entities = entities;
    }

}
