package com.novabase.omni.oney.apps.claims.dto.history;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.novabase.omni.oney.apps.claims.enums.ClaimsStatus;
import com.novabase.omni.oney.apps.claims.json.serializer.NBLocalDateTimeSerializer;

public class HistoryTimeLineElementDTO implements Serializable {

    private static final long serialVersionUID = 4831365998005858026L;

    private Long claimId;
    @JsonSerialize(using = NBLocalDateTimeSerializer.class)
    private LocalDateTime creationDate;
    private String channel;
    private String origin;
    private String reason;
    private String subReason;
    private String contract;
    private Double amount;
    private ClaimsStatus status;

    public HistoryTimeLineElementDTO() {
	super();
    }

    public HistoryTimeLineElementDTO(Long claimId, LocalDateTime creationDate, String channel,String origin, Double amount, String contract, String reason, String subReason, ClaimsStatus status) {
	super();
	this.claimId = claimId;
	this.creationDate = creationDate;
	this.channel = channel;
	this.origin = origin;
	this.amount = amount;
	this.contract = contract;
	this.reason = reason;
	this.subReason = subReason;
	this.status = status;
    }

    public Long getClaimId() {
	return claimId;
    }

    public void setClaimId(Long claimId) {
	this.claimId = claimId;
    }

    public LocalDateTime getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
	this.creationDate = creationDate;
    }

    public String getReason() {
	return reason;
    }

    public void setReason(String reason) {
	this.reason = reason;
    }

    public String getSubReason() {
	return subReason;
    }

    public void setSubReason(String subReason) {
	this.subReason = subReason;
    }

    public ClaimsStatus getStatus() {
	return status;
    }

    public void setStatus(ClaimsStatus status) {
	this.status = status;
    }

    public String getOrigin() {
	return origin;
    }

    public void setOrigin(String origin) {
	this.origin = origin;
    }

    public String getContract() {
	return contract;
    }

    public void setContract(String contract) {
	this.contract = contract;
    }

    public Double getAmount() {
	return amount;
    }

    public void setAmount(Double amount) {
	this.amount = amount;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
    
}
