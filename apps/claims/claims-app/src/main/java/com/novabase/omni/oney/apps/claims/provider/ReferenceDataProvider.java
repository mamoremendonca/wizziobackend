package com.novabase.omni.oney.apps.claims.provider;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.novabase.omni.oney.apps.claims.agent.referencedata.ReferenceDataAgentImpl;
import com.novabase.omni.oney.apps.claims.dto.generic.ReferenceDataDTO;
import com.novabase.omni.oney.apps.claims.enums.Channel;
import com.novabase.omni.oney.apps.claims.enums.ClaimsStatus;
import com.novabase.omni.oney.apps.claims.enums.Origin;
import com.novabase.omni.oney.apps.claims.enums.Reason;
import com.novabase.omni.oney.apps.claims.enums.SubReason;
import com.novabase.omni.oney.apps.claims.mapper.ReferenceDataMapper;

//@formatter:off
public class ReferenceDataProvider {

    	//private ReferenceDataAgentImpl referenceAgent;

  	public ReferenceDataProvider(final ReferenceDataAgentImpl referenceAgent) {
  		//this.referenceAgent = referenceAgent;
  	}

  	//    public List<ItemDTO> getItems() {
  	//	return ReferenceDataMapper.INSTANCE.toItemDTOList(this.referenceAgent.getItems());
  	//    }
	


	public List<ReferenceDataDTO> getStatus() {

		return ReferenceDataMapper.INSTANCE
				.toReferenceDTOList(
						Arrays.asList(
								ClaimsStatus.values())
						.stream()
						.map(enumParam -> enumParam.name())
						.collect(Collectors.toList())
						);
	}

	public List<ReferenceDataDTO> getChannels() {
		return ReferenceDataMapper.INSTANCE
				.toReferenceDTOList(
						Arrays.asList(
								Channel.values())
						.stream()
						.map(enumParam -> enumParam.name())
						.collect(Collectors.toList())
						);
	}

	public List<ReferenceDataDTO> getOrigins() {
		return ReferenceDataMapper.INSTANCE
				.toReferenceDTOList(
						Arrays.asList(
								Origin.values())
						.stream()
						.map(enumParam -> enumParam.name())
						.collect(Collectors.toList())
						);
	}

	public List<ReferenceDataDTO> getReasons() {
		return ReferenceDataMapper.INSTANCE
				.toReferenceDTOList(
						Arrays.asList(
								Reason.values())
						.stream()
						.map(enumParam -> enumParam.name())
						.collect(Collectors.toList())
						);
	}

	public List<ReferenceDataDTO> getSubReasons() {
		return ReferenceDataMapper.INSTANCE
				.toReferenceDTOList(
						Arrays.asList(
								SubReason.values())
						.stream()
						.map(enumParam -> enumParam.name())
						.collect(Collectors.toList())
						);
	}

}
