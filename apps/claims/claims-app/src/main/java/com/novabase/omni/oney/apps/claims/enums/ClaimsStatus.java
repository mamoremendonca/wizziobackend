package com.novabase.omni.oney.apps.claims.enums;

public enum ClaimsStatus {
    
    //@formatter:off
    NEW, 
    ASSIGNED, 
    IN_PROGRESS,
    FORWARDED,
    INSURANCE_COMPANY,
    OPINION_REQUESTED,
    ADDITIONAL_INFO_REQUESTED,
    OPINION_ANSWERED,
    SOLVED,
    NO_FOLLOW_UP
    //@formatter:on
}

//Nova – Quando a reclamação é registada  
//
//Assignada – Quando a reclamação é assignada a um utilizador após a sua distribuição. 
//
//Em tratamento – Quando o utilizador iniciou a tarefa de tratamento da reclamação. 
//
//Encaminhada – Quando o utilizador encaminha a reclamação para outra area/serviço . 
//
//Aguarda Seguradora – Quando a reclamação é enviada para seguradora e se aguarda resposta. 
//
//Pedido de Parecer – Quando foi solicitado parecer a uma área. 
//
//Pedido de informação adicional – Quando foi solicitado um pedido de informação a outra área. 
//
//Resposta Parecer – Quando a area já deu o parecer/informação. 
//
//Resolvida – Quando reclamação é respondida. 
//
//Sem seguimento – Quando se considera que reclamação fica sem efeito. 