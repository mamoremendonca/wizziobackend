package com.novabase.omni.oney.apps.claims.json.serializer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class NBLocalDateTimeSerializer extends JsonSerializer<LocalDateTime> {

    @Override
    public void serialize(LocalDateTime localDateTime, JsonGenerator json, SerializerProvider provider) throws IOException {
	if (localDateTime != null) {
	    //@formatter:off
		String dateString = 
			DateTimeFormatter
				.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
				.format(localDateTime);
		//@formatter:on
	    json.writeString(dateString);
	}

    }

}
