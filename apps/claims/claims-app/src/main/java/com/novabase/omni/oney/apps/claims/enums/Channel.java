package com.novabase.omni.oney.apps.claims.enums;

public enum Channel {
    
    IN_PERSON,
    PHONE,
    E_MAIL,
    LETTER,
    BACKOFFICE

}

//Presencial
//Telefone
//Email
//Carta
//Backoffice