/*-
 * #%L
 * Apps :: Claim APP App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.claims.controller;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.QueryParam;

import org.osgi.service.component.annotations.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.novabase.omni.oney.apps.claims.AppProperties;
import com.novabase.omni.oney.apps.claims.dto.history.HistoryTimeLineElementDTO;
import com.novabase.omni.oney.apps.claims.dto.list.ClaimsListEntityDTO;
import com.novabase.omni.oney.apps.claims.dto.list.ClaimsListFilterDTO;
import com.novabase.omni.oney.apps.claims.dto.list.ClaimsListPageableDTO;
import com.novabase.omni.oney.apps.claims.enums.Channel;
import com.novabase.omni.oney.apps.claims.enums.ClaimsStatus;
import com.novabase.omni.oney.apps.claims.enums.Origin;
import com.novabase.omni.oney.apps.claims.enums.Reason;
import com.novabase.omni.oney.apps.claims.enums.SubReason;
import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.OrderType;

import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;

//@formatter:off
/**
 * Base Claim APP Resource, this is only used for documentation purposes.
 * *
 */
@Component
@RSProvider(value = AppProperties.ADDRESS_CLAIM_REPOSITORY_CONTROLLER)
@RestController
@RequestMapping(AppProperties.ADDRESS_CLAIM_REPOSITORY_CONTROLLER)
//@formatter:on
public class ClaimsRepositoryController extends AbstractAppController {

    
    @RequestMapping(path = "/history", method = RequestMethod.GET)
    public List<HistoryTimeLineElementDTO> history(@QueryParam("clientId") String clientId) {
	HistoryTimeLineElementDTO element1 = new HistoryTimeLineElementDTO(12231L, LocalDateTime.of(2020,7, 5, 12, 30), Channel.IN_PERSON.name() , Origin.EFOP.name(), 100.0, "Cartão Must 00333102", Reason.COMMUNICATION.name(), SubReason.ID_DOCUMENT_PHOTO_COLLECT.name(), ClaimsStatus.SOLVED);
	HistoryTimeLineElementDTO element2 = new HistoryTimeLineElementDTO(114531L, LocalDateTime.of(2020,2, 3, 11, 30), null , null, null, null, null, null, ClaimsStatus.SOLVED);
	HistoryTimeLineElementDTO element3 = new HistoryTimeLineElementDTO(12561L, LocalDateTime.of(2020,2, 1, 9, 30), Channel.BACKOFFICE.name(), Origin.EFOP.name(), null, null, null, null, ClaimsStatus.SOLVED);
	
	return Arrays.asList(element1, element2, element3);
    }
    
    /**
     * 
     */
    @RequestMapping(path = "/list", method = RequestMethod.POST)
    public ClaimsListPageableDTO list(@RequestBody ClaimsListFilterDTO filter) {
	
	//@formatter:off
	ClaimsListEntityDTO entity1 = new ClaimsListEntityDTO(
		ClaimsStatus.ASSIGNED, 
		"299366499", 
		12L, 
		"Livro Reclamações Banco Portugal", 
		"Telefone", 
		"Cobrança", 
		"Não concorda", 
		"João Lains", 
		LocalDateTime.of(2020,7, 5, 12, 30), 
		LocalDateTime.of(2020,9, 5, 12, 30), 
		35, 
		"João das Neves");
	
	ClaimsListEntityDTO entity2 = new ClaimsListEntityDTO(
		ClaimsStatus.ASSIGNED, 
		"299366129", 
		12L, 
		"Livro Reclamações Banco Portugal", 
		"Telefone", 
		"Cobrança", 
		"Não concorda", 
		"Ricardo Timoteo", 
		LocalDateTime.of(2020,7, 5, 12, 30), 
		LocalDateTime.of(2020,9, 5, 12, 30), 
		35, 
		"Carlos Domingos");
	
	ClaimsListEntityDTO entity3 = new ClaimsListEntityDTO(
		ClaimsStatus.ASSIGNED, 
		"212366129", 
		12L, 
		"Livro Reclamações Banco Portugal", 
		"Telefone", 
		"Cobrança", 
		"Não concorda", 
		"Nuno Castro", 
		LocalDateTime.of(2020,7, 5, 12, 30), 
		LocalDateTime.of(2020,9, 5, 12, 30), 
		35, 
		"Pedro Peres");
	//@formatter:on
	
	ClaimsListPageableDTO claimsListPageable = new ClaimsListPageableDTO();
	
	claimsListPageable.setEntities(Arrays.asList(entity1, entity2, entity3));
	claimsListPageable.setTotalElements(3);
	claimsListPageable.setTotalPageElements(3);
	claimsListPageable.setTotalPages(1);
	claimsListPageable.setFilter(new ClaimsListFilterDTO(1, 100, "nif", OrderType.ASC));
	
	return claimsListPageable;
    }
}
