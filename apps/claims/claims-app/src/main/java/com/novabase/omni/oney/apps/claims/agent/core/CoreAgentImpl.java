/*-
 * #%L
 * Apps :: Claim APP App
 * %%
 * Copyright (C) 2016 - 2019 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.apps.claims.agent.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.apps.claims.AppContext;
import com.novabase.omni.oney.apps.claims.AppProperties;

import io.digitaljourney.platform.modules.mvc.api.agent.AbstractCoreAgent;
import io.digitaljourney.platform.plugins.modules.journeyblueprint.service.api.JourneyBlueprintResource;
import io.digitaljourney.platform.plugins.modules.journeyblueprint.service.api.dto.ActionBlueprintDTO;
import io.digitaljourney.platform.plugins.modules.journeyblueprint.service.api.dto.ActionBlueprintDTOBuilder;
import io.digitaljourney.platform.plugins.modules.journeyblueprint.service.api.dto.BlueprintHeaderDTO;
import io.digitaljourney.platform.plugins.modules.journeyblueprint.service.api.dto.CreateBlueprintDTO;
import io.digitaljourney.platform.plugins.modules.journeyblueprint.service.api.dto.CreateBlueprintDTOBuilder;
import io.digitaljourney.platform.plugins.modules.journeyblueprint.service.api.dto.CreateBlueprintHeaderDTO;
import io.digitaljourney.platform.plugins.modules.journeyblueprint.service.api.dto.CreateBlueprintHeaderDTOBuilder;

/**
 * Implementation of a Core Agent to communicate with micro-service.
 *
 */
// @formatter:off
@Component(
	service = { Object.class, CoreAgentImpl.class },
	configurationPid = CoreAgentConfig.CPID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	reference = {
		@Reference(
			name = AppProperties.REF_CONTEXT,
			service = AppContext.class,
			cardinality = ReferenceCardinality.MANDATORY
		)
	}
)
@Designate(ocd = CoreAgentConfig.class)
// @formatter:on
public class CoreAgentImpl extends AbstractCoreAgent<AppContext, CoreAgentConfig> {

	@Reference
	private JourneyBlueprintResource blueprintProvider;

	/**
	 * Method called whenever the component is activated.
	 *
	 * @param ctx    Component context
	 * @param config Component configuration
	 */
	@Activate
	public void activate(ComponentContext ctx, CoreAgentConfig config) {
		prepare(ctx, config);
	}

	/**
	 * Method called whenever the component configuration changes.
	 *
	 * @param config Component configuration
	 */
	@Modified
	public void modified(CoreAgentConfig config) {
		prepare(config);
	}

	// Journey Designer related methods
	public void createBlueprint(String journeyName, int journeyVersion, String path) {
		String content = null;

		try (InputStream is = CoreAgentImpl.class.getResourceAsStream(path)) {
			content = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8)).lines()
					.collect(Collectors.joining(""));
		} catch (IOException e) {
		}

		String expression = "isActive==true;journeyName==" + journeyName;
		List<BlueprintHeaderDTO> bps = blueprintProvider.searchBlueprint(expression, null, null);

		if (!bps.stream().anyMatch(bp -> bp.journeyName.equals(journeyName))) {
			CreateBlueprintHeaderDTO header = new CreateBlueprintHeaderDTOBuilder().withJourneyName(journeyName)
					.withJourneyFriendlyName(journeyName) // TODO: friendly name must not have special
					// characters, we must receive one more parameter
					// with the friendly name.
					.withMajorVersion(journeyVersion).withMinorVersion(0).build();

			CreateBlueprintDTO create = new CreateBlueprintDTOBuilder().withHeader(header).withContent(content).build();

			BlueprintHeaderDTO createdBlueprint = blueprintProvider.createBlueprint(create);

			ActionBlueprintDTO submit = new ActionBlueprintDTOBuilder().withUpdatedby("demo").build();

			// Submit and Publish blueprint
			blueprintProvider.submitBlueprint(createdBlueprint.id, submit);
			blueprintProvider.publishBlueprint(createdBlueprint.id, submit);
		}
	}
}
