pipeline {
  agent any

  options {
    buildDiscarder(logRotator(numToKeepStr: '2'))
  }

  parameters {
    choice(
      name: 'Action',
      description: 'Action',
      choices: 'Verify\nRelease'
    )
    booleanParam(
      name: 'DockerSkip',
      defaultValue: false,
      description: 'Docker Skip')
  }

  environment {
    POM_FILE = 'build/omni-oney-distro/pom.xml'
  }

  stages {
    stage('Verify') {
      when {
        expression { params.Action == 'Verify' }
      }
      steps {
        withMaven(
          maven: 'maven33',
          mavenSettingsConfig: 'omni-oney-maven-settings') {
          sh "mvn -Ddocker.skip=${params.DockerSkip} -Pverify-profile -f ${POM_FILE} clean install"
        }
      }
    }

    stage('Release') {
      when {
        expression { params.Action == 'Release' }
      }
      steps {
        withMaven(
          maven: 'maven33',
          mavenSettingsConfig: 'omni-oney-maven-settings') {
            sshagent (credentials: ['omni-oney-bitbucket-sshkey']) {
              sh "mvn -Ddocker.skip=${params.DockerSkip} -Prelease-profile -f ${POM_FILE} jgitflow:release-start"
              sh "mvn -Ddocker.skip=${params.DockerSkip} -Prelease-profile -f ${POM_FILE} jgitflow:release-finish"
            }
        }
      }
    }
  }
}
