CREATE USER purchaseorderrepository WITH PASSWORD 'purchaseorderrepository';
GRANT CONNECT ON DATABASE purchaseorderrepository to purchaseorderrepository;
GRANT USAGE ON SCHEMA public TO purchaseorderrepository;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public to purchaseorderrepository;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public to purchaseorderrepository;