package com.novabase.omni.oney.modules.directoryservice.service.api.exception;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;
import io.digitaljourney.platform.modules.ws.rs.api.exception.RSException;
import com.novabase.omni.oney.modules.directoryservice.service.api.DirectoryServiceProperties;

public class DirectoryServiceNotFoundException extends RSException {

    private static final long serialVersionUID = -4823165925222277040L;

    protected DirectoryServiceNotFoundException(PlatformCode statusCode, String errorCode, Context ctx, Object... args) {
	super(statusCode, errorCode, ctx, args);
    }

    protected DirectoryServiceNotFoundException(Context ctx, Object... args) {
	this(PlatformCode.NOT_FOUND, DirectoryServiceProperties.DIRECTORYSERVICE000, ctx, args);
    }

    protected DirectoryServiceNotFoundException(String errorCode, Context ctx, Object... args) {
	this(PlatformCode.NOT_FOUND, errorCode, ctx, args);
    }

    public static DirectoryServiceNotFoundException of(Context ctx, String message) {
	return new DirectoryServiceNotFoundException(ctx, message);
    }

    public static DirectoryServiceNotFoundException of(Context ctx, Throwable cause) {
	return new DirectoryServiceNotFoundException(ctx, cause);
    }
}
