package com.novabase.omni.oney.modules.directoryservice.service.api.exception;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;
import io.digitaljourney.platform.modules.ws.rs.api.exception.RSException;
import com.novabase.omni.oney.modules.directoryservice.service.api.DirectoryServiceProperties;

public class DirectoryServiceException extends RSException {
	private static final long serialVersionUID = -8645577031119256555L;
	
	protected DirectoryServiceException(PlatformCode statusCode, String errorCode, Context ctx, Object... args) {
		super(statusCode, errorCode, ctx, args);
	}
	
	protected DirectoryServiceException(Context ctx, Object... args) {
		this(PlatformCode.INTERNAL_SERVER_ERROR, DirectoryServiceProperties.DIRECTORYSERVICE000, ctx, args);
	}
	
	protected DirectoryServiceException(String errorCode, Context ctx, Object... args) {
		this(PlatformCode.BUSINESS_ERROR, errorCode, ctx, args);
	}

	public static DirectoryServiceException of(Context ctx, String message) {
		return new DirectoryServiceException(ctx, message);
	}
	
	public static DirectoryServiceException of(Context ctx, Throwable cause) {
		return new DirectoryServiceException(ctx, cause);
	}
}
