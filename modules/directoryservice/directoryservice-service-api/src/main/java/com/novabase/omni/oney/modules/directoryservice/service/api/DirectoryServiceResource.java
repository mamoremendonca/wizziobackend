package com.novabase.omni.oney.modules.directoryservice.service.api;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.digitaljourney.platform.modules.commons.type.HttpStatusCode;
import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;

import org.osgi.annotation.versioning.ProviderType;

import com.novabase.omni.oney.modules.directoryservice.service.api.dto.GroupDTO;
import com.novabase.omni.oney.modules.directoryservice.service.api.dto.PrincipalDTO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.ApiKeyAuthDefinition.ApiKeyLocation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.BasicAuthDefinition;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;

//@formatter:off
@ProviderType
@Path("")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@SwaggerDefinition(securityDefinition = @SecurityDefinition(basicAuthDefinitions = {
		@BasicAuthDefinition(key = RSProperties.SWAGGER_BASIC_AUTH) }, apiKeyAuthDefinitions = {
				@ApiKeyAuthDefinition(key = RSProperties.SWAGGER_BEARER_AUTH, name = RSProperties.HTTP_HEADER_API_KEY, in = ApiKeyLocation.HEADER) }), schemes = {
						SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTPS,
						SwaggerDefinition.Scheme.DEFAULT })
@Api(value = "AD Query Service", authorizations = { @Authorization(value = RSProperties.SWAGGER_BASIC_AUTH),
		@Authorization(value = RSProperties.SWAGGER_BEARER_AUTH) })
@ApiResponses(value = {
		@ApiResponse(code = HttpStatusCode.UNAUTHORIZED_CODE, message = RSProperties.SWAGGER_UNAUTHORIZED_MESSAGE),
		@ApiResponse(code = HttpStatusCode.FORBIDDEN_CODE, message = RSProperties.SWAGGER_FORBIDDEN_MESSAGE) })
//@formatter:on
public interface DirectoryServiceResource {

    @POST
    @Path("/{provider}")
    @ApiOperation(value = "Retrieves a list of AD Groups a user belongs to", response = GroupDTO.class)
    List<GroupDTO> getGroups(@ApiParam(value = "The given provider", required = true) @PathParam("provider") String provider,
	    @ApiParam(value = "User Principal", required = true) @Valid String accountName); //TODO: rever esse request, passar como param ou como body

    @GET
    @Path("/{provider}/{accountName}")
    @ApiOperation(value = "Searches for User Principals (filter accountName optional)", response = PrincipalDTO.class)
    List<PrincipalDTO> searchUsers(@ApiParam(value = "The given provider", required = true) @PathParam("provider") String provider,
	    @ApiParam(value = "User accountName", required = false) @PathParam("accountName") String accountName);

}
