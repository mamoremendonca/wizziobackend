package com.novabase.omni.oney.modules.directoryservice.service.api.dto;

import java.io.Serializable;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("PrincipalDTO")
public class PrincipalDTO extends DTO implements Serializable {

    private static final long serialVersionUID = 5391926905431282794L;

    public PrincipalDTO() {
    }

    public PrincipalDTO(String name, String accountName, String mail) {
	this.name = name;
	this.accountName = accountName;
	this.mail = mail;
    }

    @ApiModelProperty(value = "Display Name")
    public String name;
    
    @ApiModelProperty(value = "Username")
    public String accountName;

    @ApiModelProperty(value = "Mail")
    public String mail;

}
