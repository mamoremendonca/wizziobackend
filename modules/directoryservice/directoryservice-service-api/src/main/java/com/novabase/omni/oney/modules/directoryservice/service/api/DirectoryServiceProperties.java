package com.novabase.omni.oney.modules.directoryservice.service.api;

import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;

public abstract class DirectoryServiceProperties extends RSProperties {

    public static final String DIRECTORYSERVICE000 = "DIRECTORYSERVICE000";

}
