package com.novabase.omni.oney.modules.directoryservice.service.api.exception;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;
import io.digitaljourney.platform.modules.ws.rs.api.exception.RSException;

import com.novabase.omni.oney.modules.directoryservice.service.api.DirectoryServiceProperties;

public class DirectoryServiceProviderException extends RSException {

    private static final long serialVersionUID = -5751315773380228688L;

    protected DirectoryServiceProviderException(PlatformCode statusCode, String errorCode, Context ctx, Object... args) {
	super(statusCode, errorCode, ctx, args);
    }

    protected DirectoryServiceProviderException(Context ctx, Object... args) {
	this(PlatformCode.BUSINESS_ERROR, DirectoryServiceProperties.DIRECTORYSERVICE000, ctx, args);
    }

    protected DirectoryServiceProviderException(String errorCode, Context ctx, Object... args) {
	this(PlatformCode.BUSINESS_ERROR, errorCode, ctx, args);
    }

    public static DirectoryServiceProviderException of(Context ctx, String message) {
	return new DirectoryServiceProviderException(ctx, message);
    }

    public static DirectoryServiceProviderException of(Context ctx, Throwable cause) {
	return new DirectoryServiceProviderException(ctx, cause);
    }
}
