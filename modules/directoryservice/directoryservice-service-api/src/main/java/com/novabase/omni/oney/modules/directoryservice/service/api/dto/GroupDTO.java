package com.novabase.omni.oney.modules.directoryservice.service.api.dto;

import java.io.Serializable;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("GroupDTO")
public class GroupDTO extends DTO implements Serializable {

    private static final long serialVersionUID = 3533173363554698735L;

    @ApiModelProperty(value = "Name")
    public String name;

    @ApiModelProperty(value = "Description")
    public String description;
}
