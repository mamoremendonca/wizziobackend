package com.novabase.omni.oney.modules.directoryservice.service.ri;

import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/directoryservice", icon = @Icon(resource = "OSGI-INF/icon/resource.png", size = 32))
public @interface DirectoryServiceResourceConfig { 		
	public static final String CPID = "oney.base.modules.directoryservice.service.ri.directoryservice";
}
