package com.novabase.omni.oney.modules.directoryservice.service.ri;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;
import io.digitaljourney.platform.modules.ws.rs.api.resource.AbstractResource;

import com.novabase.omni.oney.modules.directoryservice.provider.spi.DirectoryServiceDAO;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.entity.Principal;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.exception.DirectoryServiceActiveDirectoryException;
import com.novabase.omni.oney.modules.directoryservice.service.api.DirectoryServiceResource;
import com.novabase.omni.oney.modules.directoryservice.service.api.dto.GroupDTO;
import com.novabase.omni.oney.modules.directoryservice.service.api.dto.PrincipalDTO;
import com.novabase.omni.oney.modules.directoryservice.service.api.exception.DirectoryServiceException;
import com.novabase.omni.oney.modules.directoryservice.service.api.exception.DirectoryServiceProviderException;

// @formatter:off
@Component(
	configurationPid = DirectoryServiceResourceConfig.CPID, 
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	reference = {
		@Reference(
			name = DirectoryServiceResourceProperties.REF_CONTEXT,
			service = DirectoryServiceContext.class,
			cardinality = ReferenceCardinality.MANDATORY)
})
@Designate(ocd = DirectoryServiceResourceConfig.class)
@RSProvider(DirectoryServiceResourceProperties.ADDRESS)
// @formatter:on
public class DirectoryServiceResourceImpl extends AbstractResource<DirectoryServiceContext, DirectoryServiceResourceConfig> implements DirectoryServiceResource {

    @Reference
    private volatile List<DirectoryServiceDAO> directoryServiceDAO;
    
    @Activate
    public void activate(ComponentContext ctx, DirectoryServiceResourceConfig config) {
	prepare(ctx, config);
    }

    @Modified
    public void modified(DirectoryServiceResourceConfig config) {
	prepare(config);
    }

    @Override
    @RequiresPermissions({ DirectoryServiceResourceProperties.PERMISSION_EXECUTE })
    public List<GroupDTO> getGroups(String provider, String accountName) {

	try {
	    DirectoryServiceDAO dao = searchProvider(provider);
	    
	    // Since group membership search requires the distinguishedName, it's necessary to search for Principal using accountName
	    List<Principal> principals = dao.searchUsers(accountName);
	
	    // If more than 1 principal is returned => throw exception
	    if (principals != null && principals.size() > 1) {
		throw new DirectoryServiceActiveDirectoryException("More than one user account was returned for accountname [" + accountName + "]");
	    }
	    
	    return (principals != null && principals.size() > 0 &&
		    principals.get(0) != null && principals.get(0).getDistinguishedName() != null?
		    	DirectoryServiceResourceMapper.INSTANCE.toListGroupDTO(dao.getGroups(principals.get(0)))
        		: null);
	
	} catch(DirectoryServiceException e) {
	    throw DirectoryServiceException.of(getCtx(), e);
	} catch(DirectoryServiceProviderException e) {
	    throw DirectoryServiceException.of(getCtx(), e);
	} catch (Throwable t) {
	    throw DirectoryServiceException.of(getCtx(), t);
	}
    }

    @Override
    public List<PrincipalDTO> searchUsers(String provider, String filter) {
	// TODO: passar aqui o basedn do grupo do wizzio a ser importado os users, esta
		// a ir sempre vazio pois no MS foi alterado para importar os users, tem que vir
		// da APP esse parametro
	try {
	    DirectoryServiceDAO dao = searchProvider(provider);
	    
	    return DirectoryServiceResourceMapper.INSTANCE.toListPrincipalDTO(dao.searchUsers(filter));
	    
	} catch(DirectoryServiceProviderException e) {
	    throw e;
	} catch (Throwable t) {
	    throw DirectoryServiceException.of(getCtx(), t);
	}
    }
    
    private DirectoryServiceDAO searchProvider(String provider) {

	//@formatter:off
	DirectoryServiceDAO dao = directoryServiceDAO.stream()
                        		.filter(s -> provider.equals(s.getProviderInfo().getName()))
                        		.findAny()
                        		.orElse(null);
	//@formatter:on
	
	if (dao == null)
	    throw DirectoryServiceProviderException.of(getCtx(), "Couldn't find provider [" + provider + "]");
	
	return dao;
    }
}
