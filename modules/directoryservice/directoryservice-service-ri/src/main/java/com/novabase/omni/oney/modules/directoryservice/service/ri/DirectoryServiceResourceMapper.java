package com.novabase.omni.oney.modules.directoryservice.service.ri;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.modules.directoryservice.provider.spi.entity.Group;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.entity.Principal;
import com.novabase.omni.oney.modules.directoryservice.service.api.dto.GroupDTO;
import com.novabase.omni.oney.modules.directoryservice.service.api.dto.PrincipalDTO;

@Mapper
public interface DirectoryServiceResourceMapper {
    public static final DirectoryServiceResourceMapper INSTANCE = Mappers.getMapper(DirectoryServiceResourceMapper.class);

    List<GroupDTO> toListGroupDTO(List<Group> lst);

    GroupDTO toGroupDTO(Group grp);

    @Mapping(source = "name", target = "displayName")
    @Mapping(ignore = true, target = "distinguishedName")
    @Mapping(source = "mail", target = "email")
    Principal toPrincipal(PrincipalDTO dto);

    @Mapping(source = "displayName", target = "name")
    @Mapping(source = "email", target = "mail")
    PrincipalDTO toPrincipalDTO(Principal principal);

    List<PrincipalDTO> toListPrincipalDTO(List<Principal> principal);
}
