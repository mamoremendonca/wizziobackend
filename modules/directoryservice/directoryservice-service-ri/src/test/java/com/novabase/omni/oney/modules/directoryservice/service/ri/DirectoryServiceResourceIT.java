package com.novabase.omni.oney.modules.directoryservice.service.ri;

import static io.digitaljourney.platform.modules.paxexam.BundleOptions.mavenBundles;
import static io.digitaljourney.platform.modules.paxexam.BundleOptions.rsProvider;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.newConfiguration;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.rsProviderConfiguration;
import static org.ops4j.pax.exam.CoreOptions.composite;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;

import io.digitaljourney.platform.modules.paxexam.base.BaseTestSupport;
import com.novabase.omni.oney.modules.directoryservice.service.api.DirectoryServiceResource;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class DirectoryServiceResourceIT extends BaseTestSupport {
	@SuppressWarnings("unused")
	@Inject 
	private DirectoryServiceResource directoryServiceResource;

	@Override
	protected Option bundles() {
		return composite(super.bundles(), rsProvider(),
				mavenBundles("com.novabase.omni.oney.modules", "directoryservice-provider-spi", "directoryservice-service-api"),
				testBundle("com.novabase.omni.oney.modules", "directoryservice-service-ri"));
	}

	@Override
	protected Option configurations() {
		return composite(super.configurations(), rsProviderConfiguration("directoryservice", newConfiguration(DirectoryServiceResourceConfig.CPID)));
	}

	@Test
	public void testBundle() {
		assertBundleActive("com.novabase.omni.oney.modules.directoryservice-service-ri");
	}
}
