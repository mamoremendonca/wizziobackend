package com.novabase.omni.oney.modules.directoryservice.provider.spi.exception;

public class DirectoryServiceActiveDirectoryException extends DirectoryServiceException {

    private static final long serialVersionUID = -3414922654790335090L;

    public DirectoryServiceActiveDirectoryException(String msg) {
	super(msg);
    }
}
