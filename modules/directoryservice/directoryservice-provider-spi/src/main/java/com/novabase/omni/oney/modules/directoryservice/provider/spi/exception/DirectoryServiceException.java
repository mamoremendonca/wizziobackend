package com.novabase.omni.oney.modules.directoryservice.provider.spi.exception;

public class DirectoryServiceException extends Exception {

    private static final long serialVersionUID = -3820927433370157670L;

    public DirectoryServiceException(String msg) {
	super(msg);
    }
    
    public DirectoryServiceException(Exception e) {
	super(e);
    }

    public DirectoryServiceException(Throwable t) {
	super(t);
    }
}
