package com.novabase.omni.oney.modules.directoryservice.provider.spi.entity;

public class Group {

	public Group(String _name, String _description) {
		this.name = _name;
		this.description = _description;
	}
	
	public String name;

	public String description;
}
