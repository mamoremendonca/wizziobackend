package com.novabase.omni.oney.modules.directoryservice.provider.spi;

import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

import com.novabase.omni.oney.modules.directoryservice.provider.spi.entity.Group;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.entity.Principal;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.entity.ProviderInfo;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.exception.DirectoryServiceException;

@ProviderType
public interface DirectoryServiceDAO {

    ProviderInfo getProviderInfo();

    // Methods
    List<Principal> searchUsers(String accountName) throws DirectoryServiceException;
    
    List<Group> getGroups(Principal principal) throws DirectoryServiceException;
    
}




