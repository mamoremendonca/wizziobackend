package com.novabase.omni.oney.modules.directoryservice.provider.spi.entity;

import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
public class ProviderInfo {

    private String name;
    private int priority;
    
    public String getName() {
	return name;
    }
    public void setName(String name) {
	this.name = name;
    }
    
    public int getPriority() {
	return priority;
    }
    public void setPriority(int priority) {
	this.priority = priority;
    }
    
}
