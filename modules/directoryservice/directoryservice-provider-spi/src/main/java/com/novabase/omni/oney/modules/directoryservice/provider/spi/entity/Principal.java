package com.novabase.omni.oney.modules.directoryservice.provider.spi.entity;

public class Principal {

    public Principal() {
    }

    private String displayName;
    
    private String accountName;
    
    private String distinguishedName;
    
    private String email;

    
    public String getAccountName() {
	return accountName;
    }

    public void setAccountName(String accountName) {
	this.accountName = accountName;
    }

    public String getDistinguishedName() {
	return distinguishedName;
    }

    public void setDistinguishedName(String distinguishedName) {
	this.distinguishedName = distinguishedName;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getDisplayName() {
	return displayName;
    }

    public void setDisplayName(String displayName) {
	this.displayName = displayName;
    }
}
