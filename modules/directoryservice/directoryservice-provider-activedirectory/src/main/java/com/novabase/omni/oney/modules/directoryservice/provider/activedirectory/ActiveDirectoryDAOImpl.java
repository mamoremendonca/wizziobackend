package com.novabase.omni.oney.modules.directoryservice.provider.activedirectory;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.directory.api.ldap.model.cursor.CursorException;
import org.apache.directory.api.ldap.model.cursor.SearchCursor;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.api.ldap.model.message.Response;
import org.apache.directory.api.ldap.model.message.SearchRequest;
import org.apache.directory.api.ldap.model.message.SearchRequestImpl;
import org.apache.directory.api.ldap.model.message.SearchResultEntry;
import org.apache.directory.api.ldap.model.message.SearchScope;
import org.apache.directory.api.ldap.model.name.Dn;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapConnectionConfig;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import io.digitaljourney.platform.modules.commons.AbstractConfigurableComponent;

import com.novabase.omni.oney.modules.directoryservice.provider.WSContext;
import com.novabase.omni.oney.modules.directoryservice.provider.activedirectory.mapper.EntryMapper;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.DirectoryServiceDAO;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.entity.Group;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.entity.Principal;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.entity.ProviderInfo;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.entity.ProviderInfoBuilder;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.exception.DirectoryServiceActiveDirectoryException;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.exception.DirectoryServiceException;

// @formatter:off
@Component(
  configurationPid = ActiveDirectoryDAOConfig.CPID,
  configurationPolicy = ConfigurationPolicy.REQUIRE,
  reference = {
    @Reference(
      name = ActiveDirectoryDAOProperties.REF_CONTEXT,
      service = WSContext.class, 
      cardinality = ReferenceCardinality.MANDATORY) })
@Designate(ocd = ActiveDirectoryDAOConfig.class)
// @formatter:on
public final class ActiveDirectoryDAOImpl extends AbstractConfigurableComponent<ActiveDirectoryDAOConfig> implements DirectoryServiceDAO {

    @Activate
    public void activate(ComponentContext ctx, ActiveDirectoryDAOConfig config) {
	prepare(config);
    }

    @Modified
    public void modified(ActiveDirectoryDAOConfig config) {
	prepare(config);
    }

    @Override
    public ProviderInfo getProviderInfo() {
	return new ProviderInfoBuilder().withName(getConfig().providerName()).build();
    }

    @Override
    public List<Principal> searchUsers(String accountName) throws DirectoryServiceException {
	// TODO: passar aqui o basedn do grupo do wizzio a ser importado os users, esta
		// a ir sempre vazio pois no MS foi alterado para importar os users, tem que vir
		// da APP esse parametro
	List<Principal> principals = new ArrayList<Principal>();
	LdapConnection con = null;

	try {
	    // Open Connection
	    con = openConnection();

	    if (isConnected(con)) {

		principals = filterUsers(con, accountName);

	    }

	} catch (DirectoryServiceException e) {
	    throw e;
	} catch (Throwable t) {
	    throw new DirectoryServiceException("Error retrieving user groups. \n" + t.getMessage());
	} finally {
	    // Close connection
	    closeConnection(con);
	}

	return principals;
    }

    @Override
    public List<Group> getGroups(Principal principal) throws DirectoryServiceException {

	List<Group> groups = new ArrayList<Group>();
	LdapConnection con = null;

	try {
	    // Open Connection
	    con = openConnection();

	    if (isConnected(con)) {

		groups = findGroupsFromUser(con, principal);

	    }

	} catch (DirectoryServiceException e) {
	    throw e;
	} catch (LdapException e) {
	    throw new DirectoryServiceActiveDirectoryException("Error connecting to AD. Message received:\n" + e.getMessage());
	} catch (Throwable t) {
	    throw new DirectoryServiceException("Error retrieving user groups. \n" + t.getMessage());
	} finally {
	    // Close connection
	    closeConnection(con);
	}

	return groups;
    }

    // #---------------------------------------------------------------------------

    private List<Group> findGroupsFromUser(LdapConnection con, Principal principal) throws LdapException, CursorException, IOException {

	List<Group> groups = new ArrayList<Group>();

	if (principal.getDistinguishedName() != null && principal.getDistinguishedName().length() > 0) {

	    SearchCursor cursor = searchByFilter(con, getConfig().categoryGroup(), filterGroupsByDistinguishedUser(principal.getDistinguishedName(), getConfig().filterGroups()));

	    // Process results
	    while (cursor.next()) {
		Response response = cursor.get();
		if (response instanceof SearchResultEntry) {
		    Entry entry = ((SearchResultEntry) response).getEntry();
		    groups.add(EntryMapper.INSTANCE.toGroup(entry));
		}
	    }

	    // Close cursor
	    cursor.close();
	}

	return groups;
    }

    private List<Principal> filterUsers(LdapConnection con, String accountName) throws LdapException, DirectoryServiceException, CursorException {
	List<Principal> principals = new ArrayList<Principal>();
	
	// Build LDAP filter
	String filter = buildFilter(accountName); //TODO: revisar isso
//	String filter = getConfig().filterUsers(); 
		
	// Search Users
	SearchCursor cursor = searchByFilter(con, getConfig().categoryUser(), filter);

	if (hasContent(cursor)) {
	    
	    // Process results
	    while (cursor.next()) {
		Response response = cursor.get();
		if (response instanceof SearchResultEntry) {
		    Entry entry = ((SearchResultEntry) response).getEntry();
		    principals.add(EntryMapper.INSTANCE.toPrincipal(entry));
		}
	    }

	    // close cursor;
	    closeCursor(cursor);
	    
	} else {

	    throw new DirectoryServiceActiveDirectoryException("No User was found in BaseDN [" + getConfig().basePath() + "] for filter [" + filter + "].");

	}
	
	return principals;
    }

    // LDAP Filter related methods
    private String filterGroupsByDistinguishedUser(String distinguishedName, String groupFilter) {
	return MessageFormat.format(getConfig().filterGroup(), distinguishedName, groupFilter);
    }
    private String buildFilter(String accountName) {
	return (accountName != null && !accountName.equals("")? 
		MessageFormat.format(getConfig().filterUser(), accountName) :
		getConfig().filterUsers());
    }
    private SearchCursor searchByFilter(LdapConnection con, String category, String filter) throws LdapException {

	SearchRequest req = new SearchRequestImpl();
	req.setScope(SearchScope.SUBTREE);
	req.addAttributes(category);
	req.setTimeLimit(0);
	req.setBase(new Dn(getConfig().basePath()));
	req.setFilter(filter);

	return con.search(req);
    }

    // Cursor related methods
    private boolean hasContent(SearchCursor cursor) throws DirectoryServiceException {
	return (cursor != null && !cursor.isDone() && !cursor.isClosed());
    }
    private void closeCursor(SearchCursor cursor) {
	try {
	    if (!cursor.isClosed()) cursor.close();
	} catch (IOException e) {}
    }
    
    // Ldap related methods
    private boolean isConnected(LdapConnection con) throws DirectoryServiceException {

	if (!(con != null && con.isConnected() && con.isAuthenticated())) {
	    throw new DirectoryServiceActiveDirectoryException("Could not open a LDAP Connection to [" + getConfig().server() + "].");
	}

	return true;
    }
    private void bindADConnection(LdapConnection con) throws LdapException {
	if (getConfig().bindUser() != null && getConfig().bindUser() != "") {
	    if (getConfig().bindCredentials() != null && getConfig().bindCredentials() != "") {
		con.bind(getConfig().bindUser(), getConfig().bindCredentials());

	    } else {
		con.bind(getConfig().bindUser());

	    }
	} else {
	    con.bind();
	}
    }
    private LdapConnection openConnection() throws DirectoryServiceException {

	try {

	    // TODO: Improvement => consider using a connection pool => must refactor
	    // Provider to instanciate pool and connectionConfig at activation (probably).
	    
	    //TODO: o produto disponibiliza a connection com o resource : https://support-digitaljourney.westeurope.cloudapp.azure.com/docs/libs/directory/v1.5.32/docs/user-manual.html

	    LdapConnectionConfig config = new LdapConnectionConfig();
	    config.setLdapHost(getConfig().server()); // Novabase: "adptl202.novabase.intra" => "172.21.127.2"
	    config.setLdapPort(getConfig().port()); // 389
//	    config.setName("NB24879@novabase.pt");
//	    config.setCredentials("xxxxxx");
	    config.setTimeout(getConfig().timeout());

	    LdapConnection con = new LdapNetworkConnection(config);
	    
	    // Bind connection (using user/credentials if needed)
	    bindADConnection(con);
//	    con.bind();
	    
	    return con;

	} catch (LdapException e) {
	    throw new DirectoryServiceActiveDirectoryException("Error opening LDAP Connection:\n" + e.getMessage());
	} catch (Throwable t) {
	    throw new DirectoryServiceException("Error opening LDAP Connection:\n" + t);
	}
    }
    private void closeConnection(LdapConnection con) {
	if (con != null && con.isConnected()) {
	    try {
		con.close();
		con.unBind();
	    } catch (Throwable th) {}
	}
    }
}
