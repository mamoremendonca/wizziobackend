package com.novabase.omni.oney.modules.directoryservice.provider.activedirectory;

import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;

public final class ActiveDirectoryDAOProperties extends RSProperties {

    private ActiveDirectoryDAOProperties() {
    }

    public static final String LDAP_ATTRIBUTE_PRINCIPALNAME = "userPrincipalName";
    public static final String LDAP_ATTRIBUTE_SAMACCOUNTNAME = "sAMAccountName";
    public static final String LDAP_ATTRIBUTE_NAME = "name";
    public static final String LDAP_ATTRIBUTE_MAIL = "userPrincipalName";
    public static final String LDAP_ATTRIBUTE_DISTINGUISHEDNAME = "distinguishedName";
    public static final String LDAP_ATTRIBUTE_DISPLAYNAME = "displayName";
    public static final String LDAP_ATTRIBUTE_DESCRIPTION = "description";
    public static final String LDAP_ATTRIBUTE_DEPARTMENT = "department";
}
