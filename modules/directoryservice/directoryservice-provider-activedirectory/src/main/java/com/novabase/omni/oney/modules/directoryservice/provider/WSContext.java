/*-
 * #%L
 * Modules :: Directory Services Provider :: ActiveDirectory
 * %%
 * Copyright (C) 2016 - 2018 Digital Journey
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Digital Journey, Novabase Business Solutions S.A. 
 * and its licensors. The disclosure,copying, adaptation, citation, transcription, 
 * translation, modification, decompilation, reverse engineering, derivatives, 
 * integration, development and/or any other form of total or partial use of the 
 * content of this document and/or accessible through or via the contents, by any 
 * possible means without the respective authorization or licensing by the owner of 
 * the intellectual property rights is prohibited, the offenders being subject to civil 
 * and/or criminal prosecution and liability. The user or licensee of all or part of this 
 * document by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.directoryservice.provider;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

import io.digitaljourney.platform.modules.uriql.api.UriqlProvider;
import io.digitaljourney.platform.modules.ws.api.WSProperties;
import io.digitaljourney.platform.modules.ws.rs.api.context.AbstractRSProxyContext;

// @formatter:off
@Component(
	service = WSContext.class,
	reference = {
		@Reference(
			name = WSProperties.REF_URIQL,
			service = UriqlProvider.class,
			cardinality = ReferenceCardinality.MANDATORY)
	})
// @formatter:on
public final class WSContext extends AbstractRSProxyContext {

    @Activate
    public void activate(ComponentContext ctx) {
	prepare(ctx);
    }
}
