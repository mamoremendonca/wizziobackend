package com.novabase.omni.oney.modules.directoryservice.provider.activedirectory.mapper;

import org.apache.directory.api.ldap.model.entry.Entry;
import org.mapstruct.Mapper;
//import org.mapstruct.Mapping;
//import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.modules.directoryservice.provider.spi.entity.Group;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.entity.Principal;

@Mapper(uses = { EntryMapper.class})
public interface ActiveDirectoryDAOMapper {

    public static final ActiveDirectoryDAOMapper INSTANCE = Mappers.getMapper(ActiveDirectoryDAOMapper.class);

//    @Mapping(target = "name", qualifiedByName = "attributeDisplayName")
//    @Mapping(target = "description", qualifiedByName = "attributeDescription")
//    Group toGroup(Entry entry);
//
//    @Mapping(target = "accountName", qualifiedByName = "attributeAccountName")
//    @Mapping(target = "distinguishedName", qualifiedByName = "attributeDistinguishedName")
//    @Mapping(target = "email", qualifiedByName = "attributeEmail")
//    Principal toPrincipal(Entry entry);
//
//    // ------------------------------------------------
//    // Specific for Entry Class
//    @Named("attributeAccountName")
//    public static String attributeAccountName(Entry entry) {
//	return (entry.containsAttribute(ActiveDirectoryDAOImpl.LDAP_ATTRIBUTE_SAMACCOUNTNAME) ? 
//			entry.get(ActiveDirectoryDAOImpl.LDAP_ATTRIBUTE_SAMACCOUNTNAME).toString() : "");
//    }
//    @Named("attributeDistinguishedName")
//    public static String attributeDistinguishedName(Entry entry) {
//	return (entry.containsAttribute(ActiveDirectoryDAOImpl.LDAP_ATTRIBUTE_DISTINGUISHEDNAME) ? 
//			entry.get(ActiveDirectoryDAOImpl.LDAP_ATTRIBUTE_DISTINGUISHEDNAME).toString() : "");
//    }
//    @Named("attributeEmail")
//    public static String attributeEmail(Entry entry) {
//	return (entry.containsAttribute(ActiveDirectoryDAOImpl.LDAP_ATTRIBUTE_MAIL) ? 
//			entry.get(ActiveDirectoryDAOImpl.LDAP_ATTRIBUTE_MAIL).toString() : "");
//    }
//    @Named("attributeDisplayName")
//    public static String attributeDisplayName(Entry entry) {
//	return (entry.containsAttribute(ActiveDirectoryDAOImpl.LDAP_ATTRIBUTE_DISPLAYNAME) ? 
//			entry.get(ActiveDirectoryDAOImpl.LDAP_ATTRIBUTE_DISPLAYNAME).toString() : "");
//    }
//    @Named("attributeDescription")
//    public static String attributeDescription(Entry entry) {
//	return (entry.containsAttribute(ActiveDirectoryDAOImpl.LDAP_ATTRIBUTE_DESCRIPTION) ? 
//			entry.get(ActiveDirectoryDAOImpl.LDAP_ATTRIBUTE_DESCRIPTION).toString() : "");
//    }
//    // ------------------------------------------------
    
    
}
