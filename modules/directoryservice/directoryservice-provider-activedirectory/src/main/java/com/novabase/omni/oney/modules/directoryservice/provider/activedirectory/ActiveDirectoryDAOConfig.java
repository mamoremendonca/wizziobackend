package com.novabase.omni.oney.modules.directoryservice.provider.activedirectory;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/activedirectory/activedirectory", icon = @Icon(resource = "OSGI-INF/icon/ws.png", size = 32))
public @interface ActiveDirectoryDAOConfig {

    public static final String CPID = "com.novabase.omni.oney.modules.directoryservice.provider.activedirectory.activedirectory";

    @AttributeDefinition(name = "%providerName", description = "%providerName")
    String providerName() default "activedirectory";

    @AttributeDefinition(name = "%server.name", description = "%server.description")
    String server() default "172.21.127.2";

    @AttributeDefinition(name = "%port.name", description = "%port.description")
    int port() default 389;

    @AttributeDefinition(name = "%domain.name", description = "%domain.description")
    String domain() default "novabase.intra";

    @AttributeDefinition(name = "%bindUser.name", description = "%bindUser.description")
    String bindUser() default "CN=UtilizadorX,CN=Users,DC=novabase,DC=intra";

    @AttributeDefinition(name = "%bindCredentials.name", description = "%bindCredentials.description")
    String bindCredentials() default "passwordX";

    // ------------------------------------------------------------------------------------------------
    @AttributeDefinition(name = "%basePath.name", description = "%basePath.description")
    String basePath() default "CN=Users,DC=novabase,DC=intra";

    @AttributeDefinition(name = "%categoryGroup.name", description = "%categoryGroup.description")
    String categoryGroup() default "(objectCategory=group)";

    @AttributeDefinition(name = "%categoryUser.name", description = "%categoryUser.description")
    String categoryUser() default "(objectCategory=user)";

    @AttributeDefinition(name = "%filterGroup.name", description = "%filterGroup.description")
    String filterGroup() default "(&(objectClass=group)(member={0})(cn={1}))";

    @AttributeDefinition(name = "%filterUser.name", description = "%filterUser.description")
    String filterUser() default "(&(objectClass=person)(sAMAccountName={0}))";

    @AttributeDefinition(name = "%filterUsers.name", description = "%filterUsers.description")
    String filterUsers() default "(&(objectClass=person)(memberOf=CN=WIZZIOUsers,CN=Servers and Services,CN=Network and Systems Access Security Groups,CN=Users,DC=oney,DC=pt))";
    
    @AttributeDefinition(name = "%filterGroups.name", description = "%filterGroups.description")
    String filterGroups() default "*";
    
    @AttributeDefinition(name = "%timeout.name", description = "%timeout.description")
    int timeout() default 10;
}
