package com.novabase.omni.oney.modules.directoryservice.provider.activedirectory.mapper;

import org.apache.commons.lang3.StringUtils;
import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.exception.LdapInvalidAttributeValueException;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.modules.directoryservice.provider.activedirectory.ActiveDirectoryDAOProperties;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.entity.Group;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.entity.Principal;

@Mapper
public class EntryMapper {

	public static final EntryMapper INSTANCE = Mappers.getMapper(EntryMapper.class);

	public Group toGroup(Entry entry) throws LdapInvalidAttributeValueException {
	    Group group = new Group(
			(entry.containsAttribute(ActiveDirectoryDAOProperties.LDAP_ATTRIBUTE_SAMACCOUNTNAME) ? 
				entry.get(ActiveDirectoryDAOProperties.LDAP_ATTRIBUTE_SAMACCOUNTNAME).getString() : ""),
			(entry.containsAttribute(ActiveDirectoryDAOProperties.LDAP_ATTRIBUTE_DESCRIPTION) ? 
				entry.get(ActiveDirectoryDAOProperties.LDAP_ATTRIBUTE_DESCRIPTION).getString() : "")
		    );
	    return group;
	}

	public Principal toPrincipal(Entry entry) throws LdapInvalidAttributeValueException {
	    Principal principal = new Principal();
	    principal.setDisplayName((entry.containsAttribute(ActiveDirectoryDAOProperties.LDAP_ATTRIBUTE_DISPLAYNAME) ? entry.get(ActiveDirectoryDAOProperties.LDAP_ATTRIBUTE_DISPLAYNAME).getString() : ""));
	    
	    if(StringUtils.isBlank(principal.getDisplayName())) {
		//Safe set
		principal.setDisplayName((entry.containsAttribute(ActiveDirectoryDAOProperties.LDAP_ATTRIBUTE_NAME) ? entry.get(ActiveDirectoryDAOProperties.LDAP_ATTRIBUTE_NAME).getString() : ""));
	    }
	    
	    principal.setAccountName((entry.containsAttribute(ActiveDirectoryDAOProperties.LDAP_ATTRIBUTE_SAMACCOUNTNAME) ? entry.get(ActiveDirectoryDAOProperties.LDAP_ATTRIBUTE_SAMACCOUNTNAME).getString() : ""));
	    principal.setDistinguishedName((entry.containsAttribute(ActiveDirectoryDAOProperties.LDAP_ATTRIBUTE_DISTINGUISHEDNAME) ? entry.get(ActiveDirectoryDAOProperties.LDAP_ATTRIBUTE_DISTINGUISHEDNAME).getString() : ""));
	    principal.setEmail((entry.containsAttribute(ActiveDirectoryDAOProperties.LDAP_ATTRIBUTE_MAIL) ? entry.get(ActiveDirectoryDAOProperties.LDAP_ATTRIBUTE_MAIL).getString() : ""));
	    
	    return principal;
	}
}
