package com.novabase.omni.oney.modules.directoryservice.provider.activedirectory;

import static io.digitaljourney.platform.modules.paxexam.BundleOptions.mavenBundles;
import static io.digitaljourney.platform.modules.paxexam.BundleOptions.rsClient;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.newConfiguration;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.rsClientConfiguration;
import static org.ops4j.pax.exam.CoreOptions.composite;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;

import io.digitaljourney.platform.modules.paxexam.base.BaseTestSupport;

import com.novabase.omni.oney.modules.directoryservice.provider.activedirectory.ActiveDirectoryDAOConfig;
import com.novabase.omni.oney.modules.directoryservice.provider.spi.DirectoryServiceDAO;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class ActiveDirectoryDAOIT extends BaseTestSupport {

  @SuppressWarnings("unused")
  @Inject
  private DirectoryServiceDAO dao;

  @Override
  protected Option bundles() {
    return composite(super.bundles(), rsClient(),
        mavenBundles("com.novabase.omni.oney.modules"),
        testBundle("com.novabase.omni.oney.modules", "directoryservice-provider-activedirectory"));
  }

  @Override
  protected Option configurations() {
    return composite(super.configurations(),
        rsClientConfiguration("activedirectory", newConfiguration(ActiveDirectoryDAOConfig.CPID)));
  }

  @Test
  public void testBundle() {
    assertBundleActive("com.novabase.omni.oney.modules.directoryservice-provider-activedirectory");
  }

}
