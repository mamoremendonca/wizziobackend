package com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist;

import java.text.DecimalFormat;

import org.apache.commons.lang3.StringUtils;

public enum TargetType {

    //@formatter:off
    NUMBER, 
    FRIENDLY_NUMBER, 
    ENTITY_CODE, 
    EXPIRATION_DATE,
    VALUE,
    DEPARTMENT_CODE,
    CURRENT_GROUP, // The current on currentAction
    CREATION_USER_ID, // The user Id that created the instance
    REPOSITORY_ID,
    ASSOCIATE_FRIENDLY_NUMBER,
    COPY_FROM;
    //@formatter:on
    
    /**
     * This parameter represent the length of the targets VALUE and NUMBER. 
     * If the process have the VALUE = 500,00 this value must be persisted in targets 
     * with the value '00000500,00' in order to filter and sort it appropriately.
     * This because the targets are persisted with Strings. 
     */
    public static final int NUMBER_VALUES_LENGTH = 8; 
    
    
    /**
     * Format a Double value with the pattern "#.00".
     * Add {@link TargetType.NUMBER_VALUES_LENGTH} zeros before the comma. 
     * @param value 10,4
     * @return formatted value "00000010.40"
     */
    public static String formatValue(Double value) {

	//@formatter:off
	String formattedValue = new DecimalFormat("#.00").format(value);
	
	return StringUtils
		.leftPad(
        		formattedValue, 
        		TargetType.NUMBER_VALUES_LENGTH + 3, 
        		"0")
		.replace(",", ".");
	//@formatter:on
    }
    
    /**
     * Format a Long value.
     * LeftPad {@link TargetType.NUMBER_VALUES_LENGTH} zeros. 
     * @param value 10
     * @return formatted value "00000010"
     */
    public static String formatNumber(Long number) {

	//@formatter:off
	return StringUtils.leftPad(
		number.toString(), 
		TargetType.NUMBER_VALUES_LENGTH + 3, 
		"0");
	//@formatter:on
    }
    
    /**
     * Format a Long value.
     * LeftPad {@link TargetType.NUMBER_VALUES_LENGTH} zeros. 
     * @param value 10
     * @return formatted value "00000010"
     */
    public static String formatNumber(String number) {

	//@formatter:off
	return StringUtils.leftPad(
		number, 
		TargetType.NUMBER_VALUES_LENGTH + 3, 
		"0");
	//@formatter:on
    }
    
    /**
     * Format of the target COPY_FROM
     * 
     * @param id
     * @param version
     * @return String {id}-{version}
     */
    public static String formatCopyFrom(Long id, Long version) {
	//@formatter:off
	return new StringBuilder()
			.append(id.toString())
			.append("-")
			.append(version.toString())
			.toString();
	//@formatter:on
    }

}
