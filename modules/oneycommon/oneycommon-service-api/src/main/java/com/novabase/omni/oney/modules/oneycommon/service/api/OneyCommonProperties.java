package com.novabase.omni.oney.modules.oneycommon.service.api;

import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;

public abstract class OneyCommonProperties extends RSProperties {
	public static final String ONEYCOMMON000 = "ONEYCOMMON000";
}
