package com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist;

import com.novabase.omni.oney.modules.oneycommon.service.api.enums.ReferenceDataEnum;

public enum OrderType implements ReferenceDataEnum {

    //@formatter:off
    ASC("worklist.order.label.asc"), 
    DESC("worklist.order.label.desc");
    //@formatter:on
    private String description;

    private OrderType(String description) {
	this.description = description;
    }

    public String getDescription() {
	return description;
    }

}
