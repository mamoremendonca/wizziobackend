package com.novabase.omni.oney.modules.oneycommon.service.api.enums.repository;

import com.novabase.omni.oney.modules.oneycommon.service.api.enums.ReferenceDataEnum;

//TODO: move to purchaseOrder APP
public enum FilterRepositoryAttribute implements ReferenceDataEnum {

    //@formatter:off
    STATUS("status"),
    TYPE("type"),
    SUPPLIER("supplierCode"),
    INTERNAL_ORDER("internalOrderCode"),
    DEPARTMENT("departmentCode"),
    PROJECT("projectCode"),
    NUMBER("number"),
    PURCHASE_ORDER_NUMBER("number"),
    CREATION_DATE("creationDate"), 
    APPROVAL_DATE("approvalDate"),
    EXPIRATION_DATE("expirationDate"),
    USER_GROUP("workflow.currentUserGroup"),
    TOTAL("totalWithoutTax");
    //@formatter:on

    private String filterAttribute;

    private FilterRepositoryAttribute(String filterAttribute) {
	this.filterAttribute = filterAttribute;
    }

    public String getFilterAttribute() {
        return filterAttribute;
    }

    @Override
    public String getDescription() {
	return this.name();
    }
}
