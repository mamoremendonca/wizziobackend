package com.novabase.omni.oney.modules.oneycommon.service.api.exception;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;
import io.digitaljourney.platform.modules.ws.rs.api.exception.RSException;
import com.novabase.omni.oney.modules.oneycommon.service.api.OneyCommonProperties;

public class OneyCommonException extends RSException {
	private static final long serialVersionUID = -8645577031119256555L;

	protected OneyCommonException(PlatformCode statusCode, String errorCode, Context ctx, Object... args) {
		super(statusCode, errorCode, ctx, args);
	}

	protected OneyCommonException(Context ctx, Object... args) {
		this(PlatformCode.INTERNAL_SERVER_ERROR, OneyCommonProperties.ONEYCOMMON000, ctx, args);
	}

	protected OneyCommonException(String errorCode, Context ctx, Object... args) {
		this(PlatformCode.BUSINESS_ERROR, errorCode, ctx, args);
	}

	public static OneyCommonException of(Context ctx, String message) {
		return new OneyCommonException(ctx, message);
	}

	public static OneyCommonException of(Context ctx, Throwable cause) {
		return new OneyCommonException(ctx, cause);
	}
}
