package com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist;

import com.novabase.omni.oney.modules.oneycommon.service.api.enums.ReferenceDataEnum;

public enum FilterAttribute implements ReferenceDataEnum {

    //@formatter:off
    NUMBER("targets.NUMBER.value", "number", "worklist.filter.label.number"),
    JOURNEY_TYPE("processType", "journeyType", "worklist.filter.label.journeyType"),
    ENTITY_CODE("targets.ENTITY_CODE.value", "entityCode", "worklist.filter.label.entityCode"),
    VALUE("targets.VALUE.value", "value", "worklist.filter.label.value"),
    STATUS("status", "status", "worklist.filter.label.status"),
    CREATION_DATE("createdOn", "creationDate", "worklist.filter.label.creationDate"), 
    EXPIRATION_DATE("targets.EXPIRATION_DATE.value", "expirationDate", "worklist.filter.label.expirationDate"),
    DEPARTMENT_CODE("targets.DEPARTMENT_CODE.value", "departmentCode", "worklist.filter.label.departmentCode");
    //@formatter:on

    private String queryPath;
    private String filterAttribute;
    private String description;

    private FilterAttribute(String queryPath, String filterAttribute, String description) {
	this.queryPath = queryPath;
	this.filterAttribute = filterAttribute;
	this.description = description;
    }

    public String getQueryPath() {
	return queryPath;
    }

    public String getDescription() {
	return description;
    }

    public String getFilterAttribute() {
        return filterAttribute;
    }
}
