package com.novabase.omni.oney.modules.oneycommon.service.api.enums.purchaseorder;

import com.novabase.omni.oney.modules.oneycommon.service.api.enums.ReferenceDataEnum;

public enum PurchaseOrderWorkListStatus implements ReferenceDataEnum {

    //@formatter:off
    DRAFT("purchase_order.status.label.draft", "Draft"), 
    IN_APPROVAL("purchase_order.status.label.in_approval", "In Approval"),
//    APPROVED("purchase_order.status.label.approved", "Approved"),
    REJECTED("purchase_order.status.label.rejected", "Rejected");
//    CANCELED("purchase_order.status.label.canceled", "Cancel State");
    //@formatter:on

    private String description;
    private String journeyValue;

    private PurchaseOrderWorkListStatus(String description, String journeyValue) {
	this.description = description;
	this.journeyValue = journeyValue;
    }

    public String getDescription() {
	return description;
    }

    public String getJourneyValue() {
	return journeyValue;
    }
    
    @Override
    public String getCode() {
        return journeyValue;
    }

}
