package com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist;

import com.novabase.omni.oney.modules.oneycommon.service.api.enums.ReferenceDataEnum;

public enum JourneyType implements ReferenceDataEnum {
    PURCHASE_ORDER("worklist.filter.label.journey_type.purchase_order"),
    FINANCIAL_DOCUMENT("worklist.filter.label.journey_type.financial_document");

    private String description;

    private JourneyType(String description) {
	this.description = description;
    }

    public String getDescription() {
	return description;
    }

}
