package com.novabase.omni.oney.modules.oneycommon.service.api.dto.worklist;

import java.util.Date;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.modules.oneycommon.service.api.enums.worklist.JourneyType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("SearchFilterDTO")
public class SearchCriteriaWorklistDTO extends DTO {

    @ApiModelProperty
    public String number; // Numero do processo ATENÇÃO NÃO É O INSTANCEID

    @ApiModelProperty
    public JourneyType journeyType; // Tipo de processo PURCHASE_ORDER ou FINANCIAL_DOCUMENT

    @ApiModelProperty
    public String entityCode; // Codigo do fornecedor TODO: validar se mantem o filtro

    @ApiModelProperty
    public String status;

    @ApiModelProperty
    public String userGroup;

    @ApiModelProperty
    public String departmentCode; // Combo com todos os departamentos que o user logado tem acesso

    @ApiModelProperty
    public Double minValue; // valor minimo Target "value"

    @ApiModelProperty
    public Double maxValue; // valor maximo Target "value"

    @ApiModelProperty
    public Date creationDate; // data de criação da do processo

    @ApiModelProperty
    public Date expirationDate; // data de expiração do processo
}
