package com.novabase.omni.oney.modules.oneycommon.service.api.enums;

public interface ReferenceDataEnum {
    
    public String name();
    
    public String getDescription();
    
    public default String getCode() {
	return name();
    }

}
