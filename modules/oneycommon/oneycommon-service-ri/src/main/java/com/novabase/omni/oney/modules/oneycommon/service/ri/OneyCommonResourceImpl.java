package com.novabase.omni.oney.modules.oneycommon.service.ri;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;
import io.digitaljourney.platform.modules.ws.rs.api.resource.AbstractResource;
import com.novabase.omni.oney.modules.oneycommon.service.api.OneyCommonResource;

// @formatter:off
@Component(
	configurationPid = OneyCommonResourceConfig.CPID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	reference = {
		@Reference(
			name = OneyCommonResourceProperties.REF_CONTEXT,
			service = OneyCommonContext.class,
			cardinality = ReferenceCardinality.MANDATORY)
})
@Designate(ocd = OneyCommonResourceConfig.class)
@RSProvider(OneyCommonResourceProperties.ADDRESS)
// @formatter:on
public class OneyCommonResourceImpl extends AbstractResource<OneyCommonContext, OneyCommonResourceConfig> implements OneyCommonResource {

	@Activate
	public void activate(ComponentContext ctx, OneyCommonResourceConfig config) {
		prepare(ctx, config);
	}
	
	@Modified
	public void modified(OneyCommonResourceConfig config) {
		prepare(config);
	}
	
	@Override
	@RequiresPermissions(OneyCommonResourceProperties.PERMISSION_READ)
	public String index() {
		return "OneyCommon is running.";
	}


}
