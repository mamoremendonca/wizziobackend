package com.novabase.omni.oney.modules.oneycommon.service.ri;

import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/oneycommon", icon = @Icon(resource = "OSGI-INF/icon/resource.png", size = 32))
public @interface OneyCommonResourceConfig { 	
	public static final String CPID = "oney.base.modules.oneycommon.service.ri.oneycommon";
}
