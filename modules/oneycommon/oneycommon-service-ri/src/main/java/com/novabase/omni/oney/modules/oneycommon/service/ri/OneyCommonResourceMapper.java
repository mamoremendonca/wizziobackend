package com.novabase.omni.oney.modules.oneycommon.service.ri;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OneyCommonResourceMapper {
	public static final OneyCommonResourceMapper INSTANCE = Mappers.getMapper(OneyCommonResourceMapper.class);
}
