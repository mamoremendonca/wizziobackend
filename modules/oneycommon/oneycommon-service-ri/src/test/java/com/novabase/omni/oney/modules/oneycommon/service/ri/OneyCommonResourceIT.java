package com.novabase.omni.oney.modules.oneycommon.service.ri;

import static io.digitaljourney.platform.modules.paxexam.BundleOptions.mavenBundles;
import static io.digitaljourney.platform.modules.paxexam.BundleOptions.rsProviderWithRSClient;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.newConfiguration;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.rsProviderWithRSClientConfiguration;
import static org.assertj.core.api.Assertions.assertThat;
import static org.ops4j.pax.exam.CoreOptions.composite;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;

import com.novabase.omni.oney.modules.oneycommon.service.api.OneyCommonResource;

import io.digitaljourney.platform.modules.paxexam.base.BaseTestSupport;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class OneyCommonResourceIT extends BaseTestSupport {
	@Inject 
	private OneyCommonResource oneycommonResource;

	@Override
	protected Option bundles() {
		return composite(super.bundles(), rsProviderWithRSClient(),
				mavenBundles("com.novabase.omni.oney.modules", 
						"oneycommon-entity", "oneycommon-data-ri", "oneycommon-service-api"),
				testBundle("com.novabase.omni.oney.modules", "oneycommon-service-ri"));
	}

	@Override
	protected Option configurations() {
		return composite(super.configurations(),
                rsProviderWithRSClientConfiguration("oneycommon", newConfiguration(OneyCommonResourceConfig.CPID)));
	}

	@Test
	public void testBundle() {
		assertBundleActive("com.novabase.omni.oney.modules.oneycommon-service-ri");
	}

	@Test
	public void testDao() {
		assertThat(oneycommonResource).isNotNull();
	}

}
