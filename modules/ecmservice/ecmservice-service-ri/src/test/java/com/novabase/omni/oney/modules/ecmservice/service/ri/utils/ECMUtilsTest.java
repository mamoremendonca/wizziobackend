package com.novabase.omni.oney.modules.ecmservice.service.ri.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ECMUtilsTest {
	
    @Before
    public void setUp() {
	
    }
    
    @Test
    public void normalizeStringTest() {
	
	final String accents = "È,É,Ê,Ë,Û,Ù,Ï,Î,À,Â,Ô,è,é,ê,ë,û,ù,ï,î,à,â,ô,Ç,ç,Ã,ã,Õ,õ";
	final String expected	= "EEEEUUIIAAOeeeeuuiiaaoCcAaOo";
	final String accents2	= "çÇáéíóúýÁÉÍÓÚÝàèìòùÀÈÌÒÙãõñäëïöüÿÄËÏÖÜÃÕÑâêîôûÂÊÎÔÛ";
	final String expected2	= "cCaeiouyAEIOUYaeiouAEIOUaonaeiouyAEIOUAONaeiouAEIOU";
	final String accents3	= "Gisele Bündchen da Conceição e Silva foi batizada assim em homenagem à sua conterrânea de Horizontina, RS.";
	final String expected3	= "GiseleBundchendaConceicaoeSilvafoibatizadaassimemhomenagemasuaconterraneadeHorizontinaRS.";
	final String accents4	= "/Users/rponte/arquivos-portalfcm/Eletron/Atualização_Diária-1.23.40.exe";
	final String expected4	= "UsersrpontearquivosportalfcmEletronAtualizacaoDiaria1.23.40.exe";
	
	Assert.assertEquals(expected, ECMUtils.normalizeString(expected));
	Assert.assertEquals(expected,  ECMUtils.normalizeString(accents));
	Assert.assertEquals(expected2, ECMUtils.normalizeString(accents2));
	Assert.assertEquals(expected3, ECMUtils.normalizeString(accents3));
	Assert.assertEquals(expected4, ECMUtils.normalizeString(accents4));
	
    }
    
}
