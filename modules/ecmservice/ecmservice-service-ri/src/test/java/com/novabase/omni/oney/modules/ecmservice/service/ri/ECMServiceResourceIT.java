package com.novabase.omni.oney.modules.ecmservice.service.ri;

import static io.digitaljourney.platform.modules.paxexam.BundleOptions.mavenBundles;
import static io.digitaljourney.platform.modules.paxexam.BundleOptions.rsProviderWithRSClient;
import static org.assertj.core.api.Assertions.assertThat;
import static org.ops4j.pax.exam.CoreOptions.composite;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;

import com.novabase.omni.oney.modules.ecmservice.service.api.ECMServiceResource;

import io.digitaljourney.platform.modules.paxexam.base.BaseTestSupport;


@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class ECMServiceResourceIT extends BaseTestSupport {
	@Inject 
	private ECMServiceResource ecmserviceResource;

	@Override
	protected Option bundles() {
		return composite(super.bundles(), rsProviderWithRSClient(),
				mavenBundles("com.novabase.omni.oney.modules", 
						"ecmservice-entity", "ecmservice-data-ri", "ecmservice-service-api"),
				testBundle("com.novabase.omni.oney.modules", "ecmservice-service-ri"));
	}

//	@Override
//	protected Option configurations() {
//		return composite(super.configurations(),
//                rsProviderWithRSClientConfiguration("ecmservice", newConfiguration(ECMServiceResourceConfig.CPID)),
//				newConfiguration(ArtistDAOConfig.CPID).asOption());
//	}

	@Test
	public void testBundle() {
		assertBundleActive("com.novabase.omni.oney.modules.ecmservice-service-ri");
	}

	@Test
	public void testDao() {
		assertThat(ecmserviceResource).isNotNull();
	}

}
