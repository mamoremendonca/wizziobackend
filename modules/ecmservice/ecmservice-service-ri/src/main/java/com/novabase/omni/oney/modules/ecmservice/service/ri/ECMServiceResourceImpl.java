package com.novabase.omni.oney.modules.ecmservice.service.ri;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.modules.ecmservice.data.api.smartdocs.SmartDocsDAO;
import com.novabase.omni.oney.modules.ecmservice.data.api.smartdocs.exceptions.SmartDocsGetDocumentUrlException;
import com.novabase.omni.oney.modules.ecmservice.service.api.ECMServiceResource;
import com.novabase.omni.oney.modules.ecmservice.service.api.exception.ECMServiceException;
import com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.dto.FinancialDocumentECMMetadataMsDTO;
import com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.dto.PurchaseOrderECMMetadataMsDTO;
import com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.enums.DocumentClassTypeMsEnum;
import com.novabase.omni.oney.modules.ecmservice.service.ri.mapper.ECMServiceResourceMapper;
import com.novabase.omni.oney.modules.ecmservice.service.ri.utils.ECMUtils;

import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;
import io.digitaljourney.platform.modules.ws.rs.api.resource.AbstractResource;

// @formatter:off
@Component(
	configurationPid = ECMServiceResourceConfig.CPID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	reference = {
		@Reference(
			name = ECMServiceResourceProperties.REF_CONTEXT,
			service = ECMServiceContext.class,
			cardinality = ReferenceCardinality.MANDATORY)
})
@Designate(ocd = ECMServiceResourceConfig.class)
@RSProvider(ECMServiceResourceProperties.ADDRESS)
// @formatter:on
public class ECMServiceResourceImpl extends AbstractResource<ECMServiceContext, ECMServiceResourceConfig> implements ECMServiceResource {

    @Reference
    private volatile SmartDocsDAO smartDocsDAO;

    @Activate
    public void activate(ComponentContext ctx, ECMServiceResourceConfig config) {
	prepare(ctx, config);
    }

    @Modified
    public void modified(ECMServiceResourceConfig config) {
	prepare(config);
    }

    @Override
    public String ping() {
	return "ECM Service is Running...";
    }

    @Override
    @RequiresPermissions(ECMServiceResourceProperties.PERMISSION_READ)
    public InputStream getDocument(final String documentId) throws ECMServiceException {

	try {
	    return this.smartDocsDAO.getDocument(documentId);
	} catch (final SmartDocsGetDocumentUrlException exception) {
	    throw this.getCtx().exception(exception);
	}

    }

    // TODO: refatorar o metodo para receber objetos definidos e não mais o
    // Map<String,String>
    @Override
    @RequiresPermissions(ECMServiceResourceProperties.PERMISSION_CREATE)
    public String createDocument(final String processId, final DocumentClassTypeMsEnum documentClass, final String documentName, PurchaseOrderECMMetadataMsDTO documentMetadata,
	    final Attachment documentStream) throws ECMServiceException {

	try {

	    final String documentUrl = this.createUrl(processId, documentName, documentStream);
	    documentMetadata.setDocumentUrl(documentUrl);

	    this.getCtx().logInfo("[ECMService] going to submit Purchase Order related document to SmartDocs: " + documentClass);

	    return this.smartDocsDAO.createDocument(documentClass.getOneyEcmValue(), ECMServiceResourceMapper.INSTANCE.toPurchaseOrderECMMetadataDTO(documentMetadata));

	} catch (final Exception exception) {

	    this.getCtx().logError("Error trying to upload Purchase Order related document: " + exception.toString());
	    throw this.getCtx().exception(exception);

	}

    }

    // TODO: refatorar o metodo para receber objetos definidos e não mais o
    // Map<String,String>
    @Override
    @RequiresPermissions(ECMServiceResourceProperties.PERMISSION_CREATE)
    public String createDocument(final String processId, final DocumentClassTypeMsEnum documentClass, final String documentName, FinancialDocumentECMMetadataMsDTO documentMetadata,
	    final Attachment documentStream) throws ECMServiceException {

	try {

	    final String documentUrl = this.createUrl(processId, documentName, documentStream);
	    documentMetadata.setDocumentUrl(documentUrl);

	    this.getCtx().logInfo("[ECMService] going to submit Financial Document related document to SmartDocs: " + documentClass);
	    
	    if(DocumentClassTypeMsEnum.FINANCIAL_DOCUMENT_ATTACHMENT.equals(documentClass)) {
		documentMetadata.setDocumentNumber(null);
	    }

	    return this.smartDocsDAO.createDocument(documentClass.getOneyEcmValue(), ECMServiceResourceMapper.INSTANCE.toFinancialDocumentECMMetadataDTO(documentMetadata));

	} catch (final Exception exception) {

	    this.getCtx().logError("Error trying to upload Financial Document related document: " + exception.toString());
	    throw this.getCtx().exception(exception);

	}

    }

    private String createUrl(final String processId, String documentName, final Attachment document) throws IOException {

	// CREATE FILE
	final File documentFolder = new File(String.format("%s%s%s", this.getConfig().smartdocsShare(), File.separator, ECMUtils.normalizeString(processId)));
	if (!documentFolder.exists()) {
	    documentFolder.mkdir();
	}

	// normalizing documentName, removing accents and other special characters.
	documentName = ECMUtils.normalizeString(documentName);

	final DateTimeFormatter formatter = DateTimeFormatter.ofPattern(this.getConfig().smartdocsShareFileNameDateFormat());
	final File documentFile = new File(String.format("%s%s%s-%s", documentFolder.getAbsolutePath(), File.separator, LocalDateTime.now().format(formatter), documentName));
	documentFile.createNewFile();

	try (final InputStream documentStream = document.getDataHandler().getInputStream(); final OutputStream outStream = new FileOutputStream(documentFile)) {

	    byte[] buffer = new byte[documentStream.available()];
	    documentStream.read(buffer);

	    outStream.write(buffer);

	}

	info("File created on shared folder with absolutePath: " + documentFile.getAbsolutePath());
	String externalPath = documentFile.getAbsolutePath().replace(this.getConfig().smartdocsShare(), "/srv/wizzio");

	info("Change to externalPath on host machine: " + externalPath);

	return externalPath;

    }

}
