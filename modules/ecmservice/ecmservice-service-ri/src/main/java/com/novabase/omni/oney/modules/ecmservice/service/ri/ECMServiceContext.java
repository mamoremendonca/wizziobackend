package com.novabase.omni.oney.modules.ecmservice.service.ri;

import java.util.Date;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

import io.digitaljourney.platform.modules.async.api.PlatformAsyncManager;
import io.digitaljourney.platform.modules.cache.api.PlatformCacheManager;
import io.digitaljourney.platform.modules.commons.annotation.DocumentationResource;
import io.digitaljourney.platform.modules.invocation.api.PlatformInvocationManager;
import io.digitaljourney.platform.modules.invocation.api.model.EventLog;
import io.digitaljourney.platform.modules.invocation.api.model.type.StatusType;
import io.digitaljourney.platform.modules.security.api.PlatformSecurityManager;
import io.digitaljourney.platform.modules.ws.rs.api.context.AbstractRSEndpointContext;
import com.novabase.omni.oney.modules.ecmservice.service.api.exception.ECMServiceException;

// @formatter:off
@Component(
	service = { Object.class, ECMServiceContext.class },
	reference = {
		@Reference(
			name = ECMServiceResourceProperties.REF_PLATFORM_SECURITY_MANAGER,
			service = PlatformSecurityManager.class,
			cardinality = ReferenceCardinality.MANDATORY),
        @Reference(
            name = ECMServiceResourceProperties.REF_PLATFORM_INVOCATION_MANAGER,
            service = PlatformInvocationManager.class,
            cardinality = ReferenceCardinality.MANDATORY),
		@Reference(
			name = ECMServiceResourceProperties.REF_ASYNC_MANAGER,
			service = PlatformAsyncManager.class,
			cardinality = ReferenceCardinality.MANDATORY),
		@Reference(
			name = ECMServiceResourceProperties.REF_CACHE_MANAGER,
			service = PlatformCacheManager.class,
			cardinality = ReferenceCardinality.MANDATORY)
	})
@DocumentationResource(ECMServiceResourceProperties.DOCS_ADDRESS)
// @formatter:on
public final class ECMServiceContext extends AbstractRSEndpointContext {
    @Activate
    public void activate(ComponentContext ctx) {
	prepare(ctx);
    }

    public ECMServiceException exception(String message) {
	return ECMServiceException.of(this, message);
    }

    public ECMServiceException exception(Throwable cause) {
	return ECMServiceException.of(this, cause);
    }

    public void logError(String message) {
	EventLog event = new EventLog();
	event.setEvent_message(message);
	event.setStart_event_time(new Date());
	getPlatformInvocationManager().logNewEvent(event, null, StatusType.ERROR);
	getLogger().error(message);
    }

    public void logInfo(String message) {
	EventLog event = new EventLog();
	event.setEvent_message(message);
	event.setStart_event_time(new Date());
	getPlatformInvocationManager().logNewEvent(event, null, StatusType.SUCCESS);
	getLogger().info(message);
    }
}
