package com.novabase.omni.oney.modules.ecmservice.service.ri.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.modules.ecmservice.data.api.dto.FinancialDocumentECMMetadataDTO;
import com.novabase.omni.oney.modules.ecmservice.data.api.dto.PurchaseOrderECMMetadataDTO;
import com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.dto.FinancialDocumentECMMetadataMsDTO;
import com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.dto.PurchaseOrderECMMetadataMsDTO;

@Mapper
public interface ECMServiceResourceMapper {

    public static final ECMServiceResourceMapper INSTANCE = Mappers.getMapper(ECMServiceResourceMapper.class);
    
    public PurchaseOrderECMMetadataDTO toPurchaseOrderECMMetadataDTO(final PurchaseOrderECMMetadataMsDTO source);
    
    public FinancialDocumentECMMetadataDTO toFinancialDocumentECMMetadataDTO(final FinancialDocumentECMMetadataMsDTO source);

}
