package com.novabase.omni.oney.modules.ecmservice.service.ri.utils;

import java.text.Normalizer;

import org.apache.commons.lang.StringUtils;

public class ECMUtils {

    private ECMUtils() {
	
    }
    
    public static String normalizeString(final String input) {
	
	String output = Normalizer.normalize(input, Normalizer.Form.NFD);
	output = output.replaceAll("[^\\p{ASCII}]", StringUtils.EMPTY);
	output = output.replaceAll("[^a-zA-Z0-9\\.]", StringUtils.EMPTY);
	
	return output;
	
    }
    
}
