package com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.dto;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Document", description = "The Document")
public class DocumentMsDTO extends DTO {

    @ApiModelProperty
    private String documentDate;
    @ApiModelProperty
    private String financialDocType;
    @ApiModelProperty
    private String sapDocNumber;
    @ApiModelProperty
    private String issueDate;
    @ApiModelProperty
    private String via;
    @ApiModelProperty
    private String dueDate;
    @ApiModelProperty
    private String poOrderNumber;
    @ApiModelProperty
    private String valueOutVat;
    @ApiModelProperty
    private String vat;
    @ApiModelProperty
    private String totalValue;
    @ApiModelProperty
    private String discountRetention;
    @ApiModelProperty
    private String customerVatNumber;
    @ApiModelProperty
    private String customerNumber;
    @ApiModelProperty
    private String customerName;
    @ApiModelProperty
    private String url;

    public DocumentMsDTO() {
	super();
    }

    public String getDocumentDate() {
	return documentDate;
    }

    public void setDocumentDate(String documentDate) {
	this.documentDate = documentDate;
    }

    public String getFinancialDocType() {
	return financialDocType;
    }

    public void setFinancialDocType(String financialDocType) {
	this.financialDocType = financialDocType;
    }

    public String getSapDocNumber() {
	return sapDocNumber;
    }

    public void setSapDocNumber(String sapDocNumber) {
	this.sapDocNumber = sapDocNumber;
    }

    public String getIssueDate() {
	return issueDate;
    }

    public void setIssueDate(String issueDate) {
	this.issueDate = issueDate;
    }

    public String getVia() {
	return via;
    }

    public void setVia(String via) {
	this.via = via;
    }

    public String getDueDate() {
	return dueDate;
    }

    public void setDueDate(String dueDate) {
	this.dueDate = dueDate;
    }

    public String getPoOrderNumber() {
	return poOrderNumber;
    }

    public void setPoOrderNumber(String poOrderNumber) {
	this.poOrderNumber = poOrderNumber;
    }

    public String getValueOutVat() {
	return valueOutVat;
    }

    public void setValueOutVat(String valueOutVat) {
	this.valueOutVat = valueOutVat;
    }

    public String getVat() {
	return vat;
    }

    public void setVat(String vat) {
	this.vat = vat;
    }

    public String getTotalValue() {
	return totalValue;
    }

    public void setTotalValue(String totalValue) {
	this.totalValue = totalValue;
    }

    public String getDiscountRetention() {
	return discountRetention;
    }

    public void setDiscountRetention(String discountRetention) {
	this.discountRetention = discountRetention;
    }

    public String getCustomerVatNumber() {
	return customerVatNumber;
    }

    public void setCustomerVatNumber(String customerVatNumber) {
	this.customerVatNumber = customerVatNumber;
    }

    public String getCustomerNumber() {
	return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
	this.customerNumber = customerNumber;
    }

    public String getCustomerName() {
	return customerName;
    }

    public void setCustomerName(String customerName) {
	this.customerName = customerName;
    }

    public String getUrl() {
	return url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

}
