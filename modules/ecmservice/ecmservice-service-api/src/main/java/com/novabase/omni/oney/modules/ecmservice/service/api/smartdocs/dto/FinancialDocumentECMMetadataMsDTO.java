package com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;

@JsonInclude(Include.NON_NULL)
@ApiModel(value = "FinancialDocumentECMMetadata", description = "The Financial Document ECM Document Metadata")
public class FinancialDocumentECMMetadataMsDTO {

    @JsonProperty("object_name")
    private String objectName;
    
    @JsonProperty("reference")
    private String reference;
    
    @JsonProperty("registration_date")
    private String registrationDate;
    
    @JsonProperty("status__key")
    private String statusKey;
    
    @JsonProperty("status__value")
    private String statusValue; 
    
    @JsonProperty("financial_doc_type__key")
    private String financialDocumentTypeKey;
    
    @JsonProperty("financial_doc_type__value")
    private String financialDocumentTypeValue;
    
    @JsonProperty("document_nr")
    private String documentNumber;

    @JsonProperty("document_date")
    private String documentDate;
    
    @JsonProperty("due_date")
    private String dueDate;
    
    @JsonProperty("po_order_nr")
    private String poOrderNumber;
    
    @JsonProperty("value_out_vat")
    private String valueOutVat;
    
    @JsonProperty("vat")
    private String vat;
    
    @JsonProperty("total_value")
    private String totalValue;
    
    @JsonProperty("discount_retention")
    private String discountRetention;
    
    @JsonProperty("department__key")
    private String departmentKey;
    
    @JsonProperty("department__value")
    private String departmentValue;
    
    @JsonProperty("project__key")
    private String projectKey;
    
    @JsonProperty("project__value")
    private String projectValue;
    
    @JsonProperty("supplier_vat_number")
    private String supplierVatNumber;
    
    @JsonProperty("supplier_name")
    private String supplierName;
    
    @JsonProperty("supplier_id")
    private String supplierId;
    
    @JsonProperty("sap_supplier_id")
    private String sapSupplierId;
    
    @JsonProperty("relational_doc")
    private String relationalDoc;
    
    @JsonProperty("url")
    private String documentUrl;
    
    public FinancialDocumentECMMetadataMsDTO() {
	super();
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getRelationalDoc() {
        return relationalDoc;
    }

    public void setRelationalDoc(String relationalDoc) {
        this.relationalDoc = relationalDoc;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getStatusKey() {
        return statusKey;
    }

    public void setStatusKey(String statusKey) {
        this.statusKey = statusKey;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

    public String getFinancialDocumentTypeKey() {
        return financialDocumentTypeKey;
    }

    public void setFinancialDocumentTypeKey(String financialDocumentTypeKey) {
        this.financialDocumentTypeKey = financialDocumentTypeKey;
    }

    public String getFinancialDocumentTypeValue() {
        return financialDocumentTypeValue;
    }

    public void setFinancialDocumentTypeValue(String financialDocumentTypeValue) {
        this.financialDocumentTypeValue = financialDocumentTypeValue;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getPoOrderNumber() {
        return poOrderNumber;
    }

    public void setPoOrderNumber(String poOrderNumber) {
        this.poOrderNumber = poOrderNumber;
    }

    public String getValueOutVat() {
        return valueOutVat;
    }

    public void setValueOutVat(String valueOutVat) {
        this.valueOutVat = valueOutVat;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(String totalValue) {
        this.totalValue = totalValue;
    }

    public String getDiscountRetention() {
        return discountRetention;
    }

    public void setDiscountRetention(String discountRetention) {
        this.discountRetention = discountRetention;
    }

    public String getDepartmentKey() {
        return departmentKey;
    }

    public void setDepartmentKey(String departmentKey) {
        this.departmentKey = departmentKey;
    }

    public String getDepartmentValue() {
        return departmentValue;
    }

    public void setDepartmentValue(String departmentValue) {
        this.departmentValue = departmentValue;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public String getProjectValue() {
        return projectValue;
    }

    public void setProjectValue(String projectValue) {
        this.projectValue = projectValue;
    }

    public String getSupplierVatNumber() {
        return supplierVatNumber;
    }

    public void setSupplierVatNumber(String supplierVatNumber) {
        this.supplierVatNumber = supplierVatNumber;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getSapSupplierId() {
        return sapSupplierId;
    }

    public void setSapSupplierId(String sapSupplierId) {
        this.sapSupplierId = sapSupplierId;
    }

    public String getDocumentUrl() {
        return documentUrl;
    }

    public void setDocumentUrl(String documentUrl) {
        this.documentUrl = documentUrl;
    }
    
}
