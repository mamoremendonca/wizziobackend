package com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.enums;

public enum DocumentClassTypeMsEnum {

    //@formatter:off
    PURCHASE_ORDER("oney_purchase_order"), 
    FINANCIAL_DOCUMENT("oney_financial_doc_supplier"),
    FINANCIAL_DOCUMENT_ATTACHMENT("oney_fin_sup_attachment");
    //@formatter:on

    private String oneyEcmValue;

    private DocumentClassTypeMsEnum(String oneyEcmValue) {
	this.oneyEcmValue = oneyEcmValue;
    }

    public String getOneyEcmValue() {
	return oneyEcmValue;
    }

}
