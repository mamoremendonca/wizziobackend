package com.novabase.omni.oney.modules.ecmservice.service.api;

import java.io.File;
import java.io.InputStream;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.hibernate.validator.constraints.NotBlank;
import org.osgi.annotation.versioning.ProviderType;

import com.novabase.omni.oney.modules.ecmservice.service.api.exception.ECMServiceException;
import com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.dto.FinancialDocumentECMMetadataMsDTO;
import com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.dto.PurchaseOrderECMMetadataMsDTO;
import com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.enums.DocumentClassTypeMsEnum;

import io.digitaljourney.platform.modules.commons.type.HttpStatusCode;
import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.ApiKeyAuthDefinition.ApiKeyLocation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.BasicAuthDefinition;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;

@ProviderType
@Path("")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

@SwaggerDefinition(securityDefinition = @SecurityDefinition(basicAuthDefinitions = { @BasicAuthDefinition(key = RSProperties.SWAGGER_BASIC_AUTH) }, apiKeyAuthDefinitions = {
	@ApiKeyAuthDefinition(key = RSProperties.SWAGGER_BEARER_AUTH, name = RSProperties.HTTP_HEADER_API_KEY, in = ApiKeyLocation.HEADER) }), schemes = { SwaggerDefinition.Scheme.HTTP,
		SwaggerDefinition.Scheme.HTTPS, SwaggerDefinition.Scheme.DEFAULT })
@Api(value = "ECM Service", authorizations = { @Authorization(value = RSProperties.SWAGGER_BASIC_AUTH), @Authorization(value = RSProperties.SWAGGER_BEARER_AUTH) })
@ApiResponses(value = { @ApiResponse(code = HttpStatusCode.UNAUTHORIZED_CODE, message = RSProperties.SWAGGER_UNAUTHORIZED_MESSAGE),
	@ApiResponse(code = HttpStatusCode.FORBIDDEN_CODE, message = RSProperties.SWAGGER_FORBIDDEN_MESSAGE) })
public interface ECMServiceResource {

    @GET
    @ApiOperation(value = "Ping Method", response = String.class)
    @Path("/")
    public String ping();

    @GET
    @ApiOperation(value = "Get the Url of the Document", response = Map.class)
    @Path("/document/{documentId}/url")
    public InputStream getDocument(@ApiParam(value = "The Id for the Document", required = true) @PathParam("documentId") @NotBlank final String documentId) throws ECMServiceException;

    //TODO: tirar id do processo do path, receber como um atributo do formdata, o id pode conter caracteres especiais
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(value = "Register the File to ECM Service", consumes = MediaType.MULTIPART_FORM_DATA, response = Map.class)
    @ApiImplicitParams({ @ApiImplicitParam(name = "documentClass", value = "The Document Class Type Enum", required = true, paramType = "form", dataTypeClass = Enum.class),
	    @ApiImplicitParam(name = "documentName", value = "The Document Name", required = true, paramType = "form", dataTypeClass = String.class),
	    @ApiImplicitParam(name = "documentMetadata", value = "The Document Metadata", required = true, paramType = "form", dataTypeClass = Map.class),
	    @ApiImplicitParam(name = "documentStream", value = "The Document Input Stream", required = true, paramType = "form", dataTypeClass = File.class, format = "binary") })
    @Path("/{processId}/purchaseorder/document/create/")
    public String createDocument(@ApiParam(value = "The Process Id for the Document", required = true) @PathParam("processId") @NotBlank final String processId,
	    @ApiParam(value = "The Document Class for the Document", required = true) @Multipart(value = "documentClass", required = true) @Valid @NotNull final DocumentClassTypeMsEnum documentClass,
	    @ApiParam(value = "The Name of the Document", required = true) @Multipart(value = "documentName", required = true) @Valid @NotBlank final String documentName,
	    @ApiParam(value = "The Metadata for the Document", required = true) @Valid @NotNull PurchaseOrderECMMetadataMsDTO documentMetadata,
	    @ApiParam(value = "The Content for the Document", required = true) @Multipart(value = "documentStream", required = true) @Valid @NotNull final Attachment document)
	    throws ECMServiceException;
    
    //TODO: tirar id do processo do path, receber como um atributo do formdata, o id pode conter caracteres especiais
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @ApiOperation(value = "Register the File to ECM Service", consumes = MediaType.MULTIPART_FORM_DATA, response = Map.class)
    @ApiImplicitParams({ @ApiImplicitParam(name = "documentClass", value = "The Document Class Type Enum", required = true, paramType = "form", dataTypeClass = Enum.class),
	    @ApiImplicitParam(name = "documentName", value = "The Document Name", required = true, paramType = "form", dataTypeClass = String.class),
	    @ApiImplicitParam(name = "documentMetadata", value = "The Document Metadata", required = true, paramType = "form", dataTypeClass = Map.class),
	    @ApiImplicitParam(name = "documentStream", value = "The Document Input Stream", required = true, paramType = "form", dataTypeClass = File.class, format = "binary") })
    @Path("/{processId}/financialdocument/document/create/")
    public String createDocument(@ApiParam(value = "The Process Id for the Document", required = true) @PathParam("processId") @NotBlank final String processId,
	    @ApiParam(value = "The Document Class for the Document", required = true) @Multipart(value = "documentClass", required = true) @Valid @NotNull final DocumentClassTypeMsEnum documentClass,
	    @ApiParam(value = "The Name of the Document", required = true) @Multipart(value = "documentName", required = true) @Valid @NotBlank final String documentName,
	    @ApiParam(value = "The Metadata for the Document", required = true) @Valid @NotNull FinancialDocumentECMMetadataMsDTO documentMetadata,
	    @ApiParam(value = "The Content for the Document", required = true) @Multipart(value = "documentStream", required = true) @Valid @NotNull final Attachment document)
	    throws ECMServiceException;

}
