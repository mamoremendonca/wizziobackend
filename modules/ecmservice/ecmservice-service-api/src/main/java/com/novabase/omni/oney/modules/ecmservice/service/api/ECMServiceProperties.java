package com.novabase.omni.oney.modules.ecmservice.service.api;

import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;

public class ECMServiceProperties extends RSProperties {

    public static final String MUSIC000 = "MUSIC000";
    public static final String MUSIC001 = "MUSIC001";

}
