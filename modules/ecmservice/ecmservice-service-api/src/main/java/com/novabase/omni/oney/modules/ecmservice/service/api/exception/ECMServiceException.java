package com.novabase.omni.oney.modules.ecmservice.service.api.exception;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;
import io.digitaljourney.platform.modules.ws.rs.api.exception.RSException;

public class ECMServiceException extends RSException {

    private static final long serialVersionUID = 2547596040767670849L;

    public static final String ERROR_CODE = "ECMSERVICE000";

    protected ECMServiceException(PlatformCode statusCode, String errorCode, Context ctx, Object... args) {
	super(statusCode, errorCode, ctx, args);
    }

    protected ECMServiceException(Context ctx, Object... args) {
	this(PlatformCode.INTERNAL_SERVER_ERROR, ERROR_CODE, ctx, args);
    }

    protected ECMServiceException(String errorCode, Context ctx, Object... args) {
	this(PlatformCode.BUSINESS_ERROR, errorCode, ctx, args);
    }

    public static ECMServiceException of(Context ctx, String message) {
	return new ECMServiceException(ctx, message);
    }

    public static ECMServiceException of(Context ctx, Throwable cause) {
	return new ECMServiceException(ctx, cause);
    }

}
