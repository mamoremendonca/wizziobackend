package com.novabase.omni.oney.modules.ecmservice.data.api.smartdocs.exceptions;

public class SmartDocsCreateDocumentException extends SmartDocsActionException {
    /**
     * 
     */
    private static final long serialVersionUID = 5121354673689L;

    public SmartDocsCreateDocumentException() {
	super();
    }

    public SmartDocsCreateDocumentException(final String msg) {
	super(msg);
    }

    public SmartDocsCreateDocumentException(final Exception e) {
	super(e);
    }

    public SmartDocsCreateDocumentException(final Throwable e) {
	super(e);
    }

}
