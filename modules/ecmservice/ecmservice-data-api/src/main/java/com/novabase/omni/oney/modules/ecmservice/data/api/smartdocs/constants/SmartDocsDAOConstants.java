package com.novabase.omni.oney.modules.ecmservice.data.api.smartdocs.constants;

public final class SmartDocsDAOConstants {

    public final static String ERROR_CODE_PROPERTY = "Code";
    public final static String ERROR_DESCRIPTION_PROPERTY = "Description";

    private SmartDocsDAOConstants() {
	super();
    }

}
