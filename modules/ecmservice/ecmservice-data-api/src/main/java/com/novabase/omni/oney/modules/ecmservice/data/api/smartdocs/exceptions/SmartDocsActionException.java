package com.novabase.omni.oney.modules.ecmservice.data.api.smartdocs.exceptions;

import com.novabase.omni.oney.modules.ecmservice.data.api.exceptions.ECMServiceDataException;

public class SmartDocsActionException extends ECMServiceDataException {

    /**
     * 
     */
    private static final long serialVersionUID = 518923625245452489L;

    public SmartDocsActionException() {
	super();
    }

    public SmartDocsActionException(final String msg) {
	super(msg);
    }

    public SmartDocsActionException(final Exception e) {
	super(e);
    }

    public SmartDocsActionException(final Throwable e) {
	super(e);
    }

}
