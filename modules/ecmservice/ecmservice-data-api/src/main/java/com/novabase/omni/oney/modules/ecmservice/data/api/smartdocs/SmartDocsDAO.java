package com.novabase.omni.oney.modules.ecmservice.data.api.smartdocs;

import java.io.InputStream;
import java.util.Map;

import org.osgi.annotation.versioning.ProviderType;

import com.novabase.omni.oney.modules.ecmservice.data.api.dto.FinancialDocumentECMMetadataDTO;
import com.novabase.omni.oney.modules.ecmservice.data.api.dto.PurchaseOrderECMMetadataDTO;
import com.novabase.omni.oney.modules.ecmservice.data.api.smartdocs.exceptions.SmartDocsCreateDocumentException;
import com.novabase.omni.oney.modules.ecmservice.data.api.smartdocs.exceptions.SmartDocsGetDocumentUrlException;
import com.novabase.omni.oney.modules.ecmservice.data.api.smartdocs.exceptions.SmartDocsUpdateDocumentException;

@ProviderType
public interface SmartDocsDAO {

    public String getDocumentUrl(final String documentId) throws SmartDocsGetDocumentUrlException;

    public String createDocument(final String documentClass, final PurchaseOrderECMMetadataDTO metadata) throws SmartDocsCreateDocumentException;
    
    public String createDocument(final String documentClass, final FinancialDocumentECMMetadataDTO metadata) throws SmartDocsCreateDocumentException;

    public InputStream getDocument(String documentId) throws SmartDocsGetDocumentUrlException;

}
