package com.novabase.omni.oney.modules.ecmservice.data.api.smartdocs.exceptions;

public class SmartDocsUpdateDocumentException extends SmartDocsActionException {
    /**
     * 
     */
    private static final long serialVersionUID = 51245623689L;

    public SmartDocsUpdateDocumentException() {
	super();
    }

    public SmartDocsUpdateDocumentException(final String msg) {
	super(msg);
    }

    public SmartDocsUpdateDocumentException(final Exception e) {
	super(e);
    }

    public SmartDocsUpdateDocumentException(final Throwable e) {
	super(e);
    }

}
