package com.novabase.omni.oney.modules.ecmservice.data.api.smartdocs.exceptions;

public class SmartDocsGetDocumentUrlException extends SmartDocsActionException {

    /**
     * 
     */
    private static final long serialVersionUID = 5124458464489L;

    public SmartDocsGetDocumentUrlException() {
	super();
    }

    public SmartDocsGetDocumentUrlException(final String msg) {
	super(msg);
    }

    public SmartDocsGetDocumentUrlException(final Exception e) {
	super(e);
    }

    public SmartDocsGetDocumentUrlException(final Throwable e) {
	super(e);
    }

}
