package com.novabase.omni.oney.modules.ecmservice.data.api.exceptions;

public class ECMServiceDataException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 518923646132489L;

    public ECMServiceDataException() {
	super();
    }

    public ECMServiceDataException(final String msg) {
	super(msg);
    }

    public ECMServiceDataException(final Exception e) {
	super(e);
    }

    public ECMServiceDataException(final Throwable e) {
	super(e);
    }

}
