package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses.base.ResponseWrapper;

public class UpdateDocumentResponseWrapperDTO extends DTO implements ResponseWrapper<UpdateDocumentResponseDTO> {

    @JsonProperty("UpdateDocumentResponse")
    private UpdateDocumentResponseDTO updateDocumentResponse;

    public UpdateDocumentResponseWrapperDTO() {

	super();

	this.updateDocumentResponse = new UpdateDocumentResponseDTO();

    }

    public UpdateDocumentResponseDTO getUpdateDocumentResponse() {
	return updateDocumentResponse;
    }

    public void setUpdateDocumentResponse(UpdateDocumentResponseDTO updateDocumentResponse) {
	this.updateDocumentResponse = updateDocumentResponse;
    }

    @Override
    public UpdateDocumentResponseDTO getResponse() {
	return this.updateDocumentResponse;
    }

}
