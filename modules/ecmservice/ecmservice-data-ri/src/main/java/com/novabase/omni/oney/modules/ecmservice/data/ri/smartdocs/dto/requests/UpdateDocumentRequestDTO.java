package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.requests;

import java.util.HashMap;
import java.util.Map;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateDocumentRequestDTO extends DTO {

    @JsonProperty("DocumentId")
    private String documentId;
    @JsonProperty("UpdateDocument")
    private Map<String, String> updateDocument;

    public UpdateDocumentRequestDTO() {

	super();

	this.updateDocument = new HashMap<String, String>();

    }

    public String getDocumentId() {
	return documentId;
    }

    public void setDocumentId(final String documentId) {
	this.documentId = documentId;
    }

    public Map<String, String> getUpdateDocument() {
	return updateDocument;
    }

    public void setUpdateDocument(final Map<String, String> updateDocument) {
	this.updateDocument = updateDocument;
    }

}
