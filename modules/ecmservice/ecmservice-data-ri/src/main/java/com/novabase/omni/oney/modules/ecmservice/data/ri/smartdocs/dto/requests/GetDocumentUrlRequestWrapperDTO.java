package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.requests;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetDocumentUrlRequestWrapperDTO extends DTO {

    @JsonProperty("GetDocumentUrlRequest")
    private GetDocumentUrlRequestDTO getDocumentUrlRequest;

    public GetDocumentUrlRequestWrapperDTO() {

	super();

	this.getDocumentUrlRequest = new GetDocumentUrlRequestDTO();
    }

    public GetDocumentUrlRequestDTO getGetDocumentUrlRequest() {
	return getDocumentUrlRequest;
    }

    public void setGetDocumentUrlRequest(GetDocumentUrlRequestDTO getDocumentUrlRequest) {
	this.getDocumentUrlRequest = getDocumentUrlRequest;
    }

}
