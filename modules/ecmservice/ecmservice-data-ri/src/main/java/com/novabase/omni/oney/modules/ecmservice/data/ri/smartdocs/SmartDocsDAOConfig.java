package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs;

import javax.ws.rs.core.MediaType;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/smartdocs", icon = @Icon(resource = "OSGI-INF/icon/ws.png", size = 32))
public @interface SmartDocsDAOConfig {

    public static final String CPID = "com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs";

    @AttributeDefinition(name = "%providerName.name", description = "%providerName.description")
    public String providerName() default "Oney ECM SmartDocs Service";

    @AttributeDefinition(name = "%address.name", description = "%address.description")
    public String address() default "https://eaidev.oney.pt";

    @AttributeDefinition(name = "%contentType.name", description = "%contentType.description")
    public String contentType() default MediaType.APPLICATION_JSON;

    @AttributeDefinition(name = "%acceptType.name", description = "%acceptType.description")
    public String acceptType() default MediaType.APPLICATION_JSON;

    @AttributeDefinition(name = "%userName.name", description = "%userName.description")
    public String userName() default "";

    @AttributeDefinition(name = "%password.name", type = AttributeType.PASSWORD, description = "%password.description")
    public String password() default "";

    @AttributeDefinition(name = "%connectionTimeout.name", type = AttributeType.LONG, min = "0", description = "%connectionTimeout.description")
    public long connectionTimeout() default 10000L;

    @AttributeDefinition(name = "%receiveTimeout.name", type = AttributeType.LONG, min = "0", description = "%receiveTimeout.description")
    public long receiveTimeout() default 180000L;

    @AttributeDefinition(name = "%proxyEnabled.name", type = AttributeType.BOOLEAN, description = "%proxyEnabled.description")
    public boolean proxyEnabled() default false;

    @AttributeDefinition(name = "%proxyHost.name", description = "%proxyHost.description")
    public String proxyHost() default "localhost";

    @AttributeDefinition(name = "%proxyPort.name", description = "%proxyPort.description")
    public int proxyPort() default 8888;

    @AttributeDefinition(name = "%jsonPathSuccessExpression.name", description = "%jsonPathSuccessExpression.description")
    public String jsonPathSuccessExpression() default "";

    @AttributeDefinition(name = "%serializationFeatures.name", description = "%serializationFeatures.description")
    public String[] serializationFeatures() default {};

    @AttributeDefinition(name = "%deserializationFeatures.name", description = "%deserializationFeatures.description")
    public String[] deserializationFeatures() default { "ACCEPT_EMPTY_STRING_AS_NULL_OBJECT=true", "FAIL_ON_UNKNOWN_PROPERTIES=false" };

    @AttributeDefinition(name = "%headersApplication.name", description = "%headersApplication.description")
    public String headersApplication() default "WIZZIO";
    
    @AttributeDefinition(name = "%headersChannel.name", description = "%headersChannel.description")
    public String headersChannel() default "WEB";
    
    @AttributeDefinition(name = "%resourceGetDocumentUrl.name", description = "%resourceGetDocumentUrl.description")
    public String resourceGetDocumentUrl() default "/CORPORATE/API/getDocumentUrl/get";

    @AttributeDefinition(name = "%resourcePostCreateDocument.name", description = "%resourcePostCreateDocument.description")
    public String resourcePostCreateDocument() default "/CORPORATE/API/createDocument/create";

    @AttributeDefinition(name = "%resourcePostUpdateDocument.name", description = "%resourcePostUpdateDocument.description")
    public String resourcePostUpdateDocument() default "/CORPORATE/API/updateDocument/update";

}
