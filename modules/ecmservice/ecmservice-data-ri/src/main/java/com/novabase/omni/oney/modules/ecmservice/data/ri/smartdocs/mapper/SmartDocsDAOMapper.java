package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.FinancialDocumentECMMetadataDTO;
import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.PurchaseOrderECMMetadataDTO;

@Mapper
public interface SmartDocsDAOMapper {

    public static final SmartDocsDAOMapper INSTANCE = Mappers.getMapper(SmartDocsDAOMapper.class);
    
    public PurchaseOrderECMMetadataDTO toPurchaseOrderECMMetadataDTO(final com.novabase.omni.oney.modules.ecmservice.data.api.dto.PurchaseOrderECMMetadataDTO source);
    
    public FinancialDocumentECMMetadataDTO toFinancialDocumentECMMetadataDTO(final com.novabase.omni.oney.modules.ecmservice.data.api.dto.FinancialDocumentECMMetadataDTO source);
    
}
