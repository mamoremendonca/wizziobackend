package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses.base;

import java.util.HashMap;
import java.util.Map;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorsDTO extends DTO {

    @JsonProperty("Error")
    private Map<String, String> error;

    public ErrorsDTO() {

	super();

	this.error = new HashMap<String, String>();

    }

    public Map<String, String> getError() {
	return error;
    }

    public void setError(final Map<String, String> error) {
	this.error = error;
    }

}
