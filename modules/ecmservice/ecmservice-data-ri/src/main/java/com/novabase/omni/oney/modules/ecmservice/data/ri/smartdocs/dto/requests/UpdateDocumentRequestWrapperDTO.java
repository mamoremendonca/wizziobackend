package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.requests;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateDocumentRequestWrapperDTO extends DTO {

    @JsonProperty("UpdateDocumentRequest")
    private UpdateDocumentRequestDTO updateDocumentRequest;

    public UpdateDocumentRequestWrapperDTO() {

	super();

	this.updateDocumentRequest = new UpdateDocumentRequestDTO();
    }

    public UpdateDocumentRequestDTO getUpdateDocumentRequest() {
	return updateDocumentRequest;
    }

    public void setUpdateDocumentRequest(UpdateDocumentRequestDTO updateDocumentRequest) {
	this.updateDocumentRequest = updateDocumentRequest;
    }

}
