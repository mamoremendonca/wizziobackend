package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.requests;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateFinancialDocumentECMDocumentRequestWrapperDTO extends DTO {

    @JsonProperty("CreateDocumentRequest")
    private CreateFinancialDocumentECMDocumentRequestDTO createDocumentRequest;

    public CreateFinancialDocumentECMDocumentRequestWrapperDTO() {

	super();

	this.createDocumentRequest = new CreateFinancialDocumentECMDocumentRequestDTO();

    }

    public CreateFinancialDocumentECMDocumentRequestDTO getCreateDocumentRequest() {
        return createDocumentRequest;
    }

    public void setCreateDocumentRequest(CreateFinancialDocumentECMDocumentRequestDTO createDocumentRequest) {
        this.createDocumentRequest = createDocumentRequest;
    }

}
