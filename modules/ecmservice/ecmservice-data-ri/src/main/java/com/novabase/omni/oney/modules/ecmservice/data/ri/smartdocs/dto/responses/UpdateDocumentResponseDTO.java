package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses;

import java.util.Map;

import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses.base.BaseResponseDTO;

// TODO: Review response to check the String with json object inside situation.
public class UpdateDocumentResponseDTO extends BaseResponseDTO<Map<String, String>> {

    public UpdateDocumentResponseDTO() {
	super();
    }

}
