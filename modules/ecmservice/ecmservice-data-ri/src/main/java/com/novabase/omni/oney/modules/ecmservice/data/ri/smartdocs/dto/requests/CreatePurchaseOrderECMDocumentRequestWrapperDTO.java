package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.requests;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreatePurchaseOrderECMDocumentRequestWrapperDTO extends DTO {

    @JsonProperty("CreateDocumentRequest")
    private CreatePurchaseOrderECMDocumentRequestDTO createDocumentRequest;

    public CreatePurchaseOrderECMDocumentRequestWrapperDTO() {

	super();

	this.createDocumentRequest = new CreatePurchaseOrderECMDocumentRequestDTO();

    }

    public CreatePurchaseOrderECMDocumentRequestDTO getCreateDocumentRequest() {
	return createDocumentRequest;
    }

    public void setCreateDocumentRequest(CreatePurchaseOrderECMDocumentRequestDTO createDocumentRequest) {
	this.createDocumentRequest = createDocumentRequest;
    }

}
