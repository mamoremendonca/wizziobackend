package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses.base;

public interface ResponseWrapper<T extends BaseResponseDTO> {

    public T getResponse();
    
}
