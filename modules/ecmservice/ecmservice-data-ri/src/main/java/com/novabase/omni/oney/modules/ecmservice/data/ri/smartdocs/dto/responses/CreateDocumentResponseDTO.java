package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses;

import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses.base.BaseResponseDTO;

public class CreateDocumentResponseDTO extends BaseResponseDTO<String> {

    public CreateDocumentResponseDTO() {
	super();
    }

}
