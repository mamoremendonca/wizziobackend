package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SmartDocsDAOConfigWrapper {

    public static final String AUTHORIZATION_USER = "AUTHORIZATION_USER";
    public static final String AUTHORIZATION_PASSWORD = "AUTHORIZATION_PASSWORD";
    public static final String PAYLOAD_STD_ATTRIBUTES_ETRANSATION_IDENTIFIER = "PayloadStdAttributes-ETransactionIdentifier";
    public static final String UX_DETAILS_CONSUMER_CREDENTIALS_APPLICATION = "UXDetails-ConsumerCredentials-Application";
    public static final String UX_DETAILS_CONSUMER_CREDENTIALS_USER = "UXDetails-ConsumerCredentials-User";
    public static final String UX_DETAILS_CONSUMER_CREDENTIALS_PASSWORD = "UXDetails-ConsumerCredentials-Password";
    public static final String UX_DETAILS_CONSUMER_CHANNERL_CHANNEL = "UXDetails-ConsumerChannel-Channel";

    private String contentType;
    private String acceptType;
    private Map<String, String> headers;
    
    public SmartDocsDAOConfigWrapper() {
	super();
    }
    
    public SmartDocsDAOConfigWrapper(final SmartDocsDAOConfig config) {
	
	super();
	
	this.contentType = config.contentType();
	this.acceptType = config.acceptType();
	
	this.headers = new HashMap<String, String>();
	this.headers.put(AUTHORIZATION_USER, config.userName());
	this.headers.put(AUTHORIZATION_PASSWORD, config.password());
	this.headers.put(UX_DETAILS_CONSUMER_CREDENTIALS_USER, config.userName());
	this.headers.put(UX_DETAILS_CONSUMER_CREDENTIALS_PASSWORD, config.password());
	this.headers.put(PAYLOAD_STD_ATTRIBUTES_ETRANSATION_IDENTIFIER, UUID.randomUUID().toString());
	this.headers.put(UX_DETAILS_CONSUMER_CREDENTIALS_APPLICATION, config.headersApplication());
	this.headers.put(UX_DETAILS_CONSUMER_CHANNERL_CHANNEL, config.headersChannel());
	
    }

    public SmartDocsDAOConfigWrapper(final SmartDocsDAOConfig config, final Map<String, String> headers) {
	
	this(config);
	this.headers = new HashMap<String, String>(headers);
	
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getAcceptType() {
        return acceptType;
    }

    public void setAcceptType(String acceptType) {
        this.acceptType = acceptType;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }
    
}
