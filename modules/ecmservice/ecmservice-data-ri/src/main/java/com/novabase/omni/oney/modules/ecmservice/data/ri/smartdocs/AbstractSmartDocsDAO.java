package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs;

import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.apache.cxf.jaxrs.client.WebClient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.novabase.omni.oney.modules.ecmservice.data.api.smartdocs.constants.SmartDocsDAOConstants;
import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses.base.BaseResponseDTO;
import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses.base.ResponseWrapper;

import io.digitaljourney.platform.modules.ws.api.dao.WSData;
import io.digitaljourney.platform.modules.ws.api.dao.WSResult;
import io.digitaljourney.platform.modules.ws.rs.api.dao.AbstractRSDAO;

public abstract class AbstractSmartDocsDAO<A extends Annotation> extends AbstractRSDAO<A> {

    public AbstractSmartDocsDAO() {
	super();
    }

    protected <T, U> U post(final T request, final Class<U> responseClass, final String path, final SmartDocsDAOConfigWrapper config) {
	return this.post(request, responseClass, path, config, Boolean.FALSE);
    }

    protected <T, U> U post(final T request, final Class<U> responseClass, final String path, final SmartDocsDAOConfigWrapper config, final Boolean defaultResponseCodes) {

	final BiFunction<T, WebClient, U> call = (final T funcRequestWrapper, final WebClient funcClient) -> this.prepare(funcClient, path, config).post(funcRequestWrapper, responseClass);
	final WSData<U> response = this.call(request, call, defaultResponseCodes); // TRUE to use WSData Default Success codes and Descriptions as this service
										   // does not return Success Data.

	return this.validateResponse(response);

    }

    protected <T> T get(final Class<T> responseClass, final String path, final SmartDocsDAOConfigWrapper config) {
	return this.get(responseClass, path, config, Boolean.FALSE);
    }

    protected <T> T get(final Class<T> responseClass, final String path, final SmartDocsDAOConfigWrapper config, final Boolean defaultSuccessCode) {

	final Function<WebClient, T> call = (final WebClient funcClient) -> this.prepare(funcClient, path, config).get(responseClass);
	final WSData<T> response = this.call(call, defaultSuccessCode);

	return this.validateResponse(response);

    }

    // TODO: Create option to call endpoints with query parameters.
    // DOES NOT WORK WITH QUERY PARAMETERS.
    private WebClient prepare(final WebClient client, final String path, final SmartDocsDAOConfigWrapper config) {
	
	//@formatter:off
	return client.path(path)
		.header("Authorization", String.format("Basic %s:%s", config.getHeaders().get(SmartDocsDAOConfigWrapper.AUTHORIZATION_USER), 
								      config.getHeaders().get(SmartDocsDAOConfigWrapper.AUTHORIZATION_PASSWORD)))
		.header(SmartDocsDAOConfigWrapper.PAYLOAD_STD_ATTRIBUTES_ETRANSATION_IDENTIFIER, 
			config.getHeaders().get(SmartDocsDAOConfigWrapper.PAYLOAD_STD_ATTRIBUTES_ETRANSATION_IDENTIFIER)) //TODO: passar correlation ID
		.header(SmartDocsDAOConfigWrapper.UX_DETAILS_CONSUMER_CREDENTIALS_APPLICATION, 
			config.getHeaders().get(SmartDocsDAOConfigWrapper.UX_DETAILS_CONSUMER_CREDENTIALS_APPLICATION))
		.header(SmartDocsDAOConfigWrapper.UX_DETAILS_CONSUMER_CREDENTIALS_USER, 
			config.getHeaders().get(SmartDocsDAOConfigWrapper.UX_DETAILS_CONSUMER_CREDENTIALS_USER))
		.header(SmartDocsDAOConfigWrapper.UX_DETAILS_CONSUMER_CREDENTIALS_PASSWORD, 
			config.getHeaders().get(SmartDocsDAOConfigWrapper.UX_DETAILS_CONSUMER_CREDENTIALS_PASSWORD))
		.header(SmartDocsDAOConfigWrapper.UX_DETAILS_CONSUMER_CHANNERL_CHANNEL, 
			config.getHeaders().get(SmartDocsDAOConfigWrapper.UX_DETAILS_CONSUMER_CHANNERL_CHANNEL))
		.type(config.getContentType())
		.accept(config.getAcceptType());
	//@formatter:on
	
    }

    private <T> T validateResponse(final WSData<T> response) {

	if (!response.success()) {
	    
	    this.error("Error trying to Invoking SAP Service: " + response.getStatusCode() + " " + response.getStatusMessage());
	    throw this.resultException(response);
	    
	}

	return response.data();

    }

    // END POINTS WITH REQUEST
    private <T, V> WSData<V> call(final T requestWrapper, final BiFunction<T, WebClient, V> function) {
	return this.call(requestWrapper, function, Boolean.FALSE);
    }

    private <T, V> WSData<V> call(final T requestWrapper, final BiFunction<T, WebClient, V> function, final Boolean defaultResponseCodes) {

	return this.invoke((final WebClient client) -> {
	    return this.processResponse(defaultResponseCodes, function.apply(requestWrapper, client));
	});

    }

    // END POINTS WITHOUT REQUEST
    private <V> WSData<V> call(final Function<WebClient, V> function) {
	return this.call(function, Boolean.FALSE);
    }

    private <V> WSData<V> call(final Function<WebClient, V> function, final Boolean defaultResponseCodes) {

	return this.invoke((final WebClient client) -> {
	    return this.processResponse(defaultResponseCodes, function.apply(client));
	});

    }

    private <V> WSData<V> processResponse(final Boolean defaultResponseCodes, final V responseWrapper) {

	final BaseResponseDTO response = ((ResponseWrapper<?>) responseWrapper).getResponse();
	final Map<String, String> errors = response.getErrors().getError();

	final String succesCode = defaultResponseCodes ? WSResult.DEFAULT_STATUS_CODE : errors.get(SmartDocsDAOConstants.ERROR_CODE_PROPERTY);
	final String succesDescription = defaultResponseCodes ? WSResult.DEFAULT_STATUS_MESSAGE : errors.get(SmartDocsDAOConstants.ERROR_DESCRIPTION_PROPERTY);

	final String errorCode = response.getSuccess() ? succesCode : errors.get(SmartDocsDAOConstants.ERROR_CODE_PROPERTY);
	final String errorDescription = response.getSuccess() ? succesDescription : errors.get(SmartDocsDAOConstants.ERROR_DESCRIPTION_PROPERTY);

	this.info("Invocation of SAP Service Returned Codes: " + errorCode + errorDescription);
	
	return WSData.of(responseWrapper).status(errorCode, errorDescription).build();

    }

    protected <T extends BaseResponseDTO> void logResponse(final ResponseWrapper<T> responseWrapper) {
	
	try {
	    
	    String responseBody = new ObjectMapper().writeValueAsString(responseWrapper);
	    this.info("Received Response JSON: " + responseBody);
	    
	} catch (JsonProcessingException e) {
	    this.warn("Unable to parse to JSON Response Received: " + responseWrapper.toString());

	}
	
    }
    
}

