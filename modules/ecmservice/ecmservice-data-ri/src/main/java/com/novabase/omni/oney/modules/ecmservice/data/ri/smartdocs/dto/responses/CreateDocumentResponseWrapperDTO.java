package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses.base.ResponseWrapper;

public class CreateDocumentResponseWrapperDTO extends DTO implements ResponseWrapper<CreateDocumentResponseDTO> {

    @JsonProperty("CreateDocumentResponse")
    private CreateDocumentResponseDTO createDocumentResponse;

    public CreateDocumentResponseWrapperDTO() {

	super();

	this.createDocumentResponse = new CreateDocumentResponseDTO();

    }

    public CreateDocumentResponseDTO getCreateDocumentResponse() {
	return createDocumentResponse;
    }

    public void setCreateDocumentResponse(CreateDocumentResponseDTO createDocumentResponse) {
	this.createDocumentResponse = createDocumentResponse;
    }

    @Override
    public CreateDocumentResponseDTO getResponse() {
	return this.createDocumentResponse;
    }

}
