package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.novabase.omni.oney.modules.ecmservice.data.api.dto.FinancialDocumentECMMetadataDTO;
import com.novabase.omni.oney.modules.ecmservice.data.api.dto.PurchaseOrderECMMetadataDTO;
import com.novabase.omni.oney.modules.ecmservice.data.api.enums.FinancialDocumentType;
import com.novabase.omni.oney.modules.ecmservice.data.api.smartdocs.SmartDocsDAO;
import com.novabase.omni.oney.modules.ecmservice.data.api.smartdocs.exceptions.SmartDocsCreateDocumentException;
import com.novabase.omni.oney.modules.ecmservice.data.api.smartdocs.exceptions.SmartDocsGetDocumentUrlException;
import com.novabase.omni.oney.modules.ecmservice.data.ri.WSContext;
import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.requests.CreateFinancialDocumentECMDocumentRequestWrapperDTO;
import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.requests.CreatePurchaseOrderECMDocumentRequestWrapperDTO;
import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.requests.GetDocumentUrlRequestWrapperDTO;
import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses.CreateDocumentResponseWrapperDTO;
import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses.GetDocumentUrlResponseWrapperDTO;
import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.mapper.SmartDocsDAOMapper;

import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;

//@formatter:off
@Component(
	configurationPid = SmartDocsDAOConfig.CPID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	reference = {
		@Reference(
			name = RSProperties.REF_CONTEXT,
			service = WSContext.class,
			cardinality = ReferenceCardinality.MANDATORY) })
@Designate(ocd = SmartDocsDAOConfig.class)
//@formatter:on
public final class SmartDocsDAOImpl extends AbstractSmartDocsDAO<SmartDocsDAOConfig> implements SmartDocsDAO {

    @Activate
    public void activate(ComponentContext ctx, SmartDocsDAOConfig config) {
	prepare(ctx, config);
    }

    @Modified
    public void modified(SmartDocsDAOConfig config) {
	prepare(config);
    }

    @Override
    public String getDocumentUrl(final String documentId) throws SmartDocsGetDocumentUrlException {

	if (documentId == null) {
	    throw new SmartDocsGetDocumentUrlException("Document Identifier Cannot Be Null...");
	} else {

	    final GetDocumentUrlRequestWrapperDTO wrapper = new GetDocumentUrlRequestWrapperDTO();
	    wrapper.getGetDocumentUrlRequest().setDocumentId(documentId);
	    this.logRequest(wrapper);

	    final GetDocumentUrlResponseWrapperDTO responseWrapper = this.callPost(wrapper, GetDocumentUrlResponseWrapperDTO.class, this.getConfig().resourceGetDocumentUrl(), Boolean.TRUE);
	    this.logResponse(responseWrapper);

	    try {
		return (String) new ObjectMapper().readValue(responseWrapper.getGetDocumentUrlResponse().getData().replace("'", "\""), Map.class).get("url");
	    } catch (final IOException exception) {

		this.error("Error trying to get Document URL from SmartDocs: " + exception.toString());
		throw new SmartDocsGetDocumentUrlException(exception.getMessage());

	    }

	}

    }

    @Override
    public InputStream getDocument(String documentId) throws SmartDocsGetDocumentUrlException {

	final String documentUrl = this.getDocumentUrl(documentId);
	if (documentUrl == null) {

	    this.error("Error trying to get document from SmartDocs: ECM Service returned invalid file....");
	    throw new SmartDocsGetDocumentUrlException("ECM Service returned invalid file....");

	}

	URL url;
	try {
	    url = new URL(documentUrl);
	} catch (MalformedURLException e) {

	    this.error("ECM Service returned invalid file...." + " exception: " + e.toString());
	    throw new SmartDocsGetDocumentUrlException("ECM Service returned invalid file...." + " exception: " + e.toString());

	}

	URLConnection uc;
	try {
	    uc = url.openConnection();
	} catch (IOException e) {

	    this.error("Could not open connection with the returned URL:" + documentUrl + " exception: " + e.toString());
	    throw new SmartDocsGetDocumentUrlException("Could not open connection with the returned URL:" + documentUrl + " exception: " + e.toString());

	}

	String userpass = getConfig().userName() + ":" + getConfig().password();
	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

	uc.setRequestProperty("Authorization", basicAuth);
	try {
	    return uc.getInputStream();
	} catch (IOException e) {

	    this.error("Could not read inputStream from returned URL:" + documentUrl + " exception: " + e.toString());
	    throw new SmartDocsGetDocumentUrlException("Could not read inputStream from returned URL:" + documentUrl + " exception: " + e.toString());

	}

    }

    @Override
    public String createDocument(final String documentClass, final PurchaseOrderECMMetadataDTO metadata) throws SmartDocsCreateDocumentException {

	if (metadata == null) {
	    error("Error trying to send document to SmartDocs: Document Cannot Be Null...");
	    throw new SmartDocsCreateDocumentException("Document Cannot Be Null...");
	} else if (documentClass == null) {
	    error("Error trying to send document to SmartDocs: Document Class Cannot Be Null...");
	    throw new SmartDocsCreateDocumentException("Document Class Cannot Be Null...");
	} else {

	    final CreatePurchaseOrderECMDocumentRequestWrapperDTO wrapper = new CreatePurchaseOrderECMDocumentRequestWrapperDTO();
	    wrapper.getCreateDocumentRequest().setDocumentClass(documentClass);
	    wrapper.getCreateDocumentRequest().setMetadata(SmartDocsDAOMapper.INSTANCE.toPurchaseOrderECMMetadataDTO(metadata));
	    this.logRequest(wrapper);

	    final CreateDocumentResponseWrapperDTO responseWrapper = this.callPost(wrapper, CreateDocumentResponseWrapperDTO.class, this.getConfig().resourcePostCreateDocument(), Boolean.TRUE);
	    this.logResponse(responseWrapper);

	    try {
		return (String) new ObjectMapper().readValue(responseWrapper.getCreateDocumentResponse().getData().replace("'", "\""), Map.class).get("ID");
	    } catch (final IOException exception) {

		error("Error trying to send document to SmartDocs: " + exception.toString());
		throw new SmartDocsCreateDocumentException(exception.getMessage());

	    }

	}

    }

    @Override
    public String createDocument(String documentClass, FinancialDocumentECMMetadataDTO metadata) throws SmartDocsCreateDocumentException {

	if (metadata == null) {
	    error("Error trying to send document to SmartDocs: Document Cannot Be Null...");
	    throw new SmartDocsCreateDocumentException("Document Cannot Be Null...");
	} else if (documentClass == null) {
	    error("Error trying to send document to SmartDocs: Document Class Cannot Be Null...");
	    throw new SmartDocsCreateDocumentException("Document Class Cannot Be Null...");
	} else {

	    final CreateFinancialDocumentECMDocumentRequestWrapperDTO wrapper = new CreateFinancialDocumentECMDocumentRequestWrapperDTO();
	    wrapper.getCreateDocumentRequest().setDocumentClass(documentClass);
	    wrapper.getCreateDocumentRequest().setMetadata(SmartDocsDAOMapper.INSTANCE.toFinancialDocumentECMMetadataDTO(metadata));
	    if (documentClass.equals("oney_financial_doc_supplier")) {
		// Mapping to SmartDocs Expected Value.
		wrapper.getCreateDocumentRequest().getMetadata().setFinancialDocumentTypeKey(FinancialDocumentType.valueOf(metadata.getFinancialDocumentTypeKey()).getValue());
	    }
	    this.logRequest(wrapper);

	    final CreateDocumentResponseWrapperDTO responseWrapper = this.callPost(wrapper, CreateDocumentResponseWrapperDTO.class, this.getConfig().resourcePostCreateDocument(), Boolean.TRUE);
	    this.logResponse(responseWrapper);

	    try {
		return (String) new ObjectMapper().readValue(responseWrapper.getCreateDocumentResponse().getData().replace("'", "\""), Map.class).get("ID");
	    } catch (final IOException exception) {

		error("Error trying to send document to SmartDocs: " + exception.toString());
		throw new SmartDocsCreateDocumentException(exception.getMessage());

	    }

	}

    }

    private void logRequest(final GetDocumentUrlRequestWrapperDTO wrapper) {

	String documentId = StringUtils.EMPTY;
	try {

	    documentId = wrapper.getGetDocumentUrlRequest().getDocumentId();
	    this.info("Retrieving Document URL for Document Id: " + documentId);

	    String requestBody = new ObjectMapper().writeValueAsString(wrapper);
	    this.info("Retrieve Document URL" + documentId + " JSON: " + requestBody);

	} catch (JsonProcessingException e) {
	    this.warn("Error trying to log request body to get Document Url. ID: " + documentId);
	}

    }

    private void logRequest(final CreatePurchaseOrderECMDocumentRequestWrapperDTO wrapper) {

	try {

	    String requestBody = new ObjectMapper().writeValueAsString(wrapper);
	    this.info("Create Document JSON: " + requestBody);

	} catch (JsonProcessingException e) {
	    this.warn("Error trying to log request body to create Document: ");
	}

    }

    private void logRequest(final CreateFinancialDocumentECMDocumentRequestWrapperDTO wrapper) {

	try {

	    String requestBody = new ObjectMapper().writeValueAsString(wrapper);
	    this.info("Create Document JSON: " + requestBody);

	} catch (JsonProcessingException e) {
	    this.warn("Error trying to log request body to create Document: ");
	}

    }

    private <T> T callGet(final Class<T> responseClass, final String path, final Boolean defaultSuccessCode) {

	return this.get(responseClass, path, new SmartDocsDAOConfigWrapper(this.getConfig()), defaultSuccessCode); // TRUE to use WSData Default Success codes and Descriptions as this service
														   // does not return Success Data.

    }

    private <T, U> U callPost(final T request, final Class<U> responseClass, final String path, final Boolean defaultSuccessCode) {

	return this.post(request, responseClass, path, new SmartDocsDAOConfigWrapper(this.getConfig()), defaultSuccessCode); // TRUE to use WSData Default Success codes and Descriptions as this
															     // service does not return Success Data.

    }

}
