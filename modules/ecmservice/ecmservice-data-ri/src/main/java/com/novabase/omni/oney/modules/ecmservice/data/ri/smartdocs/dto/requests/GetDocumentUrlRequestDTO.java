package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.requests;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetDocumentUrlRequestDTO extends DTO {

    @JsonProperty("DocumentId")
    private String documentId;

    public GetDocumentUrlRequestDTO() {
	super();
    }

    public String getDocumentId() {
	return documentId;
    }

    public void setDocumentId(String documentId) {
	this.documentId = documentId;
    }

}
