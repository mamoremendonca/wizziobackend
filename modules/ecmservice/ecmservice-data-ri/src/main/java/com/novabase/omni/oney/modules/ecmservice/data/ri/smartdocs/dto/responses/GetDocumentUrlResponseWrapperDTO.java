package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses.base.ResponseWrapper;

public class GetDocumentUrlResponseWrapperDTO extends DTO implements ResponseWrapper<GetDocumentUrlResponseDTO> {

    @JsonProperty("GetDocumentUrlResponse")
    private GetDocumentUrlResponseDTO getDocumentUrlResponse;

    public GetDocumentUrlResponseWrapperDTO() {

	super();

	this.getDocumentUrlResponse = new GetDocumentUrlResponseDTO();

    }

    public GetDocumentUrlResponseDTO getGetDocumentUrlResponse() {
	return getDocumentUrlResponse;
    }

    public void setGetDocumentUrlResponse(GetDocumentUrlResponseDTO getDocumentUrlResponse) {
	this.getDocumentUrlResponse = getDocumentUrlResponse;
    }

    @Override
    public GetDocumentUrlResponseDTO getResponse() {
	return this.getDocumentUrlResponse;
    }

}
