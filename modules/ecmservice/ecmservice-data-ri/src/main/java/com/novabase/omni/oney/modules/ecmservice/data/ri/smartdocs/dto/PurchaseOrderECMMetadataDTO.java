package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;

@JsonInclude(Include.NON_NULL)
@ApiModel(value = "PurchaseOrderECMMetadata", description = "The Purchase Order ECM Document Metadata")
public class PurchaseOrderECMMetadataDTO {

    @JsonProperty("object_name")
    private String objectName;
    
    @JsonProperty("reference")
    private String reference;
    
    @JsonProperty("registration_date")
    private String registrationDate;
    
    @JsonProperty("po_order_nr")
    private String purchaseOrderNumber;
    
    @JsonProperty("status__key")
    private String statusKey;
    
    @JsonProperty("status__value")
    private String statusValue;
    
    @JsonProperty("department__key")
    private String departmentKey;

    @JsonProperty("department__value")
    private String departmentValue;
    
    @JsonProperty("project__key")
    private String projectKey;
    
    @JsonProperty("project__value")
    private String projectValue;
    
    @JsonProperty("document_date")
    private String documentDate;
    
    @JsonProperty("total_value")
    private String totalValue;
    
    @JsonProperty("supplier_vat_number")
    private String supplierVatNumber;
    
    @JsonProperty("supplier_name")
    private String supplierName;
    
    @JsonProperty("supplier_id")
    private String supplierId;
    
    @JsonProperty("sap_supplier_id")
    private String sapSupplierId;

    @JsonProperty("url")
    private String documentUrl;
    
    public PurchaseOrderECMMetadataDTO() {
	super();
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getPurchaseOrderNumber() {
        return purchaseOrderNumber;
    }

    public void setPurchaseOrderNumber(String purchaseOrderNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
    }

    public String getStatusKey() {
        return statusKey;
    }

    public void setStatusKey(String statusKey) {
        this.statusKey = statusKey;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

    public String getDepartmentKey() {
        return departmentKey;
    }

    public void setDepartmentKey(String departmentKey) {
        this.departmentKey = departmentKey;
    }

    public String getDepartmentValue() {
        return departmentValue;
    }

    public void setDepartmentValue(String departmentValue) {
        this.departmentValue = departmentValue;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public String getProjectValue() {
        return projectValue;
    }

    public void setProjectValue(String projectValue) {
        this.projectValue = projectValue;
    }

    public String getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(String documentDate) {
        this.documentDate = documentDate;
    }

    public String getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(String totalValue) {
        this.totalValue = totalValue;
    }

    public String getSupplierVatNumber() {
        return supplierVatNumber;
    }

    public void setSupplierVatNumber(String supplierVatNumber) {
        this.supplierVatNumber = supplierVatNumber;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getSapSupplierId() {
        return sapSupplierId;
    }

    public void setSapSupplierId(String sapSupplierId) {
        this.sapSupplierId = sapSupplierId;
    }

    public String getDocumentUrl() {
        return documentUrl;
    }

    public void setDocumentUrl(String documentUrl) {
        this.documentUrl = documentUrl;
    }
        
}
