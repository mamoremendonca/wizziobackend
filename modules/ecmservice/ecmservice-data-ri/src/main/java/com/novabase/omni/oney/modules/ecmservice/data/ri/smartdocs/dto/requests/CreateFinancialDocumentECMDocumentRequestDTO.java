package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.requests;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.FinancialDocumentECMMetadataDTO;

public class CreateFinancialDocumentECMDocumentRequestDTO extends DTO {

    @JsonProperty("DocumentClass")
    private String documentClass;
    @JsonProperty("Metadata")
    private FinancialDocumentECMMetadataDTO metadata;

    public CreateFinancialDocumentECMDocumentRequestDTO() {

	super();
	
	this.metadata = new FinancialDocumentECMMetadataDTO();

    }

    public String getDocumentClass() {
	return documentClass;
    }

    public void setDocumentClass(final String documentClass) {
	this.documentClass = documentClass;
    }

    public FinancialDocumentECMMetadataDTO getMetadata() {
        return metadata;
    }

    public void setMetadata(FinancialDocumentECMMetadataDTO metadata) {
        this.metadata = metadata;
    }

}
