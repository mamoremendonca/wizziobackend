package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.requests;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.PurchaseOrderECMMetadataDTO;

public class CreatePurchaseOrderECMDocumentRequestDTO extends DTO {

    @JsonProperty("DocumentClass")
    private String documentClass;
    @JsonProperty("Metadata")
    private PurchaseOrderECMMetadataDTO metadata;

    public CreatePurchaseOrderECMDocumentRequestDTO() {

	super();
	
	this.metadata = new PurchaseOrderECMMetadataDTO();

    }

    public String getDocumentClass() {
	return documentClass;
    }

    public void setDocumentClass(final String documentClass) {
	this.documentClass = documentClass;
    }

    public PurchaseOrderECMMetadataDTO getMetadata() {
        return metadata;
    }

    public void setMetadata(PurchaseOrderECMMetadataDTO metadata) {
        this.metadata = metadata;
    }

}
