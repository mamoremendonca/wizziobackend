package com.novabase.omni.oney.modules.ecmservice.data.ri.smartdocs.dto.responses.base;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseResponseDTO<T> extends DTO {

    @JsonProperty("Success")
    private Boolean success;
    @JsonProperty("Errors")
    private ErrorsDTO errors;
    private String code;
    private String message;
    private T data;

    public BaseResponseDTO() {

	super();

	this.success = Boolean.FALSE;
	this.errors = new ErrorsDTO();

    }

    public Boolean getSuccess() {
	return success;
    }

    public void setSuccess(Boolean success) {
	this.success = success;
    }

    public ErrorsDTO getErrors() {
	return errors;
    }

    public void setErrors(ErrorsDTO errors) {
	this.errors = errors;
    }

    public String getCode() {
	return code;
    }

    public void setCode(String code) {
	this.code = code;
    }

    public String getMessage() {
	return message;
    }

    public void setMessage(String message) {
	this.message = message;
    }

    public T getData() {
	return data;
    }

    public void setData(T data) {
	this.data = data;
    }

}
