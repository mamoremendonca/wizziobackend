package com.novabase.omni.oney.modules.referencedata.data.api.exception;

public class NotFoundReferenceDataException extends SAPReferenceDataException {

    private static final long serialVersionUID = -8151659640579212876L;

    public NotFoundReferenceDataException(String msg) {
	super(msg);
    }
    
    public NotFoundReferenceDataException(Exception e) {
	super(e);
    }
}
