package com.novabase.omni.oney.modules.referencedata.data.api.exception;

public class SAPReferenceDataException extends Exception {

    private static final long serialVersionUID = 2113728450793386510L;

    public SAPReferenceDataException() {
	super();
    }
    public SAPReferenceDataException(String msg) {
	super(msg);
    }
    
    public SAPReferenceDataException(Exception e) {
	super(e);
    }
}
