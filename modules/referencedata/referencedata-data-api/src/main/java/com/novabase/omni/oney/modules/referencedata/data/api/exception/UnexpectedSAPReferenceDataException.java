package com.novabase.omni.oney.modules.referencedata.data.api.exception;

public class UnexpectedSAPReferenceDataException extends SAPReferenceDataException {

    private static final long serialVersionUID = -8151659640579212876L;

    public UnexpectedSAPReferenceDataException(String msg) {
	super(msg);
    }
    
    public UnexpectedSAPReferenceDataException(Exception e) {
	super(e);
    }
}
