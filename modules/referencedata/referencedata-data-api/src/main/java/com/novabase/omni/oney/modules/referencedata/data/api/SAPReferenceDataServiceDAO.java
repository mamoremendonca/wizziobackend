package com.novabase.omni.oney.modules.referencedata.data.api;

import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

import com.novabase.omni.oney.modules.referencedata.data.api.exception.UnexpectedSAPReferenceDataException;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Asset;
import com.novabase.omni.oney.modules.referencedata.entity.dto.CostCenter;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Department;
import com.novabase.omni.oney.modules.referencedata.entity.dto.InternalOrder;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Item;
import com.novabase.omni.oney.modules.referencedata.entity.dto.ItemType;
import com.novabase.omni.oney.modules.referencedata.entity.dto.RetentionCode;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Supplier;
import com.novabase.omni.oney.modules.referencedata.entity.dto.SupplierGeneralData;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Tax;

@ProviderType
public interface SAPReferenceDataServiceDAO {


	// => POItems
	public List<Item> getItems() throws UnexpectedSAPReferenceDataException;

	// => POItemTypes
	public List<ItemType> getItemTypes() throws UnexpectedSAPReferenceDataException;

	// => TaxCodes
	public List<Tax> getTaxes() throws UnexpectedSAPReferenceDataException;

	// => CostCenters
	public List<CostCenter> getCostCenters() throws UnexpectedSAPReferenceDataException;

	// => InternalOrders
	public List<InternalOrder> getInternalOrdens() throws UnexpectedSAPReferenceDataException;

	// => WithholdingTaxCodes
	public List<RetentionCode> getRetentionCode() throws UnexpectedSAPReferenceDataException;

	// => POAssets
	public List<Asset> getAssets() throws UnexpectedSAPReferenceDataException;

	// => Department
	public List<Department> getDepartments() throws UnexpectedSAPReferenceDataException;

	// => Suppliers
    	public List<Supplier> getSuppliers(final Supplier searchFilter) throws UnexpectedSAPReferenceDataException;
    	
	// => SupplierGeneralData
	public SupplierGeneralData getSupplierGeneralData(final String supplierCode) throws UnexpectedSAPReferenceDataException;
	
}
