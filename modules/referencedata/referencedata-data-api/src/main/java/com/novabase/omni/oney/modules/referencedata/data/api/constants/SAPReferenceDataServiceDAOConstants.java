package com.novabase.omni.oney.modules.referencedata.data.api.constants;

public final class SAPReferenceDataServiceDAOConstants {

    public final static String ERROR_CODE_PROPERTY = "Code";
    public final static String ERROR_DESCRIPTION_PROPERTY = "Description";

    private SAPReferenceDataServiceDAOConstants() {
	super();
    }

}
