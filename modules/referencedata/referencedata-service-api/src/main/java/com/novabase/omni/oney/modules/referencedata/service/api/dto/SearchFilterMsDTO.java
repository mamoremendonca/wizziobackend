package com.novabase.omni.oney.modules.referencedata.service.api.dto;

import java.io.Serializable;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("SearchFilter")
public class SearchFilterMsDTO extends DTO implements Serializable {

    public static final long serialVersionUID = 3340905291598000573L;

    @ApiModelProperty
    public String id;

    @ApiModelProperty
    public String nif;

    @ApiModelProperty
    public String name;
    
}
