package com.novabase.omni.oney.modules.referencedata.service.api.dto;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("InternalOrderDTO")
public class InternalOrderMsDTO extends ReferenceDataMsDTO {

    private static final long serialVersionUID = -7182481795175820991L;

    @ApiModelProperty(value = "CostCenter")
    public String costCenterCode;

    @ApiModelProperty(value = "Department")
    public String departmentCode;
}
