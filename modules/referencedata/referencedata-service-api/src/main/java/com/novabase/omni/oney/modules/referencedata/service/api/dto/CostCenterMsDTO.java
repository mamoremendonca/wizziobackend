package com.novabase.omni.oney.modules.referencedata.service.api.dto;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("CostCenterDTO")
public class CostCenterMsDTO extends ReferenceDataMsDTO {

    private static final long serialVersionUID = -3808478687384955796L;

    @ApiModelProperty(value = "Department")
    public String departmentCode;
}
