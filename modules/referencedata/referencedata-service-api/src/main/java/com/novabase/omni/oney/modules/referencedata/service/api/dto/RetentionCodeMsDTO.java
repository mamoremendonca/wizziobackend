package com.novabase.omni.oney.modules.referencedata.service.api.dto;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("RetentionCodeDTO")
public class RetentionCodeMsDTO extends ReferenceDataMsDTO {

    private static final long serialVersionUID = 6303720731041210057L;

    @ApiModelProperty(value = "TaxPercentage")
    public String taxPercentage;
}
