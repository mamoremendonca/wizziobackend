package com.novabase.omni.oney.modules.referencedata.service.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("TaxDTO")
public class TaxMsDTO extends ReferenceDataMsDTO {

    private static final long serialVersionUID = 1802266349903376332L;

    @ApiModelProperty(value = "taxPercentage")
    public String taxPercentage;
    
    @ApiModelProperty(value = "Market")
    public String marketCode;
}
