package com.novabase.omni.oney.modules.referencedata.service.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("DepartmentDTO")
public class DepartmentMsDTO extends ReferenceDataMsDTO {

    private static final long serialVersionUID = -5985848166017911237L;
    
}
