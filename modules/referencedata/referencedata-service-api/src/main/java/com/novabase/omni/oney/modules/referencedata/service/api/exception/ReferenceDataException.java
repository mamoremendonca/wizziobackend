package com.novabase.omni.oney.modules.referencedata.service.api.exception;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;
import io.digitaljourney.platform.modules.ws.rs.api.exception.RSException;
import com.novabase.omni.oney.modules.referencedata.service.api.ReferenceDataProperties;

public class ReferenceDataException extends RSException {
	private static final long serialVersionUID = -8645577031119256555L;

	protected ReferenceDataException(PlatformCode statusCode, String errorCode, Context ctx, Object... args) {
		super(statusCode, errorCode, ctx, args);
	}

	protected ReferenceDataException(Context ctx, Object... args) {
		this(PlatformCode.INTERNAL_SERVER_ERROR, ReferenceDataProperties.REFERENCEDATA000, ctx, args);
	}

	protected ReferenceDataException(String errorCode, Context ctx, Object... args) {
		this(PlatformCode.BUSINESS_ERROR, errorCode, ctx, args);
	}

	public static ReferenceDataException of(Context ctx, String message) {
		return new ReferenceDataException(ctx, message);
	}

	public static ReferenceDataException of(Context ctx, Throwable cause) {
		return new ReferenceDataException(ctx, cause);
	}
}
