package com.novabase.omni.oney.modules.referencedata.service.api.dto;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("ItemDTO")
public class AssetMsDTO extends ReferenceDataMsDTO {

    private static final long serialVersionUID = 1566645323981047798L;

//    @ApiModelProperty(value = "Department")
//    public String departmentCode; //TODO remover, assets nao filtra por departamento

    @ApiModelProperty(value = "Tax")
    public String defaultTaxCode;
}
