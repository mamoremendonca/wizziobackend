package com.novabase.omni.oney.modules.referencedata.service.api;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.validator.constraints.NotBlank;
import org.osgi.annotation.versioning.ProviderType;

import com.novabase.omni.oney.modules.referencedata.service.api.dto.AssetMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.CostCenterMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.DepartmentMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.InternalOrderMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ItemMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ItemTypeMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ProjectMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.RetentionCodeMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SearchFilterMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierGeneralDataMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.TaxMsDTO;

import io.digitaljourney.platform.modules.commons.type.HttpStatusCode;
import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.ApiKeyAuthDefinition.ApiKeyLocation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.BasicAuthDefinition;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;

@ProviderType
@Path("")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

@SwaggerDefinition(securityDefinition = @SecurityDefinition(basicAuthDefinitions = {
        @BasicAuthDefinition(key = RSProperties.SWAGGER_BASIC_AUTH) }, apiKeyAuthDefinitions = {
                @ApiKeyAuthDefinition(key = RSProperties.SWAGGER_BEARER_AUTH, name = RSProperties.HTTP_HEADER_API_KEY, in = ApiKeyLocation.HEADER) }), schemes = {
                        SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTPS,
                        SwaggerDefinition.Scheme.DEFAULT })
@Api(value = "Reference Data Service", authorizations = { @Authorization(value = RSProperties.SWAGGER_BASIC_AUTH),
        @Authorization(value = RSProperties.SWAGGER_BEARER_AUTH) })
@ApiResponses(value = {
        @ApiResponse(code = HttpStatusCode.UNAUTHORIZED_CODE, message = RSProperties.SWAGGER_UNAUTHORIZED_MESSAGE),
        @ApiResponse(code = HttpStatusCode.FORBIDDEN_CODE, message = RSProperties.SWAGGER_FORBIDDEN_MESSAGE) })
public interface ReferenceDataResource {

	@GET
	@Path("/items")
	@ApiOperation(value = "Returns all items based on item type ", response = ItemMsDTO.class, responseContainer = "List")
	public List<ItemMsDTO> getItems();

	@GET
	@Path("/itemtypes")
	@ApiOperation(value = "Returns all item types", response = ItemTypeMsDTO.class, responseContainer = "List")
	public List<ItemTypeMsDTO> getItemTypes();
	
	@GET
	@Path("/taxes")
	@ApiOperation(value = "Searches address by addressId and country", response = TaxMsDTO.class, responseContainer = "List")
	public List<TaxMsDTO> getTaxes();

	@GET
	@Path("/costcenters")
	@ApiOperation(value = "Returns all cost centers registered in SAP", response = CostCenterMsDTO.class, responseContainer = "List")
	public List<CostCenterMsDTO> getCostCenters();

	@GET
	@Path("/projects")
	@ApiOperation(value = "Returns all projects registered in SAP", response = ProjectMsDTO.class, responseContainer = "List")
	public List<ProjectMsDTO> getProjects();

	@GET
	@Path("/internalorders")
	@ApiOperation(value = "Returns all internal orders registered in SAP", response = InternalOrderMsDTO.class, responseContainer = "List")
	public List<InternalOrderMsDTO> getInternalOrdens();

	@GET
	@Path("/departments")
	@ApiOperation(value = "Returns all departments registered in SAP", response = DepartmentMsDTO.class, responseContainer = "List")
	public List<DepartmentMsDTO> getDepartments();
	
	@GET
	@Path("/retentioncode")
	@ApiOperation(value = "Searches address by addressId and country", response = RetentionCodeMsDTO.class, responseContainer = "List")
	public List<RetentionCodeMsDTO> getRetentionCode();	
	
//	@GET
//	@Path("/doctypes")
//	@ApiOperation(value = "Returns list of (financial) document types", response = DocumentTypeDTO.class, responseContainer = "List")
//	public List<DocumentTypeDTO> getDocumentTypes();	
	
	@GET
	@Path("/asset")
	@ApiOperation(value = "Returns list of assets (Imobilizado)", response = AssetMsDTO.class, responseContainer = "List")
	public List<AssetMsDTO> getAssets();	
	
	@POST
	@Path("/suppliers")
	@ApiOperation(value = "Searches address by addressId and country", response = SupplierMsDTO.class, responseContainer = "List")
	public List<SupplierMsDTO> getSuppliers(@ApiParam(value = "Search Parameters", required = false) @Valid final SearchFilterMsDTO searchFilter);
	
	@GET
	@Path("/supplier/{supplierCode}/market")
	@ApiOperation(value = "Returns the suppliers market info", response = SupplierGeneralDataMsDTO.class)
	public SupplierGeneralDataMsDTO getSupplierMarketInfo(@ApiParam(value = "The IdentifierNumber of the Supplier", required = true) @PathParam("supplierCode") @NotBlank final String supplierCode);
	
}
