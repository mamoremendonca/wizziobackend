package com.novabase.omni.oney.modules.referencedata.service.api.dto;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("SupplierGeneralDataDTO")
public class SupplierGeneralDataMsDTO extends SupplierMsDTO {

    @ApiModelProperty(value = "FiscalDesignator")
    private String fiscalDesignator;
    
    @ApiModelProperty(value = "SocialDesignator")
    private String socialDesignator;
    
    @ApiModelProperty(value = "MarketType")
    private String marketType;

    public SupplierGeneralDataMsDTO() {
	super();
    }

    public String getFiscalDesignator() {
        return fiscalDesignator;
    }

    public void setFiscalDesignator(String fiscalDesignator) {
        this.fiscalDesignator = fiscalDesignator;
    }

    public String getSocialDesignator() {
        return socialDesignator;
    }

    public void setSocialDesignator(String socialDesignator) {
        this.socialDesignator = socialDesignator;
    }

    public String getMarketType() {
        return marketType;
    }

    public void setMarketType(String marketType) {
        this.marketType = marketType;
    }
    
}
