package com.novabase.omni.oney.modules.referencedata.service.api.dto;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("DocumentTypeDTO")
public class DocumentTypeMsDTO extends ReferenceDataMsDTO {

    private static final long serialVersionUID = -497788227966372610L;

    @ApiModelProperty(value = "Type")
    public String type;
}
