package com.novabase.omni.oney.modules.referencedata.service.api.dto;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("ItemDTO")
public class ItemMsDTO extends ReferenceDataMsDTO {

    private static final long serialVersionUID = -5180762552066539426L;

    @ApiModelProperty(value = "Department")
    public String departmentCode;

    @ApiModelProperty(value = "CostCenter")
    public String defaultCostCenterCode;

    @ApiModelProperty(value = "Tax")
    public String defaultTaxCode;

    @ApiModelProperty(value = "Type")
    public String itemTypeCode;

}
