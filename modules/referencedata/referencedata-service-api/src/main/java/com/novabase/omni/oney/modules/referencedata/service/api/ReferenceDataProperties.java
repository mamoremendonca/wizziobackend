package com.novabase.omni.oney.modules.referencedata.service.api;

import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;

public abstract class ReferenceDataProperties extends RSProperties {
	public static final String REFERENCEDATA000 = "REFERENCEDATA000";
}
