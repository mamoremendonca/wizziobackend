package com.novabase.omni.oney.modules.referencedata.service.api.dto;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("ProjectDTO")
public class ProjectMsDTO extends CostCenterMsDTO {

    private static final long serialVersionUID = -3815242249859503675L;

}
