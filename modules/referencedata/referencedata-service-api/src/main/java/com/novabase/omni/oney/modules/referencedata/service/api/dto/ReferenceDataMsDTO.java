package com.novabase.omni.oney.modules.referencedata.service.api.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("CostCenterDTO")
public class ReferenceDataMsDTO extends DTO implements Serializable {

    private static final long serialVersionUID = 4227512032166116293L;

    @ApiModelProperty(value = "Code")
    public String code;

    @ApiModelProperty(value = "Description")
    public String description;

    @ApiModelProperty(value = "Active")
    public boolean active;
    
}
