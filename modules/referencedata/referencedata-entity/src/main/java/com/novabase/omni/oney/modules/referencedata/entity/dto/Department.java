package com.novabase.omni.oney.modules.referencedata.entity.dto;

public class Department extends ReferenceData {

    public String Code;
    public String Name;
    public String Date;				// "20200207"
    
    public Department() {
	super();
    }
    
    public Department(final String description, final String status) {
	super(description, status);
    }
    
    public Department(final String code, final String description, final String status) {
	
	super(description, status);
	
	this.Code = code;
	this.Name = description;
	
    }

}
