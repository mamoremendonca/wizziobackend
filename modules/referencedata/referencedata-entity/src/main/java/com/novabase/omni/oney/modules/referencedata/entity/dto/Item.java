package com.novabase.omni.oney.modules.referencedata.entity.dto;

public class Item extends ReferenceData {

    public String Account;			// ?
    public String AccountDeferral;		// ?
    public String Material;			// Code?!?
    public String Materialtype;		        // Tipificação de Artigos?!?
    public String DescCodtypemat;		// Descrição Tipificação de Artigos
    public String Department;
    public String CostCenter;			// default CostCenter
    public String Date;				// "20200207"
    public String TaxCode;			// default TaxCode
    public String Zorder;			// ?
    
    public Item() {
	super();
    }
    
    public Item(final String description, final String status) {
	super(description, status);
    }
    
    public Item(final String code, final String description, final String account, final String accountDeferral, final String costCenter, 
	    final String tax, final String type, final String status, final String descType, final String date, final String zOrder, 
	    final String department) {
	
	super(description, status);
	
	this.Account = account;
	this.AccountDeferral = accountDeferral;
	this.Material = code;
	this.Materialtype = type;
	this.DescCodtypemat = descType;
	this.CostCenter = costCenter;
	this.Date = date;
	this.TaxCode = tax;
	this.Zorder = zOrder;
	this.Department = department;
	
    }

}
