package com.novabase.omni.oney.modules.referencedata.entity.dto;

public class CostCenter extends ReferenceData {

    public static final String COST_CENTER_TYPE_PROJECT = "P";

    public String CostCenter;		// Name
    public String Date;			// "20200101"
    public String Department;
    public String TypeCostCenter;
    
    public CostCenter() {
	super();
    }
    
    public CostCenter(final String description, final String status) {
	super(description, status);
    }
    
    public CostCenter(final String code, final String description, final String status, final String department, final String type) {
	
	super(description, status);
	
	this.CostCenter = code;
	this.Department = department;
	this.TypeCostCenter = type;
	
    }

}




