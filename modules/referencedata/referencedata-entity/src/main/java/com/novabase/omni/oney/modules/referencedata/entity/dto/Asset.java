package com.novabase.omni.oney.modules.referencedata.entity.dto;

public class Asset extends ReferenceData {

    public String Date;
    public String AssetNumber;
    public String TaxCode;
    
    public Asset() {
	super();
    }
    
    public Asset(final String description, final String status) {
	super(description, status);
    }
    
    public Asset(final String assetNumber, final String description, final String status, final String taxCode) {
	
	super(description, status);
	
	this.AssetNumber = assetNumber;
	this.TaxCode = taxCode;
	
    }

}
