package com.novabase.omni.oney.modules.referencedata.entity.dto;

public class ReferenceData {

    public String Description;
    public String Status;
    
    public ReferenceData() {
	super();
    }
    
    public ReferenceData(String description, String status) {
	
	super();
	
	this.Description = description;
	this.Status = status;
	
    }
    
}
