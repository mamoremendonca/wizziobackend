package com.novabase.omni.oney.modules.referencedata.entity.dto;

public class InternalOrder extends ReferenceData {

    public String Order; // Code?!?
    public String Department;
    public String Date; // "20200204"
    
    public InternalOrder() {
	super();
    }
    
    public InternalOrder(final String description, final String status) {
	super(description, status);
    }
    
    public InternalOrder(final String code, final String description, final String department, final String status) {
	
	super(description, status);
	
	this.Order = code;
	this.Department = department;
	
    }

}
