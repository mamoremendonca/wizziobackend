package com.novabase.omni.oney.modules.referencedata.entity.dto;

public class SupplierGeneralData extends Supplier {

    public String FiscalDesignator;
    public String SocialDesignator;
    public String MarketType;
    
    public SupplierGeneralData() {
	super();
    }
    
    public SupplierGeneralData(final String nIF, final String identificationNumber, final String name) {
	super(nIF, identificationNumber, name);
    }
    
    public SupplierGeneralData(
	    final String nIF, final String identificationNumber, final String name, final String fiscalDesignator, 
	    final String socialDesignator, final String marketType) {
	
	super(nIF, identificationNumber, name);
	
	this.FiscalDesignator = fiscalDesignator;
	this.SocialDesignator = socialDesignator;
	this.MarketType = marketType;
	
    }
        
}
