package com.novabase.omni.oney.modules.referencedata.entity.dto;

public class Tax extends ReferenceData {

    public String Date;
    public String TaxCode;
    public String Market;
    public String ValueIva;
    public String DocType;
    
    public Tax() {
	super();
    }
    
    public Tax(final String description, final String status) {
	super(description, status);
    }
    
    public Tax(final String code, final String description, final String value, final String market, final String docType, final String status) {
	
	super(description, status);
	
	this.TaxCode = code;
	this.Market = market;
	this.ValueIva = value;
	this.DocType = docType;
	
    }

}
