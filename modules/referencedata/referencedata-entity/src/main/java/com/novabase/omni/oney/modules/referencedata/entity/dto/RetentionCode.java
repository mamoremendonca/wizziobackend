package com.novabase.omni.oney.modules.referencedata.entity.dto;

public class RetentionCode extends ReferenceData {

    public String TaxCode;
    public String Taxpercentage;
    
    public RetentionCode() {
	super();
    }
    
    public RetentionCode(final String description, final String status) {
	super(description, status);
    }
    
    public RetentionCode(final String code, final String description, final String percentage, final String status) {
	
	super(description, status);
	
	this.TaxCode = code;
	this.Taxpercentage = percentage;
	
    }

}
