package com.novabase.omni.oney.modules.referencedata.entity.dto;

public class Supplier {

    public String NIF;
    public String IdentificationNumber;
    public String Name;
    
    public Supplier() {
	super();
    }
    
    public Supplier(final String nif, final String identificationNumber, final String name) {
	
	super();
	
	this.NIF = nif;
	this.IdentificationNumber = identificationNumber;
	this.Name = name;
	
    }
    
}
