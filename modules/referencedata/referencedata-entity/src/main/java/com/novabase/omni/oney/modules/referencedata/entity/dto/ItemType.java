package com.novabase.omni.oney.modules.referencedata.entity.dto;

public class ItemType extends ReferenceData {

    public String Materialtype;				// Code?!?
    public String DescriptionMaterialtype;		// Description
    //public String defaultDepartment;		// default Department
    
    public ItemType() {
	super();
    }
    
    public ItemType(final String description, final String status) {
	super(description, status);
    }
    
    public ItemType(final String code, final String description, final String status) {
	
	super(description, status);
	
	//this.defaultDepartment = department;
	this.Materialtype = code;
	this.DescriptionMaterialtype = description;
	
    }


}
