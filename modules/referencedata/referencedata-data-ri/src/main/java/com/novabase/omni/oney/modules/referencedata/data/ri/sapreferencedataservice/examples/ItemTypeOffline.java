package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.novabase.omni.oney.modules.referencedata.entity.dto.ItemType;

public enum ItemTypeOffline {

    //@formatter:off
    ITEMTYPE_1("ItemType1", "Item Type 1"),
    ITEMTYPE_2("ItemType2", "Item Type 2"),
    ITEMTYPE_3("ItemType3", "Item Type 3");
    //@formatter:on

    private String code;
    private String description;

    private ItemTypeOffline(String code, String description) {
	this.code = code;
	this.description = description;
    }

    //@formatter:off
    public static ItemTypeOffline of(String code){
	return Arrays.asList(ItemTypeOffline.values())
		.stream()
		.filter(d -> d.getCode().equals(code))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on

    public static List<ItemType> offlineValues() {
	return Arrays.asList(ItemTypeOffline.values()).stream().map(i -> new ItemType(i.getCode(), i.getDescription(), "1")).collect(Collectors.toList());
    }

    public String getCode() {
	return this.code;
    }

    public String getDescription() {
	return this.description;
    }

}
