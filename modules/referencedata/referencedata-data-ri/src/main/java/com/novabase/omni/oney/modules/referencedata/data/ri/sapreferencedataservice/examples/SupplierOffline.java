package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.novabase.omni.oney.modules.referencedata.entity.dto.Supplier;

public enum SupplierOffline {

    //@formatter:off
    SUPPLIER_1("SupplierCode1", "Supplier 1", "123456789"),
    SUPPLIER_2("SupplierCode2", "Supplier 2", "201304506"),
    SUPPLIER_3("SupplierCode3", "Supplier 3", "987654321");
    //@formatter:on

    private String identificationNumber;
    private String name;
    private String nif;
	
     private SupplierOffline(String identificationNumber, String name, String nif) {
	this.identificationNumber = identificationNumber;
	this.name = name;
	this.nif = nif;
    }

    //@formatter:off
    public static SupplierOffline of(String number){
	return Arrays.asList(SupplierOffline.values())
		.stream()
		.filter(d -> d.getIdentificationNumber().equals(number))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on

    public static List<Supplier> offlineValues() {
	return Arrays.asList(SupplierOffline.values())
    	    	.stream()
    	    	.map(i -> new Supplier(i.getNIF(), i.getIdentificationNumber(), i.getName()) )
    	    	.collect(Collectors.toList());
    }

    public String getIdentificationNumber() {
	return this.identificationNumber;
    }
    
    public String getName() {
	return this.name;
    }
    
    public String getNIF() {
	return this.nif;
    }
}
