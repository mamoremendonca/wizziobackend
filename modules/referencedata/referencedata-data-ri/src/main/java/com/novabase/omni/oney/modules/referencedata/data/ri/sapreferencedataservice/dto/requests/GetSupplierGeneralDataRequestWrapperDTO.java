package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.requests;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GetSupplierGeneralDataRequestWrapperDTO extends DTO {

    @JsonProperty("GetSupplierGeneralDataRequest")
    private GetSupplierGeneralDataRequestDTO getSupplierGeneralDataRequest;

    public GetSupplierGeneralDataRequestWrapperDTO() {
	
	super();
	
	this.getSupplierGeneralDataRequest = new GetSupplierGeneralDataRequestDTO();
	
    }

    public GetSupplierGeneralDataRequestDTO getGetSupplierGeneralDataRequest() {
        return getSupplierGeneralDataRequest;
    }

    public void setGetSupplierGeneralDataRequest(GetSupplierGeneralDataRequestDTO getSupplierGeneralDataRequest) {
        this.getSupplierGeneralDataRequest = getSupplierGeneralDataRequest;
    }
    
    
}
