package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice;

//import java.lang.annotation.Annotation;
//import java.util.Collections;
import java.util.List;

//import org.apache.cxf.jaxrs.client.WebClient;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;
//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.ResponseEntity;

//import com.fasterxml.jackson.databind.ObjectMapper;
import com.novabase.omni.oney.modules.referencedata.data.api.SAPReferenceDataServiceDAO;
import com.novabase.omni.oney.modules.referencedata.data.api.exception.UnexpectedSAPReferenceDataException;
import com.novabase.omni.oney.modules.referencedata.data.ri.WSContext;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.requests.GetSupplierGeneralDataRequestWrapperDTO;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.requests.SearchSuppliersRequestWrapperDTO;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.GetAllCostCentersResponseWrapperDTO;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.GetAllInternalOrdersResponseWrapperDTO;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.GetAllPOAssetsResponseWrapperDTO;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.GetAllPOItemsResponseWrapperDTO;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.GetDepartmentsResponseWrapperDTO;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.GetPOItemsTypesResponseWrapperDTO;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.GetSupplierGeneralDataResponseWrapperDTO;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.GetTaxCodesResponseWrapperDTO;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.GetWithholdingTaxCodesResponseWrapperDTO;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.SearchSuppliersResponseWrapperDTO;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples.AssetOffline;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples.CostCenterOffline;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples.DepartmentOffline;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples.InternalOrderOffline;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples.ItemOffline;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples.ItemTypeOffline;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples.RetentionCodeOffline;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples.SupplierGeneralDataOffline;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples.SupplierOffline;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples.TaxOffline;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Asset;
import com.novabase.omni.oney.modules.referencedata.entity.dto.CostCenter;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Department;
import com.novabase.omni.oney.modules.referencedata.entity.dto.InternalOrder;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Item;
import com.novabase.omni.oney.modules.referencedata.entity.dto.ItemType;
import com.novabase.omni.oney.modules.referencedata.entity.dto.RetentionCode;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Supplier;
import com.novabase.omni.oney.modules.referencedata.entity.dto.SupplierGeneralData;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Tax;

import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;
//import io.digitaljourney.platform.modules.ws.rs.api.dao.AbstractRSDAO;

// @formatter:off
@Component(
  configurationPid = SAPReferenceDataServiceDAOConfig.CPID,
  configurationPolicy = ConfigurationPolicy.REQUIRE,
  reference = {
    @Reference(
      name = RSProperties.REF_CONTEXT,
      service = WSContext.class, 
      cardinality = ReferenceCardinality.MANDATORY) })
@Designate(ocd = SAPReferenceDataServiceDAOConfig.class)
// @formatter:on
public final class SAPReferenceDataServiceDAOImpl extends AbstractSAPReferenceDAO<SAPReferenceDataServiceDAOConfig> implements SAPReferenceDataServiceDAO {
    
    @Activate
    public void activate(ComponentContext ctx, SAPReferenceDataServiceDAOConfig config) {
	prepare(ctx, config);
    }

    @Modified
    public void modified(SAPReferenceDataServiceDAOConfig config) {
	prepare(config);
    }

    @Override
    public List<Asset> getAssets() throws UnexpectedSAPReferenceDataException {

	if (getConfig().offlineMode()) {
	    warn("REFERENCEDATA: OFFLINEMODE IS ON, THIS OPTION MUST BE SET TO FALSE ON PRODUCTION ENVIROMENT");
	    return AssetOffline.offlineValues();
	} else {
	    return this.callGet(GetAllPOAssetsResponseWrapperDTO.class, this.getConfig().resourceGetAssets(), Boolean.TRUE).getResponse().getOrders();
	}
    }
    
    @Override
    public List<Item> getItems() throws UnexpectedSAPReferenceDataException {

	if (getConfig().offlineMode()) {
	    warn("REFERENCEDATA: OFFLINEMODE IS ON, THIS OPTION MUST BE SET TO FALSE ON PRODUCTION ENVIROMENT");
	    return ItemOffline.offlineValues();
	} else {
	    return this.callGet(GetAllPOItemsResponseWrapperDTO.class, this.getConfig().resourceGetItems(), Boolean.TRUE).getResponse().getOrders();	    
	}
    }
    
    @Override
    public List<ItemType> getItemTypes() throws UnexpectedSAPReferenceDataException {

	if (getConfig().offlineMode()) {
	    warn("REFERENCEDATA: OFFLINEMODE IS ON, THIS OPTION MUST BE SET TO FALSE ON PRODUCTION ENVIROMENT");
	    return ItemTypeOffline.offlineValues();
	} else {
	    return this.callGet(GetPOItemsTypesResponseWrapperDTO.class, this.getConfig().resourceGetItemTypes(), Boolean.TRUE).getResponse().getOrders();	    
	}
    }

    @Override
    public List<Tax> getTaxes() throws UnexpectedSAPReferenceDataException {

	if (getConfig().offlineMode()) {
	    warn("REFERENCEDATA: OFFLINEMODE IS ON, THIS OPTION MUST BE SET TO FALSE ON PRODUCTION ENVIROMENT");
	    return TaxOffline.offlineValues();
	} else {
	    return this.callGet(GetTaxCodesResponseWrapperDTO.class, this.getConfig().resourceGetTaxes(), Boolean.TRUE).getResponse().getOrders();	 
	}
    }

    @Override
    public List<CostCenter> getCostCenters() throws UnexpectedSAPReferenceDataException {

	if (getConfig().offlineMode()) {
	    warn("REFERENCEDATA: OFFLINEMODE IS ON, THIS OPTION MUST BE SET TO FALSE ON PRODUCTION ENVIROMENT");
	    return CostCenterOffline.offlineValues();
	} else {
	    return this.callGet(GetAllCostCentersResponseWrapperDTO.class, this.getConfig().resourceGetCostCenters(), Boolean.TRUE).getResponse().getOrders();
	}
    }

    @Override
    public List<InternalOrder> getInternalOrdens() throws UnexpectedSAPReferenceDataException {

	if (getConfig().offlineMode()) {
	    warn("REFERENCEDATA: OFFLINEMODE IS ON, THIS OPTION MUST BE SET TO FALSE ON PRODUCTION ENVIROMENT");
	    return InternalOrderOffline.offlineValues();
	} else {
	    return this.callGet(GetAllInternalOrdersResponseWrapperDTO.class, this.getConfig().resourceGetInternalOrders(), Boolean.TRUE).getResponse().getOrders();
	}
    }

    @Override
    public List<Department> getDepartments() throws UnexpectedSAPReferenceDataException {

	if (getConfig().offlineMode()) {
	    warn("REFERENCEDATA: OFFLINEMODE IS ON, THIS OPTION MUST BE SET TO FALSE ON PRODUCTION ENVIROMENT");
	    return DepartmentOffline.offlineValues();
	} else {
	    return this.callGet(GetDepartmentsResponseWrapperDTO.class, this.getConfig().resourceGetDepartments(), Boolean.TRUE).getResponse().getDepartments();
	}
    }

    @Override
    public List<RetentionCode> getRetentionCode() throws UnexpectedSAPReferenceDataException {

	if (getConfig().offlineMode()) {
	    warn("REFERENCEDATA: OFFLINEMODE IS ON, THIS OPTION MUST BE SET TO FALSE ON PRODUCTION ENVIROMENT");
	    return RetentionCodeOffline.offlineValues();
	} else {
	    return this.callGet(GetWithholdingTaxCodesResponseWrapperDTO.class, this.getConfig().resourceGetRetentionCode(), Boolean.TRUE).getResponse().getOrders();
	}
    }

    @Override
    public List<Supplier> getSuppliers(final Supplier searchFilter) throws UnexpectedSAPReferenceDataException {

	if (searchFilter == null) {
	    throw new UnexpectedSAPReferenceDataException("Document Identifier Cannot Be Null...");
	} else {
	    
	    // OFFLINE MODE ON
	    if (getConfig().offlineMode()) {
		warn("REFERENCEDATA: OFFLINEMODE IS ON, THIS OPTION MUST BE SET TO FALSE ON PRODUCTION ENVIROMENT");
		return SupplierOffline.offlineValues();
	    } else {
		
		final SearchSuppliersRequestWrapperDTO requestWrapper = new SearchSuppliersRequestWrapperDTO();
		requestWrapper.getSearchSuppliersRequest().setSuppliers(searchFilter);
		
		return this.callPost(requestWrapper, SearchSuppliersResponseWrapperDTO.class, this.getConfig().resourcePOSTSuppliers(), Boolean.TRUE).getResponse().getSuppliers();
		
	    }

	}
	
    }

    @Override
    public SupplierGeneralData getSupplierGeneralData(final String supplierCode) throws UnexpectedSAPReferenceDataException {
	
	if (supplierCode == null) {
	    throw new UnexpectedSAPReferenceDataException("Supplier NIF Cannot Be Null...");
	} else {
	    
	    // OFFLINE MODE ON
	    if (this.getConfig().offlineMode()) {
		warn("REFERENCEDATA: OFFLINEMODE IS ON, THIS OPTION MUST BE SET TO FALSE ON PRODUCTION ENVIROMENT");
		
		return SupplierGeneralDataOffline.offlineValues().parallelStream().
									filter(supplierGeneralData -> supplierGeneralData.IdentificationNumber.equals(supplierCode)).
									findFirst().
									orElse(null);
		
	    } else {

		final GetSupplierGeneralDataRequestWrapperDTO requestWrapper = new GetSupplierGeneralDataRequestWrapperDTO();
		requestWrapper.getGetSupplierGeneralDataRequest().getSupplierGeneralData().IdentificationNumber = supplierCode;
		
		return this.callPost(requestWrapper, GetSupplierGeneralDataResponseWrapperDTO.class, this.getConfig().resourcePOSTSuppliersGeneralData(), Boolean.TRUE).getResponse().getSupplierGeneralData();
		
	    }
	    
	}
	
    }
        
    private <T> T callGet(final Class<T> responseClass, final String path, final Boolean defaultSuccessCode) {

	return this.get(responseClass,path, new SAPReferenceDataServiceDAOConfigWrapper(this.getConfig()), 
		defaultSuccessCode); // TRUE to use WSData Default Success codes and Descriptions as this service does not return Success Data.
	
    }
    
    private <T, U> U callPost(final T request, final Class<U> responseClass, final String path, final Boolean defaultSuccessCode) {

	return this.post(request, responseClass, path, new SAPReferenceDataServiceDAOConfigWrapper(this.getConfig()), 
		defaultSuccessCode); // TRUE to use WSData Default Success codes and Descriptions as this service does not return Success Data.

    }

}
