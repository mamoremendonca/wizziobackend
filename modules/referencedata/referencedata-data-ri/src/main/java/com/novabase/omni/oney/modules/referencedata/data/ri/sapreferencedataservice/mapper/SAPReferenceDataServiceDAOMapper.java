package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.http.HttpStatus;

import io.digitaljourney.platform.modules.invocation.api.model.type.OutcomeType;
import io.digitaljourney.platform.modules.invocation.api.model.type.StatusType;

@Mapper
public abstract class SAPReferenceDataServiceDAOMapper {

    public static final SAPReferenceDataServiceDAOMapper INSTANCE = Mappers.getMapper(SAPReferenceDataServiceDAOMapper.class);

    public OutcomeType toOutcomeType(HttpStatus statusCode) {
	// @formatter:off
	return (statusCode.is2xxSuccessful() ? 
		OutcomeType.SUCCESS : 
	    	(statusCode.is1xxInformational() || statusCode.is3xxRedirection() ? 
	    		OutcomeType.UNKNOWN : 
	    		OutcomeType.ERROR));
	// @formatter:on
    }

    public StatusType toStatusType(HttpStatus statusCode) {
	// @formatter:off
	return (statusCode.is2xxSuccessful() ? 
		StatusType.SUCCESS: 
		(statusCode.is1xxInformational() || statusCode.is3xxRedirection() ? 
			StatusType.UNKNOWN : 
			StatusType.ERROR));
	// @formatter:on
    }
    
}
