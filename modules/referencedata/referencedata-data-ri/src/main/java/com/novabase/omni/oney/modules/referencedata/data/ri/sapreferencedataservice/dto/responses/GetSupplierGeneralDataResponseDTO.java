package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base.BaseResponseDTO;
import com.novabase.omni.oney.modules.referencedata.entity.dto.SupplierGeneralData;

public class GetSupplierGeneralDataResponseDTO extends BaseResponseDTO {

    @JsonProperty("SupplierGeneralData")
    private SupplierGeneralData supplierGeneralData;
    


    public GetSupplierGeneralDataResponseDTO() {
	
	super();
	
	this.supplierGeneralData = new SupplierGeneralData();
	
    }

    public SupplierGeneralData getSupplierGeneralData() {
        return supplierGeneralData;
    }

    public void setSupplierGeneralData(SupplierGeneralData supplierGeneralData) {
        this.supplierGeneralData = supplierGeneralData;
    }
    
    
}
