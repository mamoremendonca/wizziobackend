package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses;

import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base.DepartmentsBaseResponseDTO;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Department;

public class GetDepartmentsResponseDTO extends DepartmentsBaseResponseDTO<Department> {

}
