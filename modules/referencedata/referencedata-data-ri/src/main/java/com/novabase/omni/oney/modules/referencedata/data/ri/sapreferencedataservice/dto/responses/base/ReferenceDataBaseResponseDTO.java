package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReferenceDataBaseResponseDTO<T> extends BaseResponseDTO {

    @JsonProperty("Orders")
    private List<T> orders;

    public ReferenceDataBaseResponseDTO() {

	super();
	
	this.orders = new ArrayList<T>();
	
    }

    public List<T> getOrders() {
        return orders;
    }

    public void setOrders(List<T> orders) {
        this.orders = orders;
    }
    
}
