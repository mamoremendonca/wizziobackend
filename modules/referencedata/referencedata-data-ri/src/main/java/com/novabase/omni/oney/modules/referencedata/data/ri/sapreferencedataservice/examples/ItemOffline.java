package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.novabase.omni.oney.modules.referencedata.entity.dto.Item;

public enum ItemOffline {

    //@formatter:off
    ITEM_1("Item1", "Item 1", "", "ItemType1", "CostCenter1", "TaxCode1"),
    ITEM_2("Item2", "Item 2", "", "ItemType1", "CostCenter1", "TaxCode1"),
    ITEM_3("Item3", "Item 3", "", "ItemType1", "CostCenter2", "TaxCode1"),
    ITEM_4("Item4", "Item 4", "", "ItemType2", "", "TaxCode2"),
    ITEM_5("Item5", "Item 5", "", "ItemType2", "", "TaxCode2"),
    ITEM_6("Item6", "Item 6", "", "ItemType2", "", "TaxCode2"),
    ITEM_7("Item7", "Item 7", "DAF-1100", "ItemType2", "CostCenter1", "TaxCode2"),
    ITEM_8("Item8", "Item 8", "DAF-1100", "ItemType1", "CostCenter1", "TaxCode2");
    
    
    
    
    //@formatter:on

    private String code;
    private String description;
    private String department;
    private String typeCode;
    private String defaultCostCenterCode;
    private String defaultTaxCode;

     private ItemOffline(String code, String description, String department, String type, String costCenter, String tax) {
	this.code = code;
	this.description = description;
	this.department = department;
	this.typeCode = type;
	this.defaultCostCenterCode = costCenter;
	this.defaultTaxCode = tax;
    }

    //@formatter:off
    public static ItemOffline of(String code){
	return Arrays.asList(ItemOffline.values())
		.stream()
		.filter(d -> d.getCode().equals(code))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on

    public static List<Item> offlineValues() {
	return Arrays.asList(ItemOffline.values())
    	    	.stream()
    	    	.map(i -> new Item(i.getCode(), i.getDescription(), "", "", i.getDefaultCostCenterCode(), i.getDefaultTaxCode(), i.getTypeCode(), "1", i.getTypeCode(), "", "", i.department) )
    	    	.collect(Collectors.toList());
    }

    public String getCode() {
	return this.code;
    }
    
    public String getDescription() {
	return this.description;
    }
    
    public String getDepartment() {
	return this.department;
    }
    
    public String getTypeCode() {
	return this.typeCode;
    }
    
    public String getDefaultCostCenterCode() {
	return this.defaultCostCenterCode;
    }
    public String getDefaultTaxCode() {
	return this.defaultTaxCode;
    }    
}
