package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base.ResponseWrapper;

public class GetTaxCodesResponseWrapperDTO extends DTO implements ResponseWrapper<GetTaxCodesResponseDTO> {

    @JsonProperty("GetTaxCodesResponse")
    private GetTaxCodesResponseDTO getTaxCodesResponseDTO;
    
    public GetTaxCodesResponseWrapperDTO() {
	
	super();
	
	this.getTaxCodesResponseDTO = new GetTaxCodesResponseDTO();
	
    }

    public GetTaxCodesResponseDTO getGetTaxCodesResponseDTO() {
        return getTaxCodesResponseDTO;
    }

    public void setGetTaxCodesResponseDTO(GetTaxCodesResponseDTO getTaxCodesResponseDTO) {
        this.getTaxCodesResponseDTO = getTaxCodesResponseDTO;
    }

    @Override
    public GetTaxCodesResponseDTO getResponse() {
	return this.getTaxCodesResponseDTO;
    }

}
