package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.requests;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SearchSuppliersRequestWrapperDTO extends DTO {

    @JsonProperty("SearchSuppliersRequest")
    private SearchSuppliersRequestDTO searchSuppliersRequest;

    public SearchSuppliersRequestWrapperDTO() {
	
	super();
	
	this.searchSuppliersRequest = new SearchSuppliersRequestDTO();
	
    }

    public SearchSuppliersRequestDTO getSearchSuppliersRequest() {
        return searchSuppliersRequest;
    }

    public void setSearchSuppliersRequest(SearchSuppliersRequestDTO searchSuppliersRequest) {
        this.searchSuppliersRequest = searchSuppliersRequest;
    }
    
}
