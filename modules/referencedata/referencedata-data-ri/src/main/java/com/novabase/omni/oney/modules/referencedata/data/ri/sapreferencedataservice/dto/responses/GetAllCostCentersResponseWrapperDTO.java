package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base.ResponseWrapper;

public class GetAllCostCentersResponseWrapperDTO extends DTO implements ResponseWrapper<GetAllCostCentersResponseDTO> {

    @JsonProperty("GetAllCostCentersResponse")
    private GetAllCostCentersResponseDTO getAllCostCentersResponseDTO;
    
    public GetAllCostCentersResponseWrapperDTO() {
	
	super();
	
	this.getAllCostCentersResponseDTO = new GetAllCostCentersResponseDTO();
	
    }

    public GetAllCostCentersResponseDTO getGetAllCostCentersResponseDTO() {
        return getAllCostCentersResponseDTO;
    }

    public void setGetAllCostCentersResponseDTO(GetAllCostCentersResponseDTO getAllCostCentersResponseDTO) {
        this.getAllCostCentersResponseDTO = getAllCostCentersResponseDTO;
    }

    @Override
    public GetAllCostCentersResponseDTO getResponse() {
	return this.getAllCostCentersResponseDTO;
    }

}
