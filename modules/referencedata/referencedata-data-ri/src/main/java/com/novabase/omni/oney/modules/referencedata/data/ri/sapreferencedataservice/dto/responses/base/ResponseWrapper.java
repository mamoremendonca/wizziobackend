package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base;

public interface ResponseWrapper<T extends BaseResponseDTO> {

    public T getResponse();
    
}
