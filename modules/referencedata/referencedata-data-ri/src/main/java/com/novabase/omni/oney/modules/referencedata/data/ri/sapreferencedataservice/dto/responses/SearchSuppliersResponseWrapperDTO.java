package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base.ResponseWrapper;

public class SearchSuppliersResponseWrapperDTO extends DTO implements ResponseWrapper<SearchSuppliersResponseDTO> {

    @JsonProperty("SearchSuppliersResponse")
    private SearchSuppliersResponseDTO searchSuppliersResponseDTO;

    public SearchSuppliersResponseWrapperDTO() {
	
	super();
	
	this.searchSuppliersResponseDTO = new SearchSuppliersResponseDTO();
	
    }

    public SearchSuppliersResponseDTO getSearchSuppliersResponseDTO() {
        return searchSuppliersResponseDTO;
    }

    public void setSearchSuppliersResponseDTO(SearchSuppliersResponseDTO searchSuppliersResponseDTO) {
        this.searchSuppliersResponseDTO = searchSuppliersResponseDTO;
    }

    @Override
    public SearchSuppliersResponseDTO getResponse() {
	return this.searchSuppliersResponseDTO;
    }

}
