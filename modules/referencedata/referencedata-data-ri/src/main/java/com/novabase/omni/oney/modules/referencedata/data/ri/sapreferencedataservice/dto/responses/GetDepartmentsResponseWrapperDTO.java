package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base.ResponseWrapper;

public class GetDepartmentsResponseWrapperDTO extends DTO implements ResponseWrapper<GetDepartmentsResponseDTO> {

    @JsonProperty("GetDepartmentsResponse")
    private GetDepartmentsResponseDTO getDepartmentsResponseDTO;
    
    public GetDepartmentsResponseWrapperDTO() {
	
	super();
	
	this.getDepartmentsResponseDTO = new GetDepartmentsResponseDTO();
	
    }

    public GetDepartmentsResponseDTO getGetDepartmentsResponseDTO() {
        return getDepartmentsResponseDTO;
    }

    public void setGetDepartmentsResponseDTO(GetDepartmentsResponseDTO getDepartmentsResponseDTO) {
        this.getDepartmentsResponseDTO = getDepartmentsResponseDTO;
    }

    @Override
    public GetDepartmentsResponseDTO getResponse() {
	return this.getDepartmentsResponseDTO;
    }

}
