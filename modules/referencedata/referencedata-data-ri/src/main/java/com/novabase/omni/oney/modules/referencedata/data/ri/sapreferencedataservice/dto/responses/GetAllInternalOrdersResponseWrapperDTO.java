package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base.ResponseWrapper;

public class GetAllInternalOrdersResponseWrapperDTO extends DTO implements ResponseWrapper<GetAllInternalOrdersResponseDTO> {

    @JsonProperty("GetAllInternalOrdersResponse")
    private GetAllInternalOrdersResponseDTO getAllInternalOrdersResponseDTO;
    
    public GetAllInternalOrdersResponseWrapperDTO() {
	
	super();
	
	this.getAllInternalOrdersResponseDTO = new GetAllInternalOrdersResponseDTO();
	
    }

    public GetAllInternalOrdersResponseDTO getGetAllInternalOrdersResponseDTO() {
        return getAllInternalOrdersResponseDTO;
    }

    public void setGetAllInternalOrdersResponseDTO(GetAllInternalOrdersResponseDTO getAllInternalOrdersResponseDTO) {
        this.getAllInternalOrdersResponseDTO = getAllInternalOrdersResponseDTO;
    }

    @Override
    public GetAllInternalOrdersResponseDTO getResponse() {
	return this.getAllInternalOrdersResponseDTO;
    }

}
