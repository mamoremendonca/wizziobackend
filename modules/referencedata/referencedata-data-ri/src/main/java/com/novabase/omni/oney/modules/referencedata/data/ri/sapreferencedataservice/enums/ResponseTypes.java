package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.enums;

public enum ResponseTypes {
    GetAllCostCentersResponse, 
    GetAllInternalOrdersResponse, 
    GetAllPOAssetsResponse, 
    GetAllPOItemsResponse, 
    GetTaxCodesResponse, 
    GetWithholdingTaxCodesResponse,
    GetPOItemsTypesResponse,
    GetDepartmentsResponse
}
