package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses;

import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base.ReferenceDataBaseResponseDTO;
import com.novabase.omni.oney.modules.referencedata.entity.dto.CostCenter;

public class GetAllCostCentersResponseDTO extends ReferenceDataBaseResponseDTO<CostCenter> {

}
