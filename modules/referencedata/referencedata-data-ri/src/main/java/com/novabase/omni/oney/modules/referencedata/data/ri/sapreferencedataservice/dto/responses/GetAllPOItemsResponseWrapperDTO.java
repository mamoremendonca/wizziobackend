package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base.ResponseWrapper;

public class GetAllPOItemsResponseWrapperDTO extends DTO implements ResponseWrapper<GetAllPOItemsResponseDTO> {

    @JsonProperty("GetAllPOItemsResponse")
    private GetAllPOItemsResponseDTO getAllPOItemsResponseDTO;
    
    public GetAllPOItemsResponseWrapperDTO() {
	
	super();
	
	this.getAllPOItemsResponseDTO = new GetAllPOItemsResponseDTO();
	
    }


    public GetAllPOItemsResponseDTO getGetAllPOItemsResponseDTO() {
        return getAllPOItemsResponseDTO;
    }


    public void setGetAllPOItemsResponseDTO(GetAllPOItemsResponseDTO getAllPOItemsResponseDTO) {
        this.getAllPOItemsResponseDTO = getAllPOItemsResponseDTO;
    }


    @Override
    public GetAllPOItemsResponseDTO getResponse() {
	return this.getAllPOItemsResponseDTO;
    }

}
