package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.novabase.omni.oney.modules.referencedata.entity.dto.Department;

public enum DepartmentOffline {

    //@formatter:off
    DAF_CONTABILIDADE("DAF", "DAF-1100", "DAF - Contabilidade e Controlo Financeiro", "CostCenter1"),
    DAF_GESTAO("DAF", "DAF-1200", "DAF - Controlo de Gestão", ""),
    DAF_JURIDICO("DAF", "DAF-1500", "DAF - Jurídico & Compliance", ""),
    DAF_LOGISTICA("DAF", "DAF-1400", "DAF - Logística", "CostCenter3"),
    DAF_DPO("DAF", "DR-6400", "DAF - DPO", ""),
    DAF_TESOURARIA("DAF", "DAF-1300", "DAF - Tesouraria", ""),
    DC_ADVISER("DC", "DC-2100", "DC - Customer Adviser ", ""),
    DC_CARE("DC", "DC-2200", "DC - Customer Care", ""),
    DC_LITIGATION("DC", "DC-2300", "DC - Recoveries & Litigation", ""),
    DC_QUALITY("DC", "DC-2400", "DC - Business Quality", ""),
    DSI_DATA("DSI", "DSI-7100", "DSI - Data Management", ""),
    DSI_PROJECT("DSI", "DSI-7200", "DSI - Project & Service Management", ""),
    DSI_DEMAND("DSI", "DSI-7300", "DSI - Demand Management", ""),
    DSI_IT_OPERATIONS("DSI", "DSI-7400", "DSI - IT Operations", ""),
    DSI_IT_PROCUREMENT("DSI", "DSI-7500", "DSI - IT Procurement", ""),
    DSI_SEGURANCA("DSI", "DSI-7600", "DSI - Segurança de Sistemas de Informação", ""),
    DP_REDE_AUCHAN("DP", "DRP-5500", "DP - Rede Auchan", ""),
    DP_REDES_EXPANSAO("DP", "DRP-5700", "DP - Redes em Expansão", ""),
    DP_B2B("DP", "DEV-8100", "DP- Soluções B2B", ""),
    DP_REDE_LEROY_MERLIN("DP", "DRP-5600", "DP - Rede Leroy Merlin", "");
//    DP_PROSPEPCAO("DP", "DP - Prospepção", ""),
//    DDCA_COMUNICACAO("DDCA", "DDCA - Formação & Comunicação Interna", ""),
//    DDCA_GCDO("DDCA", "DDCA - GCDO", ""),
//    DMD_DIGITAL("DMD", "DMD - Digital & Customer Experience", ""),
//    DMD_ONEY("DMD", "DMD - 3x 4x Oney", ""),
//    DMD_MARKETING_MARCA("DMD", "DMD - Marketing Operacional & Marca", ""),
//    DMD_MARKETING_CRM("DMD", "DMD - Marketing Estratégico & CRM", ""),
//    DR_OPERATIONAL("DR", "DR - Internal Control and Operacional Risk", ""),
//    DR_MODELING("DR", "DR - Risk Modeling", ""),
//    DR_PROVISIONS("DR", "DR - Provisions and Basel", ""),
//    DR_CREDIT("DR", "DR - Credit Risk", "");
    //@formatter:on

    private String area;
    private String code;
    private String department;
    private String defaultCostCenter;

    private DepartmentOffline(String area, String code, String department, String costCenter) {
	this.area = area;
	this.code = code;
	this.department = department;
	this.defaultCostCenter = costCenter;
    }

    //@formatter:off
    public static DepartmentOffline of(String department){
	return Arrays.asList(DepartmentOffline.values())
		.stream()
		.filter(d -> d.getDepartment().equals(department))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on

    public static List<Department> offlineValues() {
	return Arrays.asList(DepartmentOffline.values()).stream().map(i -> new Department(i.getCode(), i.getDepartment(), "1")).collect(Collectors.toList());
    }

    public String getArea() {
	return this.area;
    }

    public String getDepartment() {
	return this.department;
    }

    public String getDefaultCostCenter() {
	return this.defaultCostCenter;
    }

    public String getCode() {
        return code;
    }
    
    
}
