package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base.ResponseWrapper;

public class GetWithholdingTaxCodesResponseWrapperDTO extends DTO implements ResponseWrapper<GetWithholdingTaxCodesResponseDTO> {

    @JsonProperty("GetWithholdingTaxCodesResponse")
    private GetWithholdingTaxCodesResponseDTO getWithholdingTaxCodesResponseDTO;
    
    public GetWithholdingTaxCodesResponseWrapperDTO() {
	
	super();
	
	this.getWithholdingTaxCodesResponseDTO = new GetWithholdingTaxCodesResponseDTO();
	
    }
    
    public GetWithholdingTaxCodesResponseDTO getGetWithholdingTaxCodesResponseDTO() {
        return getWithholdingTaxCodesResponseDTO;
    }

    public void setGetWithholdingTaxCodesResponseDTO(GetWithholdingTaxCodesResponseDTO getWithholdingTaxCodesResponseDTO) {
        this.getWithholdingTaxCodesResponseDTO = getWithholdingTaxCodesResponseDTO;
    }

    @Override
    public GetWithholdingTaxCodesResponseDTO getResponse() {
	return this.getWithholdingTaxCodesResponseDTO;
    }

}
