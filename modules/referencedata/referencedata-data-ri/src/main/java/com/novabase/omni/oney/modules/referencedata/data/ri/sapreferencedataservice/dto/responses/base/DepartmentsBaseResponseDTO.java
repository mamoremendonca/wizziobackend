package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * 
 * @author NB24430
 *
 * @param <T>
 * 
 * this class is identifical to ReferenceDataBaseResponseDTO because it has the same structure but the list aggregator changes name to Departments
 */
public class DepartmentsBaseResponseDTO<T> extends BaseResponseDTO {

    @JsonProperty("Departments")
    private List<T> departments;

    public DepartmentsBaseResponseDTO() {

	super();
	
	this.departments = new ArrayList<T>();
	
    }

    public List<T> getDepartments() {
        return departments;
    }

    public void setDepartments(List<T> departments) {
        this.departments = departments;
    }

    
}
