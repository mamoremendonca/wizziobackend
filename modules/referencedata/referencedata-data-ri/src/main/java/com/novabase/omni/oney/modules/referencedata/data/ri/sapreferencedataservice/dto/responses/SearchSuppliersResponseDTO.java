package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base.BaseResponseDTO;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Supplier;

public class SearchSuppliersResponseDTO extends BaseResponseDTO {

    @JsonProperty("Suppliers")
    private List<Supplier> suppliers;
    
    public SearchSuppliersResponseDTO() {
	
	super();
	
	this.suppliers = new ArrayList<Supplier>();
	
    }

    public List<Supplier> getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(List<Supplier> suppliers) {
        this.suppliers = suppliers;
    }

}
