package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReferenceDataResultErrors {

    @JsonProperty("Error")
    ReferenceDataResultError error;

    public ReferenceDataResultErrors() {}

    public ReferenceDataResultErrors(ReferenceDataResultError error) {
	this.error = error;
    }
    
}
