package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.novabase.omni.oney.modules.referencedata.entity.dto.Asset;

public enum AssetOffline {

    //@formatter:off
    ASSET_1("Asset1", "Asset 1", "", "TaxCode1"),
    ASSET_2("Asset2", "Asset 2", "", "TaxCode1"),
    ASSET_3("Asset3", "Asset 3", "", "TaxCode1"),
    ASSET_4("Asset4", "Asset 4", "", "TaxCode2"),
    ASSET_5("Asset5", "Asset 5", "", "TaxCode2"),
    ASSET_6("Asset6", "Asset 6", "", "TaxCode2");
    //@formatter:on

    private String code;
    private String description;
    private String department;
    private String taxCode;

     private AssetOffline(String code, String description, String department, String tax) {
	this.code = code;
	this.description = description;
	this.department = department;
	this.taxCode = tax;
    }

    //@formatter:off
    public static AssetOffline of(String code){
	return Arrays.asList(AssetOffline.values())
		.stream()
		.filter(d -> d.getCode().equals(code))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on

    public static List<Asset> offlineValues() {
	return Arrays.asList(AssetOffline.values())
    	    	.stream()
    	    	.map(i -> new Asset(i.getCode(), i.getDescription(), "1", i.getTaxCode()) )
    	    	.collect(Collectors.toList());
    }

    public String getCode() {
	return this.code;
    }
    
    public String getDescription() {
	return this.description;
    }
    
    public String getDepartment() {
	return this.department;
    }
    
    public String getTaxCode() {
	return this.taxCode;
    }
}
