package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base.ResponseWrapper;

public class GetSupplierGeneralDataResponseWrapperDTO extends DTO implements ResponseWrapper<GetSupplierGeneralDataResponseDTO>{

    @JsonProperty("GetSupplierGeneralDataResponse")
    private GetSupplierGeneralDataResponseDTO getSupplierGeneralDataResponseDTO;

    public GetSupplierGeneralDataResponseWrapperDTO() {
	
	super();
	
	this.getSupplierGeneralDataResponseDTO = new GetSupplierGeneralDataResponseDTO();
	
    }

    public GetSupplierGeneralDataResponseDTO getGetSupplierGeneralDataResponseDTO() {
        return getSupplierGeneralDataResponseDTO;
    }

    public void setGetSupplierGeneralDataResponseDTO(GetSupplierGeneralDataResponseDTO getSupplierGeneralDataResponseDTO) {
        this.getSupplierGeneralDataResponseDTO = getSupplierGeneralDataResponseDTO;
    }

    @Override
    public GetSupplierGeneralDataResponseDTO getResponse() {
	return this.getSupplierGeneralDataResponseDTO;
    }
    
}
