package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.requests;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.referencedata.entity.dto.SupplierGeneralData;

public class GetSupplierGeneralDataRequestDTO extends DTO {

    @JsonProperty("GetSupplierGeneralData")
    private SupplierGeneralData supplierGeneralData;

    public GetSupplierGeneralDataRequestDTO() {
	
	super();
	
	this.supplierGeneralData = new SupplierGeneralData();
	
    }

    public SupplierGeneralData getSupplierGeneralData() {
        return supplierGeneralData;
    }

    public void setSupplierGeneralData(SupplierGeneralData supplierGeneralData) {
        this.supplierGeneralData = supplierGeneralData;
    }
    
}
