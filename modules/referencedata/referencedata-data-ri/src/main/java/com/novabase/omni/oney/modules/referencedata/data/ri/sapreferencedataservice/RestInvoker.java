package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.novabase.omni.oney.modules.referencedata.data.api.exception.UnexpectedSAPReferenceDataException;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.ReferenceDataResponse;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.mapper.SAPReferenceDataServiceDAOMapper;
import com.novabase.omni.oney.modules.referencedata.entity.dto.ReferenceData;

import io.digitaljourney.platform.modules.invocation.api.PlatformInvocationManager;
import io.digitaljourney.platform.modules.invocation.api.model.HttpLogBuilder;
import io.digitaljourney.platform.modules.invocation.api.model.RestLogBuilder;
import io.digitaljourney.platform.modules.invocation.api.model.type.HttpMethodType;
import io.digitaljourney.platform.modules.invocation.api.model.type.OutcomeType;
import io.digitaljourney.platform.modules.invocation.api.model.type.StatusType;

//public class RestInvoker<T extends ReferenceData, R extends ReferenceDataResponse<T>> {
public class RestInvoker<T extends ReferenceData> {

    private static final String CLIENT_APPLICATION = "WIZZIO";
    private static final String CLIENT_CHANNEL = "WEB";

    PlatformInvocationManager invocationManager;

    public RestInvoker(PlatformInvocationManager platformInvocationManager) {
	this.invocationManager = platformInvocationManager;
    }

    public List<T> retrieveReference(String address, String resourcePath) throws UnexpectedSAPReferenceDataException {
	return retrieveReference(address, resourcePath, null);
    }
    
    public List<T> retrieveReference(String address, String resourcePath, String requestBody) throws UnexpectedSAPReferenceDataException {

	String responseJson = "";
	List<T> list = new ArrayList<T>();

	ClientBuilder builder = new ClientBuilder();
	RestTemplate client = builder.withApplication(CLIENT_APPLICATION).withChannel(CLIENT_CHANNEL).build(address);

	try {
	    // Generating random UUID for Invocation
	    // TODO: if retry functionality is needed, we must save identifier and resend it on next call
	    //ResponseEntity<R> rsp = client.exchange(resourcePath, HttpMethod.GET, builder.buildHeaders(UUID.randomUUID().toString()), new ParameterizedTypeReference<R>() {});
	    ResponseEntity<ReferenceDataResponse<T>> rsp = 
		    (StringUtils.isEmpty(requestBody) ?
			    client.exchange(resourcePath, HttpMethod.GET, builder.buildHeaders("", UUID.randomUUID().toString()), new ParameterizedTypeReference<ReferenceDataResponse<T>>() {}) :
			    client.exchange(resourcePath, HttpMethod.POST, builder.buildHeaders(requestBody, UUID.randomUUID().toString()), new ParameterizedTypeReference<ReferenceDataResponse<T>>() {})
			    );

	    responseJson = new ObjectMapper().writeValueAsString(rsp.getBody());

	    if (rsp != null && rsp.hasBody()) {
		ReferenceDataResponse<T> response = rsp.getBody();
		if (response != null) {
		    if (!response.getSuccess()) {
			logRest(address, resourcePath, "", responseJson, rsp.getStatusCode());
			throw new UnexpectedSAPReferenceDataException(response.getErrorMessage());
		    } else {
			logRest(address, resourcePath, "", responseJson, HttpStatus.OK);
			list = response.getList();
		    }
		} else {
		    logRest(address, resourcePath, "", responseJson, rsp.getStatusCode());
		    throw new UnexpectedSAPReferenceDataException("Response from Service [" + resourcePath + "] has no body");
		}
	    } else {
		logRest(address, resourcePath, "", responseJson, rsp.getStatusCode());
		throw new UnexpectedSAPReferenceDataException("Response from Service [" + resourcePath + "] has no body");
	    }

	} catch (UnexpectedSAPReferenceDataException ex) {
	    throw ex;
	} catch (RestClientException e) {
	    logRest(address, resourcePath, "", responseJson, HttpStatus.REQUEST_TIMEOUT);
	    throw new UnexpectedSAPReferenceDataException("Error acessing REST Service [" + resourcePath + "].\n" + e.getMessage());
	} catch (Throwable t) {
	    logRest(address, resourcePath, "", responseJson, HttpStatus.BAD_REQUEST);
	    throw new UnexpectedSAPReferenceDataException("Error acessing REST Service [" + resourcePath + "].\n" + t.getMessage());
	}
	
	return list;
    }

    private void logRest(String address, String resource, String jsonOut, String jsonIn, HttpStatus statusCode) {
	// @formatter:off
	this.invocationManager.logRest(
		new RestLogBuilder()
			.withJson_out(jsonOut)
			.withJson_in(jsonIn)
			.withName(resource)
			.withResource(resource)
			.withOutcome(statusCode != null ? SAPReferenceDataServiceDAOMapper.INSTANCE.toOutcomeType(statusCode) : OutcomeType.UNKNOWN)
			.build(),
		new HttpLogBuilder()
			.withHost(address)
			.withMethod(HttpMethodType.GET)
			.withRequest_headers("value")
			.withUrl(address + resource)
			.build(), 
		null, 
		(statusCode != null ? SAPReferenceDataServiceDAOMapper.INSTANCE.toStatusType(statusCode) : StatusType.UNKNOWN));
	// @formatter:on
    }
}
