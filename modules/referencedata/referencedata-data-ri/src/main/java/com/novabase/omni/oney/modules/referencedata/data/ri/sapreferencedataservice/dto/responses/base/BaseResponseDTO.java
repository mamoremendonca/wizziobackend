package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseResponseDTO extends DTO {

    @JsonProperty("Errors")
    private ErrorsDTO errors;

    @JsonProperty("ErrorMsg")
    private String errorMsg;
    
    @JsonProperty("ErrorCode")
    private String errorCode;
    
    @JsonProperty("Success")
    private Boolean success;

    public BaseResponseDTO() {

	super();

	this.success = Boolean.FALSE;
	this.errors = new ErrorsDTO();

    }

    public ErrorsDTO getErrors() {
        return errors;
    }

    public void setErrors(ErrorsDTO errors) {
        this.errors = errors;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
