package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.novabase.omni.oney.modules.referencedata.entity.dto.RetentionCode;

public enum RetentionCodeOffline {

    //@formatter:off
    RETENTIONCODE_1("RetentionCode1", "Retention Code 1", "1.000"),
    RETENTIONCODE_2("RetentionCode2", "Retention Code 2", "2.000"),
    RETENTIONCODE_3("RetentionCode3", "Retention Code 3", "3.000");
    //@formatter:on

    private String code;
    private String description;
    private String value;
	
     private RetentionCodeOffline(String code, String description, String value) {
	this.code = code;
	this.description = description;
	this.value = value;
    }

    //@formatter:off
    public static RetentionCodeOffline of(String code){
	return Arrays.asList(RetentionCodeOffline.values())
		.stream()
		.filter(d -> d.getCode().equals(code))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on

    public static List<RetentionCode> offlineValues() {
	return Arrays.asList(RetentionCodeOffline.values())
    	    	.stream()
    	    	.map(i -> new RetentionCode(i.getCode(), i.getDescription(), i.getValue(), "1") )
    	    	.collect(Collectors.toList());
    }

    public String getCode() {
	return this.code;
    }
    
    public String getDescription() {
	return this.description;
    }
    
    public String getValue() {
	return this.value;
    }
}
