package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.deserializers.ReferenceDataDeserializer;
import com.novabase.omni.oney.modules.referencedata.entity.dto.ReferenceData;

@JsonDeserialize(using = ReferenceDataDeserializer.class)
public class ReferenceDataResponse <T extends ReferenceData> {

    @JsonProperty("Success")
    private Boolean success;
    @JsonProperty("Orders")
    private List<T> list;			// TODO: EAI da Oney retornada todas as listas com nome da lista = orders. Caso seja alterado, será necessário separar esta classe nas variantes por tipo de dados de referencia.
    @JsonProperty("Errors")
    private ReferenceDataResultErrors errors;

    public ReferenceDataResponse() {}

    public ReferenceDataResponse(boolean success, List<T> list, ReferenceDataResultErrors errors) {
	this.success = success;
	this.list = list;
	this.errors = errors;
    }
    
    @JsonGetter(value = "Success")
    public boolean getSuccess() {
	return (success != null ? success : false);
    }

    @JsonSetter("Success")
    public void setSuccess(boolean value) {
	this.success= value;
    }
    
    @JsonGetter("Orders")
    public List<T> getList() {
	return list;
    }

    @JsonSetter("Orders")
    public void setList(List<T> value) {
	this.list= value;
    }
    
    @JsonGetter("Errors")
    public ReferenceDataResultErrors getErrors() {
        return errors;
    }

    @JsonSetter("Errors")
    public void setErrors(ReferenceDataResultErrors value) {
        this.errors = value;
    }
    
    public String getErrorMessage() {
	return (errors != null & errors.error != null ? errors.error.code + " - " + errors.error.description : "");
//		errors.stream()
//		.map(e -> e.Code + " - " + e.Description)
//		.collect(Collectors.joining("\n")) : "");
//		//.reduce("\n\r", String::concat) : "");
    }
}
