package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.novabase.omni.oney.modules.referencedata.entity.dto.SupplierGeneralData;

public enum SupplierGeneralDataOffline {

    //@formatter:off
    SUPPLIERGENERALDATA_1("123456789", "SupplierCode1", "Supplier 1", "Supplier Fiscal Designator 1", "Supplier Social Designator 1", "N"),
    SUPPLIERGENERALDATA_2("201304506", "SupplierCode2", "Supplier 2", "Supplier Fiscal Designator 2", "Supplier Social Designator 2", "N"),
    SUPPLIERGENERALDATA_3("987654321", "SupplierCode3", "Supplier 3", "Supplier Fiscal Designator 3", "Supplier Social Designator 3", "I");
    //@formatter:on

    private String nif;
    private String identificationNumber;
    private String name;
    private String fiscalDesignator;
    private String socialDesignator;
    private String marketType;
    
    private SupplierGeneralDataOffline (
	    final String nIF, final String identificationNumber, final String name, final String fiscalDesignator, final String socialDesignator, final String marketType) {

	this.nif = nIF;
	this.identificationNumber = identificationNumber;
	this.name = name;
	this.fiscalDesignator = fiscalDesignator;
	this.socialDesignator = socialDesignator;
	this.marketType = marketType;
	
   }
    
    //@formatter:off
    public static SupplierGeneralDataOffline of(final String number){
	return Arrays.asList(SupplierGeneralDataOffline.values())
		.stream()
		.filter(d -> d.getIdentificationNumber().equals(number))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on
    
    public static SupplierGeneralData offlineValue() {
	return SupplierGeneralDataOffline.offlineValues().get(0);
    }

    public static List<SupplierGeneralData> offlineValues() {
	return Arrays.asList(SupplierGeneralDataOffline.values())
    	    	.stream()
    	    	.map(i -> new SupplierGeneralData(i.getNif(), i.getIdentificationNumber(), i.getName(), i.getFiscalDesignator(), i.getSocialDesignator(), i.getMarketType()))
    	    	.collect(Collectors.toList());
    }

    public String getNif() {
        return nif;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public String getName() {
        return name;
    }

    public String getFiscalDesignator() {
        return fiscalDesignator;
    }

    public String getSocialDesignator() {
        return socialDesignator;
    }

    public String getMarketType() {
        return marketType;
    }

}
