package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReferenceDataResultError {

    @JsonProperty("Type")
    String type;
    @JsonProperty("ICode")
    String iCode;
    @JsonProperty("IDescription")
    String iDescription;
    @JsonProperty("Code")
    String code;
    @JsonProperty("Description")
    String description;
    
    public ReferenceDataResultError() {}
    
    public ReferenceDataResultError(String type, String iCode, String iDescription, String code, String description) {
	this.type = type;
	this.iCode = iCode;
	this.iDescription = iDescription;
	this.code = code;
	this.description = description;
    }
}
