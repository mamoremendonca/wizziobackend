package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.requests;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Supplier;

public class SearchSuppliersRequestDTO extends DTO {

    @JsonProperty("SearchSuppliers")
    private Supplier suppliers;

    public SearchSuppliersRequestDTO() {
	
	super();
	
	this.suppliers = new Supplier();
    }

    public Supplier getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(Supplier suppliers) {
        this.suppliers = suppliers;
    }

}
