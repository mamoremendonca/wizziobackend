package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.novabase.omni.oney.modules.referencedata.entity.dto.CostCenter;

public enum CostCenterOffline {

    //@formatter:off
    COSTCENTER_1("CostCenter1", "Cost Center 1", "DAF-1100", "P"),
    COSTCENTER_2("CostCenter2", "Cost Center 2", "DAF-1100", "W"),
    COSTCENTER_3("CostCenter3", "Cost Center 3", "DAF-1400", "P"),
    COSTCENTER_4("CostCenter4", "Cost Center 4", "DAF-1400", "W"),
    COSTCENTER_5("CostCenter5", "Cost Center 5", "", "P"),
    COSTCENTER_6("CostCenter6", "Cost Center 6", "", "P"),
    COSTCENTER_7("CostCenter7", "Cost Center 7", "", "W"),
    COSTCENTER_8("CostCenter8", "Cost Center 8", "", "W"),
    COSTCENTER_9("CostCenter9", "Cost Center 9", "", "W"),
    COSTCENTER_10("CostCenter10", "Cost Center 10", "", "P");
    //@formatter:on

    private String code;
    private String description;
    private String department;
    private String type;

    private CostCenterOffline(String code, String description, String department, String type) {
	this.code = code;
	this.description = description;
	this.department = department;
	this.type = type;
    }

    //@formatter:off
    public static CostCenterOffline of(String code){
	return Arrays.asList(CostCenterOffline.values())
		.stream()
		.filter(d -> d.getCode().equals(code))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on

    public static List<CostCenter> offlineValues() {
	return Arrays.asList(CostCenterOffline.values())
    	    	.stream()
    	    	.map(i -> new CostCenter(i.getCode(), i.getDescription(), "1", i.getDepartment(), i.getType() ))
    	    	.collect(Collectors.toList());
    }

    public String getCode() {
	return this.code;
    }
    
    public String getDescription() {
	return this.description;
    }
    
    public String getDepartment() {
	return this.department;
    }
    
    public String getType() {
	return this.type;
    }
}
