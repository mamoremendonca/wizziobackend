package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.novabase.omni.oney.modules.referencedata.entity.dto.InternalOrder;

public enum InternalOrderOffline {

    //@formatter:off
    INTERNALORDER_1("InternalOrder1", "Internal Order 1", "DAF-1400"),
    INTERNALORDER_2("InternalOrder2", "Internal Order 2", "DAF-1100"),
    INTERNALORDER_3("InternalOrder3", "Internal Order 3", ""),
    INTERNALORDER_4("InternalOrder4", "Internal Order 4", ""),
    INTERNALORDER_5("InternalOrder5", "Internal Order 5", "");
    //@formatter:on

    private String code;
    private String description;
    private String department;

     private InternalOrderOffline(String code, String description, String department) {
	this.code = code;
	this.description = description;
	this.department = department;
    }

    //@formatter:off
    public static InternalOrderOffline of(String code){
	return Arrays.asList(InternalOrderOffline.values())
		.stream()
		.filter(d -> d.getCode().equals(code))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on

    public static List<InternalOrder> offlineValues() {
	return Arrays.asList(InternalOrderOffline.values())
    	    	.stream()
    	    	.map(i -> new InternalOrder(i.getCode(), i.getDescription(), i.getDepartment(), "1") )
    	    	.collect(Collectors.toList());
    }

    public String getCode() {
	return this.code;
    }
    
    public String getDescription() {
	return this.description;
    }
    
    public String getDepartment() {
	return this.department;
    }
}
