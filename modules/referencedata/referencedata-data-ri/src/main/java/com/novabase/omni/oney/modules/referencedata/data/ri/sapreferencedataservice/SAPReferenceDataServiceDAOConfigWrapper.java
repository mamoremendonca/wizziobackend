package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice;

import java.util.HashMap;
import java.util.Map;

public class SAPReferenceDataServiceDAOConfigWrapper {

    private String username;
    private String password;
    private String contentType;
    private String acceptType;
    private Map<String, String> headers;
    
    public SAPReferenceDataServiceDAOConfigWrapper() {
	super();
    }
    
    public SAPReferenceDataServiceDAOConfigWrapper(final SAPReferenceDataServiceDAOConfig config) {
	
	super();
	
	this.username = config.userName();
	this.password = config.password();
	this.contentType = config.contentType();
	this.acceptType = config.acceptType();
	
	this.headers = new HashMap<String, String>();
	
	
    }

    public SAPReferenceDataServiceDAOConfigWrapper(final SAPReferenceDataServiceDAOConfig config, final Map<String, String> headers) {
	
	this(config);
	
	this.headers = new HashMap<String, String>(headers);
	
	
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getAcceptType() {
        return acceptType;
    }

    public void setAcceptType(String acceptType) {
        this.acceptType = acceptType;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }
    
}
