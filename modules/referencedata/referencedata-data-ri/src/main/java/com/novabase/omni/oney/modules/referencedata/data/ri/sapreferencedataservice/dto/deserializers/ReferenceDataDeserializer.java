package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.deserializers;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.ReferenceDataResponse;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.ReferenceDataResultErrors;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.enums.ResponseTypes;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Asset;
import com.novabase.omni.oney.modules.referencedata.entity.dto.CostCenter;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Department;
import com.novabase.omni.oney.modules.referencedata.entity.dto.InternalOrder;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Item;
import com.novabase.omni.oney.modules.referencedata.entity.dto.ItemType;
import com.novabase.omni.oney.modules.referencedata.entity.dto.RetentionCode;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Tax;

public class ReferenceDataDeserializer extends JsonDeserializer<ReferenceDataResponse> {

    private final String DEFAULT_JSON_ERRORS = "{ \"Error\": {}}";
    private final String DEFAULT_JSON_LIST = "[]";
    private final String DEFAULT_JSON_OBJ = "{}";
    private final String JSON_SUCCESS = "Success";
    private final String JSON_ERRORS = "Errors";
    private final String JSON_LIST = "Orders";
    private final String JSON_LIST_SUPPLIERS = "Suppliers";
    private final String JSON_LIST_DEPARTMENTS = "Departments";
    private final String JSON_WRAPPER_DEPARTMENTS = "SearchDepartmentStructure";

    @Override
    public ReferenceDataResponse deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
	ObjectMapper mapper = (ObjectMapper) jp.getCodec();
	ObjectNode root = (ObjectNode) mapper.readTree(jp);

	JsonNode subNode = extractResponse(root);
	ResponseTypes type = extractResponseType(root);
	ReferenceDataResponse response = null;
	boolean success = extractAsBoolean(subNode, JSON_SUCCESS, false);
	ReferenceDataResultErrors errors = mapper.readValue(extractAsString(subNode, JSON_ERRORS, DEFAULT_JSON_ERRORS), ReferenceDataResultErrors.class);
	switch (type) {
	case GetAllCostCentersResponse:
//	    List<CostCenter> list = mapper.readValue(extractAsString(subNode, JSON_LIST, DEFAULT_JSON_LIST), mapper.getTypeFactory().constructCollectionType(List.class, CostCenter.class));
//	    response = new ReferenceDataResponse<CostCenter>(success, list, errors);
	    response = new ReferenceDataResponse<CostCenter>(success, extractList(subNode, mapper, CostCenter.class), errors);
	    break;
	case GetAllInternalOrdersResponse:
//	    List<CostCenter> list = mapper.readValue(extractAsString(subNode, JSON_LIST, DEFAULT_JSON_LIST), mapper.getTypeFactory().constructCollectionType(List.class, InternalOrder.class));
//	    response = new ReferenceDataResponse<InternalOrder>(success, list, errors);
	    response = new ReferenceDataResponse<InternalOrder>(success, extractList(subNode, mapper, InternalOrder.class), errors);
	    break;
	case GetAllPOAssetsResponse:
//	    List<CostCenter> list = mapper.readValue(extractAsString(subNode, JSON_LIST, DEFAULT_JSON_LIST), mapper.getTypeFactory().constructCollectionType(List.class, Asset.class));
//	    response = new ReferenceDataResponse<Asset>(success, list, errors);
	    response = new ReferenceDataResponse<Asset>(success, extractList(subNode, mapper, Asset.class), errors);
	    break;
	case GetAllPOItemsResponse:
//	    List<CostCenter> list = mapper.readValue(extractAsString(subNode, JSON_LIST, DEFAULT_JSON_LIST), mapper.getTypeFactory().constructCollectionType(List.class, Item.class));
//	    response = new ReferenceDataResponse<Item>(success, list, errors);
	    response = new ReferenceDataResponse<Item>(success, extractList(subNode, mapper, Item.class), errors);
	    break;
	case GetPOItemsTypesResponse:
	    response = new ReferenceDataResponse<ItemType>(success, extractList(subNode, mapper, ItemType.class), errors);
	    break;
	case GetTaxCodesResponse:
//	    List<CostCenter> list = mapper.readValue(extractAsString(subNode, JSON_LIST, DEFAULT_JSON_LIST), mapper.getTypeFactory().constructCollectionType(List.class, Tax.class));
//	    response = new ReferenceDataResponse<Tax>(success, list, errors);
	    response = new ReferenceDataResponse<Tax>(success, extractList(subNode, mapper, Tax.class), errors);
	    break;
	case GetWithholdingTaxCodesResponse:
//	    List<CostCenter> list = mapper.readValue(extractAsString(subNode, JSON_LIST, DEFAULT_JSON_LIST), mapper.getTypeFactory().constructCollectionType(List.class, RetentionCode.class));
//	    response = new ReferenceDataResponse<RetentionCode>(success, list, errors);
	    response = new ReferenceDataResponse<RetentionCode>(success, extractList(subNode, mapper, RetentionCode.class), errors);
	    break;
	case GetDepartmentsResponse:
	    response = new ReferenceDataResponse<Department>(success, this.extractList(subNode, mapper, Department.class, JSON_LIST_DEPARTMENTS, JSON_WRAPPER_DEPARTMENTS), errors);
	    break;
	default:
	    // TODO: Implement Default With not Implemented.
	    break;
	}

	return response;
    }

    private <T,R> List<T> extractList(JsonNode node, ObjectMapper mapper, Class<T> clazz, String jsonElement, String jsonWrapper) {
	try {
	    if (containsList(node)) {
		return mapper.readValue(extractAsString(node, jsonElement, DEFAULT_JSON_LIST), mapper.getTypeFactory().constructCollectionType(List.class, clazz));
	    } else {
		JsonNode nodeWrapper = extractAsNode(node, JSON_LIST_SUPPLIERS);
		return Arrays.asList(mapper.readValue(
			(nodeWrapper != null ? extractAsString(nodeWrapper, jsonWrapper, DEFAULT_JSON_OBJ) : DEFAULT_JSON_OBJ), clazz));
	    }
	} catch (IOException e) {
	    return null;
	}
    }
    private <T> List<T> extractList(JsonNode node, ObjectMapper mapper, Class<T> clazz) {
	return extractList(node, mapper, clazz, JSON_LIST, "");
    }
    
    private String extractAsString(JsonNode node, String tag, String defaultString) {
	return (node.has(tag) ? node.get(tag).toString() : defaultString);
    }
    private JsonNode extractAsNode(JsonNode node, String tag) {
	return (node.has(tag) ? node.get(tag): null);
    }

    private boolean extractAsBoolean(JsonNode node, String tag, boolean defaultBoolean) {
	return (node.has(tag) ? node.get(tag).asBoolean() : defaultBoolean);
    }

    private JsonNode extractResponse(ObjectNode root) {
	Optional<ResponseTypes> type = Arrays.asList(ResponseTypes.values()).stream().filter(rt -> root.has(rt.name())).findFirst();
	return (type.isPresent() ? root.get(type.get().name()) : null);
    }

    private ResponseTypes extractResponseType(ObjectNode root) {
	Optional<ResponseTypes> type = Arrays.asList(ResponseTypes.values()).stream().filter(rt -> root.has(rt.name())).findFirst();
	return (type.isPresent() ? type.get() : null);
    }
    private boolean containsList(JsonNode node) {
	return (node != null ? node.toString().contains("[") : false);
    }
}
