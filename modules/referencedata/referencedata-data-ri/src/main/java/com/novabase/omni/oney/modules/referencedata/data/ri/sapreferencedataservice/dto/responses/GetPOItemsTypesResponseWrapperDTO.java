package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base.ResponseWrapper;

public class GetPOItemsTypesResponseWrapperDTO extends DTO implements ResponseWrapper<GetPOItemsTypesResponseDTO> {

    @JsonProperty("GetPOItemsTypesResponse")
    private GetPOItemsTypesResponseDTO getPOItemsTypesResponseDTO;
    
    public GetPOItemsTypesResponseWrapperDTO() {
	
	super();
	
	this.getPOItemsTypesResponseDTO = new GetPOItemsTypesResponseDTO();
	
    }

    public GetPOItemsTypesResponseDTO getGetPOItemsTypesResponseDTO() {
        return getPOItemsTypesResponseDTO;
    }

    public void setGetPOItemsTypesResponseDTO(GetPOItemsTypesResponseDTO getPOItemsTypesResponseDTO) {
        this.getPOItemsTypesResponseDTO = getPOItemsTypesResponseDTO;
    }

    @Override
    public GetPOItemsTypesResponseDTO getResponse() {
	return this.getPOItemsTypesResponseDTO;
    }

}
