package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.dto.responses.base.ResponseWrapper;

public class GetAllPOAssetsResponseWrapperDTO extends DTO implements ResponseWrapper<GetAllPOAssetsResponseDTO> {

    @JsonProperty("GetAllPOAssetsResponse")
    private GetAllPOAssetsResponseDTO getAllPOAssetsResponseDTO;
    
    public GetAllPOAssetsResponseWrapperDTO() {
	
	super();
	
	this.getAllPOAssetsResponseDTO = new GetAllPOAssetsResponseDTO();
	
    }

    public GetAllPOAssetsResponseDTO getGetAllPOAssetsResponseDTO() {
        return getAllPOAssetsResponseDTO;
    }

    public void setGetAllPOAssetsResponseDTO(GetAllPOAssetsResponseDTO getAllPOAssetsResponseDTO) {
        this.getAllPOAssetsResponseDTO = getAllPOAssetsResponseDTO;
    }

    @Override
    public GetAllPOAssetsResponseDTO getResponse() {
	return this.getAllPOAssetsResponseDTO;
    }

}
