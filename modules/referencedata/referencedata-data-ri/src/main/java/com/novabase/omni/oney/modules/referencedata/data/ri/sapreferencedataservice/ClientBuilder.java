package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice;

//import java.util.ArrayList;
//import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
//import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriTemplateHandler;

public class ClientBuilder {

    private static final String HEADER_TRANSACTION_IDENTIFIER = "PayloadStdAttributes-ETransactionIdentifier";
    private static final String HEADER_SOURCE_APPLICATION = "UXDetails-ConsumerCredentials-Application";
    private static final String HEADER_SOURCE_CHANNEL = "UXDetails-ConsumerChannel-Channel";

    private String channel;
    private String application;

    public RestTemplate build(String baseUri) {

	DefaultUriTemplateHandler handler = new DefaultUriTemplateHandler();
	handler.setBaseUrl(baseUri);

	RestTemplate client = new RestTemplate();
	client.setUriTemplateHandler(handler);
	
	return client;
    }

    public ClientBuilder withApplication(String application) {
	this.application = application;
	return this;
    }

    public ClientBuilder withChannel(String channel) {
	this.channel = channel;
	return this;
    }
    
    public HttpEntity<String> buildHeaders(String body, String identifier) {
	HttpHeaders headers = new HttpHeaders();
	headers.setContentType(MediaType.APPLICATION_JSON);
	headers.add(HEADER_TRANSACTION_IDENTIFIER, identifier);
	if (StringUtils.isNoneEmpty(this.application)) {
	    headers.add(HEADER_SOURCE_APPLICATION, this.application);
	}
	if (StringUtils.isNotEmpty(this.channel)) {
	    headers.add(HEADER_SOURCE_CHANNEL, this.channel);
	}
	
        return (StringUtils.isEmpty(body) ? 
        	new HttpEntity<String>(headers) : new HttpEntity<String>(body, headers));	
    }
}