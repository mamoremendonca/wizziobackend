package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice;

import javax.ws.rs.core.MediaType;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/sapreferencedataservice/sapreferencedataservice", icon = @Icon(resource = "OSGI-INF/icon/ws.png", size = 32))
public @interface SAPReferenceDataServiceDAOConfig {

  public static final String CPID = "com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice";

  @AttributeDefinition(name = "%providerName.name", description = "%providerName.description")
  String providerName() default "Oney SAP Reference Data";

  @AttributeDefinition(name = "%address.name", description = "%address.description")
  String address() default "https://eaidev.oney.pt";

  @AttributeDefinition(name = "%contentType.name", description = "%contentType.description")
  String contentType() default MediaType.APPLICATION_JSON;

  @AttributeDefinition(name = "%acceptType.name", description = "%acceptType.description")
  String acceptType() default MediaType.APPLICATION_JSON;

  @AttributeDefinition(name = "%userName.name", description = "%userName.description")
  String userName() default "";

  @AttributeDefinition(name = "%password.name", type = AttributeType.PASSWORD, description = "%password.description")
  String password() default "";

  @AttributeDefinition(name = "%connectionTimeout.name", type = AttributeType.LONG, min = "0", description = "%connectionTimeout.description")
  long connectionTimeout() default 10000;

  @AttributeDefinition(name = "%receiveTimeout.name", type = AttributeType.LONG, min = "0", description = "%receiveTimeout.description")
  long receiveTimeout() default 180000;

  @AttributeDefinition(name = "%proxyEnabled.name", type = AttributeType.BOOLEAN, description = "%proxyEnabled.description")
  boolean proxyEnabled() default false;

  @AttributeDefinition(name = "%proxyHost.name", description = "%proxyHost.description")
  String proxyHost() default "localhost";

  @AttributeDefinition(name = "%proxyPort.name", description = "%proxyPort.description")
  int proxyPort() default 8888;

  @AttributeDefinition(name = "%jsonPathSuccessExpression.name", description = "%jsonPathSuccessExpression.description")
  String jsonPathSuccessExpression() default "";

  @AttributeDefinition(name = "%serializationFeatures.name",
      description = "%serializationFeatures.description")
  String[] serializationFeatures() default {};

  @AttributeDefinition(name = "%deserializationFeatures.name",
      description = "%deserializationFeatures.description")
  String[] deserializationFeatures() default {"ACCEPT_EMPTY_STRING_AS_NULL_OBJECT=true", "FAIL_ON_UNKNOWN_PROPERTIES=false"};

  @AttributeDefinition(name = "%resourceGetAssets.name", description = "%resourceGetAssets.description")
  String resourceGetAssets() default "/CORPORATE/API/getAllPOAssets/get";
  @AttributeDefinition(name = "%resourceGetItems.name", description = "%resourceGetItems.description")
  String resourceGetItems() default "/CORPORATE/API/getAllPOItems/get";
  @AttributeDefinition(name = "%resourceGetItemTypes.name", description = "%resourceGetItemTypes.description")
  String resourceGetItemTypes() default "/CORPORATE/API/getPOItemsTypes/get";
  @AttributeDefinition(name = "%resourceGetTaxes.name", description = "%resourceGetTaxes.description")
  String resourceGetTaxes() default "/CORPORATE/API/getTaxCodes/get";
  @AttributeDefinition(name = "%resourceGetCostCenters.name", description = "%resourceGetCostCenters.description")
  String resourceGetCostCenters() default "/CORPORATE/API/getAllCostCenters/get";
  @AttributeDefinition(name = "%resourceGetInternalOrders.name", description = "%resourceGetInternalOrders.description")
  String resourceGetInternalOrders() default "/CORPORATE/API/getAllInternalOrders/get";
  @AttributeDefinition(name = "%resourceGetDepartments.name", description = "%resourceGetDepartments.description")
  String resourceGetDepartments()  default "/CORPORATE/API/getDepartments/get";
  @AttributeDefinition(name = "%resourceGetRetentionCode.name", description = "%resourceGetRetentionCode.description")
  String resourceGetRetentionCode() default "/CORPORATE/API/getWithholdingTaxCodes/get";
  @AttributeDefinition(name = "%resourcePOSTSuppliers.name", description = "%resourcePOSTSuppliers.description")
  String resourcePOSTSuppliers() default "/CORPORATE/API/searchSuppliers/search";
  @AttributeDefinition(name = "%resourcePOSTSuppliersGeneralData.name", description = "%resourcePOSTSuppliersGeneralData.description")
  String resourcePOSTSuppliersGeneralData() default "/CORPORATE/API/getSupplierGeneralData/get";

  @AttributeDefinition(name = "%offlineMode.name", description = "%offlineMode.description")
  boolean offlineMode() default false;



}
