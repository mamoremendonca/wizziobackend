package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.examples;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.novabase.omni.oney.modules.referencedata.entity.dto.Tax;

public enum TaxOffline {

    //@formatter:off
    TAX_1("TaxCode1", "Tax 1", "1.000", "N", ""),
    TAX_2("TaxCode2", "Tax 2", "2.000", "N", ""),
    TAX_3("TaxCode3", "Tax 3", "3.000", "I", "");
    //@formatter:on

    private String code;
    private String description;
    private String value;
    private String market;
    private String docType;
	
     private TaxOffline(String code, String description, String value, String market, String docType) {
	this.code = code;
	this.description = description;
	this.value = value;
	this.market = market;
	this.docType = docType;
    }

    //@formatter:off
    public static TaxOffline of(String code){
	return Arrays.asList(TaxOffline.values())
		.stream()
		.filter(d -> d.getCode().equals(code))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on

    public static List<Tax> offlineValues() {
	return Arrays.asList(TaxOffline.values())
    	    	.stream()
    	    	.map(i -> new Tax(i.getCode(), i.getDescription(), i.getValue(), i.getMarket(), i.getDocType(), "1") )
    	    	.collect(Collectors.toList());
    }

    public String getCode() {
	return this.code;
    }
    
    public String getDescription() {
	return this.description;
    }
    
    public String getValue() {
	return this.value;
    }
    public String getMarket() {
	return this.market;
    }
    public String getDocType() {
	return this.docType;
    }
}
