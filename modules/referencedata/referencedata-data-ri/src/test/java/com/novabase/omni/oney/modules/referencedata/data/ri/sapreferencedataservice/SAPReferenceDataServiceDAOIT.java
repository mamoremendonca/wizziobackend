package com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice;

import static io.digitaljourney.platform.modules.paxexam.BundleOptions.mavenBundles;
import static io.digitaljourney.platform.modules.paxexam.BundleOptions.rsClient;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.newConfiguration;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.rsClientConfiguration;
import static org.ops4j.pax.exam.CoreOptions.composite;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;

import io.digitaljourney.platform.modules.paxexam.base.BaseTestSupport;
import com.novabase.omni.oney.modules.referencedata.data.ri.sapreferencedataservice.SAPReferenceDataServiceDAOConfig;
import com.novabase.omni.oney.modules.referencedata.data.api.SAPReferenceDataServiceDAO;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class SAPReferenceDataServiceDAOIT extends BaseTestSupport {

  @SuppressWarnings("unused")
  @Inject
  private SAPReferenceDataServiceDAO dao;

  @Override
  protected Option bundles() {
    return composite(super.bundles(), rsClient(),
        mavenBundles("com.novabase.omni.oney.modules"),
        testBundle("com.novabase.omni.oney.modules", "referencedata-data-ri"));
  }

  @Override
  protected Option configurations() {
    return composite(super.configurations(),
        rsClientConfiguration("sapreferencedataservice", newConfiguration(SAPReferenceDataServiceDAOConfig.CPID)));
  }

  @Test
  public void testBundle() {
    assertBundleActive("com.novabase.omni.oney.modules.referencedata-data-ri");
  }

}
