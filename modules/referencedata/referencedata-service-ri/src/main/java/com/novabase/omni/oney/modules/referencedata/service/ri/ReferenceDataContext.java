package com.novabase.omni.oney.modules.referencedata.service.ri;

import java.util.Date;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

import io.digitaljourney.platform.modules.async.api.PlatformAsyncManager;
import io.digitaljourney.platform.modules.cache.api.PlatformCacheManager;
import io.digitaljourney.platform.modules.commons.annotation.DocumentationResource;
import io.digitaljourney.platform.modules.invocation.api.PlatformInvocationManager;
import io.digitaljourney.platform.modules.invocation.api.model.EventLog;
import io.digitaljourney.platform.modules.invocation.api.model.type.StatusType;
import io.digitaljourney.platform.modules.security.api.PlatformSecurityManager;
import io.digitaljourney.platform.modules.ws.rs.api.context.AbstractRSEndpointContext;
import com.novabase.omni.oney.modules.referencedata.service.api.exception.ReferenceDataException;

// @formatter:off
@Component(
	service = { Object.class, ReferenceDataContext.class },
	reference = {
		@Reference(
			name = ReferenceDataResourceProperties.REF_PLATFORM_SECURITY_MANAGER,
			service = PlatformSecurityManager.class,
			cardinality = ReferenceCardinality.MANDATORY),
        @Reference(
            name = ReferenceDataResourceProperties.REF_PLATFORM_INVOCATION_MANAGER,
            service = PlatformInvocationManager.class,
            cardinality = ReferenceCardinality.MANDATORY),
		@Reference(
			name = ReferenceDataResourceProperties.REF_ASYNC_MANAGER,
			service = PlatformAsyncManager.class,
			cardinality = ReferenceCardinality.MANDATORY),
		@Reference(
			name = ReferenceDataResourceProperties.REF_CACHE_MANAGER,
			service = PlatformCacheManager.class,
			cardinality = ReferenceCardinality.MANDATORY)
	})
@DocumentationResource(ReferenceDataResourceProperties.DOCS_ADDRESS)
// @formatter:on
public final class ReferenceDataContext extends AbstractRSEndpointContext {
    @Activate
    public void activate(ComponentContext ctx) {
	prepare(ctx);
    }

    public void logError(String message) {
	EventLog event = new EventLog();
	event.setEvent_message(message);
	event.setStart_event_time(new Date());
	getPlatformInvocationManager().logNewEvent(event, null, StatusType.ERROR);
	getLogger().error(message);
    }

    public ReferenceDataException exception(String message) {
	return ReferenceDataException.of(this, message);
    }

    public ReferenceDataException exception(Throwable cause) {
	return ReferenceDataException.of(this, cause);
    }
}
