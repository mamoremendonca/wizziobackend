package com.novabase.omni.oney.modules.referencedata.service.ri;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.modules.referencedata.entity.dto.Asset;
import com.novabase.omni.oney.modules.referencedata.entity.dto.CostCenter;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Department;
import com.novabase.omni.oney.modules.referencedata.entity.dto.InternalOrder;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Item;
import com.novabase.omni.oney.modules.referencedata.entity.dto.ItemType;
import com.novabase.omni.oney.modules.referencedata.entity.dto.RetentionCode;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Supplier;
import com.novabase.omni.oney.modules.referencedata.entity.dto.SupplierGeneralData;
import com.novabase.omni.oney.modules.referencedata.entity.dto.Tax;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.AssetMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.CostCenterMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.DepartmentMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.InternalOrderMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ItemMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ItemTypeMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ProjectMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.RetentionCodeMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SearchFilterMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierGeneralDataMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.TaxMsDTO;

@Mapper
public interface ReferenceDataResourceMapper {
    public static final ReferenceDataResourceMapper INSTANCE = Mappers.getMapper(ReferenceDataResourceMapper.class);

    @IterableMapping(elementTargetType = CostCenterMsDTO.class)
    List<CostCenterMsDTO> toListCostCenterDTO(List<CostCenter> lst);

    @IterableMapping(elementTargetType = DepartmentMsDTO.class)
    List<DepartmentMsDTO> toListDepartmentDTO(List<Department> lst);

    @IterableMapping(elementTargetType = InternalOrderMsDTO.class)
    List<InternalOrderMsDTO> toListInternalOrderDTO(List<InternalOrder> lst);

    @IterableMapping(elementTargetType = ItemMsDTO.class)
    List<ItemMsDTO> toListItemDTO(List<Item> lst);

    @IterableMapping(elementTargetType = ItemTypeMsDTO.class)
    List<ItemTypeMsDTO> toListItemTypeDTO(List<ItemType> lst);

    @IterableMapping(elementTargetType = ProjectMsDTO.class)
    List<ProjectMsDTO> toListProjectDTO(List<CostCenter> lst);

    @IterableMapping(elementTargetType = RetentionCodeMsDTO.class)
    List<RetentionCodeMsDTO> toListRetentionCodeDTO(List<RetentionCode> lst);

    @IterableMapping(elementTargetType = TaxMsDTO.class)
    List<TaxMsDTO> toListTaxDTO(List<Tax> lst);

    @IterableMapping(elementTargetType = AssetMsDTO.class)
    List<AssetMsDTO> toListAssetDTO(List<Asset> lst);

    @IterableMapping(elementTargetType = SupplierMsDTO.class)
    List<SupplierMsDTO> toListSupplierMsDTO(List<Supplier> lst);
    
    // --------------------------------------------------------------------

    /* # SuperClass mapping doesn't work. Upper class attributes must be included in the mapping for the subClasses
    @Mapping(source = "Description", target = "description")
    @Mapping(expression = "java(\"1\".equals(obj.Status))", target = "active")
    ReferenceDataDTO toReferenceDataDTO(ReferenceData obj);
    */
    
    @Mapping(source = "CostCenter", target = "code")
    @Mapping(source = "Description", target = "description")
    @Mapping(expression = "java(\"1\".equals(obj.Status))", target = "active")
    @Mapping(source = "Department", target = "departmentCode")
    CostCenterMsDTO toCostCenterDTO(CostCenter obj);
    
    @Mapping(source = "Code", target = "code")
    @Mapping(source = "Name", target = "description")
    @Mapping(expression = "java(\"1\".equals(obj.Status))", target = "active")
    DepartmentMsDTO toDepartmentDTO(Department obj);

    @Mapping(source = "Order", target = "code")
    @Mapping(source = "Description", target = "description")
    @Mapping(expression = "java(\"1\".equals(obj.Status))", target = "active")
    @Mapping(source = "Department", target = "departmentCode")
    @Mapping(constant = "", target = "costCenterCode")
    InternalOrderMsDTO toInternalOrderDTO(InternalOrder obj);
    
    @Mapping(source = "Material", target = "code")
    @Mapping(source = "Description", target = "description")
    @Mapping(expression = "java(\"1\".equals(obj.Status))", target = "active")
    @Mapping(source = "Department", target = "departmentCode")
//    @Mapping(constant = "", target = "departmentCode")
    @Mapping(source = "CostCenter", target = "defaultCostCenterCode")
    @Mapping(source = "TaxCode", target = "defaultTaxCode")
    @Mapping(source = "Materialtype", target = "itemTypeCode")
    ItemMsDTO toItemDTO(Item obj);

    @Mapping(source = "Materialtype", target = "code")
    @Mapping(source = "DescriptionMaterialtype", target = "description")
    @Mapping(expression = "java(\"1\".equals(obj.Status))", target = "active")
    @Mapping(constant = "", target = "departmentCode")
    ItemTypeMsDTO toItemTypeDTO(ItemType obj);

    @Mapping(source = "CostCenter", target = "code")
    @Mapping(source = "Description", target = "description")
    @Mapping(expression = "java(\"1\".equals(obj.Status))", target = "active")
    @Mapping(source = "Department", target = "departmentCode")
    ProjectMsDTO toProjectDTO(CostCenter obj);
    
    @Mapping(source = "TaxCode", target = "code")
    @Mapping(source = "Description", target = "description")
    @Mapping(expression = "java(\"1\".equals(obj.Status))", target = "active")
//    @Mapping(source = "TaxPercentage", target = "percentage")
    @Mapping(source = "ValueIva", target = "taxPercentage")
    @Mapping(source = "Market", target = "marketCode")
    TaxMsDTO toTaxDTO(Tax obj);
    
    @Mapping(source = "TaxCode", target = "code")
    @Mapping(source = "Description", target = "description")
    @Mapping(expression = "java(\"1\".equals(obj.Status))", target = "active")
    @Mapping(source = "Taxpercentage", target = "taxPercentage")
    RetentionCodeMsDTO toRetentionCodeDTO(RetentionCode obj);

    @Mapping(source = "AssetNumber", target = "code")
    @Mapping(source = "Description", target = "description")
    @Mapping(expression = "java(\"1\".equals(obj.Status))", target = "active")
    @Mapping(source = "TaxCode", target = "defaultTaxCode")
    AssetMsDTO toAssetDTO(Asset obj);
    
    @Mapping(source = "IdentificationNumber", target = "code")
    @Mapping(source = "NIF", target = "nif")
    @Mapping(source = "Name", target = "description")
    @Mapping(constant = "true", target = "active")
    SupplierMsDTO toSupplierMsDTO(final Supplier obj);
    
    @InheritInverseConfiguration
    Supplier toSupplier(final SupplierMsDTO supplier);
    
    @Mapping(source = "id", target = "IdentificationNumber")
    @Mapping(source = "nif", target = "NIF")
    @Mapping(source = "name", target = "Name")
    Supplier toSupplier(final SearchFilterMsDTO supplier);
    
    @Mapping(source = "IdentificationNumber", target = "code")
    @Mapping(source = "NIF", target = "nif")
    @Mapping(source = "Name", target = "description")
    @Mapping(constant = "true", target = "active")
    @Mapping(source = "FiscalDesignator", target = "fiscalDesignator")
    @Mapping(source = "SocialDesignator", target = "socialDesignator")
    @Mapping(source = "MarketType", target = "marketType")
    SupplierGeneralDataMsDTO toSupplierGeneralDataMsDTO(final SupplierGeneralData obj);
    
}
