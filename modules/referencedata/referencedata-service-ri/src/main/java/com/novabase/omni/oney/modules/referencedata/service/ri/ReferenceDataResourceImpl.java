package com.novabase.omni.oney.modules.referencedata.service.ri;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.modules.referencedata.data.api.SAPReferenceDataServiceDAO;
import com.novabase.omni.oney.modules.referencedata.data.api.exception.SAPReferenceDataException;
import com.novabase.omni.oney.modules.referencedata.entity.dto.CostCenter;
import com.novabase.omni.oney.modules.referencedata.service.api.ReferenceDataResource;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.AssetMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.CostCenterMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.DepartmentMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.InternalOrderMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ItemMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ItemTypeMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.ProjectMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.RetentionCodeMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SearchFilterMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierGeneralDataMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.TaxMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.exception.ReferenceDataException;

import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;
import io.digitaljourney.platform.modules.ws.rs.api.resource.AbstractResource;

// @formatter:off
@Component(
	configurationPid = ReferenceDataResourceConfig.CPID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	reference = {
		@Reference(
			name = ReferenceDataResourceProperties.REF_CONTEXT,
			service = ReferenceDataContext.class,
			cardinality = ReferenceCardinality.MANDATORY)
})
@Designate(ocd = ReferenceDataResourceConfig.class)
@RSProvider(ReferenceDataResourceProperties.ADDRESS)
// @formatter:on
public class ReferenceDataResourceImpl extends AbstractResource<ReferenceDataContext, ReferenceDataResourceConfig> implements ReferenceDataResource {

    @Reference
    private volatile SAPReferenceDataServiceDAO referenceServiceDAO;

    @Activate
    public void activate(ComponentContext ctx, ReferenceDataResourceConfig config) {
	prepare(ctx, config);
    }

    @Modified
    public void modified(ReferenceDataResourceConfig config) {
	prepare(config);
    }

    @Override
    @RequiresPermissions(ReferenceDataResourceProperties.PERMISSION_READ)
    public List<ItemMsDTO> getItems() {

	try {
	    return ReferenceDataResourceMapper.INSTANCE.toListItemDTO(referenceServiceDAO.getItems());
	} catch (SAPReferenceDataException e) {
	    getCtx().logError(e.getMessage());
	    throw getCtx().exception(e.getMessage());
	} catch (Throwable t) {
	    getCtx().logError(t.getMessage());
	    throw ReferenceDataException.of(getCtx(), "Error searching for Items. \n" + t.getMessage());
	}
    }

    @Override
    @RequiresPermissions(ReferenceDataResourceProperties.PERMISSION_READ)
    public List<ItemTypeMsDTO> getItemTypes() {

	try {
	    // Expected behaviour
	    return ReferenceDataResourceMapper.INSTANCE.toListItemTypeDTO(referenceServiceDAO.getItemTypes());
	    
//	    // Workaround while the actual service doesn't exist
//	    List<Item> items = referenceServiceDAO.getItems();
//	    // @formatter:off
//	    return (items != null? 
//		items.stream()
//			.map(i -> new ItemTypeMsDTOBuilder()
//				.withCode(i.CodMaterialtype)
//				.withDepartmentCode("")
//				.withDescription(i.DescCodtypemat)
//				.withActive(true)
//				.build())
//			.distinct()
//			.collect(Collectors.toList())
//		: null);
//	    // @formatter:on
	    
	} catch (SAPReferenceDataException e) {
	    getCtx().logError(e.getMessage());
	    throw getCtx().exception(e.getMessage());
	} catch (Throwable t) {
	    getCtx().logError(t.getMessage());
	    throw ReferenceDataException.of(getCtx(), "Error searching for Item Types. \n" + t.getMessage());
	}
    }

    
    @Override
    @RequiresPermissions(ReferenceDataResourceProperties.PERMISSION_READ)
    public List<TaxMsDTO> getTaxes() {

	try {
	    return ReferenceDataResourceMapper.INSTANCE.toListTaxDTO(referenceServiceDAO.getTaxes());
	} catch (SAPReferenceDataException e) {
	    getCtx().logError(e.getMessage());
	    throw getCtx().exception(e.getMessage());
	} catch (Throwable t) {
	    getCtx().logError(t.getMessage());
	    throw ReferenceDataException.of(getCtx(), "Error searching for Taxes. \n" + t.getMessage());
	}
    }

    @Override
    @RequiresPermissions(ReferenceDataResourceProperties.PERMISSION_READ)
    public List<CostCenterMsDTO> getCostCenters() {

	try {
	    return ReferenceDataResourceMapper.INSTANCE.toListCostCenterDTO(
		    referenceServiceDAO.getCostCenters()
		    	.stream()
		    	.filter(obj -> !CostCenter.COST_CENTER_TYPE_PROJECT.equals(obj.TypeCostCenter)).collect(Collectors.toList())
		    );
	} catch (SAPReferenceDataException e) {
	    getCtx().logError(e.getMessage());
	    throw getCtx().exception(e.getMessage());
	} catch (Throwable t) {
	    getCtx().logError(t.getMessage());
	    throw ReferenceDataException.of(getCtx(), "Error searching for CostCenters. \n" + t.getMessage());
	}
    }

    @Override
    @RequiresPermissions(ReferenceDataResourceProperties.PERMISSION_READ)
    public List<ProjectMsDTO> getProjects() {

	try {
	    return ReferenceDataResourceMapper.INSTANCE.toListProjectDTO(
		    referenceServiceDAO.getCostCenters()
		    	.stream()
		    	.filter(obj -> CostCenter.COST_CENTER_TYPE_PROJECT.equals(obj.TypeCostCenter))
		    	.collect(Collectors.toList())
		    );
	} catch (SAPReferenceDataException e) {
	    getCtx().logError(e.getMessage());
	    throw getCtx().exception(e.getMessage());
	} catch (Throwable t) {
	    getCtx().logError(t.getMessage());
	    throw ReferenceDataException.of(getCtx(), "Error searching for Projects. \n" + t.getMessage());
	}
    }

    @Override
    @RequiresPermissions(ReferenceDataResourceProperties.PERMISSION_READ)
    public List<InternalOrderMsDTO> getInternalOrdens() {

	try {
	    return ReferenceDataResourceMapper.INSTANCE.toListInternalOrderDTO(referenceServiceDAO.getInternalOrdens());
	} catch (SAPReferenceDataException e) {
	    getCtx().logError(e.getMessage());
	    throw getCtx().exception(e.getMessage());
	} catch (Throwable t) {
	    getCtx().logError(t.getMessage());
	    throw ReferenceDataException.of(getCtx(), "Error searching for InternalOrders. \n" + t.getMessage());
	}
    }

    @Override
    @RequiresPermissions(ReferenceDataResourceProperties.PERMISSION_READ)
    public List<DepartmentMsDTO> getDepartments() {

	try {
//	    return ReferenceDataResourceMapper.INSTANCE.toListDepartmentDTO(
//		    referenceServiceDAO.getDepartments()
//        	    	.stream()
//        	    	.filter(d -> d.Code.contains(getConfig().filter()))
//        	    	.collect(Collectors.toList()));
	    final List<DepartmentMsDTO> resultList = ReferenceDataResourceMapper.INSTANCE.toListDepartmentDTO(
		    referenceServiceDAO.getDepartments()
		    	.stream()
		    	.filter(d -> d.Code.contains(getConfig().filter()))
		    	.collect(Collectors.toList())
		    );
	    
	    // TODO: Temporarily forcing active status. Need to revised with Oney Team.
	    resultList.parallelStream().forEach(d -> d.active = true);
	    
	    return resultList; 
	    
	} catch (SAPReferenceDataException e) {
	    getCtx().logError(e.getMessage());
	    throw getCtx().exception(e.getMessage());
	} catch (Throwable t) {
	    getCtx().logError(t.getMessage());
	    throw ReferenceDataException.of(getCtx(), "Error searching for Departments. \n" + t.getMessage());
	}
    }

        
    @Override
    @RequiresPermissions(ReferenceDataResourceProperties.PERMISSION_READ)
    public List<RetentionCodeMsDTO> getRetentionCode() {

	try {
	    return ReferenceDataResourceMapper.INSTANCE.toListRetentionCodeDTO(referenceServiceDAO.getRetentionCode());
	} catch (SAPReferenceDataException e) {
	    getCtx().logError(e.getMessage());
	    throw getCtx().exception(e.getMessage());
	} catch (Throwable t) {
	    getCtx().logError(t.getMessage());
	    throw ReferenceDataException.of(getCtx(), "Error searching for RetentionCodes. \n" + t.getMessage());
	}
    }

    @Override
    @RequiresPermissions(ReferenceDataResourceProperties.PERMISSION_READ)
    public List<AssetMsDTO> getAssets() {

	try {
	    return ReferenceDataResourceMapper.INSTANCE.toListAssetDTO(referenceServiceDAO.getAssets());
	} catch (SAPReferenceDataException e) {
	    getCtx().logError(e.getMessage());
	    throw getCtx().exception(e.getMessage());
	} catch (Throwable t) {
	    getCtx().logError(t.getMessage());
	    throw ReferenceDataException.of(getCtx(), "Error searching for Assets. \n" + t.getMessage());
	}
    }
    

    @Override
    @RequiresPermissions(ReferenceDataResourceProperties.PERMISSION_READ)
    public List<SupplierMsDTO> getSuppliers(final SearchFilterMsDTO searchFilter) {
	
	try {
	    
	    if (searchFilter.id == null) {
		searchFilter.id = "";
	    }
		
	    if (searchFilter.nif == null) {
		searchFilter.nif = "";
	    }
		
	    if (searchFilter.name == null) {
		searchFilter.name = "";
	    }
	    
	    return ReferenceDataResourceMapper.INSTANCE.toListSupplierMsDTO(this.referenceServiceDAO.getSuppliers(ReferenceDataResourceMapper.INSTANCE.toSupplier(searchFilter)));
	} catch (SAPReferenceDataException e) {
	    throw this.getCtx().exception(e.getMessage());
	} catch (Throwable t) {
	    throw this.getCtx().exception("Error searching for Suppliers Market Info. \n" + t.getMessage());
	}
	
	
    }
    

    @Override
    @RequiresPermissions(ReferenceDataResourceProperties.PERMISSION_READ)
    public SupplierGeneralDataMsDTO getSupplierMarketInfo(final String supplierCode) {
	
	try {
	    return ReferenceDataResourceMapper.INSTANCE.toSupplierGeneralDataMsDTO(this.referenceServiceDAO.getSupplierGeneralData(supplierCode));
	} catch (SAPReferenceDataException e) {
	    throw this.getCtx().exception(e.getMessage());
	} catch (Throwable t) {
	    throw this.getCtx().exception("Error searching for Suppliers Market Info. \n" + t.getMessage());
	}
	
    }
}
