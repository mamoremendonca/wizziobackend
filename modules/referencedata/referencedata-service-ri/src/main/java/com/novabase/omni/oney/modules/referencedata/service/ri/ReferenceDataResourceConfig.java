package com.novabase.omni.oney.modules.referencedata.service.ri;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/referencedata", icon = @Icon(resource = "OSGI-INF/icon/resource.png", size = 32))
public @interface ReferenceDataResourceConfig {
    public static final String CPID = "oney.base.modules.referencedata.service.ri.referencedata";

    @AttributeDefinition(name = "%filter.name", description = "%filter.description")
    String filter();
}
