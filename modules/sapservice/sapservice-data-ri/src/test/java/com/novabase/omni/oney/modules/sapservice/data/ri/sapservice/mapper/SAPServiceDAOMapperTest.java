package com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.novabase.omni.oney.modules.referencedata.service.api.dto.TaxMsDTO;
import com.novabase.omni.oney.modules.sapservice.data.api.enums.FinancialDocumentType;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.requests.CreateSupplierFinancialDocumentRequestDTO;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.requests.CreateSupplierFinancialDocumentRequestItemDTO;
import com.novabase.omni.oney.modules.sapservice.entity.FinancialDocument;
import com.novabase.omni.oney.modules.sapservice.entity.ItemFD;

public class SAPServiceDAOMapperTest {

    private static final DateTimeFormatter DEFAULT_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd");
    
    private static final String DEFAULT_DOCUMENT_EXTERNAL_ID = "DocumentExternalId";
    private static final String DEFAULT_DOCUMENT_NUMBER = "DocumentNumber";
    private static final String DEFAULT_IDENTIFIER_PURCHASE_ORDER = "IdentifierPurchaseOrder";
    private static final String DEFAULT_SUPPLIER_NUMBER = "SupplierNumber";
    private static final String DEFAULT_SUPPLIER_VAT_NUMBER = "SupplierVatNumber";
    private static final String DEFAULT_FINANCIAL_DOCUMENT_TYPE = "INVOICE";
    private static final String DEFAULT_ASSET = "Asset";
    private static final String DEFAULT_COST_CENTER = "CostCenter";
    private static final String DEFAULT_MATERIAL = "Material";
    private static final String DEFAULT_ORDER_NUMBER = "OrderNumber";
    private static final String DEFAULT_TAX_CODE = "TaxCode";
    private static final String DEFAULT_WITH_HOLDING_TAX_CODE = "WithholdingTaxCode";
    private static final LocalDateTime DEFAULT_LOCAL_DATE_TIME_NOW = LocalDateTime.now();
    
    @Before
    public void setUp() {
	
    }
    
    @Test
    public void toCreateSupplierFinancialDocumentRequestDTOTest() {
	
	final TaxMsDTO tax = new TaxMsDTO();
	tax.code = DEFAULT_TAX_CODE;
	tax.taxPercentage = "1.0";
	final List<TaxMsDTO> taxes = new ArrayList<TaxMsDTO>();
	taxes.add(tax);
	
	CreateSupplierFinancialDocumentRequestDTO requestMull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestDTO(null, null, null);
	Assert.assertNull(requestMull);
	requestMull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestDTO(null, taxes, DEFAULT_DATE_FORMATTER);
	Assert.assertNull(requestMull);
	requestMull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestDTO(new FinancialDocument(), null, DEFAULT_DATE_FORMATTER);
	Assert.assertNull(requestMull);
	requestMull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestDTO(null, null, DEFAULT_DATE_FORMATTER);
	Assert.assertNull(requestMull); 
	
	final ItemFD itemFD = new ItemFD();
	itemFD.amount = 1D;
	itemFD.articleCode = DEFAULT_MATERIAL;
	itemFD.asset = DEFAULT_ASSET;
	itemFD.costCenterCode = DEFAULT_COST_CENTER;
	itemFD.id = 1L;
	itemFD.internalOrderCode = DEFAULT_ORDER_NUMBER;
	itemFD.startDate = DEFAULT_LOCAL_DATE_TIME_NOW.minusDays(1L);
	itemFD.taxCode = DEFAULT_TAX_CODE;
	itemFD.withholdingTaxCode = DEFAULT_WITH_HOLDING_TAX_CODE;

	final FinancialDocument financialDocument = new FinancialDocument();
	financialDocument.approvalDate = DEFAULT_LOCAL_DATE_TIME_NOW.minusDays(1L);
	financialDocument.documentNumber = DEFAULT_DOCUMENT_NUMBER;
	financialDocument.ecmNumber = DEFAULT_DOCUMENT_EXTERNAL_ID;
	financialDocument.granting = Boolean.FALSE;
	financialDocument.purchaseOrderFriendlyNumber = DEFAULT_IDENTIFIER_PURCHASE_ORDER;
	financialDocument.supplierCode = DEFAULT_SUPPLIER_NUMBER;
	financialDocument.supplierNIF = DEFAULT_SUPPLIER_VAT_NUMBER;
	financialDocument.type = DEFAULT_FINANCIAL_DOCUMENT_TYPE;
	financialDocument.items.add(itemFD);
	
	CreateSupplierFinancialDocumentRequestDTO mappedRequest = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestDTO(financialDocument, taxes, DEFAULT_DATE_FORMATTER);
	Assert.assertEquals(mappedRequest.getDocumentExternalId(), financialDocument.ecmNumber);
	Assert.assertEquals(mappedRequest.getDocumentNumber(), financialDocument.documentNumber);
	Assert.assertEquals(mappedRequest.getIdentifierPurchaseOrder(), financialDocument.purchaseOrderFriendlyNumber);
	Assert.assertEquals(mappedRequest.getSupplierNumber(), financialDocument.supplierCode);
	Assert.assertEquals(mappedRequest.getTypeFinancialDocument(), FinancialDocumentType.valueOf(financialDocument.type).getValue());
	Assert.assertEquals(mappedRequest.getItems().get(0).getAsset(), financialDocument.items.get(0).asset);
	Assert.assertEquals(mappedRequest.getItems().get(0).getCostCenter(), financialDocument.items.get(0).costCenterCode);
	Assert.assertEquals(mappedRequest.getItems().get(0).getItemId(), Long.toString(financialDocument.items.get(0).id));
	Assert.assertEquals(mappedRequest.getItems().get(0).getMaterial(), financialDocument.items.get(0).articleCode);
	Assert.assertEquals(mappedRequest.getItems().get(0).getNumberReleases(), "001");
	Assert.assertEquals(mappedRequest.getItems().get(0).getOrderNumber(), financialDocument.items.get(0).internalOrderCode);
	Assert.assertEquals(mappedRequest.getItems().get(0).getPeriod(), "0");
	Assert.assertEquals(mappedRequest.getItems().get(0).getPeriodValue(), "0");
	Assert.assertEquals(mappedRequest.getItems().get(0).getPostingDate(), financialDocument.items.get(0).startDate.format(DEFAULT_DATE_FORMATTER));
	Assert.assertEquals(mappedRequest.getItems().get(0).getTaxCode(), financialDocument.items.get(0).taxCode);
	
        final BigDecimal amount = new BigDecimal(financialDocument.items.get(0).amount);
        final BigDecimal unitPriceWithoutTax = new BigDecimal(financialDocument.items.get(0).unitPriceWithoutTax);
        final BigDecimal taxValueBD = new BigDecimal(Double.parseDouble(tax.taxPercentage));
	Assert.assertEquals(mappedRequest.getItems().get(0).getValue(), amount.multiply(unitPriceWithoutTax).multiply(taxValueBD.multiply(new BigDecimal(0.01)).add(new BigDecimal(1))).setScale(2, RoundingMode.HALF_UP).toString());
	
	Assert.assertEquals(mappedRequest.getItems().get(0).getWithholdingTaxCode(), financialDocument.items.get(0).withholdingTaxCode);
	Assert.assertEquals(mappedRequest.getSupplierVatNumber(), "PT" + financialDocument.supplierNIF);
	
	financialDocument.supplierNIF = "PT" + DEFAULT_SUPPLIER_VAT_NUMBER;
	mappedRequest = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestDTO(financialDocument, taxes, DEFAULT_DATE_FORMATTER);
	Assert.assertEquals(mappedRequest.getSupplierVatNumber(), financialDocument.supplierNIF);	
	
    }
    
    @Test
    public void toCreateSupplierFinancialDocumentRequestItemDTOListTest() {
	
	final TaxMsDTO tax = new TaxMsDTO();
	tax.code = DEFAULT_TAX_CODE;
	tax.taxPercentage = "1.0";
	final List<TaxMsDTO> taxes = new ArrayList<TaxMsDTO>();
	taxes.add(tax);
	
	List<CreateSupplierFinancialDocumentRequestItemDTO> itemsNull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTOList(null, null, Boolean.FALSE, null, null, null);
	Assert.assertNull(itemsNull);
	itemsNull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTOList(null, DEFAULT_FINANCIAL_DOCUMENT_TYPE, Boolean.FALSE, DEFAULT_LOCAL_DATE_TIME_NOW, taxes, DEFAULT_DATE_FORMATTER);
	Assert.assertNull(itemsNull);
	itemsNull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTOList(new ArrayList<ItemFD>(), null, Boolean.FALSE, DEFAULT_LOCAL_DATE_TIME_NOW, taxes, DEFAULT_DATE_FORMATTER);
	Assert.assertNull(itemsNull);
	itemsNull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTOList(new ArrayList<ItemFD>(), DEFAULT_FINANCIAL_DOCUMENT_TYPE, Boolean.FALSE, null, taxes, DEFAULT_DATE_FORMATTER);
	Assert.assertNull(itemsNull);
	itemsNull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTOList(new ArrayList<ItemFD>(), DEFAULT_FINANCIAL_DOCUMENT_TYPE, Boolean.FALSE, DEFAULT_LOCAL_DATE_TIME_NOW, null, DEFAULT_DATE_FORMATTER);
	Assert.assertNull(itemsNull);
	itemsNull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTOList(new ArrayList<ItemFD>(), DEFAULT_FINANCIAL_DOCUMENT_TYPE, Boolean.FALSE, DEFAULT_LOCAL_DATE_TIME_NOW, taxes, null);
	Assert.assertNull(itemsNull);
	
	final ItemFD itemFD = new ItemFD();
	itemFD.amount = 1D;
	itemFD.articleCode = DEFAULT_MATERIAL;
	itemFD.asset = DEFAULT_ASSET;
	itemFD.costCenterCode = DEFAULT_COST_CENTER;
	itemFD.id = 1L;
	itemFD.internalOrderCode = DEFAULT_ORDER_NUMBER;
	itemFD.startDate = DEFAULT_LOCAL_DATE_TIME_NOW.minusDays(1L);
	itemFD.taxCode = DEFAULT_TAX_CODE;
	itemFD.withholdingTaxCode = DEFAULT_WITH_HOLDING_TAX_CODE;
	
	final List<ItemFD> itemsFDList = new ArrayList<ItemFD>();
	itemsFDList.add(itemFD);
	List<CreateSupplierFinancialDocumentRequestItemDTO> items = 
		SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTOList(itemsFDList, DEFAULT_FINANCIAL_DOCUMENT_TYPE, Boolean.FALSE, DEFAULT_LOCAL_DATE_TIME_NOW, taxes, DEFAULT_DATE_FORMATTER);
	Assert.assertEquals(items.get(0).getAsset(), itemsFDList.get(0).asset);
	Assert.assertEquals(items.get(0).getCostCenter(), itemsFDList.get(0).costCenterCode);
	Assert.assertEquals(items.get(0).getItemId(), Long.toString(itemsFDList.get(0).id));
	Assert.assertEquals(items.get(0).getMaterial(), itemsFDList.get(0).articleCode);
	Assert.assertEquals(items.get(0).getNumberReleases(), "001");
	Assert.assertEquals(items.get(0).getOrderNumber(), itemsFDList.get(0).internalOrderCode);
	Assert.assertEquals(items.get(0).getPeriod(), "0");
	Assert.assertEquals(items.get(0).getPeriodValue(), "0");
	Assert.assertEquals(items.get(0).getPostingDate(), DEFAULT_LOCAL_DATE_TIME_NOW.format(DEFAULT_DATE_FORMATTER));
	Assert.assertEquals(items.get(0).getTaxCode(), itemsFDList.get(0).taxCode);
	Assert.assertEquals(items.get(0).getWithholdingTaxCode(), itemsFDList.get(0).withholdingTaxCode);
	
        final BigDecimal amount = new BigDecimal(itemsFDList.get(0).amount);
        final BigDecimal unitPriceWithoutTax = new BigDecimal(itemsFDList.get(0).unitPriceWithoutTax);
        final BigDecimal taxValueBD = new BigDecimal(Double.parseDouble(tax.taxPercentage));
	Assert.assertEquals(items.get(0).getValue(), amount.multiply(unitPriceWithoutTax).multiply(taxValueBD.multiply(new BigDecimal(0.01)).add(new BigDecimal(1))).setScale(2, RoundingMode.HALF_UP).toString());
	
	items = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTOList(itemsFDList, DEFAULT_FINANCIAL_DOCUMENT_TYPE, Boolean.TRUE, DEFAULT_LOCAL_DATE_TIME_NOW, taxes, DEFAULT_DATE_FORMATTER);
	Assert.assertEquals(items.get(0).getPostingDate(), itemsFDList.get(0).startDate.format(DEFAULT_DATE_FORMATTER));
	
    }
    
    @Test
    public void toCreateSupplierFinancialDocumentRequestItemDTOTest() {
	
	final TaxMsDTO tax = new TaxMsDTO();
	tax.code = DEFAULT_TAX_CODE;
	tax.taxPercentage = "1.0";
	final List<TaxMsDTO> taxes = new ArrayList<TaxMsDTO>();
	taxes.add(tax);
	
	CreateSupplierFinancialDocumentRequestItemDTO itemNull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTO(null, null, Boolean.FALSE, null, null, null);
	Assert.assertNull(itemNull);
	itemNull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTO(null, DEFAULT_FINANCIAL_DOCUMENT_TYPE, Boolean.FALSE, DEFAULT_LOCAL_DATE_TIME_NOW, taxes, DEFAULT_DATE_FORMATTER);
	Assert.assertNull(itemNull);
	itemNull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTO(new ItemFD(), null, Boolean.FALSE, DEFAULT_LOCAL_DATE_TIME_NOW, taxes, DEFAULT_DATE_FORMATTER);
	Assert.assertNull(itemNull);
	itemNull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTO(new ItemFD(), DEFAULT_FINANCIAL_DOCUMENT_TYPE, Boolean.FALSE, null, taxes, DEFAULT_DATE_FORMATTER);
	Assert.assertNull(itemNull);
	itemNull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTO(new ItemFD(), DEFAULT_FINANCIAL_DOCUMENT_TYPE, Boolean.FALSE, DEFAULT_LOCAL_DATE_TIME_NOW, null, DEFAULT_DATE_FORMATTER);
	Assert.assertNull(itemNull);
	itemNull = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTO(new ItemFD(), DEFAULT_FINANCIAL_DOCUMENT_TYPE, Boolean.FALSE, DEFAULT_LOCAL_DATE_TIME_NOW, taxes, null);
	Assert.assertNull(itemNull);
	
	final ItemFD itemFD = new ItemFD();
	itemFD.amount = 1D;
	itemFD.articleCode = DEFAULT_MATERIAL;
	itemFD.asset = DEFAULT_ASSET;
	itemFD.costCenterCode = DEFAULT_COST_CENTER;
	itemFD.id = 1L;
	itemFD.internalOrderCode = DEFAULT_ORDER_NUMBER;
	itemFD.startDate = DEFAULT_LOCAL_DATE_TIME_NOW.minusDays(1L);
	itemFD.taxCode = DEFAULT_TAX_CODE;
	itemFD.withholdingTaxCode = DEFAULT_WITH_HOLDING_TAX_CODE;
	
	CreateSupplierFinancialDocumentRequestItemDTO item = 
		SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTO(itemFD, DEFAULT_FINANCIAL_DOCUMENT_TYPE, Boolean.FALSE, DEFAULT_LOCAL_DATE_TIME_NOW, taxes, DEFAULT_DATE_FORMATTER);
	Assert.assertEquals(item.getAsset(), itemFD.asset);
	Assert.assertEquals(item.getCostCenter(), itemFD.costCenterCode);
	Assert.assertEquals(item.getItemId(), Long.toString(itemFD.id));
	Assert.assertEquals(item.getMaterial(), itemFD.articleCode);
	Assert.assertEquals(item.getNumberReleases(), "001");
	Assert.assertEquals(item.getOrderNumber(), itemFD.internalOrderCode);
	Assert.assertEquals(item.getPeriod(), "0");
	Assert.assertEquals(item.getPeriodValue(), "0");
	Assert.assertEquals(item.getPostingDate(), DEFAULT_LOCAL_DATE_TIME_NOW.format(DEFAULT_DATE_FORMATTER));
	Assert.assertEquals(item.getTaxCode(), itemFD.taxCode);
	Assert.assertEquals(item.getWithholdingTaxCode(), itemFD.withholdingTaxCode);
	
        final BigDecimal amount = new BigDecimal(itemFD.amount);
        final BigDecimal unitPriceWithoutTax = new BigDecimal(itemFD.unitPriceWithoutTax);
        final BigDecimal taxValueBD = new BigDecimal(Double.parseDouble(tax.taxPercentage));
	Assert.assertEquals(item.getValue(), amount.multiply(unitPriceWithoutTax).multiply(taxValueBD.multiply(new BigDecimal(0.01)).add(new BigDecimal(1))).setScale(2, RoundingMode.HALF_UP).toString());
	
	item = SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestItemDTO(itemFD, DEFAULT_FINANCIAL_DOCUMENT_TYPE, Boolean.TRUE, DEFAULT_LOCAL_DATE_TIME_NOW, taxes, DEFAULT_DATE_FORMATTER);
	Assert.assertEquals(item.getPostingDate(), itemFD.startDate.format(DEFAULT_DATE_FORMATTER));
	
    }
    
}
