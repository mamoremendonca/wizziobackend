/*-
 * #%L
 * Modules :: SAP Integration Microservice  Data RI
 * %%
 * Copyright (C) 2017 - 2020 Financial Services
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Novabase Business Solutions S.A. and its licensors. The disclosure,
 * copying, adaptation, citation, transcription, translation, modification,
 * decompilation, reverse engineering, derivatives, integration, development and/or
 * any other form of total or partial use of the content of this document and/or
 * accessible through or via the contents, by any possible means without the
 * respective authorization or licensing by the owner of the intellectual property
 * rights is prohibited, the offenders being subject to civil and/or criminal
 * prosecution and liability. The user or licensee of all or part of this document
 * by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.sapservice.data.ri.sapservice;

import static io.digitaljourney.platform.modules.paxexam.BundleOptions.mavenBundles;
import static io.digitaljourney.platform.modules.paxexam.BundleOptions.rsClient;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.newConfiguration;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.rsClientConfiguration;
import static org.ops4j.pax.exam.CoreOptions.composite;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;

import io.digitaljourney.platform.modules.paxexam.base.BaseTestSupport;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.SAPServiceDAOConfig;
import com.novabase.omni.oney.modules.sapservice.data.api.SAPServiceDAO;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class SAPServiceDAOIT extends BaseTestSupport {

  @SuppressWarnings("unused")
  @Inject
  private SAPServiceDAO dao;

  @Override
  protected Option bundles() {
    return composite(super.bundles(), rsClient(),
        mavenBundles("com.novabase.omni.oney.modules"),
        testBundle("com.novabase.omni.oney.modules", "sapservice-data-ri"));
  }

  @Override
  protected Option configurations() {
    return composite(super.configurations(),
        rsClientConfiguration("sapservice", newConfiguration(SAPServiceDAOConfig.CPID)));
  }

  @Test
  public void testBundle() {
    assertBundleActive("com.novabase.omni.oney.modules.sapservice-data-ri");
  }

}
