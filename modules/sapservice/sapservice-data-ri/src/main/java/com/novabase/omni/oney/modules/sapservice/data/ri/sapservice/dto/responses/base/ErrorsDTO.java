/*-
 * #%L
 * Modules :: SAP Integration Microservice  Data RI
 * %%
 * Copyright (C) 2017 - 2020 Financial Services
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Novabase Business Solutions S.A. and its licensors. The disclosure,
 * copying, adaptation, citation, transcription, translation, modification,
 * decompilation, reverse engineering, derivatives, integration, development and/or
 * any other form of total or partial use of the content of this document and/or
 * accessible through or via the contents, by any possible means without the
 * respective authorization or licensing by the owner of the intellectual property
 * rights is prohibited, the offenders being subject to civil and/or criminal
 * prosecution and liability. The user or licensee of all or part of this document
 * by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.responses.base;

import java.util.HashMap;
import java.util.Map;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorsDTO extends DTO {

    @JsonProperty("Error")
    private Map<String, String> error;

    public ErrorsDTO() {

	super();

	this.error = new HashMap<String, String>();

    }

    public Map<String, String> getError() {
	return error;
    }

    public void setError(final Map<String, String> error) {
	this.error = error;
    }
    
}
