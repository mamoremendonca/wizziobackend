package com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.responses;

import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.responses.base.BaseResponseDTO;

public class CreateSupplierFinancialDocumentResponseDTO extends BaseResponseDTO {

    public CreateSupplierFinancialDocumentResponseDTO() {
	super();
    }
    
}
