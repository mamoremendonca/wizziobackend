/*-
 * #%L
 * Modules :: SAP Integration Microservice  Data RI
 * %%
 * Copyright (C) 2017 - 2020 Financial Services
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Novabase Business Solutions S.A. and its licensors. The disclosure,
 * copying, adaptation, citation, transcription, translation, modification,
 * decompilation, reverse engineering, derivatives, integration, development and/or
 * any other form of total or partial use of the content of this document and/or
 * accessible through or via the contents, by any possible means without the
 * respective authorization or licensing by the owner of the intellectual property
 * rights is prohibited, the offenders being subject to civil and/or criminal
 * prosecution and liability. The user or licensee of all or part of this document
 * by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.modules.referencedata.service.api.dto.TaxMsDTO;
import com.novabase.omni.oney.modules.sapservice.data.api.enums.FinancialDocumentType;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.requests.CreatePurchaseOrderRequestDTO;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.requests.CreatePurchaseOrderRequestItemDTO;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.requests.CreateSupplierFinancialDocumentRequestDTO;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.requests.CreateSupplierFinancialDocumentRequestItemDTO;
import com.novabase.omni.oney.modules.sapservice.entity.FinancialDocument;
import com.novabase.omni.oney.modules.sapservice.entity.ItemFD;
import com.novabase.omni.oney.modules.sapservice.entity.ItemPO;
import com.novabase.omni.oney.modules.sapservice.entity.PurchaseOrder;

@Mapper
public interface SAPServiceDAOMapper {

    public static final SAPServiceDAOMapper INSTANCE = Mappers.getMapper(SAPServiceDAOMapper.class);

    public default CreateSupplierFinancialDocumentRequestDTO toCreateSupplierFinancialDocumentRequestDTO(final FinancialDocument financialDocument, final List<TaxMsDTO> taxes, final DateTimeFormatter dateTimeFormatter) {
	
	if (financialDocument == null || dateTimeFormatter == null || taxes == null) {
	    return null;
	}
	
	final CreateSupplierFinancialDocumentRequestDTO request = new CreateSupplierFinancialDocumentRequestDTO();
	
	request.setIdentifierPurchaseOrder(financialDocument.purchaseOrderFriendlyNumber);
	request.setSupplierNumber(financialDocument.supplierCode);
	
	request.setSupplierVatNumber(financialDocument.supplierNIF);
	if (StringUtils.contains(request.getSupplierVatNumber(), "PT") == false) {
	    request.setSupplierVatNumber("PT" + request.getSupplierVatNumber());       
	}
	
	request.setDeferral(financialDocument.granting ? "1" : "0");
	request.setDocumentExternalId(financialDocument.ecmNumber);
	request.setDocumentNumber(financialDocument.documentNumber);
	request.setTypeFinancialDocument(FinancialDocumentType.valueOf(financialDocument.type).getValue());
	request.setItems(this.toCreateSupplierFinancialDocumentRequestItemDTOList(financialDocument.items, financialDocument.type, financialDocument.granting, financialDocument.approvalDate, taxes, dateTimeFormatter));
	
	return request;
	
    }
    
    public default List<CreateSupplierFinancialDocumentRequestItemDTO> toCreateSupplierFinancialDocumentRequestItemDTOList(final List<ItemFD> items, final String financialDocumentType, final boolean financialDocumentGranting, final LocalDateTime financialDocumentApprovalDate, final List<TaxMsDTO> taxes, final DateTimeFormatter dateTimeFormatter) {
	
	if (items == null || StringUtils.isBlank(financialDocumentType) || financialDocumentApprovalDate == null || dateTimeFormatter == null || taxes == null) {
	    return null;
	}
	
	// cannot use paralelStream in this case to assure index order.
	return items.stream().
		map(item -> this.toCreateSupplierFinancialDocumentRequestItemDTO(item, financialDocumentType, financialDocumentGranting, financialDocumentApprovalDate, taxes, dateTimeFormatter)).
		collect(Collectors.toList());
	
    }
    
    public default CreateSupplierFinancialDocumentRequestItemDTO toCreateSupplierFinancialDocumentRequestItemDTO(final ItemFD itemDTO, final String financialDocumentType, final boolean financialDocumentGranting, final LocalDateTime financialDocumentApprovalDate, final List<TaxMsDTO> taxes, final DateTimeFormatter dateTimeFormatter) {
        
	if (itemDTO == null || StringUtils.isBlank(financialDocumentType) || financialDocumentApprovalDate == null || dateTimeFormatter == null || taxes == null) {
	    return null;
	}
	
        final CreateSupplierFinancialDocumentRequestItemDTO item = new CreateSupplierFinancialDocumentRequestItemDTO();
        
        item.setItemId(itemDTO.id.toString());
        item.setMaterial(itemDTO.articleCode == null ? StringUtils.EMPTY : itemDTO.articleCode);
        item.setAsset(itemDTO.asset == null ? StringUtils.EMPTY : itemDTO.asset);
        item.setOrderNumber(itemDTO.internalOrderCode == null ? StringUtils.EMPTY : itemDTO.internalOrderCode);
        item.setCostCenter(itemDTO.costCenterCode == null ? StringUtils.EMPTY : itemDTO.costCenterCode);
        item.setWithholdingTaxCode(itemDTO.withholdingTaxCode == null ? StringUtils.EMPTY : itemDTO.withholdingTaxCode);
        item.setTaxCode(itemDTO.taxCode == null ? StringUtils.EMPTY : itemDTO.taxCode);
        
        final BigDecimal amount = new BigDecimal(itemDTO.amount);
        final BigDecimal unitPriceWithoutTax = new BigDecimal(itemDTO.unitPriceWithoutTax);
        
        final TaxMsDTO taxFound = taxes.parallelStream().
        				filter(tax -> tax.code.equals(itemDTO.taxCode)).
        				findAny().
        				orElse(null);
        final BigDecimal taxValueBD = new BigDecimal(Double.parseDouble(taxFound.taxPercentage));
        
        // (amount * unitPriceWithoutTax) * ((taxValue * 0.01) + 1)
        // final Double taxedValueD = (itemDTO.amount * itemDTO.unitPriceWithoutTax) * ((taxValueBDD * 0.01D) + 1D);
        final BigDecimal taxedValue = amount.multiply(unitPriceWithoutTax).multiply(taxValueBD.multiply(new BigDecimal(0.01)).add(new BigDecimal(1))).setScale(2, RoundingMode.HALF_UP);
	item.setValue(taxedValue.toString());
	
        item.setPostingDate(financialDocumentGranting ? itemDTO.startDate.format(dateTimeFormatter) : financialDocumentApprovalDate.format(dateTimeFormatter));
        item.setNumberReleases("001");
        item.setPeriod("0");
        item.setPeriodValue("0");
        
        return item;
          
    }
    
    public default CreatePurchaseOrderRequestDTO toCreatePurchaseOrderRequestDTO(final PurchaseOrder purchaseOrder, final DateTimeFormatter dateTimeFormatter) {

	if (purchaseOrder == null) {
	    return null;
	}
	
	final CreatePurchaseOrderRequestDTO request = new CreatePurchaseOrderRequestDTO();

	request.setIdentifierPurchaseOrder(purchaseOrder.friendlyNumber);
	request.setSupplierNumber(purchaseOrder.supplierCode);
	request.setSupplierVatNumber(purchaseOrder.supplierNIF);
	request.setTypePurchaseOrder(purchaseOrder.type);
	request.setItems(this.toCreatePurchaseOrderRequestItemDTOList(purchaseOrder.items, purchaseOrder.type, dateTimeFormatter));

	return request;

    }
      
    public default List<CreatePurchaseOrderRequestItemDTO> toCreatePurchaseOrderRequestItemDTOList(final List<ItemPO> items, final String purchaseOrderType, final DateTimeFormatter dateTimeFormatter) {
	
	if (items == null) {
	    return null;
	}
	
	// cannot use paralelStream in this case to assure index order.
	return items.stream().
		map(item -> this.toCreatePurchaseOrderRequestItemDTO(item, purchaseOrderType, dateTimeFormatter)).
		collect(Collectors.toList());
	
    }

    public default CreatePurchaseOrderRequestItemDTO toCreatePurchaseOrderRequestItemDTO(final ItemPO itemDTO, final String purchaseOrderType, final DateTimeFormatter dateTimeFormatter) {

	if (itemDTO == null) {
	    return null;
	}
	
	final CreatePurchaseOrderRequestItemDTO item = new CreatePurchaseOrderRequestItemDTO();

	item.setItemId(itemDTO.id.toString());
	item.setMaterial("");
	item.setAsset("");
	item.setOrderNumber(itemDTO.internalOrderCode);
	item.setCostCenter("");
	item.setValue(new BigDecimal(itemDTO.unitPriceWithoutTax).setScale(2, RoundingMode.HALF_UP).toString());
	item.setQuantity(new BigDecimal(itemDTO.amount).setScale(2, RoundingMode.HALF_UP).toString());
	item.setTaxCode(itemDTO.taxCode);
	item.setDebitCredit("1");

	if (!purchaseOrderType.equals("CAPEX")) {

	    item.setPostingDate(LocalDateTime.now().format(dateTimeFormatter)); // TODO: verificar com a Oney por que nao aceita datas anteriores a hoje,
										// deveria ser a data de aprovação da PO e nao a data atual
	    item.setMaterial(itemDTO.articleCode);
	    item.setCostCenter(itemDTO.costCenterCode);
	    item.setPeriod("0");
	    item.setPeriodValue("0");

	}

	if (purchaseOrderType.equals("CONTRATO")) {

	    item.setPostingDate(itemDTO.startDate.format(dateTimeFormatter));
	    item.setNumberReleases("001");
	    item.setPeriod("0");
	    item.setPeriodValue("0");

	}

	return item;

    }

}
