/*-
 * #%L
 * Modules :: SAP Integration Microservice  Data RI
 * %%
 * Copyright (C) 2017 - 2020 Financial Services
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Novabase Business Solutions S.A. and its licensors. The disclosure,
 * copying, adaptation, citation, transcription, translation, modification,
 * decompilation, reverse engineering, derivatives, integration, development and/or
 * any other form of total or partial use of the content of this document and/or
 * accessible through or via the contents, by any possible means without the
 * respective authorization or licensing by the owner of the intellectual property
 * rights is prohibited, the offenders being subject to civil and/or criminal
 * prosecution and liability. The user or licensee of all or part of this document
 * by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.responses.base;

import java.util.ArrayList;
import java.util.List;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BaseResponseDTO extends DTO {

    @JsonProperty("Success")
    private Boolean success;
    private String code;
    private String message;
    private String severity;
    private String target;
    private Boolean transition;
//    private List<String> details; Commented because the service return an complex object instead of a String. This object is not documented, so we didn't map it.
    @JsonProperty("Errors")
    private ErrorsDTO errors;
    
    public BaseResponseDTO() {
	
	super();
	
	this.errors = new ErrorsDTO();
	this.success = Boolean.FALSE;

//	this.details = new ArrayList<String>();
	
    }
    
    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Boolean getTransition() {
        return transition;
    }

    public void setTransition(Boolean transition) {
        this.transition = transition;
    }

//    public List<String> getDetails() {
//        return details;
//    }
//
//    public void setDetails(List<String> details) {
//        this.details = details;
//    }

    public Boolean getSuccess() {
        return success;
    }
    
    public void setSuccess(Boolean success) {
        this.success = success;
    }
    
    public String getCode() {
        return code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public ErrorsDTO getErrors() {
        return errors;
    }
    
    public void setErrors(ErrorsDTO errors) {
        this.errors = errors;
    }
    
}
