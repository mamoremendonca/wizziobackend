/*-
 * #%L
 * Modules :: SAP Integration Microservice  Data RI
 * %%
 * Copyright (C) 2017 - 2020 Financial Services
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Novabase Business Solutions S.A. and its licensors. The disclosure,
 * copying, adaptation, citation, transcription, translation, modification,
 * decompilation, reverse engineering, derivatives, integration, development and/or
 * any other form of total or partial use of the content of this document and/or
 * accessible through or via the contents, by any possible means without the
 * respective authorization or licensing by the owner of the intellectual property
 * rights is prohibited, the offenders being subject to civil and/or criminal
 * prosecution and liability. The user or licensee of all or part of this document
 * by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.requests;

import java.util.ArrayList;
import java.util.List;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class CreatePurchaseOrderRequestDTO extends DTO {

    @JsonProperty("TypePo")
    private String typePurchaseOrder;
    @JsonProperty("IdentifierPo")
    private String identifierPurchaseOrder;
    @JsonProperty("SupplierNr")
    private String supplierNumber;
    @JsonProperty("SupplierVatNr")
    private String supplierVatNumber;
    @JsonProperty("Items")
    private List<CreatePurchaseOrderRequestItemDTO> items;

    public CreatePurchaseOrderRequestDTO() {

	super();

	this.items = new ArrayList<CreatePurchaseOrderRequestItemDTO>();

    }

    public String getTypePurchaseOrder() {
	return typePurchaseOrder;
    }

    public void setTypePurchaseOrder(String typePurchaseOrder) {
	this.typePurchaseOrder = typePurchaseOrder;
    }

    public String getIdentifierPurchaseOrder() {
	return identifierPurchaseOrder;
    }

    public void setIdentifierPurchaseOrder(String identifierPurchaseOrder) {
	this.identifierPurchaseOrder = identifierPurchaseOrder;
    }

    public String getSupplierNumber() {
	return supplierNumber;
    }

    public void setSupplierNumber(String supplierNumber) {
	this.supplierNumber = supplierNumber;
    }

    public String getSupplierVatNumber() {
	return supplierVatNumber;
    }

    public void setSupplierVatNumber(String supplierVatNumber) {
	this.supplierVatNumber = supplierVatNumber;
    }

    public List<CreatePurchaseOrderRequestItemDTO> getItems() {
	return items;
    }

    public void setItems(List<CreatePurchaseOrderRequestItemDTO> items) {
	this.items = items;
    }

}
