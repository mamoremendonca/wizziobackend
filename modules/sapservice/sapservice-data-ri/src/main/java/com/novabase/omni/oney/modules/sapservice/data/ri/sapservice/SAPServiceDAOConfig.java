/*-
 * #%L
 * Modules :: SAP Integration Microservice  Data RI
 * %%
 * Copyright (C) 2017 - 2020 Financial Services
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Novabase Business Solutions S.A. and its licensors. The disclosure,
 * copying, adaptation, citation, transcription, translation, modification,
 * decompilation, reverse engineering, derivatives, integration, development and/or
 * any other form of total or partial use of the content of this document and/or
 * accessible through or via the contents, by any possible means without the
 * respective authorization or licensing by the owner of the intellectual property
 * rights is prohibited, the offenders being subject to civil and/or criminal
 * prosecution and liability. The user or licensee of all or part of this document
 * by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.sapservice.data.ri.sapservice;

import javax.ws.rs.core.MediaType;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/sapservice/sapservice", icon = @Icon(resource = "OSGI-INF/icon/ws.png", size = 32))
public @interface SAPServiceDAOConfig {

  public static final String CPID = "com.novabase.omni.oney.modules.sapservice.data.ri.sapservice";

  @AttributeDefinition(name = "%providerName.name", description = "%providerName.description")
  String providerName() default "Oney SAP Service";

  @AttributeDefinition(name = "%address.name", description = "%address.description")
  String address() default "https://eaidev.oney.pt";

  @AttributeDefinition(name = "%contentType.name", description = "%contentType.description")
  String contentType() default MediaType.APPLICATION_JSON;

  @AttributeDefinition(name = "%acceptType.name", description = "%acceptType.description")
  String acceptType() default MediaType.APPLICATION_JSON;

  @AttributeDefinition(name = "%userName.name", description = "%userName.description")
  String userName() default "";

  @AttributeDefinition(name = "%password.name", type = AttributeType.PASSWORD, description = "%password.description")
  String password() default "";

  @AttributeDefinition(name = "%connectionTimeout.name", type = AttributeType.LONG, min = "0", description = "%connectionTimeout.description")
  long connectionTimeout() default 10000;

  @AttributeDefinition(name = "%receiveTimeout.name", type = AttributeType.LONG, min = "0", description = "%receiveTimeout.description")
  long receiveTimeout() default 180000;

  @AttributeDefinition(name = "%proxyEnabled.name", type = AttributeType.BOOLEAN, description = "%proxyEnabled.description")
  boolean proxyEnabled() default false;

  @AttributeDefinition(name = "%proxyHost.name", description = "%proxyHost.description")
  String proxyHost() default "localhost";

  @AttributeDefinition(name = "%proxyPort.name", description = "%proxyPort.description")
  int proxyPort() default 8888;

  @AttributeDefinition(name = "%jsonPathSuccessExpression.name", description = "%jsonPathSuccessExpression.description")
  String jsonPathSuccessExpression() default "";

  @AttributeDefinition(name = "%serializationFeatures.name",
      description = "%serializationFeatures.description")
  String[] serializationFeatures() default {};

  @AttributeDefinition(name = "%deserializationFeatures.name",
      description = "%deserializationFeatures.description")
  String[] deserializationFeatures() default {"ACCEPT_EMPTY_STRING_AS_NULL_OBJECT=true", "FAIL_ON_UNKNOWN_PROPERTIES=false"};

  @AttributeDefinition(name = "%sapserviceDateFormat.name", description = "%sapserviceDateFormat.description")
  String sapServiceDateFormat() default "yyyyMMdd";

  @AttributeDefinition(name = "%resourcePostCreatePurchaseOrder.name", description = "%resourcePostCreatePurchaseOrder.description")
  String resourcePostCreatePurchaseOrder() default "/CORPORATE/API/createPurchaseOrder/create";

  @AttributeDefinition(name = "%resourcePostCreateFinancialDocument.name", description = "%resourcePostCreateFinancialDocument.description")
  String resourcePostCreateFinancialDocument() default "/CORPORATE/API/createSupplierFinancialDocument/create";
  
  @AttributeDefinition(name = "%headersApplication.name", description = "%headersApplication.description")
  String headersApplication() default "WIZZIO";
  
  @AttributeDefinition(name = "%headersChannel.name", description = "%headersChannel.description")
  String headersChannel() default "WEB";
  
}
