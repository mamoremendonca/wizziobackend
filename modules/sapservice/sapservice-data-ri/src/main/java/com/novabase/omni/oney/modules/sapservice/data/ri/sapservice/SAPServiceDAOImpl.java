/*-
 * #%L
 * Modules :: SAP Integration Microservice  Data RI
 * %%
 * Copyright (C) 2017 - 2020 Financial Services
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Novabase Business Solutions S.A. and its licensors. The disclosure,
 * copying, adaptation, citation, transcription, translation, modification,
 * decompilation, reverse engineering, derivatives, integration, development and/or
 * any other form of total or partial use of the content of this document and/or
 * accessible through or via the contents, by any possible means without the
 * respective authorization or licensing by the owner of the intellectual property
 * rights is prohibited, the offenders being subject to civil and/or criminal
 * prosecution and liability. The user or licensee of all or part of this document
 * by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.sapservice.data.ri.sapservice;

import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.TaxMsDTO;
import com.novabase.omni.oney.modules.sapservice.data.api.SAPServiceDAO;
import com.novabase.omni.oney.modules.sapservice.data.api.exceptions.SAPServiceCreatePurchaseOrderException;
import com.novabase.omni.oney.modules.sapservice.data.api.exceptions.SAPServiceCreateSupplierFinancialDocumentException;
import com.novabase.omni.oney.modules.sapservice.data.ri.WSContext;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.requests.CreatePurchaseOrderRequestWrapperDTO;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.requests.CreateSupplierFinancialDocumentRequestWrapperDTO;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.responses.CreatePurchaseOrderResponseWrapperDTO;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.responses.CreateSupplierFinancialDocumentResponseWrapperDTO;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.responses.base.BaseResponseDTO;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.responses.base.ResponseWrapper;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.mapper.SAPServiceDAOMapper;
import com.novabase.omni.oney.modules.sapservice.entity.FinancialDocument;
import com.novabase.omni.oney.modules.sapservice.entity.PurchaseOrder;

import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;

// @formatter:off
@Component(
  configurationPid = SAPServiceDAOConfig.CPID,
  configurationPolicy = ConfigurationPolicy.REQUIRE,
  reference = {
    @Reference(
      name = RSProperties.REF_CONTEXT,
      service = WSContext.class, 
      cardinality = ReferenceCardinality.MANDATORY) })
@Designate(ocd = SAPServiceDAOConfig.class)
// @formatter:on

public final class SAPServiceDAOImpl extends AbstractSAPServiceDAO<SAPServiceDAOConfig> implements SAPServiceDAO {

    @Activate
    public void activate(ComponentContext ctx, SAPServiceDAOConfig config) {
	prepare(ctx, config);
    }

    @Modified
    public void modified(SAPServiceDAOConfig config) {
	prepare(config);
    }

    @Override
    public Boolean submitPurchaseOrder(final PurchaseOrder purchaseOrder) throws SAPServiceCreatePurchaseOrderException {

	if (purchaseOrder == null) {
	    throw new SAPServiceCreatePurchaseOrderException("Purchase Cannot Be Null...");
	} else {

	    final CreatePurchaseOrderRequestWrapperDTO wrapper = new CreatePurchaseOrderRequestWrapperDTO();
	    wrapper.setCreatePurchaseOrderRequestDTO(SAPServiceDAOMapper.INSTANCE.toCreatePurchaseOrderRequestDTO(purchaseOrder, DateTimeFormatter.ofPattern(this.getConfig().sapServiceDateFormat())));
	    this.logRequest(wrapper);

	    final CreatePurchaseOrderResponseWrapperDTO responseWrapper = this.callPost(wrapper, CreatePurchaseOrderResponseWrapperDTO.class, this.getConfig().resourcePostCreatePurchaseOrder(),
		    Boolean.TRUE);
	    this.logResponse(responseWrapper);

	    return responseWrapper.getResponse().getSuccess();

	}

    }

    @Override
    public Boolean submitFinancialDocument(final FinancialDocument financialDocument, final List<TaxMsDTO> taxes) throws SAPServiceCreateSupplierFinancialDocumentException {

	if (financialDocument == null) {
	    throw new SAPServiceCreateSupplierFinancialDocumentException("Financial Document Cannot Be Null...");
	} else {

	    final CreateSupplierFinancialDocumentRequestWrapperDTO wrapper = new CreateSupplierFinancialDocumentRequestWrapperDTO();
	    wrapper.setCreateSupplierFinancialDocumentRequestDTO(
		    SAPServiceDAOMapper.INSTANCE.toCreateSupplierFinancialDocumentRequestDTO(financialDocument, taxes, DateTimeFormatter.ofPattern(this.getConfig().sapServiceDateFormat())));
	    this.logRequest(wrapper);

	    final CreateSupplierFinancialDocumentResponseWrapperDTO responseWrapper = this.callPost(wrapper, CreateSupplierFinancialDocumentResponseWrapperDTO.class,
		    this.getConfig().resourcePostCreateFinancialDocument(), Boolean.TRUE);
	    this.logResponse(responseWrapper);

	    return responseWrapper.getResponse().getSuccess();

	}

    }

    private void logRequest(final CreatePurchaseOrderRequestWrapperDTO wrapper) {

	String purchaseOrderIdentifier = StringUtils.EMPTY;
	try {

	    purchaseOrderIdentifier = wrapper.getCreatePurchaseOrderRequestDTO().getIdentifierPurchaseOrder();
	    this.info("Creating Purchase Order: " + purchaseOrderIdentifier);

	    String requestBody = new ObjectMapper().writeValueAsString(wrapper);
	    this.info("Create Purchase Order " + purchaseOrderIdentifier + " JSON: " + requestBody);

	} catch (JsonProcessingException e) {
	    this.warn("Error trying to log request body to create Purchase Order: " + purchaseOrderIdentifier);
	}

    }

    private void logRequest(final CreateSupplierFinancialDocumentRequestWrapperDTO wrapper) {

	String financialDocumentNumber = StringUtils.EMPTY;
	try {

	    financialDocumentNumber = wrapper.getCreateSupplierFinancialDocumentRequestDTO().getDocumentNumber();
	    this.info("Creating Financial Document: " + financialDocumentNumber);

	    String requestBody = new ObjectMapper().writeValueAsString(wrapper);
	    this.info("Create Financial Document " + financialDocumentNumber + " JSON: " + requestBody);

	} catch (JsonProcessingException e) {
	    this.warn("Error trying to log request body to create Financial Document: " + financialDocumentNumber);
	}

    }

    private <T extends BaseResponseDTO> void logResponse(final ResponseWrapper<T> responseWrapper) {

	try {

	    String responseBody = new ObjectMapper().writeValueAsString(responseWrapper);
	    this.info("Received Response JSON: " + responseBody);

	} catch (JsonProcessingException e) {
	    this.warn("Unable to parse to JSON Response Received: " + responseWrapper.toString());

	}

    }

    private <T> T callGet(final Class<T> responseClass, final String path, final Boolean defaultSuccessCode) {

	return this.get(responseClass, path, new SAPServiceDAOConfigWrapper(this.getConfig()), defaultSuccessCode); // TRUE to use WSData Default Success codes and Descriptions as this service
														    // does not return Success Data.

    }

    private <T, U> U callPost(final T request, final Class<U> responseClass, final String path, final Boolean defaultSuccessCode) {

	return this.post(request, responseClass, path, new SAPServiceDAOConfigWrapper(this.getConfig()), defaultSuccessCode); // TRUE to use WSData Default Success codes and Descriptions as this
															      // service does not return Success Data.

    }

}
