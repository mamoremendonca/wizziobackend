package com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.requests.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class BaseRequestItemDTO {

    @JsonProperty("ItemId")
    private String itemId;
    @JsonProperty("Material")
    private String material;
    @JsonProperty("Asset")
    private String asset;
    @JsonProperty("OrderNr")
    private String orderNumber;
    @JsonProperty("CostCenter")
    private String costCenter;
    @JsonProperty("TaxCode")
    private String taxCode;
    @JsonProperty("Value")
    private String value;
    @JsonProperty("PostingDate")
    private String postingDate;
    @JsonProperty("NumberReleases")
    private String numberReleases;
    @JsonProperty("Period")
    private String period;
    @JsonProperty("PeriodValue")
    private String periodValue;
    
    public BaseRequestItemDTO() {
	super();
    }
    
    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }
    
    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(String postingDate) {
        this.postingDate = postingDate;
    }

    public String getNumberReleases() {
        return numberReleases;
    }

    public void setNumberReleases(String numberReleases) {
        this.numberReleases = numberReleases;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPeriodValue() {
        return periodValue;
    }

    public void setPeriodValue(String periodValue) {
        this.periodValue = periodValue;
    }
    
}
