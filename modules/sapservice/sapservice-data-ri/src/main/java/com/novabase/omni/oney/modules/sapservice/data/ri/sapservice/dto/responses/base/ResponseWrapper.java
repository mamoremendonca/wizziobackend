package com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.responses.base;

public interface ResponseWrapper<T extends BaseResponseDTO> {

    public T getResponse();
    
}
