/*-
 * #%L
 * Modules :: SAP Integration Microservice  Data RI
 * %%
 * Copyright (C) 2017 - 2020 Financial Services
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Novabase Business Solutions S.A. and its licensors. The disclosure,
 * copying, adaptation, citation, transcription, translation, modification,
 * decompilation, reverse engineering, derivatives, integration, development and/or
 * any other form of total or partial use of the content of this document and/or
 * accessible through or via the contents, by any possible means without the
 * respective authorization or licensing by the owner of the intellectual property
 * rights is prohibited, the offenders being subject to civil and/or criminal
 * prosecution and liability. The user or licensee of all or part of this document
 * by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.sapservice.data.ri.sapservice;

import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.apache.cxf.jaxrs.client.WebClient;

import com.novabase.omni.oney.modules.sapservice.data.api.constants.SAPServiceDAOConstants;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.responses.base.BaseResponseDTO;
import com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.responses.base.ResponseWrapper;

import io.digitaljourney.platform.modules.ws.api.dao.WSData;
import io.digitaljourney.platform.modules.ws.api.dao.WSResult;
import io.digitaljourney.platform.modules.ws.rs.api.dao.AbstractRSDAO;

public abstract class AbstractSAPServiceDAO<A extends Annotation> extends AbstractRSDAO<A> {

    public AbstractSAPServiceDAO() {
	super();
    }

    protected <T, U> U post(final T request, final Class<U> responseClass, final String path, final SAPServiceDAOConfigWrapper config) {
	return this.post(request, responseClass, path, config, Boolean.FALSE);
    }

    protected <T, U> U post(final T request, final Class<U> responseClass, final String path, final SAPServiceDAOConfigWrapper config, final Boolean defaultResponseCodes) {

	final BiFunction<T, WebClient, U> call = (final T funcRequestWrapper, final WebClient funcClient) -> this.prepare(funcClient, path, config).post(funcRequestWrapper, responseClass);
	final WSData<U> response = this.call(request, call, defaultResponseCodes); // TRUE to use WSData Default Success codes and Descriptions as this service
										   // does not return Success Data.

	return this.validateResponse(response);

    }

    protected <T> T get(final Class<T> responseClass, final String path, final SAPServiceDAOConfigWrapper config) {
	return this.get(responseClass, path, config, Boolean.FALSE);
    }

    protected <T> T get(final Class<T> responseClass, final String path, final SAPServiceDAOConfigWrapper config, final Boolean defaultSuccessCode) {

	final Function<WebClient, T> call = (final WebClient funcClient) -> this.prepare(funcClient, path, config).get(responseClass);
	final WSData<T> response = this.call(call, defaultSuccessCode);

	return this.validateResponse(response);

    }

    // TODO: Create option to call endpoints with query parameters.
    // DOES NOT WORK WITH QUERY PARAMETERS.
    private WebClient prepare(final WebClient client, final String path, final SAPServiceDAOConfigWrapper config) {
	
	//@formatter:off
	return client.path(path)
		.header("Authorization", String.format("Basic %s:%s", config.getHeaders().get(SAPServiceDAOConfigWrapper.AUTHORIZATION_USER), 
								      config.getHeaders().get(SAPServiceDAOConfigWrapper.AUTHORIZATION_PASSWORD)))
		.header(SAPServiceDAOConfigWrapper.PAYLOAD_STD_ATTRIBUTES_ETRANSATION_IDENTIFIER, 
			config.getHeaders().get(SAPServiceDAOConfigWrapper.PAYLOAD_STD_ATTRIBUTES_ETRANSATION_IDENTIFIER)) //TODO: passar correlation ID
		.header(SAPServiceDAOConfigWrapper.UX_DETAILS_CONSUMER_CREDENTIALS_APPLICATION, 
			config.getHeaders().get(SAPServiceDAOConfigWrapper.UX_DETAILS_CONSUMER_CREDENTIALS_APPLICATION))
		.header(SAPServiceDAOConfigWrapper.UX_DETAILS_CONSUMER_CREDENTIALS_USER, 
			config.getHeaders().get(SAPServiceDAOConfigWrapper.UX_DETAILS_CONSUMER_CREDENTIALS_USER))
		.header(SAPServiceDAOConfigWrapper.UX_DETAILS_CONSUMER_CREDENTIALS_PASSWORD, 
			config.getHeaders().get(SAPServiceDAOConfigWrapper.UX_DETAILS_CONSUMER_CREDENTIALS_PASSWORD))
		.header(SAPServiceDAOConfigWrapper.UX_DETAILS_CONSUMER_CHANNERL_CHANNEL, 
			config.getHeaders().get(SAPServiceDAOConfigWrapper.UX_DETAILS_CONSUMER_CHANNERL_CHANNEL))
		.type(config.getContentType())
		.accept(config.getAcceptType());
	//@formatter:on
	
    }

    private <T> T validateResponse(final WSData<T> response) {

	if (!response.success()) {
	    
	    this.error("Error trying to Invoking SAP Service: " + response.getStatusCode() + " " + response.getStatusMessage());
	    throw this.resultException(response);
	    
	}

	return response.data();

    }

    // END POINTS WITH REQUEST
    private <T, V> WSData<V> call(final T requestWrapper, final BiFunction<T, WebClient, V> function) {
	return this.call(requestWrapper, function, Boolean.FALSE);
    }

    private <T, V> WSData<V> call(final T requestWrapper, final BiFunction<T, WebClient, V> function, final Boolean defaultResponseCodes) {

	return this.invoke((final WebClient client) -> {
	    return this.processResponse(defaultResponseCodes, function.apply(requestWrapper, client));
	});

    }

    // END POINTS WITHOUT REQUEST
    private <V> WSData<V> call(final Function<WebClient, V> function) {
	return this.call(function, Boolean.FALSE);
    }

    private <V> WSData<V> call(final Function<WebClient, V> function, final Boolean defaultResponseCodes) {

	return this.invoke((final WebClient client) -> {
	    return this.processResponse(defaultResponseCodes, function.apply(client));
	});

    }

    private <V> WSData<V> processResponse(final Boolean defaultResponseCodes, final V responseWrapper) {

	final BaseResponseDTO response = ((ResponseWrapper<?>) responseWrapper).getResponse();
	final Map<String, String> errors = response.getErrors().getError();

	final String succesCode = defaultResponseCodes ? WSResult.DEFAULT_STATUS_CODE : errors.get(SAPServiceDAOConstants.ERROR_CODE_PROPERTY);
	final String succesDescription = defaultResponseCodes ? WSResult.DEFAULT_STATUS_MESSAGE : errors.get(SAPServiceDAOConstants.ERROR_DESCRIPTION_PROPERTY);

	final String errorCode = response.getSuccess() ? succesCode : errors.get(SAPServiceDAOConstants.ERROR_CODE_PROPERTY);
	final String errorDescription = response.getSuccess() ? succesDescription : errors.get(SAPServiceDAOConstants.ERROR_DESCRIPTION_PROPERTY);

	this.info("Invocation of SAP Service Returned Codes: " + errorCode + errorDescription);
	
	return WSData.of(responseWrapper).status(errorCode, errorDescription).build();

    }
    
}
