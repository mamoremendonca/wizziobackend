/*-
 * #%L
 * Modules :: SAP Integration Microservice  Data RI
 * %%
 * Copyright (C) 2017 - 2020 Financial Services
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Novabase Business Solutions S.A. and its licensors. The disclosure,
 * copying, adaptation, citation, transcription, translation, modification,
 * decompilation, reverse engineering, derivatives, integration, development and/or
 * any other form of total or partial use of the content of this document and/or
 * accessible through or via the contents, by any possible means without the
 * respective authorization or licensing by the owner of the intellectual property
 * rights is prohibited, the offenders being subject to civil and/or criminal
 * prosecution and liability. The user or licensee of all or part of this document
 * by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.sapservice.data.ri.sapservice.dto.requests;

import java.util.ArrayList;
import java.util.List;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(Include.NON_NULL)
public class CreateSupplierFinancialDocumentRequestDTO extends DTO {

    @JsonProperty("TypeFinDoc")
    private String typeFinancialDocument;
    @JsonProperty("DocNumber")
    private String documentNumber;
    @JsonProperty("IdentifierPo")
    private String identifierPurchaseOrder;
    @JsonProperty("Deferral")
    private String deferral;
    @JsonProperty("SupplierNr")
    private String supplierNumber;
    @JsonProperty("SupplierVatNr")
    private String supplierVatNumber;
    @JsonProperty("DocumentId")
    private String documentExternalId;
    @JsonProperty("Items")
    private List<CreateSupplierFinancialDocumentRequestItemDTO> items;

    public CreateSupplierFinancialDocumentRequestDTO() {

	super();

	this.items = new ArrayList<CreateSupplierFinancialDocumentRequestItemDTO>();

    }

    public String getDeferral() {
        return deferral;
    }

    public void setDeferral(String deferral) {
        this.deferral = deferral;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentExternalId() {
        return documentExternalId;
    }

    public void setDocumentExternalId(String documentExternalId) {
        this.documentExternalId = documentExternalId;
    }

    public String getTypeFinancialDocument() {
	return typeFinancialDocument;
    }

    public void setTypeFinancialDocument(String typeFinancialDocument) {
	this.typeFinancialDocument = typeFinancialDocument;
    }

    public String getIdentifierPurchaseOrder() {
	return identifierPurchaseOrder;
    }

    public void setIdentifierPurchaseOrder(String identifierPurchaseOrder) {
	this.identifierPurchaseOrder = identifierPurchaseOrder;
    }

    public String getSupplierNumber() {
	return supplierNumber;
    }

    public void setSupplierNumber(String supplierNumber) {
	this.supplierNumber = supplierNumber;
    }

    public String getSupplierVatNumber() {
	return supplierVatNumber;
    }

    public void setSupplierVatNumber(String supplierVatNumber) {
	this.supplierVatNumber = supplierVatNumber;
    }

    public List<CreateSupplierFinancialDocumentRequestItemDTO> getItems() {
	return items;
    }

    public void setItems(List<CreateSupplierFinancialDocumentRequestItemDTO> items) {
	this.items = items;
    }

}
