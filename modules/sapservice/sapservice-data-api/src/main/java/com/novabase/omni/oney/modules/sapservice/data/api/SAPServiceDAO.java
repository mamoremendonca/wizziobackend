/*-
 * #%L
 * Modules :: SAP Integration Microservice  Data API
 * %%
 * Copyright (C) 2017 - 2020 Financial Services
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Novabase Business Solutions S.A. and its licensors. The disclosure,
 * copying, adaptation, citation, transcription, translation, modification,
 * decompilation, reverse engineering, derivatives, integration, development and/or
 * any other form of total or partial use of the content of this document and/or
 * accessible through or via the contents, by any possible means without the
 * respective authorization or licensing by the owner of the intellectual property
 * rights is prohibited, the offenders being subject to civil and/or criminal
 * prosecution and liability. The user or licensee of all or part of this document
 * by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.sapservice.data.api;

import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

import com.novabase.omni.oney.modules.referencedata.service.api.dto.TaxMsDTO;
import com.novabase.omni.oney.modules.sapservice.data.api.exceptions.SAPServiceCreatePurchaseOrderException;
import com.novabase.omni.oney.modules.sapservice.data.api.exceptions.SAPServiceCreateSupplierFinancialDocumentException;
import com.novabase.omni.oney.modules.sapservice.entity.FinancialDocument;
import com.novabase.omni.oney.modules.sapservice.entity.PurchaseOrder;

@ProviderType
public interface SAPServiceDAO {

    public Boolean submitPurchaseOrder(final PurchaseOrder purchaseOrder) throws SAPServiceCreatePurchaseOrderException;
    
    public Boolean submitFinancialDocument(final FinancialDocument financialDocument, final List<TaxMsDTO> taxes) throws SAPServiceCreateSupplierFinancialDocumentException;
    
    
    
}
