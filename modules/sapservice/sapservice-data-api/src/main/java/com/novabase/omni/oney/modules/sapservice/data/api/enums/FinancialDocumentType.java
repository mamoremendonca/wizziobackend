package com.novabase.omni.oney.modules.sapservice.data.api.enums;

public enum FinancialDocumentType {
    
    INVOICE("KR"), 
    RETENTION_INVOICE("KI"), 
    CREDIT_NOTE("KG");
 
    private String value;
    
    private FinancialDocumentType(final String value) {
	this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
}
