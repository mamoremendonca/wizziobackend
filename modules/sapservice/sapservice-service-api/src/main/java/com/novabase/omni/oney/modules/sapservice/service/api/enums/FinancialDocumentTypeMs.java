package com.novabase.omni.oney.modules.sapservice.service.api.enums;

public enum FinancialDocumentTypeMs {
    
    INVOICE("FATURA"), 
    RETENTION_INVOICE("Fatura de Retencao"), 
    CREDIT_NOTE("Nota de Credito");
    
    private String oneyValue;

    private FinancialDocumentTypeMs(final String oneyValue) {
	this.oneyValue = oneyValue;
    }

    public String getOneyValue() {
        return oneyValue;
    }
    
}
