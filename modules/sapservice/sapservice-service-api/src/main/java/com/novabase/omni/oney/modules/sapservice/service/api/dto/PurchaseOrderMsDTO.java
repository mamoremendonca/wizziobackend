/*-
 * #%L
 * Modules :: SAP Integration Microservice  Service API
 * %%
 * Copyright (C) 2017 - 2020 Financial Services
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Novabase Business Solutions S.A. and its licensors. The disclosure,
 * copying, adaptation, citation, transcription, translation, modification,
 * decompilation, reverse engineering, derivatives, integration, development and/or
 * any other form of total or partial use of the content of this document and/or
 * accessible through or via the contents, by any possible means without the
 * respective authorization or licensing by the owner of the intellectual property
 * rights is prohibited, the offenders being subject to civil and/or criminal
 * prosecution and liability. The user or licensee of all or part of this document
 * by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.sapservice.service.api.dto;

import java.util.List;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.modules.sapservice.service.api.enums.PurchaseOrderStatusMs;
import com.novabase.omni.oney.modules.sapservice.service.api.enums.PurchaseOrderTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("PurchaseOrder")
public class PurchaseOrderMsDTO extends DTO {

    // ----------------------------------
    // Identification
    @ApiModelProperty
    public PurchaseOrderTypeMs type;
    
    @ApiModelProperty
    public String friendlyNumber;
    
    // ----------------------------------
    // Supplier
    @ApiModelProperty
    public String supplierCode;

    @ApiModelProperty
    public String supplierNIF;

    // ----------------------------------
    // Artigos / Imobilizado
    @ApiModelProperty
    public List<ItemPOMsDTO> items;

    @ApiModelProperty(readOnly = true)
    public PurchaseOrderStatusMs status;

    public PurchaseOrderMsDTO() {
	super();
    }

    public PurchaseOrderTypeMs getType() {
        return type;
    }

    public void setType(PurchaseOrderTypeMs type) {
        this.type = type;
    }

    public String getFriendlyNumber() {
        return friendlyNumber;
    }

    public void setFriendlyNumber(String friendlyNumber) {
        this.friendlyNumber = friendlyNumber;
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    public String getSupplierNIF() {
        return supplierNIF;
    }

    public void setSupplierNIF(String supplierNIF) {
        this.supplierNIF = supplierNIF;
    }

    public List<ItemPOMsDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemPOMsDTO> items) {
        this.items = items;
    }

    public PurchaseOrderStatusMs getStatus() {
        return status;
    }

    public void setStatus(PurchaseOrderStatusMs status) {
        this.status = status;
    }
    
}
