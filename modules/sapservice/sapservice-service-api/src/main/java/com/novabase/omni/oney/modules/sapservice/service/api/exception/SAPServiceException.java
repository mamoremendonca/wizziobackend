/*-
 * #%L
 * Modules :: SAP Integration Microservice  Service API
 * %%
 * Copyright (C) 2017 - 2020 Financial Services
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Novabase Business Solutions S.A. and its licensors. The disclosure,
 * copying, adaptation, citation, transcription, translation, modification,
 * decompilation, reverse engineering, derivatives, integration, development and/or
 * any other form of total or partial use of the content of this document and/or
 * accessible through or via the contents, by any possible means without the
 * respective authorization or licensing by the owner of the intellectual property
 * rights is prohibited, the offenders being subject to civil and/or criminal
 * prosecution and liability. The user or licensee of all or part of this document
 * by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.sapservice.service.api.exception;

import com.novabase.omni.oney.modules.sapservice.service.api.SAPServiceProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;
import io.digitaljourney.platform.modules.ws.rs.api.exception.RSException;

public class SAPServiceException extends RSException {
	private static final long serialVersionUID = -8645577031119256555L;

	protected SAPServiceException(PlatformCode statusCode, String errorCode, Context ctx, Object... args) {
		super(statusCode, errorCode, ctx, args);
	}

	protected SAPServiceException(Context ctx, Object... args) {
		this(PlatformCode.INTERNAL_SERVER_ERROR, SAPServiceProperties.SAPSERVICE000, ctx, args);
	}

	protected SAPServiceException(String errorCode, Context ctx, Object... args) {
		this(PlatformCode.BUSINESS_ERROR, errorCode, ctx, args);
	}

	public static SAPServiceException of(Context ctx, String message) {
		return new SAPServiceException(ctx, message);
	}

	public static SAPServiceException of(Context ctx, Throwable cause) {
		return new SAPServiceException(ctx, cause);
	}
}
