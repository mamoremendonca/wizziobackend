package com.novabase.omni.oney.modules.sapservice.service.api.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.novabase.omni.oney.modules.sapservice.service.api.enums.FinancialDocumentTypeMs;

import io.swagger.annotations.ApiModelProperty;

public class FinancialDocumentMsDTO extends DTO {

    // ----------------------------------
    // Identification
    @ApiModelProperty
    private FinancialDocumentTypeMs type;
    
    @ApiModelProperty
    private String documentNumber;	

    @ApiModelProperty
    private String purchaseOrderFriendlyNumber;
    
    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime approvalDate;
    
    @ApiModelProperty
    private String ecmNumber;
    
    @ApiModelProperty
    private boolean granting;
    
    // ----------------------------------
    // Supplier
    @ApiModelProperty
    private String supplierCode;

    @ApiModelProperty
    private String supplierNIF;

    // ----------------------------------
    // Artigos / Imobilizado
    @ApiModelProperty
    public List<ItemFDMsDTO> items;

    public FinancialDocumentMsDTO() {
	
	super();
	
	this.items = new ArrayList<ItemFDMsDTO>();
	
    }

    public FinancialDocumentTypeMs getType() {
        return type;
    }

    public void setType(FinancialDocumentTypeMs type) {
        this.type = type;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getPurchaseOrderFriendlyNumber() {
        return purchaseOrderFriendlyNumber;
    }

    public void setPurchaseOrderFriendlyNumber(String purchaseOrderFriendlyNumber) {
        this.purchaseOrderFriendlyNumber = purchaseOrderFriendlyNumber;
    }

    public LocalDateTime getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(LocalDateTime approvalDate) {
        this.approvalDate = approvalDate;
    }

    public String getEcmNumber() {
        return ecmNumber;
    }

    public void setEcmNumber(String ecmNumber) {
        this.ecmNumber = ecmNumber;
    }

    public boolean isGranting() {
        return granting;
    }

    public void setGranting(boolean granting) {
        this.granting = granting;
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    public String getSupplierNIF() {
        return supplierNIF;
    }

    public void setSupplierNIF(String supplierNIF) {
        this.supplierNIF = supplierNIF;
    }

    public List<ItemFDMsDTO> getItems() {
        return items;
    }

    public void setItems(List<ItemFDMsDTO> items) {
        this.items = items;
    }
    
}
