/*-
 * #%L
 * Modules :: SAP Integration Microservice  Service API
 * %%
 * Copyright (C) 2017 - 2020 Financial Services
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Novabase Business Solutions S.A. and its licensors. The disclosure,
 * copying, adaptation, citation, transcription, translation, modification,
 * decompilation, reverse engineering, derivatives, integration, development and/or
 * any other form of total or partial use of the content of this document and/or
 * accessible through or via the contents, by any possible means without the
 * respective authorization or licensing by the owner of the intellectual property
 * rights is prohibited, the offenders being subject to civil and/or criminal
 * prosecution and liability. The user or licensee of all or part of this document
 * by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.sapservice.service.api.dto;

import java.time.LocalDateTime;

import org.osgi.dto.DTO;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("Item")
public class ItemMsDTO extends DTO {

    @ApiModelProperty
    public Long id;

    // Contem tanto o código de artigo como o código de imobilizado
    @ApiModelProperty
    public String articleCode;

    @ApiModelProperty
    public String articleDescription;

    @ApiModelProperty
    public String internalOrderCode;

    @ApiModelProperty
    public String costCenterCode;

    // Quantity => manteve-se amount por coerencia com MsPurchaseOrderRepository
    @ApiModelProperty
    public Double amount;

    @ApiModelProperty
    public String taxCode;

    @ApiModelProperty
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public LocalDateTime startDate;

    @ApiModelProperty
    public Double unitPriceWithoutTax;
    
    public ItemMsDTO() {
	super();
    }

    public Double getUnitPriceWithoutTax() {
        return unitPriceWithoutTax;
    }

    public void setUnitPriceWithoutTax(Double unitPriceWithoutTax) {
        this.unitPriceWithoutTax = unitPriceWithoutTax;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getArticleCode() {
	return articleCode;
    }

    public void setArticleCode(String articleCode) {
	this.articleCode = articleCode;
    }

    public String getArticleDescription() {
	return articleDescription;
    }

    public void setArticleDescription(String articleDescription) {
	this.articleDescription = articleDescription;
    }

    public String getInternalOrderCode() {
	return internalOrderCode;
    }

    public void setInternalOrderCode(String internalOrderCode) {
	this.internalOrderCode = internalOrderCode;
    }

    public String getCostCenterCode() {
	return costCenterCode;
    }

    public void setCostCenterCode(String costCenterCode) {
	this.costCenterCode = costCenterCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTaxCode() {
	return taxCode;
    }

    public void setTaxCode(String taxCode) {
	this.taxCode = taxCode;
    }

    public LocalDateTime getStartDate() {
	return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
	this.startDate = startDate;
    }

}
