:docdir: ../appendices
:icons: font
:author: Digital Journey Product Development Team
:imagesdir: ./images
:imagesoutdir: ./images
//embedded images
:data-uri:
// empty line
:blank: pass:[ +]
// Toc
:toc: macro
:toclevels: 3
:sectnums:
:sectnumlevels: 3
// Variables
:revnumber: 1.0
:api_version: v1
:arrow: icon:angle-double-down[]
:ms_name: SAP Integration Microservice 
:source-highlighter: highlightjs
:xrefstyle: short

image::shared/header.png[]

= {ms_name} - Developer Manual
v{revnumber}, {docdate}

<<<

.Change Log
[%header,cols=3*]
|===
| Version
| Date
| Changes

| 1.0
| January/2019
| Initial Version
|===

toc::[]

<<<

== Introduction

The purpose of this document is to describe how to develop the {ms_name} micro-service.

It will cover the following topics:

* Environment Setup - What are the requirements in terms of local and external dependencies to develop the micro-service
* Implementation Details - Important notes and details while developing the micro-service
* Quality Assurance - What developers should have in mind while developing in terms of ensuring the best quality


== Environment setup
'''

Environment setup...

== Domain Architecture
'''
The {ms_name} micro-service is comprised of five different types of modules, as shown in the diagram below:

[plantuml, uam-diagram-uml, png]
....
@startuml
!include ./macros/shared/er.iuml

package "sapservice" {
  [sapservice RI] -right- [sapservice RDB]
  [sapservice RI] -up- [sapservice API]
  [sapservice RDB] -down- [sapservice SPI]
  [sapservice RI] -down- [sapservice SPI]
}

@enduml
....

* *sapservice API*: Contains all the {ms_name} service interface methods
* *sapservice RI*: Service implementation methods of {ms_name} API
* *sapservice RDB*: Contains all the DAOs that interact with database

== Implementation Details
'''

=== Database Model

The following diagram shows the database model used by UAM RDB:

include::incl/database-model.adoc[]

{blank}


=== Extension Points
'''

Extension points

=== Security Guidelines
'''

The {ms_name} micro-service implementation has the following impact on the security guidelines:

* Item


=== Pitfalls and Q&A
'''
* The permissions.uam file permission titles are used as the id of a liquibase changeset, so they must be unique for the entire system.
* When implementing new Shiro realms, don't forget to implement the correspondent component in the Foundation App.
Also, when using the generic distro, only the core realm component should be active.
* User domain should not be used for internal users.
* User password column current size won't support hashes longer than SHA-256. For longer hashes, the column size needs
to be increased.

== Quality Assurance
'''

=== Integrated Tests
'''

