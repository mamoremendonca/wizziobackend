/*-
 * #%L
 * Modules :: SAP Integration Microservice  Service RI
 * %%
 * Copyright (C) 2017 - 2020 Financial Services
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Novabase Business Solutions S.A. and its licensors. The disclosure,
 * copying, adaptation, citation, transcription, translation, modification,
 * decompilation, reverse engineering, derivatives, integration, development and/or
 * any other form of total or partial use of the content of this document and/or
 * accessible through or via the contents, by any possible means without the
 * respective authorization or licensing by the owner of the intellectual property
 * rights is prohibited, the offenders being subject to civil and/or criminal
 * prosecution and liability. The user or licensee of all or part of this document
 * by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.sapservice.service.ri;

import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.modules.referencedata.service.api.ReferenceDataResource;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.TaxMsDTO;
import com.novabase.omni.oney.modules.sapservice.data.api.SAPServiceDAO;
import com.novabase.omni.oney.modules.sapservice.data.api.exceptions.SAPServiceCreatePurchaseOrderException;
import com.novabase.omni.oney.modules.sapservice.data.api.exceptions.SAPServiceCreateSupplierFinancialDocumentException;
import com.novabase.omni.oney.modules.sapservice.service.api.SAPServiceResource;
import com.novabase.omni.oney.modules.sapservice.service.api.dto.FinancialDocumentMsDTO;
import com.novabase.omni.oney.modules.sapservice.service.api.dto.PurchaseOrderMsDTO;
import com.novabase.omni.oney.modules.sapservice.service.ri.mapper.SAPServiceResourceMapper;

import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;
import io.digitaljourney.platform.modules.ws.rs.api.resource.AbstractResource;

// @formatter:off
@Component(
	configurationPid = SAPServiceResourceConfig.CPID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	reference = {
		@Reference(
			name = SAPServiceResourceProperties.REF_CONTEXT,
			service = SAPServiceContext.class,
			cardinality = ReferenceCardinality.MANDATORY)
})
@Designate(ocd = SAPServiceResourceConfig.class)
@RSProvider(SAPServiceResourceProperties.ADDRESS)
// @formatter:on
public class SAPServiceResourceImpl extends AbstractResource<SAPServiceContext, SAPServiceResourceConfig> implements SAPServiceResource {

    @Reference
    private volatile SAPServiceDAO sapServiceDAO;

    @Reference
    private volatile ReferenceDataResource referenceDataResource;

    @Activate
    public void activate(ComponentContext ctx, SAPServiceResourceConfig config) {
	prepare(ctx, config);
    }

    @Modified
    public void modified(SAPServiceResourceConfig config) {
	prepare(config);
    }

    @Override
    public Boolean submitPurchaseOrder(final PurchaseOrderMsDTO purchaseOrder) {

	try {
	    return this.sapServiceDAO.submitPurchaseOrder(SAPServiceResourceMapper.INSTANCE.toPurchaseOrder(purchaseOrder));
	} catch (SAPServiceCreatePurchaseOrderException e) {
	    throw this.getCtx().exception(e.getMessage());
	} catch (Throwable t) {
	    throw this.getCtx().exception("Error Submmiting Purchase Order to SAP EAI Service. " + t.getMessage());
	}

    }

    @Override
    public Boolean submitFinancialDocument(final FinancialDocumentMsDTO financialDocument) {

	try {

	    return this.sapServiceDAO.submitFinancialDocument(SAPServiceResourceMapper.INSTANCE.toFinancialDocument(financialDocument),
		    this.getItemTaxes(financialDocument, this.referenceDataResource.getTaxes()));

	} catch (SAPServiceCreateSupplierFinancialDocumentException e) {
	    throw this.getCtx().exception(e.getMessage());
	} catch (Throwable t) {
	    throw this.getCtx().exception("Error Submmiting Financial Document to SAP EAI Service. " + t.getMessage());
	}

    }

    private List<TaxMsDTO> getItemTaxes(final FinancialDocumentMsDTO financialDocument, final List<TaxMsDTO> allTaxes) {

	return allTaxes.stream().filter(tax -> financialDocument.getItems().parallelStream().filter(item -> tax.code.equals(item.taxCode)).findAny().orElse(null) != null).collect(Collectors.toList());

    }

}
