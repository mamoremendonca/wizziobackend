/*-
 * #%L
 * Modules :: SAP Integration Microservice  Service RI
 * %%
 * Copyright (C) 2017 - 2020 Financial Services
 * %%
 * All rights reserved. This software is protected under several
 * Laws in various countries. All content, layout, design of this document are the
 * intellectual property of Novabase Business Solutions S.A. and its licensors. The disclosure,
 * copying, adaptation, citation, transcription, translation, modification,
 * decompilation, reverse engineering, derivatives, integration, development and/or
 * any other form of total or partial use of the content of this document and/or
 * accessible through or via the contents, by any possible means without the
 * respective authorization or licensing by the owner of the intellectual property
 * rights is prohibited, the offenders being subject to civil and/or criminal
 * prosecution and liability. The user or licensee of all or part of this document
 * by any means may only use the document under the terms and conditions agreed
 * upon with the owner of the intellectual property rights, and for the purposes
 * justifying the granting of the license or authorization, without which the
 * unauthorized use may subject the offenders to civil or criminal prosecution
 * under applicable Laws.
 * #L%
 */
package com.novabase.omni.oney.modules.sapservice.service.ri.mapper;

import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.modules.sapservice.entity.FinancialDocument;
import com.novabase.omni.oney.modules.sapservice.entity.ItemFD;
import com.novabase.omni.oney.modules.sapservice.entity.ItemPO;
import com.novabase.omni.oney.modules.sapservice.entity.PurchaseOrder;
import com.novabase.omni.oney.modules.sapservice.service.api.dto.FinancialDocumentMsDTO;
import com.novabase.omni.oney.modules.sapservice.service.api.dto.ItemFDMsDTO;
import com.novabase.omni.oney.modules.sapservice.service.api.dto.ItemPOMsDTO;
import com.novabase.omni.oney.modules.sapservice.service.api.dto.PurchaseOrderMsDTO;

@Mapper
public interface SAPServiceResourceMapper {
    
    public static final SAPServiceResourceMapper INSTANCE = Mappers.getMapper(SAPServiceResourceMapper.class);
  
    @Mapping(expression = "java(purchaseOrder.type.getOneyValue())", target = "type")
    public PurchaseOrder toPurchaseOrder(final PurchaseOrderMsDTO purchaseOrder);
	
    @IterableMapping(elementTargetType = ItemPO.class)
    public List<ItemPO> toListItemPOMsDTO(final List<ItemPOMsDTO> lst);
    
    public ItemPO toItemFromItemPOMsDTO(final ItemPOMsDTO item);
	
    @Mapping(expression = "java(financialDocument.getType().name())", target = "type")
    public FinancialDocument toFinancialDocument(final FinancialDocumentMsDTO financialDocument);
	
    @IterableMapping(elementTargetType = ItemFD.class)
    public List<ItemFD> toListItemFDMsDTO(final List<ItemFDMsDTO> lst);
    
    public ItemFD toItemFromItemFDMsDTO(final ItemFDMsDTO item);
    
}
