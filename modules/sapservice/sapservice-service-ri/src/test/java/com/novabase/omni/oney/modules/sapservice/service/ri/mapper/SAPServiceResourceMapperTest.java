package com.novabase.omni.oney.modules.sapservice.service.ri.mapper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.novabase.omni.oney.modules.sapservice.entity.FinancialDocument;
import com.novabase.omni.oney.modules.sapservice.entity.ItemFD;
import com.novabase.omni.oney.modules.sapservice.service.api.dto.FinancialDocumentMsDTO;
import com.novabase.omni.oney.modules.sapservice.service.api.dto.ItemFDMsDTO;
import com.novabase.omni.oney.modules.sapservice.service.api.enums.FinancialDocumentTypeMs;

public class SAPServiceResourceMapperTest {

    private static final String DEFAULT_DOCUMENT_EXTERNAL_ID = "DocumentExternalId";
    private static final String DEFAULT_DOCUMENT_NUMBER = "DocumentNumber";
    private static final String DEFAULT_IDENTIFIER_PURCHASE_ORDER = "IdentifierPurchaseOrder";
    private static final String DEFAULT_SUPPLIER_NUMBER = "SupplierNumber";
    private static final String DEFAULT_SUPPLIER_VAT_NUMBER = "SupplierVatNumber";
    private static final FinancialDocumentTypeMs DEFAULT_FINANCIAL_DOCUMENT_TYPE = FinancialDocumentTypeMs.INVOICE;
    private static final String DEFAULT_COST_CENTER = "CostCenter";
    private static final String DEFAULT_MATERIAL = "Material";
    private static final String DEFAULT_ORDER_NUMBER = "OrderNumber";
    private static final String DEFAULT_TAX_CODE = "TaxCode";
    private static final String DEFAULT_WITH_HOLDING_TAX_CODE = "WithholdingTaxCode";

    private static final LocalDateTime DEFAULT_LOCAL_DATE_TIME_NOW = LocalDateTime.now();
    
    @Before
    public void setUp() {
	
    }
    
    @Test
    public void toFinancialDocumentTest() {
	
	final FinancialDocument financialDocumentNull = SAPServiceResourceMapper.INSTANCE.toFinancialDocument(null);
	Assert.assertNull(financialDocumentNull);
	
	final ItemFDMsDTO itemFD = new ItemFDMsDTO();
	itemFD.amount = 1D;
	itemFD.articleCode = DEFAULT_MATERIAL;
	itemFD.costCenterCode = DEFAULT_COST_CENTER;
	itemFD.id = 1L;
	itemFD.internalOrderCode = DEFAULT_ORDER_NUMBER;
	itemFD.startDate = DEFAULT_LOCAL_DATE_TIME_NOW.minusDays(1L);
	itemFD.taxCode = DEFAULT_TAX_CODE;
	itemFD.setWithholdingTaxCode(DEFAULT_WITH_HOLDING_TAX_CODE);

	final FinancialDocumentMsDTO financialDocument = new FinancialDocumentMsDTO();
	financialDocument.setApprovalDate(DEFAULT_LOCAL_DATE_TIME_NOW.minusDays(1L));
	financialDocument.setDocumentNumber(DEFAULT_DOCUMENT_NUMBER);
	financialDocument.setEcmNumber(DEFAULT_DOCUMENT_EXTERNAL_ID);
	financialDocument.setGranting(Boolean.FALSE);
	financialDocument.setPurchaseOrderFriendlyNumber(DEFAULT_IDENTIFIER_PURCHASE_ORDER);
	financialDocument.setSupplierCode(DEFAULT_SUPPLIER_NUMBER);
	financialDocument.setSupplierNIF(DEFAULT_SUPPLIER_VAT_NUMBER);
	financialDocument.setType(DEFAULT_FINANCIAL_DOCUMENT_TYPE);
	financialDocument.getItems().add(itemFD);
	
	FinancialDocument financialDocumentMapped = SAPServiceResourceMapper.INSTANCE.toFinancialDocument(financialDocument);
	Assert.assertEquals(financialDocumentMapped.documentNumber, financialDocument.getDocumentNumber());
	Assert.assertEquals(financialDocumentMapped.ecmNumber, financialDocument.getEcmNumber());
	Assert.assertEquals(financialDocumentMapped.purchaseOrderFriendlyNumber, financialDocument.getPurchaseOrderFriendlyNumber());
	Assert.assertEquals(financialDocumentMapped.supplierCode, financialDocument.getSupplierCode());
	Assert.assertEquals(financialDocumentMapped.supplierNIF, financialDocument.getSupplierNIF());
	Assert.assertEquals(financialDocumentMapped.type, financialDocument.getType().name());
	Assert.assertEquals(financialDocumentMapped.approvalDate, financialDocument.getApprovalDate());
	Assert.assertEquals(financialDocumentMapped.granting, financialDocument.isGranting());
	Assert.assertEquals(financialDocumentMapped.items.get(0).articleCode, financialDocument.items.get(0).getArticleCode());
	Assert.assertEquals(financialDocumentMapped.items.get(0).asset, null);
	Assert.assertEquals(financialDocumentMapped.items.get(0).costCenterCode, financialDocument.items.get(0).costCenterCode);
	Assert.assertEquals(financialDocumentMapped.items.get(0).internalOrderCode, financialDocument.items.get(0).internalOrderCode);
	Assert.assertEquals(financialDocumentMapped.items.get(0).taxCode, financialDocument.items.get(0).taxCode);
	Assert.assertEquals(financialDocumentMapped.items.get(0).withholdingTaxCode, financialDocument.items.get(0).getWithholdingTaxCode());
	Assert.assertEquals(financialDocumentMapped.items.get(0).amount, financialDocument.items.get(0).amount);
	Assert.assertEquals(financialDocumentMapped.items.get(0).id, financialDocument.items.get(0).id);
	Assert.assertEquals(financialDocumentMapped.items.get(0).itemPurchaseOrderId, financialDocument.items.get(0).getItemPurchaseOrderId());
	Assert.assertEquals(financialDocumentMapped.items.get(0).startDate, financialDocument.items.get(0).startDate);
	
    }
    
    @Test
    public void toListItemFDMsDTO() {
	
	final List<ItemFD> itemsListNull = SAPServiceResourceMapper.INSTANCE.toListItemFDMsDTO(null);
	Assert.assertNull(itemsListNull);
	
	final ItemFDMsDTO itemFD = new ItemFDMsDTO();
	itemFD.amount = 1D;
	itemFD.articleCode = DEFAULT_MATERIAL;
	itemFD.costCenterCode = DEFAULT_COST_CENTER;
	itemFD.id = 1L;
	itemFD.internalOrderCode = DEFAULT_ORDER_NUMBER;
	itemFD.startDate = DEFAULT_LOCAL_DATE_TIME_NOW.minusDays(1L);
	itemFD.taxCode = DEFAULT_TAX_CODE;
	itemFD.setWithholdingTaxCode(DEFAULT_WITH_HOLDING_TAX_CODE);
	
	final List<ItemFDMsDTO> itemFDMsDTOList = new ArrayList<ItemFDMsDTO>();
	itemFDMsDTOList.add(itemFD);
	
	final List<ItemFD> items = SAPServiceResourceMapper.INSTANCE.toListItemFDMsDTO(itemFDMsDTOList);
	Assert.assertEquals(items.get(0).articleCode, itemFDMsDTOList.get(0).getArticleCode());
	Assert.assertEquals(items.get(0).asset, null);
	Assert.assertEquals(items.get(0).costCenterCode, itemFDMsDTOList.get(0).costCenterCode);
	Assert.assertEquals(items.get(0).internalOrderCode, itemFDMsDTOList.get(0).internalOrderCode);
	Assert.assertEquals(items.get(0).taxCode, itemFDMsDTOList.get(0).taxCode);
	Assert.assertEquals(items.get(0).withholdingTaxCode, itemFDMsDTOList.get(0).getWithholdingTaxCode());
	Assert.assertEquals(items.get(0).amount, itemFDMsDTOList.get(0).amount);
	Assert.assertEquals(items.get(0).id, itemFDMsDTOList.get(0).id);
	Assert.assertEquals(items.get(0).itemPurchaseOrderId, itemFDMsDTOList.get(0).getItemPurchaseOrderId());
	Assert.assertEquals(items.get(0).startDate, itemFDMsDTOList.get(0).startDate);
	
    }
    
    @Test
    public void toItem() {
	
	final ItemFD itemNull = SAPServiceResourceMapper.INSTANCE.toItemFromItemFDMsDTO(null);
	Assert.assertNull(itemNull);
	
	final ItemFDMsDTO itemFD = new ItemFDMsDTO();
	itemFD.amount = 1D;
	itemFD.articleCode = DEFAULT_MATERIAL;
	itemFD.costCenterCode = DEFAULT_COST_CENTER;
	itemFD.id = 1L;
	itemFD.internalOrderCode = DEFAULT_ORDER_NUMBER;
	itemFD.startDate = DEFAULT_LOCAL_DATE_TIME_NOW.minusDays(1L);
	itemFD.taxCode = DEFAULT_TAX_CODE;
	itemFD.setWithholdingTaxCode(DEFAULT_WITH_HOLDING_TAX_CODE);
	
	final ItemFD item = SAPServiceResourceMapper.INSTANCE.toItemFromItemFDMsDTO(itemFD);
	Assert.assertEquals(item.articleCode, itemFD.getArticleCode());
	Assert.assertEquals(item.asset, null);
	Assert.assertEquals(item.costCenterCode, itemFD.costCenterCode);
	Assert.assertEquals(item.internalOrderCode, itemFD.internalOrderCode);
	Assert.assertEquals(item.taxCode, itemFD.taxCode);
	Assert.assertEquals(item.withholdingTaxCode, itemFD.getWithholdingTaxCode());
	Assert.assertEquals(item.amount, itemFD.amount);
	Assert.assertEquals(item.id, itemFD.id);
	Assert.assertEquals(item.itemPurchaseOrderId, itemFD.getItemPurchaseOrderId());
	Assert.assertEquals(item.startDate, itemFD.startDate);
	
    }
    
}
