package com.novabase.omni.oney.modules.sapservice.service.ri;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;

import com.novabase.omni.oney.modules.referencedata.service.api.ReferenceDataResource;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.TaxMsDTO;
import com.novabase.omni.oney.modules.sapservice.data.api.SAPServiceDAO;
import com.novabase.omni.oney.modules.sapservice.entity.FinancialDocument;
import com.novabase.omni.oney.modules.sapservice.service.api.dto.FinancialDocumentMsDTO;
import com.novabase.omni.oney.modules.sapservice.service.api.dto.ItemFDMsDTO;
import com.novabase.omni.oney.modules.sapservice.service.api.enums.FinancialDocumentTypeMs;

public class SAPServiceResourceImplTest {
    
    private static final String DEFAULT_DOCUMENT_EXTERNAL_ID = "DocumentExternalId";
    private static final String DEFAULT_DOCUMENT_NUMBER = "DocumentNumber";
    private static final String DEFAULT_IDENTIFIER_PURCHASE_ORDER = "IdentifierPurchaseOrder";
    private static final String DEFAULT_SUPPLIER_NUMBER = "SupplierNumber";
    private static final String DEFAULT_SUPPLIER_VAT_NUMBER = "SupplierVatNumber";
    private static final String DEFAULT_COST_CENTER = "CostCenter";
    private static final String DEFAULT_MATERIAL = "Material";
    private static final String DEFAULT_ORDER_NUMBER = "OrderNumber";
    private static final String DEFAULT_TAX_CODE = "TaxCode";
    private static final String DEFAULT_WITH_HOLDING_TAX_CODE = "WithholdingTaxCode";
    
    private static final LocalDateTime DEFAULT_LOCAL_DATE_TIME_NOW = LocalDateTime.now();
    
    @InjectMocks
    private SAPServiceResourceImpl sapServiceResourceImpl;
    
    @Mock
    private SAPServiceDAO sapServiceDAO;
    
    @Mock
    private ReferenceDataResource referenceDataResource;
    
    private ItemFDMsDTO defaultItem;
    private FinancialDocumentMsDTO defaultFinancialDocument;
    
    @Before
    public void setUp() {
	
	MockitoAnnotations.initMocks(this);
	
	this.defaultItem = new ItemFDMsDTO();
	this.defaultItem.amount = 1D;
	this.defaultItem.articleCode = DEFAULT_MATERIAL;
	this.defaultItem.articleDescription = "Description1";
	this.defaultItem.costCenterCode = DEFAULT_COST_CENTER;
	this.defaultItem.id = 1L;
	this.defaultItem.internalOrderCode = DEFAULT_ORDER_NUMBER;
	this.defaultItem.startDate = DEFAULT_LOCAL_DATE_TIME_NOW;
	this.defaultItem.taxCode = DEFAULT_TAX_CODE;
	this.defaultItem.setItemPurchaseOrderId(1L);
	this.defaultItem.setWithholdingTaxCode(DEFAULT_WITH_HOLDING_TAX_CODE);
	
	this.defaultFinancialDocument = new FinancialDocumentMsDTO();
	this.defaultFinancialDocument.setApprovalDate(DEFAULT_LOCAL_DATE_TIME_NOW);
	this.defaultFinancialDocument.setDocumentNumber(DEFAULT_DOCUMENT_NUMBER);
	this.defaultFinancialDocument.setEcmNumber(DEFAULT_DOCUMENT_EXTERNAL_ID);
	this.defaultFinancialDocument.setGranting(Boolean.FALSE);
	this.defaultFinancialDocument.setPurchaseOrderFriendlyNumber(DEFAULT_IDENTIFIER_PURCHASE_ORDER);
	this.defaultFinancialDocument.setSupplierCode(DEFAULT_SUPPLIER_NUMBER);
	this.defaultFinancialDocument.setSupplierNIF(DEFAULT_SUPPLIER_VAT_NUMBER);
	this.defaultFinancialDocument.setType(FinancialDocumentTypeMs.INVOICE);
	this.defaultFinancialDocument.getItems().add(this.defaultItem);
	
    }

    @Test
    public void submitFinancialDocumentTest() throws Exception {

	final TaxMsDTO tax = new TaxMsDTO();
	tax.code = DEFAULT_TAX_CODE;
	tax.taxPercentage = "1.0";
	final List<TaxMsDTO> taxes = new ArrayList<TaxMsDTO>();
	taxes.add(tax);
	
	this.getSubmitFinancialDocumentMockitoBehavior().thenReturn(Boolean.TRUE);
	this.getTaxesMockitoBehavior().thenReturn(taxes);
	Boolean result = this.sapServiceResourceImpl.submitFinancialDocument(this.defaultFinancialDocument);
	
	Assert.assertTrue(result);
	
    }
    
    private <T> OngoingStubbing<Boolean> getSubmitFinancialDocumentMockitoBehavior() throws Exception {
	return Mockito.when(this.sapServiceDAO.submitFinancialDocument(Mockito.any(FinancialDocument.class), Mockito.anyList()));
    }
    
    private <T> OngoingStubbing<List<TaxMsDTO>> getTaxesMockitoBehavior() throws Exception {
	return Mockito.when(this.referenceDataResource.getTaxes());
    }
    
}
