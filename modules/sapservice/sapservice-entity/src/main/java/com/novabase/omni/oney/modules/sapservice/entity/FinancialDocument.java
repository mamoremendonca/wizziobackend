package com.novabase.omni.oney.modules.sapservice.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FinancialDocument {

    public String type;
    public String documentNumber;
    public String purchaseOrderFriendlyNumber;
    public LocalDateTime approvalDate;
    public String supplierCode;
    public String supplierNIF;
    public String ecmNumber;
    public boolean granting;
    public List<ItemFD> items;
    
    public FinancialDocument() {
	
	super();
	
	this.items = new ArrayList<ItemFD>();
	
    }
    
}
