package com.novabase.omni.oney.modules.financialdocumentrepository.data.api.exception;

public class UniqueConstraintViolationException extends Exception {

    private static final long serialVersionUID = -8711276064115158998L;

    public UniqueConstraintViolationException(String message) {
	super(message);
    }

}
