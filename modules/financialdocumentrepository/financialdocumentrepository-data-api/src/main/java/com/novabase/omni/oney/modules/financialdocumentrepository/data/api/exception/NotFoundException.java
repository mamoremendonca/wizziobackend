package com.novabase.omni.oney.modules.financialdocumentrepository.data.api.exception;

public class NotFoundException extends Exception {

    private static final long serialVersionUID = 7932336475072674523L;
    
    private Class<?> entity;

    public NotFoundException(String message) {
	super(message);
    }

    public NotFoundException(Class<?> entity) {
	super();
	this.entity = entity;
    }

    public Class<?> getEntity() {
	return entity;
    }

}
