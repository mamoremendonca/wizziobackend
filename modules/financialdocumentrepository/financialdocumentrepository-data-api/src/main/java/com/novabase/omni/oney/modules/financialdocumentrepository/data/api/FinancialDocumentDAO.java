package com.novabase.omni.oney.modules.financialdocumentrepository.data.api;

import java.time.LocalDateTime;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.exception.NotFoundException;
import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.exception.UnexpectedPersistenceException;
import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.exception.UniqueConstraintViolationException;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.Attachment;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.FinancialDocument;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.History;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.SearchCriteria;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.Workflow;

@ProviderType
public interface FinancialDocumentDAO {

    public FinancialDocument getFinancialDocument(Long id) throws NotFoundException, UnexpectedPersistenceException;

    public FinancialDocument addFinancialDocument(FinancialDocument financialDocument) throws UniqueConstraintViolationException, UnexpectedPersistenceException;
    
    public Long getFinancialDocumentIdByInstanceId(Long instanceId) throws UnexpectedPersistenceException;
    
    public void deleteFinancialDocumentById(Long id) throws UnexpectedPersistenceException, NotFoundException;

    public History addHistory(History history) throws UnexpectedPersistenceException;

    public List<History> addHistories(List<History> histories) throws UnexpectedPersistenceException;

    public List<History> searchFinancialDocumentHistory(Long financialDocumentId) throws UnexpectedPersistenceException;

    public Workflow searchFinancialDocumentWorkFlow(Long financialDocumentId) throws UnexpectedPersistenceException;

    public Workflow updateWorkFlow(Workflow workflow, Long financialDocumentId, String financialDocumentStatus, LocalDateTime finishDate) throws UnexpectedPersistenceException;

    //TODO: refazer com a logica de paginação
    public List<FinancialDocument> searchFinancialDocuments(final SearchCriteria search) throws UnexpectedPersistenceException;

    public Attachment getFinancialDocumentAttachment(Long id, int index) throws NotFoundException, UnexpectedPersistenceException;

    public List<Attachment> getFinancialDocumentAttachments(Long id) throws NotFoundException, UnexpectedPersistenceException;

    public Attachment updateAttachment(Attachment attachment) throws UnexpectedPersistenceException;

    public List<Long> findProcessToSubmit(List<String> financialDocumentTypes) throws UnexpectedPersistenceException;

    public void updateFinancialDocumentStatus(final FinancialDocument financialDocument, final String status) throws UnexpectedPersistenceException;
    
    public FinancialDocument getFinancialDocumentNotCanceledNotRejectedByNumber(final String number) throws NotFoundException, UnexpectedPersistenceException;

    public void updateFDDocumentExternalId(final Long financialDocumentId, final String documentExternalId) throws UnexpectedPersistenceException;

}
