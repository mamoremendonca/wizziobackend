package com.novabase.omni.oney.modules.financialdocumentrepository.data.api.exception;

public class UnexpectedPersistenceException extends Exception {

    private static final long serialVersionUID = 4649664889610605900L;

    public UnexpectedPersistenceException(String message) {
	super(message);
    }

}
