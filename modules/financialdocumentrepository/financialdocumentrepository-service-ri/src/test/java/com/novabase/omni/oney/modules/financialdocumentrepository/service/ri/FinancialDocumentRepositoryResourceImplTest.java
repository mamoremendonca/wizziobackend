package com.novabase.omni.oney.modules.financialdocumentrepository.service.ri;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.manager.FDRepositoryDataManager;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.manager.FDRepositorySubmitOneyManager;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.manager.FDRepositoryWorkflowManager;

public class FinancialDocumentRepositoryResourceImplTest {

    @InjectMocks 
    private FinancialDocumentRepositoryResourceImpl financialDocumentRepositoryResourceImpl;
    
    @Mock
    private FDRepositoryDataManager fdRepositoryDataManager;

    @Mock
    private FDRepositorySubmitOneyManager fdRepositorySubmitOneyManager;

    @Mock
    private FDRepositoryWorkflowManager fdRepositoryWorkflowManager;
    
    @Before
    public void setUp() {
	MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void runFinancialDocumentSubmitJobTest() {
	Assert.assertTrue(Boolean.TRUE);
    }
    
}
