package com.novabase.omni.oney.modules.financialdocumentrepository.service.ri.manager;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;

import com.novabase.omni.oney.modules.ecmservice.service.api.ECMServiceResource;
import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.FinancialDocumentDAO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.manager.FDRepositoryDataManager;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.manager.FDRepositorySubmitOneyManager;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.ri.FinancialDocumentRepositoryContext;
import com.novabase.omni.oney.modules.referencedata.service.api.ReferenceDataResource;
import com.novabase.omni.oney.modules.sapservice.service.api.SAPServiceResource;

public class FDRepositorySubmitOneyManagerTest {

    @InjectMocks
    public FDRepositorySubmitOneyManager fdRepositorySubmitOneyManager;
    
    @Mock
    private FinancialDocumentRepositoryContext ctx;

    @Mock
    private FDRepositoryDataManager financialDocumentRepositoryDataManager;

    @Mock
    private volatile FinancialDocumentDAO financialDocumentDAO ;

    @Mock
    private volatile ECMServiceResource ecmServiceResource;

    @Mock
    private volatile ReferenceDataResource referenceDataResource;
    
//    @Mock
//    private volatile BlobResource blobResource;
    
    @Mock
    private volatile SAPServiceResource sapServiceResource;
    
    @Before
    public void setUp() {
	MockitoAnnotations.initMocks(this);
    }
    
    @Test
    public void submitFinancialDocumentsTest() throws Exception {
	
	// TODO: DEFAULT BEHAVIOR WHEN NO FD ARE FOUND TO SEND TO SAP.
	this.getFindProcessToSubmitMockitoBehavior().thenReturn(null);
	this.fdRepositorySubmitOneyManager.submitFinancialDocuments(1L);
	
	this.getFindProcessToSubmitMockitoBehavior().thenReturn(new ArrayList<Long>());
	this.fdRepositorySubmitOneyManager.submitFinancialDocuments(1L);
	
    }
    
    @Test
    public void asyncSubmitToExternalServicesTest() {
	
	// TODO: DEFAULT BEHAVIOR WHEN NO FD ARE FOUND TO SEND TO SAP.
	this.fdRepositorySubmitOneyManager.asyncSubmitToExternalServices(new ArrayList<Long>(), 1L);
	
    }
    
    private <T> OngoingStubbing<List<Long>> getFindProcessToSubmitMockitoBehavior() throws Exception {
	return Mockito.when(this.financialDocumentDAO.findProcessToSubmit(Mockito.anyList()));
    }
    
    private <T> OngoingStubbing<Throwable> getExceptionMockitoBehavior() throws Exception {
	return Mockito.when(this.ctx.exception(Mockito.anyString()));
    }
    
}
