package com.novabase.omni.oney.modules.financialdocumentrepository.service.ri;

import static io.digitaljourney.platform.modules.paxexam.BundleOptions.mavenBundles;
import static io.digitaljourney.platform.modules.paxexam.BundleOptions.rsProviderWithJPAClient;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.newConfiguration;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.rsProviderWithJPAClientConfiguration;
import static org.ops4j.pax.exam.CoreOptions.composite;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;

import io.digitaljourney.platform.modules.paxexam.base.BaseTestSupport;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.FinancialDocumentRepositoryResource;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class FinancialDocumentRepositoryResourceIT extends BaseTestSupport{

    	@SuppressWarnings("unused")
	@Inject private FinancialDocumentRepositoryResource financialdocumentrepositoryResource;

	@Override
	protected Option bundles() {
		return composite(super.bundles(), rsProviderWithJPAClient(),
				mavenBundles("com.novabase.omni.oney.modules", 
						"financialdocumentrepository-entity", "financialdocumentrepository-data-ri", "financialdocumentrepository-service-api"),
				testBundle("com.novabase.omni.oney.modules", "financialdocumentrepository-service-ri"));
	}

	@Override
	protected Option configurations() {
		return composite(super.configurations(), rsProviderWithJPAClientConfiguration("financialdocumentrepository", newConfiguration(FinancialDocumentRepositoryResourceConfig.CPID)));
	}

	@Test
	public void testBundle() {
		assertBundleActive("com.novabase.omni.oney.modules.financialdocumentrepository-service-ri");
	}

}
