package com.novabase.omni.oney.modules.financialdocumentrepository.service.manager;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.ext.multipart.ContentDisposition;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.modules.ecmservice.service.api.ECMServiceResource;
import com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.dto.FinancialDocumentECMMetadataMsDTO;
import com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.enums.DocumentClassTypeMsEnum;
import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.FinancialDocumentDAO;
import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.exception.NotFoundException;
import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.exception.UnexpectedPersistenceException;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.Attachment;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.FinancialDocument;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.History;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.FinancialDocumentStatusMs;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.FinancialDocumentTypeMs;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.TimeLineStepTypeMs;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.mapper.FinancialDocumentRepositoryResourceMapper;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.ri.FinancialDocumentRepositoryContext;
import com.novabase.omni.oney.modules.referencedata.service.api.ReferenceDataResource;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SearchFilterMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierGeneralDataMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierMsDTO;
import com.novabase.omni.oney.modules.sapservice.service.api.SAPServiceResource;
import com.novabase.omni.oney.modules.sapservice.service.api.dto.FinancialDocumentMsDTO;

import io.digitaljourney.platform.plugins.modules.blob.service.api.BlobResource;

/**
 * This manager is responsible to submit the FinancialDocument data to Oney
 * services and persist the references
 * 
 * @author NB24879
 *
 */
//@formatter:off
@Component(
//	reference = {
//	@Reference(
//		name = FinancialDocumentRepositoryResourceProperties.REF_CONTEXT,
//		service = FinancialDocumentRepositoryContext.class, 
//		cardinality = ReferenceCardinality.MANDATORY)
//	},
	service = { FDRepositorySubmitOneyManager.class })
//@formatter:on
public class FDRepositorySubmitOneyManager {

    @Reference
    private FinancialDocumentRepositoryContext ctx;

    @Reference
    private FDRepositoryDataManager financialDocumentRepositoryDataManager;

    @Reference
    private volatile FinancialDocumentDAO financialDocumentDAO;

    @Reference
    private volatile ECMServiceResource ecmServiceResource;

    @Reference
    private volatile ReferenceDataResource referenceDataResource;

    @Reference
    private volatile BlobResource blobResource;

    @Reference
    private volatile SAPServiceResource sapServiceResource;

    /**
     * Submits the FinancialDocuments that correspond to the types passed on the
     * parameter "financialDocumentTypes" that are approved to the SAP service and
     * submit every attachment in all FinancialDocument approved no matter the type
     * to the ECM service if they are not submitted yet.
     * 
     * @param financialDocumentTypes
     */
    public void submitFinancialDocuments(final Long userId) {

	List<Long> findProcessToSubmit = null;
	try {

	    final List<String> financialDocumentTypesToSend = Arrays.asList(FinancialDocumentTypeMs.values()).parallelStream().map(type -> type.name()).collect(Collectors.toList());

	    findProcessToSubmit = this.financialDocumentDAO.findProcessToSubmit(financialDocumentTypesToSend);

	} catch (UnexpectedPersistenceException e) {
	    throw this.ctx.exception(e.getMessage());
	}

	if (findProcessToSubmit != null && !findProcessToSubmit.isEmpty()) {
	    this.asyncSubmitToExternalServices(findProcessToSubmit, userId);
	}

    }

    /**
     * Start a {@link Runnable} to submit the FinancialDocuments that the Id is in
     * the list financialDocumentIds
     * 
     * @param financialDocumentIds
     */
    public void asyncSubmitToExternalServices(final List<Long> financialDocumentIds, final Long userId) {

	Runnable submitTask = () -> {

	    financialDocumentIds.stream().forEach(financialDocumentId -> {
		try {
		    this.ctx.logInfo("[SubmitFinancialDocumentThread] Going to submit FinancialDocument: " + financialDocumentId + " to Oney Services");
		    this.submitToExternalServices(financialDocumentId, userId);
		} catch (Exception e) {
		    this.ctx.logError("[SubmitFinancialDocumentThread] error trying to submitToExternalServices the FinancialDocument: " + financialDocumentId + " " + e);
		}
	    });

	};

	String financialDocumentIdsString = financialDocumentIds.stream().map(id -> id.toString()).collect(Collectors.joining(","));

	this.ctx.logInfo("[SubmitFinancialDocumentThread] Creating thread to submit FinancialDocument: " + financialDocumentIdsString);
	Thread submitThread = new Thread(submitTask);
	this.ctx.logInfo("[SubmitFinancialDocumentThread] Starting thread to submit FinancialDocument: " + financialDocumentIdsString);
	submitThread.start();
	this.ctx.logInfo("[SubmitFinancialDocumentThread] Thread to submit FinancialDocument: " + financialDocumentIdsString + " is started");

    }

    private void submitToExternalServices(final Long financialDocumentId, final Long userId) {

	final FinancialDocument financialDocument = this.financialDocumentRepositoryDataManager.findFinancialDocument(financialDocumentId);

	this.submitToECM(financialDocument);
	this.submitFinancialDocumentToSAP(financialDocument, userId);

    }

    private void submitFinancialDocumentToSAP(final FinancialDocument financialDocument, final Long userId) {

	// Send to SAP
	final FinancialDocumentMsDTO sapServiceFinancialDocument = FinancialDocumentRepositoryResourceMapper.INSTANCE.toSAPServiceFinancialDocumentMsDTO(financialDocument);
	final SupplierGeneralDataMsDTO supplierGeneralData = this.referenceDataResource.getSupplierMarketInfo(financialDocument.getSupplierCode());
	if (StringUtils.isBlank(supplierGeneralData.nif)) {
	    throw this.ctx.exception("Error Getting the supplier information to SAP submit...");
	}

	sapServiceFinancialDocument.setSupplierNIF(supplierGeneralData.nif);

	final Boolean sucessSubmitted = new Boolean(this.sapServiceResource.submitFinancialDocument(sapServiceFinancialDocument));
	if (sucessSubmitted.booleanValue()) {

	    try {

		// update status
		this.financialDocumentDAO.updateFinancialDocumentStatus(financialDocument, FinancialDocumentStatusMs.SAP_SUBMITTED.name());

		// update history
		final History history = new History();
		history.setFinancialDocument(financialDocument);
		history.setType(TimeLineStepTypeMs.SAP_SUBMITTED.name());
		history.setUserId(userId);
		history.setDate(LocalDateTime.now());

		this.financialDocumentDAO.addHistory(history);

	    } catch (UnexpectedPersistenceException e) {
		throw ctx.exception(e.getMessage());
	    }

	} else {
	    throw ctx.exception("[SubmitFinancialDocumentThread] Failed to submit Financial Document to SAP: ID: " + financialDocument.getDocumentId() + "  Number:" + financialDocument.getNumber());
	}

    }

    private void submitToECM(final FinancialDocument financialDocument) {

	final SearchFilterMsDTO supplierFilter = new SearchFilterMsDTO();
	supplierFilter.id = financialDocument.getSupplierCode();

	final SupplierMsDTO supplierInfo = this.getSuplierInfo(financialDocument.getId(), supplierFilter);

	final FinancialDocumentECMMetadataMsDTO metadata = new FinancialDocumentECMMetadataMsDTO();
	metadata.setStatusKey("1");

	// DocumentTypeKey will be converted later in sapService
	metadata.setFinancialDocumentTypeKey(financialDocument.getType());
	metadata.setDepartmentKey(financialDocument.getDepartmentCode());
	metadata.setPoOrderNumber(financialDocument.getPurchaseOrderFriendlyNumber());
	metadata.setSupplierVatNumber(supplierInfo.nif);
	metadata.setSupplierName(supplierInfo.description);
	metadata.setSupplierId(supplierFilter.id);
	metadata.setProjectKey(financialDocument.getProjectCode());
	metadata.setDocumentNumber(financialDocument.getNumber());

	final DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	metadata.setDocumentDate(financialDocument.getEmissionDate().format(df));
	metadata.setDueDate(financialDocument.getExpirationDate().format(df));

	this.submitFDDocumentToECM(financialDocument, metadata);
	this.submitAttachmentsToECM(financialDocument, metadata);

    }

    private void submitAttachmentsToECM(FinancialDocument financialDocument, FinancialDocumentECMMetadataMsDTO metadata) {

	// filtering all attahcments other than the financial document.
	final List<Attachment> attachmentsToSubmit = this.getAttachmentsToSubmit(financialDocument).stream()
		.filter(attachment -> attachment.getBlobId().longValue() != financialDocument.getDocumentId().longValue()).collect(Collectors.toList());

	attachmentsToSubmit.forEach(attachment -> {

	    if (attachment.getExternalId() == null) {
		
		FinancialDocumentECMMetadataMsDTO attachmentMetadata = new FinancialDocumentECMMetadataMsDTO();
		attachmentMetadata.setSupplierId(metadata.getSupplierId());
		attachmentMetadata.setSupplierName(metadata.getSupplierName());
		attachmentMetadata.setStatusKey(metadata.getStatusKey());
		attachmentMetadata.setSupplierVatNumber(metadata.getSupplierVatNumber());
		attachmentMetadata.setRelationalDoc(financialDocument.getDocumentExternalId());
		attachmentMetadata.setDocumentNumber(financialDocument.getNumber());
		

		final String extenalId = this.submitFileToECM(attachmentMetadata, attachment.getBlobId(), financialDocument.getId(), attachment.getName(),
			FinancialDocumentRepositoryResourceMapper.INSTANCE.toDocumentClassTypeMs(attachment.getDocumentClass()).getEcmDocumentClass());

		attachment.setExternalId(extenalId);

		this.removeSubmittedToECMAttachment(attachment, financialDocument);

	    }

	});

    }

    private String submitFileToECM(final FinancialDocumentECMMetadataMsDTO metadata, final Long blobId, final Long financialDocumentId, final String documentFileName,
	    final DocumentClassTypeMsEnum documentClass) {

	//@formatter:off
	String externalId = null;
	try {

	    final org.apache.cxf.jaxrs.ext.multipart.Attachment document = this.getBlobAttachment(blobId, financialDocumentId, documentFileName);
		
//	    metadata.setObjectName(documentFileName);
	    externalId = this.ecmServiceResource.createDocument(metadata.getDocumentNumber(), documentClass , documentFileName, metadata, document);
			
	} catch (Exception e) {
			
	    this.ctx.logError("[SubmitFinancialDocumentThread] Error: failed to upload Attachment: " + blobId + " to ECMService on submit process of FinancialDocument: " + financialDocumentId);
	    throw ctx.submitECMException(financialDocumentId, blobId);
			
	}
		
	// @formatter:on
	if (StringUtils.isBlank(externalId)) {

	    this.ctx.logError("[SubmitFinancialDocumentThread] Error: failed to upload Attachment: " + blobId + " to ECMService on submit process of FinancialDocument: " + financialDocumentId);
	    throw ctx.submitECMException(financialDocumentId, blobId);

	}

	return externalId;

    }

    private void submitFDDocumentToECM(final FinancialDocument financialDocument, final FinancialDocumentECMMetadataMsDTO metadata) {

	if (financialDocument.getDocumentExternalId() == null) {

	    Attachment attachment = null;
	    try {
		attachment = this.financialDocumentDAO.getFinancialDocumentAttachment(financialDocument.getId(), 1);
	    } catch (NotFoundException | UnexpectedPersistenceException e) {

		this.ctx.logError(
			"[SubmitFinancialDocumentThread] Error: failed to retrieve Attachment with id or index 1 from Database on process of FinancialDocument: " + financialDocument.getId());
		throw ctx.submitECMException(financialDocument.getId(), financialDocument.getDocumentId());

	    }

	    final String externalId = this.submitFileToECM(metadata, financialDocument.getDocumentId(), financialDocument.getId(), financialDocument.getDocumentFileName(),
		    DocumentClassTypeMsEnum.FINANCIAL_DOCUMENT);
	    attachment.setExternalId(externalId);

	    this.removeSubmittedToECMAttachment(attachment, financialDocument);

	    financialDocument.setDocumentExternalId(externalId);

	    try {
		this.financialDocumentDAO.updateFDDocumentExternalId(financialDocument.getId(), externalId);
	    } catch (UnexpectedPersistenceException e) {

		this.ctx.logError("[SubmitFinancialDocumentThread] Error: failed to change FinancialDocument: " + financialDocument.getId() + " to set Document External Id : " + externalId);
		throw ctx.submitECMException(financialDocument.getId(), financialDocument.getDocumentId());

	    }

	}

    }

    private void removeSubmittedToECMAttachment(final Attachment attachment, final FinancialDocument financialDocument) {

	try {

	    this.blobResource.removeBlob(attachment.getBlobId());
	    attachment.setBlobId(null);

	} catch (final Exception e) {
	    this.ctx.logInfo(
		    "[SubmitFinancialDocumentThread] Warning: failed to remove Blob with blobId: " + attachment.getBlobId() + " on submit process of FinancialDocument: " + financialDocument.getId());
	}

	try {
	    this.financialDocumentDAO.updateAttachment(attachment);
	} catch (final UnexpectedPersistenceException e) {

	    this.ctx.logError("[SubmitFinancialDocumentThread] Error: failed to update Attachment: " + attachment.getId() + " on submit process of FinancialDocument: " + financialDocument.getId());
	    throw this.ctx.exception(e.getMessage());

	}

    }

    private org.apache.cxf.jaxrs.ext.multipart.Attachment getBlobAttachment(final Long blobId, final Long financialDocumentId, final String documentName) {

	InputStream blob;
	try {

	    ctx.logInfo("[SubmitFinancialDocumentThread] Reading blob with blobId: " + blobId + " on FinancialDocument: " + financialDocumentId);
	    blob = this.blobResource.getBlob(blobId);

	} catch (Exception e) {

	    ctx.logError("[SubmitFinancialDocumentThread] Failed to read blob with blobId: " + blobId + " on submit process of FinancialDocument: " + financialDocumentId);
	    throw ctx.exception(e);

	}

	final ContentDisposition cd = new ContentDisposition("form-data; name=\"data\"; filename=" + documentName);
	final org.apache.cxf.jaxrs.ext.multipart.Attachment document = new org.apache.cxf.jaxrs.ext.multipart.Attachment("root.message@cxf.apache.org", blob, cd);

	return document;

    }

    /**
     * Gets the {@link SupplierMsDTO} on the ReferenceData MS.<br>
     * 
     * Throws{@link FinancialDocumentRepositoryGetSupplierInfoException} if the
     * supplier is not found.
     * 
     * @param financialDocumentId
     * @param financialDocument
     * @param supplierFilter
     * @return
     */
    //@formatter:off
    private SupplierMsDTO getSuplierInfo(Long financialDocumentId, SearchFilterMsDTO supplierFilter) {
	
	SupplierMsDTO supplierInfo = null;

	try {
	    List<SupplierMsDTO> searchSupplierResult = 
		    referenceDataResource.getSuppliers(supplierFilter);
	    
	    if (searchSupplierResult != null 
		    && searchSupplierResult.isEmpty() == false 
		    && searchSupplierResult.get(0) != null) {
		supplierInfo = searchSupplierResult.get(0);
	    }

	} catch (Exception e) {
	    throw ctx.getSupplierInfoException(financialDocumentId, supplierFilter.id);
	}

	if (supplierInfo == null) {
	    throw ctx.getSupplierInfoException(financialDocumentId, supplierFilter.id);
	}

	return supplierInfo;
    }
    //@formatter:on

    /**
     * Returns the attachments that don't have externalId
     * 
     * @param financialDocument
     * @return
     */
    private List<Attachment> getAttachmentsToSubmit(FinancialDocument financialDocument) {
	//@formatter:off
	return financialDocument.getHistories()
			.stream()
			.filter(historyHasAttachment())
			.flatMap(attachmentsStream())
			.filter(attachment -> attachment != null && StringUtils.isBlank(attachment.getExternalId()))
			.collect(Collectors.toList());
	//@formatter:on
    }

    /**
     * Function to return a stream of the history attachments
     * 
     * @return
     */
    private Function<? super History, ? extends Stream<? extends Attachment>> attachmentsStream() {
	return history -> history.getAttachments().stream();
    }

    /**
     * Predicate that filter the history that have attachments
     * 
     * @return
     */
    private Predicate<? super History> historyHasAttachment() {
	//@formatter:off
	return history -> 
			history.getAttachments() != null 
			&& 
			history.getAttachments().isEmpty() == false;
	//@formatter:on
    }

}
