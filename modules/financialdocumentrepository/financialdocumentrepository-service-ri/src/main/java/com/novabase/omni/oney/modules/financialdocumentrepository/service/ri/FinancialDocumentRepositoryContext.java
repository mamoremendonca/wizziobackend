package com.novabase.omni.oney.modules.financialdocumentrepository.service.ri;

import java.util.Date;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception.FDRepositoryClaimException;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception.FDRepositoryException;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception.FDRepositoryGetSupplierInfoException;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception.FDRepositoryGroupCannotClaimException;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception.FDRepositoryNotFoundException;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception.FDRepositorySubmitECMException;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception.FDRepositoryUniqueConstraintException;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception.FDRepositoryWorkflowReturnException;

import io.digitaljourney.platform.modules.async.api.PlatformAsyncManager;
import io.digitaljourney.platform.modules.cache.api.PlatformCacheManager;
import io.digitaljourney.platform.modules.commons.annotation.DocumentationResource;
import io.digitaljourney.platform.modules.invocation.api.PlatformInvocationManager;
import io.digitaljourney.platform.modules.invocation.api.model.EventLog;
import io.digitaljourney.platform.modules.invocation.api.model.type.StatusType;
import io.digitaljourney.platform.modules.security.api.PlatformSecurityManager;
import io.digitaljourney.platform.modules.ws.rs.api.context.AbstractRSEndpointContext;

// @formatter:off
@Component(
	service = { Object.class, FinancialDocumentRepositoryContext.class },
	reference = {
		@Reference(
			name = FinancialDocumentRepositoryResourceProperties.REF_PLATFORM_SECURITY_MANAGER,
			service = PlatformSecurityManager.class,
			cardinality = ReferenceCardinality.MANDATORY),
        @Reference(
            name = FinancialDocumentRepositoryResourceProperties.REF_PLATFORM_INVOCATION_MANAGER,
            service = PlatformInvocationManager.class,
            cardinality = ReferenceCardinality.MANDATORY),
		@Reference(
			name = FinancialDocumentRepositoryResourceProperties.REF_ASYNC_MANAGER,
			service = PlatformAsyncManager.class,
			cardinality = ReferenceCardinality.MANDATORY),
		@Reference(
			name = FinancialDocumentRepositoryResourceProperties.REF_CACHE_MANAGER,
			service = PlatformCacheManager.class,
			cardinality = ReferenceCardinality.MANDATORY)
	})
@DocumentationResource(FinancialDocumentRepositoryResourceProperties.DOCS_ADDRESS)
// @formatter:on
public class FinancialDocumentRepositoryContext extends AbstractRSEndpointContext {
    
    @Activate
    public void activate(ComponentContext ctx) {
	prepare(ctx);
    }

    public void logError(String message) {
	EventLog event = new EventLog();
	event.setEvent_message(message);
	event.setStart_event_time(new Date());
	getPlatformInvocationManager().logNewEvent(event, null, StatusType.ERROR);
	getLogger().error(message);
    }

    public void logInfo(String message) {
	EventLog event = new EventLog();
	event.setEvent_message(message);
	event.setStart_event_time(new Date());
	getPlatformInvocationManager().logNewEvent(event, null, StatusType.SUCCESS);
	getLogger().info(message);
    }

    public FDRepositoryException exception(String message) {
	return FDRepositoryException.of(this, message);
    }

    public FDRepositoryException exception(Throwable cause) {
	return FDRepositoryException.of(this, cause);
    }

    public FDRepositoryUniqueConstraintException uniqueConstraintException(Long id) {
	return FDRepositoryUniqueConstraintException.of(this, id);
    }

    public FDRepositoryNotFoundException notFoundException(Class<?> entity, Long id) {
	return FDRepositoryNotFoundException.of(this, entity, id);
    }

    public FDRepositoryNotFoundException notFoundException(final Class<?> entity, final String number) {
	return FDRepositoryNotFoundException.of(this, entity, number);
    }

    public FDRepositoryGroupCannotClaimException groupCannotClaimException(Long id, String userGroup) {
	return FDRepositoryGroupCannotClaimException.of(this, id, userGroup);
    }

    public FDRepositoryClaimException claimException(Long id) {
	return FDRepositoryClaimException.of(this, id);
    }

    public FDRepositoryWorkflowReturnException workflowReturnException(Long id) {
	return FDRepositoryWorkflowReturnException.of(this, id);
    }

    public FDRepositorySubmitECMException submitECMException(Long id, Long attachmentId) {
	return FDRepositorySubmitECMException.of(this, id, attachmentId);
    }

    public FDRepositoryGetSupplierInfoException getSupplierInfoException(Long id, String supplierCode) {
	return FDRepositoryGetSupplierInfoException.of(this, id, supplierCode);
    }
}
