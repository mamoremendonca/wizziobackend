package com.novabase.omni.oney.modules.financialdocumentrepository.service.ri;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.modules.financialdocumentrepository.entity.FinancialDocument;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.FinancialDocumentRepositoryResource;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.AttachmentMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.FinancialDocumentListMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.FinancialDocumentMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.SearchCriteriaMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.TimeLineStepMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.WorkflowActionMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.WorkflowMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.manager.FDRepositoryDataManager;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.manager.FDRepositorySubmitOneyManager;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.manager.FDRepositoryWorkflowManager;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.mapper.FinancialDocumentRepositoryResourceMapper;

import io.digitaljourney.platform.modules.security.api.principal.PrincipalDTO;
import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;
import io.digitaljourney.platform.modules.ws.rs.api.resource.AbstractResource;

// @formatter:off
@Component(
	configurationPid = FinancialDocumentRepositoryResourceConfig.CPID, 
	configurationPolicy = ConfigurationPolicy.REQUIRE, 
	reference = {
		@Reference(
			name = FinancialDocumentRepositoryResourceProperties.REF_CONTEXT,
			service = FinancialDocumentRepositoryContext.class, 
			cardinality = ReferenceCardinality.MANDATORY)
	})
@Designate(ocd = FinancialDocumentRepositoryResourceConfig.class)
@RSProvider(FinancialDocumentRepositoryResourceProperties.ADDRESS)
// @formatter:on
public class FinancialDocumentRepositoryResourceImpl extends AbstractResource<FinancialDocumentRepositoryContext, FinancialDocumentRepositoryResourceConfig>
	implements FinancialDocumentRepositoryResource {

    @Reference
    private FDRepositoryDataManager fdRepositoryDataManager;

    @Reference
    private FDRepositorySubmitOneyManager fdRepositorySubmitOneyManager;

    @Reference
    private FDRepositoryWorkflowManager fdRepositoryWorkflowManager;

    @Activate
    public void activate(ComponentContext ctx, FinancialDocumentRepositoryResourceConfig config) {
	prepare(ctx, config);
    }

    @Modified
    public void modified(FinancialDocumentRepositoryResourceConfig config) {
	prepare(config);
    }

    @Override
    @RequiresPermissions(FinancialDocumentRepositoryResourceProperties.PERMISSION_READ)
    public FinancialDocumentMsDTO getFinancialDocument(Long financialDocumentId, String userGroup) {

	FinancialDocument financialDocument = fdRepositoryDataManager.findFinancialDocument(financialDocumentId);

	FinancialDocumentMsDTO financialDocumentDTO = FinancialDocumentRepositoryResourceMapper.INSTANCE.toFinancialDocumentMsDTOWithItemsAndWorkflow(financialDocument);

	financialDocumentDTO.setCanClaim(fdRepositoryWorkflowManager.userGroupCanClaimProcess(financialDocument, userGroup));

	return financialDocumentDTO;
    }

    @Override
    @RequiresPermissions(FinancialDocumentRepositoryResourceProperties.PERMISSION_CREATE)
    public FinancialDocumentMsDTO addFinancialDocument(FinancialDocumentMsDTO financialDocument) {

	return fdRepositoryDataManager.addFinancialDocument(financialDocument, getLimitDateToNextStep());
    }

    @Override
    @RequiresPermissions(FinancialDocumentRepositoryResourceProperties.PERMISSION_EXECUTE)
    public WorkflowMsDTO updateWorkflow(Long financialDocumentId, WorkflowActionMsDTO action) {

	final PrincipalDTO principal = currentPrincipal();
	return this.fdRepositoryWorkflowManager.updateWorkflow(financialDocumentId, action, getLimitDateToNextStep(), Long.parseLong(principal.id));
    }

    @Override
    @RequiresPermissions(FinancialDocumentRepositoryResourceProperties.PERMISSION_READ)
    public List<TimeLineStepMsDTO> getTimeLine(Long financialDocumentId) {

	return fdRepositoryDataManager.getTimeLine(financialDocumentId);
    }

    @Override
    @RequiresPermissions(FinancialDocumentRepositoryResourceProperties.PERMISSION_READ)
    public List<FinancialDocumentListMsDTO> search(SearchCriteriaMsDTO searchCriteria) {
	return this.fdRepositoryDataManager.search(searchCriteria);
    }

    @Override
    @RequiresPermissions(FinancialDocumentRepositoryResourceProperties.PERMISSION_EXECUTE)
    public WorkflowMsDTO claimProcess(Long financialDocumentId, Long userId, String userGroup, WorkflowActionMsDTO workflowAction) {

	return fdRepositoryWorkflowManager.claimProcess(financialDocumentId, userId, userGroup, workflowAction, getLimitDateToNextStep());
    }

    @RequiresPermissions(FinancialDocumentRepositoryResourceProperties.PERMISSION_READ)
    public List<AttachmentMsDTO> getFinancialDocumentAttachments(Long financialDocumentId) {

	return fdRepositoryDataManager.getFinancialDocumentAttachments(financialDocumentId);
    }

    @Override
    @RequiresPermissions(FinancialDocumentRepositoryResourceProperties.PERMISSION_READ)
    public AttachmentMsDTO getFinancialDocumentAttachment(Long financialDocumentId, int index) {

	return fdRepositoryDataManager.getFinancialDocumentAttachment(financialDocumentId, index);
    }

    @Override
    @RequiresPermissions(FinancialDocumentRepositoryResourceProperties.PERMISSION_EXECUTE)
    public void runFinancialDocumentSubmitJob() {

	final PrincipalDTO principal = this.currentPrincipal();
	this.fdRepositorySubmitOneyManager.submitFinancialDocuments(Long.parseLong(principal.id));

    }

    private LocalDateTime getLimitDateToNextStep() {
	LocalDateTime date = LocalDateTime.now();
	return date.plusDays(getConfig().maxDays());
    }
    

    @Override
    @RequiresPermissions(FinancialDocumentRepositoryResourceProperties.PERMISSION_READ)
    public FinancialDocumentMsDTO getFinancialDocumentNotCanceledNotRejectedByNumber(final String financialDocumentNumber) {

	final FinancialDocument financialDocument = this.fdRepositoryDataManager.getFinancialDocumentNotCanceledNotRejectedByNumber(financialDocumentNumber);

	return FinancialDocumentRepositoryResourceMapper.INSTANCE.toFinancialDocumentMsDTOWithItems(financialDocument);

    }

}
