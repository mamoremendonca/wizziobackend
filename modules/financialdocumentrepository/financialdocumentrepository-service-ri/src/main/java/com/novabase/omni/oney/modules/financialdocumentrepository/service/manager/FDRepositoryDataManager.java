package com.novabase.omni.oney.modules.financialdocumentrepository.service.manager;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.FinancialDocumentDAO;
import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.exception.NotFoundException;
import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.exception.UnexpectedPersistenceException;
import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.exception.UniqueConstraintViolationException;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.Attachment;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.FinancialDocument;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.History;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.Workflow;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.AttachmentMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.FinancialDocumentListMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.FinancialDocumentMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.SearchCriteriaMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.TimeLineStepMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.FinancialDocumentStatusMs;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.mapper.FinancialDocumentRepositoryResourceMapper;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.ri.FinancialDocumentRepositoryContext;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.ri.WorkflowResourceMapper;

/**
 * This manager is responsible to persist and retrieve FinancialDocument data
 * 
 * @author NB24879
 *
 */
//@formatter:off
@Component(
//	reference = {
//	@Reference(
//		name = FinancialDocumentRepositoryResourceProperties.REF_CONTEXT,
//		service = FinancialDocumentRepositoryContext.class, 
//		cardinality = ReferenceCardinality.MANDATORY)
//	},
	service = { FDRepositoryDataManager.class })
//@formatter:on
public class FDRepositoryDataManager {

    @Reference
    private FinancialDocumentRepositoryContext ctx;

    @Reference
    private FinancialDocumentDAO financialDocumentDAO;

    /**
     * Finds a {@link FinancialDocument} by Id and throw exception when: Not Found
     * or some Unexpected error occur
     * 
     * @param financialDocumentId
     * @return FinancialDocument
     */
    public FinancialDocument findFinancialDocument(Long financialDocumentId) {

	try {
	    return this.financialDocumentDAO.getFinancialDocument(financialDocumentId);
	} catch (NotFoundException e) {
	    throw this.ctx.notFoundException(e.getClass(), financialDocumentId);
	} catch (UnexpectedPersistenceException e) {
	    throw this.ctx.exception(e.getMessage());
	}
    }

    /**
     * 
     * @param financialDocument
     * @param limitDateToNextStep
     * @return FinancialDocumentMsDTO
     */
    public FinancialDocumentMsDTO addFinancialDocument(FinancialDocumentMsDTO financialDocument, LocalDateTime limitDateToNextStep) {

	try {

	    // failsafe for concurrency problems with FD submission
	    final Long financialDocumentId = this.financialDocumentDAO.getFinancialDocumentIdByInstanceId(financialDocument.getInstanceId());
	    if (financialDocumentId != null && financialDocumentId.longValue() > 0) {
		this.financialDocumentDAO.deleteFinancialDocumentById(financialDocumentId);
	    }

	    financialDocument.getWorkflow().setLimitDateToNextStep(limitDateToNextStep);
	    financialDocument.setStatus(FinancialDocumentStatusMs.IN_APPROVAL);

	    return FinancialDocumentRepositoryResourceMapper.INSTANCE
		    .toFinancialDocumentMsDTOWithItemsAndWorkflow(financialDocumentDAO.addFinancialDocument(FinancialDocumentRepositoryResourceMapper.INSTANCE.toFinancialDocument(financialDocument)));

	} catch (UniqueConstraintViolationException e) {

	    ctx.logError(e.getMessage());
	    throw ctx.uniqueConstraintException(financialDocument.getId());

	} catch (UnexpectedPersistenceException | NotFoundException e) {

	    ctx.logError(e.getMessage());
	    throw ctx.exception(e.getMessage());

	}

    }

    /**
     * Finds the histories and the scheduled steps of one FinancialDocument and
     * returns the time line
     * 
     * @param financialDocumentId
     * @return List<TimeLineStepMsDTO>
     */
    public List<TimeLineStepMsDTO> getTimeLine(Long financialDocumentId) {

	Workflow workflow;
	List<History> histories;

	try {

	    workflow = financialDocumentDAO.searchFinancialDocumentWorkFlow(financialDocumentId);
	    histories = financialDocumentDAO.searchFinancialDocumentHistory(financialDocumentId);

	} catch (UnexpectedPersistenceException e) {
	    throw ctx.exception(e.getMessage());
	}

	if (workflow == null || workflow.getSteps() == null || workflow.getSteps().isEmpty()) {
	    throw ctx.notFoundException(Workflow.class, financialDocumentId);
	}
	if (histories == null || histories.isEmpty()) {
	    throw ctx.notFoundException(History.class, financialDocumentId);
	}

	return WorkflowResourceMapper.INSTANCE.toTimeLineStepMsDTOList(workflow, histories);
    }

    /**
     * Search FinancialDocuments by criteria filters
     * 
     * @param searchCriteria
     * @return List<FinancialDocumentMsDTO>
     */
    public List<FinancialDocumentListMsDTO> search(SearchCriteriaMsDTO searchCriteria) {

	try {

	    this.ctx.logInfo(String.format("Received Criteria Data in FDRepositoryDataManager: %s", searchCriteria.toString()));
	    final List<FinancialDocument> resultList = this.financialDocumentDAO.searchFinancialDocuments(FinancialDocumentRepositoryResourceMapper.INSTANCE.toSearchCriteria(searchCriteria));

	    this.ctx.logInfo(String.format("Search Result: Total Resutl List Size: %d", resultList.size()));

	    return FinancialDocumentRepositoryResourceMapper.INSTANCE.toFinancialDocumentListMsDTOList(resultList);

	} catch (UnexpectedPersistenceException e) {
	    throw this.ctx.exception(e.getMessage());
	}

    }

    /**
     * Finds and returns the attachments of the FinancialDocument by
     * financialDocumentId
     * 
     * @param financialDocumentId
     * @return List<AttachmentMsDTO>
     */
    public List<AttachmentMsDTO> getFinancialDocumentAttachments(Long financialDocumentId) {

	try {
	    List<Attachment> attachments = financialDocumentDAO.getFinancialDocumentAttachments(financialDocumentId);

	    return attachments.stream().map(att -> FinancialDocumentRepositoryResourceMapper.INSTANCE.toAttachmentMsDTO(att)).collect(Collectors.toList());
	} catch (NotFoundException e) {
	    throw ctx.notFoundException(e.getClass(), financialDocumentId);
	} catch (UnexpectedPersistenceException e) {
	    throw ctx.exception(e.getMessage());
	}
    }

    /**
     * Finds and returns one specified attachment of the FinancialDocument by
     * financialDocumentId and index
     * 
     * @param financialDocumentId
     * @param index
     * @return
     */
    public AttachmentMsDTO getFinancialDocumentAttachment(Long financialDocumentId, int index) {

	try {

	    final Attachment attachment = financialDocumentDAO.getFinancialDocumentAttachment(financialDocumentId, index);
	    final AttachmentMsDTO attachmentMsDTO = FinancialDocumentRepositoryResourceMapper.INSTANCE.toAttachmentMsDTO(attachment);

	    return attachmentMsDTO;

	} catch (NotFoundException e) {
	    throw ctx.notFoundException(e.getClass(), financialDocumentId);
	} catch (UnexpectedPersistenceException e) {
	    throw ctx.exception(e.getMessage());
	}

    }

    /**
     * Finds a {@link FinancialDocument} by Number and not canceled and not rejected
     * and throw exception when: Not Found or some Unexpected error occur
     * 
     * @param financialDocumentNumber
     * @return FinancialDocument
     */
    public FinancialDocument getFinancialDocumentNotCanceledNotRejectedByNumber(final String financialDocumentNumber) {

	try {
	    return this.financialDocumentDAO.getFinancialDocumentNotCanceledNotRejectedByNumber(financialDocumentNumber);
	} catch (NotFoundException e) {
	    throw this.ctx.notFoundException(e.getClass(), financialDocumentNumber);
	} catch (UnexpectedPersistenceException e) {
	    throw this.ctx.exception(e.getMessage());
	}
    }

}
