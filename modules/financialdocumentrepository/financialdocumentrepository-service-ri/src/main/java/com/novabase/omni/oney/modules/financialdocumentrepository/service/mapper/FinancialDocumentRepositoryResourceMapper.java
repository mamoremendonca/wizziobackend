package com.novabase.omni.oney.modules.financialdocumentrepository.service.mapper;

import java.util.List;
import java.util.Set;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.modules.financialdocumentrepository.entity.Attachment;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.FinancialDocument;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.History;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.Item;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.SearchCriteria;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.Workflow;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.WorkflowStep;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.AttachmentMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.FinancialDocumentListMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.FinancialDocumentMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.HistoryMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.ItemMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.SearchCriteriaMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.WorkflowMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.WorkflowStepMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.DocumentClassTypeMs;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.FinancialDocumentStatusMs;

@Mapper
public abstract class FinancialDocumentRepositoryResourceMapper {
    public static final FinancialDocumentRepositoryResourceMapper INSTANCE = Mappers.getMapper(FinancialDocumentRepositoryResourceMapper.class);

    @Mapping(source = "items", ignore = true, target = "items")
    @Mapping(source = "histories", ignore = true, target = "histories")
    @Mapping(source = "workflow.steps", ignore = true, target = "workflow.steps")
    public abstract FinancialDocumentMsDTO toFinancialDocumentMsDTO(FinancialDocument financialDocument);

    @Mapping(source = "histories", ignore = true, target = "histories")
    @Mapping(source = "workflow", ignore = true, target = "workflow")
    public abstract FinancialDocumentMsDTO toFinancialDocumentMsDTOWithItems(FinancialDocument financialDocument);

    @Mapping(source = "histories", ignore = true, target = "histories")
    @Mapping(source = "workflow", target = "workflow")
    @Mapping(source = "status", target = "workflow.financialDocumentStatus")
    public abstract FinancialDocumentMsDTO toFinancialDocumentMsDTOWithItemsAndWorkflow(FinancialDocument financialDocument);

    public abstract FinancialDocument toFinancialDocument(FinancialDocumentMsDTO financialDocument);
    
    @Mapping(source = "index", target = "idx")
    public abstract Item itemMsDTOToItem(ItemMsDTO itemMsDTO);

    @Mapping(source = "code", target = "articleCode")
    @Mapping(source = "description", target = "articleDescription")
    @Mapping(source = "item.financialDocument.internalOrderCode", target = "internalOrderCode")
    @Mapping(source = "retentionCode", target = "withholdingTaxCode")
    public abstract com.novabase.omni.oney.modules.sapservice.service.api.dto.ItemFDMsDTO toSAPServiceItemFDMsDTO(final Item item);

    @IterableMapping(elementTargetType = ItemMsDTO.class)
    public abstract List<com.novabase.omni.oney.modules.sapservice.service.api.dto.ItemMsDTO> toListSAPServiceItemMsDTO(final List<Item> item);
    
//    @Mapping(expression = "java(com.novabase.omni.oney.modules.sapservice.service.api.enums.FinancialDocumentStatusMs.valueOf(financialDocument.getStatus()))", target = "status")
    @Mapping(expression = "java(com.novabase.omni.oney.modules.sapservice.service.api.enums.FinancialDocumentTypeMs.valueOf(financialDocument.getType()))", target = "type")
    @Mapping(source = "number", target = "documentNumber")
    @Mapping(source = "documentExternalId", target = "ecmNumber")
    @Mapping(source = "finishDate", target = "approvalDate")
    public abstract com.novabase.omni.oney.modules.sapservice.service.api.dto.FinancialDocumentMsDTO toSAPServiceFinancialDocumentMsDTO(final FinancialDocument financialDocument);

    @Mapping(target = "financialDocumentStatus", expression = "java(financialDocumentStatusMs)")
    public abstract WorkflowMsDTO toWorkflowMsDTO(Workflow workflow, FinancialDocumentStatusMs financialDocumentStatusMs);

    @Mapping(source = "index", target = "idx")
    public abstract WorkflowStep toWorkflowStep(WorkflowStepMsDTO workflowStepMsDTO);

    @Mapping(source = "idx", target = "index")
    public abstract WorkflowStepMsDTO toWorkflowStepMsDTO(WorkflowStep workflowStep);

    public abstract List<ItemMsDTO> toItemListMsDTO(List<Item> searchFinancialDocumentItems);

    public abstract ItemMsDTO toItemMsDTO(Item item);

    public abstract List<HistoryMsDTO> toHistoryListMsDTO(List<History> searchFinancialDocumentHistory);

    public abstract History toHistory(HistoryMsDTO history);

    public abstract HistoryMsDTO toHistoryMsDTO(History addHistory);

    @Mapping(source = "index", target = "idx")
    public abstract Attachment toAttachment(AttachmentMsDTO attachment);

    @Mapping(source = "idx", target = "index")
    public abstract AttachmentMsDTO toAttachmentMsDTO(Attachment attachment);

    public abstract Set<Attachment> toAttachmentList(List<AttachmentMsDTO> attachment);

    public abstract List<AttachmentMsDTO> toAttachmentListMsDTO(List<Attachment> attachment);

    @Mapping(source = "orderBy.field", target = "orderField")
    @Mapping(source = "orderBy.type", target = "orderType")
    public abstract SearchCriteria toSearchCriteria(final SearchCriteriaMsDTO searchCriteria);

    @Mapping(source = "workflow.currentUserGroup", target = "currentUserGroup")
    public abstract FinancialDocumentListMsDTO toFinancialDocumentListMsDTO(final FinancialDocument financialDocument);

    @IterableMapping(elementTargetType = FinancialDocumentListMsDTO.class)
    public abstract List<FinancialDocumentListMsDTO> toFinancialDocumentListMsDTOList(final List<FinancialDocument> financialDocument);
    
    public DocumentClassTypeMs toDocumentClassTypeMs(String documentClassTypeMs) {

	DocumentClassTypeMs enumValue = DocumentClassTypeMs.FINANCIAL_DOCUMENT_ATTACHMENT;

	try {
	    enumValue = DocumentClassTypeMs.valueOf(documentClassTypeMs);
	} catch (Exception e) {
	}

	// formatter:off
	return enumValue != null ? enumValue : DocumentClassTypeMs.FINANCIAL_DOCUMENT_ATTACHMENT;
	// formatter:on
    }
}
