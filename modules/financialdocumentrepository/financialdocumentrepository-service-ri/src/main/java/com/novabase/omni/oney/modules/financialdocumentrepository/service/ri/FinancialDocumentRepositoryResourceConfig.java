package com.novabase.omni.oney.modules.financialdocumentrepository.service.ri;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/financialdocumentrepository", icon = @Icon(resource = "OSGI-INF/icon/resource.png", size = 32))
public @interface FinancialDocumentRepositoryResourceConfig {
    public static final String CPID = "oney.base.modules.financialdocumentrepository.service.ri.financialdocumentrepository";

    /**
     * Max number of days that a group can be with the Financial Document.
     * 
     * @return Max Days
     */
    @AttributeDefinition(name = "%maxDays.name", type = AttributeType.INTEGER, description = "%maxDays.description")
    int maxDays() default 7;


}
