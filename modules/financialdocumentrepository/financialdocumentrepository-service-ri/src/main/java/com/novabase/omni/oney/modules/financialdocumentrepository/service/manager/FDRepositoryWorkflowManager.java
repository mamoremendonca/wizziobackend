package com.novabase.omni.oney.modules.financialdocumentrepository.service.manager;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.FinancialDocumentDAO;
import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.exception.UnexpectedPersistenceException;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.FinancialDocument;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.FinancialDocumentBuilder;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.History;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.HistoryBuilder;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.Workflow;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.WorkflowStep;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.WorkflowStepBuilder;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.WorkflowActionMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto.WorkflowMsDTO;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.FinancialDocumentStatusMs;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.TimeLineStepTypeMs;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.WorkFlowActionTypeMs;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.mapper.FinancialDocumentRepositoryResourceMapper;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.ri.FinancialDocumentRepositoryContext;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.ri.WorkflowResourceMapper;

/**
 * This manager is responsible to manage the workflow and to persist updates on
 * the workflow
 * 
 * @author NB24879
 *
 */
//@formatter:off
@Component(
//	reference = {
//	@Reference(
//		name = FinancialDocumentRepositoryResourceProperties.REF_CONTEXT,
//		service = FinancialDocumentRepositoryContext.class, 
//		cardinality = ReferenceCardinality.MANDATORY)
//	},
	service = { FDRepositoryWorkflowManager.class })
//@formatter:on
public class FDRepositoryWorkflowManager {

    @Reference
    private FinancialDocumentRepositoryContext ctx;

    @Reference
    private volatile FinancialDocumentDAO financialDocumentDAO;

    @Reference
    private FDRepositoryDataManager financialDocumentRepositoryDataManager;

    @Reference
    private FDRepositorySubmitOneyManager financialDocumentRepositorySubmitOneyManager;

    /**
     * Update workflow using the currentAction
     * 
     * @param financialDocumentId
     * @param action
     * @param limitDateToNextStep
     * 
     * @return WorkflowMsDTO
     */
    public WorkflowMsDTO updateWorkflow(final Long financialDocumentId, final WorkflowActionMsDTO action, final LocalDateTime limitDateToNextStep, final Long userId) {

	FinancialDocument financialDocument = financialDocumentRepositoryDataManager.findFinancialDocument(financialDocumentId);

	Workflow workflow = financialDocument.getWorkflow();

	int currentStepIndex = workflow.getCurrentIndex();

	WorkflowStep currentWorkflowStep = workflow.getWorkflowStepByIndex(currentStepIndex);

	//@formatter:off
	Long currentWorkflowStepId = 
		currentWorkflowStep != null 
			? currentWorkflowStep.getId() 
			: null;
	//@formatter:on

	History history = WorkflowResourceMapper.INSTANCE.toHistory(action);

	FinancialDocumentStatusMs financialDocumentStatus = null;
	LocalDateTime finishDate = null;

	if (WorkFlowActionTypeMs.APPROVE.equals(action.getType()) && currentWorkflowStep.isHasNextStep()) {

	    //@formatter:off
	    int nextStepIndex = 
		    workflow.getSteps()
		    	.stream()
		    	.filter(step -> 
		    		step.isActive() 
		    			&& step.getIdx() > currentStepIndex)
		    	.map(WorkflowStep::getIdx)
		    	.findFirst()
		    	.orElse(0);
	    //@formatter:on

	    if (nextStepIndex != 0) {
		updateWorkflowToIndex(workflow, nextStepIndex, limitDateToNextStep);
	    } else {
		// fail safe, Approved
		ctx.logError("Purchase Order with ID: " + financialDocumentId + " are being approved because the search for the next active workflowstep returned index 0");
		endWorkflow(workflow);
		financialDocumentStatus = FinancialDocumentStatusMs.APPROVED;
		finishDate = history.getDate();
	    }

	} else if (WorkFlowActionTypeMs.RETURN.equals(action.getType())) {

	    //@formatter:off
	    int previousStepIndex = 
		    	workflow.getSteps()
		    	.stream()
			.sorted((step1, step2) -> step2.getIdx() - step1.getIdx())
			.filter(step -> 
				step.isActive() 
			    	&& step.getIdx() < currentStepIndex)
			.map(WorkflowStep::getIdx)
			.findFirst()
			.orElse(0);
	    //@formatter:on

	    if (previousStepIndex == 1) {
		throw ctx.workflowReturnException(financialDocumentId);
	    }

	    updateWorkflowToIndex(workflow, previousStepIndex, limitDateToNextStep);

	} else if (WorkFlowActionTypeMs.REJECT.equals(action.getType())) {

	    endWorkflow(workflow);
	    financialDocumentStatus = FinancialDocumentStatusMs.REJECTED;
	    finishDate = history.getDate();

	} else if (WorkFlowActionTypeMs.CANCEL.equals(action.getType())) {

	    endWorkflow(workflow);
	    financialDocumentStatus = FinancialDocumentStatusMs.CANCELED;

	} else if (WorkFlowActionTypeMs.PAY.equals(action.getType())) {

	    financialDocumentStatus = FinancialDocumentStatusMs.PAID;

	} else {
	    // Approved
	    endWorkflow(workflow);
	    financialDocumentStatus = FinancialDocumentStatusMs.APPROVED;
	    finishDate = history.getDate();
	}

	try {

	    addWorkflowUpdateHistory(financialDocumentId, currentWorkflowStepId, history);

	    //@formatter:off
	    String financialDocumentStatusString = 
		    financialDocumentStatus != null 
		    	? financialDocumentStatus.name() 
		    	: null;
		    	
	    WorkflowMsDTO workflowMsDTO = 
		    WorkflowResourceMapper.INSTANCE.toWorkflow(
			    financialDocumentDAO.updateWorkFlow(
				    workflow, 
				    financialDocumentId, 
				    financialDocumentStatusString, 
				    finishDate),
		    financialDocumentStatus);

	    // If the FinancialDocument is approved we must submit the date to Oney services(ECM & SAP)
	    if (FinancialDocumentStatusMs.APPROVED.equals(financialDocumentStatus)) {
		financialDocumentRepositorySubmitOneyManager.asyncSubmitToExternalServices(Arrays.asList(financialDocumentId),  userId);
	    }
	    //@formatter:on

	    return workflowMsDTO;
	} catch (UnexpectedPersistenceException e) {
	    throw ctx.exception(e.getMessage());
	}
    }

    /**
     * Claims the process to some group that are schedule to the next steps on the
     * workflow.
     * 
     * @param financialDocumentId
     * @param userId
     * @param userGroup
     * @param workflowAction
     * @param limitDateToNextStep
     * 
     * @return WorkflowMsDTO
     */
    public WorkflowMsDTO claimProcess(Long financialDocumentId, Long userId, String userGroup, WorkflowActionMsDTO workflowAction, LocalDateTime limitDateToNextStep) {

	FinancialDocument financialDocument = financialDocumentRepositoryDataManager.findFinancialDocument(financialDocumentId);

	if (userGroupCanClaimProcess(financialDocument, userGroup) == false) {
	    throw ctx.groupCannotClaimException(financialDocumentId, userGroup);
	}

	Workflow workflow = financialDocument.getWorkflow();
	int currentStepIndex = workflow.getCurrentIndex();

	int targetStepIndex = findTargetStepIndex(userGroup, workflow);

	if (targetStepIndex == 0) {
	    throw ctx.groupCannotClaimException(financialDocumentId, userGroup);
	}

	List<WorkflowStep> skippedSteps = getSkippedSteps(workflow, currentStepIndex, targetStepIndex);

	if (skippedSteps == null || skippedSteps.isEmpty() == true) {
	    throw ctx.groupCannotClaimException(financialDocumentId, userGroup);
	}

	List<History> histories = generateSkippedHistories(userId, workflowAction, financialDocument, skippedSteps);

	skippedSteps.forEach(step -> step.setActive(false)); // In order to guarantee that the skipped steps will not be executed in case of
							     // the P.O be returned we must set the active flag to false

	updateWorkflowToIndex(workflow, targetStepIndex, limitDateToNextStep);
	try {
	    financialDocumentDAO.addHistories(histories);
	    //@formatter:off
	    return FinancialDocumentRepositoryResourceMapper.INSTANCE
		    .toWorkflowMsDTO(
			    financialDocumentDAO.updateWorkFlow(
				    workflow, 
				    financialDocumentId, 
				    null, 
				    null), 
			    FinancialDocumentStatusMs.IN_APPROVAL);
	    //@formatter:on
	} catch (UnexpectedPersistenceException e) {
	    throw ctx.exception(e.getMessage());
	}
    }

    /**
     * Persist a history to the financialDocument
     * 
     * @param financialDocumentId
     * @param currentWorkflowStepId
     * @param history
     * @throws UnexpectedPersistenceException
     */
    private void addWorkflowUpdateHistory(Long financialDocumentId, Long currentWorkflowStepId, History history) throws UnexpectedPersistenceException {

	history.setFinancialDocument(new FinancialDocumentBuilder().withId(financialDocumentId).build());

	// If the FinancialDocument is being canceled we don't have workFlowStepId
	if (currentWorkflowStepId != null) {
	    history.setWorkflowStep(new WorkflowStepBuilder().withId(currentWorkflowStepId).build());
	}

	financialDocumentDAO.addHistory(history);
    }

    /**
     * Generate skipped histories, if there some attachment in the currentAction we
     * add this attachment to the first generated history
     * 
     * @param userId
     * @param workflowAction
     * @param financialDocument
     * @param skippedSteps
     * 
     * @return List<History>
     */
    private List<History> generateSkippedHistories(Long userId, WorkflowActionMsDTO workflowAction, FinancialDocument financialDocument, List<WorkflowStep> skippedSteps) {
	//@formatter:off
	List<History> histories = skippedSteps
		.stream()
		.map(step -> 
        		{
        		    return new HistoryBuilder()
        			    	.withDate(LocalDateTime.now())
        			    	.withWorkflowStep(step)
        			    	.withType(TimeLineStepTypeMs.SKIPPED.name())
        			    	.withFinancialDocument(financialDocument)
        			    	.withUserId(userId)
        			    	.withUserGroup(step.getUserGroup()) // In order to keep the original group step we save the history with the original group and not with the claimGroup
        			    	.build();
        		})
		.collect(Collectors.toList());

	if (workflowAction != null 
		&& workflowAction.getAttachments() != null 
		&& workflowAction.getAttachments().isEmpty() == false) {

	    histories.stream()
	    	.findFirst()
	    	.get()
	    	.setAttachments(
	    		FinancialDocumentRepositoryResourceMapper.INSTANCE.toAttachmentList(workflowAction.getAttachments())
	    		);
	}
	return histories;
	//@formatter:on
    }

    /**
     * Find the workFlowStepIndex by userGroup
     * 
     * @param userGroup
     * @param workflow
     * @return Integer
     */
    private Integer findTargetStepIndex(String userGroup, Workflow workflow) {
	//@formatter:off
	return workflow.getSteps()
		.stream()
		.filter(step -> step.getUserGroup().equals(userGroup))
		.map(WorkflowStep::getIdx)
		.findFirst()
		.orElse(0);
	//@formatter:on
    }

    /**
     * Get the skipped steps of the workflow by currentStepIndex and targetStepIndex
     * 
     * @param workflow
     * @param currentStepIndex
     * @param targetStepIndex
     * 
     * @return List<WorkflowStep>
     */
    private List<WorkflowStep> getSkippedSteps(Workflow workflow, int currentStepIndex, int targetStepIndex) {
	//@formatter:off
	return workflow.getSteps()
			.stream()
			.sorted((step1, step2) -> step1.getIdx() - step2.getIdx())
			.filter(step -> step.getIdx() >= currentStepIndex 
					&& step.getIdx() < targetStepIndex)
			.collect(Collectors.toList());
	//@formatter:on
    }

    /**
     * Validate if the userGroup is schedule on FinancialDocument approval workflow
     * and if this step was not executed yet.
     * 
     * @param financialDocument
     * @param userGroup
     * 
     * @return boolean true when the currentUserGroup can claim the
     *         FinancialDocument else false
     */
    public boolean userGroupCanClaimProcess(FinancialDocument financialDocument, String userGroup) {

	if (StringUtils.isBlank(userGroup)) {
	    return false;
	}

	Workflow workflow = financialDocument.getWorkflow();
	int currentIndex = workflow.getCurrentIndex();
	String currentGroup = workflow.getCurrentUserGroup();

	//@formatter:off
	if(workflow.isHasNextStep() == false 
		|| userGroup.equals(currentGroup) 
		|| currentIndex == 0) {
	    
	    return false;
	}
	
	return workflow.getSteps()
			.stream()
			.anyMatch(step -> 	
					step.getIdx() > currentIndex 
        				&& userGroup.equals(step.getUserGroup()));
	//@formatter:on
    }

    /**
     * Updates the Workflow to the target index
     * 
     * @param workflow
     * @param targetIndex
     * @param limitDateToNextStep
     */
    private void updateWorkflowToIndex(Workflow workflow, int targetIndex, LocalDateTime limitDateToNextStep) {
	WorkflowStep nextWorkFlowStep;
	nextWorkFlowStep = workflow.getWorkflowStepByIndex(targetIndex);
	nextWorkFlowStep.setActive(true); // When the FinancialDocument is claimed, the step maybe was inactive, so here
					  // we
					  // guarantee that the current step is active
	workflow.setCurrentIndex(targetIndex);
	workflow.setCurrentUserGroup(nextWorkFlowStep.getUserGroup());
	workflow.setHasNextStep(nextWorkFlowStep.isHasNextStep());
	workflow.setLimitDateToNextStep(limitDateToNextStep);
    }

    /**
     * Finish the Workflow setting: currentUserGroup = NULL limitDateToNextStep =
     * NULL currentIndex = 0 hasNextStep = false
     * 
     * @param workflow
     */
    private void endWorkflow(Workflow workflow) {
	workflow.setCurrentIndex(0);
	workflow.setCurrentUserGroup(null);
	workflow.setHasNextStep(false);
	workflow.setLimitDateToNextStep(null);
    }

}
