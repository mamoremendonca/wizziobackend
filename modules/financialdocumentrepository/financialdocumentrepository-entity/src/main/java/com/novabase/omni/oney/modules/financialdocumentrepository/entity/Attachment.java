package com.novabase.omni.oney.modules.financialdocumentrepository.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import net.karneim.pojobuilder.GeneratePojoBuilder;

//@formatter:off
@Entity
@Table(name = "attachment", 
	uniqueConstraints = { 
		@UniqueConstraint(columnNames = "id", name = "seq_attachment_id"),
		@UniqueConstraint(columnNames = "blob_id", name = "blob_id_unique_constraint"),
		@UniqueConstraint(columnNames = "external_id", name = "external_id_unique_constraint")
	}
)
@GeneratePojoBuilder
@NamedQueries({
	@NamedQuery(
		name = Attachment.NAMED_QUERY_FIND_ATTACHMENT_FETCHING_ALL, 
		query = "Select attachment from FinancialDocument financialDocument "
			+ "join financialDocument.histories history "
			+ "join history.attachments attachment "
			+ "where financialDocument.id = :financialDocumentId "),
	@NamedQuery(
		name = Attachment.NAMED_QUERY_FIND_ATTACHMENT_FETCHING_ONE, 
		query = "Select attachment from FinancialDocument as financialDocument "
			+ "join financialDocument.histories as history "
			+ "join history.attachments as attachment "
			+ "where financialDocument.id = :financialDocumentId "
			+ "  and attachment.idx = :idx ")
	})

//@formatter:on
public class Attachment {

    public static final String NAMED_QUERY_FIND_ATTACHMENT_FETCHING_ALL = "Attachment.findAttachmentsByFinancialDocumentId";
    public static final String NAMED_QUERY_FIND_ATTACHMENT_FETCHING_ONE = "Attachment.findAttachmentByFinancialDocumentIdAndAttachmentIndex";
    
    @Id
    @SequenceGenerator(name = "attachment_seq_task", sequenceName = "seq_attachment_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "attachment_seq_task")
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "idx")
    private int idx;

    @ManyToOne
    @JoinColumn(name = "history_id")
    private History history;

    @Column(name = "blob_id")
    private Long blobId;

    @Column(name = "external_id")
    private String externalId;

    @Column(name = "document_class")
    private String documentClass;

    @Column(name = "mime_type")
    private String mimeType;

    @Column(name = "name")
    private String name;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "date")
    private LocalDateTime date;

    public Attachment() {
	super();
    }

    public Attachment(Long id, History history, Long blobId, String externalId, String documentClass, String mimeType, String name, Long userId, LocalDateTime date) {
	super();
	this.id = id;
	this.history = history;
	this.blobId = blobId;
	this.externalId = externalId;
	this.documentClass = documentClass;
	this.mimeType = mimeType;
	this.name = name;
	this.userId = userId;
	this.date = date;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public int getIdx() {
	return idx;
    }

    public void setIdx(int idx) {
	this.idx = idx;
    }

    public History getHistory() {
	return history;
    }

    public void setHistory(History history) {
	this.history = history;
    }

    public Long getBlobId() {
	return blobId;
    }

    public void setBlobId(Long blobId) {
	this.blobId = blobId;
    }

    public String getExternalId() {
	return externalId;
    }

    public void setExternalId(String externalId) {
	this.externalId = externalId;
    }

    public String getDocumentClass() {
	return documentClass;
    }

    public void setDocumentClass(String documentClass) {
	this.documentClass = documentClass;
    }

    public String getMimeType() {
	return mimeType;
    }

    public void setMimeType(String mimeType) {
	this.mimeType = mimeType;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    public LocalDateTime getDate() {
	return date;
    }

    public void setDate(LocalDateTime date) {
	this.date = date;
    }

    @Override
    public String toString() {
	return "Attachment [id=" + id + ", name=" + name + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ( new Integer(idx).hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Attachment other = (Attachment) obj;
	if (idx == 0) {
	    if (other.idx != 0)
		return false;
	} else if (idx != other.idx)
	    return false;
	return true;
    }
}
