package com.novabase.omni.oney.modules.financialdocumentrepository.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class SearchCriteria {

    private String status;
    private String number;
    private String type;
    private Long purchaseOrderNumber;
    private String internalOrderCode;
    private String projectCode;
    private String departmentCode;
    private SearchScope scope;
    private Long minVATValue;
    private Long maxVATValue;
    private String supplierCode;
    private LocalDate creationDate;
    private LocalDate expirationDate;
    private LocalDate finishDate;
    private String orderField;
    private String orderType;

    public SearchCriteria() {

	super();

	this.purchaseOrderNumber = 0L;
	this.scope = new SearchScope();
	this.minVATValue = 0L;
	this.maxVATValue = 0L;

    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getNumber() {
	return number;
    }

    public void setNumber(String number) {
	this.number = number;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public Long getPurchaseOrderNumber() {
	return purchaseOrderNumber;
    }

    public void setPurchaseOrderNumber(Long purchaseOrderNumber) {
	this.purchaseOrderNumber = purchaseOrderNumber;
    }

    public String getInternalOrderCode() {
	return internalOrderCode;
    }

    public void setInternalOrderCode(String internalOrderCode) {
	this.internalOrderCode = internalOrderCode;
    }

    public String getProjectCode() {
	return projectCode;
    }

    public void setProjectCode(String projectCode) {
	this.projectCode = projectCode;
    }

    public String getDepartmentCode() {
	return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
	this.departmentCode = departmentCode;
    }

    public SearchScope getScope() {
	return scope;
    }

    public void setScope(SearchScope scope) {
	this.scope = scope;
    }

    public Long getMinVATValue() {
	return minVATValue;
    }

    public void setMinVATValue(Long minVATValue) {
	this.minVATValue = minVATValue;
    }

    public Long getMaxVATValue() {
	return maxVATValue;
    }

    public void setMaxVATValue(Long maxVATValue) {
	this.maxVATValue = maxVATValue;
    }

    public String getSupplierCode() {
	return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
	this.supplierCode = supplierCode;
    }

    public LocalDate getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
	this.creationDate = creationDate;
    }

    public LocalDate getExpirationDate() {
	return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
	this.expirationDate = expirationDate;
    }

    public LocalDate getFinishDate() {
	return finishDate;
    }

    public void setFinishDate(LocalDate finishDate) {
	this.finishDate = finishDate;
    }

    public String getOrderField() {
	return orderField;
    }

    public void setOrderField(String orderField) {
	this.orderField = orderField;
    }

    public String getOrderType() {
	return orderType;
    }

    public void setOrderType(String orderType) {
	this.orderType = orderType;
    }

}
