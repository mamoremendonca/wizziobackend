package com.novabase.omni.oney.modules.financialdocumentrepository.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import net.karneim.pojobuilder.GeneratePojoBuilder;

//@formatter:off
@Entity
@Table(name = "Item", 
	uniqueConstraints = { 
		@UniqueConstraint(columnNames = "id", name = "seq_item_id") 
	}
)
@GeneratePojoBuilder
//@formatter:on
public class Item {

    @Id
    @SequenceGenerator(name = "item_seq_task", sequenceName = "seq_item_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "item_seq_task")
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "idx")
    private int idx;

    @ManyToOne
    @JoinColumn(name = "financial_document_id")
    private FinancialDocument financialDocument;

    @Column(name = "purchase_order_item_id")
    private Long purchaseOrderItemId;

    @Column(name = "code")
    private String code;

    @Column(name = "description")
    private String description;

    @Column(name = "cost_center_code")
    private String costCenterCode;

    @Column(name = "retention_code")
    private String retentionCode;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "unit_price_without_tax")
    private Double unitPriceWithoutTax;

    @Column(name = "tax_code")
    private String taxCode;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    @Column(name = "comment")
    private String comment;

    public Item() {
	super();
    }

    public Item(Long id, int idx, FinancialDocument financialDocument, Long purchaseOrderItemId, String code, String description, String costCenterCode, String retentionCode, Double amount,
	    Double unitPriceWithoutTax, String taxCode, LocalDateTime startDate, LocalDateTime endDate, String comment) {
	super();
	this.id = id;
	this.idx = idx;
	this.financialDocument = financialDocument;
	this.purchaseOrderItemId = purchaseOrderItemId;
	this.code = code;
	this.description = description;
	this.costCenterCode = costCenterCode;
	this.retentionCode = retentionCode;
	this.amount = amount;
	this.unitPriceWithoutTax = unitPriceWithoutTax;
	this.taxCode = taxCode;
	this.startDate = startDate;
	this.endDate = endDate;
	this.comment = comment;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public int getIdx() {
	return idx;
    }

    public void setIdx(int idx) {
	this.idx = idx;
    }

    public FinancialDocument getFinancialDocument() {
	return financialDocument;
    }

    public void setFinancialDocument(FinancialDocument financialDocument) {
	this.financialDocument = financialDocument;
    }

    public Long getPurchaseOrderItemId() {
	return purchaseOrderItemId;
    }

    public void setPurchaseOrderItemId(Long purchaseOrderItemId) {
	this.purchaseOrderItemId = purchaseOrderItemId;
    }

    public String getCode() {
	return code;
    }

    public void setCode(String code) {
	this.code = code;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getCostCenterCode() {
	return costCenterCode;
    }

    public void setCostCenterCode(String costCenterCode) {
	this.costCenterCode = costCenterCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getUnitPriceWithoutTax() {
	return unitPriceWithoutTax;
    }

    public void setUnitPriceWithoutTax(Double unitPriceWithoutTax) {
	this.unitPriceWithoutTax = unitPriceWithoutTax;
    }

    public String getTaxCode() {
	return taxCode;
    }

    public void setTaxCode(String taxCode) {
	this.taxCode = taxCode;
    }

    public LocalDateTime getStartDate() {
	return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
	this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
	return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
	this.endDate = endDate;
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public String getRetentionCode() {
	return retentionCode;
    }

    public void setRetentionCode(String retentionCode) {
	this.retentionCode = retentionCode;
    }

    @Override
    public String toString() {
	return "Item [id=" + id + ", articleCode=" + code + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ( new Integer(idx).hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Item other = (Item) obj;
	if (idx == 0) {
	    if (other.idx != 0)
		return false;
	} else if (idx != other.idx)
	    return false;
	return true;
    }
}
