package com.novabase.omni.oney.modules.financialdocumentrepository.entity;

public class SearchScope {

    private String userGroup;			// Filter
    private String currentUserGroup;		// Logged user
    private String currentUserId;		// Logged user
    private String scopeType;
    
    public SearchScope() {
	super();
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public String getCurrentUserGroup() {
        return currentUserGroup;
    }

    public void setCurrentUserGroup(String currentUserGroup) {
        this.currentUserGroup = currentUserGroup;
    }

    public String getCurrentUserId() {
        return currentUserId;
    }

    public void setCurrentUserId(String currentUserId) {
        this.currentUserId = currentUserId;
    }

    public String getScopeType() {
        return scopeType;
    }

    public void setScopeType(String scopeType) {
        this.scopeType = scopeType;
    }
    
}
