package com.novabase.omni.oney.modules.financialdocumentrepository.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import net.karneim.pojobuilder.GeneratePojoBuilder;

//@formatter:off
@Entity
@Table(name = "financial_document", 
	uniqueConstraints = { 
		@UniqueConstraint(columnNames = "id", name = "financial_document_pk")
	}
)
@GeneratePojoBuilder
@NamedQueries({
	@NamedQuery(
		name = FinancialDocument.NAMED_QUERY_FIND_FINANCIAL_DOCUMENT_FETHING_ALL, 
		query = "Select financialDocument from FinancialDocument financialDocument "
			+ "left join fetch financialDocument.workflow workflow "
			+ "left join fetch workflow.steps "
			+ "left join fetch financialDocument.items "
			+ "left join fetch financialDocument.histories history "
			+ "left join fetch history.attachments "
			+ "left join fetch history.workflowStep "
			+ "where financialDocument.id = :financialDocumentId "),
	@NamedQuery(
		name = FinancialDocument.NAMED_QUERY_FIND_FINANCIAL_DOCUMENT_IDS_TO_SUBMIT,
		query = "Select distinct po.id from FinancialDocument po where po.id in "
			+ "(Select financialDocument.id "
			+ "from FinancialDocument financialDocument "
			+ "join financialDocument.histories history "
			+ "join history.attachments attachment "
			+ "where "
			+ "financialDocument.status = 'APPROVED' "
			+ "and attachment.externalId is NULL "
			+ ") or po.id in "
			+ "(Select financialDocument.id "
			+ "from FinancialDocument financialDocument "
			+ "where "
			+ "financialDocument.status = 'APPROVED' "
			+ "and financialDocument.type in :fdTypes) "),
	@NamedQuery(
		name = FinancialDocument.NAMED_QUERY_FIND_FINANCIAL_DOCUMENT_NOT_CANCELED_NO_REJECT_BY_NUMBER, 
		query = "Select financialDocument from FinancialDocument financialDocument "
			+ "left join fetch financialDocument.items "
			+" where (financialDocument.status != 'CANCELED' and financialDocument.status != 'REJECTED' ) "
			+ "and financialDocument.number = :financialDocumentNumber "),
	
	@NamedQuery(
		name = FinancialDocument.NAMED_QUERY_FIND_FINANCIAL_DOCUMENT_ID_BY_INSTANCE_ID, 
		query = "Select financialDocument.id from FinancialDocument financialDocument "
			+"where financialDocument.instanceId = :instanceId ")
	})
//@formatter:on
public class FinancialDocument {

    public static final String FINANCIAL_DOCUMENT_UNIQUE_NUMBER = "financial_document_unique_number";
    public static final String NAMED_QUERY_FIND_FINANCIAL_DOCUMENT_FETHING_ALL = "FinancialDocument.findHistoryByFinancialDocumentIdOrderById";
    public static final String NAMED_QUERY_FIND_FINANCIAL_DOCUMENT_IDS_TO_SUBMIT = "FinancialDocument.findFinancialDocumentIdsToSubmit";
    public static final String NAMED_QUERY_FIND_FINANCIAL_DOCUMENT_NOT_CANCELED_NO_REJECT_BY_NUMBER = "FinancialDocument.findFinancialDocumentNotCanceledNotRejectedByFinancialDocumentNumber";
    public static final String NAMED_QUERY_FIND_FINANCIAL_DOCUMENT_ID_BY_INSTANCE_ID = "FinancialDocument.findFinancialDocumentIdByInstanceId";

    @Id
    @SequenceGenerator(name = "financial_document_seq_task", sequenceName = "seq_financial_document_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "financial_document_seq_task")
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "instance_id")
    private Long instanceId; // Instance ID that origin this financial document

    @Column(name = "number", updatable = false, nullable = false)
    private String number;

    // @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private String status; // TODO: criar modulo partilhado entre apps e MS que contenha enuns e
			   // Exceptions, depois alterar esse atributo para enum

    // @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private String type; // TODO: criar modulo partilhado entre apps e MS que contenha enuns e
			 // Exceptions, depois alterar esse atributo para enum

    @Column(name = "associated_document_number")
    private String associatedDocument; // This is the Number of the associated FinancialDocument, used when the
				       // FinancialDocument is a CREDIT_NOTE

    @Column(name = "associated_document_id")
    private Long associatedDocumentId; // This is the ID of the associated FinancialDocument, used when the
				       // FinancialDocument is a CREDIT_NOTE

    @Column(name = "purchase_order_friendly_number")
    private String purchaseOrderFriendlyNumber; // This is the friendly Number of the associated PurchaseOrder, used when the
						// FinancialDocument is a INVOICE

    @Column(name = "purchase_order_id")
    private Long purchaseOrderId; // This is the ID of the associated PurchaseOrder, used when the
				  // FinancialDocument is a INVOICE

    @Column(name = "purchase_order_type")
    private String purchaseOrderType;

    @Column(name = "supplier_code")
    private String supplierCode;

    @Column(name = "description")
    private String description;

    @Column(name = "department_code")
    private String departmentCode;

    @Column(name = "project_code")
    private String projectCode;

    @Column(name = "internal_order_code")
    private String internalOrderCode;

    @Column(name = "granting")
    private boolean granting;

    @Column(name = "blob_document_id")
    private Long documentId;

    @Column(name = "document_external_id")
    private String documentExternalId;

    @Column(name = "document_mime_type")
    private String documentMimeType;

    @Column(name = "document_file_name")
    private String documentFileName;

    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @Column(name = "emission_date")
    private LocalDateTime emissionDate;

    @Column(name = "expiration_date")
    private LocalDateTime expirationDate;

    @Column(name = "finish_date")
    private LocalDateTime finishDate;

    @Column(name = "total_without_tax")
    private Double totalWithoutTax;

    @Column(name = "user_group")
    private String userGroup;

    @Column(name = "user_id")
    private Long userId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "workflow_id")
    private Workflow workflow;

    @OrderBy("idx")
    @OneToMany(mappedBy = "financialDocument", fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Item> items;

    @OrderBy("date")
    @OneToMany(mappedBy = "financialDocument", fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<History> histories;

    public FinancialDocument() {
	super();
    }

    public FinancialDocument(Long id, Long instanceId, String number, String status, String type, String associatedDocument, Long associatedDocumentId, String purchaseOrderFriendlyNumber,
	    Long purchaseOrderId, String purchaseOrderType, String supplierCode, String description, String departmentCode, String projectCode, String internalOrderCode, boolean granting,
	    Long documentId, String documentExternalId, String documentMimeType, String documentFileName, LocalDateTime creationDate, LocalDateTime emissionDate, LocalDateTime expirationDate,
	    LocalDateTime finishDate, Double totalWithoutTax, String userGroup, Long userId, Workflow workflow, Set<Item> items, Set<History> histories) {
	super();
	this.id = id;
	this.instanceId = instanceId;
	this.number = number;
	this.status = status;
	this.type = type;
	this.associatedDocument = associatedDocument;
	this.associatedDocumentId = associatedDocumentId;
	this.purchaseOrderFriendlyNumber = purchaseOrderFriendlyNumber;
	this.purchaseOrderId = purchaseOrderId;
	this.purchaseOrderType = purchaseOrderType;
	this.supplierCode = supplierCode;
	this.description = description;
	this.departmentCode = departmentCode;
	this.projectCode = projectCode;
	this.internalOrderCode = internalOrderCode;
	this.granting = granting;
	this.documentId = documentId;
	this.documentExternalId = documentExternalId;
	this.documentMimeType = documentMimeType;
	this.documentFileName = documentFileName;
	this.creationDate = creationDate;
	this.emissionDate = emissionDate;
	this.expirationDate = expirationDate;
	this.finishDate = finishDate;
	this.totalWithoutTax = totalWithoutTax;
	this.userGroup = userGroup;
	this.userId = userId;
	this.workflow = workflow;
	this.items = items;
	this.histories = histories;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Long getInstanceId() {
	return instanceId;
    }

    public void setInstanceId(Long instanceId) {
	this.instanceId = instanceId;
    }

    public String getNumber() {
	return number;
    }

    public void setNumber(String number) {
	this.number = number;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public String getAssociatedDocument() {
	return associatedDocument;
    }

    public void setAssociatedDocument(String associatedDocument) {
	this.associatedDocument = associatedDocument;
    }

    public Long getAssociatedDocumentId() {
	return associatedDocumentId;
    }

    public void setAssociatedDocumentId(Long associatedDocumentId) {
	this.associatedDocumentId = associatedDocumentId;
    }

    public String getPurchaseOrderFriendlyNumber() {
	return purchaseOrderFriendlyNumber;
    }

    public void setPurchaseOrderFriendlyNumber(String purchaseOrderFriendlyNumber) {
	this.purchaseOrderFriendlyNumber = purchaseOrderFriendlyNumber;
    }

    public Long getPurchaseOrderId() {
	return purchaseOrderId;
    }

    public void setPurchaseOrderId(Long purchaseOrderId) {
	this.purchaseOrderId = purchaseOrderId;
    }

    public String getPurchaseOrderType() {
	return purchaseOrderType;
    }

    public void setPurchaseOrderType(String purchaseOrderType) {
	this.purchaseOrderType = purchaseOrderType;
    }

    public String getSupplierCode() {
	return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
	this.supplierCode = supplierCode;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getDepartmentCode() {
	return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
	this.departmentCode = departmentCode;
    }

    public String getProjectCode() {
	return projectCode;
    }

    public void setProjectCode(String projectCode) {
	this.projectCode = projectCode;
    }

    public String getInternalOrderCode() {
	return internalOrderCode;
    }

    public void setInternalOrderCode(String internalOrderCode) {
	this.internalOrderCode = internalOrderCode;
    }

    public boolean isGranting() {
	return granting;
    }

    public void setGranting(boolean granting) {
	this.granting = granting;
    }

    public Long getDocumentId() {
	return documentId;
    }

    public void setDocumentId(Long documentId) {
	this.documentId = documentId;
    }

    public String getDocumentExternalId() {
	return documentExternalId;
    }

    public void setDocumentExternalId(String documentExternalId) {
	this.documentExternalId = documentExternalId;
    }

    public String getDocumentMimeType() {
	return documentMimeType;
    }

    public void setDocumentMimeType(String documentMimeType) {
	this.documentMimeType = documentMimeType;
    }

    public String getDocumentFileName() {
	return documentFileName;
    }

    public void setDocumentFileName(String documentFileName) {
	this.documentFileName = documentFileName;
    }

    public LocalDateTime getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
	this.creationDate = creationDate;
    }

    public LocalDateTime getEmissionDate() {
	return emissionDate;
    }

    public void setEmissionDate(LocalDateTime emissionDate) {
	this.emissionDate = emissionDate;
    }

    public LocalDateTime getExpirationDate() {
	return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
	this.expirationDate = expirationDate;
    }

    public LocalDateTime getFinishDate() {
	return finishDate;
    }

    public void setFinishDate(LocalDateTime finishDate) {
	this.finishDate = finishDate;
    }

    public Double getTotalWithoutTax() {
	return totalWithoutTax;
    }

    public void setTotalWithoutTax(Double totalWithoutTax) {
	this.totalWithoutTax = totalWithoutTax;
    }

    public String getUserGroup() {
	return userGroup;
    }

    public void setUserGroup(String userGroup) {
	this.userGroup = userGroup;
    }

    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    public Workflow getWorkflow() {
	return workflow;
    }

    public void setWorkflow(Workflow workflow) {
	this.workflow = workflow;
    }

    public Set<Item> getItems() {
	return items;
    }

    public void setItems(Set<Item> items) {
	this.items = items;
    }

    public Set<History> getHistories() {
	return histories;
    }

    public void setHistories(Set<History> histories) {
	this.histories = histories;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((type == null) ? 0 : type.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	FinancialDocument other = (FinancialDocument) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}
