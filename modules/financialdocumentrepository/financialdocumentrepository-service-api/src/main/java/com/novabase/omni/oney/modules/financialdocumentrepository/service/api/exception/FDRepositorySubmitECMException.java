package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.FinancialDocumentRepositoryProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FDRepositorySubmitECMException extends FDRepositoryException {

    private static final long serialVersionUID = -8296704273363781831L;

    public FDRepositorySubmitECMException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.INTERNAL_SERVER_ERROR, errorCode, ctx, args);
    }

    public static FDRepositorySubmitECMException of(Context ctx, Long id, Long attachmentId) {
	return new FDRepositorySubmitECMException(FinancialDocumentRepositoryProperties.SUBMIT_ECM_EXCEPTION, ctx, id, attachmentId);
    }

}
