package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums;

public enum PurchaseOrderTypeMs {
    CAPEX, OPEX, CONTRACT;
}
