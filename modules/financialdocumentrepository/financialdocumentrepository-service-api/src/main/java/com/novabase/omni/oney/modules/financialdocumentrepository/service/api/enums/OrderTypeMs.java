package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums;

public enum OrderTypeMs {
    ASC, DESC;
    
    @SuppressWarnings("unchecked")
    public static <T extends Enum<OrderTypeMs>> T getEnum(String name){

	Enum<OrderTypeMs> value = OrderTypeMs.ASC;
	try {
	    value = OrderTypeMs.valueOf(name);
	} catch (Throwable t) {
	    // DO nothing
	}
	return (T) value;
    }
}
