package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto;

import java.time.LocalDateTime;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.FinancialDocumentStatusMs;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.FinancialDocumentTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;


@ApiModel("FinancialDocumentListMsDTO")
public class FinancialDocumentListMsDTO extends DTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty
    private String number;

    @ApiModelProperty(readOnly = true)
    private FinancialDocumentStatusMs status;

    @ApiModelProperty
    private FinancialDocumentTypeMs type;
    
    @ApiModelProperty
    private String supplierCode;
    
    @ApiModelProperty
    private String purchaseOrderFriendlyNumber;

    @ApiModelProperty
    private Double totalWithoutTax;
    
    @ApiModelProperty
    private LocalDateTime creationDate;

    @ApiModelProperty
    private LocalDateTime expirationDate;

    @ApiModelProperty
    private String departmentCode;

    @ApiModelProperty
    private String projectCode;

    @ApiModelProperty
    private String internalOrderCode;

    @ApiModelProperty
    private String currentUserGroup;

    public FinancialDocumentListMsDTO() {
	super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public FinancialDocumentStatusMs getStatus() {
        return status;
    }

    public void setStatus(FinancialDocumentStatusMs status) {
        this.status = status;
    }

    public FinancialDocumentTypeMs getType() {
        return type;
    }

    public void setType(FinancialDocumentTypeMs type) {
        this.type = type;
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    public String getPurchaseOrderFriendlyNumber() {
        return purchaseOrderFriendlyNumber;
    }

    public void setPurchaseOrderFriendlyNumber(String purchaseOrderFriendlyNumber) {
        this.purchaseOrderFriendlyNumber = purchaseOrderFriendlyNumber;
    }

    public Double getTotalWithoutTax() {
        return totalWithoutTax;
    }

    public void setTotalWithoutTax(Double totalWithoutTax) {
        this.totalWithoutTax = totalWithoutTax;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getInternalOrderCode() {
        return internalOrderCode;
    }

    public void setInternalOrderCode(String internalOrderCode) {
        this.internalOrderCode = internalOrderCode;
    }

    public String getCurrentUserGroup() {
        return currentUserGroup;
    }

    public void setCurrentUserGroup(String currentUserGroup) {
        this.currentUserGroup = currentUserGroup;
    }

}
