package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.FinancialDocumentRepositoryProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FDRepositoryUniqueConstraintException extends FDRepositoryException {

    private static final long serialVersionUID = 5561319990599008866L;

    public FDRepositoryUniqueConstraintException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
    }

    public static FDRepositoryUniqueConstraintException of(Context ctx, Long id) {
	return new FDRepositoryUniqueConstraintException(FinancialDocumentRepositoryProperties.UNIQUE_CONSTRAINT_EXCEPTION_ID, ctx, id);
    }

}
