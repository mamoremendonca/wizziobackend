package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto;

import java.time.LocalDateTime;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.FinancialDocumentStatusMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("Workflow")
public class WorkflowMsDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty
    private LocalDateTime creationDate;

    @ApiModelProperty
    private String currentUserGroup;

    @ApiModelProperty
    private int currentIndex;

    @ApiModelProperty
    private LocalDateTime limitDateToNextStep;

    @ApiModelProperty
    private boolean hasNextStep;

    @ApiModelProperty
    private FinancialDocumentStatusMs financialDocumentStatus;

    @ApiModelProperty
    private List<WorkflowStepMsDTO> steps;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
	this.creationDate = creationDate;
    }

    public String getCurrentUserGroup() {
	return currentUserGroup;
    }

    public void setCurrentUserGroup(String currentUserGroup) {
	this.currentUserGroup = currentUserGroup;
    }

    public int getCurrentIndex() {
	return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
	this.currentIndex = currentIndex;
    }

    public LocalDateTime getLimitDateToNextStep() {
	return limitDateToNextStep;
    }

    public void setLimitDateToNextStep(LocalDateTime limitDateToNextStep) {
	this.limitDateToNextStep = limitDateToNextStep;
    }

    public boolean isHasNextStep() {
	return hasNextStep;
    }

    public void setHasNextStep(boolean hasNextStep) {
	this.hasNextStep = hasNextStep;
    }

    public FinancialDocumentStatusMs getFinancialDocumentStatus() {
	return financialDocumentStatus;
    }

    public void setFinancialDocumentStatus(FinancialDocumentStatusMs financialDocumentStatus) {
	this.financialDocumentStatus = financialDocumentStatus;
    }

    public List<WorkflowStepMsDTO> getSteps() {
	return steps;
    }

    public void setSteps(List<WorkflowStepMsDTO> steps) {
	this.steps = steps;
    }

}
