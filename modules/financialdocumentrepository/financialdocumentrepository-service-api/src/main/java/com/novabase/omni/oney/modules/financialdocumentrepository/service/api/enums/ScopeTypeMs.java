package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums;

public enum ScopeTypeMs {
    MINE, TEAM, ALL;
}
