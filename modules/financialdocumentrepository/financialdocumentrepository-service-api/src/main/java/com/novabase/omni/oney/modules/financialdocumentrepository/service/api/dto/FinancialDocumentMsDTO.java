package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto;

import java.time.LocalDateTime;
import java.util.List;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.FinancialDocumentStatusMs;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.FinancialDocumentTypeMs;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.PurchaseOrderTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("FinancialDocumentMsDTO")
public class FinancialDocumentMsDTO extends DTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty
    private Long instanceId;

    @ApiModelProperty
    private String number;

    @ApiModelProperty(readOnly = true)
    private FinancialDocumentStatusMs status;

    @ApiModelProperty
    private FinancialDocumentTypeMs type;

    @ApiModelProperty
    private PurchaseOrderTypeMs purchaseOrderType;

    @ApiModelProperty
    private String associatedDocument;

    @ApiModelProperty
    private Long associatedDocumentId;

    @ApiModelProperty
    private String purchaseOrderFriendlyNumber;

    @ApiModelProperty
    private Long purchaseOrderId;

    @ApiModelProperty
    private String supplierCode;

    @ApiModelProperty
    private String description;

    @ApiModelProperty
    private String departmentCode;

    @ApiModelProperty
    private String projectCode;

    @ApiModelProperty
    private String internalOrderCode;

    @ApiModelProperty
    private boolean granting;

    @ApiModelProperty
    private Long documentId;

    @ApiModelProperty
    private String documentExternalId;

    @ApiModelProperty
    private String documentMimeType;

    @ApiModelProperty
    private String documentFileName;

    @ApiModelProperty
    private LocalDateTime creationDate;

    @ApiModelProperty
    private LocalDateTime emissionDate;

    @ApiModelProperty
    private LocalDateTime expirationDate;

    @ApiModelProperty
    private LocalDateTime finishDate;

    @ApiModelProperty
    private Double totalWithoutTax;

    @ApiModelProperty
    private String userGroup;

    @ApiModelProperty
    private Long userId;

    @ApiModelProperty
    private boolean canClaim;

    @ApiModelProperty
    private List<ItemMsDTO> items;

    @ApiModelProperty
    private List<HistoryMsDTO> histories;

    @ApiModelProperty
    private WorkflowMsDTO workflow;

    public FinancialDocumentMsDTO() {
	super();
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Long getInstanceId() {
	return instanceId;
    }

    public void setInstanceId(Long instanceId) {
	this.instanceId = instanceId;
    }

    public String getNumber() {
	return number;
    }

    public void setNumber(String number) {
	this.number = number;
    }

    public FinancialDocumentStatusMs getStatus() {
	return status;
    }

    public void setStatus(FinancialDocumentStatusMs status) {
	this.status = status;
    }

    public FinancialDocumentTypeMs getType() {
	return type;
    }

    public void setType(FinancialDocumentTypeMs type) {
	this.type = type;
    }

    public PurchaseOrderTypeMs getPurchaseOrderType() {
	return purchaseOrderType;
    }

    public void setPurchaseOrderType(PurchaseOrderTypeMs purchaseOrderType) {
	this.purchaseOrderType = purchaseOrderType;
    }

    public String getAssociatedDocument() {
	return associatedDocument;
    }

    public void setAssociatedDocument(String associatedDocument) {
	this.associatedDocument = associatedDocument;
    }

    public Long getAssociatedDocumentId() {
	return associatedDocumentId;
    }

    public void setAssociatedDocumentId(Long associatedDocumentId) {
	this.associatedDocumentId = associatedDocumentId;
    }

    public String getPurchaseOrderFriendlyNumber() {
	return purchaseOrderFriendlyNumber;
    }

    public void setPurchaseOrderFriendlyNumber(String purchaseOrderFriendlyNumber) {
	this.purchaseOrderFriendlyNumber = purchaseOrderFriendlyNumber;
    }

    public Long getPurchaseOrderId() {
	return purchaseOrderId;
    }

    public void setPurchaseOrderId(Long purchaseOrderId) {
	this.purchaseOrderId = purchaseOrderId;
    }

    public String getSupplierCode() {
	return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
	this.supplierCode = supplierCode;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getDepartmentCode() {
	return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
	this.departmentCode = departmentCode;
    }

    public String getProjectCode() {
	return projectCode;
    }

    public void setProjectCode(String projectCode) {
	this.projectCode = projectCode;
    }

    public String getInternalOrderCode() {
	return internalOrderCode;
    }

    public void setInternalOrderCode(String internalOrderCode) {
	this.internalOrderCode = internalOrderCode;
    }

    public boolean isGranting() {
	return granting;
    }

    public void setGranting(boolean granting) {
	this.granting = granting;
    }

    public Long getDocumentId() {
	return documentId;
    }

    public void setDocumentId(Long documentId) {
	this.documentId = documentId;
    }

    public String getDocumentExternalId() {
	return documentExternalId;
    }

    public void setDocumentExternalId(String documentExternalId) {
	this.documentExternalId = documentExternalId;
    }

    public String getDocumentMimeType() {
	return documentMimeType;
    }

    public void setDocumentMimeType(String documentMimeType) {
	this.documentMimeType = documentMimeType;
    }

    public String getDocumentFileName() {
	return documentFileName;
    }

    public void setDocumentFileName(String documentFileName) {
	this.documentFileName = documentFileName;
    }

    public LocalDateTime getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
	this.creationDate = creationDate;
    }

    public LocalDateTime getEmissionDate() {
	return emissionDate;
    }

    public void setEmissionDate(LocalDateTime emissionDate) {
	this.emissionDate = emissionDate;
    }

    public LocalDateTime getExpirationDate() {
	return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
	this.expirationDate = expirationDate;
    }

    public LocalDateTime getFinishDate() {
	return finishDate;
    }

    public void setFinishDate(LocalDateTime finishDate) {
	this.finishDate = finishDate;
    }

    public Double getTotalWithoutTax() {
	return totalWithoutTax;
    }

    public void setTotalWithoutTax(Double totalWithoutTax) {
	this.totalWithoutTax = totalWithoutTax;
    }

    public String getUserGroup() {
	return userGroup;
    }

    public void setUserGroup(String userGroup) {
	this.userGroup = userGroup;
    }

    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    public boolean isCanClaim() {
	return canClaim;
    }

    public void setCanClaim(boolean canClaim) {
	this.canClaim = canClaim;
    }

    public List<ItemMsDTO> getItems() {
	return items;
    }

    public void setItems(List<ItemMsDTO> items) {
	this.items = items;
    }

    public List<HistoryMsDTO> getHistories() {
	return histories;
    }

    public void setHistories(List<HistoryMsDTO> histories) {
	this.histories = histories;
    }

    public WorkflowMsDTO getWorkflow() {
	return workflow;
    }

    public void setWorkflow(WorkflowMsDTO workflow) {
	this.workflow = workflow;
    }

}
