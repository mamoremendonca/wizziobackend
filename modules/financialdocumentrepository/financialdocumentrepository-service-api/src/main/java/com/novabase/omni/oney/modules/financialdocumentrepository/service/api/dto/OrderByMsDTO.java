package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto;

import javax.xml.bind.annotation.XmlRootElement;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.OrderTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@XmlRootElement(name = "OrderBy")
@GeneratePojoBuilder
@ApiModel("OrderBy")
public class OrderByMsDTO extends DTO {

    @ApiModelProperty
    private String field;

    @ApiModelProperty
    private OrderTypeMs type;

    public String getField() {
	return field;
    }

    public void setField(String field) {
	this.field = field;
    }

    public OrderTypeMs getType() {
	return type;
    }

    public void setType(OrderTypeMs type) {
	this.type = type;
    }

}
