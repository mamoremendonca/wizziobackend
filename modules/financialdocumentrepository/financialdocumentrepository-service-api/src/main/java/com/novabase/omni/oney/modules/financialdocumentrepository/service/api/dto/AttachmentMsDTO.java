package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlRootElement;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.DocumentClassTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("Attachment")
public class AttachmentMsDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty
    private int index;

    @ApiModelProperty
    private Long blobId;

    @ApiModelProperty
    private String externalId;

    @ApiModelProperty
    private DocumentClassTypeMs documentClass;

    @ApiModelProperty
    private String mimeType;

    @ApiModelProperty
    private String name;

    @ApiModelProperty
    private Long userId;

    @ApiModelProperty
    private LocalDateTime date;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public int getIndex() {
	return index;
    }

    public void setIndex(int index) {
	this.index = index;
    }

    public Long getBlobId() {
	return blobId;
    }

    public void setBlobId(Long blobId) {
	this.blobId = blobId;
    }

    public String getExternalId() {
	return externalId;
    }

    public void setExternalId(String externalId) {
	this.externalId = externalId;
    }

    public DocumentClassTypeMs getDocumentClass() {
	return documentClass;
    }

    public void setDocumentClass(DocumentClassTypeMs documentClass) {
	this.documentClass = documentClass;
    }

    public String getMimeType() {
	return mimeType;
    }

    public void setMimeType(String mimeType) {
	this.mimeType = mimeType;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    public LocalDateTime getDate() {
	return date;
    }

    public void setDate(LocalDateTime date) {
	this.date = date;
    }

}
