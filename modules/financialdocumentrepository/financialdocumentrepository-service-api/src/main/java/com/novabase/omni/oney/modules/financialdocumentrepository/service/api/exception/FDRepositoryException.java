package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;
import io.digitaljourney.platform.modules.ws.rs.api.exception.RSException;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.FinancialDocumentRepositoryProperties;

public class FDRepositoryException extends RSException {

    private static final long serialVersionUID = -7195203846055875397L;

    protected FDRepositoryException(PlatformCode statusCode, String errorCode, Context ctx, Object... args) {
	super(statusCode, errorCode, ctx, args);
    }

    protected FDRepositoryException(Context ctx, Object... args) {
	this(PlatformCode.INTERNAL_SERVER_ERROR, FinancialDocumentRepositoryProperties.UNEXPECTED_EXCEPTION, ctx, args);
    }

    protected FDRepositoryException(String errorCode, Context ctx, Object... args) {
	this(PlatformCode.BUSINESS_ERROR, errorCode, ctx, args);
    }

    public static FDRepositoryException of(Context ctx, String message) {
	return new FDRepositoryException(ctx, message);
    }

    public static FDRepositoryException of(Context ctx, Throwable cause) {
	return new FDRepositoryException(ctx, cause);
    }
}
