package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums;

import java.util.Arrays;

import com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.enums.DocumentClassTypeMsEnum;

public enum DocumentClassTypeMs {
    
    //@formatter:off
    FINANCIAL_DOCUMENT(DocumentClassTypeMsEnum.FINANCIAL_DOCUMENT), 
    FINANCIAL_DOCUMENT_ATTACHMENT(DocumentClassTypeMsEnum.FINANCIAL_DOCUMENT_ATTACHMENT); 
    //@formatter:on
    
    private DocumentClassTypeMsEnum ecmDocumentClass;

    private DocumentClassTypeMs(DocumentClassTypeMsEnum ecmDocumentClass) {
	this.ecmDocumentClass = ecmDocumentClass;
    }

    public DocumentClassTypeMsEnum getEcmDocumentClass() {
	return ecmDocumentClass;
    }
    
    //@formatter:off
    public static DocumentClassTypeMs valueOf(DocumentClassTypeMsEnum ecmDocumentClassMsType){
	return Arrays.asList(DocumentClassTypeMs.values())
		.stream()
		.filter(type -> type.getEcmDocumentClass().equals(ecmDocumentClassMsType))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on

}
