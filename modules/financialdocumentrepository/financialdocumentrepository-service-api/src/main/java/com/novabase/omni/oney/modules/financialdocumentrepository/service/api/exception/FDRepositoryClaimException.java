package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.FinancialDocumentRepositoryProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FDRepositoryClaimException extends FDRepositoryException {

    private static final long serialVersionUID = 4432732152267431358L;

    public FDRepositoryClaimException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
    }

    public static FDRepositoryClaimException of(Context ctx, Long id) {
	return new FDRepositoryClaimException(FinancialDocumentRepositoryProperties.CLAIM_EXCEPTION, ctx, id);
    }

}
