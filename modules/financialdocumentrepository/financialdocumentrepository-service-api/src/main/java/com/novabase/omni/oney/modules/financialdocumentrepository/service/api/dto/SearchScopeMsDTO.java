package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.ScopeTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("SearchScope")
public class SearchScopeMsDTO extends DTO {

    @ApiModelProperty
    private String userGroup;

    @ApiModelProperty
    private String currentUserGroup; // Logged user
    @ApiModelProperty
    private String currentUserId; // Logged user

    @ApiModelProperty
    private ScopeTypeMs scopeType;

    public String getUserGroup() {
	return userGroup;
    }

    public void setUserGroup(String userGroup) {
	this.userGroup = userGroup;
    }

    public String getCurrentUserGroup() {
	return currentUserGroup;
    }

    public void setCurrentUserGroup(String currentUserGroup) {
	this.currentUserGroup = currentUserGroup;
    }

    public String getCurrentUserId() {
	return currentUserId;
    }

    public void setCurrentUserId(String currentUserId) {
	this.currentUserId = currentUserId;
    }

    public ScopeTypeMs getScopeType() {
	return scopeType;
    }

    public void setScopeType(ScopeTypeMs scopeType) {
	this.scopeType = scopeType;
    }

}
