package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums;

public enum FinancialDocumentStatusMs {

    IN_APPROVAL, APPROVED, SAP_SUBMITTED, PAID, REJECTED, CANCELED;
    
}
