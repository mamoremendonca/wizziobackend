package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.FinancialDocumentRepositoryProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FDRepositoryGetSupplierInfoException extends FDRepositoryException {

    private static final long serialVersionUID = 3498399623781703137L;

    public FDRepositoryGetSupplierInfoException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.INTERNAL_SERVER_ERROR, errorCode, ctx, args);
    }

    public static FDRepositoryGetSupplierInfoException of(Context ctx, Long id, String supplierCode) {
	return new FDRepositoryGetSupplierInfoException(FinancialDocumentRepositoryProperties.GET_SUPPLIER_INFO_EXCEPTION, ctx, id, supplierCode);
    }

}
