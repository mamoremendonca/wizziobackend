package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums;

public enum TimeLineStepTypeMs {
    DRAFT, SUBMITTED, TO_APPROVE, APPROVED, SKIPPED, RETURNED, REJECTED, CANCELED, SAP_SUBMITTED, PAID;
}
