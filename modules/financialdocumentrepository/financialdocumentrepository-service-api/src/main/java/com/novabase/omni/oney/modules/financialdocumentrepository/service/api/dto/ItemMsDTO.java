package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("Item")
public class ItemMsDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty
    private Long purchaseOrderItemId;

    @ApiModelProperty
    private int index;

    @ApiModelProperty
    private String code;

    @ApiModelProperty
    private String description;

    @ApiModelProperty
    private String costCenterCode;

    @ApiModelProperty
    private String retentionCode;

    @ApiModelProperty
    private Double amount;

    @ApiModelProperty
    private Double unitPriceWithoutTax;

    @ApiModelProperty
    private String taxCode;

    @ApiModelProperty
    private LocalDateTime startDate;

    @ApiModelProperty
    private LocalDateTime endDate;

    @ApiModelProperty
    private String comment;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Long getPurchaseOrderItemId() {
	return purchaseOrderItemId;
    }

    public void setPurchaseOrderItemId(Long purchaseOrderItemId) {
	this.purchaseOrderItemId = purchaseOrderItemId;
    }

    public int getIndex() {
	return index;
    }

    public void setIndex(int index) {
	this.index = index;
    }

    public String getCode() {
	return code;
    }

    public void setCode(String code) {
	this.code = code;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getCostCenterCode() {
	return costCenterCode;
    }

    public void setCostCenterCode(String costCenterCode) {
	this.costCenterCode = costCenterCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getUnitPriceWithoutTax() {
	return unitPriceWithoutTax;
    }

    public void setUnitPriceWithoutTax(Double unitPriceWithoutTax) {
	this.unitPriceWithoutTax = unitPriceWithoutTax;
    }

    public String getTaxCode() {
	return taxCode;
    }

    public void setTaxCode(String taxCode) {
	this.taxCode = taxCode;
    }

    public LocalDateTime getStartDate() {
	return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
	this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
	return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
	this.endDate = endDate;
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public String getRetentionCode() {
	return retentionCode;
    }

    public void setRetentionCode(String retentionCode) {
	this.retentionCode = retentionCode;
    }

}
