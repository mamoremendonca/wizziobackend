package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto;

import java.time.LocalDateTime;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.WorkFlowActionTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("WorkFlowAction")
public class WorkflowActionMsDTO {

    @ApiModelProperty
    private WorkFlowActionTypeMs type;

    @ApiModelProperty(readOnly = true)
    private LocalDateTime date;

    private String userGroup;

    private Long userId;

    private String comment;

    private List<AttachmentMsDTO> attachments;

    public WorkFlowActionTypeMs getType() {
	return type;
    }

    public void setType(WorkFlowActionTypeMs type) {
	this.type = type;
    }

    public LocalDateTime getDate() {
	return date;
    }

    public void setDate(LocalDateTime date) {
	this.date = date;
    }

    public String getUserGroup() {
	return userGroup;
    }

    public void setUserGroup(String userGroup) {
	this.userGroup = userGroup;
    }

    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public List<AttachmentMsDTO> getAttachments() {
	return attachments;
    }

    public void setAttachments(List<AttachmentMsDTO> attachments) {
	this.attachments = attachments;
    }

}
