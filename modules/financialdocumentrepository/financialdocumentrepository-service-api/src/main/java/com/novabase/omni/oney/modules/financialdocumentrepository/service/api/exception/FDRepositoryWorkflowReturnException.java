package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.FinancialDocumentRepositoryProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FDRepositoryWorkflowReturnException extends FDRepositoryException {

    private static final long serialVersionUID = 5593617278597990897L;

    public FDRepositoryWorkflowReturnException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
    }

    public static FDRepositoryWorkflowReturnException of(Context ctx, Long id) {
	return new FDRepositoryWorkflowReturnException(FinancialDocumentRepositoryProperties.FIRST_APPROVER_CANNOT_RETURN_EXCEPTION, ctx, id);
    }

}
