package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto;

import java.time.LocalDateTime;
import java.util.Set;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.TimeLineStepTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("History")
public class HistoryMsDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty
    private LocalDateTime date;

    @ApiModelProperty
    private TimeLineStepTypeMs type;

    @ApiModelProperty
    private Long userId;

    @ApiModelProperty
    private String comment;

    @ApiModelProperty
    private String userGroup;

    @ApiModelProperty
    private Set<AttachmentMsDTO> attachments;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getDate() {
	return date;
    }

    public void setDate(LocalDateTime date) {
	this.date = date;
    }

    public TimeLineStepTypeMs getType() {
	return type;
    }

    public void setType(TimeLineStepTypeMs type) {
	this.type = type;
    }

    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public String getUserGroup() {
	return userGroup;
    }

    public void setUserGroup(String userGroup) {
	this.userGroup = userGroup;
    }

    public Set<AttachmentMsDTO> getAttachments() {
	return attachments;
    }

    public void setAttachments(Set<AttachmentMsDTO> attachments) {
	this.attachments = attachments;
    }

}
