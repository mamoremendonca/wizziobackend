package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.FinancialDocumentRepositoryProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FDRepositoryGroupCannotClaimException extends FDRepositoryException {

    private static final long serialVersionUID = -1487344621117540960L;

    public FDRepositoryGroupCannotClaimException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
    }

    public static FDRepositoryGroupCannotClaimException of(Context ctx, Long id, String userGroup) {
	return new FDRepositoryGroupCannotClaimException(FinancialDocumentRepositoryProperties.GROUP_CANNOT_CLAIM_EXCEPTION, ctx, id, userGroup);
    }

}
