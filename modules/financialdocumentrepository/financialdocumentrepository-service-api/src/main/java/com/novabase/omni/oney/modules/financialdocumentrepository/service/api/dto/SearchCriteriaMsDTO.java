package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.FinancialDocumentStatusMs;
import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.FinancialDocumentTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("SearchCriteria")
public class SearchCriteriaMsDTO extends DTO {

    @ApiModelProperty
    private FinancialDocumentStatusMs status;

    @ApiModelProperty
    private String number;

    @ApiModelProperty
    private FinancialDocumentTypeMs type;
    
    @ApiModelProperty
    private Long purchaseOrderNumber;

    @ApiModelProperty
    private String internalOrderCode;

    @ApiModelProperty
    private String projectCode;

    @ApiModelProperty
    private String departmentCode;

    @ApiModelProperty
    private SearchScopeMsDTO scope;

    @ApiModelProperty
    private Long minVATValue;

    @ApiModelProperty
    private Long maxVATValue;

    @ApiModelProperty
    private String supplierCode;

    @ApiModelProperty
    private LocalDate creationDate;

    @ApiModelProperty
    private LocalDate expirationDate;

    @ApiModelProperty
    private LocalDate finishDate;

    @ApiModelProperty
    private String orderField;

    @ApiModelProperty
    private OrderByMsDTO orderBy;

    public SearchCriteriaMsDTO() {
	
	super();
	
	this.purchaseOrderNumber = 0L;
	this.scope = new SearchScopeMsDTO();
	this.minVATValue = 0L;
	this.maxVATValue = 0L;
	this.orderBy = new OrderByMsDTO();
	
    }

    public FinancialDocumentStatusMs getStatus() {
	return status;
    }

    public void setStatus(FinancialDocumentStatusMs status) {
	this.status = status;
    }

    public String getNumber() {
	return number;
    }

    public void setNumber(String number) {
	this.number = number;
    }

    public FinancialDocumentTypeMs getType() {
	return type;
    }

    public void setType(FinancialDocumentTypeMs type) {
	this.type = type;
    }

    public Long getPurchaseOrderNumber() {
	return purchaseOrderNumber;
    }

    public void setPurchaseOrderNumber(Long purchaseOrderNumber) {
	this.purchaseOrderNumber = purchaseOrderNumber;
    }

    public String getInternalOrderCode() {
	return internalOrderCode;
    }

    public void setInternalOrderCode(String internalOrderCode) {
	this.internalOrderCode = internalOrderCode;
    }

    public String getProjectCode() {
	return projectCode;
    }

    public void setProjectCode(String projectCode) {
	this.projectCode = projectCode;
    }

    public String getDepartmentCode() {
	return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
	this.departmentCode = departmentCode;
    }

    public SearchScopeMsDTO getScope() {
	return scope;
    }

    public void setScope(SearchScopeMsDTO scope) {
	this.scope = scope;
    }

    public Long getMinVATValue() {
	return minVATValue;
    }

    public void setMinVATValue(Long minVATValue) {
	this.minVATValue = minVATValue;
    }

    public Long getMaxVATValue() {
	return maxVATValue;
    }

    public void setMaxVATValue(Long maxVATValue) {
	this.maxVATValue = maxVATValue;
    }

    public String getSupplierCode() {
	return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
	this.supplierCode = supplierCode;
    }

    public LocalDate getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
	this.creationDate = creationDate;
    }

    public LocalDate getExpirationDate() {
	return expirationDate;
    }

    public void setExpirationDate(LocalDate expirationDate) {
	this.expirationDate = expirationDate;
    }

    public LocalDate getFinishDate() {
	return finishDate;
    }

    public void setFinishDate(LocalDate finishDate) {
	this.finishDate = finishDate;
    }

    public String getOrderField() {
	return orderField;
    }

    public void setOrderField(String orderField) {
	this.orderField = orderField;
    }

    public OrderByMsDTO getOrderBy() {
	return orderBy;
    }

    public void setOrderBy(OrderByMsDTO orderBy) {
	this.orderBy = orderBy;
    }

}
