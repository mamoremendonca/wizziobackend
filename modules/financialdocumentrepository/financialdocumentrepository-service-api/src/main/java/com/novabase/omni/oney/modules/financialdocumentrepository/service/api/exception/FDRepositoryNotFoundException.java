package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.exception;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.FinancialDocumentRepositoryProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class FDRepositoryNotFoundException extends FDRepositoryException {

    private static final long serialVersionUID = -1870305288313369884L;

    public FDRepositoryNotFoundException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
    }

    public static FDRepositoryNotFoundException of(Context ctx, Class<?> entity, Long id) {
	return new FDRepositoryNotFoundException(FinancialDocumentRepositoryProperties.NOT_FOUND_EXCEPTION, ctx, entity.getSimpleName(), id);
    }
    
    public static FDRepositoryNotFoundException of(final Context ctx, final Class<?> entity, final String number) {
	return new FDRepositoryNotFoundException(FinancialDocumentRepositoryProperties.NOT_FOUND_EXCEPTION, ctx, entity.getSimpleName(), number);
    }

}
