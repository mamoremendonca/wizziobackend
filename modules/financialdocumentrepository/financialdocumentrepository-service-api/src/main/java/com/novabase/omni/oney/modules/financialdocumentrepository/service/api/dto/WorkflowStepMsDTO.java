package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("WorkflowStep")
public class WorkflowStepMsDTO {

    @ApiModelProperty
    private int index;

    @ApiModelProperty
    private String userGroup;

    @ApiModelProperty
    private boolean hasNextStep;

    @ApiModelProperty
    private boolean active;

    public int getIndex() {
	return index;
    }

    public void setIndex(int index) {
	this.index = index;
    }

    public String getUserGroup() {
	return userGroup;
    }

    public void setUserGroup(String userGroup) {
	this.userGroup = userGroup;
    }

    public boolean isHasNextStep() {
	return hasNextStep;
    }

    public void setHasNextStep(boolean hasNextStep) {
	this.hasNextStep = hasNextStep;
    }

    public boolean isActive() {
	return active;
    }

    public void setActive(boolean active) {
	this.active = active;
    }

}
