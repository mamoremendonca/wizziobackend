package com.novabase.omni.oney.modules.financialdocumentrepository.service.api.dto;

import java.time.LocalDateTime;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.novabase.omni.oney.modules.financialdocumentrepository.service.api.enums.TimeLineStepTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("TimeLineStep")
public class TimeLineStepMsDTO {

    @ApiModelProperty(readOnly = true)
    private int index;

    @ApiModelProperty(readOnly = true)
    private TimeLineStepTypeMs type;

    @ApiModelProperty(readOnly = true)
    private LocalDateTime date;

    @ApiModelProperty(readOnly = true)
    private String comment;

    @ApiModelProperty(readOnly = true)
    private Long userId;

    @ApiModelProperty(readOnly = true)
    private String userGroup;

    @ApiModelProperty(readOnly = true)
    private boolean hasNext;

    @ApiModelProperty(readOnly = true)
    private List<AttachmentMsDTO> attachments;

    public int getIndex() {
	return index;
    }

    public void setIndex(int index) {
	this.index = index;
    }

    public TimeLineStepTypeMs getType() {
	return type;
    }

    public void setType(TimeLineStepTypeMs type) {
	this.type = type;
    }

    public LocalDateTime getDate() {
	return date;
    }

    public void setDate(LocalDateTime date) {
	this.date = date;
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    public String getUserGroup() {
	return userGroup;
    }

    public void setUserGroup(String userGroup) {
	this.userGroup = userGroup;
    }

    public boolean isHasNext() {
	return hasNext;
    }

    public void setHasNext(boolean hasNext) {
	this.hasNext = hasNext;
    }

    public List<AttachmentMsDTO> getAttachments() {
	return attachments;
    }

    public void setAttachments(List<AttachmentMsDTO> attachments) {
	this.attachments = attachments;
    }

}
