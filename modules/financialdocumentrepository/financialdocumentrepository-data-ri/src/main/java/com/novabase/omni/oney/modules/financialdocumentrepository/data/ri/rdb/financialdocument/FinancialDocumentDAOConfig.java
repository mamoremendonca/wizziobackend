package com.novabase.omni.oney.modules.financialdocumentrepository.data.ri.rdb.financialdocument;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/financialdocument",
    icon = @Icon(resource = "OSGI-INF/icon/rdb.png", size = 32))
public @interface FinancialDocumentDAOConfig {

  public static final String CPID = "oney.base.modules.financialdocumentrepository.data.ri.rdb.financialdocument";

  @AttributeDefinition(name = "%provider_target.name", description = "%provider_target.description", required = true)
  String provider_target();
  
  @AttributeDefinition(name = "%maxRecords.name", description = "%maxRecords.description", required = true)
  int maxRecords();
  
}
