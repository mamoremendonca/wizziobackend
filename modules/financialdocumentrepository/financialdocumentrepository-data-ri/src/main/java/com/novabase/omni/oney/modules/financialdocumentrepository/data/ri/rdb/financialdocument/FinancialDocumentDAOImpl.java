package com.novabase.omni.oney.modules.financialdocumentrepository.data.ri.rdb.financialdocument;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.transaction.control.TransactionControl;
import org.osgi.service.transaction.control.jpa.JPAEntityManagerProvider;

import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.FinancialDocumentDAO;
import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.exception.NotFoundException;
import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.exception.UnexpectedPersistenceException;
import com.novabase.omni.oney.modules.financialdocumentrepository.data.api.exception.UniqueConstraintViolationException;
import com.novabase.omni.oney.modules.financialdocumentrepository.data.ri.RDBContext;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.Attachment;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.FinancialDocument;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.History;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.SearchCriteria;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.Workflow;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.WorkflowStep;

import io.digitaljourney.platform.modules.rdb.api.exception.RDBClientException;
import io.digitaljourney.platform.modules.rdb.jpa.api.JPAProperties;
import io.digitaljourney.platform.modules.rdb.jpa.api.dao.AbstractJPADAO;

// @formatter:off
@Component(
	configurationPid = FinancialDocumentDAOConfig.CPID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	reference = {
		@Reference(
			name = JPAProperties.REF_CONTEXT,
			service = RDBContext.class, 
			cardinality = ReferenceCardinality.MANDATORY),
		@Reference(
			name = JPAProperties.REF_TX_CONTROL,
			service = TransactionControl.class,
			cardinality = ReferenceCardinality.MANDATORY),
		@Reference(
			name = JPAProperties.REF_PROVIDER,
			service = JPAEntityManagerProvider.class,
			cardinality = ReferenceCardinality.MANDATORY)
		})
@Designate(ocd = FinancialDocumentDAOConfig.class)
// @formatter:on
public final class FinancialDocumentDAOImpl extends AbstractJPADAO<FinancialDocumentDAOConfig> implements FinancialDocumentDAO {
    @Activate
    public void activate(ComponentContext ctx, FinancialDocumentDAOConfig config) {
	prepare(ctx, config);
    }

    @Modified
    public void modified(FinancialDocumentDAOConfig config) {
	prepare(config);
    }

    @Override
    public FinancialDocument getFinancialDocument(Long id) throws NotFoundException, UnexpectedPersistenceException {
	try {
	    
	    return txSupports(em -> {
		
		TypedQuery<FinancialDocument> namedQuery = em.createNamedQuery(FinancialDocument.NAMED_QUERY_FIND_FINANCIAL_DOCUMENT_FETHING_ALL, FinancialDocument.class);
		namedQuery.setParameter("financialDocumentId", id);
		
		return namedQuery.getSingleResult();
		
	    });
	    
	} catch (Exception e) {

	    if (e.getCause().getClass() == NoResultException.class) {
		throw new NotFoundException(FinancialDocument.class);
	    } else {
		throw new UnexpectedPersistenceException(e.getMessage());
	    }
	    
	}
	
    }

    @Override
    public synchronized FinancialDocument addFinancialDocument(FinancialDocument financialDocument) throws UniqueConstraintViolationException, UnexpectedPersistenceException {
	try {

	    // We have to set the first and second history reference to the first and second step on the workflow
	    //@formatter:off
	    
	    WorkflowStep firstStep = 
		    financialDocument.getWorkflow().getSteps()
		    	.stream()
		    	.filter(
		    		step -> step.getIdx() == 1)
		    	.findFirst()
		    	.orElse(null);
	    
	    WorkflowStep secondStep = 
		    financialDocument.getWorkflow().getSteps()
		    	.stream()
		    	.filter(
		    		step -> step.getIdx() == 2)
		    	.findFirst()
		    	.orElse(null);
	    
	    History firstHistory = 
		    financialDocument.getHistories()
		    	.stream()
		    	.filter(h -> h.getUserGroup().equals(firstStep.getUserGroup()))
		    	.findFirst()
		    	.orElse(null);
	    
	    History secondHistory = 
		    financialDocument.getHistories()
		    	.stream()
		    	.filter(h -> h.getUserGroup().equals(secondStep.getUserGroup()))
		    	.findFirst()
		    	.orElse(null);
	    
	    if (firstHistory != null) {
		
		firstHistory.setWorkflowStep(
			financialDocument.getWorkflow().getWorkflowStepByIndex(1));
	    }
	    
	    if (secondHistory != null) {
		
		secondHistory.setWorkflowStep(
			financialDocument.getWorkflow().getWorkflowStepByIndex(2));
	    }
	    //@formatter:on

	    setFinancialDocumentMappedReferences(financialDocument);
	    return save(financialDocument);

	} catch (EntityExistsException e) {

	    throw new UniqueConstraintViolationException(e.getMessage());
	} catch (Exception e) {

	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public Long getFinancialDocumentIdByInstanceId(Long instanceId) throws UnexpectedPersistenceException {
	try {
	    return txSupports(em -> {
		TypedQuery<Long> namedQuery = em.createNamedQuery(FinancialDocument.NAMED_QUERY_FIND_FINANCIAL_DOCUMENT_ID_BY_INSTANCE_ID, Long.class);
		namedQuery.setParameter("instanceId", instanceId);
		return namedQuery.getSingleResult();
	    });
	} catch (NoResultException | RDBClientException e) {
	    return null;
	}
	catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public void deleteFinancialDocumentById(Long id) throws UnexpectedPersistenceException, NotFoundException {

	FinancialDocument financialDocument = getFinancialDocument(id);
	try {
//	    delete(financialDocument);
	    txRequired(em -> {
		em.remove(em.getReference(FinancialDocument.class, financialDocument.getId()));
	    });
	    
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}

    }

    @Override
    public History addHistory(History history) throws UnexpectedPersistenceException {

	setAttachmentMappedReferences(history);

	try {
	    return save(history);
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public List<History> addHistories(List<History> histories) throws UnexpectedPersistenceException {

	try {
	    txRequired(em -> {
		histories.forEach(history -> {
		    setAttachmentMappedReferences(history);
		    em.persist(history);
		    em.flush();
		});
	    });

	    return histories;
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public List<History> searchFinancialDocumentHistory(Long financialDocumentId) throws UnexpectedPersistenceException {
	try {
	    return txSupports(em -> {
		TypedQuery<History> namedQuery = em.createNamedQuery(History.NAMED_QUERY_FIND_HISTORY, History.class);
		namedQuery.setParameter("financialDocumentId", financialDocumentId);
		return namedQuery.getResultList();
	    });
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public Workflow searchFinancialDocumentWorkFlow(Long financialDocumentId) throws UnexpectedPersistenceException {
	try {
	    return txSupports(em -> {
		TypedQuery<Workflow> namedQuery = em.createNamedQuery(Workflow.NAMED_QUERY_FIND_WORKFLOW_BY_FINANCIAL_DOCUMENT_ID, Workflow.class);
		namedQuery.setParameter("financialDocumentId", financialDocumentId);
		return namedQuery.getSingleResult();
	    });
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public Workflow updateWorkFlow(Workflow workflow, Long financialDocumentId, String financialDocumentStatus, LocalDateTime finishDate) throws UnexpectedPersistenceException {

	try {
	    return txRequired(em -> {
		Workflow mergedEntity = em.merge(workflow);

		if (financialDocumentStatus != null && financialDocumentStatus.trim().isEmpty() == false) { // Only update in case of the financialDocumentStatus is different of null and
													    // is
		    // not empty
		    CriteriaBuilder cb = em.getCriteriaBuilder();

		    CriteriaUpdate<FinancialDocument> updateFinancialDocumentStatusCriteria = cb.createCriteriaUpdate(FinancialDocument.class);

		    Root<FinancialDocument> root = updateFinancialDocumentStatusCriteria.from(FinancialDocument.class);

		    updateFinancialDocumentStatusCriteria.set("status", financialDocumentStatus);
		    updateFinancialDocumentStatusCriteria.set("finishDate", finishDate); 

		    updateFinancialDocumentStatusCriteria.where(cb.equal(root.get("id"), financialDocumentId));

		    em.createQuery(updateFinancialDocumentStatusCriteria).executeUpdate();
		}

		em.flush();

		return mergedEntity;
	    });
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}

    }

    public List<FinancialDocument> searchFinancialDocuments(final SearchCriteria search) throws UnexpectedPersistenceException {
	
	try {
	    
	    // TODO: refazer com a logica de paginação
	    return this.txSupports(em -> { 
		return em.createQuery(FinancialDocumentQueryBuilder.buildQueryFromSearchCriteria(search), FinancialDocument.class).getResultList(); 
	    });
	    
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public Attachment getFinancialDocumentAttachment(Long financialDocumentId, int index) throws NotFoundException, UnexpectedPersistenceException {
	try {
	    return txSupports(em -> {
		TypedQuery<Attachment> namedQuery = em.createNamedQuery(Attachment.NAMED_QUERY_FIND_ATTACHMENT_FETCHING_ONE, Attachment.class);
		namedQuery.setParameter("financialDocumentId", financialDocumentId);
		namedQuery.setParameter("idx", index);
		return namedQuery.getSingleResult();
	    });
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public List<Attachment> getFinancialDocumentAttachments(Long financialDocumentId) throws NotFoundException, UnexpectedPersistenceException {
	try {
	    return txSupports(em -> {
		TypedQuery<Attachment> namedQuery = em.createNamedQuery(Attachment.NAMED_QUERY_FIND_ATTACHMENT_FETCHING_ALL, Attachment.class);
		namedQuery.setParameter("financialDocumentId", financialDocumentId);
		return namedQuery.getResultList();
	    });
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public Attachment updateAttachment(Attachment attachment) throws UnexpectedPersistenceException {
	try {
	    return txRequired(em -> {
		Attachment mergedEntity = em.merge(attachment);
		em.flush();
		return mergedEntity;
	    });
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public List<Long> findProcessToSubmit(List<String> financialDocumentTypes) throws UnexpectedPersistenceException {
	
	try {
	    
	    return txSupports(em -> {
		
		TypedQuery<Long> namedQuery = em.createNamedQuery(FinancialDocument.NAMED_QUERY_FIND_FINANCIAL_DOCUMENT_IDS_TO_SUBMIT, Long.class);
		namedQuery.setParameter("fdTypes", financialDocumentTypes);
		
		return namedQuery.getResultList();
		
	    });
	    
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
	
    }

    private void setFinancialDocumentMappedReferences(FinancialDocument financialDocument) {
	if (financialDocument != null && financialDocument.getItems() != null) {
	    financialDocument.getItems().forEach(item -> item.setFinancialDocument(financialDocument));
	}
	setHistoryMappedReferences(financialDocument);
	setWorkFlowStepsMappedReferences(financialDocument);
    }

    private void setWorkFlowStepsMappedReferences(FinancialDocument financialDocument) {
	if (financialDocument.getWorkflow() != null && financialDocument.getWorkflow().getSteps() != null) {
	    financialDocument.getWorkflow().getSteps().forEach(step -> {
		step.setWorkflow(financialDocument.getWorkflow());
	    });
	}
    }

    private void setHistoryMappedReferences(FinancialDocument financialDocument) {
	if (financialDocument != null && financialDocument.getHistories() != null) {
	    financialDocument.getHistories().forEach(history -> {
		history.setFinancialDocument(financialDocument);
		setAttachmentMappedReferences(history);
	    });
	}
    }

    private void setAttachmentMappedReferences(History history) {
	if (history.getAttachments() != null) {
	    history.getAttachments().forEach(attachment -> attachment.setHistory(history));
	}
    }

    @Override
    public void updateFinancialDocumentStatus(final FinancialDocument financialDocument, final String status) throws UnexpectedPersistenceException {

	try {

	    this.txRequired(em -> {

		if (financialDocument != null && StringUtils.isBlank(status) == false && financialDocument.getStatus().equals(status) == false) {

		    final CriteriaBuilder cb = em.getCriteriaBuilder();
		    final CriteriaUpdate<FinancialDocument> updateFinancialDocumentStatusCriteria = cb.createCriteriaUpdate(FinancialDocument.class);
		    final Root<FinancialDocument> root = updateFinancialDocumentStatusCriteria.from(FinancialDocument.class);

		    updateFinancialDocumentStatusCriteria.set("status", status);
		    updateFinancialDocumentStatusCriteria.where(cb.equal(root.get("id"), financialDocument.getId()));
		    em.createQuery(updateFinancialDocumentStatusCriteria).executeUpdate();

		}

		em.flush();

	    });

	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}

    }

    
    @Override
    public FinancialDocument getFinancialDocumentNotCanceledNotRejectedByNumber(final String number) throws NotFoundException, UnexpectedPersistenceException {

	try {

	    return txSupports(em -> {

		TypedQuery<FinancialDocument> namedQuery = em.createNamedQuery(FinancialDocument.NAMED_QUERY_FIND_FINANCIAL_DOCUMENT_NOT_CANCELED_NO_REJECT_BY_NUMBER, FinancialDocument.class);
		namedQuery.setParameter("financialDocumentNumber", number);

		return namedQuery.getSingleResult();

	    });

	} catch (Exception e) {

	    if (e.getCause().getClass() == NoResultException.class) {
		throw new NotFoundException(FinancialDocument.class);
	    } else {
		throw new UnexpectedPersistenceException(e.getMessage());
	    }

	}
    }

    @Override
    public void updateFDDocumentExternalId(final Long financialDocumentId, final String documentExternalId) throws UnexpectedPersistenceException {
	
	try {

	    this.txRequired(em -> {

		if (financialDocumentId != null && StringUtils.isBlank(documentExternalId) == false) {

		    final CriteriaBuilder cb = em.getCriteriaBuilder();
		    final CriteriaUpdate<FinancialDocument> updateFinancialDocumentStatusCriteria = cb.createCriteriaUpdate(FinancialDocument.class);
		    final Root<FinancialDocument> root = updateFinancialDocumentStatusCriteria.from(FinancialDocument.class);

		    updateFinancialDocumentStatusCriteria.set("documentExternalId", documentExternalId);
		    updateFinancialDocumentStatusCriteria.where(cb.equal(root.get("id"), financialDocumentId));
		    em.createQuery(updateFinancialDocumentStatusCriteria).executeUpdate();

		}

		em.flush();

	    });

	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
	
    }

}
