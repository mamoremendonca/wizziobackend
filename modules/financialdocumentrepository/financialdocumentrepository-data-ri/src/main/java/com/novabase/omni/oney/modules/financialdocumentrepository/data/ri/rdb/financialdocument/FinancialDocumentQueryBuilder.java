package com.novabase.omni.oney.modules.financialdocumentrepository.data.ri.rdb.financialdocument;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.novabase.omni.oney.modules.financialdocumentrepository.data.ri.rdb.financialdocument.enums.ScopeType;
import com.novabase.omni.oney.modules.financialdocumentrepository.entity.SearchCriteria;

public class FinancialDocumentQueryBuilder {

    private static final String QUERY_SELECT = "SELECT DISTINCT fd ";
    private static final String QUERY_FROM = "FROM FinancialDocument fd ";
    private static final String QUERY_WHERE = "WHERE (1 = 1) ";

    private static final String FIELD_STATUS = "fd.status";
    private static final String FIELD_NUMBER = "fd.number";
    private static final String FIELD_TYPE = "fd.type";
    private static final String FIELD_PURCHASE_ORDER_FRIENDLY_NUMBER = "fd.purchaseOrderFriendlyNumber";
    private static final String FIELD_SUPPLIER_CODE = "fd.supplierCode";
    private static final String FIELD_INTERNAL_ORDER_CODE = "fd.internalOrderCode";
    private static final String FIELD_PROJECT_CODE = "fd.projectCode";
    private static final String FIELD_DEPARTMENT_CODE = "fd.departmentCode";
    private static final String FIELD_TOTAL = "fd.totalWithoutTax";
    private static final String FIELD_CREATION_DATE = "fd.creationDate";
    private static final String FIELD_EXPIRATION_DATE = "fd.expirationDate";
    private static final String FIELD_FINISH_DATE = "fd.finishDate";

    private static final String SPACE = " ";
    private static final String OPEN_PARENTHESES = " (";
    private static final String CLOSE_PARENTHESES = ") ";
    private static final String SINGLE_QUOTE = "'";
    private static final String EQUALS = " = ";
    private static final String LESS_THAN_OR_EQUAL_TO = " <= ";
    private static final String GREATER_THAN_OR_EQUAL_TO = " >= ";
    private static final String BETWEEN = " BETWEEN ";

    private static final String AND = " AND ";
    private static final String ORDER_BY = " ORDER BY ";
    private static final String JOIN = " JOIN ";

    //@formatter:off
    public static String buildQueryFromSearchCriteria(final SearchCriteria searchCriteria) {

	return new StringBuilder().
			append(QUERY_SELECT).
			append(QUERY_FROM).
			append(scopeFromClause(searchCriteria)).
			append(QUERY_WHERE).		
			append(buildWhereClauses(searchCriteria)).
			append(buildSortResults(searchCriteria)).
			toString();
    }
    
    private static String buildWhereClauses(final SearchCriteria searchCriteria) {
	
	final List<String> whereClauses = new ArrayList<>();
	whereClauses.add(scopeWhereClause(searchCriteria));
	whereClauses.add(filterByStatus(searchCriteria));
	whereClauses.add(filterByNumber(searchCriteria));
	whereClauses.add(filterByType(searchCriteria));
	whereClauses.add(filterByPurchaseOrderFriendlyNumber(searchCriteria));
	whereClauses.add(filterByInternalOrderCode(searchCriteria));
	whereClauses.add(filterProjectCode(searchCriteria));
	whereClauses.add(filterDepartmentCode(searchCriteria));
	whereClauses.add(filterByMinTotalValue(searchCriteria));
	whereClauses.add(filterByMaxTotalValue(searchCriteria));
	whereClauses.add(filterSupplierCode(searchCriteria));
	whereClauses.add(filterByCreationDate(searchCriteria));
	whereClauses.add(filterByExpirationDate(searchCriteria));
	whereClauses.add(filterByFinishDate(searchCriteria));

	// Adding AND for the first argument.
	final List<String> whereList = whereClauses.stream().
							filter(clause -> StringUtils.isNotBlank(clause)).
							collect(Collectors.toList());

	final StringBuilder sb = new StringBuilder();
	if (whereList.isEmpty() == false) {
	    sb.append(AND);
	}
	sb.append(whereList.stream().collect(Collectors.joining(AND)));
	
	return sb.toString();
	
    }

    private static String filterByNumber(final SearchCriteria searchCriteria) {
	
	if (StringUtils.isNotBlank(searchCriteria.getNumber())) {
	  //@formatter:off
	    return 
		    new StringBuilder()
		    	.append(FIELD_NUMBER)
		    	.append(EQUALS)
		    	.append(SINGLE_QUOTE)
		    	.append(searchCriteria.getNumber())
		    	.append(SINGLE_QUOTE)
		    	.toString();
	    //@formatter:on
	}

	return StringUtils.EMPTY;
    }

    private static String filterByStatus(final SearchCriteria searchCriteria) {

	if (StringUtils.isNotBlank(searchCriteria.getStatus())) {

	    //@formatter:off
	    return new StringBuilder().
		    		append(OPEN_PARENTHESES).
		    		append(FIELD_STATUS).
		    		append(EQUALS).
		    		append(SINGLE_QUOTE).
		    		append(searchCriteria.getStatus()).
		    		append(SINGLE_QUOTE).
		    		append(CLOSE_PARENTHESES).
		    		toString();
	    //@formatter:on

	}

	return StringUtils.EMPTY;

    }

    private static String filterByType(final SearchCriteria searchCriteria) {

	if (StringUtils.isNotBlank(searchCriteria.getType())) {

	    //@formatter:off
	    return new StringBuilder().
		    		append(OPEN_PARENTHESES).
		    		append(FIELD_TYPE).
		    		append(EQUALS).
		    		append(SINGLE_QUOTE).
		    		append(searchCriteria.getType()).
		    		append(SINGLE_QUOTE).
		    		append(CLOSE_PARENTHESES).
		    		toString();
	    //@formatter:on

	}

	return StringUtils.EMPTY;

    }

    private static String filterByPurchaseOrderFriendlyNumber(final SearchCriteria searchCriteria) {

	if (searchCriteria.getPurchaseOrderNumber() != null) {

	    //@formatter:off
	    return new StringBuilder().
		    		append(OPEN_PARENTHESES).
		    		append(FIELD_PURCHASE_ORDER_FRIENDLY_NUMBER).
		    		append(EQUALS).
		    		append(SINGLE_QUOTE).
		    		append("PO" + StringUtils.leftPad(Long.toString(searchCriteria.getPurchaseOrderNumber()), 5, "0")).
		    		append(SINGLE_QUOTE).
		    		append(CLOSE_PARENTHESES).
		    		toString();
	    //@formatter:on

	}

	return StringUtils.EMPTY;

    }

    private static String filterProjectCode(final SearchCriteria searchCriteria) {

	if (StringUtils.isNotBlank(searchCriteria.getProjectCode())) {

	    //@formatter:off
	    return new StringBuilder().
		    		append(OPEN_PARENTHESES).
		    		append(FIELD_PROJECT_CODE).
		    		append(EQUALS).
		    		append(SINGLE_QUOTE).
		    		append(searchCriteria.getProjectCode()).
		    		append(SINGLE_QUOTE).
		    		append(CLOSE_PARENTHESES).
		    		toString();
	    //@formatter:on

	}

	return StringUtils.EMPTY;

    }

    private static String filterDepartmentCode(final SearchCriteria searchCriteria) {

	if (StringUtils.isNotBlank(searchCriteria.getDepartmentCode())) {

	    //@formatter:off
	    return new StringBuilder().
		    		append(OPEN_PARENTHESES).
		    		append(FIELD_DEPARTMENT_CODE).
		    		append(EQUALS).
		    		append(SINGLE_QUOTE).
		    		append(searchCriteria.getDepartmentCode()).
		    		append(SINGLE_QUOTE).
		    		append(CLOSE_PARENTHESES).
		    		toString();
	    //@formatter:on

	}

	return StringUtils.EMPTY;

    }

    private static String filterSupplierCode(final SearchCriteria searchCriteria) {

	if (StringUtils.isNotBlank(searchCriteria.getSupplierCode())) {

	    //@formatter:off
	    return new StringBuilder().
		    		append(OPEN_PARENTHESES).
		    		append(FIELD_SUPPLIER_CODE).
		    		append(EQUALS).
		    		append(SINGLE_QUOTE).
		    		append(searchCriteria.getSupplierCode()).
		    		append(SINGLE_QUOTE).
		    		append(CLOSE_PARENTHESES).
		    		toString();
	    //@formatter:on

	}

	return StringUtils.EMPTY;

    }

    private static String filterByInternalOrderCode(final SearchCriteria searchCriteria) {

	if (StringUtils.isNotBlank(searchCriteria.getInternalOrderCode())) {

	    //@formatter:off
	    return new StringBuilder().
		    		append(OPEN_PARENTHESES).
		    		append(FIELD_INTERNAL_ORDER_CODE).
		    		append(EQUALS).
		    		append(SINGLE_QUOTE).
		    		append(searchCriteria.getInternalOrderCode()).
		    		append(SINGLE_QUOTE).
		    		append(CLOSE_PARENTHESES).
		    		toString();
	    //@formatter:on

	}

	return StringUtils.EMPTY;

    }

    private static String filterByMinTotalValue(final SearchCriteria searchCriteria) {

	if (searchCriteria.getMinVATValue() != null) {

	    //@formatter:off
	    return new StringBuilder()
		    	.append(FIELD_TOTAL)
		    	.append(GREATER_THAN_OR_EQUAL_TO)
		 	.append(Long.toString(searchCriteria.getMinVATValue()))
		    	.toString();
	    //@formatter:on

	}

	return StringUtils.EMPTY;

    }

    private static String filterByMaxTotalValue(final SearchCriteria searchCriteria) {

	if (searchCriteria.getMaxVATValue() != null) {

	    //@formatter:off
	    return new StringBuilder()
		    	.append(FIELD_TOTAL)
		    	.append(LESS_THAN_OR_EQUAL_TO)
		 	.append(Long.toString(searchCriteria.getMaxVATValue()))
		    	.toString();
	    //@formatter:on

	}

	return StringUtils.EMPTY;

    }

    private static String filterByCreationDate(final SearchCriteria searchCriteria) {

	if (searchCriteria.getCreationDate() != null) {

	    return new StringBuilder().append(FIELD_CREATION_DATE).append(BETWEEN).append(SINGLE_QUOTE).append(searchCriteria.getCreationDate().format(DateTimeFormatter.ISO_LOCAL_DATE))
		    .append(SINGLE_QUOTE).append(AND).append(SINGLE_QUOTE).append(searchCriteria.getCreationDate().plusDays(1L).format(DateTimeFormatter.ISO_LOCAL_DATE)).append(SINGLE_QUOTE)
		    .toString();

	}

	return StringUtils.EMPTY;

    }

    private static String filterByExpirationDate(final SearchCriteria searchCriteria) {

	if (searchCriteria.getExpirationDate() != null) {

	    return new StringBuilder().append(FIELD_EXPIRATION_DATE).append(BETWEEN).append(SINGLE_QUOTE).append(searchCriteria.getExpirationDate().format(DateTimeFormatter.ISO_LOCAL_DATE))
		    .append(SINGLE_QUOTE).append(AND).append(SINGLE_QUOTE).append(searchCriteria.getExpirationDate().plusDays(1L).format(DateTimeFormatter.ISO_LOCAL_DATE)).append(SINGLE_QUOTE)
		    .toString();

	}

	return StringUtils.EMPTY;

    }

    private static String filterByFinishDate(final SearchCriteria searchCriteria) {

	if (searchCriteria.getFinishDate() != null) {

	    return new StringBuilder().append(FIELD_FINISH_DATE).append(BETWEEN).append(SINGLE_QUOTE).append(searchCriteria.getFinishDate().format(DateTimeFormatter.ISO_LOCAL_DATE))
		    .append(SINGLE_QUOTE).append(AND).append(SINGLE_QUOTE).append(searchCriteria.getFinishDate().plusDays(1L).format(DateTimeFormatter.ISO_LOCAL_DATE)).append(SINGLE_QUOTE).toString();

	}

	return StringUtils.EMPTY;

    }

    private static String buildSortResults(final SearchCriteria searchCriteria) {

	final StringBuilder sb = new StringBuilder();
	if (StringUtils.isNotBlank(searchCriteria.getOrderField())) {

	    sb.append(ORDER_BY + "fd." + searchCriteria.getOrderField());

	    if (StringUtils.isNotBlank(searchCriteria.getOrderType())) {
		sb.append(SPACE + searchCriteria.getOrderType() + " ");
	    } else {
		sb.append(" ASC ");
	    }

	} else {
	    sb.append(SPACE);
	}

	if ("DESC".equals(searchCriteria.getOrderType())) {
	    return sb.toString() + " NULLS LAST ";
	} else {
	    return sb.toString() + " NULLS FIRST ";
	}

    }

    private static String scopeFromClause(final SearchCriteria searchCriteria) {

//	final ScopeType type = ScopeType.valueOf(searchCriteria.getScope().getScopeType());
//	switch (type) {
//	
//		case ALL:
	return JOIN + " FETCH fd.workflow workflow" + JOIN + "workflow.steps wfs ";
//		
//		default:
//		    return StringUtils.EMPTY;
//		
//	}

    }

    private static String scopeWhereClause(final SearchCriteria searchCriteria) {

	final ScopeType type = ScopeType.valueOf(searchCriteria.getScope().getScopeType());
	switch (type) {

	case MINE:
	    return "fd.userId = '" + searchCriteria.getScope().getCurrentUserId() + "'" + AND + "fd.userGroup = '" + searchCriteria.getScope().getCurrentUserGroup() + "' ";

	case TEAM:
	    return "fd.userGroup = '" + searchCriteria.getScope().getCurrentUserGroup() + "' ";

	case ALL:
	    return (StringUtils.isNotEmpty(searchCriteria.getScope().getUserGroup()) ? "fd.userGroup = '" + searchCriteria.getScope().getUserGroup() + "' " : StringUtils.EMPTY)
		    + (StringUtils.isNotEmpty(searchCriteria.getScope().getCurrentUserGroup()) ? "wfs.userGroup = '" + searchCriteria.getScope().getCurrentUserGroup() + "' " : StringUtils.EMPTY);

	default:
	    return "";

	}

    }

}
