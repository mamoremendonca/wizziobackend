package com.novabase.omni.oney.modules.purchaseorderrepository.data.api.exception;

public class UniqueConstraintViolationException extends Exception {

    public static final int PURCHASE_ORDER_ID = 1;
    public static final int PURCHASE_ORDER_NUMBER_VERSION = 2;

    private static final long serialVersionUID = -2845483354342267384L;

    private int type;

    public UniqueConstraintViolationException(String message, int type) {
	super(message);
	this.type = type;
    }

    public int getType() {
	return type;
    }
}
