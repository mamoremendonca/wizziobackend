package com.novabase.omni.oney.modules.purchaseorderrepository.data.api.exception;

public class NotFoundException extends Exception {

    private static final long serialVersionUID = 6708200998900278015L;

    private Class<?> entity;

    public NotFoundException(String message) {
	super(message);
    }

    public NotFoundException(Class<?> entity) {
	super();
	this.entity = entity;
    }

    public Class<?> getEntity() {
	return entity;
    }

}
