package com.novabase.omni.oney.modules.purchaseorderrepository.data.api;

import java.time.LocalDateTime;
import java.util.List;

import org.osgi.annotation.versioning.ProviderType;

import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.exception.NotFoundException;
import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.exception.UnexpectedPersistenceException;
import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.exception.UniqueConstraintViolationException;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.Attachment;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.History;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.PurchaseOrder;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.SearchCriteria;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.Workflow;

@ProviderType
public interface PurchaseOrderDAO {

    public PurchaseOrder getPurchaseOrder(Long id) throws NotFoundException, UnexpectedPersistenceException;
    public PurchaseOrder addPurchaseOrder(PurchaseOrder purchaseOrder) throws UniqueConstraintViolationException, UnexpectedPersistenceException;
    public History addHistory(History history) throws UnexpectedPersistenceException;
    public List<History> addHistories(List<History> histories) throws UnexpectedPersistenceException;
    public List<History> searchPurchaseOrderHistory(Long purchaseOrderId) throws UnexpectedPersistenceException;
    public Workflow searchPurchaseOrderWorkFlow(Long purchaseOrderId) throws UnexpectedPersistenceException;
    public Workflow updateWorkFlow(Workflow workflow, Long purchaseOrderId, String purchaseOrderStatus, LocalDateTime approvalDate) throws UnexpectedPersistenceException;
    public List<PurchaseOrder> searchPurchaseOrders(SearchCriteria search) throws UnexpectedPersistenceException;
    public Attachment getPurchaseOrderAttachment(Long id, int index) throws NotFoundException, UnexpectedPersistenceException;
    public List<Attachment> getPurchaseOrderAttachments(Long id) throws NotFoundException, UnexpectedPersistenceException;
    public Attachment updateAttachment(Attachment attachment) throws UnexpectedPersistenceException;
    public List<Long> findProcessToSubmit(List<String> purchaseOrderTypes) throws UnexpectedPersistenceException;
    public void updatePurchaseOrderStatus(final PurchaseOrder purchaseOrder, final String status) throws UnexpectedPersistenceException;
    public PurchaseOrder getApprovedPurchaseOrderByNumber(final Long number) throws NotFoundException, UnexpectedPersistenceException;
    public Long getPurchaseOrderIdByInstanceId(Long instanceId) throws UnexpectedPersistenceException;
    public void deletePurchaseOrderById(Long id) throws UnexpectedPersistenceException, NotFoundException;
    
}
