package com.novabase.omni.oney.modules.purchaseorderrepository.jobs.dto;

import java.util.List;

public class TokenDTO {
    public String id;
    public String accessToken;
    public String refreshToken;
    public String firstName;
    public String lastName;
    public String email;
    public String domain;
    public String authenticatorName;
    public List<String> properties;
    public List<String> entitlements;
}
