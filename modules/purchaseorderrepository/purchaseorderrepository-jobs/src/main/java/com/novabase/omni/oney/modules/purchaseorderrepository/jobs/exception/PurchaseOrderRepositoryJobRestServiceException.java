package com.novabase.omni.oney.modules.purchaseorderrepository.jobs.exception;

public class PurchaseOrderRepositoryJobRestServiceException extends RuntimeException {

    private static final long serialVersionUID = 7073426414526421368L;

    public PurchaseOrderRepositoryJobRestServiceException(String msg) {
	super(msg);
    }

    public static PurchaseOrderRepositoryJobRestServiceException of(String msg) {
	return new PurchaseOrderRepositoryJobRestServiceException("PurchaseOrderRepository, Error trying to execute rest request: " + msg);
    }
}
