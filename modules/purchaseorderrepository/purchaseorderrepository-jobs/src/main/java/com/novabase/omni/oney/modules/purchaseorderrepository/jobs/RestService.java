package com.novabase.omni.oney.modules.purchaseorderrepository.jobs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Base64;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class RestService {

    public static String getTokenUAM(String address, String recource, String userName, String password) throws IOException, ParseException {

	final JSONObject json = (JSONObject) new JSONParser().parse(callTokenService(address, recource, userName, password));

	return (String) json.get("accessToken");
    }

    public static int runTask(final String address, final String resource, final String token) throws IOException {
	
	URL url = new URL(address + resource);
	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	connection.setRequestMethod("POST");
	connection.setDoOutput(Boolean.TRUE);
	connection.setRequestProperty("Authorization", "Bearer " + token);
	connection.setRequestProperty("Content-Type", "application/json");

	return connection.getResponseCode();
    }

    private static String callTokenService(final String address, final String resource, final String userName, final String password) throws IOException {

	URL url = new URL(address + resource);
	String encoding = Base64.getEncoder().encodeToString((userName + ":" + password).getBytes(Charset.defaultCharset()));

	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	connection.setRequestMethod("POST");
	connection.setDoOutput(Boolean.TRUE);
	connection.setRequestProperty("Authorization", "Basic " + encoding);
	InputStream content = connection.getInputStream();
	BufferedReader in = new BufferedReader(new InputStreamReader(content));

	return in.readLine();
    }

}
