package com.novabase.omni.oney.modules.purchaseorderrepository.jobs;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.PurchaseOrderRepositoryResource;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.ri.PurchaseOrderRepositoryContext;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.ri.PurchaseOrderRepositoryResourceProperties;

import io.digitaljourney.platform.modules.commons.AbstractContextualComponent;
import io.digitaljourney.platform.modules.scheduling.api.Job;
import io.digitaljourney.platform.modules.scheduling.api.JobContext;
import io.digitaljourney.platform.modules.scheduling.api.JobManager;

//@formatter:off
@Component(
	immediate = true,
	configurationPid = PurchaseOrderSubmitJobConfig.CPID,
	configurationPolicy = ConfigurationPolicy.REQUIRE
	,
	reference = {
		@Reference(
			name = PurchaseOrderRepositoryResourceProperties.REF_CONTEXT,
			service = PurchaseOrderRepositoryContext.class,
			cardinality = ReferenceCardinality.MANDATORY
		)
	}
)
@Designate(ocd = PurchaseOrderSubmitJobConfig.class)
//@formatter:on
public class PurchaseOrderSubmitJob extends AbstractContextualComponent<PurchaseOrderRepositoryContext, PurchaseOrderSubmitJobConfig> implements Job {

    /**
     * Reference to the scheduler, this is optional and only used to unschedule the
     * job
     */
    @Reference
    private JobManager scheduler;

    @Reference
    private PurchaseOrderRepositoryResource purchaseOrderRepositoryResource;

    /**
     * Method that is executed whenever the bundle changes to the active state (i.e.
     * all dependencies are resolved).
     *
     * @param ctx    Component context
     * @param config Component configuration
     */
    @Activate
    public void activate(ComponentContext ctx, PurchaseOrderSubmitJobConfig config) {
	prepare(ctx, config);
	info("PurchaseOrderSubmitJobConfig was initialized and will start as soon as schedule conditions are satisfied.");
    }

    /**
     * Method that is executed whenever the is a change on the associated
     * configurations.
     *
     * @param config Component configuration
     */
    @Modified
    public void modified(PurchaseOrderSubmitJobConfig config) {
	prepare(config);
	info("PurchaseOrderSubmitJobConfig was modified, preparing.");
    }

    /**
     * Method called whenever the bundle transits from the Active state to the
     * Resolved state
     */
    @Deactivate
    public void deactivate() {
	try {
	    scheduler.unschedule(getConfig().scheduling_name());
	} catch (Exception e) {
	    // could not unschedule the job for some reason, this is not fatal
	    warn("Could not unschedule PurchaseOrderSubmitJob. Cause: {}", e.getMessage(), e);
	}
    }

    //@formatter:off
    @Override
    public void execute(JobContext context) {

	try {
	    String tokenUAM = 
		    RestService.getTokenUAM(
			    getConfig().address(), 
			    getConfig().resourceAuth(), 
			    getConfig().scheduling_username(), 
			    getConfig().scheduling_password());
	    
	    int responseCode = RestService.runTask(
		    getConfig().address(), 
		    getConfig().resourceTask(), 
		    tokenUAM);
	    
	    if(responseCode != 200) {
		getCtx().logError("[PurchaseOrderSubmitJob] error trying to execute JOB, response code: " + responseCode);
	    }else {
		getCtx().logInfo("[PurchaseOrderSubmitJob] JOB executed successfully");
	    }
	} catch (Exception e) {
	    getCtx().logError("[PurchaseOrderSubmitJob] error trying to execute JOB: " + e.toString());
	    e.printStackTrace();
	}
    }
    //@formatter:on

}