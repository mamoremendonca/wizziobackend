package com.novabase.omni.oney.modules.purchaseorderrepository.jobs;

import javax.ws.rs.core.MediaType;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Job Configuration.
 *
 */
//@formatter:off
@ObjectClassDefinition(
		name = "%name", description = "%description",
		localization = "OSGI-INF/l10n/purchaseordersubmitjobconfig",
		icon = @Icon(resource = "OSGI-INF/icon/resource.png", size = 32)
)
//@formatter:on
public @interface PurchaseOrderSubmitJobConfig {
    /** Component Identifier */
    public static final String CPID = "oney.base.modules.purchaseorderrepository.jobs.purchaseordersubmitjob";

    /**
     * Cron expression to describe how often should the job run. Replaces
     * scheduling.period and scheduling.times configurations.
     *
     * @return Scheduling expression
     */
    @AttributeDefinition(name = "%scheduling_expression.name", description = "%scheduling_expression.description", type = AttributeType.STRING)
    String scheduling_expression();

    /**
     * How often should the job run, in seconds.
     *
     * @return Scheduling period
     */
    @AttributeDefinition(name = "%scheduling_period.name", description = "%scheduling_period.description", type = AttributeType.LONG)
    long scheduling_period();

    /**
     * How many times the job should run. Set this if the job should only run a
     * finite number of times.
     *
     * @return Number of times the job should run
     */
    @AttributeDefinition(name = "%scheduling_times.name", description = "%scheduling_times.description", type = AttributeType.STRING)
    int scheduling_times();

    /**
     * Determines if the job should start immediately, or only after the first
     * period. Defaults to false.
     *
     * @return True if the schedule should start immediately, false otherwise
     *         (default: false)
     */
    @AttributeDefinition(name = "%scheduling_immediate.name", description = "%scheduling_immediate.description", type = AttributeType.BOOLEAN)
    boolean scheduling_immediate() default false;

    /**
     * Determines if the job can run at concurrently with other jobs.
     *
     * @return True if the job can run concurrently with other jobs, false otherwise
     *         (default: true)
     */
    @AttributeDefinition(name = "%scheduling_concurrent.name", description = "%scheduling_concurrent.description", type = AttributeType.BOOLEAN)
    boolean scheduling_concurrent() default true;

    /**
     * Job’s identifier. This name could be useful when requesting the job object
     * from the scheduler but, in most cases, just set it to an unique value and
     * ignore it.
     *
     * @return Job Identifier
     */
    @AttributeDefinition(name = "%scheduling_name.name", description = "%scheduling_name.description", required = true, type = AttributeType.STRING)
    String scheduling_name();

    /**
     * Determines if the job should support recoverable data. Set to true if the job
     * uses the JobContext getRecoverableData and putRecoverableData API.
     *
     * @return True if the job needs to persist context, false otherwise (default:
     *         false)
     */
    @AttributeDefinition(name = "%scheduling_persistData.name", description = "%scheduling_persistData.description", type = AttributeType.BOOLEAN)
    boolean scheduling_persistData() default false;

    /**
     * Username of the system user used to make requests to protected endpoints.
     *
     * @return Username of the system user
     */
    @AttributeDefinition(name = "%scheduling_username.name", description = "%scheduling_username.description", type = AttributeType.STRING)
    String scheduling_username();

    /**
     * Password of the system user used to make requests to protected endpoints.
     *
     * @return Password of the system user
     */
    @AttributeDefinition(name = "%scheduling_password.password", description = "%scheduling_password.description", type = AttributeType.PASSWORD)
    String scheduling_password();

    @AttributeDefinition(name = "%address.name", description = "%address.description")
    public String address() default "http://localhost:4500/";

    @AttributeDefinition(name = "%resourceAuth.name", description = "%resourceAuth.description")
    public String resourceAuth() default "/core/uam/v10/oauth/authorize";

    @AttributeDefinition(name = "%resourceTask.name", description = "%resourceTask.description")
    public String resourceTask() default "/core/purchaseorderrepository/run/purchaseOrderSubmitJob";

}