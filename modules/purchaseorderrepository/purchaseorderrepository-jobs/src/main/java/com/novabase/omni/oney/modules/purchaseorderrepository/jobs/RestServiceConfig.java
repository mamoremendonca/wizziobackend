//package com.novabase.omni.oney.modules.purchaseorderrepository.jobs;
//
//import javax.ws.rs.core.MediaType;
//
//import org.osgi.service.metatype.annotations.AttributeDefinition;
//import org.osgi.service.metatype.annotations.AttributeType;
//import org.osgi.service.metatype.annotations.Icon;
//import org.osgi.service.metatype.annotations.ObjectClassDefinition;
//
//@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/restservice", icon = @Icon(resource = "OSGI-INF/icon/ws.png", size = 32))
//public @interface RestServiceConfig {
//
//    public static final String CPID = "com.novabase.omni.oney.modules.purchaseorderrepository.jobs.restservice";
//
//    @AttributeDefinition(name = "%providerName.name", description = "%providerName.description")
//    public String providerName() default "PurchaseOrderRepository Wizzio REST Service";
//
//    @AttributeDefinition(name = "%address.name", description = "%address.description")
//    public String address() default "http://localhost:4500/";
//
//    @AttributeDefinition(name = "%contentType.name", description = "%contentType.description")
//    public String contentType() default MediaType.APPLICATION_JSON;
//
//    @AttributeDefinition(name = "%acceptType.name", description = "%acceptType.description")
//    public String acceptType() default MediaType.APPLICATION_JSON;
//
//    @AttributeDefinition(name = "%userName.name", description = "%userName.description")
//    public String userName() default "admin";
//
//    @AttributeDefinition(name = "%password.name", type = AttributeType.PASSWORD, description = "%password.description")
//    public String password() default "admin";
//
//    @AttributeDefinition(name = "%connectionTimeout.name", type = AttributeType.LONG, min = "0", description = "%connectionTimeout.description")
//    public long connectionTimeout() default 10000L;
//
//    @AttributeDefinition(name = "%receiveTimeout.name", type = AttributeType.LONG, min = "0", description = "%receiveTimeout.description")
//    public long receiveTimeout() default 180000L;
//
//    @AttributeDefinition(name = "%proxyEnabled.name", type = AttributeType.BOOLEAN, description = "%proxyEnabled.description")
//    public boolean proxyEnabled() default false;
//
//    @AttributeDefinition(name = "%proxyHost.name", description = "%proxyHost.description")
//    public String proxyHost() default "localhost";
//
//    @AttributeDefinition(name = "%proxyPort.name", description = "%proxyPort.description")
//    public int proxyPort() default 8888;
//
//    @AttributeDefinition(name = "%jsonPathSuccessExpression.name", description = "%jsonPathSuccessExpression.description")
//    public String jsonPathSuccessExpression() default "";
//
//    @AttributeDefinition(name = "%serializationFeatures.name", description = "%serializationFeatures.description")
//    public String[] serializationFeatures() default {};
//
//    @AttributeDefinition(name = "%deserializationFeatures.name", description = "%deserializationFeatures.description")
//    public String[] deserializationFeatures() default { "ACCEPT_EMPTY_STRING_AS_NULL_OBJECT=true", "FAIL_ON_UNKNOWN_PROPERTIES=false" };
//
//    @AttributeDefinition(name = "%resourceAuth.name", description = "%resourceGetDocumentUrl.description")
//    public String resourceAuth() default "/core/uam/v10/oauth/authorize";
//
//    @AttributeDefinition(name = "%resourceTask.name", description = "%resourcePostCreateDocument.description")
//    public String resourceTask() default "/core/purchaseorderrepository/run/purchaseOrderSubmitJob";
//
//}
