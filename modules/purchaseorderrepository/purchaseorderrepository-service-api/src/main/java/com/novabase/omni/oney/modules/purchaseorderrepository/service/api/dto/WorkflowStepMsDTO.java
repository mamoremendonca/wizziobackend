package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto;

import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("WorkflowStep")
public class WorkflowStepMsDTO {

    @ApiModelProperty
    public int index;
    
    @ApiModelProperty
    public String userGroup;
    
    @ApiModelProperty
    public boolean hasNextStep;
    
    @ApiModelProperty
    public boolean active;

}
