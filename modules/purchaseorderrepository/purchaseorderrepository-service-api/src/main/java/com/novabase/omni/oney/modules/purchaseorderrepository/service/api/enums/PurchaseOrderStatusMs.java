package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums;

public enum PurchaseOrderStatusMs {
    
    IN_APPROVAL, APPROVED, SAP_SUBMITTED, REJECTED, CANCELED;

}
