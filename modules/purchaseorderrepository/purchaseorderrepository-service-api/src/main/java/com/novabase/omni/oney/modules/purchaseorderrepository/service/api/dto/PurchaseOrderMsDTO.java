package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto;

import java.time.LocalDateTime;
import java.util.List;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.PurchaseOrderStatusMs;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.PurchaseOrderTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("PurchaseOrder")
public class PurchaseOrderMsDTO extends DTO {

    @ApiModelProperty(hidden = true)
    public Long id;

    @ApiModelProperty
    public Long instanceId;
    
    @ApiModelProperty
    public Long number;
    
    @ApiModelProperty(readOnly = true)
    public String friendlyNumber;

    @ApiModelProperty
    public Long version;
    
    @ApiModelProperty(readOnly = true)
    public PurchaseOrderStatusMs status;

    @ApiModelProperty
    public PurchaseOrderTypeMs type;
    
    @ApiModelProperty
    public String supplierCode;

    @ApiModelProperty
    public String description;

    @ApiModelProperty
    public String departmentCode;

    @ApiModelProperty
    public String projectCode;

    @ApiModelProperty
    public String internalOrderCode;

    @ApiModelProperty
    public LocalDateTime creationDate;

    @ApiModelProperty
    public LocalDateTime expirationDate;
    
    @ApiModelProperty
    public LocalDateTime approvalDate; //TODO: change to finish date
    
    @ApiModelProperty
    public Double totalWithoutTax;
    
    @ApiModelProperty
    public String userGroup;
    
    @ApiModelProperty
    public Long userId;
    
    @ApiModelProperty
    public boolean canClaim;

    @ApiModelProperty
    public List<ItemMsDTO> items;

    @ApiModelProperty
    public List<HistoryMsDTO> histories;
    
    @ApiModelProperty
    public WorkflowMsDTO workflow;
}
