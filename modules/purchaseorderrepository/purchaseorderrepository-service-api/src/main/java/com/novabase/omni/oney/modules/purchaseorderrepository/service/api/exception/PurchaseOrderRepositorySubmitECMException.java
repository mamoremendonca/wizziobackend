package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.exception;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.PurchaseOrderRepositoryProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderRepositorySubmitECMException extends PurchaseOrderRepositoryException {

    private static final long serialVersionUID = -5517511378640712205L;

    public PurchaseOrderRepositorySubmitECMException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.INTERNAL_SERVER_ERROR, errorCode, ctx, args);
    }

    public static PurchaseOrderRepositorySubmitECMException of(Context ctx, Long id, Long attachmentId) {
	return new PurchaseOrderRepositorySubmitECMException(PurchaseOrderRepositoryProperties.SUBMIT_ECM_EXCEPTION, ctx, id, attachmentId);
    }

}
