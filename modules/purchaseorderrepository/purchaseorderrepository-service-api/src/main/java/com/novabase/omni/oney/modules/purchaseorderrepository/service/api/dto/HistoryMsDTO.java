package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto;

import java.time.LocalDateTime;
import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.TimeLineStepTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("History")
public class HistoryMsDTO {

    @ApiModelProperty(hidden = true)
    private Long id;
    
    @ApiModelProperty
    public LocalDateTime date;

    @ApiModelProperty
    public TimeLineStepTypeMs type; 

    @ApiModelProperty
    public Long userId;

    @ApiModelProperty
    public String comment;

    @ApiModelProperty
    public String userGroup;

    @ApiModelProperty
    public Set<AttachmentMsDTO> attachments;
}
