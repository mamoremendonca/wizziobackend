package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto;

import java.time.LocalDate;

import javax.xml.bind.annotation.XmlRootElement;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.PurchaseOrderStatusMs;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.PurchaseOrderTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("SearchCriteria")
public class SearchCriteriaMsDTO extends DTO {

    @ApiModelProperty
    public PurchaseOrderStatusMs status;

    @ApiModelProperty
    public Long number;

    @ApiModelProperty
    public PurchaseOrderTypeMs type;

    @ApiModelProperty
    public String entityCode;			// Code or NIF?

    @ApiModelProperty
    public String internalOrderCode;

    @ApiModelProperty
    public String projectCode;
    
    @ApiModelProperty
    public String departmentCode;
    
    @ApiModelProperty
    public SearchScopeMsDTO scope;

    @ApiModelProperty
    public Long minVATValue;
    @ApiModelProperty
    public Long maxVATValue;

    @ApiModelProperty
    public LocalDate createdDate;
    @ApiModelProperty
    public LocalDate approvalDate;
    @ApiModelProperty
    public LocalDate expirationDate;
    
    @ApiModelProperty
    public OrderByMsDTO orderBy;
}
