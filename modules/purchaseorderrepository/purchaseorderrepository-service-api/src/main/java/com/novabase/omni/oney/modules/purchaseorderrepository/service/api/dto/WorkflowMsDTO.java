package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto;

import java.time.LocalDateTime;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.PurchaseOrderStatusMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("Workflow")
public class WorkflowMsDTO {

    @ApiModelProperty(hidden = true)
    public Long id;

    @ApiModelProperty
    public LocalDateTime creationDate;

    @ApiModelProperty
    public String currentUserGroup;
    
    @ApiModelProperty
    public int currentIndex;

    @ApiModelProperty
    public LocalDateTime limitDateToNextStep;

    @ApiModelProperty
    public boolean hasNextStep;
    
    @ApiModelProperty
    public PurchaseOrderStatusMs purchaseOrderStatus;
    
    @ApiModelProperty
    public List<WorkflowStepMsDTO> steps;

}
