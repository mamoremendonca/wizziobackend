package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto;

import java.time.LocalDateTime;

import javax.xml.bind.annotation.XmlRootElement;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.DocumentClassTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("Attachment")
public class AttachmentMsDTO {

    @ApiModelProperty(hidden = true)
    public Long id;
    
    @ApiModelProperty
    public int index;

    @ApiModelProperty
    public Long blobId;

    @ApiModelProperty
    public String externalId;
    
    @ApiModelProperty
    public DocumentClassTypeMs documentClass;

    @ApiModelProperty
    public String mimeType;

    @ApiModelProperty
    public String name;

    @ApiModelProperty
    public Long userId;

    @ApiModelProperty
    public LocalDateTime date;

}
