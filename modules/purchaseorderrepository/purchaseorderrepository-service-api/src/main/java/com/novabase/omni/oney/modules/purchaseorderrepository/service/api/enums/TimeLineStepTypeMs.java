package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums;

public enum TimeLineStepTypeMs {
    SUBMITTED, TO_APPROVE, APPROVED, SKIPPED, RETURNED, REJECTED, CANCELED, SAP_SUBMITTED;
}
