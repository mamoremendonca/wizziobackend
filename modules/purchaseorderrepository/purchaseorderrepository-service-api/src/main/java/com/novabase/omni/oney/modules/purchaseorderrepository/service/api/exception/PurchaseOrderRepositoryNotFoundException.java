package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.exception;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.PurchaseOrderRepositoryProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderRepositoryNotFoundException extends PurchaseOrderRepositoryException {

    private static final long serialVersionUID = -5517511378640712205L;

    public PurchaseOrderRepositoryNotFoundException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
    }

    public static PurchaseOrderRepositoryNotFoundException of(Context ctx, Class<?> entity, Long id) {
	return new PurchaseOrderRepositoryNotFoundException(PurchaseOrderRepositoryProperties.NOT_FOUND_EXCEPTION, ctx, entity.getSimpleName(), id);
    }
    
    public static PurchaseOrderRepositoryNotFoundException of(final Context ctx, final Class<?> entity, final String number) {
	return new PurchaseOrderRepositoryNotFoundException(PurchaseOrderRepositoryProperties.NOT_FOUND_EXCEPTION, ctx, entity.getSimpleName(), number);
    }

}
