package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.exception;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;
import io.digitaljourney.platform.modules.ws.rs.api.exception.RSException;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.PurchaseOrderRepositoryProperties;

public class PurchaseOrderRepositoryException extends RSException {
	private static final long serialVersionUID = -8645577031119256555L;
	
	protected PurchaseOrderRepositoryException(PlatformCode statusCode, String errorCode, Context ctx, Object... args) {
		super(statusCode, errorCode, ctx, args);
	}
	
	protected PurchaseOrderRepositoryException(Context ctx, Object... args) {
		this(PlatformCode.INTERNAL_SERVER_ERROR, PurchaseOrderRepositoryProperties.UNEXPECTED_EXCEPTION, ctx, args);
	}
	
	protected PurchaseOrderRepositoryException(String errorCode, Context ctx, Object... args) {
		this(PlatformCode.BUSINESS_ERROR, errorCode, ctx, args);
	}

	public static PurchaseOrderRepositoryException of(Context ctx, String message) {
		return new PurchaseOrderRepositoryException(ctx, message);
	}
	
	public static PurchaseOrderRepositoryException of(Context ctx, Throwable cause) {
		return new PurchaseOrderRepositoryException(ctx, cause);
	}
}
