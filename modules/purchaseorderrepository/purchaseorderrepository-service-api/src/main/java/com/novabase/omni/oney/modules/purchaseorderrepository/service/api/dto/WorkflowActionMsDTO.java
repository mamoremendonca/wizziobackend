package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto;

import java.time.LocalDateTime;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.WorkFlowActionTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("WorkFlowAction")
public class WorkflowActionMsDTO {

    @ApiModelProperty
    public WorkFlowActionTypeMs type;

    @ApiModelProperty(readOnly = true)
    public LocalDateTime date;

    public String userGroup;

    public Long userId;

    public String comment;

    public List<AttachmentMsDTO> attachments;

}
