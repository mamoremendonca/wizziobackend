package com.novabase.omni.oney.modules.purchaseorderrepository.service.api;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.osgi.annotation.versioning.ProviderType;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.AttachmentMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.PurchaseOrderMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.SearchCriteriaMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.TimeLineStepMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.WorkflowActionMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.WorkflowMsDTO;

import io.digitaljourney.platform.modules.commons.type.HttpStatusCode;
import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.ApiKeyAuthDefinition.ApiKeyLocation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.BasicAuthDefinition;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;

//@formatter:off
@ProviderType
@Path("")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

@SwaggerDefinition(securityDefinition = @SecurityDefinition(basicAuthDefinitions = {
	@BasicAuthDefinition(key = RSProperties.SWAGGER_BASIC_AUTH) }, apiKeyAuthDefinitions = {
		@ApiKeyAuthDefinition(key = RSProperties.SWAGGER_BEARER_AUTH, name = RSProperties.HTTP_HEADER_API_KEY, in = ApiKeyLocation.HEADER) }), schemes = {
			SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTPS,
			SwaggerDefinition.Scheme.DEFAULT })
@Api(value = "PurchaseOrder Repository (CRUD operations)", authorizations = { @Authorization(value = RSProperties.SWAGGER_BASIC_AUTH),
	@Authorization(value = RSProperties.SWAGGER_BEARER_AUTH) })
@ApiResponses(value = {
	@ApiResponse(code = HttpStatusCode.UNAUTHORIZED_CODE, message = RSProperties.SWAGGER_UNAUTHORIZED_MESSAGE),
	@ApiResponse(code = HttpStatusCode.FORBIDDEN_CODE, message = RSProperties.SWAGGER_FORBIDDEN_MESSAGE) })
public interface PurchaseOrderRepositoryResource {

    @POST
    @Path("/")
    @ApiOperation(value = "Adds a new purchaseOrder", response = PurchaseOrderMsDTO.class)
    PurchaseOrderMsDTO addPurchaseOrder(
	    @ApiParam(value = "The new purchaseOrder", required = true) @Valid PurchaseOrderMsDTO purchaseOrder);

    @GET 
    @Path("/{id}")
    @ApiOperation(value = "Looks up a purchaseOrder by id", response = PurchaseOrderMsDTO.class)
    PurchaseOrderMsDTO getPurchaseOrder(
	    @ApiParam(value = "The purchaseOrder id", required = true) @PathParam("id") Long id, @ApiParam(value = "The userGroup that are reading the Purchase Order", required = false) String userGroup);

    @GET 
    @Path("/number/{number}")
    @ApiOperation(value = "Looks up a purchaseOrder by number", response = PurchaseOrderMsDTO.class)
    PurchaseOrderMsDTO getApprovedPurchaseOrderByNumber(
	    @ApiParam(value = "The purchaseOrder number", required = true) @PathParam("number") final String number);
 
    @POST
    @Path("/{id}/workflow")
    @ApiOperation(value = "Update workflow on the purchaseOrder", response = WorkflowMsDTO.class)
    WorkflowMsDTO updateWorkflow(
	    @ApiParam(value = "The purchaseOrder id", required = true) @PathParam("id") Long purchaseOrderId,
	    @ApiParam(value = "The workflow action", required = true) @Valid WorkflowActionMsDTO action) ;
    
    @PUT
    @Path("/{id}/claim")
    @ApiOperation(value = "Claim the purchaseOrder to some group", response = WorkflowMsDTO.class)
    WorkflowMsDTO claimProcess(
	    @ApiParam(value = "The purchaseOrder id", required = true) @PathParam("id") Long purchaseOrderId,
	    @ApiParam(value = "The userId that are claiming the PurchaseOrder", required = true) @Valid Long userId,
	    @ApiParam(value = "The userGroup that are claiming the PurchaseOrder", required = true) @Valid String userGroup,
	    @ApiParam(value = "The workflow action", required = false) @Valid WorkflowActionMsDTO action) ;
    
    
    @GET
    @Path("/{id}/timeline")
    @ApiOperation(value = "Returns the timeline of a purchaseOrder", response = TimeLineStepMsDTO.class, responseContainer = "List")
    List<TimeLineStepMsDTO> getTimeLine(
	    @ApiParam(value = "The purchaseOrder id", required = true) @PathParam("id") Long purchaseOrderId);
    
    @POST
    @Path("/search")
    @ApiOperation(value = "Searches for purchaseOrder based on a specific search criteria", response = PurchaseOrderMsDTO.class, responseContainer = "List")
    List<PurchaseOrderMsDTO> search(
	    @ApiParam(value = "Search Criteria", required = true) @Valid SearchCriteriaMsDTO searchCriteria);
    
    @GET 
    @Path("/{id}/attachment")
    @ApiOperation(value = "Returns the list of attachments for a purchaseOrder", response = AttachmentMsDTO.class, responseContainer = "List")
    List<AttachmentMsDTO> getPurchaseOrderAttachments(
	    @ApiParam(value = "The purchaseOrder id", required = true) @PathParam("id") Long id);
    
    @GET 
    @Path("/{id}/attachment/{index}")
    @ApiOperation(value = "Returns an attachment", response = AttachmentMsDTO.class)
    AttachmentMsDTO getPurchaseOrderAttachment(
	    @ApiParam(value = "The purchaseOrder id", required = true) @PathParam("id") Long id,
	    @ApiParam(value = "The attachment index", required = true) @PathParam("index") int index);
    
    @POST
    @Path("/run/purchaseOrderSubmitJob")
    void runPurchaseOrderSubmitJob();

}
