package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums;

import java.util.Arrays;

import com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.enums.DocumentClassTypeMsEnum;

public enum DocumentClassTypeMs {
    
    //@formatter:off
    PURCHASE_ORDER(DocumentClassTypeMsEnum.PURCHASE_ORDER), 
    FINANCIAL_DOCUMENT(DocumentClassTypeMsEnum.FINANCIAL_DOCUMENT); 
//    GENERIC(DocumentClassTypeMsEnum.GENERIC)
    //@formatter:on
    
    private DocumentClassTypeMsEnum ecmDocumentClass;

    private DocumentClassTypeMs(DocumentClassTypeMsEnum ecmDocumentClass) {
	this.ecmDocumentClass = ecmDocumentClass;
    }

    public DocumentClassTypeMsEnum getEcmDocumentClass() {
	return ecmDocumentClass;
    }
    
    //@formatter:off
    public static DocumentClassTypeMs valueOf(DocumentClassTypeMsEnum ecmDocumentClassMsType){
	return Arrays.asList(DocumentClassTypeMs.values())
		.stream()
		.filter(type -> type.getEcmDocumentClass().equals(ecmDocumentClassMsType))
		.findFirst()
		.orElse(null);
    }
    //@formatter:on

}
