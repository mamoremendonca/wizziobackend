package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.exception;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.PurchaseOrderRepositoryProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderRepositoryUniqueConstraintException extends PurchaseOrderRepositoryException {

    private static final long serialVersionUID = 5561319990599008866L;

    public PurchaseOrderRepositoryUniqueConstraintException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
    }

    public static PurchaseOrderRepositoryUniqueConstraintException of(Context ctx, Long number, Long version) {
	return new PurchaseOrderRepositoryUniqueConstraintException(PurchaseOrderRepositoryProperties.UNIQUE_CONSTRAINT_EXCEEPTION_VERSION_NUMBER, ctx, number, version);
    }

    public static PurchaseOrderRepositoryUniqueConstraintException of(Context ctx, Long id) {
	return new PurchaseOrderRepositoryUniqueConstraintException(PurchaseOrderRepositoryProperties.UNIQUE_CONSTRAINT_EXCEEPTION_ID, ctx, id);
    }

}
