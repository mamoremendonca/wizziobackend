package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.exception;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.PurchaseOrderRepositoryProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderRepositoryGetSupplierInfoException extends PurchaseOrderRepositoryException {

    private static final long serialVersionUID = -5517511378640712205L;

    public PurchaseOrderRepositoryGetSupplierInfoException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.INTERNAL_SERVER_ERROR, errorCode, ctx, args);
    }

    public static PurchaseOrderRepositoryGetSupplierInfoException of(Context ctx, Long id, String supplierCode) {
	return new PurchaseOrderRepositoryGetSupplierInfoException(PurchaseOrderRepositoryProperties.GET_SUPPLIER_INFO_EXCEPTION, ctx, id, supplierCode);
    }

}
