package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.exception;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.PurchaseOrderRepositoryProperties;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;

public class PurchaseOrderRepositoryClaimException extends PurchaseOrderRepositoryException {

    private static final long serialVersionUID = -5517511378640712205L;

    public PurchaseOrderRepositoryClaimException(String errorCode, Context ctx, Object... args) {
	super(PlatformCode.PARAMETER_VALIDATION_ERROR, errorCode, ctx, args);
    }

    public static PurchaseOrderRepositoryClaimException of(Context ctx, Long id) {
	return new PurchaseOrderRepositoryClaimException(PurchaseOrderRepositoryProperties.CLAIM_EXCEPTION, ctx, id);
    }

}
