package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums;

public enum ScopeTypeMs {
    MINE, TEAM, ALL;
}
