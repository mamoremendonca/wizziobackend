package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums;

public enum PurchaseOrderTypeMs {
    CAPEX, OPEX, CONTRACT;
}
