package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto;

import javax.xml.bind.annotation.XmlRootElement;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.OrderTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("OrderBy")
public class OrderByMsDTO extends DTO {

	@ApiModelProperty
	public String field;

	@ApiModelProperty
	public OrderTypeMs type;

}
