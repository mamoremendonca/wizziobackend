package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto;

import javax.xml.bind.annotation.XmlRootElement;

import org.osgi.dto.DTO;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.ScopeTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("SearchScope")
public class SearchScopeMsDTO extends DTO {

    @ApiModelProperty
    public String userGroup;
    
    @ApiModelProperty
    public String currentUserGroup;	// Logged user
    @ApiModelProperty
    public String currentUserId;	// Logged user

    @ApiModelProperty
    public ScopeTypeMs scopeType;
}
