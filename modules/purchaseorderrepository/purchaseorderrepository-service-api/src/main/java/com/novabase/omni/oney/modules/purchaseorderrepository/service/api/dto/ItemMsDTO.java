package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("Item")
public class ItemMsDTO {

    @ApiModelProperty(hidden = true)
    public Long id;
    
    @ApiModelProperty
    public int index;

    @ApiModelProperty
    public String code;

    @ApiModelProperty
    public String description;

    @ApiModelProperty
    public String costCenterCode;

    @ApiModelProperty
    public Double amount;

    @ApiModelProperty
    public Double unitPriceWithoutTax;

    @ApiModelProperty
    public String taxCode;

    @ApiModelProperty
    public LocalDateTime startDate;

    @ApiModelProperty
    public LocalDateTime endDate;

    @ApiModelProperty
    public String comment;

}
