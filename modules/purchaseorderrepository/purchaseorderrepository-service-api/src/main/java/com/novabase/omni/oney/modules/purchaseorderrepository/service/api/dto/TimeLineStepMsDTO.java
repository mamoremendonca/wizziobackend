package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto;

import java.time.LocalDateTime;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.TimeLineStepTypeMs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("TimeLineStep")
public class TimeLineStepMsDTO {

    @ApiModelProperty(readOnly = true)
    public int index;

    @ApiModelProperty(readOnly = true)
    public TimeLineStepTypeMs type;

    @ApiModelProperty(readOnly = true)
    public LocalDateTime date;

    @ApiModelProperty(readOnly = true)
    public String comment;

    @ApiModelProperty(readOnly = true)
    public Long userId;

    @ApiModelProperty(readOnly = true)
    public String userGroup;

    @ApiModelProperty(readOnly = true)
    public boolean hasNext;

    @ApiModelProperty(readOnly = true)
    public List<AttachmentMsDTO> attachments;

}
