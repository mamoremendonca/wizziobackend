package com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums;

public enum WorkFlowActionTypeMs {

    //@formatter:off
    APPROVE(TimeLineStepTypeMs.APPROVED), 
    RETURN(TimeLineStepTypeMs.RETURNED), 
    REJECT(TimeLineStepTypeMs.REJECTED), 
    CANCEL(TimeLineStepTypeMs.CANCELED),
    SKIP(TimeLineStepTypeMs.SKIPPED);
    //@formatter:on

    private TimeLineStepTypeMs stepType;

    private WorkFlowActionTypeMs(TimeLineStepTypeMs stepType) {
	this.stepType = stepType;
    }

    public TimeLineStepTypeMs getStepType() {
	return stepType;
    }

}
