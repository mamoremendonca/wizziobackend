package com.novabase.omni.oney.modules.purchaseorderrepository.service.api;

import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;

public abstract class PurchaseOrderRepositoryProperties extends RSProperties {
    public static final String UNEXPECTED_EXCEPTION = "UNEXPECTED_EXCEPTION";
    public static final String UNIQUE_CONSTRAINT_EXCEEPTION_VERSION_NUMBER = "UNIQUE_CONSTRAINT_EXCEEPTION_VERSION_NUMBER";
    public static final String NOT_FOUND_EXCEPTION = "NOT_FOUND_EXCEPTION";
    public static final String UNIQUE_CONSTRAINT_EXCEEPTION_ID = "UNIQUE_CONSTRAINT_EXCEEPTION_ID";
    public static final String GROUP_CANNOT_CLAIM_EXCEPTION = "GROUP_CANNOT_CLAIM_EXCEPTION";
    public static final String CLAIM_EXCEPTION = "CLAIM_EXCEPTION";
    public static final String FIRST_APPROVER_CANNOT_RETURN_EXCEPTION = "FIRST_APPROVER_CANNOT_RETURN_EXCEPTION";
    public static final String SUBMIT_ECM_EXCEPTION = "SUBMIT_ECM_EXCEPTION";
    public static final String GET_SUPPLIER_INFO_EXCEPTION = "GET_SUPPLIER_INFO_EXCEPTION";
    public static final String JOB_REST_SERVICE_EXCEPTION = "JOB_REST_SERVICE_EXCEPTION";
    
}
