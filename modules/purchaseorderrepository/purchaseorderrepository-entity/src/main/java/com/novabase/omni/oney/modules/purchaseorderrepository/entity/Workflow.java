package com.novabase.omni.oney.modules.purchaseorderrepository.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import net.karneim.pojobuilder.GeneratePojoBuilder;

//@formatter:off
@Entity
@Table(name = "workflow", 
	uniqueConstraints = { 
		@UniqueConstraint(columnNames = "id", name = "seq_workflow_id"),
	}
)
@GeneratePojoBuilder
@NamedQueries(
	@NamedQuery(
		name = Workflow.NAMED_QUERY_FIND_WORKFLOW_BY_PURCHASE_ORDER_ID, 
		query = "Select workflow from PurchaseOrder purchaseOrder "
			+ "join purchaseOrder.workflow workflow "
			+ "join fetch workflow.steps "
			+ "where purchaseOrder.id = :purchaseOrderId")
	)
//@formatter:on
public class Workflow {

    public static final String NAMED_QUERY_FIND_WORKFLOW_BY_PURCHASE_ORDER_ID = "Workflow.findByPurchaseOrderId";

    @Id
    @SequenceGenerator(name = "workflow_seq_task", sequenceName = "seq_workflow_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "workflow_seq_task")
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @Column(name = "current_index")
    private int currentIndex; //TODO: mudar para currentIdx para ficar coerente com os steps LEMBRAR DOS MAPPERS 

    @Column(name = "current_user_group")
    private String currentUserGroup;

    @Column(name = "limit_date_to_next_step")
    private LocalDateTime limitDateToNextStep;

    @Column(name = "has_next_step")
    private boolean hasNextStep;

    @OrderBy("idx")
    @OneToMany(mappedBy = "workflow", fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<WorkflowStep> steps;

    public Workflow() {
	super();
    }

    public Workflow(Long id, LocalDateTime creationDate, int currentIndex, String currentUserGroup, LocalDateTime limitDateToNextStep, boolean hasNextStep, Set<WorkflowStep> steps) {
	super();
	this.id = id;
	this.creationDate = creationDate;
	this.currentIndex = currentIndex;
	this.currentUserGroup = currentUserGroup;
	this.limitDateToNextStep = limitDateToNextStep;
	this.hasNextStep = hasNextStep;
	this.steps = steps;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public LocalDateTime getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
	this.creationDate = creationDate;
    }

    public int getCurrentIndex() {
	return currentIndex;
    }

    public void setCurrentIndex(int currentIndex) {
	this.currentIndex = currentIndex;
    }

    public String getCurrentUserGroup() {
	return currentUserGroup;
    }

    public void setCurrentUserGroup(String currentUserGroup) {
	this.currentUserGroup = currentUserGroup;
    }

    public LocalDateTime getLimitDateToNextStep() {
	return limitDateToNextStep;
    }

    public void setLimitDateToNextStep(LocalDateTime limitDateToNextStep) {
	this.limitDateToNextStep = limitDateToNextStep;
    }

    public boolean isHasNextStep() {
	return hasNextStep;
    }

    public void setHasNextStep(boolean hasNextStep) {
	this.hasNextStep = hasNextStep;
    }

    public Set<WorkflowStep> getSteps() {
	return steps;
    }

    public void setSteps(Set<WorkflowStep> steps) {
	this.steps = steps;
    }

    public WorkflowStep getWorkflowStepByIndex(int idx) {
	//@formatter:off
	return getSteps().stream()
			.filter(step -> step.getIdx() == idx)
			.findFirst()
			.orElse(null);
	//@formatter:on
    }

    @Override
    public String toString() {
	return "Workflow [id=" + id + ", currentUserGroup=" + currentUserGroup + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((currentUserGroup == null) ? 0 : currentUserGroup.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Workflow other = (Workflow) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }
}
