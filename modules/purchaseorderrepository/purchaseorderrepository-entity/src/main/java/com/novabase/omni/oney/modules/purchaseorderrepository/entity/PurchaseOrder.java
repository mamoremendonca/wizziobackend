package com.novabase.omni.oney.modules.purchaseorderrepository.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang3.StringUtils;

import net.karneim.pojobuilder.GeneratePojoBuilder;

//@formatter:off
@Entity
@Table(name = "purchase_order", 
	uniqueConstraints = { 
		@UniqueConstraint(columnNames = "id", name = "purchase_order_pk"), 
		@UniqueConstraint(columnNames = {"number", "version"}, name = PurchaseOrder.PURCHASE_ORDER_UNIQUE_VERSION_AND_NUMBER) 
	}
)
@GeneratePojoBuilder
@NamedQueries({
	@NamedQuery(
		name = PurchaseOrder.NAMED_QUERY_FIND_PURCHASEORDER_FETHING_ALL, 
		query = "Select purchaseOrder from PurchaseOrder purchaseOrder "
			+ "left join fetch purchaseOrder.workflow workflow "
			+ "left join fetch workflow.steps "
			+ "left join fetch purchaseOrder.items "
			+ "left join fetch purchaseOrder.histories history "
			+ "left join fetch history.attachments "
			+ "left join fetch history.workflowStep "
			+ "where purchaseOrder.id = :purchaseOrderId "),
	@NamedQuery(
		name = PurchaseOrder.NAMED_QUERY_GET_MAX_PURCHASEORDER_NUMBER,
		query = "Select max(purchaseOrder.number) from PurchaseOrder purchaseOrder"),
	@NamedQuery(
		name = PurchaseOrder.NAMED_QUERY_FIND_PURCHASEORDER_IDS_TO_SUBMIT,
		query = "Select distinct po.id from PurchaseOrder po where po.id in "
			+ "(Select purchaseOrder.id "
			+ "from PurchaseOrder purchaseOrder "
			+ "join purchaseOrder.histories history "
			+ "join history.attachments attachment "
			+ "where "
			+ "purchaseOrder.status = 'APPROVED' "
			+ "and attachment.externalId is NULL "
			+ ") or po.id in "
			+ "(Select purchaseOrder.id "
			+ "from PurchaseOrder purchaseOrder "
			+ "where "
			+ "purchaseOrder.status = 'APPROVED' "
			+ "and purchaseOrder.type in :poTypes) "),
	@NamedQuery(
		name = PurchaseOrder.NAMED_QUERY_FIND_APPROVED_PURCHASEORDER_BY_NUMBER, 
		query = "Select purchaseOrder from PurchaseOrder purchaseOrder "
			+ "left join fetch purchaseOrder.items "
			+" where (purchaseOrder.status = 'APPROVED' or purchaseOrder.status = 'SAP_SUBMITTED') "
			+ "and purchaseOrder.number = :purchaseOrderNumber "),
	@NamedQuery(
		name = PurchaseOrder.NAMED_QUERY_FIND_PURCHASE_ORDER_ID_BY_INSTANCE_ID, 
		query = "Select purchaseOrder.id from PurchaseOrder purchaseOrder "
			+ "where purchaseOrder.instanceId = :instanceId ")
	})
//@formatter:on
public class PurchaseOrder {

    public static final String PURCHASE_ORDER_UNIQUE_VERSION_AND_NUMBER = "purchase_order_unique_version_and_number";
    public static final String NAMED_QUERY_FIND_PURCHASEORDER_FETHING_ALL = "PurchaseOrder.findHistoryByPurchaseOrderIdOrderById";
    public static final String NAMED_QUERY_GET_MAX_PURCHASEORDER_NUMBER = "PurchaseOrder.getMaxPurchaseOrderNumber";
    public static final String NAMED_QUERY_FIND_PURCHASEORDER_IDS_TO_SUBMIT = "PurchaseOrder.findPurchaseOrderIdsToSubmit";
    public static final String NAMED_QUERY_FIND_APPROVED_PURCHASEORDER_BY_NUMBER = "PurchaseOrder.findApprovedPurchaseOrderByPurchaseOrderNumber";
    public static final String NAMED_QUERY_FIND_PURCHASE_ORDER_ID_BY_INSTANCE_ID = "PurchaseOrder.findPurchaseOrderIdByInstanceId";

    @Id
    @SequenceGenerator(name = "purchase_order_seq_task", sequenceName = "seq_purchase_order_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "purchase_order_seq_task")
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;
    
    @Column(name = "instance_id")
    private Long instanceId; // Instance ID that origin this financial document

    @Column(name = "number", updatable = false, nullable = false)
    private Long number;

    @Column(name = "version", updatable = false, nullable = false)
    private Long version;

    // @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private String status; // TODO: criar modulo partilhado entre apps e MS que contenha enuns e
			   // Exceptions, depois alterar esse atributo para enum

    // @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private String type; // TODO: criar modulo partilhado entre apps e MS que contenha enuns e
			 // Exceptions, depois alterar esse atributo para enum

    @Column(name = "supplier_code")
    private String supplierCode;

    @Column(name = "description")
    private String description;

    @Column(name = "department_code")
    private String departmentCode;

    @Column(name = "project_code")
    private String projectCode;

    @Column(name = "internal_order_code")
    private String internalOrderCode;

    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @Column(name = "expiration_date")
    private LocalDateTime expirationDate;

    @Column(name = "approval_date") //TODO: change to finish date
    private LocalDateTime approvalDate; //TODO: change to finish date

    @Column(name = "total_without_tax")
    private Double totalWithoutTax;

    @Column(name = "user_group")
    private String userGroup;

    @Column(name = "user_id")
    private Long userId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "workflow_id")
    private Workflow workflow;

    @OrderBy("index")
    @OneToMany(mappedBy = "purchaseOrder", fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Item> items;

    @OrderBy("date")
    @OneToMany(mappedBy = "purchaseOrder", fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<History> histories;
    
    public PurchaseOrder() {
	super();
    }

    public PurchaseOrder(Long id, Long instanceId, Long number, Long version, String status, String type, String supplierCode, String description, String departmentCode, String projectCode, String internalOrderCode,
	    LocalDateTime creationDate, LocalDateTime expirationDate, LocalDateTime approvalDate, Double totalWithoutTax, String userGroup, Long userId, Workflow workflow, Set<Item> items,
	    Set<History> histories) {
	super();
	this.id = id;
	this.instanceId = instanceId;
	this.number = number;
	this.version = version;
	this.status = status;
	this.type = type;
	this.supplierCode = supplierCode;
	this.description = description;
	this.departmentCode = departmentCode;
	this.projectCode = projectCode;
	this.internalOrderCode = internalOrderCode;
	this.creationDate = creationDate;
	this.expirationDate = expirationDate;
	this.approvalDate = approvalDate;
	this.totalWithoutTax = totalWithoutTax;
	this.userGroup = userGroup;
	this.userId = userId;
	this.workflow = workflow;
	this.items = items;
	this.histories = histories;
    }

    public Long getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(Long instanceId) {
        this.instanceId = instanceId;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Long getNumber() {
	return number;
    }

    public void setNumber(Long number) {
	this.number = number;
    }

    public Long getVersion() {
	return version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public String getSupplierCode() {
	return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
	this.supplierCode = supplierCode;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getDepartmentCode() {
	return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
	this.departmentCode = departmentCode;
    }

    public String getProjectCode() {
	return projectCode;
    }

    public void setProjectCode(String projectCode) {
	this.projectCode = projectCode;
    }

    public String getInternalOrderCode() {
	return internalOrderCode;
    }

    public void setInternalOrderCode(String internalOrderCode) {
	this.internalOrderCode = internalOrderCode;
    }

    public LocalDateTime getCreationDate() {
	return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
	this.creationDate = creationDate;
    }

    public LocalDateTime getExpirationDate() {
	return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
	this.expirationDate = expirationDate;
    }

    public String getUserGroup() {
	return userGroup;
    }

    public void setUserGroup(String userGroup) {
	this.userGroup = userGroup;
    }

    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    public Workflow getWorkflow() {
	return workflow;
    }

    public void setWorkflow(Workflow workflow) {
	this.workflow = workflow;
    }

    public Set<Item> getItems() {
	return items;
    }

    public void setItems(Set<Item> items) {
	this.items = items;
    }

    public Set<History> getHistories() {
	return histories;
    }

    public void setHistories(Set<History> histories) {
	this.histories = histories;
    }

    public Double getTotalWithoutTax() {
	return totalWithoutTax;
    }

    public void setTotalWithoutTax(Double totalWithoutTax) {
	this.totalWithoutTax = totalWithoutTax;
    }

    public LocalDateTime getApprovalDate() {
	return approvalDate;
    }

    public void setApprovalDate(LocalDateTime approvalDate) {
	this.approvalDate = approvalDate;
    }

    public String getFriendlyNumber() {
	return "PO" + this.getNumberLeftPad();
    }
    
    public String getNumberLeftPad() {
	return StringUtils.leftPad(number.toString(), 5, "0");
    }

    @Override
    public String toString() {
	return "PurchaseOrder [id=" + id + ", type=" + type + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((type == null) ? 0 : type.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	PurchaseOrder other = (PurchaseOrder) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}
