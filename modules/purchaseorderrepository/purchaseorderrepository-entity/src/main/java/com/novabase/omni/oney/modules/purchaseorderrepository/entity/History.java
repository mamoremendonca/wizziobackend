package com.novabase.omni.oney.modules.purchaseorderrepository.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import net.karneim.pojobuilder.GeneratePojoBuilder;

//@formatter:off
@Entity
@Table(name = "history", 
	uniqueConstraints = { 
		@UniqueConstraint(columnNames = "id", name = "seq_history_id") 
	}
)
@GeneratePojoBuilder
@NamedQueries(
	@NamedQuery(
		name = History.NAMED_QUERY_FIND_HISTORY, 
		query = "Select distinct history from History history "
			+ "left join fetch history.attachments "
			+ "left join fetch history.workflowStep "
			+ "where history.purchaseOrder.id = :purchaseOrderId "
			+ "order by history.id "))
//@formatter:on
public class History {

    public static final String NAMED_QUERY_FIND_HISTORY = "History.findHistoryByPurchaseOrderIdOrderById";

    @Id
    @SequenceGenerator(name = "history_seq_task", sequenceName = "seq_history_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "history_seq_task")
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "purchase_order_id")
    private PurchaseOrder purchaseOrder;

    @ManyToOne
    @JoinColumn(name = "workflow_step_id")
    private WorkflowStep workflowStep;

    @Column(name = "date")
    private LocalDateTime date;

    @Column(name = "type")
    private String type; // TODO: criar modulo partilhado entre apps e MS que contenha enuns e
			 // Exceptions, depois alterar esse atributo para enum

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "comment")
    private String comment;

    @Column(name = "user_group")
    private String userGroup;

    @OrderBy("idx")
    @OneToMany(mappedBy = "history", fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Attachment> attachments;

    public History() {
	super();
    }

    public History(Long id, PurchaseOrder purchaseOrder, WorkflowStep workflowStep, LocalDateTime date, String type, Long userId, String comment, String userGroup, Set<Attachment> attachments) {
	super();
	this.id = id;
	this.purchaseOrder = purchaseOrder;
	this.workflowStep = workflowStep;
	this.date = date;
	this.type = type;
	this.userId = userId;
	this.comment = comment;
	this.userGroup = userGroup;
	this.attachments = attachments;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public PurchaseOrder getPurchaseOrder() {
	return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
	this.purchaseOrder = purchaseOrder;
    }

    public WorkflowStep getWorkflowStep() {
	return workflowStep;
    }

    public void setWorkflowStep(WorkflowStep workflowStep) {
	this.workflowStep = workflowStep;
    }

    public LocalDateTime getDate() {
	return date;
    }

    public void setDate(LocalDateTime date) {
	this.date = date;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public Long getUserId() {
	return userId;
    }

    public void setUserId(Long userId) {
	this.userId = userId;
    }

    public String getComment() {
	return comment;
    }

    public void setComment(String comment) {
	this.comment = comment;
    }

    public String getUserGroup() {
	return userGroup;
    }

    public void setUserGroup(String userGroup) {
	this.userGroup = userGroup;
    }

    public Set<Attachment> getAttachments() {
	return attachments;
    }

    public void setAttachments(Set<Attachment> attachments) {
	this.attachments = attachments;
    }

    @Override
    public String toString() {
	return "History [id=" + id + ", type=" + type + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((type == null) ? 0 : type.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	History other = (History) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }
}
