package com.novabase.omni.oney.modules.purchaseorderrepository.entity;

import java.time.LocalDate;

import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
public class SearchCriteria {

    public String status;

    public Long number;

    public String type;

    public String entityCode;

    public String internalOrderCode;

    public String projectCode;
    
    public String departmentCode;
    
    public SearchScope scope;
    
    public Long minVATValue;
    public Long maxVATValue;

    public LocalDate createdDate;
    public LocalDate approvalDate;

    public String orderField;
    public String orderType;
    
}
