package com.novabase.omni.oney.modules.purchaseorderrepository.entity;

import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
public class SearchScope {

    public String userGroup;			// Filter
    public String currentUserGroup;		// Logged user
    public String currentUserId;		// Logged user
    public String scopeType;
}
