package com.novabase.omni.oney.modules.purchaseorderrepository.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import net.karneim.pojobuilder.GeneratePojoBuilder;

//@formatter:off
@Entity
@Table(name = "workflow_step", 
	uniqueConstraints = { 
		@UniqueConstraint(columnNames = "id", name = "seq_workflow_step_id"),
	}
)
@GeneratePojoBuilder
//@formatter:on
public class WorkflowStep {

    @Id
    @SequenceGenerator(name = "workflow_step_seq_ask", sequenceName = "seq_workflow_step_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "workflow_step_seq_ask")
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name = "idx")
    private int idx;

    @ManyToOne
    @JoinColumn(name = "workflow_id")
    private Workflow workflow;

    @Column(name = "user_group")
    private String userGroup;

    @Column(name = "has_next_step")
    private boolean hasNextStep;

    @Column(name = "active")
    private boolean active;

    public WorkflowStep() {
	super();
    }

    public WorkflowStep(Long id, int idx, Workflow workflow, String userGroup, boolean hasNextStep, boolean active) {
	super();
	this.id = id;
	this.idx = idx;
	this.workflow = workflow;
	this.userGroup = userGroup;
	this.hasNextStep = hasNextStep;
	this.active = active;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public int getIdx() {
	return idx;
    }

    public void setIdx(int idx) {
	this.idx = idx;
    }

    public Workflow getWorkflow() {
	return workflow;
    }

    public void setWorkflow(Workflow workflow) {
	this.workflow = workflow;
    }

    public String getUserGroup() {
	return userGroup;
    }

    public void setUserGroup(String userGroup) {
	this.userGroup = userGroup;
    }

    public boolean isHasNextStep() {
	return hasNextStep;
    }

    public void setHasNextStep(boolean hasNextStep) {
	this.hasNextStep = hasNextStep;
    }

    public boolean isActive() {
	return active;
    }

    public void setActive(boolean active) {
	this.active = active;
    }

    @Override
    public String toString() {
	return "WorkflowStep [id=" + id + ", userGroup=" + userGroup + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	result = prime * result + ((userGroup == null) ? 0 : userGroup.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	WorkflowStep other = (WorkflowStep) obj;
	if (idx == 0) {
	    if (other.idx != 0)
		return false;
	} else if (idx != other.idx)
	    return false;
	return true;
    }
}
