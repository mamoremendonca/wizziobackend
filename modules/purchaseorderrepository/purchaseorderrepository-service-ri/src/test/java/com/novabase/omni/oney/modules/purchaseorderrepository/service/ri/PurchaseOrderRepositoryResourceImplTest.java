package com.novabase.omni.oney.modules.purchaseorderrepository.service.ri;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;

import com.novabase.omni.oney.modules.purchaseorderrepository.entity.PurchaseOrder;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.manager.PurchaseOrderRepositoryDataManager;

public class PurchaseOrderRepositoryResourceImplTest {

    public static final String DEFAULT_PURCHASE_ORDER_FRIENDLY_NUMBER = "PO00001";
    public static final String DEFAULT_PURCHASE_ORDER_NUMBER_STRING = "1";
    public static final Long DEFAULT_PURCHASE_ORDER_NUMBER_LONG = Long.parseLong(DEFAULT_PURCHASE_ORDER_NUMBER_STRING);
    
    @InjectMocks
    private PurchaseOrderRepositoryResourceImpl purchaseOrderRepositoryResourceImpl;
    
    @Mock
    private PurchaseOrderRepositoryDataManager purchaseOrderRepositoryDataManager;
    
//    @Mock
//    private PurchaseOrderRepositoryContext purchaseOrderRepositoryContext;
    
    private PurchaseOrder defaultPurchaeOrder;
    
    @Before
    public void setUp() {
	
	MockitoAnnotations.initMocks(this);
	
	
    }
    
    
    @Test
    public void getPurchaseOrderFriendlyNumberTest() throws Exception {
	
//	this.getSaveBlobMockitoBehavior(ArgumentMatchers.anyLong()).thenReturn(null);
//	this.purchaseOrderRepositoryResourceImpl.getPurchaseOrder(DEFAULT_PURCHASE_ORDER_FRIENDLY_NUMBER);
//	
//	Mockito.verify(this.purchaseOrderRepositoryDataManager).findAppprovedPurchaseOrderByNumber(DEFAULT_PURCHASE_ORDER_NUMBER_LONG);
	
//	TODO: Testing createProcess requires Mock of the JourneyContext to be able to call JourneySession.getInstance(this) there. Review with Wizzio Team.
	
	Assert.assertTrue(Boolean.TRUE);
	
    }
    
    private <T> OngoingStubbing<PurchaseOrder> getSaveBlobMockitoBehavior(final Long number) throws Exception {
	return Mockito.when(this.purchaseOrderRepositoryDataManager.findAppprovedPurchaseOrderByNumber(number));
    }
    
}
