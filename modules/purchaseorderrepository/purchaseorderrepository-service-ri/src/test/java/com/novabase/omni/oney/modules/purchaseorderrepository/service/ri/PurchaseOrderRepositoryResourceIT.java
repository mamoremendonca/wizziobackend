package com.novabase.omni.oney.modules.purchaseorderrepository.service.ri;

import static io.digitaljourney.platform.modules.paxexam.BundleOptions.mavenBundles;
import static io.digitaljourney.platform.modules.paxexam.BundleOptions.rsProviderWithJPAClient;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.newConfiguration;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.newConsumerConfiguration;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.rsProviderWithJPAClientConfiguration;
import static org.ops4j.pax.exam.CoreOptions.composite;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;

import com.novabase.omni.oney.modules.purchaseorderrepository.data.ri.rdb.purchaseorder.PurchaseOrderDAOConfig;

import io.digitaljourney.platform.modules.paxexam.base.BaseTestSupport;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class PurchaseOrderRepositoryResourceIT extends BaseTestSupport {

    @Override
    protected Option bundles() {
	return composite(super.bundles(), rsProviderWithJPAClient(),
		mavenBundles("com.novabase.omni.oney.modules", "purchaseorderrepository-entity", "purchaseorderrepository-data-ri", "purchaseorderrepository-service-api"),
		testBundle("com.novabase.omni.oney.modules", "purchaseorderrepository-service-ri"));
    }

    @Override
    protected Option configurations() {
	return composite(super.configurations(), rsProviderWithJPAClientConfiguration("purchaseorderrepository", newConfiguration(PurchaseOrderRepositoryResourceConfig.CPID)),
		newConsumerConfiguration(PurchaseOrderDAOConfig.CPID, "purchaseorderrepository").asOption());
    }

    @Test
    public void testBundle() {
	assertBundleActive("com.novabase.omni.oney.modules.purchaseorderrepository-service-ri");
    }

}
