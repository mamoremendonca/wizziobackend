package com.novabase.omni.oney.modules.purchaseorderrepository.service.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;

import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.PurchaseOrderDAO;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.PurchaseOrder;


public class PurchaseOrderRepositoryDataManagerTest {

    public static final String DEFAULT_PURCHASE_ORDER_FRIENDLY_NUMBER = "PO00001";
    public static final String DEFAULT_PURCHASE_ORDER_NUMBER_STRING = "1";
    public static final Long DEFAULT_PURCHASE_ORDER_NUMBER_LONG = Long.parseLong(DEFAULT_PURCHASE_ORDER_NUMBER_STRING);
    
    @InjectMocks
    private PurchaseOrderRepositoryDataManager purchaseOrderRepositoryDataManager;
    
    @Mock
    private PurchaseOrderDAO purchaseOrderDAO;
    
    private PurchaseOrder defaultPurchaseOrder;
    
    @Before
    public void setUp() {
	
	MockitoAnnotations.initMocks(this);
	
//	this.defaultPurchaseOrder = new PurchaseOrder();
//	this.defaultPurchaseOrder.setId(1L);
//	
//	final Item item1 = new Item();
//	item1.setIndex(1);
//	final Item item2 = new Item();
//	item2.setIndex(2);
//	final Set<Item> items = new HashSet<Item>();
//	items.add(item1);
//	items.add(item2);
//	
//	this.defaultPurchaseOrder.setItems(items);
//	this.defaultPurchaseOrder.setWorkflow(new Workflow());
	
    }
    
    @Test
    public void findAppprovedPurchaseOrderByNumberTest() throws Exception {
	
	this.getApprovedPurchaseOrderByNumberMockitoBehavior(DEFAULT_PURCHASE_ORDER_NUMBER_LONG).thenReturn(this.defaultPurchaseOrder);
	
	final PurchaseOrder resultPurchaseOrder = this.purchaseOrderRepositoryDataManager.findAppprovedPurchaseOrderByNumber(DEFAULT_PURCHASE_ORDER_NUMBER_LONG);
	
	Assert.assertEquals(resultPurchaseOrder, this.defaultPurchaseOrder);
	
    }
    
    private <T> OngoingStubbing<PurchaseOrder> getApprovedPurchaseOrderByNumberMockitoBehavior(final Long number) throws Exception {
	return Mockito.when(this.purchaseOrderDAO.getApprovedPurchaseOrderByNumber(number));
    }
    
}
