package com.novabase.omni.oney.modules.purchaseorderrepository.service.mapper;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.novabase.omni.oney.modules.purchaseorderrepository.entity.History;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.Item;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.PurchaseOrder;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.Workflow;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.PurchaseOrderMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.ri.PurchaseOrderRepositoryResourceMapper;

public class PurchaseOrderRepositoryResourceMapperTest {

    
    private PurchaseOrder defaultPurchaseOrder;
    
    @Before
    public void setUp() {
	
	this.defaultPurchaseOrder = new PurchaseOrder();
	this.defaultPurchaseOrder.setId(1L);
	this.defaultPurchaseOrder.setNumber(1L);
	this.defaultPurchaseOrder.setVersion(1L);
	this.defaultPurchaseOrder.setStatus("APPROVED");
	this.defaultPurchaseOrder.setType("CAPEX");
	this.defaultPurchaseOrder.setSupplierCode("SupplierCode1");
	this.defaultPurchaseOrder.setDescription("PurchseOrderDescription1");
	this.defaultPurchaseOrder.setDepartmentCode("DepartmentCode1");
	this.defaultPurchaseOrder.setProjectCode("ProjectCode1");
	this.defaultPurchaseOrder.setInternalOrderCode("InternalCode1");
	this.defaultPurchaseOrder.setCreationDate(LocalDateTime.now());
	this.defaultPurchaseOrder.setExpirationDate(LocalDateTime.now());
	this.defaultPurchaseOrder.setApprovalDate(LocalDateTime.now());
	this.defaultPurchaseOrder.setTotalWithoutTax(1d);
	this.defaultPurchaseOrder.setUserGroup("UserGroup1");
	this.defaultPurchaseOrder.setUserId(1L);
	
	this.defaultPurchaseOrder.setHistories(new HashSet<History>());
	
	final Item item1 = new Item();
	item1.setIndex(1);
	final Item item2 = new Item();
	item2.setIndex(2);
	final Set<Item> items = new HashSet<Item>();
	items.add(item1);
	items.add(item2);
	
	this.defaultPurchaseOrder.setItems(items);
	this.defaultPurchaseOrder.setWorkflow(new Workflow());

    }
    
    @Test
    public void toPurchaseOrderMsDTOWithItemsNullTest() {
	
	final PurchaseOrderMsDTO purchaseOrderMsDTO = PurchaseOrderRepositoryResourceMapper.INSTANCE.toPurchaseOrderMsDTOWithItems(null);
	
	Assert.assertNull(purchaseOrderMsDTO);
	
    }
    
    @Test
    public void toPurchaseOrderMsDTOWithItemsTest() {
	
	final PurchaseOrderMsDTO purchaseOrderMsDTO = PurchaseOrderRepositoryResourceMapper.INSTANCE.toPurchaseOrderMsDTOWithItems(this.defaultPurchaseOrder);
	
	Assert.assertEquals(purchaseOrderMsDTO.id.longValue(), this.defaultPurchaseOrder.getId().longValue());
	Assert.assertEquals(purchaseOrderMsDTO.number.longValue(), this.defaultPurchaseOrder.getNumber().longValue());
	Assert.assertEquals(purchaseOrderMsDTO.version.longValue(), this.defaultPurchaseOrder.getVersion().longValue());
	Assert.assertEquals(purchaseOrderMsDTO.status.name(), this.defaultPurchaseOrder.getStatus());
	Assert.assertEquals(purchaseOrderMsDTO.type.name(), this.defaultPurchaseOrder.getType());
	Assert.assertEquals(purchaseOrderMsDTO.supplierCode, this.defaultPurchaseOrder.getSupplierCode());
	Assert.assertEquals(purchaseOrderMsDTO.description, this.defaultPurchaseOrder.getDescription());
	Assert.assertEquals(purchaseOrderMsDTO.departmentCode, this.defaultPurchaseOrder.getDepartmentCode());
	Assert.assertEquals(purchaseOrderMsDTO.projectCode, this.defaultPurchaseOrder.getProjectCode());
	Assert.assertEquals(purchaseOrderMsDTO.internalOrderCode, this.defaultPurchaseOrder.getInternalOrderCode());
	Assert.assertEquals(purchaseOrderMsDTO.creationDate, this.defaultPurchaseOrder.getCreationDate());
	Assert.assertEquals(purchaseOrderMsDTO.expirationDate, this.defaultPurchaseOrder.getExpirationDate());
	Assert.assertEquals(purchaseOrderMsDTO.approvalDate, this.defaultPurchaseOrder.getApprovalDate());
	Assert.assertEquals(purchaseOrderMsDTO.totalWithoutTax.longValue(), this.defaultPurchaseOrder.getTotalWithoutTax().longValue());
	Assert.assertEquals(purchaseOrderMsDTO.userGroup, this.defaultPurchaseOrder.getUserGroup());
	Assert.assertEquals(purchaseOrderMsDTO.userId.longValue(), this.defaultPurchaseOrder.getUserId().longValue());
	
	Assert.assertNotNull(purchaseOrderMsDTO.items);
	Assert.assertFalse(purchaseOrderMsDTO.items.isEmpty());
	Assert.assertEquals(purchaseOrderMsDTO.items.size(), 2);
	Assert.assertEquals(purchaseOrderMsDTO.items.get(0).index, 1);
	Assert.assertEquals(purchaseOrderMsDTO.items.get(1).index, 2);
	Assert.assertNull(purchaseOrderMsDTO.histories);
	Assert.assertNull(purchaseOrderMsDTO.workflow);
	
    }
    
    
}
