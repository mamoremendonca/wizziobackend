package com.novabase.omni.oney.modules.purchaseorderrepository.service.ri;

import java.util.List;
import java.util.Set;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.modules.purchaseorderrepository.entity.Attachment;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.History;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.Item;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.PurchaseOrder;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.SearchCriteria;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.Workflow;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.WorkflowStep;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.AttachmentMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.HistoryMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.ItemMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.PurchaseOrderMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.SearchCriteriaMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.WorkflowMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.WorkflowStepMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.DocumentClassTypeMs;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.PurchaseOrderStatusMs;

@Mapper
public abstract class PurchaseOrderRepositoryResourceMapper {
    public static final PurchaseOrderRepositoryResourceMapper INSTANCE = Mappers.getMapper(PurchaseOrderRepositoryResourceMapper.class);

    @Mapping(source = "items", ignore = true, target = "items")
    @Mapping(source = "histories", ignore = true, target = "histories")
    @Mapping(source = "workflow.steps", ignore = true, target = "workflow.steps")
    public abstract PurchaseOrderMsDTO toPurchaseOrderMsDTO(PurchaseOrder purchaseOrder);

    @Mapping(source = "histories", ignore = true, target = "histories")
    @Mapping(source = "workflow", ignore = true, target = "workflow")
    public abstract PurchaseOrderMsDTO toPurchaseOrderMsDTOWithItems(PurchaseOrder purchaseOrder);

    @Mapping(source = "histories", ignore = true, target = "histories")
    @Mapping(source = "workflow", target = "workflow")
    @Mapping(source = "status", target = "workflow.purchaseOrderStatus")
    public abstract PurchaseOrderMsDTO toPurchaseOrderMsDTOWithItemsAndWorkflow(PurchaseOrder purchaseOrder);

    public abstract PurchaseOrder toPurchaseOrder(PurchaseOrderMsDTO purchaseOrder);

    @Mapping(source="code", target="articleCode")
    @Mapping(source="description", target="articleDescription")
    @Mapping(source="item.purchaseOrder.internalOrderCode", target="internalOrderCode")
    public abstract com.novabase.omni.oney.modules.sapservice.service.api.dto.ItemPOMsDTO toSAPServiceItemPOMsDTO(final Item item);
    
    @IterableMapping(elementTargetType = com.novabase.omni.oney.modules.sapservice.service.api.dto.ItemPOMsDTO.class)
    public abstract List<com.novabase.omni.oney.modules.sapservice.service.api.dto.ItemPOMsDTO> toListSAPServiceItemPOMsDTO(final List<Item> item);
    
    @Mapping(expression="java(com.novabase.omni.oney.modules.sapservice.service.api.enums.PurchaseOrderTypeMs.valueOf(purchaseOrder.getType()))", target="type")
    @Mapping(expression="java(com.novabase.omni.oney.modules.sapservice.service.api.enums.PurchaseOrderStatusMs.valueOf(purchaseOrder.getStatus()))", target="status")
    public abstract com.novabase.omni.oney.modules.sapservice.service.api.dto.PurchaseOrderMsDTO toSAPServicePurchaseOrderMsDTO(final PurchaseOrder purchaseOrder);

    @Mapping(target = "purchaseOrderStatus", expression = "java(purchaseOrderStatusMs)")
    public abstract WorkflowMsDTO toWorkflowMsDTO(Workflow workflow, PurchaseOrderStatusMs purchaseOrderStatusMs);

    @Mapping(source = "index", target = "idx")
    public abstract WorkflowStep toWorkflowStep(WorkflowStepMsDTO workflowStepMsDTO);

    @Mapping(source = "idx", target = "index")
    public abstract WorkflowStepMsDTO toWorkflowStepMsDTO(WorkflowStep workflowStep);

    public abstract List<ItemMsDTO> toItemListMsDTO(List<Item> searchPurchaseOrderItems);

    public abstract ItemMsDTO toItemMsDTO(Item item);

    public abstract List<HistoryMsDTO> toHistoryListMsDTO(List<History> searchPurchaseOrderHistory);

    public abstract History toHistory(HistoryMsDTO history);

    public abstract HistoryMsDTO toHistoryMsDTO(History addHistory);

    @Mapping(source = "index", target = "idx")
    public abstract Attachment toAttachment(AttachmentMsDTO attachment);

    @Mapping(source = "idx", target = "index")
    public abstract AttachmentMsDTO toAttachmentMsDTO(Attachment attachment);

    public abstract Set<Attachment> toAttachmentList(List<AttachmentMsDTO> attachment);

    public abstract List<AttachmentMsDTO> toAttachmentListMsDTO(List<Attachment> attachment);

    @Mapping(source = "orderBy.field", target = "orderField")
    @Mapping(source = "orderBy.type", target = "orderType")
    public abstract SearchCriteria toSearchCriteria(SearchCriteriaMsDTO searchCriteria);

    public DocumentClassTypeMs toDocumentClassTypeMs(String documentClassTypeMs) {
	DocumentClassTypeMs enumValue = DocumentClassTypeMs.PURCHASE_ORDER;

	try {
	    enumValue = DocumentClassTypeMs.valueOf(documentClassTypeMs);
	} catch (Exception e) {
	}

	// formatter:off
	return enumValue != null ? enumValue : DocumentClassTypeMs.PURCHASE_ORDER;
	// formatter:on
    }
}
