package com.novabase.omni.oney.modules.purchaseorderrepository.service.ri;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.modules.purchaseorderrepository.entity.History;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.HistoryBuilder;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.Workflow;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.WorkflowStep;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.TimeLineStepMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.WorkflowActionMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.WorkflowMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.PurchaseOrderStatusMs;

@Mapper(uses = PurchaseOrderRepositoryResourceMapper.class)
public abstract class WorkflowResourceMapper {

    public static final WorkflowResourceMapper INSTANCE = Mappers.getMapper(WorkflowResourceMapper.class);

    public WorkflowMsDTO toWorkflow(Workflow workflow, PurchaseOrderStatusMs purchaseOrderStatus) {
	WorkflowMsDTO workflowDTO = toWorkflow(workflow);
	if (workflowDTO != null) {
	    //@formatter:off
	    workflowDTO.purchaseOrderStatus = 
		    purchaseOrderStatus != null 
		    	? purchaseOrderStatus 
		    	: PurchaseOrderStatusMs.IN_APPROVAL;
	    //@formatter:on
	}
	return workflowDTO;
    }

    public abstract WorkflowMsDTO toWorkflow(Workflow workflow);

    public History toHistory(WorkflowActionMsDTO workFlowAction) {

	if (workFlowAction == null) {
	    return null;
	}
	//@formatter:off
	return new HistoryBuilder()
		.withComment(workFlowAction.comment)
		.withDate(LocalDateTime.now())
		.withType(workFlowAction.type.getStepType().name())
		.withUserGroup(workFlowAction.userGroup)
		.withUserId(workFlowAction.userId)
		.withAttachments(PurchaseOrderRepositoryResourceMapper.INSTANCE.toAttachmentList(workFlowAction.attachments))
		.build();
	//@formatter:on
    }

    public List<TimeLineStepMsDTO> toTimeLineStepMsDTOList(Workflow workflow, List<History> histories) {

	//@formatter:off
	histories = 
		histories.stream()
        		.sorted((step1, step2) -> step1.getDate().compareTo(step2.getDate()))
        		.collect(Collectors.toList());
	//@formatter:on

	List<TimeLineStepMsDTO> timeLineSteps = toTimeLineStepMsDTOListFromHistory(histories);

	AtomicInteger atomicIndex = new AtomicInteger(1);

	timeLineSteps.forEach(step -> step.index = atomicIndex.getAndIncrement());

	int currentWorkflowIndex = workflow.getCurrentIndex();

	if (workflow.getCurrentIndex() != 0) { // If the current index is different of 0, the workflow isn't end, so we have to
					       // add the future steps
	    //@formatter:off
	    List<WorkflowStep> steps = 
		    workflow.getSteps()
    	    		.stream()
    			.filter(step -> step.getIdx() >= currentWorkflowIndex && step.isActive())
    			.sorted((step1, step2) -> step1.getIdx() - step2.getIdx())
    			.collect(Collectors.toList());
	    //@formatter:on

	    List<TimeLineStepMsDTO> futureSteps = toTimeLineStepMsDTOListFromStep(steps);

	    if (futureSteps != null && futureSteps.isEmpty() == false) {
		futureSteps.forEach(step -> step.index = atomicIndex.getAndIncrement());
		timeLineSteps.addAll(futureSteps);
	    }
	}

	return timeLineSteps;
    }

    @Mapping(target = "type", expression = "java(com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.TimeLineStepTypeMs.valueOf(history.getType()))")
    @Mapping(target = "hasNext", source = "workflowStep.hasNextStep")
    public abstract TimeLineStepMsDTO toTimeLineStepMsDTO(History history);

    @Mapping(target = "type", expression = "java(com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.TimeLineStepTypeMs.TO_APPROVE)")
    @Mapping(target = "index", source = "idx", ignore = true)
    @Mapping(target = "hasNext", source = "hasNextStep")
    public abstract TimeLineStepMsDTO toTimeLineStepMsDTO(WorkflowStep step);

    public abstract List<TimeLineStepMsDTO> toTimeLineStepMsDTOListFromHistory(List<History> histories);

    public abstract List<TimeLineStepMsDTO> toTimeLineStepMsDTOListFromStep(List<WorkflowStep> steps);

}
