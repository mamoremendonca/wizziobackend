package com.novabase.omni.oney.modules.purchaseorderrepository.service.ri;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.modules.purchaseorderrepository.entity.PurchaseOrder;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.PurchaseOrderRepositoryResource;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.AttachmentMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.PurchaseOrderMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.SearchCriteriaMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.TimeLineStepMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.WorkflowActionMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.WorkflowMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.manager.PurchaseOrderRepositoryDataManager;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.manager.PurchaseOrderRepositorySubmitOneyManager;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.manager.PurchaseOrderRepositoryWorkflowManager;

import io.digitaljourney.platform.modules.security.api.principal.PrincipalDTO;
import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;
import io.digitaljourney.platform.modules.ws.rs.api.resource.AbstractResource;

// @formatter:off
@Component(configurationPid = PurchaseOrderRepositoryResourceConfig.CPID, configurationPolicy = ConfigurationPolicy.REQUIRE, reference = {
		@Reference(name = PurchaseOrderRepositoryResourceProperties.REF_CONTEXT, service = PurchaseOrderRepositoryContext.class, cardinality = ReferenceCardinality.MANDATORY) })
@Designate(ocd = PurchaseOrderRepositoryResourceConfig.class)
@RSProvider(PurchaseOrderRepositoryResourceProperties.ADDRESS)
// @formatter:on
public class PurchaseOrderRepositoryResourceImpl extends AbstractResource<PurchaseOrderRepositoryContext, PurchaseOrderRepositoryResourceConfig> implements PurchaseOrderRepositoryResource {

    @Reference
    private volatile PurchaseOrderRepositorySubmitOneyManager purchaseOrderRepositorySubmitOneyManager;

    @Reference
    private volatile PurchaseOrderRepositoryWorkflowManager purchaseOrderRepositoryWorkflowManager;

    @Reference
    private volatile PurchaseOrderRepositoryDataManager purchaseOrderRepositoryDataManager;

    @Activate
    public void activate(ComponentContext ctx, PurchaseOrderRepositoryResourceConfig config) {
	prepare(ctx, config);
    }

    @Modified
    public void modified(PurchaseOrderRepositoryResourceConfig config) {
	prepare(config);
    }

    @Override
    @RequiresPermissions(PurchaseOrderRepositoryResourceProperties.PERMISSION_READ)
    public PurchaseOrderMsDTO getPurchaseOrder(Long purchaseOrderId, String userGroup) {

	PurchaseOrder purchaseOrder = purchaseOrderRepositoryDataManager.findPurchaseOrder(purchaseOrderId);

	PurchaseOrderMsDTO purchaseOrderDTO = PurchaseOrderRepositoryResourceMapper.INSTANCE.toPurchaseOrderMsDTOWithItemsAndWorkflow(purchaseOrder);

	purchaseOrderDTO.canClaim = purchaseOrderRepositoryWorkflowManager.userGroupCanClaimProcess(purchaseOrder, userGroup);

	return purchaseOrderDTO;
    }

    @Override
    @RequiresPermissions(PurchaseOrderRepositoryResourceProperties.PERMISSION_CREATE)
    public PurchaseOrderMsDTO addPurchaseOrder(PurchaseOrderMsDTO purchaseOrder) {

	return purchaseOrderRepositoryDataManager.addPurchaseOrder(purchaseOrder, getLimitDateToNextStep());
    }

    @Override
    @RequiresPermissions(PurchaseOrderRepositoryResourceProperties.PERMISSION_EXECUTE)
    public WorkflowMsDTO updateWorkflow(Long purchaseOrderId, WorkflowActionMsDTO action) {

	final PrincipalDTO principal = currentPrincipal();
	return this.purchaseOrderRepositoryWorkflowManager.updateWorkflow(purchaseOrderId, action, getLimitDateToNextStep(), new PurchaseOrderRepositoryResourceConfigWrapper(this.getConfig()),
		Long.parseLong(principal.id));
    }

    @Override
    @RequiresPermissions(PurchaseOrderRepositoryResourceProperties.PERMISSION_READ)
    public List<TimeLineStepMsDTO> getTimeLine(Long purchaseOrderId) {

	return purchaseOrderRepositoryDataManager.getTimeLine(purchaseOrderId);
    }

    @Override
    @RequiresPermissions(PurchaseOrderRepositoryResourceProperties.PERMISSION_READ)
    public List<PurchaseOrderMsDTO> search(SearchCriteriaMsDTO searchCriteria) {

	return purchaseOrderRepositoryDataManager.search(searchCriteria);
    }

    @Override
    @RequiresPermissions(PurchaseOrderRepositoryResourceProperties.PERMISSION_EXECUTE)
    public WorkflowMsDTO claimProcess(Long purchaseOrderId, Long userId, String userGroup, WorkflowActionMsDTO workflowAction) {

	return purchaseOrderRepositoryWorkflowManager.claimProcess(purchaseOrderId, userId, userGroup, workflowAction, getLimitDateToNextStep());
    }

    @RequiresPermissions(PurchaseOrderRepositoryResourceProperties.PERMISSION_READ)
    public List<AttachmentMsDTO> getPurchaseOrderAttachments(Long purchaseOrderId) {

	return purchaseOrderRepositoryDataManager.getPurchaseOrderAttachments(purchaseOrderId);
    }

    @Override
    @RequiresPermissions(PurchaseOrderRepositoryResourceProperties.PERMISSION_READ)
    public AttachmentMsDTO getPurchaseOrderAttachment(Long purchaseOrderId, int index) {

	return purchaseOrderRepositoryDataManager.getPurchaseOrderAttachment(purchaseOrderId, index);
    }

    @Override
    @RequiresPermissions(PurchaseOrderRepositoryResourceProperties.PERMISSION_EXECUTE)
    public void runPurchaseOrderSubmitJob() {

	final PrincipalDTO principal = currentPrincipal();
	this.purchaseOrderRepositorySubmitOneyManager.submitPurchaseOrders(new PurchaseOrderRepositoryResourceConfigWrapper(this.getConfig()), Long.parseLong(principal.id));

    }

    private LocalDateTime getLimitDateToNextStep() {
	LocalDateTime date = LocalDateTime.now();
	return date.plusDays(getConfig().maxDays());
    }

    // TODO: mudar o nome para getApprovedPurchaseOrderByNumber
    @Override
    @RequiresPermissions(PurchaseOrderRepositoryResourceProperties.PERMISSION_READ)
    public PurchaseOrderMsDTO getApprovedPurchaseOrderByNumber(final String purchaseOrderNumber) {

	final Long purchaseOrderNumberLong = Long.parseLong(StringUtils.replace(purchaseOrderNumber, "PO", StringUtils.EMPTY));

	final PurchaseOrder purchaseOrder = this.purchaseOrderRepositoryDataManager.findAppprovedPurchaseOrderByNumber(purchaseOrderNumberLong);

	return PurchaseOrderRepositoryResourceMapper.INSTANCE.toPurchaseOrderMsDTOWithItems(purchaseOrder);

    }

}
