package com.novabase.omni.oney.modules.purchaseorderrepository.service.ri;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/purchaseorderrepository", icon = @Icon(resource = "OSGI-INF/icon/resource.png", size = 32))
public @interface PurchaseOrderRepositoryResourceConfig {
    public static final String CPID = "oney.base.modules.purchaseorderrepository.service.ri.purchaseorderrepository";

    /**
     * Max number of days that a group can be with the purchase order.
     * 
     * @return Max Days
     */
    @AttributeDefinition(name = "%maxDays.name", type = AttributeType.INTEGER, description = "%maxDays.description")
    int maxDays() default 7;

    /**
     * PO Types that should be submitted to SAP.
     *
     * @return String with an array of Purchase Order Types
     */
    @AttributeDefinition(name = "%purchaseOrdertypes.name", description = "%purchaseOrdertypes.description", type = AttributeType.STRING)
    String purchaseOrderTypes();
    
    /**
     * PO Types that should be submitted to SAP.
     *
     * @return String with an array of Purchase Order Types
     */
    @AttributeDefinition(name = "%ecmServiceOffline.name", description = "%ecmServiceOffline.description", type = AttributeType.BOOLEAN)
    boolean ecmServiceOffline() default false;
    
    /**
     * PO Types that should be submitted to SAP.
     *
     * @return String with an array of Purchase Order Types
     */
    @AttributeDefinition(name = "%sapServiceOffline.name", description = "%sapServiceOffline.description", type = AttributeType.BOOLEAN)
    boolean sapServiceOffline() default false;
    
}
