package com.novabase.omni.oney.modules.purchaseorderrepository.service.manager;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.ext.multipart.ContentDisposition;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.modules.ecmservice.service.api.ECMServiceResource;
import com.novabase.omni.oney.modules.ecmservice.service.api.smartdocs.dto.PurchaseOrderECMMetadataMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.PurchaseOrderDAO;
import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.exception.UnexpectedPersistenceException;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.Attachment;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.History;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.PurchaseOrder;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.DocumentClassTypeMs;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.PurchaseOrderStatusMs;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.TimeLineStepTypeMs;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.exception.PurchaseOrderRepositoryGetSupplierInfoException;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.ri.PurchaseOrderRepositoryContext;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.ri.PurchaseOrderRepositoryResourceConfigWrapper;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.ri.PurchaseOrderRepositoryResourceMapper;
import com.novabase.omni.oney.modules.referencedata.service.api.ReferenceDataResource;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SearchFilterMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierGeneralDataMsDTO;
import com.novabase.omni.oney.modules.referencedata.service.api.dto.SupplierMsDTO;
import com.novabase.omni.oney.modules.sapservice.service.api.SAPServiceResource;
import com.novabase.omni.oney.modules.sapservice.service.api.dto.PurchaseOrderMsDTO;

import io.digitaljourney.platform.plugins.modules.blob.service.api.BlobResource;

/**
 * This manager is responsible to submit the PurchaseOrder data to Oney services
 * and persist the references
 * 
 * @author NB24879
 *
 */
//@formatter:off
@Component(
//	reference = {
//	@Reference(
//		name = PurchaseOrderRepositoryResourceProperties.REF_CONTEXT,
//		service = PurchaseOrderRepositoryContext.class, 
//		cardinality = ReferenceCardinality.MANDATORY)
//	},
	service = { PurchaseOrderRepositorySubmitOneyManager.class })
//@formatter:on
public class PurchaseOrderRepositorySubmitOneyManager {

    @Reference
    private PurchaseOrderRepositoryContext ctx;

    @Reference
    private PurchaseOrderRepositoryDataManager purchaseOrderRepositoryDataManager;

    @Reference
    private volatile PurchaseOrderDAO purchaseOrderDAO;

    @Reference
    private volatile ECMServiceResource ecmServiceResource;

    @Reference
    private volatile ReferenceDataResource referenceDataResource;

    @Reference
    private volatile BlobResource blobResource;

    @Reference
    private volatile SAPServiceResource sapServiceResource;

    /**
     * Submits the PurchaseOrders that correspond to the types passed on the
     * parameter "purchaseOrderTypes" that are approved to the SAP service and
     * submit every attachment in all PurchaseOrder approved no matter the type to
     * the ECM service if they are not submitted yet.
     * 
     * @param purchaseOrderTypes
     */
    public void submitPurchaseOrders(final PurchaseOrderRepositoryResourceConfigWrapper configWrapper, final Long userId) {

	List<Long> findProcessToSubmit = null;

	try {
	    findProcessToSubmit = new ArrayList<Long>(this.purchaseOrderDAO.findProcessToSubmit(configWrapper.getPurchaseOrderTypesToSubmitSAP()));
	} catch (UnexpectedPersistenceException e) {
	    throw this.ctx.exception(e.getMessage());
	}

	if (findProcessToSubmit != null && !findProcessToSubmit.isEmpty()) {
	    this.asyncSubmitToExternalServices(findProcessToSubmit, configWrapper, userId);
	}
    }

    /**
     * Start a {@link Runnable} to submit the PurchaseOrders that the Id is in the
     * list purchaseOrderIds
     * 
     * @param purchaseOrderIds
     */
    public void asyncSubmitToExternalServices(final List<Long> purchaseOrderIds, final PurchaseOrderRepositoryResourceConfigWrapper configWrapper, final Long userId) {

	Runnable submitTask = () -> {

	    purchaseOrderIds.stream().forEach(purchaseOrderId -> {
		try {
		    this.ctx.logInfo("[SubmitPurchaseOrderThread] Going to submit PurchaseOrder: " + purchaseOrderId + " to Oney Services");
		    this.submitToExternalServices(purchaseOrderId, configWrapper, userId);
		} catch (Exception e) {
		    this.ctx.logError("[SubmitPurchaseOrderThread] error trying to submitToExternalServices the purcharseOrderId: " + purchaseOrderId + " " + e);
		}
	    });

	};

	String purchaseOrderIdsString = purchaseOrderIds.stream().map(id -> id.toString()).collect(Collectors.joining(","));

	this.ctx.logInfo("[SubmitPurchaseOrderThread] Creating thread to submit PurchaseOrder: " + purchaseOrderIdsString);
	Thread submitThread = new Thread(submitTask);
	this.ctx.logInfo("[SubmitPurchaseOrderThread] Starting thread to submit PurchaseOrder: " + purchaseOrderIdsString);
	submitThread.start();
	this.ctx.logInfo("[SubmitPurchaseOrderThread] Thread to submit PurchaseOrder: " + purchaseOrderIdsString + " is started");

    }

    private void submitToExternalServices(final Long purchaseOrderId, final PurchaseOrderRepositoryResourceConfigWrapper configWrapper, final Long userId) {

	final PurchaseOrder purchaseOrder = this.purchaseOrderRepositoryDataManager.findPurchaseOrder(purchaseOrderId);

	if (!configWrapper.getEcmServiceOffline()) {
	    this.submitAttachmentsToECM(purchaseOrder);
	} else {
	    this.ctx.logInfo("Will not send Attachments to ECM Service because Configuration is set to offLine...");
	}

	if (!configWrapper.getSapServiceOffline()) {
	    this.submitPurchaseOrderToSAP(purchaseOrder, configWrapper, userId);
	} else {
	    this.ctx.logInfo("Will not send Purchase Order to SAP Service because Configuration is set to offLine...");
	}

    }

    private void submitPurchaseOrderToSAP(final PurchaseOrder purchaseOrder, final PurchaseOrderRepositoryResourceConfigWrapper configWrapper, final Long userId) {

	// TODO:submit to sap the purchaseOrders that purchaseOrderTypes contains the
	// type
	if (configWrapper.getPurchaseOrderTypesToSubmitSAP().contains(purchaseOrder.getType())) {

	    // Send to SAP
	    final PurchaseOrderMsDTO sapServicePurchaseOrder = PurchaseOrderRepositoryResourceMapper.INSTANCE.toSAPServicePurchaseOrderMsDTO(purchaseOrder);
	    final SupplierGeneralDataMsDTO supplierGeneralData = this.referenceDataResource.getSupplierMarketInfo(purchaseOrder.getSupplierCode());
	    if (StringUtils.isBlank(supplierGeneralData.nif)) {
		throw this.ctx.exception("Error Getting the supplier information to SAP submit...");
	    }

	    sapServicePurchaseOrder.supplierNIF = "PT" + supplierGeneralData.nif; // TODO: colocar o prefixo "PT" numa config

	    Boolean sucessSubmitted = this.sapServiceResource.submitPurchaseOrder(sapServicePurchaseOrder);
	    if (sucessSubmitted.booleanValue()) {

		try {

		    // update status
		    this.purchaseOrderDAO.updatePurchaseOrderStatus(purchaseOrder, PurchaseOrderStatusMs.SAP_SUBMITTED.name());

		    // update history
		    final History history = new History();
		    history.setPurchaseOrder(purchaseOrder);
		    history.setType(TimeLineStepTypeMs.SAP_SUBMITTED.name());
		    history.setUserId(userId);
		    history.setDate(LocalDateTime.now());

		    this.purchaseOrderDAO.addHistory(history);

		} catch (UnexpectedPersistenceException e) {
		    throw ctx.exception(e.getMessage());
		}
		
	    } else {
		throw ctx.exception("[SubmitPurchaseOrderThread] Failed to submit Purchase Order to SAP: ID: " + purchaseOrder.getId() + "  Number:" + purchaseOrder.getFriendlyNumber());
	    }

	}

    }

    private void submitAttachmentsToECM(PurchaseOrder purchaseOrder) {

	SearchFilterMsDTO supplierFilter = new SearchFilterMsDTO();
	supplierFilter.id = purchaseOrder.getSupplierCode();

	SupplierMsDTO supplierInfo = getSuplierInfo(purchaseOrder.getId(), supplierFilter);

	final PurchaseOrderECMMetadataMsDTO metadata = new PurchaseOrderECMMetadataMsDTO();
	metadata.setPurchaseOrderNumber(purchaseOrder.getFriendlyNumber());
	metadata.setStatusKey("1");
	metadata.setSupplierVatNumber(supplierInfo.nif);
	metadata.setSupplierName(supplierInfo.description);
	metadata.setSupplierId(supplierFilter.id);

	final List<Attachment> attachmentsToSubmit = getAttachmentsToSubmit(purchaseOrder);
	attachmentsToSubmit.stream().
			forEach(attachment -> {

                	    InputStream blob;
                
                	    try {
                		ctx.logInfo("[SubmitPurchaseOrderThread] Reading blob with blobId: " + attachment.getBlobId() + " on PurchaseOrder: " + purchaseOrder.getId());
                		blob = blobResource.getBlob(attachment.getBlobId());
                	    } catch (Exception e) {
                		ctx.logError("[SubmitPurchaseOrderThread] Failed to read blob with blobId: " + attachment.getBlobId() + " on submit process of PurchaseOrder: " + purchaseOrder.getId());
                		throw ctx.exception(e);
                	    }
                
                	    DocumentClassTypeMs documentClassTypeMs = PurchaseOrderRepositoryResourceMapper.INSTANCE.toDocumentClassTypeMs(attachment.getDocumentClass());
                
                	    ContentDisposition cd = new ContentDisposition("form-data; name=\"data\"; filename=" + attachment.getName());
                	    org.apache.cxf.jaxrs.ext.multipart.Attachment document = new org.apache.cxf.jaxrs.ext.multipart.Attachment("root.message@cxf.apache.org", blob, cd);
                
                	    //@formatter:off
                	    String externalId = null; 
                	    
                	    try {
                		
//                		metadata.setObjectName(attachment.getName());
                	
                		externalId = this.ecmServiceResource.createDocument(
                				purchaseOrder.getFriendlyNumber(), 
                				documentClassTypeMs.getEcmDocumentClass(), 
                				attachment.getName(), 
                				metadata, 
                				document);
                		
                	    } catch (Exception e) {
                		
                		ctx.logError("[SubmitPurchaseOrderThread] Error: failed to upload Attachment: " + attachment.getId() + " to ECMService on submit process of PurchaseOrder: " + purchaseOrder.getId() + " " + e);
                		throw ctx.submitECMException(purchaseOrder.getId(), attachment.getId());
                	    }
                	    //@formatter:on
                
                	    if (StringUtils.isBlank(externalId)) {
                
                		ctx.logError("[SubmitPurchaseOrderThread] Error: failed to upload Attachment: " + attachment.getId() + " to ECMService on submit process of PurchaseOrder: " + purchaseOrder.getId());
                		throw ctx.submitECMException(purchaseOrder.getId(), attachment.getId());
                	    }
                
                	    attachment.setExternalId(externalId);
                
                	    try {
                
                		blobResource.removeBlob(attachment.getBlobId());
                		attachment.setBlobId(null);
                	    } catch (Exception e) {
                
                		ctx.logInfo("[SubmitPurchaseOrderThread] Warning: failed to remove Blob with blobId: " + attachment.getBlobId() + " on submit process of PurchaseOrder: " + purchaseOrder.getId());
                	    }
                
                	    try {
                
                		purchaseOrderDAO.updateAttachment(attachment);
                	    } catch (UnexpectedPersistenceException e) {
                
                		ctx.logError("[SubmitPurchaseOrderThread] Error: failed to update Attachment: " + attachment.getId() + " on submit process of PurchaseOrder: " + purchaseOrder.getId());
                		throw ctx.exception(e.getMessage());
                	    }
                	    
                	});
	
    }

    /**
     * Gets the {@link SupplierMsDTO} on the ReferenceData MS.<br>
     * 
     * Throws{@link PurchaseOrderRepositoryGetSupplierInfoException} if the supplier
     * is not found.
     * 
     * @param purchaseOrderId
     * @param purchaseOrder
     * @param supplierFilter
     * @return
     */
    //@formatter:off
    private SupplierMsDTO getSuplierInfo(Long purchaseOrderId, SearchFilterMsDTO supplierFilter) {
	
	SupplierMsDTO supplierInfo = null;

	try {
	    List<SupplierMsDTO> searchSupplierResult = 
		    referenceDataResource.getSuppliers(supplierFilter);
	    
	    if (searchSupplierResult != null 
		    && searchSupplierResult.isEmpty() == false 
		    && searchSupplierResult.get(0) != null) {
		supplierInfo = searchSupplierResult.get(0);
	    }

	} catch (Exception e) {
	    throw ctx.getSupplierInfoException(purchaseOrderId, supplierFilter.id);
	}

	if (supplierInfo == null) {
	    throw ctx.getSupplierInfoException(purchaseOrderId, supplierFilter.id);
	}

	return supplierInfo;
    }
    //@formatter:on

    /**
     * Returns the attachments that don't have externalId
     * 
     * @param purchaseOrder
     * @return
     */
    private List<Attachment> getAttachmentsToSubmit(PurchaseOrder purchaseOrder) {
	//@formatter:off
	return purchaseOrder.getHistories()
			.stream()
			.filter(historyHasAttachment())
			.flatMap(attachmentsStream())
			.filter(attachment -> attachment != null && StringUtils.isBlank(attachment.getExternalId()))
			.collect(Collectors.toList());
	//@formatter:on
    }

    /**
     * Function to return a stream of the history attachments
     * 
     * @return
     */
    private Function<? super History, ? extends Stream<? extends Attachment>> attachmentsStream() {
	return history -> history.getAttachments().stream();
    }

    /**
     * Predicate that filter the history that have attachments
     * 
     * @return
     */
    private Predicate<? super History> historyHasAttachment() {
	//@formatter:off
	return history -> 
			history.getAttachments() != null 
			&& 
			history.getAttachments().isEmpty() == false;
	//@formatter:on
    }

}
