package com.novabase.omni.oney.modules.purchaseorderrepository.service.ri;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PurchaseOrderRepositoryResourceConfigWrapper {

    private List<String> purchaseOrderTypesToSubmitSAP;
    private Boolean ecmServiceOffline;
    private Boolean sapServiceOffline;
    
    public PurchaseOrderRepositoryResourceConfigWrapper() {
	super();
    }
    
    public PurchaseOrderRepositoryResourceConfigWrapper(final PurchaseOrderRepositoryResourceConfig config) {
	
	super();
	
	this.ecmServiceOffline = config.ecmServiceOffline();
	this.sapServiceOffline = config.sapServiceOffline();
	this.purchaseOrderTypesToSubmitSAP = new ArrayList<String>(Arrays.asList(config.purchaseOrderTypes().split(",")));
	
    }
    
    public List<String> getPurchaseOrderTypesToSubmitSAP() {
        return purchaseOrderTypesToSubmitSAP;
    }
    public void setPurchaseOrderTypesToSubmitSAP(List<String> purchaseOrderTypesToSubmitSAP) {
        this.purchaseOrderTypesToSubmitSAP = purchaseOrderTypesToSubmitSAP;
    }
    public Boolean getEcmServiceOffline() {
        return ecmServiceOffline;
    }
    public void setEcmServiceOffline(Boolean ecmServiceOffline) {
        this.ecmServiceOffline = ecmServiceOffline;
    }
    public Boolean getSapServiceOffline() {
        return sapServiceOffline;
    }
    public void setSapServiceOffline(Boolean sapServiceOffline) {
        this.sapServiceOffline = sapServiceOffline;
    }
    
}
