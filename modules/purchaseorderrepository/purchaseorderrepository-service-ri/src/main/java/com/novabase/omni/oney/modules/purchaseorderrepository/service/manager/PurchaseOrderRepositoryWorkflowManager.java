package com.novabase.omni.oney.modules.purchaseorderrepository.service.manager;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.PurchaseOrderDAO;
import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.exception.UnexpectedPersistenceException;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.History;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.HistoryBuilder;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.PurchaseOrder;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.PurchaseOrderBuilder;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.Workflow;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.WorkflowStep;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.WorkflowStepBuilder;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.WorkflowActionMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.WorkflowMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.PurchaseOrderStatusMs;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.TimeLineStepTypeMs;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.WorkFlowActionTypeMs;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.ri.PurchaseOrderRepositoryResourceConfigWrapper;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.ri.PurchaseOrderRepositoryContext;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.ri.PurchaseOrderRepositoryResourceMapper;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.ri.WorkflowResourceMapper;

/**
 * This manager is responsible to manage the workflow and to persist updates on the workflow
 * 
 * @author NB24879
 *
 */
//@formatter:off
@Component(
//	reference = {
//	@Reference(
//		name = PurchaseOrderRepositoryResourceProperties.REF_CONTEXT,
//		service = PurchaseOrderRepositoryContext.class, 
//		cardinality = ReferenceCardinality.MANDATORY)
//	},
	service = { PurchaseOrderRepositoryWorkflowManager.class })
//@formatter:on
public class PurchaseOrderRepositoryWorkflowManager {

    @Reference
    private PurchaseOrderRepositoryContext ctx;

    @Reference
    private volatile PurchaseOrderDAO purchaseOrderDAO;

    @Reference
    private PurchaseOrderRepositoryDataManager purchaseOrderRepositoryDataManager;

    @Reference
    private PurchaseOrderRepositorySubmitOneyManager purchaseOrderRepositorySubmitOneyManager;

    /**
     * Update workflow using the currentAction
     * 
     * @param purchaseOrderId
     * @param action
     * @param limitDateToNextStep
     * 
     * @return WorkflowMsDTO
     */
    public WorkflowMsDTO updateWorkflow(final Long purchaseOrderId, final WorkflowActionMsDTO action, final LocalDateTime limitDateToNextStep, final PurchaseOrderRepositoryResourceConfigWrapper configWrapper, final Long userId) {

	PurchaseOrder purchaseOrder = purchaseOrderRepositoryDataManager.findPurchaseOrder(purchaseOrderId);

	Workflow workflow = purchaseOrder.getWorkflow();

	int currentStepIndex = workflow.getCurrentIndex();

	WorkflowStep currentWorkflowStep = workflow.getWorkflowStepByIndex(currentStepIndex);

	//@formatter:off
	Long currentWorkflowStepId = 
		currentWorkflowStep != null 
			? currentWorkflowStep.getId() 
			: null;
	//@formatter:on

	History history = WorkflowResourceMapper.INSTANCE.toHistory(action);

	PurchaseOrderStatusMs purchaseOrderStatus = null;
	LocalDateTime approvalDate = null; //TODO: change to finish date

	if (WorkFlowActionTypeMs.APPROVE.equals(action.type) && currentWorkflowStep.isHasNextStep()) {

	    //@formatter:off
	    int nextStepIndex = 
		    workflow.getSteps()
		    	.stream()
		    	.filter(step -> 
		    		step.isActive() 
		    			&& step.getIdx() > currentStepIndex)
		    	.map(WorkflowStep::getIdx)
		    	.findFirst()
		    	.orElse(0);
	    //@formatter:on

	    if (nextStepIndex != 0) {
		updateWorkflowToIndex(workflow, nextStepIndex, limitDateToNextStep);
	    } else {
		// fail safe, Approved
		ctx.logError("Purchase Order with ID: " + purchaseOrderId + " are being approved because the search for the next active workflowstep returned index 0");
		endWorkflow(workflow);
		purchaseOrderStatus = PurchaseOrderStatusMs.APPROVED;
		approvalDate = history.getDate();
	    }

	} else if (WorkFlowActionTypeMs.RETURN.equals(action.type)) {

	    //@formatter:off
	    int previousStepIndex = 
		    	workflow.getSteps()
		    	.stream()
			.sorted((step1, step2) -> step2.getIdx() - step1.getIdx())
			.filter(step -> 
				step.isActive() 
			    	&& step.getIdx() < currentStepIndex)
			.map(WorkflowStep::getIdx)
			.findFirst()
			.orElse(0);
	    //@formatter:on

	    if (previousStepIndex == 1) {
		throw ctx.workflowReturnException(purchaseOrderId);
	    }

	    updateWorkflowToIndex(workflow, previousStepIndex, limitDateToNextStep);

	} else if (WorkFlowActionTypeMs.REJECT.equals(action.type)) {

	    endWorkflow(workflow);
	    purchaseOrderStatus = PurchaseOrderStatusMs.REJECTED;
	    approvalDate = history.getDate();

	} else if (WorkFlowActionTypeMs.CANCEL.equals(action.type)) {

	    endWorkflow(workflow);
	    purchaseOrderStatus = PurchaseOrderStatusMs.CANCELED;

	} else {
	    // Approved
	    endWorkflow(workflow);
	    purchaseOrderStatus = PurchaseOrderStatusMs.APPROVED;
	    approvalDate = history.getDate();
	}

	try {

	    addWorkflowUpdateHistory(purchaseOrderId, currentWorkflowStepId, history);

	    //@formatter:off
	    String purchaseOrderStatusString = 
		    purchaseOrderStatus != null 
		    	? purchaseOrderStatus.name() 
		    	: null;
		    	
	    WorkflowMsDTO workflowMsDTO = 
		    WorkflowResourceMapper.INSTANCE.toWorkflow(
			    purchaseOrderDAO.updateWorkFlow(
				    workflow, 
				    purchaseOrderId, 
				    purchaseOrderStatusString, 
				    approvalDate),
		    purchaseOrderStatus);

	    // If the PurchaseOrder is approved we must submit the date to Oney services(ECM & SAP)
	    if (PurchaseOrderStatusMs.APPROVED.equals(purchaseOrderStatus)) {
		purchaseOrderRepositorySubmitOneyManager.asyncSubmitToExternalServices(Arrays.asList(purchaseOrderId), configWrapper, userId);
	    }
	    //@formatter:on

	    return workflowMsDTO;
	} catch (UnexpectedPersistenceException e) {
	    throw ctx.exception(e.getMessage());
	}
    }

    /**
     * Claims the process to some group that are schedule to the next steps on the workflow.
     * 
     * @param purchaseOrderId
     * @param userId
     * @param userGroup
     * @param workflowAction
     * @param limitDateToNextStep
     * 
     * @return WorkflowMsDTO
     */
    public WorkflowMsDTO claimProcess(Long purchaseOrderId, Long userId, String userGroup, WorkflowActionMsDTO workflowAction, LocalDateTime limitDateToNextStep) {

	PurchaseOrder purchaseOrder = purchaseOrderRepositoryDataManager.findPurchaseOrder(purchaseOrderId);

	if (userGroupCanClaimProcess(purchaseOrder, userGroup) == false) {
	    throw ctx.groupCannotClaimException(purchaseOrderId, userGroup);
	}

	Workflow workflow = purchaseOrder.getWorkflow();
	int currentStepIndex = workflow.getCurrentIndex();

	int targetStepIndex = findTargetStepIndex(userGroup, workflow);

	if (targetStepIndex == 0) {
	    throw ctx.groupCannotClaimException(purchaseOrderId, userGroup);
	}

	List<WorkflowStep> skippedSteps = getSkippedSteps(workflow, currentStepIndex, targetStepIndex);

	if (skippedSteps == null || skippedSteps.isEmpty() == true) {
	    throw ctx.groupCannotClaimException(purchaseOrderId, userGroup);
	}

	List<History> histories = generateSkippedHistories(userId, workflowAction, purchaseOrder, skippedSteps);

	skippedSteps.forEach(step -> step.setActive(false)); // In order to guarantee that the skipped steps will not be executed in case of
							     // the P.O be returned we must set the active flag to false

	updateWorkflowToIndex(workflow, targetStepIndex, limitDateToNextStep);
	try {
	    purchaseOrderDAO.addHistories(histories);
	    //@formatter:off
	    return PurchaseOrderRepositoryResourceMapper.INSTANCE
		    .toWorkflowMsDTO(
			    purchaseOrderDAO.updateWorkFlow(
				    workflow, 
				    purchaseOrderId, 
				    null, 
				    null), 
			    PurchaseOrderStatusMs.IN_APPROVAL);
	    //@formatter:on
	} catch (UnexpectedPersistenceException e) {
	    throw ctx.exception(e.getMessage());
	}
    }

    /**
     * Persist a history to the purchaseOrder
     * 
     * @param purchaseOrderId
     * @param currentWorkflowStepId
     * @param history
     * @throws UnexpectedPersistenceException
     */
    private void addWorkflowUpdateHistory(Long purchaseOrderId, Long currentWorkflowStepId, History history) throws UnexpectedPersistenceException {

	history.setPurchaseOrder(new PurchaseOrderBuilder().withId(purchaseOrderId).build());

	// If the PurchaseOrder is being canceled we don't have workFlowStepId
	if (currentWorkflowStepId != null) {
	    history.setWorkflowStep(new WorkflowStepBuilder().withId(currentWorkflowStepId).build());
	}

	purchaseOrderDAO.addHistory(history);
    }

    /**
     * Generate skipped histories, if there some attachment in the currentAction we
     * add this attachment to the first generated history
     * 
     * @param userId
     * @param workflowAction
     * @param purchaseOrder
     * @param skippedSteps
     * 
     * @return List<History>
     */
    private List<History> generateSkippedHistories(Long userId, WorkflowActionMsDTO workflowAction, PurchaseOrder purchaseOrder, List<WorkflowStep> skippedSteps) {
	//@formatter:off
	List<History> histories = skippedSteps
		.stream()
		.map(step -> 
        		{
        		    return new HistoryBuilder()
        			    	.withDate(LocalDateTime.now())
        			    	.withWorkflowStep(step)
        			    	.withType(TimeLineStepTypeMs.SKIPPED.name())
        			    	.withPurchaseOrder(purchaseOrder)
        			    	.withUserId(userId)
        			    	.withUserGroup(step.getUserGroup()) // In order to keep the original group step we save the history with the original group and not with the claimGroup
        			    	.build();
        		})
		.collect(Collectors.toList());

	if (workflowAction != null 
		&& workflowAction.attachments != null 
		&& workflowAction.attachments.isEmpty() == false) {

	    histories.stream()
	    	.findFirst()
	    	.get()
	    	.setAttachments(
	    		PurchaseOrderRepositoryResourceMapper.INSTANCE.toAttachmentList(workflowAction.attachments)
	    		);
	}
	return histories;
	//@formatter:on
    }

    /**
     * Find the workFlowStepIndex by userGroup
     * 
     * @param userGroup
     * @param workflow
     * @return Integer
     */
    private Integer findTargetStepIndex(String userGroup, Workflow workflow) {
	//@formatter:off
	return workflow.getSteps()
		.stream()
		.filter(step -> step.getUserGroup().equals(userGroup))
		.map(WorkflowStep::getIdx)
		.findFirst()
		.orElse(0);
	//@formatter:on
    }

    /**
     * Get the skipped steps of the workflow by currentStepIndex and targetStepIndex
     * 
     * @param workflow
     * @param currentStepIndex
     * @param targetStepIndex
     * 
     * @return List<WorkflowStep>
     */
    private List<WorkflowStep> getSkippedSteps(Workflow workflow, int currentStepIndex, int targetStepIndex) {
	//@formatter:off
	return workflow.getSteps()
			.stream()
			.sorted((step1, step2) -> step1.getIdx() - step2.getIdx())
			.filter(step -> step.getIdx() >= currentStepIndex 
					&& step.getIdx() < targetStepIndex)
			.collect(Collectors.toList());
	//@formatter:on
    }

    /**
     * Validate if the userGroup is schedule on PurchaseOrder approval workflow and
     * if this step was not executed yet.
     * 
     * @param purchaseOrder
     * @param userGroup
     * 
     * @return boolean true when the currentUserGroup can claim the PurchaseOrder
     *         else false
     */
    public boolean userGroupCanClaimProcess(PurchaseOrder purchaseOrder, String userGroup) {

	if (StringUtils.isBlank(userGroup)) {
	    return false;
	}

	Workflow workflow = purchaseOrder.getWorkflow();
	int currentIndex = workflow.getCurrentIndex();
	String currentGroup = workflow.getCurrentUserGroup();

	//@formatter:off
	if(workflow.isHasNextStep() == false 
		|| userGroup.equals(currentGroup) 
		|| currentIndex == 0) {
	    
	    return false;
	}
	
	return workflow.getSteps()
			.stream()
			.anyMatch(step -> 	
					step.getIdx() > currentIndex 
        				&& userGroup.equals(step.getUserGroup()));
	//@formatter:on
    }

    /**
     * Updates the Workflow to the target index
     * 
     * @param workflow
     * @param targetIndex
     * @param limitDateToNextStep
     */
    private void updateWorkflowToIndex(Workflow workflow, int targetIndex, LocalDateTime limitDateToNextStep) {
	WorkflowStep nextWorkFlowStep;
	nextWorkFlowStep = workflow.getWorkflowStepByIndex(targetIndex);
	nextWorkFlowStep.setActive(true); // When the PurchaseOrder is claimed, the step maybe was inactive, so here we
					  // guarantee that the current step is active
	workflow.setCurrentIndex(targetIndex);
	workflow.setCurrentUserGroup(nextWorkFlowStep.getUserGroup());
	workflow.setHasNextStep(nextWorkFlowStep.isHasNextStep());
	workflow.setLimitDateToNextStep(limitDateToNextStep);
    }

    /**
     * Finish the Workflow setting: currentUserGroup = NULL limitDateToNextStep =
     * NULL currentIndex = 0 hasNextStep = false
     * 
     * @param workflow
     */
    private void endWorkflow(Workflow workflow) {
	workflow.setCurrentIndex(0);
	workflow.setCurrentUserGroup(null);
	workflow.setHasNextStep(false);
	workflow.setLimitDateToNextStep(null);
    }

}
