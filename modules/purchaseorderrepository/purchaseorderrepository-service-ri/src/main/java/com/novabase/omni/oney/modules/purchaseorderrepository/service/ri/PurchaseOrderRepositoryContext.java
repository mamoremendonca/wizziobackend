package com.novabase.omni.oney.modules.purchaseorderrepository.service.ri;

import java.util.Date;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;

import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.exception.PurchaseOrderRepositoryClaimException;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.exception.PurchaseOrderRepositoryException;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.exception.PurchaseOrderRepositoryGetSupplierInfoException;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.exception.PurchaseOrderRepositoryGroupCannotClaimException;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.exception.PurchaseOrderRepositoryNotFoundException;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.exception.PurchaseOrderRepositorySubmitECMException;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.exception.PurchaseOrderRepositoryUniqueConstraintException;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.exception.PurchaseOrderRepositoryWorkflowReturnException;

import io.digitaljourney.platform.modules.async.api.PlatformAsyncManager;
import io.digitaljourney.platform.modules.cache.api.PlatformCacheManager;
import io.digitaljourney.platform.modules.commons.annotation.DocumentationResource;
import io.digitaljourney.platform.modules.invocation.api.PlatformInvocationManager;
import io.digitaljourney.platform.modules.invocation.api.model.EventLog;
import io.digitaljourney.platform.modules.invocation.api.model.type.StatusType;
import io.digitaljourney.platform.modules.security.api.PlatformSecurityManager;
import io.digitaljourney.platform.modules.ws.rs.api.context.AbstractRSEndpointContext;

// @formatter:off
@Component(
	service = { Object.class, PurchaseOrderRepositoryContext.class },
	reference = {
		@Reference(
			name = PurchaseOrderRepositoryResourceProperties.REF_PLATFORM_SECURITY_MANAGER,
			service = PlatformSecurityManager.class,
			cardinality = ReferenceCardinality.MANDATORY),
        @Reference(
            name = PurchaseOrderRepositoryResourceProperties.REF_PLATFORM_INVOCATION_MANAGER,
            service = PlatformInvocationManager.class,
            cardinality = ReferenceCardinality.MANDATORY),
		@Reference(
			name = PurchaseOrderRepositoryResourceProperties.REF_ASYNC_MANAGER,
			service = PlatformAsyncManager.class,
			cardinality = ReferenceCardinality.MANDATORY),
		@Reference(
			name = PurchaseOrderRepositoryResourceProperties.REF_CACHE_MANAGER,
			service = PlatformCacheManager.class,
			cardinality = ReferenceCardinality.MANDATORY)
	})
@DocumentationResource(PurchaseOrderRepositoryResourceProperties.DOCS_ADDRESS)
// @formatter:on
public final class PurchaseOrderRepositoryContext extends AbstractRSEndpointContext {
    @Activate
    public void activate(ComponentContext ctx) {
	prepare(ctx);
    }

    public void logError(String message) {
	EventLog event = new EventLog();
	event.setEvent_message(message);
	event.setStart_event_time(new Date());
	getPlatformInvocationManager().logNewEvent(event, null, StatusType.ERROR);
	getLogger().error(message);
    }

    public void logInfo(String message) {
	EventLog event = new EventLog();
	event.setEvent_message(message);
	event.setStart_event_time(new Date());
	getPlatformInvocationManager().logNewEvent(event, null, StatusType.SUCCESS);
	getLogger().info(message);
    }

    public PurchaseOrderRepositoryException exception(String message) {
	return PurchaseOrderRepositoryException.of(this, message);
    }

    public PurchaseOrderRepositoryException exception(Throwable cause) {
	return PurchaseOrderRepositoryException.of(this, cause);
    }

    public PurchaseOrderRepositoryUniqueConstraintException uniqueConstraintException(Long number, Long version) {
	return PurchaseOrderRepositoryUniqueConstraintException.of(this, number, version);
    }

    public PurchaseOrderRepositoryUniqueConstraintException uniqueConstraintException(Long id) {
	return PurchaseOrderRepositoryUniqueConstraintException.of(this, id);
    }

    public PurchaseOrderRepositoryNotFoundException notFoundException(Class<?> entity, Long id) {
	return PurchaseOrderRepositoryNotFoundException.of(this, entity, id);
    }

    public PurchaseOrderRepositoryNotFoundException notFoundException(final Class<?> entity, final String number) {
	return PurchaseOrderRepositoryNotFoundException.of(this, entity, number);
    }

    public PurchaseOrderRepositoryGroupCannotClaimException groupCannotClaimException(Long id, String userGroup) {
	return PurchaseOrderRepositoryGroupCannotClaimException.of(this, id, userGroup);
    }

    public PurchaseOrderRepositoryClaimException claimException(Long id) {
	return PurchaseOrderRepositoryClaimException.of(this, id);
    }

    public PurchaseOrderRepositoryWorkflowReturnException workflowReturnException(Long id) {
	return PurchaseOrderRepositoryWorkflowReturnException.of(this, id);
    }

    public PurchaseOrderRepositorySubmitECMException submitECMException(Long id, Long attachmentId) {
	return PurchaseOrderRepositorySubmitECMException.of(this, id, attachmentId);
    }

    public PurchaseOrderRepositoryGetSupplierInfoException getSupplierInfoException(Long id, String supplierCode) {
	return PurchaseOrderRepositoryGetSupplierInfoException.of(this, id, supplierCode);
    }
    
    

}
