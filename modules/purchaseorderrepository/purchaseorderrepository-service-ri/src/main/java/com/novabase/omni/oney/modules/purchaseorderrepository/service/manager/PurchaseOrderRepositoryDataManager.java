package com.novabase.omni.oney.modules.purchaseorderrepository.service.manager;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.PurchaseOrderDAO;
import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.exception.NotFoundException;
import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.exception.UnexpectedPersistenceException;
import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.exception.UniqueConstraintViolationException;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.Attachment;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.History;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.PurchaseOrder;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.Workflow;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.AttachmentMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.PurchaseOrderMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.SearchCriteriaMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.dto.TimeLineStepMsDTO;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.api.enums.PurchaseOrderStatusMs;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.ri.PurchaseOrderRepositoryContext;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.ri.PurchaseOrderRepositoryResourceMapper;
import com.novabase.omni.oney.modules.purchaseorderrepository.service.ri.WorkflowResourceMapper;

/**
 * This manager is responsible to persist and retrieve PurchaseOrder data
 * 
 * @author NB24879
 *
 */
//@formatter:off
@Component(
//	reference = {
//	@Reference(
//		name = PurchaseOrderRepositoryResourceProperties.REF_CONTEXT,
//		service = PurchaseOrderRepositoryContext.class, 
//		cardinality = ReferenceCardinality.MANDATORY)
//	},
	service = { PurchaseOrderRepositoryDataManager.class })
//@formatter:on
public class PurchaseOrderRepositoryDataManager {

    @Reference
    private PurchaseOrderRepositoryContext ctx;

    @Reference
    private PurchaseOrderDAO purchaseOrderDAO;

    /**
     * Finds a {@link PurchaseOrder} by Id and throw exception when: Not Found or
     * some Unexpected error occur
     * 
     * @param purchaseOrderId
     * @return PurchaseOrder
     */
    public PurchaseOrder findPurchaseOrder(Long purchaseOrderId) {

	try {

	    return purchaseOrderDAO.getPurchaseOrder(purchaseOrderId);
	} catch (NotFoundException e) {
	    throw ctx.notFoundException(e.getClass(), purchaseOrderId);
	} catch (UnexpectedPersistenceException e) {
	    throw ctx.exception(e.getMessage());
	}
    }

    /**
     * 
     * @param purchaseOrder
     * @param limitDateToNextStep
     * @return PurchaseOrderMsDTO
     */
    public PurchaseOrderMsDTO addPurchaseOrder(PurchaseOrderMsDTO purchaseOrder, LocalDateTime limitDateToNextStep) {

	try {

	    // failsafe for concurrency problems with PO submission
	    final Long purchaseOrderId = this.purchaseOrderDAO.getPurchaseOrderIdByInstanceId(purchaseOrder.instanceId);
	    if (purchaseOrderId != null && purchaseOrderId.longValue() > 0) {
		this.purchaseOrderDAO.deletePurchaseOrderById(purchaseOrderId);
	    }
	    
	    purchaseOrder.workflow.limitDateToNextStep = limitDateToNextStep;
	    purchaseOrder.status = PurchaseOrderStatusMs.IN_APPROVAL;

	    return PurchaseOrderRepositoryResourceMapper.INSTANCE
		    .toPurchaseOrderMsDTOWithItemsAndWorkflow(purchaseOrderDAO.addPurchaseOrder(PurchaseOrderRepositoryResourceMapper.INSTANCE.toPurchaseOrder(purchaseOrder)));

	} catch (UniqueConstraintViolationException e) {

	    ctx.logError(e.getMessage());

	    if (e.getType() == UniqueConstraintViolationException.PURCHASE_ORDER_ID) {
		throw ctx.uniqueConstraintException(purchaseOrder.id);

	    } else if (e.getType() == UniqueConstraintViolationException.PURCHASE_ORDER_NUMBER_VERSION) {
		throw ctx.uniqueConstraintException(purchaseOrder.number, purchaseOrder.version);

	    } else {
		throw ctx.exception(e.getMessage());
	    }

	} catch (UnexpectedPersistenceException | NotFoundException e) {

	    ctx.logError(e.getMessage());
	    throw ctx.exception(e.getMessage());
	}
    }

    /**
     * Finds the histories and the scheduled steps of one PurchaseOrder and returns
     * the time line
     * 
     * @param purchaseOrderId
     * @return List<TimeLineStepMsDTO>
     */
    public List<TimeLineStepMsDTO> getTimeLine(Long purchaseOrderId) {

	Workflow workflow;
	List<History> histories;

	try {

	    workflow = purchaseOrderDAO.searchPurchaseOrderWorkFlow(purchaseOrderId);
	    histories = purchaseOrderDAO.searchPurchaseOrderHistory(purchaseOrderId);

	} catch (UnexpectedPersistenceException e) {
	    throw ctx.exception(e.getMessage());
	}

	if (workflow == null || workflow.getSteps() == null || workflow.getSteps().isEmpty()) {
	    throw ctx.notFoundException(Workflow.class, purchaseOrderId);
	}
	if (histories == null || histories.isEmpty()) {
	    throw ctx.notFoundException(History.class, purchaseOrderId);
	}

	return WorkflowResourceMapper.INSTANCE.toTimeLineStepMsDTOList(workflow, histories);
    }

    /**
     * Search PurchaseOrders by criteria filters
     * 
     * @param searchCriteria
     * @return List<PurchaseOrderMsDTO>
     */
    public List<PurchaseOrderMsDTO> search(SearchCriteriaMsDTO searchCriteria) {

	try {
	    //@formatter:off
	    List<PurchaseOrder> purchaseOrders = 
		    purchaseOrderDAO.searchPurchaseOrders(
			    PurchaseOrderRepositoryResourceMapper.INSTANCE.toSearchCriteria(searchCriteria));
	    
	    return purchaseOrders.stream()
		    		.map(po -> PurchaseOrderRepositoryResourceMapper.INSTANCE.toPurchaseOrderMsDTO(po))
		    		.collect(Collectors.toList());
	    //@formatter:on

	} catch (UnexpectedPersistenceException e) {
	    throw ctx.exception(e.getMessage());
	}
    }

    /**
     * Finds and returns the attachments of the PurchaseOrder by purchaseOrderId
     * 
     * @param purchaseOrderId
     * @return List<AttachmentMsDTO>
     */
    public List<AttachmentMsDTO> getPurchaseOrderAttachments(Long purchaseOrderId) {

	try {
	    List<Attachment> attachments = purchaseOrderDAO.getPurchaseOrderAttachments(purchaseOrderId);

	    return attachments.stream().map(att -> PurchaseOrderRepositoryResourceMapper.INSTANCE.toAttachmentMsDTO(att)).collect(Collectors.toList());
	} catch (NotFoundException e) {
	    throw ctx.notFoundException(e.getClass(), purchaseOrderId);
	} catch (UnexpectedPersistenceException e) {
	    throw ctx.exception(e.getMessage());
	}
    }

    /**
     * Finds and returns one specified attachment of the PurchaseOrder by purchaseOrderId and index
     * 
     * @param purchaseOrderId
     * @param index
     * @return
     */
    public AttachmentMsDTO getPurchaseOrderAttachment(Long purchaseOrderId, int index) {

	try {

	    final Attachment attachment = purchaseOrderDAO.getPurchaseOrderAttachment(purchaseOrderId, index);
	    final AttachmentMsDTO attachmentMsDTO = PurchaseOrderRepositoryResourceMapper.INSTANCE.toAttachmentMsDTO(attachment);
	    if (StringUtils.stripToEmpty(attachment.getExternalId()).contains("OFFLINE-")) {
		attachmentMsDTO.mimeType = "image/png";
	    }

	    return attachmentMsDTO;

	} catch (NotFoundException e) {
	    throw ctx.notFoundException(e.getClass(), purchaseOrderId);
	} catch (UnexpectedPersistenceException e) {
	    throw ctx.exception(e.getMessage());
	}

    }
    
    /**
     * Finds a {@link PurchaseOrder} by Number and throw exception when: Not Found or
     * some Unexpected error occur
     * 
     * @param purchaseOrderNumber
     * @return PurchaseOrder
     */
    public PurchaseOrder findAppprovedPurchaseOrderByNumber(final Long purchaseOrderNumber) {

	try {
	    return this.purchaseOrderDAO.getApprovedPurchaseOrderByNumber(purchaseOrderNumber);
	} catch (NotFoundException e) {
	    throw this.ctx.notFoundException(e.getClass(), purchaseOrderNumber);
	} catch (UnexpectedPersistenceException e) {
	    throw this.ctx.exception(e.getMessage());
	}
    }
    
}
