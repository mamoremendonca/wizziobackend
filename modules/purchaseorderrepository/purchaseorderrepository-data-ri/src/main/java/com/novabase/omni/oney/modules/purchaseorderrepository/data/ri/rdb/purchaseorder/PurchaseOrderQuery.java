package com.novabase.omni.oney.modules.purchaseorderrepository.data.ri.rdb.purchaseorder;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.novabase.omni.oney.modules.purchaseorderrepository.data.ri.rdb.purchaseorder.enums.ScopeType;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.SearchScope;

import liquibase.util.StringUtils;

public class PurchaseOrderQuery {

    private static final String QUERY_SELECT = "SELECT DISTINCT po ";
    private static final String QUERY_FROM = "FROM PurchaseOrder po ";
    private static final String QUERY_WHERE = "WHERE (1 = 1) ";
    private static final String FIELD_STATUS = "po.status";
    private static final String FIELD_TYPE = "po.type";
    private static final String FIELD_SUPPLIER = "po.supplierCode";
    private static final String FIELD_INTERNAL_ORDER = "po.internalOrderCode";
    private static final String FIELD_PROJECT = "po.projectCode";
    private static final String FIELD_DEPARTMENT = "po.departmentCode";
    private static final String FIELD_NUMBER = "po.number";
    private static final String FIELD_CREATION_DATE = "po.creationDate";
    private static final String FIELD_APPROVA_DATE = "po.approvalDate";
    private static final String FIELD_EXPIRATION_DATE = "po.expirationDate";
    private static final String FIELD_TOTAL = "po.totalWithoutTax";
    
    public PurchaseOrderQuery() {
    }

    // TODO: converter este builder em algo que receba o entityManager e devolva uma
    // Query.

    public static class Builder {

	private String status;
	private Long number;
	private String type;
	private String entityCode;
	private String internalOrderCode;
	private String projectCode;
	private String departmentCode;
	private SearchScope scope;
	private Long minVATValue;
	private Long maxVATValue;
	private LocalDate createdDate;
	private LocalDate approvalDate;
	private LocalDate expirationDate;
	// ----------------------
	private String orderField;
	private String orderType;

	public Builder() {
	    this.status = null;
	    this.number = -1L;
	    this.type = null;
	    this.entityCode = null;
	    this.internalOrderCode = null;
	    this.projectCode = null;
	    this.departmentCode = null;
	    this.scope = null;
	    this.minVATValue = -1L;
	    this.maxVATValue = -1L;
	    this.createdDate = null;
	    this.approvalDate = null;
	    this.expirationDate = null;
	    this.orderField = null;
	    this.orderType = null;
	}

	public Builder withStatus(String status) {
	    this.status = status;
	    return this;
	}

	public Builder withNumber(Long number) {
	    this.number = number;
	    return this;
	}

	public Builder withType(String type) {
	    this.type = type;
	    return this;
	}

	public Builder withEntity(String entityCode) {
	    this.entityCode = entityCode;
	    return this;
	}

	public Builder withInternalOrder(String internalOrderCode) {
	    this.internalOrderCode = internalOrderCode;
	    return this;
	}

	public Builder withProject(String projectCode) {
	    this.projectCode = projectCode;
	    return this;
	}

	public Builder withDepartment(String departmentCode) {
	    this.departmentCode = departmentCode;
	    return this;
	}

	public Builder withTotalBetween(Long minTotal, Long maxTotal) {
	    if (minTotal != null) { //minTotal > 0) {
		this.minVATValue = minTotal;
	    }
	    if (maxTotal != null) { //maxTotal > 0) {
		this.maxVATValue = maxTotal;
	    }
	    return this;
	}

	public Builder createdOn(LocalDate createdDate) {
	    this.createdDate = createdDate;
	    return this;
	}

	public Builder approvedOn(LocalDate approvalDate) {
	    this.approvalDate = approvalDate;
	    return this;
	}

	public Builder expiredOn(LocalDate expirationDate) {
	    this.expirationDate = expirationDate;
	    return this;
	}

	public Builder orderBy(String field, String type) {
	    this.orderField = field;
	    this.orderType = type;
	    return this;
	}

	public Builder scope(SearchScope scope) {
	    this.scope = scope;
	    return this;
	}

	// public QueryBuilder
	public String build() {
	    //@formatter:off
	    // TODO: resolver problema com joins
	    return new StringBuilder()
		    	.append(QUERY_SELECT)
		    	.append(QUERY_FROM)
		    	.append(scopeFromClause())
		    	.append(QUERY_WHERE)
		    	.append(scopeWhereClause())
		    	.append(stringClause(status, FIELD_STATUS))
		    	.append(stringClause(type, FIELD_TYPE))
		    	.append(stringClause(entityCode, FIELD_SUPPLIER))
		    	.append(stringClause(internalOrderCode, FIELD_INTERNAL_ORDER))
		    	.append(stringClause(departmentCode, FIELD_DEPARTMENT))
		    	.append(stringClause(projectCode, FIELD_PROJECT))
		    	.append(numberClause(number, FIELD_NUMBER))
		    	.append(dateClause(createdDate, FIELD_CREATION_DATE))
		    	.append(dateClause(approvalDate, FIELD_APPROVA_DATE))
		    	.append(dateClause(expirationDate, FIELD_EXPIRATION_DATE))
		    	.append(betweenClause(minVATValue, maxVATValue, FIELD_TOTAL))
		    	.append(orderClause())
		    	.toString();
	    //@formatter:on
	}

	private String scopeFromClause() {
//	    ScopeType type = ScopeType.valueOf(scope.scopeType);
//	    switch (type) {
//	    case ALL:
		//return " INNER JOIN Workflow wf on po.workflow.id = wf.id " + " INNER JOIN WorkflowStep wfs on wf.id = wfs.workflow.id AND wfs.userGroup = '" + scope.currentUserGroup + "' ";
		//return " JOIN po.workflow wf JOIN wf.steps wfs AND wfs.userGroup = '" + scope.currentUserGroup + "' ";
		return " JOIN FETCH po.workflow workflow JOIN workflow.steps wfs ";
//	    default:
//		return "";
//	    }
	}

	private String scopeWhereClause() {
	    
	    final ScopeType type = ScopeType.valueOf(this.scope.scopeType);
	    switch (type) {
	    
	    case MINE:
		return " AND po.userId = '" + this.scope.currentUserId + "' AND po.userGroup = '" + this.scope.currentUserGroup + "' ";
	    
	    case TEAM:
		return " AND po.userGroup = '" + this.scope.currentUserGroup + "' ";
	    
	    case ALL:
		return (StringUtils.isNotEmpty(this.scope.userGroup) ? " AND po.userGroup = '" + this.scope.userGroup + "' " : "") +
		       (StringUtils.isNotEmpty(this.scope.currentUserGroup) ? " AND wfs.userGroup = '" + this.scope.currentUserGroup + "' " : "");
		// TODO: no caso de não ser enviado o 'currentUserGroup' (not set yet), o filtro all deverá apanhar todos os processos. Provavelmente deveria ser alterado este comportamento.
	    
	    default:
		return "";
	    
	    }
	
	}

	private String orderClause() {
	    String order = (StringUtils.isNotEmpty(orderField) ? " ORDER BY po." + this.orderField + (StringUtils.isNotEmpty(orderType) ? " " + orderType + " " : " ASC ") : " ");
	    
	    if("DESC".equals(orderType)) {
		return order + " NULLS LAST ";
	    }else {
		return order + " NULLS FIRST ";
	    }
	}

	private String stringClause(String value, String field) {
	    return (StringUtils.isNotEmpty(value) ? " AND " + field + " = '" + value + "'" : "");
	}

	private String numberClause(Long value, String field) {
	    return (value != null && number > 0 ? " AND " + field + " = " + value : "");
	}

	private String dateClause(LocalDate value, String field) {
	    if (value != null) {
		return " AND " + field + " between '" + value.format(DateTimeFormatter.ISO_LOCAL_DATE) + 
					       "' and '" + value.plusDays(1).format(DateTimeFormatter.ISO_LOCAL_DATE) + "' ";
	    }
	    return "";
	}

	private String betweenClause(Long minValue, Long maxValue, String field) {
	    return (minValue < maxValue ? " AND " + field + " BETWEEN " + minValue + " AND " + maxValue + " " : (minValue >= 0 ? " AND " + field + " >= " + minValue + " " : ""));
	}
    }
}
