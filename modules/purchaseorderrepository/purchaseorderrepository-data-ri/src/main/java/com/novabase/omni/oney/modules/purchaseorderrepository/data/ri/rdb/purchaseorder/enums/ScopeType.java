package com.novabase.omni.oney.modules.purchaseorderrepository.data.ri.rdb.purchaseorder.enums;

public enum ScopeType {
    MINE, TEAM, ALL, NONE;

    @SuppressWarnings("unchecked")
    public static <T extends Enum<ScopeType>> T getEnum(String name){

	Enum<ScopeType> value = ScopeType.NONE;
	try {
	    value = ScopeType.valueOf(name);
	} catch (Throwable t) {
	    // DO nothing
	}
	return (T) value;
    }

}
