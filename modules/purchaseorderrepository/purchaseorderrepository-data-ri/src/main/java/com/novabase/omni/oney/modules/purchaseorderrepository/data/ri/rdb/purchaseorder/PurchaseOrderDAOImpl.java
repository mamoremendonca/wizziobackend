package com.novabase.omni.oney.modules.purchaseorderrepository.data.ri.rdb.purchaseorder;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.transaction.control.TransactionControl;
import org.osgi.service.transaction.control.jpa.JPAEntityManagerProvider;

import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.PurchaseOrderDAO;
import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.exception.NotFoundException;
import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.exception.UnexpectedPersistenceException;
import com.novabase.omni.oney.modules.purchaseorderrepository.data.api.exception.UniqueConstraintViolationException;
import com.novabase.omni.oney.modules.purchaseorderrepository.data.ri.RDBContext;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.Attachment;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.History;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.PurchaseOrder;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.SearchCriteria;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.Workflow;

import io.digitaljourney.platform.modules.rdb.api.exception.RDBClientException;
import io.digitaljourney.platform.modules.rdb.jpa.api.JPAProperties;
import io.digitaljourney.platform.modules.rdb.jpa.api.dao.AbstractJPADAO;

// @formatter:off
@Component(
	configurationPid = PurchaseOrderDAOConfig.CPID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	reference = {
		@Reference(
			name = JPAProperties.REF_CONTEXT,
			service = RDBContext.class, 
			cardinality = ReferenceCardinality.MANDATORY),
		@Reference(
			name = JPAProperties.REF_TX_CONTROL,
			service = TransactionControl.class,
			cardinality = ReferenceCardinality.MANDATORY),
		@Reference(
			name = JPAProperties.REF_PROVIDER,
			service = JPAEntityManagerProvider.class,
			cardinality = ReferenceCardinality.MANDATORY)
		})
@Designate(ocd = PurchaseOrderDAOConfig.class)
// @formatter:on
public final class PurchaseOrderDAOImpl extends AbstractJPADAO<PurchaseOrderDAOConfig> implements PurchaseOrderDAO {
    @Activate
    public void activate(ComponentContext ctx, PurchaseOrderDAOConfig config) {
	prepare(ctx, config);
    }

    @Modified
    public void modified(PurchaseOrderDAOConfig config) {
	prepare(config);
    }

    @Override
    public PurchaseOrder getPurchaseOrder(Long id) throws NotFoundException, UnexpectedPersistenceException {
	try {
	    return txSupports(em -> {
		TypedQuery<PurchaseOrder> namedQuery = em.createNamedQuery(PurchaseOrder.NAMED_QUERY_FIND_PURCHASEORDER_FETHING_ALL, PurchaseOrder.class);
		namedQuery.setParameter("purchaseOrderId", id);
		return namedQuery.getSingleResult();
	    });
	} catch (Exception e) {

	    if (e.getCause().getClass() == NoResultException.class) {

		throw new NotFoundException(PurchaseOrder.class);
	    } else {
		throw new UnexpectedPersistenceException(e.getMessage());
	    }
	}
    }

    @Override
    public synchronized PurchaseOrder addPurchaseOrder(PurchaseOrder purchaseOrder) throws UniqueConstraintViolationException, UnexpectedPersistenceException {
	try {

	    if (purchaseOrder.getNumber() == null && purchaseOrder.getVersion() == null) {
		purchaseOrder.setNumber(getNextPurchaseOrderNumber());
		purchaseOrder.setVersion(1L);
	    }

	    // We have to set the first history reference to the first step on the workflow
	    //@formatter:off
	    History firstHistory = 
		    purchaseOrder.getHistories()
		    	.stream()
		    	.findFirst()
		    	.orElse(null);
	    if (firstHistory != null) {
		
		firstHistory.setWorkflowStep(
			purchaseOrder.getWorkflow().getWorkflowStepByIndex(1));
	    }
	    //@formatter:on

	    setPurchaseOrderMappedReferences(purchaseOrder);
	    return save(purchaseOrder);

	} catch (EntityExistsException e) {

	    throw new UniqueConstraintViolationException(e.getMessage(), UniqueConstraintViolationException.PURCHASE_ORDER_ID);
	} catch (Exception e) {

	    if (e.getMessage() != null && e.getMessage().toLowerCase().contains(PurchaseOrder.PURCHASE_ORDER_UNIQUE_VERSION_AND_NUMBER.toLowerCase())) {
		throw new UniqueConstraintViolationException(e.getMessage(), UniqueConstraintViolationException.PURCHASE_ORDER_NUMBER_VERSION);
	    }

	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public History addHistory(History history) throws UnexpectedPersistenceException {

	setAttachmentMappedReferences(history);

	try {
	    return save(history);
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public List<History> addHistories(List<History> histories) throws UnexpectedPersistenceException {

	try {
	    txRequired(em -> {
		histories.forEach(history -> {
		    setAttachmentMappedReferences(history);
		    em.persist(history);
		    em.flush();
		});
	    });

	    return histories;
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public List<History> searchPurchaseOrderHistory(Long purchaseOrderId) throws UnexpectedPersistenceException {
	try {
	    return txSupports(em -> {
		TypedQuery<History> namedQuery = em.createNamedQuery(History.NAMED_QUERY_FIND_HISTORY, History.class);
		namedQuery.setParameter("purchaseOrderId", purchaseOrderId);
		return namedQuery.getResultList();
	    });
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public Workflow searchPurchaseOrderWorkFlow(Long purchaseOrderId) throws UnexpectedPersistenceException {
	try {
	    return txSupports(em -> {
		TypedQuery<Workflow> namedQuery = em.createNamedQuery(Workflow.NAMED_QUERY_FIND_WORKFLOW_BY_PURCHASE_ORDER_ID, Workflow.class);
		namedQuery.setParameter("purchaseOrderId", purchaseOrderId);
		return namedQuery.getSingleResult();
	    });
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public Workflow updateWorkFlow(Workflow workflow, Long purchaseOrderId, String purchaseOrderStatus, LocalDateTime approvalDate) throws UnexpectedPersistenceException {

	try {
	    return txRequired(em -> {
		Workflow mergedEntity = em.merge(workflow);

		if (purchaseOrderStatus != null && purchaseOrderStatus.trim().isEmpty() == false) { // Only update in case of the purchaseOrderStatus is different of null and is
												    // not empty
		    CriteriaBuilder cb = em.getCriteriaBuilder();

		    CriteriaUpdate<PurchaseOrder> updatePurchaseOrderStatusCriteria = cb.createCriteriaUpdate(PurchaseOrder.class);

		    Root<PurchaseOrder> root = updatePurchaseOrderStatusCriteria.from(PurchaseOrder.class);

		    updatePurchaseOrderStatusCriteria.set("status", purchaseOrderStatus);
		    updatePurchaseOrderStatusCriteria.set("approvalDate", approvalDate); //TODO: change to finish date

		    updatePurchaseOrderStatusCriteria.where(cb.equal(root.get("id"), purchaseOrderId));

		    em.createQuery(updatePurchaseOrderStatusCriteria).executeUpdate();
		}

		em.flush();

		return mergedEntity;
	    });
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}

    }

    @SuppressWarnings("unchecked")
    public List<PurchaseOrder> searchPurchaseOrders(SearchCriteria search) throws UnexpectedPersistenceException {
	try {
	    // @formatter:off
	    return txSupports(em -> {
		return em
			.createQuery(new PurchaseOrderQuery.Builder()
				.scope(search.scope)
				.approvedOn(search.approvalDate)
				.createdOn(search.createdDate)
				.withDepartment(search.departmentCode)
				.withEntity(search.entityCode)
				.withInternalOrder(search.internalOrderCode)
				.withNumber(search.number)
				.withProject(search.projectCode)
				.withStatus(search.status)
				.withType(search.type)
				.withTotalBetween(search.minVATValue, search.maxVATValue)
				.orderBy(search.orderField, search.orderType).build())
			.setMaxResults(getConfig().maxRecords()).getResultList();
	    });
	    // @formatter:on
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public Attachment getPurchaseOrderAttachment(Long purchaseOrderId, int index) throws NotFoundException, UnexpectedPersistenceException {
	try {
	    return txSupports(em -> {
		TypedQuery<Attachment> namedQuery = em.createNamedQuery(Attachment.NAMED_QUERY_FIND_ATTACHMENT_FETCHING_ONE, Attachment.class);
		namedQuery.setParameter("purchaseOrderId", purchaseOrderId);
		namedQuery.setParameter("idx", index);
		return namedQuery.getSingleResult();
	    });
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public List<Attachment> getPurchaseOrderAttachments(Long purchaseOrderId) throws NotFoundException, UnexpectedPersistenceException {
	try {
	    return txSupports(em -> {
		TypedQuery<Attachment> namedQuery = em.createNamedQuery(Attachment.NAMED_QUERY_FIND_ATTACHMENT_FETCHING_ALL, Attachment.class);
		namedQuery.setParameter("purchaseOrderId", purchaseOrderId);
		return namedQuery.getResultList();
	    });
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public Attachment updateAttachment(Attachment attachment) throws UnexpectedPersistenceException{
	try {
	    return txRequired(em -> {
		Attachment mergedEntity = em.merge(attachment);
		em.flush();
		return mergedEntity;
	    });
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }
    
    @Override
    public List<Long> findProcessToSubmit(List<String> purchaseOrderTypes) throws UnexpectedPersistenceException {
	try {
	    return txSupports(em -> {
		TypedQuery<Long> namedQuery = em.createNamedQuery(PurchaseOrder.NAMED_QUERY_FIND_PURCHASEORDER_IDS_TO_SUBMIT, Long.class);
		namedQuery.setParameter("poTypes", purchaseOrderTypes);
		return namedQuery.getResultList();
	    });
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    
    //TODO: criar trigger na BD para buscar o proximo numero no save
    private Long getNextPurchaseOrderNumber() throws UnexpectedPersistenceException {
	Long currentMaxNumber = 0L;
	try {
	    currentMaxNumber = txSupports(em -> {
		TypedQuery<Long> namedQuery = em.createNamedQuery(PurchaseOrder.NAMED_QUERY_GET_MAX_PURCHASEORDER_NUMBER, Long.class);
		return namedQuery.getSingleResult();
	    });
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}

	//@formatter:off
	return currentMaxNumber != null 
		? currentMaxNumber + 1
		: 1L;
	//@formatter:on
    }

    private void setPurchaseOrderMappedReferences(PurchaseOrder purchaseOrder) {
	if (purchaseOrder != null && purchaseOrder.getItems() != null) {
	    purchaseOrder.getItems().forEach(item -> item.setPurchaseOrder(purchaseOrder));
	}
	setHistoryMappedReferences(purchaseOrder);
	setWorkFlowStepsMappedReferences(purchaseOrder);
    }

    private void setWorkFlowStepsMappedReferences(PurchaseOrder purchaseOrder) {
	if (purchaseOrder.getWorkflow() != null && purchaseOrder.getWorkflow().getSteps() != null) {
	    purchaseOrder.getWorkflow().getSteps().forEach(step -> {
		step.setWorkflow(purchaseOrder.getWorkflow());
	    });
	}
    }

    private void setHistoryMappedReferences(PurchaseOrder purchaseOrder) {
	if (purchaseOrder != null && purchaseOrder.getHistories() != null) {
	    purchaseOrder.getHistories().forEach(history -> {
		history.setPurchaseOrder(purchaseOrder);
		setAttachmentMappedReferences(history);
	    });
	}
    }

    private void setAttachmentMappedReferences(History history) {
	if (history.getAttachments() != null) {
	    history.getAttachments().forEach(attachment -> attachment.setHistory(history));
	}
    }

    @Override
    public void updatePurchaseOrderStatus(final PurchaseOrder purchaseOrder, final String status) throws UnexpectedPersistenceException {
	
	try {
	    
	    this.txRequired(em -> {
		
		if (purchaseOrder != null && !StringUtils.isBlank(status) && !purchaseOrder.getStatus().equals(status)) {
		    
		    final CriteriaBuilder cb = em.getCriteriaBuilder();
		    final CriteriaUpdate<PurchaseOrder> updatePurchaseOrderStatusCriteria = cb.createCriteriaUpdate(PurchaseOrder.class);
		    final Root<PurchaseOrder> root = updatePurchaseOrderStatusCriteria.from(PurchaseOrder.class);
		    
		    updatePurchaseOrderStatusCriteria.set("status", status);
		    updatePurchaseOrderStatusCriteria.where(cb.equal(root.get("id"), purchaseOrder.getId()));
		    em.createQuery(updatePurchaseOrderStatusCriteria).executeUpdate();
		    
		}
		
		em.flush();
		
	    });
	    
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
	
    }
    
    @Override
    public PurchaseOrder getApprovedPurchaseOrderByNumber(final Long number) throws NotFoundException, UnexpectedPersistenceException {
	
	try {
	
	    return txSupports(em -> {
		
		TypedQuery<PurchaseOrder> namedQuery = em.createNamedQuery(PurchaseOrder.NAMED_QUERY_FIND_APPROVED_PURCHASEORDER_BY_NUMBER, PurchaseOrder.class);
		namedQuery.setParameter("purchaseOrderNumber", number);
		
		return namedQuery.getSingleResult();
		
	    });
	    
	} catch (Exception e) {

	    if (e.getCause().getClass() == NoResultException.class) {
		throw new NotFoundException(PurchaseOrder.class);
	    } else {
		throw new UnexpectedPersistenceException(e.getMessage());
	    }
	    
	}
	
    }
    
    @Override
    public Long getPurchaseOrderIdByInstanceId(Long instanceId) throws UnexpectedPersistenceException {
	try {
	    return txSupports(em -> {
		TypedQuery<Long> namedQuery = em.createNamedQuery(PurchaseOrder.NAMED_QUERY_FIND_PURCHASE_ORDER_ID_BY_INSTANCE_ID, Long.class);
		namedQuery.setParameter("instanceId", instanceId);
		return namedQuery.getSingleResult();
	    });
	} catch (NoResultException | RDBClientException e) {
	    return null;
	}
	catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}
    }

    @Override
    public void deletePurchaseOrderById(Long id) throws UnexpectedPersistenceException, NotFoundException {

	PurchaseOrder purchaseOrder = this.getPurchaseOrder(id);
	try {
//	    delete(financialDocument);
	    txRequired(em -> {
		em.remove(em.getReference(PurchaseOrder.class, purchaseOrder.getId()));
	    });
	    
	} catch (Exception e) {
	    throw new UnexpectedPersistenceException(e.getMessage());
	}

    }
    
}
