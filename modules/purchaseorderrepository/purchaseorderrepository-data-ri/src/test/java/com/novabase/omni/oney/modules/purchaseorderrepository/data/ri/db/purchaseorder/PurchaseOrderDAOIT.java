package com.novabase.omni.oney.modules.purchaseorderrepository.data.ri.db.purchaseorder;

import static io.digitaljourney.platform.modules.paxexam.BundleOptions.jpaClient;
import static io.digitaljourney.platform.modules.paxexam.BundleOptions.mavenBundles;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.jpaClientConfiguration;
import static io.digitaljourney.platform.modules.paxexam.ConfigOptions.newConsumerConfiguration;
import static org.ops4j.pax.exam.CoreOptions.composite;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;

import com.novabase.omni.oney.modules.purchaseorderrepository.data.ri.rdb.purchaseorder.PurchaseOrderDAOConfig;

import io.digitaljourney.platform.modules.paxexam.base.BaseTestSupport;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class PurchaseOrderDAOIT extends BaseTestSupport {

  @Override
  protected Option bundles() {
    return composite(super.bundles(), jpaClient(),
        mavenBundles("com.novabase.omni.oney.modules", "purchaseorderrepository-entity"),
        testBundle("com.novabase.omni.oney.modules", "purchaseorderrepository-data-ri"));
  }

  @Override
  protected Option configurations() {
    return composite(super.configurations(),
        jpaClientConfiguration("purchaseorderrepository", newConsumerConfiguration(PurchaseOrderDAOConfig.CPID, "purchaseorderrepository")));
  }

  @Test
  public void testBundle() {
    assertBundleActive("com.novabase.omni.oney.modules.purchaseorderrepository-data-ri");
  }

}
