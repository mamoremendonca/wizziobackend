package com.novabase.omni.oney.modules.purchaseorderrepository.data.ri.db.purchaseorder;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;
import org.osgi.service.transaction.control.TransactionControl;

import com.novabase.omni.oney.modules.purchaseorderrepository.data.ri.rdb.purchaseorder.PurchaseOrderDAOImpl;
import com.novabase.omni.oney.modules.purchaseorderrepository.entity.PurchaseOrder;

public class PurchaseOrderDAOImplTest {

    public static final String DEFAULT_PURCHASE_ORDER_FRIENDLY_NUMBER = "PO00001";
    public static final String DEFAULT_PURCHASE_ORDER_NUMBER_STRING = "1";
    public static final Long DEFAULT_PURCHASE_ORDER_NUMBER_LONG = Long.parseLong(DEFAULT_PURCHASE_ORDER_NUMBER_STRING);
    
    @InjectMocks
    private PurchaseOrderDAOImpl purchaseOrderDAOImpl;
    
    @Mock
    private TransactionControl txControl;
    
    private PurchaseOrder defaultPurchaseOrder;
    
    @Before
    public void setUp() {
	
	MockitoAnnotations.initMocks(this);
	
//	this.defaultPurchaeOrder = new PurchaseOrder();
//	this.defaultPurchaeOrder.setId(1L);
//	
//	final Item item1 = new Item();
//	item1.setIndex(1);
//	final Item item2 = new Item();
//	item1.setIndex(2);
//	final Set<Item> items = new HashSet<Item>();
//	items.add(item1);
//	items.add(item2);
//	
//	this.defaultPurchaeOrder.setItems(items);
//	this.defaultPurchaeOrder.setWorkflow(new Workflow());
	
    }
    
    @Test
    public void findAppprovedPurchaseOrderByNumberTest() throws Exception {
	
	this.getApprovedPurchaseOrderByNumberMockitoBehavior(DEFAULT_PURCHASE_ORDER_NUMBER_LONG).thenReturn(this.defaultPurchaseOrder);
	
	final PurchaseOrder resultPurchaseOrder = this.purchaseOrderDAOImpl.getApprovedPurchaseOrderByNumber(DEFAULT_PURCHASE_ORDER_NUMBER_LONG);
	
	Assert.assertEquals(resultPurchaseOrder, this.defaultPurchaseOrder);
	
//	TODO: Review Due to Nullpointer Exception when using transactional context inside
	
    }
    
    private <T> OngoingStubbing<PurchaseOrder> getApprovedPurchaseOrderByNumberMockitoBehavior(final Long number) throws Exception {
	return Mockito.when(this.purchaseOrderDAOImpl.getApprovedPurchaseOrderByNumber(number));
    }
    
}
