package com.novabase.omni.oney.modules.workflow.data.api.exception;

public class WorkflowException extends Exception {

    private static final long serialVersionUID = -2084716589989109199L;

    public WorkflowException(String msg) {
	super(msg);
    }
    
    public WorkflowException(Exception e) {
	super(e);
    }
}
