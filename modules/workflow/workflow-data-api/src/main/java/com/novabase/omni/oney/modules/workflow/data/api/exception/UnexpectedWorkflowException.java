package com.novabase.omni.oney.modules.workflow.data.api.exception;

public class UnexpectedWorkflowException extends WorkflowException {

    private static final long serialVersionUID = -2564304638234371269L;

    public UnexpectedWorkflowException(String msg) {
	super(msg);
    }
    
    public UnexpectedWorkflowException(Exception e) {
	super(e);
    }
}
