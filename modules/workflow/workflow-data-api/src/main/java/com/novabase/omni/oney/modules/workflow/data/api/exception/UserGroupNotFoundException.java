package com.novabase.omni.oney.modules.workflow.data.api.exception;

public class UserGroupNotFoundException extends WorkflowException {

    private static final long serialVersionUID = -1214941428244468538L;

    public UserGroupNotFoundException(String msg) {
	super(msg);
    }
    
    public UserGroupNotFoundException(Exception e) {
	super(e);
    }
}
