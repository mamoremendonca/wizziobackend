package com.novabase.omni.oney.modules.workflow.data.api;

import java.util.List;

import com.novabase.omni.oney.modules.workflow.entity.Hierarchy;
import com.novabase.omni.oney.modules.workflow.entity.HierarchyNode;
import com.novabase.omni.oney.modules.workflow.entity.SearchHierarchy;

public interface HierarchyDAO {

    List<HierarchyNode> getDescendants(String group);

    List<HierarchyNode> getAscendents(String group);
    
    List<HierarchyNode> getDepartmentNodes(String department);
    
    List<HierarchyNode> getAllGroups();

    HierarchyNode getHierachyNodeByAttribute(String attribute, String value);

    //Workflow buildWorkflow(String group, SearchHierarchy search);
    
    Hierarchy loadHierarchy(Hierarchy root, boolean isRoot);

    <T> T findUpperNode(Class<T> classz, String group, SearchHierarchy search);
    
}