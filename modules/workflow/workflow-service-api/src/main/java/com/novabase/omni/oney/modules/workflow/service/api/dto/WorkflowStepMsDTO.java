package com.novabase.omni.oney.modules.workflow.service.api.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("WorkflowStepMsDTO")
public class WorkflowStepMsDTO extends DTO implements Serializable {

    private static final long serialVersionUID = 5631206613273122050L;
    
    @ApiModelProperty(value = "Index")
    public int index;
    
    @ApiModelProperty(value = "UserGroup")
    public String userGroup;

    @ApiModelProperty(value = "hasNextStep")
    public boolean hasNextStep;

    @ApiModelProperty(value = "isActive")
    public boolean active;
}