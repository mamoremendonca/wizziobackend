package com.novabase.omni.oney.modules.workflow.service.api.exception;

import io.digitaljourney.platform.modules.commons.context.Context;
import io.digitaljourney.platform.modules.commons.exception.PlatformCode;
import io.digitaljourney.platform.modules.ws.rs.api.exception.RSException;
import com.novabase.omni.oney.modules.workflow.service.api.WorkflowProperties;

public class WorkflowException extends RSException {
	private static final long serialVersionUID = -5530211240072278623L;

	protected WorkflowException(PlatformCode statusCode, String errorCode, Context ctx, Object... args) {
		super(statusCode, errorCode, ctx, args);
	}

	protected WorkflowException(Context ctx, Object... args) {
		this(PlatformCode.INTERNAL_SERVER_ERROR, WorkflowProperties.WORKFLOW000, ctx, args);
	}

	protected WorkflowException(String errorCode, Context ctx, Object... args) {
		this(PlatformCode.BUSINESS_ERROR, errorCode, ctx, args);
	}

	public static WorkflowException of(Context ctx, String message) {
		return new WorkflowException(ctx, message);
	}

	public static WorkflowException of(Context ctx, Throwable cause) {
		return new WorkflowException(ctx, cause);
	}
}
