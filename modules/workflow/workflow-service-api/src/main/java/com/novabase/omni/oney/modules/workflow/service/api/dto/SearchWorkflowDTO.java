package com.novabase.omni.oney.modules.workflow.service.api.dto;

import java.io.Serializable;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("SearchWorkflowDTO")
public class SearchWorkflowDTO extends DTO implements Serializable {

    private static final long serialVersionUID = -876278333047279934L;
    
    @ApiModelProperty(value = "Variables")
    public Map<String, String> variables;
}
