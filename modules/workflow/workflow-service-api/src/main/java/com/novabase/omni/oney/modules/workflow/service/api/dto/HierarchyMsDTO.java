package com.novabase.omni.oney.modules.workflow.service.api.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("HierarchyMsDTO")
public class HierarchyMsDTO extends DTO implements Serializable {

    private static final long serialVersionUID = -8018065186028091163L;

    private boolean isRoot;
    private HierarchyNodeMsDTO node;
    private HierarchyRelationshipsMsDTO relationships;

    // Getter Methods

    public HierarchyRelationshipsMsDTO getRelationships() {
	return relationships;
    }

    public void setRelationships(HierarchyRelationshipsMsDTO relationships) {
	this.relationships = relationships;
    }

    public HierarchyNodeMsDTO getNode() {
	return node;
    }

    public void setNode(HierarchyNodeMsDTO node) {
	this.node = node;
    }

    public boolean isRoot() {
        return isRoot;
    }

    public void setRoot(boolean isRoot) {
        this.isRoot = isRoot;
    }
}