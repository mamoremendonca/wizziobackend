package com.novabase.omni.oney.modules.workflow.service.api;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.osgi.annotation.versioning.ProviderType;

import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyNodeMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.SearchWorkflowMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.WorkflowMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.enums.NodeAttributeType;

import io.digitaljourney.platform.modules.commons.type.HttpStatusCode;
import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.ApiKeyAuthDefinition.ApiKeyLocation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.BasicAuthDefinition;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;

@ProviderType
@Path("")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

@SwaggerDefinition(securityDefinition = @SecurityDefinition(basicAuthDefinitions = { @BasicAuthDefinition(key = RSProperties.SWAGGER_BASIC_AUTH) }, apiKeyAuthDefinitions = {
	@ApiKeyAuthDefinition(key = RSProperties.SWAGGER_BEARER_AUTH, name = RSProperties.HTTP_HEADER_API_KEY, in = ApiKeyLocation.HEADER) }), schemes = { SwaggerDefinition.Scheme.HTTP,
		SwaggerDefinition.Scheme.HTTPS, SwaggerDefinition.Scheme.DEFAULT })
@Api(value = "Workflow Management", authorizations = { @Authorization(value = RSProperties.SWAGGER_BASIC_AUTH), @Authorization(value = RSProperties.SWAGGER_BEARER_AUTH) })
@ApiResponses(value = { @ApiResponse(code = HttpStatusCode.UNAUTHORIZED_CODE, message = RSProperties.SWAGGER_UNAUTHORIZED_MESSAGE),
	@ApiResponse(code = HttpStatusCode.FORBIDDEN_CODE, message = RSProperties.SWAGGER_FORBIDDEN_MESSAGE) })
public interface WorkflowResource {

    @POST
    @Path("/{group}/children/{attribute}")
    @ApiOperation(value = "Returns the complete lower hierarchy for a specific UserGroup (position)", response = String.class, responseContainer = "List")
    List<String> getDescendants(
	    @ApiParam(value = "Current user group", required = true) @PathParam("group") String group,
	    @ApiParam(value = "Attribute", required = true) @PathParam("attribute") NodeAttributeType attributeType);

    @POST
    @Path("/{group}/parent")
    @ApiOperation(value = "Returns the complete direct upper hierarchy for a specific UserGroup (position)", response = String.class, responseContainer = "List")
    List<String> getAscendents(@ApiParam(value = "Current user group", required = true) @PathParam("group") String group);

    @POST
    @Path("/{group}/departments")
    @ApiOperation(value = "Returns the department attribute for a specific UserGroup (position)", response = String.class, responseContainer = "List")
    List<String> getDepartmentsByGroup(@ApiParam(value = "Current user group", required = true) @PathParam("group") String group);

    @GET
    @Path("/groups")
    @ApiOperation(value = "Returns the department attribute for all UserGroups (positions)", response = String.class, responseContainer = "List")
    List<HierarchyNodeMsDTO> getAllGroups();
    
    @POST
    @Path("/{group}/workflow")
    @ApiOperation(value = "Generates the Workflow (steps) based on a specific UserGroup (position) and attributes (value)", response = String.class, responseContainer = "List")
    WorkflowMsDTO generateWorkflow(
	    @ApiParam(value = "Current user group", required = true) @PathParam("group") String group, 
	    @ApiParam(value = "Workflow search parameters", required = true) SearchWorkflowMsDTO search);
    
    @POST
    @Path("/{group}/workflow/2steps")
    @ApiOperation(value = "Generates the Workflow (steps) based on a specific UserGroup (position) and attributes (value)", response = String.class, responseContainer = "List")
    WorkflowMsDTO generateWorkflowWith2ActiveSteps(
	    @ApiParam(value = "Current user group", required = true) @PathParam("group") String group, 
	    @ApiParam(value = "Workflow search parameters", required = true) SearchWorkflowMsDTO search);
    
    @POST
    @Path("/{departmentCode}/workflowDepartment")
    @ApiOperation(value = "Generates the Workflow (steps) based on a specific DepartmentCode (department) and attributes (value)", response = String.class, responseContainer = "List")
    WorkflowMsDTO generateWorkflowByDepartment(
	    @ApiParam(value = "Department Code", required = true) @PathParam("departmentCode") String departmentCode, 
	    @ApiParam(value = "Workflow search parameters", required = true) SearchWorkflowMsDTO search);

    @POST
    @Path("/hierarchy")
    @ApiOperation(value = "Loads a hierachy into the GraphDB")
    void loadHierarchy(@ApiParam(value = "Hierarchy", required = true) HierarchyMsDTO hierarchy);
    
    
}
