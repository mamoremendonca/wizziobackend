package com.novabase.omni.oney.modules.workflow.service.api;

import io.digitaljourney.platform.modules.ws.rs.api.RSProperties;

public abstract class WorkflowProperties extends RSProperties {
	public static final String WORKFLOW000 = "WORKFLOW000";
}
