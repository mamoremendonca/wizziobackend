package com.novabase.omni.oney.modules.workflow.service.api.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("HierarchyRelationshipDTO")
public class HierarchyRelationshipDTO extends DTO implements Serializable {

    private static final long serialVersionUID = 3279044310672426761L;

    private Map<String, String> attributes;
    private List<HierarchyNodeDTO> descendants;
    
    public Map<String, String> getAttributes() {
	return attributes;
    }
    public void setAttributes(Map<String, String> attributes) {
	this.attributes = attributes;
    }
    public List<HierarchyNodeDTO> getDescendants() {
	return descendants;
    }
    public void setDescendants(List<HierarchyNodeDTO> descendants) {
	this.descendants = descendants;
    }
    
    
}
