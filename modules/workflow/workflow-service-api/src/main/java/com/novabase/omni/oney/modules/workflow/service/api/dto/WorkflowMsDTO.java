package com.novabase.omni.oney.modules.workflow.service.api.dto;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("WorkflowMsDTO")
public class WorkflowMsDTO extends DTO implements Serializable {

    private static final long serialVersionUID = -876278333047279934L;
    
    @ApiModelProperty
    public LocalDateTime creationDate;
//    @ApiModelProperty
//    public Long workflowId;
//    @ApiModelProperty
//    public Long ownerId;
    @ApiModelProperty
    public int currentIndex;
    @ApiModelProperty
    public String currentUserGroup;
//    @ApiModelProperty
//    public Date limitDateToNextStep;
    @ApiModelProperty
    public boolean hasNextStep;
//    @ApiModelProperty
//    public Double totalWithoutTax;
    
    @ApiModelProperty(value = "Steps")
    public List<WorkflowStepMsDTO> steps;
    
    public WorkflowMsDTO() {
	steps = new ArrayList<WorkflowStepMsDTO>();
    }
}
