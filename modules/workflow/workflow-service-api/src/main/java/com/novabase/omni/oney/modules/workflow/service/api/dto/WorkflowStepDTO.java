package com.novabase.omni.oney.modules.workflow.service.api.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("WorkflowStepDTO")
public class WorkflowStepDTO extends DTO implements Serializable {

    private static final long serialVersionUID = 5631206613273122050L;
    
    @ApiModelProperty(value = "Index")
    private int index;
    
    @ApiModelProperty(value = "UserGroup")
    private String userGroup;

    @ApiModelProperty(value = "hasNextStepo")
    private boolean hasNextStep;

    public WorkflowStepDTO() {
	
	super();
	
	this.index = 0;
	this.hasNextStep = Boolean.FALSE;
	
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public boolean isHasNextStep() {
        return hasNextStep;
    }

    public void setHasNextStep(boolean hasNextStep) {
        this.hasNextStep = hasNextStep;
    }
    
}

