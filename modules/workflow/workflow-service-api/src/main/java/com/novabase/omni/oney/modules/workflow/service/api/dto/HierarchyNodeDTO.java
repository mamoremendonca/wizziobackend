package com.novabase.omni.oney.modules.workflow.service.api.dto;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("HierarchyDTO")
public class HierarchyNodeDTO extends DTO implements Serializable {

    private static final long serialVersionUID = -8018065186028091163L;

    private String name;
    private String description;
    private String userGroup;
    private int hierarchycalLevel;
    private List<HierarchyRelationshipDTO> relationships;

    // Getter Methods

    public String getName() {
	return name;
    }

    public String getDescription() {
	return description;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getUserGroup() {
	return userGroup;
    }

    public float getHierarchycalLevel() {
	return hierarchycalLevel;
    }

    public void setUserGroup(String userGroup) {
	this.userGroup = userGroup;
    }

    public void setHierarchycalLevel(int hierarchycalLevel) {
	this.hierarchycalLevel = hierarchycalLevel;
    }

    public List<HierarchyRelationshipDTO> getRelationships() {
	return relationships;
    }

    public void setRelationships(List<HierarchyRelationshipDTO> relationships) {
	this.relationships = relationships;
    }
}