package com.novabase.omni.oney.modules.workflow.service.api.enums;

public enum NodeAttributeType {
    // @formatter:off
    NAME, 
    DESCRIPTION, 
    GROUP, 
    AREA,
    DEPARTMENT;
    // @formatter:on

    @SuppressWarnings("unchecked")
    public static <T extends Enum<NodeAttributeType>> T getEnum(String name){

	Enum<NodeAttributeType> value = NodeAttributeType.NAME;
	try {
	    value = NodeAttributeType.valueOf(name);
	} catch (Throwable t) {
	    // DO nothing
	}
	return (T) value;
    }
}
