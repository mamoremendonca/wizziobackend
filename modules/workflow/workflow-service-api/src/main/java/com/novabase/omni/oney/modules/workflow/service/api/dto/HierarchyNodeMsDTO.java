package com.novabase.omni.oney.modules.workflow.service.api.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.osgi.dto.DTO;

import io.swagger.annotations.ApiModel;
import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
@ApiModel("HierarchyNodeMsDTO")
public class HierarchyNodeMsDTO extends DTO implements Serializable {

    private static final long serialVersionUID = -8018065186028091163L;

    private String name;
    private String description;
    private String group;
    private String department;
    private String area;
    private int hierarchycalLevel;

    // Getter Methods

    public String getName() {
	return name;
    }

    public String getDescription() {
	return description;
    }

    public void setName(String name) {
	this.name = name;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getGroup() {
	return group;
    }

    public float getHierarchycalLevel() {
	return hierarchycalLevel;
    }

    public void setGroup(String group) {
	this.group = group;
    }

    public void setHierarchycalLevel(int hierarchycalLevel) {
	this.hierarchycalLevel = hierarchycalLevel;
    }

    public String getDepartment() {
	return department;
    }

    public void setDepartment(String department) {
	this.department = department;
    }
    
    public String getArea() {
	return area;
    }

    public void setArea(String area) {
	this.area = area;
    }
}