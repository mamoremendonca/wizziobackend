package com.novabase.omni.oney.modules.workflow.entity;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
public class Workflow implements Serializable {

    private static final long serialVersionUID = -5011684989158769959L;
    
    private List<WorkflowStep> workflowSteps;

    public List<WorkflowStep> getWorkflowSteps() {
	return workflowSteps;
    }

    public void setWorkflowSteps(List<WorkflowStep> workflowSteps) {
	this.workflowSteps = workflowSteps;
    }
    
    public static class JsonRawDeserializer extends JsonDeserializer<String> {
	@Override
	public String deserialize(JsonParser jp, DeserializationContext context) throws IOException {
	    return jp.readValueAsTree().toString();
	}
    }
    }
