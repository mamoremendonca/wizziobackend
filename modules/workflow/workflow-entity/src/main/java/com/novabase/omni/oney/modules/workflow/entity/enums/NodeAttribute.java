package com.novabase.omni.oney.modules.workflow.entity.enums;

public enum NodeAttribute {
    //@formatter:off
    NAME("name"), 
    DESCRIPTION("description"), 
    GROUP("group"), 
    AREA("area"), 
    DEPARTMENT("department");
    //@formatter:off
    
    private String field;
    
    private NodeAttribute(String field) {
	this.field = field;
    }
    
    public String getField() {
	return this.field;
    }
    
    @SuppressWarnings("unchecked")
    public static <T extends Enum<NodeAttribute>> T getEnum(String name){

	Enum<NodeAttribute> value = NodeAttribute.NAME;
	try {
	    value = NodeAttribute.valueOf(name);
	} catch (Throwable t) {
	    // DO nothing
	}
	return (T) value;
    }
    
}
