package com.novabase.omni.oney.modules.workflow.entity;

import java.io.IOException;
import java.io.Serializable;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
public class WorkflowStep implements Serializable {

    private static final long serialVersionUID = -4275007808737036917L;

    public int index;

    public String group;

    public boolean hasNextStep;
    
    public boolean active;
    
    public static class JsonRawDeserializer extends JsonDeserializer<String> {
	@Override
	public String deserialize(JsonParser jp, DeserializationContext context) throws IOException {
	    return jp.readValueAsTree().toString();
	}
    }    
}
