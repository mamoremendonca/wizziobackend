package com.novabase.omni.oney.modules.workflow.entity;

import java.io.IOException;
import java.io.Serializable;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.novabase.omni.oney.modules.workflow.entity.enums.NodeAttribute;

import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
public class HierarchyNode implements Serializable {

    // TODO: considerar a criação de subClasses para tipos de nós (PO, DF, etc...)
    
    private static final long serialVersionUID = -3910019252494075806L;

    private String name;
    private String description;
    private String group;
    private String department;
    private String area;
    private int hierarchycalLevel;
        
    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getArea() {
	return area;
    }

    public void setArea(String area) {
	this.area = area;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getGroup() {
	return group;
    }

    public void setGroup(String group) {
	this.group = group;
    }

    public int getHierarchycalLevel() {
	return hierarchycalLevel;
    }

    public void setHierarchycalLevel(int hierarchycalLevel) {
	this.hierarchycalLevel = hierarchycalLevel;
    }

    public String getDepartment() {
	return department;
    }

    public void setDepartment(String department) {
	this.department = department;
    }

    public static class JsonRawDeserializer extends JsonDeserializer<String> {
	@Override
	public String deserialize(JsonParser jp, DeserializationContext context) throws IOException {
	    return jp.readValueAsTree().toString();
	}
    }
    
    public String getAttribute(String attributeName) {
	NodeAttribute type = NodeAttribute.getEnum(attributeName.toUpperCase());
	switch (type) {
	case NAME:
	    return this.name;
	case DESCRIPTION:
	    return this.description;
	case GROUP:
	    return this.group;
	case DEPARTMENT:
	    return this.department;
	case AREA:
	    return this.area;
	default:
	    return "";
	}
    }
}
