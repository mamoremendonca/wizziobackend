package com.novabase.omni.oney.modules.workflow.entity;

import java.io.IOException;
import java.io.Serializable;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
public class SearchHierarchy implements Serializable {

    private static final long serialVersionUID = -138048372122574897L;

    // TODO: add additional search attributes (DocumentType, Supplier, etc..)

    private Long value;

    public Long getValue() {
	return value;
    }

    public void setValue(Long value) {
	this.value = value;
    }

    public static class JsonRawDeserializer extends JsonDeserializer<String> {
	@Override
	public String deserialize(JsonParser jp, DeserializationContext context) throws IOException {
	    return jp.readValueAsTree().toString();
	}
    }

}
