package com.novabase.omni.oney.modules.workflow.nosql;

public class GraphClasses {

    public static final String GRAPH_HIERARCHYNODE = "HierarchyNode";
    public static final String GRAPH_HIERARCHYNODEPROPERTY = "HierarchyNodeProperty";
    public static final String GRAPH_HIERARCHYRELATIONSHIP = "HierarchyRelationship";

    
    public static final String GRAPH_EDGE_CHILD = "Child";
    public static final String GRAPH_EDGE_PARENT = "Parent";

}
