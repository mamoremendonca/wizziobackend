package com.novabase.omni.oney.modules.workflow.entity.enums;

public enum SearchDirection {
    UPPER, LOWER
}
