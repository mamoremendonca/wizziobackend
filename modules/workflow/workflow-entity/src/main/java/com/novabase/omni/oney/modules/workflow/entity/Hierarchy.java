package com.novabase.omni.oney.modules.workflow.entity;

import java.io.IOException;
import java.io.Serializable;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
public class Hierarchy implements Serializable {

    // TODO: considerar a criação de subClasses para tipos de nós (PO, DF, etc...)
    
    private static final long serialVersionUID = 3422200393840342287L;
    
    private HierarchyNode node;
    private HierarchyRelationships relationships;
        
    public HierarchyRelationships getRelationships() {
	return relationships;
    }

    public void setRelationships(HierarchyRelationships relationships) {
	this.relationships = relationships;
    }
    
    public HierarchyNode getNode() {
	return node;
    }

    public void setNode(HierarchyNode node) {
	this.node = node;
    }

    public static class JsonRawDeserializer extends JsonDeserializer<String> {
	@Override
	public String deserialize(JsonParser jp, DeserializationContext context) throws IOException {
	    return jp.readValueAsTree().toString();
	}
    }
}
