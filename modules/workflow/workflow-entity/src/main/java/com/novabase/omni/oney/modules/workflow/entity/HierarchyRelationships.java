package com.novabase.omni.oney.modules.workflow.entity;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import net.karneim.pojobuilder.GeneratePojoBuilder;

@GeneratePojoBuilder
public class HierarchyRelationships implements Serializable {

    private static final long serialVersionUID = -3910019252494075806L;

    private Map<String, String> attributes;
    private List<Hierarchy> descendants;

    public Map<String, String> getAttributes() {
	return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
	this.attributes = attributes;
    }

    public List<Hierarchy> getDescendants() {
	return descendants;
    }

    public void setDescendants(List<Hierarchy> descendants) {
	this.descendants = descendants;
    }

    public static class JsonRawDeserializer extends JsonDeserializer<String> {
	@Override
	public String deserialize(JsonParser jp, DeserializationContext context) throws IOException {
	    return jp.readValueAsTree().toString();
	}
    }
}
