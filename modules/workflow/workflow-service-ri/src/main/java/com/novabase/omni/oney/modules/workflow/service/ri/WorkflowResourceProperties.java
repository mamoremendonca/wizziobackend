package com.novabase.omni.oney.modules.workflow.service.ri;

import com.novabase.omni.oney.modules.workflow.service.api.WorkflowProperties;

public final class WorkflowResourceProperties extends WorkflowProperties {
	private WorkflowResourceProperties() {}
	
	private static final String RESOURCE_NAME = "workflow";
	
	public static final String ADDRESS = "/" + RESOURCE_NAME;
	
	public static final String DOCS_ADDRESS = CORE_RESOURCE_PATTERN + ADDRESS + DOCS_PATH;

	public static final String PERMISSION_ALL = RESOURCE_NAME + ACTION_ALL;
	public static final String PERMISSION_CREATE = RESOURCE_NAME + ACTION_CREATE;
	public static final String PERMISSION_READ = RESOURCE_NAME + ACTION_READ;
	public static final String PERMISSION_UPDATE = RESOURCE_NAME + ACTION_UPDATE;
	public static final String PERMISSION_DELETE = RESOURCE_NAME + ACTION_DELETE;
	public static final String PERMISSION_EXECUTE = RESOURCE_NAME + ACTION_EXECUTE;
}
