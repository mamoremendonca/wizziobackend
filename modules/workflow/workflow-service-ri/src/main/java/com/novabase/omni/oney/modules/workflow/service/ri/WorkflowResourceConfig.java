package com.novabase.omni.oney.modules.workflow.service.ri;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "%name", description = "%description",
	localization = "OSGI-INF/l10n/workflow",
	icon = @Icon(resource = "OSGI-INF/icon/resource.png", size = 32))
public @interface WorkflowResourceConfig {

	static final String CPID = "oney.base.modules.workflow.service.ri.workflow";
	
	@AttributeDefinition(name = "%event.topic", description = "%event.description")
	String eventTopic() default "io/digitaljourney/platform/plugins/modules/workflow";
	
	@AttributeDefinition(name = "%defaultLockTimeOut.name", description = "%defaultLockTimeOut.description")
	long defaultLockTimeOut() default 60;
	
	@AttributeDefinition(name = "%expireRecords.name", description = "%expireRecords.description")
	int expireRecords()  default 10000;
}
