package com.novabase.omni.oney.modules.workflow.service.ri;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.modules.workflow.data.api.HierarchyDAO;
import com.novabase.omni.oney.modules.workflow.entity.HierarchyNode;
import com.novabase.omni.oney.modules.workflow.entity.Workflow;
import com.novabase.omni.oney.modules.workflow.entity.enums.NodeAttribute;
import com.novabase.omni.oney.modules.workflow.service.api.WorkflowResource;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyNodeMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.SearchWorkflowMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.WorkflowMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.WorkflowStepMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.WorkflowStepMsDTOBuilder;
import com.novabase.omni.oney.modules.workflow.service.api.enums.NodeAttributeType;
import com.novabase.omni.oney.modules.workflow.service.api.exception.WorkflowException;
import com.novabase.omni.oney.modules.workflow.service.ri.mappers.SearchHierarchyMapper;
import com.novabase.omni.oney.modules.workflow.service.ri.mappers.WorkflowResourceMapper;

import io.digitaljourney.platform.modules.ws.rs.api.annotation.RSProvider;
import io.digitaljourney.platform.modules.ws.rs.api.resource.AbstractResource;
import io.swagger.annotations.ApiOperation;

// @formatter:off
@Component(
	configurationPid = WorkflowResourceConfig.CPID,
	configurationPolicy = ConfigurationPolicy.REQUIRE,
	reference = {
		@Reference(
			name = WorkflowResourceProperties.REF_CONTEXT,
			service = WorkflowContext.class,
			cardinality = ReferenceCardinality.MANDATORY)})
@Designate(ocd = WorkflowResourceConfig.class)
@RSProvider(WorkflowResourceProperties.ADDRESS)
// @formatter:on
public class WorkflowResourceImpl extends AbstractResource<WorkflowContext, WorkflowResourceConfig> implements WorkflowResource {

    @Reference
    private volatile HierarchyDAO hierarchyServiceDAO;

    @Activate
    public void activate(ComponentContext ctx, WorkflowResourceConfig config) {
	prepare(ctx, config);
    }

    @Override
    public List<String> getAscendents(String group) {

	List<HierarchyNode> ascendents = hierarchyServiceDAO.getAscendents(group);

	try {
	    if (ascendents == null) {
		throw WorkflowException.of(getCtx(), "UserGroup [" + group + "] could not be found");
	    }

	    // @formatter:off
		return ascendents
			.stream()
	                .map(d -> 
	                	d.getGroup()).distinct().collect(Collectors.toList());
		// @formatter:on
	} catch (WorkflowException ex) {
	    throw ex;
	} catch (Throwable t) {
	    throw WorkflowException.of(getCtx(), t);
	}
    }

    @Override
    public List<String> getDepartmentsByGroup(String group) {

	List<HierarchyNode> descendents = hierarchyServiceDAO.getDescendants(group);

	try {
	    if (descendents == null) {
		throw WorkflowException.of(getCtx(), "UserGroup [" + group + "] could not be found");
	    }

	    // @formatter:off
        	return descendents
        		.stream()
        		.map(d -> d.getAttribute(NodeAttribute.DEPARTMENT.name()))
        		.filter(d -> d != null && d != "")
        		.distinct()
        		.collect(Collectors.toList());
        	// @formatter:on
	} catch (WorkflowException ex) {
	    throw ex;
	} catch (Throwable t) {
	    throw WorkflowException.of(getCtx(), t);
	}
    }

    @Override
    public WorkflowMsDTO generateWorkflow(String group, SearchWorkflowMsDTO search) {
	return generateWorkflow(group, search, false);
    }
    

    @Override
    public WorkflowMsDTO generateWorkflowWith2ActiveSteps(String group, SearchWorkflowMsDTO search) {
	return generateWorkflow(group, search, true);
    }

    /**
     * If atLeastTwoActiveSteps is == true the generated workflow will be generated with at least two active steps. Otherwise will be generated only by parameters.
     * @param group
     * @param search
     * @param atLeastTwoActiveSteps
     * @return
     */
    private WorkflowMsDTO generateWorkflow(String group, SearchWorkflowMsDTO search, boolean atLeastTwoActiveSteps) {
	int index = 1;
	boolean isActive = true;
	boolean hasNextStep = true;
	boolean upperLevelReached = false;
	boolean requiresUpperLevel = false;
	WorkflowMsDTO wf = new WorkflowMsDTO();

	try {
	    // Traverse complete hierarchy (until top)
	    List<HierarchyNode> flow = hierarchyServiceDAO.getAscendents(group);
	    if (flow == null)
		throw WorkflowException.of(getCtx(), "Hiearchy for UserGroup [" + group + "] could not be determined");

	    // search for last node (based on search parameters)
	    HierarchyNode lastNode = hierarchyServiceDAO.findUpperNode(HierarchyNode.class, group, SearchHierarchyMapper.INSTANCE.toSearchWorkflow(search));
	    if (lastNode == null)
		throw WorkflowException.of(getCtx(), "Highest Approval level for UserGroup [" + group + "] could not be determined");

	    // Checks if current level is last;
	    requiresUpperLevel = (group.contentEquals(lastNode.getGroup())) && atLeastTwoActiveSteps;

	    for (HierarchyNode node : flow) {

		WorkflowStepMsDTO step = new WorkflowStepMsDTOBuilder().withUserGroup(node.getGroup()).withIndex(index++)
			.withHasNextStep(hasNextStep & (index < flow.size() || flow.size() == 1 || (requiresUpperLevel))).withActive(isActive).build();

		wf.steps.add(step);

		isActive = (isActive & ((!node.getName().equals(lastNode.getName()) && !upperLevelReached) || requiresUpperLevel));

		hasNextStep = isActive;
		step.hasNextStep = hasNextStep;

		requiresUpperLevel = requiresUpperLevel & !node.getName().equals(lastNode.getName());
		upperLevelReached = node.getName().equals(lastNode.getName());
	    }
	    if (flow.size() == 1) {
		wf.steps.add(new WorkflowStepMsDTOBuilder().withUserGroup(flow.get(0).getGroup()).withIndex(2).withHasNextStep(false).withActive(true).build());
	    }

	    wf.creationDate = LocalDateTime.now();
	    if (wf.steps != null && wf.steps.get(0) != null) {
		wf.currentIndex = wf.steps.get(0).index;
		wf.currentUserGroup = wf.steps.get(0).userGroup;
		wf.hasNextStep = wf.steps.get(0).hasNextStep;
	    }

	    return wf;
	} catch (WorkflowException ex) {
	    throw ex;
	} catch (Throwable t) {
	    throw WorkflowException.of(getCtx(), t);
	}
    }

    @Override
    public WorkflowMsDTO generateWorkflowByDepartment(String departmentCode, SearchWorkflowMsDTO search) {

	List<HierarchyNode> departmentNodes = null;

	try {
	    departmentNodes = hierarchyServiceDAO.getDepartmentNodes(departmentCode);
	} catch (Exception e) {
	    throw WorkflowException.of(getCtx(), e);
	}

	if (departmentNodes == null || departmentNodes.isEmpty() == true) {
	    throw WorkflowException.of(getCtx(), "Failed to generate workflow by department: " + departmentCode + ", could not found any node for this department");
	}

	//@formatter:off
	HierarchyNode highLevelDepartmentGroup = 
		departmentNodes
			.stream()
			.sorted((d1, d2) -> d1.getHierarchycalLevel() - d2.getHierarchycalLevel())
			.findFirst()
			.get();
	//@formatter:on

	return generateWorkflow(highLevelDepartmentGroup.getGroup(), search);

    }

    @Override
    public List<String> getDescendants(String group, NodeAttributeType attributeType) {
	try {
	    List<HierarchyNode> descendants = hierarchyServiceDAO.getDescendants(group);

	    if (descendants == null) {
		throw WorkflowException.of(getCtx(), "UserGroup [" + group + "] could not be found");
	    }

	    // @formatter:off
        	return descendants
        		.stream()
        		.map(d -> 
        			d.getAttribute(attributeType.name()))
        		.distinct()
        		.collect(Collectors.toList());
        	// @formatter:on
	} catch (WorkflowException ex) {
	    throw ex;
	} catch (Throwable t) {
	    throw WorkflowException.of(getCtx(), t);
	}
    }

    @Override
    public void loadHierarchy(HierarchyMsDTO hierarchy) {
	try {
	    hierarchyServiceDAO.loadHierarchy(WorkflowResourceMapper.INSTANCE.toHierarchy(hierarchy), hierarchy.isRoot());
	} catch (WorkflowException ex) {
	    throw ex;
	} catch (Throwable t) {
	    throw WorkflowException.of(getCtx(), t);
	}
    }
    
    @Override
    public List<HierarchyNodeMsDTO> getAllGroups() {
	
	try {
	    
	    final List<HierarchyNode> result = this.hierarchyServiceDAO.getAllGroups();
	    return WorkflowResourceMapper.INSTANCE.toHierarchyNodeMsDTOList(result);
	    
	} catch (WorkflowException ex) {
	    throw ex;
	} catch (Throwable t) {
	    throw WorkflowException.of(getCtx(), t);
	}
	
    }
    
}
