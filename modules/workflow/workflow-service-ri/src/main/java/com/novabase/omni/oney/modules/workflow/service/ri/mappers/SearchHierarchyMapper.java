package com.novabase.omni.oney.modules.workflow.service.ri.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.modules.workflow.entity.SearchHierarchy;
import com.novabase.omni.oney.modules.workflow.entity.SearchHierarchyBuilder;
import com.novabase.omni.oney.modules.workflow.service.api.dto.SearchWorkflowMsDTO;

@Mapper
public class SearchHierarchyMapper {

    public static final SearchHierarchyMapper INSTANCE = Mappers.getMapper(SearchHierarchyMapper.class);

    public SearchHierarchy toSearchWorkflow(SearchWorkflowMsDTO searchDTO) {
    	return new SearchHierarchyBuilder()
    		.withValue((searchDTO.variables.containsKey("value")) ? Long.valueOf(searchDTO.variables.get("value")) : 0)
    		.build();
    }
    
}

