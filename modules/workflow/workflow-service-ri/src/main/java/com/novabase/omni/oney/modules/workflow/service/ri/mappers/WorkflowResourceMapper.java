package com.novabase.omni.oney.modules.workflow.service.ri.mappers;

import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.factory.Mappers;

import com.novabase.omni.oney.modules.workflow.entity.Hierarchy;
import com.novabase.omni.oney.modules.workflow.entity.HierarchyNode;
import com.novabase.omni.oney.modules.workflow.entity.HierarchyRelationships;
import com.novabase.omni.oney.modules.workflow.entity.Workflow;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyNodeMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyRelationshipsMsDTO;
import com.novabase.omni.oney.modules.workflow.service.api.dto.WorkflowMsDTO;

@Mapper(uses = SearchHierarchyMapper.class,
	nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface WorkflowResourceMapper {
	
    public static final WorkflowResourceMapper INSTANCE = Mappers.getMapper(WorkflowResourceMapper.class);

    Hierarchy toHierarchy(HierarchyMsDTO hierarchy);
    HierarchyNode toHierarchyNode(HierarchyNodeMsDTO hierarchyNode);
    HierarchyRelationships toHierarchy(HierarchyRelationshipsMsDTO hierarchyRelationships);
    WorkflowMsDTO toWorkflowDTO(Workflow buildWorkflow);
    
    public HierarchyNodeMsDTO toHierarchyNodeMsDTO(final HierarchyNode node);
    
    @IterableMapping(elementTargetType = HierarchyNodeMsDTO.class)
    public List<HierarchyNodeMsDTO> toHierarchyNodeMsDTOList(final List<HierarchyNode> nodeList);
    
}
