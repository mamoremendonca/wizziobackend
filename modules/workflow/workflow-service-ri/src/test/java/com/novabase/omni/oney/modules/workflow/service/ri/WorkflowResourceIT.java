package com.novabase.omni.oney.modules.workflow.service.ri;

import static io.digitaljourney.platform.modules.paxexam.BundleOptions.mavenBundles;
import static io.digitaljourney.platform.modules.paxexam.BundleOptions.rsProviderWithGraphClient;
import static org.ops4j.pax.exam.CoreOptions.composite;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.spi.reactors.ExamReactorStrategy;
import org.ops4j.pax.exam.spi.reactors.PerClass;

import io.digitaljourney.platform.modules.paxexam.ConfigOptions;
import io.digitaljourney.platform.modules.paxexam.base.BaseTestSupport;
import com.novabase.omni.oney.modules.workflow.service.api.WorkflowResource;

@RunWith(PaxExam.class)
@ExamReactorStrategy(PerClass.class)
public class WorkflowResourceIT extends BaseTestSupport {

	@SuppressWarnings("unused")
    @Inject
    private WorkflowResource workflowResource;

    @Override
    protected Option bundles() {
	return composite(super.bundles(), rsProviderWithGraphClient(),
		mavenBundles("com.novabase.omni.oney.modules", "workflow-entity",
			"workflow-data-ri", "workflow-service-api"),
		testBundle("com.novabase.omni.oney.modules", "workflow-service-ri"));
    }

    @Override
    protected Option configurations() {
	return composite(super.configurations(), ConfigOptions.orientdbConfiguration("workflow"),
		ConfigOptions.rsProviderWithGraphClientConfiguration("workflow",
			ConfigOptions.newConsumerConfiguration(WorkflowResourceConfig.CPID, "workflow")));
    }

    @Test
    public void testBundle() {
	assertBundleActive("com.novabase.omni.oney.modules.workflow-service-ri");
    }

}
