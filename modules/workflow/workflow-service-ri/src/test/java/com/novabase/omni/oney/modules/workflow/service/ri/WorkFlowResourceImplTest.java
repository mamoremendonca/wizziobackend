package com.novabase.omni.oney.modules.workflow.service.ri;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.OngoingStubbing;

import com.novabase.omni.oney.modules.workflow.data.api.HierarchyDAO;
import com.novabase.omni.oney.modules.workflow.entity.HierarchyNode;
import com.novabase.omni.oney.modules.workflow.entity.HierarchyNodeBuilder;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyNodeMsDTO;

public class WorkFlowResourceImplTest {

	@InjectMocks
	private WorkflowResourceImpl workflowResourceImpl;
	
	@Mock
	private HierarchyDAO hierarchyServiceDAO;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getAllGroupsTest() throws Exception {
		
		List<HierarchyNode> nodeList = new ArrayList<HierarchyNode>();
		HierarchyNode hierarchyNode = new HierarchyNodeBuilder().withDepartment("D1").withDescription("Dep 1").withGroup("Group 1").build();
		nodeList.add(hierarchyNode);
		
		this.getAllGroupsMockitoBehavior().thenReturn(nodeList);
		
		List<HierarchyNodeMsDTO> toHierarchyNodeMsDTOList = this.workflowResourceImpl.getAllGroups();
		
		Assert.assertEquals(1, toHierarchyNodeMsDTOList.size());
		Assert.assertEquals(hierarchyNode.getDepartment(), toHierarchyNodeMsDTOList.get(0).getDepartment());
		Assert.assertEquals(hierarchyNode.getDescription(), toHierarchyNodeMsDTOList.get(0).getDescription());
		Assert.assertEquals(hierarchyNode.getGroup(), toHierarchyNodeMsDTOList.get(0).getGroup());
		
	}
	
	@Test(expected = Exception.class)
	public void getAllGroupsExceptionTest() throws Exception {
		
		List<HierarchyNode> nodeList = new ArrayList<HierarchyNode>();
		HierarchyNode hierarchyNode = new HierarchyNodeBuilder().withDepartment("D1").withDescription("Dep 1").withGroup("Group 1").build();
		nodeList.add(hierarchyNode);
		
		this.getAllGroupsMockitoBehavior().thenThrow(new Exception());
		
		List<HierarchyNodeMsDTO> toHierarchyNodeMsDTOList = this.workflowResourceImpl.getAllGroups();
		
	}
	
	private <T> OngoingStubbing<List<HierarchyNode>> getAllGroupsMockitoBehavior() throws Exception {
		return Mockito.when(this.hierarchyServiceDAO.getAllGroups());
	}
	
}
