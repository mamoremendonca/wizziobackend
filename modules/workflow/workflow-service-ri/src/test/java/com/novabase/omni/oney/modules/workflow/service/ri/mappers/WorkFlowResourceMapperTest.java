package com.novabase.omni.oney.modules.workflow.service.ri.mappers;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.novabase.omni.oney.modules.workflow.entity.HierarchyNode;
import com.novabase.omni.oney.modules.workflow.entity.HierarchyNodeBuilder;
import com.novabase.omni.oney.modules.workflow.service.api.dto.HierarchyNodeMsDTO;



public class WorkFlowResourceMapperTest {
	
	@Before
    public void setUp() {
		
	}
	
	@Test
	public void toHierarchyNodeMsDTOTest() {
		
		HierarchyNode hierarchyNode = new HierarchyNodeBuilder().withDepartment("D1").withDescription("Dep 1").withGroup("Group 1").build();
		HierarchyNodeMsDTO hierarchyNodeMs = WorkflowResourceMapper.INSTANCE.toHierarchyNodeMsDTO(hierarchyNode);
		
		Assert.assertEquals(hierarchyNode.getDepartment(), hierarchyNodeMs.getDepartment());
		Assert.assertEquals(hierarchyNode.getDescription(), hierarchyNodeMs.getDescription());
		Assert.assertEquals(hierarchyNode.getGroup(), hierarchyNodeMs.getGroup());
		
	}
	
	@Test
	public void toHierarchyNodeMsDTOListTest() {
		
		List<HierarchyNode> nodeList = new ArrayList<HierarchyNode>();
		HierarchyNode hierarchyNode = new HierarchyNodeBuilder().withDepartment("D1").withDescription("Dep 1").withGroup("Group 1").build();
		nodeList.add(hierarchyNode);
		
		List<HierarchyNodeMsDTO> toHierarchyNodeMsDTOList = WorkflowResourceMapper.INSTANCE.toHierarchyNodeMsDTOList(nodeList);
		
		Assert.assertEquals(1, toHierarchyNodeMsDTOList.size());
		Assert.assertEquals(hierarchyNode.getDepartment(), toHierarchyNodeMsDTOList.get(0).getDepartment());
		Assert.assertEquals(hierarchyNode.getDescription(), toHierarchyNodeMsDTOList.get(0).getDescription());
		Assert.assertEquals(hierarchyNode.getGroup(), toHierarchyNodeMsDTOList.get(0).getGroup());
		
	}
}
