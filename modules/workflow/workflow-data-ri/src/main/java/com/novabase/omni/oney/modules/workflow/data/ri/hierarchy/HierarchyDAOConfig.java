package com.novabase.omni.oney.modules.workflow.data.ri.hierarchy;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Icon;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "%name", description = "%description", localization = "OSGI-INF/l10n/hierarchy/hierarchy", icon = @Icon(resource = "OSGI-INF/icon/nosql.png", size = 32))
public @interface HierarchyDAOConfig {

    public static final String CPID = "com.novabase.omni.oney.modules.workflow.data.ri.hierarchy";

    @AttributeDefinition(name = "%dataSource_target.name", description = "%dataSource_target.description", required = true)
    String dataSource_target();

    @AttributeDefinition(name = "%maxDepth.name", description = "%maxDepth.description", required = false)
    int maxDepth() default 5;
    
    @AttributeDefinition(name = "%maxApprovalAmount.name", description = "%maxApprovalAmount.description", required = false)
    String maxApprovalAmount() default "999999999";
}
