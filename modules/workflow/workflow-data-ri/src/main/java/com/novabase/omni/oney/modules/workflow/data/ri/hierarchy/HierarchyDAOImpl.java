package com.novabase.omni.oney.modules.workflow.data.ri.hierarchy;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.metatype.annotations.Designate;

import com.novabase.omni.oney.modules.workflow.data.api.HierarchyDAO;
import com.novabase.omni.oney.modules.workflow.data.ri.NoSQLContextImpl;
import com.novabase.omni.oney.modules.workflow.entity.Hierarchy;
import com.novabase.omni.oney.modules.workflow.entity.HierarchyNode;
import com.novabase.omni.oney.modules.workflow.entity.HierarchyRelationships;
import com.novabase.omni.oney.modules.workflow.entity.SearchHierarchy;
import com.novabase.omni.oney.modules.workflow.entity.enums.NodeAttribute;
import com.novabase.omni.oney.modules.workflow.entity.enums.SearchDirection;
import com.novabase.omni.oney.modules.workflow.nosql.GraphClasses;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.query.OSQLSynchQuery;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientVertex;

import io.digitaljourney.platform.modules.invocation.api.PlatformInvocationManager;
import io.digitaljourney.platform.modules.nosql.graph.api.GraphProperties;
import io.digitaljourney.platform.modules.nosql.graph.api.dao.AbstractGraphDAO;
import io.digitaljourney.platform.modules.orientdb.api.OrientDBDataSource;

//@formatter:off
@Component(configurationPid = HierarchyDAOConfig.CPID, configurationPolicy = ConfigurationPolicy.REQUIRE, reference = {
     @Reference(name = GraphProperties.REF_CONTEXT, service = NoSQLContextImpl.class, cardinality = ReferenceCardinality.MANDATORY),
     @Reference(name = GraphProperties.REF_DATASOURCE, service = OrientDBDataSource.class, cardinality = ReferenceCardinality.MANDATORY),
     @Reference(
         name = GraphProperties.REF_PLATFORM_INVOCATION_MANAGER,
         service = PlatformInvocationManager.class,
         cardinality = ReferenceCardinality.MANDATORY) })
@Designate(ocd = HierarchyDAOConfig.class)
//@formatter:on
public final class HierarchyDAOImpl extends AbstractGraphDAO<HierarchyDAOConfig> implements HierarchyDAO {

    @Activate
    public void activate(ComponentContext ctx, HierarchyDAOConfig config) {
	prepare(ctx, config);
    }

    @Modified
    public void modified(HierarchyDAOConfig config) {
	prepare(config);
    }

    public List<HierarchyNode> getDescendants(String group) {
	return traverseGraph(HierarchyNode.class, group, SearchDirection.LOWER);
    }

    public List<HierarchyNode> getAscendents(String group) {
	return traverseGraph(HierarchyNode.class, group, SearchDirection.UPPER);
    }

    private <T> List<T> traverseGraph(Class<T> clazz, String group, SearchDirection direction) {
	return tx((OrientGraph connection) -> {
	    StringBuilder builder = new StringBuilder();
	    OrientVertex currentVertex = getVertexByField(connection, GraphClasses.GRAPH_HIERARCHYNODE, NodeAttribute.GROUP.getField(), group);

	    if (currentVertex != null) {
		Iterable<OrientVertex> iterable = connection.command(new OSQLSynchQuery<ODocument>(builder.append("SELECT FROM (").append("TRAVERSE ")
			.append((direction.equals(SearchDirection.LOWER) ? " inE(), outV() " : " outE(), inV() ")).append(" FROM " + currentVertex.getIdentity())
			.append((direction.equals(SearchDirection.LOWER) ? " STRATEGY BREADTH_FIRST " : " STRATEGY DEPTH_FIRST ")).append(") WHERE @class = '" + GraphClasses.GRAPH_HIERARCHYNODE + "'")
			.append(" AND $DEPTH <= " + (getConfig().maxDepth() * 2)).append(direction.equals(SearchDirection.UPPER) ? " ORDER BY hierarchycalLevel DESC " : "").toString())).execute();
		return Utils.toStream(iterable).map(vertex -> Utils.unmarshall(vertex.getRecord().toJSON(), clazz)).collect(Collectors.toList());
	    } else {
		return null;
	    }
	});
    }
    
    @Override
    public List<HierarchyNode> getDepartmentNodes(String department) {
	
	return tx((OrientGraph connection) -> {
	    
	    StringBuilder builder = new StringBuilder();
	    //@formatter:off
	    Iterable<OrientVertex> iterable = connection.command(new OSQLSynchQuery<ODocument>(
		    builder
		    	.append("SELECT FROM (")
			.append("TRAVERSE ")
			.append(" outE(), inV() ")
			.append(" FROM (SELECT FROM HierarchyNode WHERE department = '" + department + "') ")
			.append(" MAXDEPTH 1 STRATEGY BREADTH_FIRST ")
			.append(") WHERE @class = '" + GraphClasses.GRAPH_HIERARCHYNODE + "'")
			.append(" ORDER BY hierarchycalLevel ASC ").toString())).execute();
		return Utils.toStream(iterable).map(vertex -> Utils.unmarshall(vertex.getRecord().toJSON(), HierarchyNode.class)).collect(Collectors.toList());
	    //@formatter:off
	});
	
    }
    
    @Override
    public List<HierarchyNode> getAllGroups() {
	
	return tx((OrientGraph connection) -> {
	    
	    StringBuilder builder = new StringBuilder();
	    //@formatter:off
	    Iterable<OrientVertex> iterable = connection.command(new OSQLSynchQuery<ODocument>(
		    builder
		    	.append("SELECT FROM (")
			.append("TRAVERSE ")
			.append(" outE(), inV() ")
			.append(" FROM (SELECT FROM HierarchyNode) ")
			.append(" MAXDEPTH 1 STRATEGY BREADTH_FIRST ")
			.append(") WHERE @class = '" + GraphClasses.GRAPH_HIERARCHYNODE + "'")
			.append(" ORDER BY hierarchycalLevel ASC ").toString())).execute();
		return Utils.toStream(iterable).map(vertex -> Utils.unmarshall(vertex.getRecord().toJSON(), HierarchyNode.class)).collect(Collectors.toList());
	    //@formatter:off
		
	});
	
    }

    public HierarchyNode getHierachyNodeByAttribute(String attribute, String value) {
	return tx((OrientGraph connection) -> {

	    OrientVertex vertex = getVertexByField(connection, GraphClasses.GRAPH_HIERARCHYNODE, attribute, value);

	    return (vertex != null ? Utils.unmarshall(vertex.getRecord().toJSON(), HierarchyNode.class) : null);
	});
    }

    public <T> T findUpperNode(Class<T> clazz, String group, SearchHierarchy search) {
	return tx((OrientGraph connection) -> {
	    StringBuilder builder = new StringBuilder();
	    OrientVertex currentVertex = getVertexByField(connection, GraphClasses.GRAPH_HIERARCHYNODE, NodeAttribute.GROUP.getField(), group);

	    if (currentVertex != null) {
		Iterable<OrientVertex> iterable = connection.command(new OSQLSynchQuery<ODocument>(
//			    builder.append("SELECT EXPAND(inV()) FROM ")
//			    	.append("(TRAVERSE inV(), outE() FROM " + currentVertex.getIdentity() + ") ")
//			    	// TODO: construct the filter conditions in a specific/dedicated function => search.getValue()
//			    	.append(" WHERE value <= " + search.getValue())
			builder.append("SELECT EXPAND(outV()) FROM ").append("(TRAVERSE inV(), outE() FROM " + currentVertex.getIdentity() + ") ")
				// TODO: construct the filter conditions in a specific/dedicated function =>
				// search.getValue()
				.append(" WHERE value >= " + search.getValue()).append(" ORDER BY $depth ASC").toString()))
			.execute();
		Optional<OrientVertex> lastVertex = Utils.toStream(iterable).findFirst();

		return (lastVertex.isPresent() ? Utils.unmarshall(lastVertex.get().getRecord().toJSON(), clazz) : null);
	    } else {
		return null;
	    }
	});
    }

    // TODO: remover método. Replicado comportamento a nivel do Service-Ri
//    public Workflow buildWorkflow(String group, SearchHierarchy search) throws UnexpectedWorkflowException, UserGroupNotFoundException {
//	int index = 1;
//	boolean isActive = true;
//	Workflow wf = new Workflow();
//
//	// Traverse complete hierarchy (until top)
//	List<HierarchyNode> flow;
//	try {
//	    flow = traverseGraph(HierarchyNode.class, group, SearchDirection.UPPER);
//
//	    // search for last node (based on search parameters)
//	    HierarchyNode lastNode = findUpperNode(HierarchyNode.class, group, search);
//
//	    for (HierarchyNode node : flow) {
//		wf.getWorkflowSteps().add(
//			new WorkflowStepBuilder()
//				.withGroup(node.getGroup())
//				.withIndex(index++)
//				// .withHasNextStep(flow.size() > index)
//				.withActive(isActive)
//				.build());
//		isActive = (isActive & node.getName().equals(lastNode.getName()));
//	    }
//	} catch (UserGroupNotFoundException ex) {
//	    throw ex;
//	} catch (Throwable t) {
//	    throw new UnexpectedWorkflowException(t.getMessage());
//	}
//
//	return wf;
//    }

    public Hierarchy loadHierarchy(Hierarchy root, boolean isRoot) {
	if (root != null) {
	    tx((OrientGraph connection) -> {

		OrientVertex vertex = createVertex(connection, root);

		if (isRoot) {
		    Edge rootEdge = vertex.addEdge(GraphClasses.GRAPH_HIERARCHYRELATIONSHIP, vertex);
		    rootEdge.setProperty("value", getConfig().maxApprovalAmount());
		}
		
		connection.commit();

		return Utils.unmarshall(vertex.getRecord().toJSON(), HierarchyNode.class);
	    });
	}
	;
	return null;
    }

    @Override
    protected String getDataSourceTarget() {
	return this.getConfig().dataSource_target();
    }

    // ---------------------------------------------------------------------------------
    private OrientVertex createVertex(OrientGraph connection, Hierarchy hierarchy) {
	OrientVertex vertex = null;
	if (hierarchy != null) {
	    vertex = vertexExists(connection, hierarchy.getNode());
	    if (vertex == null) {
		vertex = connection.addVertex("class:" + GraphClasses.GRAPH_HIERARCHYNODE, Utils.unmarshall(Utils.marshall(hierarchy.getNode()), Map.class));
	    }
	    createDescendants(connection, vertex, hierarchy.getRelationships());
	}
	return vertex;
    }

    private OrientVertex vertexExists(OrientGraph connection, HierarchyNode node) {
	return getVertexByField(connection, GraphClasses.GRAPH_HIERARCHYNODE, NodeAttribute.GROUP.getField(), node.getGroup());
    }

    private void createDescendants(OrientGraph connection, OrientVertex vertex, HierarchyRelationships relationships) {
	if (vertex != null) {

	    // TODO: Clean existing edges? or leave as is
	    cleanEdges(vertex, Direction.IN, GraphClasses.GRAPH_HIERARCHYRELATIONSHIP);

	    if (relationships != null) {
		relationships.getDescendants().forEach(descendant -> {

		    OrientVertex child = createVertex(connection, descendant);

		    Edge edge = child.addEdge(GraphClasses.GRAPH_HIERARCHYRELATIONSHIP, vertex);

		    // TODO: passar os atributos como input da construção da Edge, em vez de criar 1
		    // a 1
		    if (relationships.getAttributes() != null) {
			relationships.getAttributes().forEach((key, value) -> {
			    edge.setProperty(key, value);
			});
		    }
		});
	    }
	}
    }

    private void cleanEdges(OrientVertex vertex, Direction direction, String type) {
	Iterable<Edge> existingEdges = vertex.getEdges(direction, type);
	existingEdges.forEach(edge -> {
	    edge.remove();
	});
    }

    @SuppressWarnings("unchecked")
    private <RET extends Vertex> RET getVertexByField(OrientGraph connection, String classname, String field, Object value) {
	return (RET) Utils.toStream(connection.getVertices(classname, new String[] { field }, new Object[] { value })).findFirst().orElse(null);
    }

    // ---------------------------------------------------------------------

    /*
     * private <T> List<T> findByGraphExpression(Class<T> clazz, String expression)
     * { return tx((OrientGraph connection) -> { Iterable<OrientVertex> iterable =
     * connection.command(new OCommandSQL(expression)).execute(); return
     * Utils.toStream(iterable).map(vertex ->
     * Utils.unmarshall(vertex.getRecord().toJSON(),
     * clazz)).collect(Collectors.toList()); }); }
     * 
     * private <T> List<T> findByExpression(Class<T> clazz, String expression) {
     * return tx((OrientGraph connection) -> { return
     * connection.getRawGraph().query(new
     * OSQLSynchQuery<ODocument>(expression)).stream().map(doc ->
     * Utils.unmarshall(((ODocument) doc).toJSON(),
     * clazz)).collect(Collectors.toList()); }); }
     * 
     * private HierarchyNode getNode(String name) { return tx((OrientGraph
     * connection) -> { OrientVertex vertex = getVertexByField(connection,
     * GraphClasses.GRAPH_HIERARCHYNODE, "name", name); if (vertex == null) { return
     * null; }
     * 
     * return Utils.unmarshall(vertex.getRecord().toJSON(), HierarchyNode.class);
     * }); }
     * 
     * private HierarchyNode deleteNode(String name) { return tx((OrientGraph
     * connection) -> { OrientVertex vertex = getVertexByField(connection,
     * GraphClasses.GRAPH_HIERARCHYNODE, "name", name); if (vertex != null) { //
     * Remove orphan-to-be targets
     * Utils.toStream(vertex.getVertices(Direction.IN)).filter(v ->
     * Utils.toStream(v.getVertices(Direction.BOTH)).count() == 1).forEach(v ->
     * connection.removeVertex(v));
     * 
     * connection.removeVertex(vertex); return
     * Utils.unmarshall(vertex.getRecord().toJSON(), HierarchyNode.class); }
     * 
     * return null; }); }
     * 
     * private HierarchyNode updateNode(HierarchyNode node) { return tx((OrientGraph
     * connection) -> { OrientVertex vertex = getVertexByField(connection,
     * GraphClasses.GRAPH_HIERARCHYNODE, "name", node.getName()); if (vertex ==
     * null) { return null; }
     * 
     * ODocument doc = vertex.getRecord(); ODocument newDoc = new
     * ODocument().fromJSON(Utils.marshall(vertex));
     * 
     * doc.merge(newDoc, true, false).save(); return Utils.unmarshall(doc.toJSON(),
     * HierarchyNode.class); }); }
     */
}
