package com.novabase.omni.oney.modules.workflow.data.ri;

import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;

import io.digitaljourney.platform.modules.nosql.api.context.AbstractNoSQLClientContext;

// @formatter:off
@Component(
	service = NoSQLContextImpl.class)
// @formatter:on
public final class NoSQLContextImpl extends AbstractNoSQLClientContext {
    @Activate
    public void activate(ComponentContext ctx) {
	prepare(ctx);
    }
}
