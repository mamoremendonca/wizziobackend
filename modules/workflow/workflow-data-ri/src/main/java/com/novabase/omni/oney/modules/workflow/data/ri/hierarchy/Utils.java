package com.novabase.omni.oney.modules.workflow.data.ri.hierarchy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import io.digitaljourney.platform.modules.commons.Platform;

public class Utils {

    public static String marshall(Object obj) {
	try {
	    ObjectMapper mapper = new ObjectMapper();
	    mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	    mapper.setSerializationInclusion(Include.NON_NULL);
	    mapper.registerModule(new JavaTimeModule());
	    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
	    return mapper.writeValueAsString(obj);
	} catch (JsonProcessingException e) {
	    throw Platform.handle("Failed to convert object to String");
	}
    }

    public static <T> T unmarshall(String json, Class<T> type) {
	try {
	    ObjectMapper mapper = new ObjectMapper();
	    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	    mapper.registerModule(new JavaTimeModule());
	    mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"));
	    return mapper.readValue(json, type);
	} catch (IOException e) {
	    throw Platform.handle("Failed to convert object to String");
	}
    }

    public static <T> Stream<T> toStream(Iterable<T> iterable) {
	return StreamSupport.stream(iterable.spliterator(), false);
    }

}
